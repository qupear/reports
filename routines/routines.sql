create definer = test_user@`%` procedure count_codes(IN date_start date, IN date_end date, IN depart_id int, IN codes varchar(255), IN pay_type int, IN detail tinyint(1), IN closed tinyint(1))
BEGIN
    if pay_type = 0 then
        if detail then 
            select p.code, u.name as doc_name, count(p.code) as code_count
            from case_services cs
                left join doctor_spec ds ON ds.doctor_id = cs.id_doctor
                left join users u ON u.id = ds.user_id
                left join departments d ON d.id = ds.department_id
                left join price p ON p.id = cs.id_profile
				left join orders o on o.id = cs.id_order
            where ((cs.date_begin between date_start and date_end and cs.is_oms = 0 and cs.id_order < 0) 
				or 
            ( cs.is_oms = 0 and o.order_type <> 1                
               and ((o.orderCompletionDate between date_start and date_end and o.status = 2) * closed 
			or (o.orderCreateDate between date_start and date_end and o.status < 2) * (not closed))))
                and (d.id = depart_id or depart_id = 0)
                and ((FIND_IN_SET(p.code, codes) or CHAR_LENGTH(codes) < 2) or codes = 'all')
            group by ds.doctor_id , cs.id_profile
            order by p.code , doc_name; 
        else
            select p.code, count(p.code) as code_count
            from case_services cs
                left join doctor_spec ds ON ds.doctor_id = cs.id_doctor
                left join departments d ON d.id = ds.department_id
                left join price p ON p.id = cs.id_profile
				left join orders o on o.id = cs.id_order
            where ((cs.date_begin between date_start and date_end and cs.is_oms = 0 and cs.id_order < 0) 
				or 
            ( cs.is_oms = 0 and o.order_type <> 1                
               and ((o.orderCompletionDate between date_start and date_end and o.status = 2) * closed 
			or (o.orderCreateDate between date_start and date_end and o.status < 2) * (not closed))))
                and (d.id = depart_id or depart_id = 0)
                and ((FIND_IN_SET(p.code, codes) or CHAR_LENGTH(codes) < 2) or codes = 'all')
            group by cs.id_profile
            order by p.code ; 
        end if;
    elseif pay_type = 1 then
        if detail then 
            select vp.profile_infis_code as code, u.name, count(cs.id_profile) as number 
            from case_services cs 
                left join doctor_spec ds ON ds.doctor_id = cs.id_doctor 
                left join users u ON u.id = ds.user_id 
                left join departments d ON d.id = ds.department_id 
                left join vmu_profile vp on vp.id_profile = cs.id_profile 
            where cs.date_begin between date_start and date_end and cs.is_oms = 1 and (d.id = depart_id or depart_id = 0) 
                and ((FIND_IN_SET(vp.profile_infis_code, codes) or CHAR_LENGTH(codes) < 2) or codes = 'all')  
            group by ds.doctor_id , cs.id_profile 
            order by code , u.name; 
        else 
            select vp.profile_infis_code as code, count(cs.id_profile) as number 
            from case_services cs 
                left join doctor_spec ds ON ds.doctor_id = cs.id_doctor 
                left join departments d ON d.id = ds.department_id 
                left join vmu_profile vp on vp.id_profile = cs.id_profile 
            where cs.date_begin between date_start and date_end and cs.is_oms = 1 and (d.id = depart_id or depart_id = 0) 
                and ((FIND_IN_SET(vp.profile_infis_code, codes) or CHAR_LENGTH(codes) < 2) or codes = 'all')  
            group by cs.id_profile 
            order by code ;
        end if;
    else
        if detail then 
            select p.code, u.name as doc_name, count(p.code) as code_count
            from case_services cs
                left join doctor_spec ds ON ds.doctor_id = cs.id_doctor
                left join users u ON u.id = ds.user_id
                left join departments d ON d.id = ds.department_id
                left join price_orto_soc p ON p.id = cs.id_profile 
                left join orders ord on ord.id = cs.id_order 
            where ((cs.date_begin between date_start and date_end and cs.is_oms = 2) 
		or (cs.id_order > 0 and ord.order_type = 1 and ((ord.orderCompletionDate between date_start and date_end and ord.status = 2) * closed 
			or (ord.orderCreateDate between date_start and date_end and ord.status < 2) * (not closed))))                and (d.id = depart_id or depart_id = 0) 
                and ((FIND_IN_SET(p.code, codes) or CHAR_LENGTH(codes) < 2) or codes = 'all')
			group by ds.doctor_id , cs.id_profile
            order by p.code , doc_name; 
        else
            select p.code, count(p.code) as code_count
            from case_services cs
                left join doctor_spec ds ON ds.doctor_id = cs.id_doctor
                left join departments d ON d.id = ds.department_id
                left join price_orto_soc p ON p.id = cs.id_profile 
                left join orders ord on ord.id = cs.id_order 
            where ((cs.date_begin between date_start and date_end and cs.is_oms = 2) 
                or (cs.id_order > 0 and ord.order_type = 1 and ((ord.orderCompletionDate between date_start and date_end and ord.status = 2) * closed 
			or (ord.orderCreateDate between date_start and date_end and ord.status < 2) * (not closed))))
                and (d.id = depart_id or depart_id = 0) 
                and ((FIND_IN_SET(p.code, codes) or CHAR_LENGTH(codes) < 2) or codes = 'all')
			group by cs.id_profile 
            order by p.code; 
        end if;
    end if;
END;

create definer = test_user@`%` procedure count_codes_withpat(IN date_start date, IN date_end date, IN depart_id int, IN codes varchar(255), IN pay_type int, IN doc_id int, IN closed tinyint(1))
BEGIN
    if pay_type = 0 then

            select p.code, u.name as doc_name
 , concat( pd.SURNAME, ' ', left(pd.NAME, 1), '.', left(pd.SECOND_NAME,1), '.') as pat_name,
                   case ID_ORDER when -1 then (select cs.id_case) else (select cs.id_order)  end
                   ,  count(p.code) as code_count
            from case_services cs
                left join doctor_spec ds ON ds.doctor_id = cs.id_doctor
                left join users u ON u.id = ds.user_id
                left join departments d ON d.id = ds.department_id
                left join price p ON p.id = cs.id_profile
              left join cases c on cs.id_case = c.id_case
              left join  patient_data pd on pd.id_patient = c.ID_PATIENT
        left join orders o on o.id = cs.id_order
            where ((cs.date_begin between date_start and date_end and cs.is_oms = 0 and cs.id_order < 0)
        or
            ( cs.is_oms = 0 and o.order_type <> 1
               and ((o.orderCompletionDate between date_start and date_end and o.status = 2) * closed
      or (o.orderCreateDate between date_start and date_end and o.status < 2) * (not closed))))
                and (d.id = depart_id or depart_id = 0)
                and ((FIND_IN_SET(p.code, codes) or CHAR_LENGTH(codes) < 2) or codes = 'all')
            and (ds.doctor_id = doc_id or doc_id = 0)
            group by ds.doctor_id , cs.id_profile, pd.surname
            order by p.code , doc_name, pat_name;


    elseif pay_type = 1 then

            select vp.profile_infis_code as code, u.name, 
                   concat( pd.SURNAME, ' ', left(pd.NAME, 1), '.', left(pd.SECOND_NAME,1), '.') as pat_name,
                   case ID_ORDER when -1 then (select cs.id_case) else (select cs.id_order)  end
                   , count(cs.id_profile) as number
            from case_services cs
                left join doctor_spec ds ON ds.doctor_id = cs.id_doctor
                left join users u ON u.id = ds.user_id
                left join departments d ON d.id = ds.department_id
                left join vmu_profile vp on vp.id_profile = cs.id_profile
            left join cases c on cs.id_case = c.id_case
              left join  patient_data pd on pd.id_patient = c.ID_PATIENT
        left join orders o on o.id = cs.id_order
         where cs.date_begin between date_start and date_end and cs.is_oms = 1 and (d.id = depart_id or depart_id = 0)
                and ((FIND_IN_SET(vp.profile_infis_code, codes) or CHAR_LENGTH(codes) < 2) or codes = 'all')
                       and (ds.doctor_id = doc_id or doc_id = 0)
             group by ds.doctor_id , cs.id_profile, pd.surname
            order by code , u.name, pat_name;

    else

            select p.code, u.name as doc_name, 
                   concat( pd.SURNAME, ' ', left(pd.NAME, 1), '.', left(pd.SECOND_NAME,1), '.') as pat_name,
                   case ID_ORDER when -1 then (select cs.id_case) else (select cs.id_order)  end
                   , count(p.code) as code_count
            from case_services cs
                left join doctor_spec ds ON ds.doctor_id = cs.id_doctor
                left join users u ON u.id = ds.user_id
                left join departments d ON d.id = ds.department_id
                left join price_orto_soc p ON p.id = cs.id_profile
                left join orders ord on ord.id = cs.id_order
           left join cases c on cs.id_case = c.id_case
              left join  patient_data pd on pd.id_patient = c.ID_PATIENT
         left join orders o on o.id = cs.id_order
         where ((cs.date_begin between date_start and date_end and cs.is_oms = 2)
		or (cs.id_order > 0 and ord.order_type = 1 and ((ord.orderCompletionDate between date_start and date_end and ord.status = 2) * closed
			or (ord.orderCreateDate between date_start and date_end and ord.status < 2) * (not closed))))
              and (d.id = depart_id or depart_id = 0)
                and ((FIND_IN_SET(p.code, codes) or CHAR_LENGTH(codes) < 2) or codes = 'all')
		            and (ds.doctor_id = doc_id or doc_id = 0)
            	group by ds.doctor_id , cs.id_profile, pd.surname
            order by p.code , doc_name, pat_name;


    end if;
END;

create definer = test_user@`%` procedure create_123_table_orders(IN date_start date, IN date_end date, IN depart_id int, IN closed tinyint(1), IN bzp tinyint(1))
BEGIN
create temporary table if not exists fio     
(SELECT id, concat(SUBSTRING_INDEX(name, ' ', 1), ' ', 
	left(SUBSTRING_INDEX(SUBSTRING_INDEX(name, ' ', 2), ' ', -1), 1), '. ',            
	left(SUBSTRING_INDEX(SUBSTRING_INDEX(name, ' ', -1), ' ', -1), 1), '.') as short_name FROM users); 
    
create temporary table if not exists t123              
(select fio.short_name, 
		ord.id_doctor, 
		cs.ID_CASE, 
		ord.orderCompletionDate,
		cs.service_cost * cs.discount * (cs.discount > 0.3) + cs.service_cost * (cs.discount <= 0.3) as cost, 
		cs.ID_PROFILE,
		concat(coalesce(p.code, ""), coalesce(pos.code,"")) as code,
		ds.spec_id, 
		pd.id_human,
		dep.id as depid         
from case_services cs    
left join orders ord on cs.ID_ORDER = ord.id        
left join price_orto_soc pos on pos.id = cs.id_profile and (cs.is_oms = 2 or ord.order_type = 1)       
left join price p on p.id = cs.id_profile and cs.is_oms = 0 and ord.order_type <> 1       
left join doctor_spec ds on cs.id_doctor = ds.doctor_id                
left join fio on ds.user_id = fio.id               
left join departments dep on ds.department_id = dep.id                
left join patient_data pd on ord.Id_Human = pd.ID_HUMAN  and pd.is_active = 1 and pd.date_end = '2200-01-01'              
where cs.id_order > 0 and 
	((ord.orderCompletionDate between date_start and date_end and ord.status = 2 and closed) or 
		(ord.orderCreateDate between date_start and date_end and ord.status < 2 and not closed)) 
	and ((order_type = 1) = bzp) 
	and (dep.id = depart_id or depart_id = 0 or depart_id is null)         
	and ds.spec_id in (107) and cs.is_oms in (0)
); 

Create temporary table if not exists docs select short_name, id_doctor from t123 group by id_doctor;
END;

create definer = test_user@`%` procedure create_temp_table(IN tname varchar(45), IN codes varchar(256))
BEGIN
set @str = concat("create temporary table if not exists ", tname, " select id_doctor, count(code) as count from t123 where code in ", codes, " group by id_doctor; ");
PREPARE stmt FROM @str; 
EXECUTE stmt;
END;

create definer = test_user@`%` function fio(name text, surname text, patronymic text) returns text
BEGIN
  RETURN CONCAT(
    `surname`,
    ' ',
    SUBSTRING(`name`, 1, 1),
    '. ',
    SUBSTRING(`patronymic`, 1, 1),
    '.');
END;

create definer = test_user@`%` procedure list_zn_st8(IN date_start date, IN date_end date, IN doc_id int, IN closed int, IN pu int)
begin
 set @rn = 0;
select  @rn := @rn + 1, T.*
from
(select pd.CARD_NUMBER, o.order_number , o.orderCreateDate,  o.orderCompletionDate,
       concat(pd.surname,' ', pd.name,' ', pd.second_name) as pat, concat(pa.UNSTRUCT_ADDRESS, '   ', coalesce(pd.REMARK,  ' ')),
       concat(SUBSTRING_INDEX(u.name, ' ', 1), ' ',
left(SUBSTRING_INDEX(SUBSTRING_INDEX(u.name, ' ', 2), ' ', -1), 1), '. ',
left(SUBSTRING_INDEX(SUBSTRING_INDEX(u.name, ' ', -1), ' ', -1), 1), '.') as doc_name,
concat(SUBSTRING_INDEX(tec.name, ' ', 1), ' ',
left(SUBSTRING_INDEX(SUBSTRING_INDEX(tec.name, ' ', 2), ' ', -1), 1), '. ',
left(SUBSTRING_INDEX(SUBSTRING_INDEX(tec.name, ' ', -1), ' ', -1), 1), '.') as tec_name,
  case when p.group_ids like '%1%' then 'Починка'
                        when p.group_ids like '%2%' then 'Имплантология'
                        when p.group_ids like '%3%' then 'Сн. и цем. коронок'
                        when pos.ID_GROUP like '%37%' then 'Починка'
                        when pos.ID_GROUP like '%38%' then 'Сн. и цем. коронок'
                        else 'Протезирование' end
                    as service_type ,  coalesce(sum(cs.SERVICE_COST ), 0) as summ, coalesce(prep.summ, 0), coalesce(sum(cs.SERVICE_COST) - sum(prep.summ), 0) as debt
                    
FROM orders o
    left join case_services cs on cs.id_order = o.id
    left join patient_data pd on o.id_human= pd.id_human and pd.is_active = 1 and pd.date_end = '2200-01-01'
    left join patient_address pa on pd.ID_ADDRESS_REG = pa.ID_ADDRESS
    left join discounts dis on dis.id = o.id_discount
    left join price_orto_soc pos on cs.id_profile = pos.id
    left join price p on cs.id_profile = p.id
    left join doctor_spec ds on o.id_doctor = ds.doctor_id
    left join users u on ds.user_id = u.id
    left join technicians tec on o.Id_technician = tec.id
    left join prepayments prep on cs.id_case = prep.case_id or cs.ID_ORDER = prep.order_id
WHERE  ((o.orderCompletionDate between date_start and date_end ) * closed
      or (o.orderCreateDate between date_start and date_end  and o.status < 2 and o.orderCompletionDate is null) * (not closed)) and
  ((o.order_type = 0 and pu = 0) or (o.order_type = 2 and pu = 1) or (o.order_type = 1 and pu = 2)) 
  and (ds.doctor_id = doc_id or doc_id = 0)
group by Order_Number
order by pat ) T;
end;


create definer = test_user@`%` procedure prcKladrFiltr(IN IN_STR_NAME varchar(255))
BEGIN
  DECLARE currentName varchar(255);
  DECLARE currentC varchar(255);
  DECLARE currentG varchar(255);
  DECLARE strC varchar(255);
  DECLARE idG int;
  DECLARE strR varchar(255);
  DECLARE idR int;
  DECLARE strP varchar(255);
  DECLARE strG varchar(255);
  DECLARE idC int;
  DECLARE poop int;

  DECLARE strG_C varchar(255);
  DECLARE strG_R varchar(255);
  DECLARE strG_G varchar(255);

  DECLARE strR_C varchar(255);
  DECLARE strR_R varchar(255);

  DECLARE resultC varchar(255) DEFAULT ' ';
  DECLARE resultR varchar(255) DEFAULT ' ';
  DECLARE resultG varchar(255) DEFAULT ' ';
  DECLARE resultP varchar(255) DEFAULT ' ';
  DECLARE resultType varchar(10) DEFAULT ' ';
  DECLARE rawDone int DEFAULT FALSE;
  DECLARE rawId int;
  DECLARE rawNum int;
  DECLARE cursRaw CURSOR FOR
  SELECT
    id, idStr
  FROM rawSearch s;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET rawDone = TRUE;
  DROP TEMPORARY TABLE IF EXISTS tblResults;
  CREATE TEMPORARY TABLE IF NOT EXISTS tblResults (
    resId int,
    resC varchar(255),
    resR varchar(255),
    resG varchar(255),
    resP varchar(255),
    resType varchar(10)
  );
  DROP TEMPORARY TABLE IF EXISTS rawSearch;
  CREATE TEMPORARY TABLE IF NOT EXISTS rawSearch (
  id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    idStr  varchar(255)
  );
  INSERT into rawSearch (idStr) SELECT
      vk.ID_KLADR
    FROM vmu_kladr vk
    WHERE vk.KLADR_NAME = IN_STR_NAME;
  OPEN cursRaw;
read_loop:
  LOOP
    FETCH cursRaw INTO rawNum,rawId;
    IF rawDone THEN
      LEAVE read_loop;
    END IF;
		
    set resultType=(SELECT vk.KLADR_TYPE FROM vmu_kladr vk WHERE vk.ID_KLADR = rawId);

    
    SET strP = (SELECT
        vk.KLADR_CODE_P
      FROM vmu_kladr vk
      WHERE vk.ID_KLADR = rawId);
    SET strG = (SELECT
        vk.KLADR_CODE_G
      FROM vmu_kladr vk
      WHERE vk.ID_KLADR = rawId);
    SET strR = (SELECT
        vk.KLADR_CODE_R
      FROM vmu_kladr vk
      WHERE vk.ID_KLADR = rawId);
    SET strC = (SELECT
        vk.KLADR_CODE_C
      FROM vmu_kladr vk
      WHERE vk.ID_KLADR = rawId);
    SET idG = (SELECT
        ID_KLADR
      FROM vmu_kladr
      WHERE ID_KLADR <= rawId
      AND KLADR_CODE_G = strG
      AND KLADR_CODE_R = strR
      AND KLADR_CODE_C = strC
      AND KLADR_CODE_P = '000'
      ORDER BY ID_KLADR DESC LIMIT 1);
    SET resultG = (SELECT
        KLADR_NAME
      FROM vmu_kladr
      WHERE ID_KLADR <= rawId
      AND KLADR_CODE_G = strG
      AND KLADR_CODE_R = strR
      AND KLADR_CODE_C = strC
      AND KLADR_CODE_P = '000'
      ORDER BY ID_KLADR DESC LIMIT 1);
    
    SET resultR = (SELECT
        KLADR_NAME
      FROM vmu_kladr
      WHERE ID_KLADR <= idG
      AND KLADR_CODE_R = strR
      AND KLADR_CODE_C = strC
      AND KLADR_CODE_G = '000'
      AND KLADR_CODE_P = '000'
      ORDER BY ID_KLADR DESC LIMIT 1);
    IF resultG=resultR THEN
      set resultG='Нет';
    END IF;
    SET resultC = (SELECT
        KLADR_NAME
      FROM vmu_kladr
      WHERE ID_KLADR <= idG
      AND KLADR_CODE_C = strC
      AND KLADR_CODE_G = '000'
      AND KLADR_CODE_P = '000'
      AND KLADR_CODE_R = '000'
      ORDER BY ID_KLADR DESC LIMIT 1);
    SET currentName = (SELECT
        vk.KLADR_NAME
      FROM vmu_kladr vk
      WHERE vk.ID_KLADR = rawId);
    INSERT INTO tblResults (resId, resC, resR, resG, resP,resType)
            VALUES (rawNum, resultC, resultR, resultG, currentName,resultType);
  END LOOP;
  CLOSE cursRaw;
  SELECT
    *
  FROM tblResults r;
END;

create definer = test_user@`%` procedure procedureIno(IN dateBegin datetime, IN dateEnd datetime)
BEGIN
  DECLARE rawDone int DEFAULT FALSE;

  DECLARE rawStrana varchar(255);
  DECLARE rawOms int;
  DECLARE isItOms int;
  DECLARE rawCase int;
  DECLARE rawFio varchar(1024);
  DECLARE rawPat int;
  DECLARE rawKolvo int;
  DECLARE rawCounter int;
  DECLARE rawCounterOMS int;
  DECLARE rawCounterPlat int;
  DECLARE finalStrana varchar(255);
  DECLARE finalFio varchar(1024);

  DECLARE innerIsOms int;

  DECLARE rawPatCase int;

  DECLARE rawId int;
  DECLARE cursPat CURSOR FOR
  SELECT
    *
  FROM rawPatients;
  DECLARE cursPatCase CURSOR FOR
  SELECT
    rId
  FROM tblResultsCase;
  DECLARE cursRaw CURSOR FOR
  SELECT
    *
  FROM rawSearch s;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET rawDone = TRUE;


  DROP TEMPORARY TABLE IF EXISTS tblResults;

  CREATE TEMPORARY TABLE IF NOT EXISTS tblResults (
    rId int,
    rOms int,
    rFio varchar(1024),
    rStrana varchar(255),
    rPat int
  );

  DROP TEMPORARY TABLE IF EXISTS tblFinal;

  CREATE TEMPORARY TABLE IF NOT EXISTS tblFinal (
    rId int,
    rCntOms int,
    rCntPlat int,
    rFio varchar(1024),
    rStrana varchar(255)
  /*
  rOms int,
  ,
  rPat int,
  rCntOms int,
  rCntPlat int    */
  );

  DROP TEMPORARY TABLE IF EXISTS rawSearch;
  CREATE TEMPORARY TABLE IF NOT EXISTS rawSearch AS (SELECT DISTINCT
      zap.zapCase AS zap2C,
      zap.zapOms AS zap2O
    FROM (SELECT
        cs.ID_CASE AS zapCase,
        cs.IS_OMS AS zapOms
      FROM case_services cs
        INNER JOIN cases ca
          ON cs.ID_CASE = ca.ID_CASE
        INNER JOIN patient_data pd
          ON pd.id_patient = ca.id_patient
        INNER JOIN vmu_oksm vo
          ON pd.C_OKSM = vo.CODE
      WHERE cs.DATE_BEGIN >= dateBegin
      AND cs.DATE_BEGIN <= dateEnd
      AND pd.IS_ACTIVE = 1
      AND vo.COUNTRY_SHORT_NAME <> 'РОССИЯ'
      AND ((cs.IS_OMS = 1)
      OR (cs.IS_OMS = 0))
      AND ca.id_patient IN (SELECT
          ca.id_patient AS zapPat
        FROM case_services cs
          INNER JOIN cases ca
            ON cs.ID_CASE = ca.ID_CASE
          INNER JOIN patient_data pd
            ON pd.id_patient = ca.id_patient
          INNER JOIN vmu_oksm vo
            ON pd.C_OKSM = vo.CODE
        WHERE cs.DATE_BEGIN >= dateBegin
        AND cs.DATE_BEGIN <= dateEnd
        AND pd.IS_ACTIVE = 1
        AND vo.COUNTRY_SHORT_NAME <> 'РОССИЯ'
        AND ((cs.IS_OMS = 1)
        OR (cs.IS_OMS = 0))
        GROUP BY cs.ID_CASE)) AS zap
    ORDER BY zap.zapCase, zap.zapOms);

  DROP TEMPORARY TABLE IF EXISTS rawCasesSelect;
  CREATE TEMPORARY TABLE IF NOT EXISTS rawCasesSelect AS (SELECT DISTINCT
      zap.zapCase AS zap2C,
      zap.zapOms AS zap2O
    FROM (SELECT
        cs.ID_CASE AS zapCase,
        cs.IS_OMS AS zapOms
      FROM case_services cs
        INNER JOIN cases ca
          ON cs.ID_CASE = ca.ID_CASE
        INNER JOIN patient_data pd
          ON pd.id_patient = ca.id_patient
        INNER JOIN vmu_oksm vo
          ON pd.C_OKSM = vo.CODE
      WHERE cs.DATE_BEGIN >= dateBegin
      AND cs.DATE_BEGIN <= dateEnd
      AND pd.IS_ACTIVE = 1
      AND vo.COUNTRY_SHORT_NAME <> 'РОССИЯ'
      AND ((cs.IS_OMS = 1)
      OR (cs.IS_OMS = 0))
      AND ca.id_patient IN (SELECT
          ca.id_patient AS zapPat
        FROM case_services cs
          INNER JOIN cases ca
            ON cs.ID_CASE = ca.ID_CASE
          INNER JOIN patient_data pd
            ON pd.id_patient = ca.id_patient
          INNER JOIN vmu_oksm vo
            ON pd.C_OKSM = vo.CODE
        WHERE cs.DATE_BEGIN >= dateBegin
        AND cs.DATE_BEGIN <= dateEnd
        AND pd.IS_ACTIVE = 1
        AND vo.COUNTRY_SHORT_NAME <> 'РОССИЯ'
        AND ((cs.IS_OMS = 1)
        OR (cs.IS_OMS = 0))
        GROUP BY cs.ID_CASE)) AS zap
    ORDER BY zap.zapCase, zap.zapOms);

  OPEN cursRaw;
read_loop:
  LOOP

    FETCH cursRaw INTO rawId, rawOms;

    IF rawDone THEN
      LEAVE read_loop;
    END IF;
		
		

    SELECT
      CONCAT(pd.SURNAME, ' ', pd.name, ' ', pd.SECOND_NAME),
      vo.COUNTRY_SHORT_NAME,
      pd.id_patient
    FROM case_services cs
      INNER JOIN cases ca
        ON cs.ID_CASE = ca.ID_CASE
      INNER JOIN patient_data pd
        ON pd.id_patient = ca.id_patient
      INNER JOIN vmu_oksm vo
        ON pd.C_OKSM = vo.CODE
    WHERE cs.DATE_BEGIN >= dateBegin
    AND cs.DATE_BEGIN <= dateEnd
    AND pd.IS_ACTIVE = 1
    AND vo.COUNTRY_SHORT_NAME <> 'РОССИЯ'
    AND ((cs.IS_OMS = 1)
    OR (cs.IS_OMS = 0))
    AND ca.id_patient IN (SELECT
        ca.id_patient AS zapPat
      FROM case_services cs
        INNER JOIN cases ca
          ON cs.ID_CASE = ca.ID_CASE
        INNER JOIN patient_data pd
          ON pd.id_patient = ca.id_patient
        INNER JOIN vmu_oksm vo
          ON pd.C_OKSM = vo.CODE
      WHERE cs.DATE_BEGIN >= dateBegin
      AND cs.DATE_BEGIN <= dateEnd
      AND pd.IS_ACTIVE = 1
      AND vo.COUNTRY_SHORT_NAME <> 'РОССИЯ'
      AND ((cs.IS_OMS = 1)
      OR (cs.IS_OMS = 0))
      AND pd.id_patient = (SELECT
          ca.id_patient
        FROM cases ca
        WHERE ca.ID_CASE = rawId LIMIT 1)
      GROUP BY cs.ID_CASE) LIMIT 1 INTO rawFio, rawStrana, rawPat;


    SELECT
      COUNT(rawCasesSelect.zap2C)
    FROM rawCasesSelect
    WHERE rawCasesSelect.zap2C = rawId INTO rawCounter;



    IF (rawCounter = 2) THEN
    BEGIN

      DELETE
        FROM rawCasesSelect
      WHERE zap2C = rawId
        AND zap2O = 0;




      INSERT INTO tblResults (rId, rOms, rFio, rStrana, rPat)
        VALUES (rawId, 1, rawFio, rawStrana, rawPat);
    END;
    END IF;


    IF (rawCounter = 1) THEN
    BEGIN



      INSERT INTO tblResults (rId, rOms, rFio, rStrana, rPat)
        VALUES (rawId, rawOms, rawFio, rawStrana, rawPat);


    END;
    END IF;

    DELETE
      FROM rawCasesSelect
    WHERE rawCasesSelect.zap2C = rawId;

  END LOOP;
  CLOSE cursRaw;

  BEGIN
    DECLARE done1 int DEFAULT FALSE;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;

    DROP TEMPORARY TABLE IF EXISTS rawPatients;
    CREATE TEMPORARY TABLE IF NOT EXISTS rawPatients AS (SELECT DISTINCT
        r.rPat
      FROM tblResults r);
    OPEN cursPat;
  read_loop1:
    LOOP

      FETCH cursPat INTO rawPat;





      IF done1 THEN
        LEAVE read_loop1;
      END IF;




      BEGIN
        DECLARE done2 int DEFAULT FALSE;
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done2 = TRUE;

        DROP TEMPORARY TABLE IF EXISTS tblResultsCase;
        CREATE TEMPORARY TABLE IF NOT EXISTS tblResultsCase AS (SELECT DISTINCT
            r.rId
          FROM tblResults r
          WHERE r.rPat = rawPat);

        OPEN cursPatCase;
      _loop:
        LOOP

          FETCH cursPatCase INTO rawPatCase;

          IF done2 THEN
            LEAVE _loop;
          END IF;

          SELECT
            r.rOms
          FROM tblResults r
          WHERE r.rId = rawPatCase INTO innerIsOms;
          IF (innerIsOms = 1) THEN
          BEGIN
            IF(rawCounterPlat is null) THEN set rawCounterPlat=0;END IF;
            IF(rawCounterOMS is null) THEN set rawCounterOMS =0;END IF;
            SET rawCounterOMS = rawCounterOMS + 1;
          END;
          END IF;
          IF (innerIsOms = 0) THEN
          BEGIN
            IF(rawCounterOMS is null) THEN set rawCounterOMS =0;END IF;
            IF(rawCounterPlat is null) THEN set rawCounterPlat=0;END IF;
            SET rawCounterPlat = rawCounterPlat + 1;
          END;
          END IF;

        END LOOP;
        CLOSE cursPatCase;
      END;

    SELECT r.rFio,r.rStrana FROM tblResults r WHERE r.rPat=rawPat LIMIT 1 INTO finalFio,finalStrana;

      INSERT INTO tblFinal (rId, rCntOms, rCntPlat,rFio,rStrana)
        VALUES (rawPat, rawCounterOMS, rawCounterPlat,finalFio,finalStrana);
      SET rawCounterOMS = 0;
      SET rawCounterPlat = 0;

    END LOOP;
    CLOSE cursPat;
  END;
  SELECT
    *
  FROM tblFinal;
END;

create definer = test_user@`%` procedure reestr_zn_st8(IN date_start date, IN date_end_ date, IN closed int, IN pu int)
begin
    set @rn = 0;
select  sr.name, @rn := @rn + 1, group_concat(distinct o.order_number separator  ',') as ord_num,
       concat(pd.surname,' ', pd.name,' ', pd.second_name) as pat, pa.UNSTRUCT_ADDRESS as adr,
o.bzp_direction_text,  case when p.group_ids like '%1%' then 'Починка'
                        when p.group_ids like '%2%' then 'Имплантология'
                        when p.group_ids like '%3%' then 'Сн. и цем. коронок'
                        when pos.ID_GROUP like '%37%' then 'Починка'
                        when pos.ID_GROUP like '%38%' then 'Сн. и цем. коронок'
                        else 'Протезирование' end
                    as service_type , o.orderCreateDate,  o.orderCompletionDate, sum(cs.SERVICE_COST ) as summ
FROM orders o
    left join case_services cs on cs.id_order = o.id
    left join patient_data pd on o.id_human= pd.id_human
    left join patient_address pa on pd.ID_ADDRESS_REG = pa.ID_ADDRESS
    left join discounts dis on dis.id = o.id_discount
    left join price_orto_soc pos on cs.id_profile = pos.id
    left join price_groups pg on pos.id_group = pg.id
    left join price p on pg.id = p.GROUP_IDs
    left join spb_regions sr on o.bzp_region = sr.id
WHERE  ((o.orderCompletionDate between date_start and date_end_ and o.status = 2) * closed
			or (o.orderCreateDate between date_start and date_end_ and o.status < 2 and o.orderCompletionDate is null) * (not closed)) and
  ((o.order_type = 0 and pu = 0) or (o.order_type = 1 and pu = 1) or (o.order_type = 2 and pu = 2))
group by sr.name, pat, adr
union
select '','', '', '', '', 'Итого: сумма по заказ-нарядам', '', '', '', sum(cs.SERVICE_COST ) as summ
FROM orders o
left join case_services cs on cs.id_order = o.id
left join patient_data pd on o.id_human= pd.id_human and pd.is_active = 1 and pd.date_end = '2200-01-01'
    left join patient_address pa on pd.ID_ADDRESS_REG = pa.ID_ADDRESS
    left join discounts dis on dis.id = o.id_discount
    left join price_orto_soc pos on cs.id_profile = pos.id
    left join price p on cs.id_profile = p.id
    WHERE  ((o.orderCompletionDate between date_start and date_end_ and o.status = 2) * closed
			or (o.orderCreateDate between date_start and date_end_ and o.status < 2 and o.orderCompletionDate is null ) * (not closed))
 and   ((o.order_type = 0 and pu = 0) or (o.order_type = 2 and pu = 1) or (o.order_type = 1 and pu = 2))
union
select '', '', '', '', 'Итого: всего пациентов', count(distinct o.Id_Human), '', '', '', ''
FROM orders o
    WHERE  ((o.orderCompletionDate between date_start and date_end_ and o.status = 2) * closed
			or (o.orderCreateDate between date_start and date_end_ and o.status < 2 and o.orderCompletionDate is null ) * (not closed))
 and   ((o.order_type = 0 and pu = 0) or (o.order_type = 1 and pu = 1) or (o.order_type = 2 and pu = 2))
union
select '', '', '', '', 'Итого: всего заказ-нарядов', count(o.ID), '', '', '', ''
FROM orders o
    WHERE  ((o.orderCompletionDate between date_start and date_end_ and o.status = 2) * closed
			or (o.orderCreateDate between date_start and date_end_ and o.status < 2 and o.orderCompletionDate is null) * (not closed))
 and   ((o.order_type = 0 and pu = 0) or (o.order_type = 1 and pu = 1) or (o.order_type = 2 and pu = 2))
;
  end;

create definer = test_user@`%` function short_name(long_name varchar(255)) returns varchar(255)
BEGIN
 DECLARE short_name varchar(255);
  select concat(substring_index(long_name, ' ', 1), ' ',
       left(substring_index(substring_index(long_name, ' ', -2), ' ', 1), 1),
'. ',
        left(substring_index(long_name, ' ', -1),1), '.') into short_name;
  RETURN short_name;
END;

create definer = root@localhost procedure stoma_8_work_types(IN date_start date, IN date_end date, IN closed int, IN pu int, IN tech int)
BEGIN


create temporary table if not exists orders_services  
select cs.id_order, ord.id_doctor, cs.service_cost, cs.discount, 
    ord.order_type, cs.id_profile as id_profile, p.code as p_code, p.group_ids as p_group, pos.code as pos_code, pos.id_group as pos_group, 
        coalesce(p.in_tech,0) + coalesce(pos.in_tech,0) as in_tech, t.id as id_tech, t.name as tech_name       
from case_services cs 
left join orders ord on ord.id = cs.id_order 
left join price p on p.id = cs.id_profile and ord.order_type <> 1 
left join price_orto_soc pos on pos.id = cs.id_profile and ord.order_type = 1 
left join doctor_spec ds on ds.doctor_id = ord.id_doctor 
left join technicians t on t.id = ord.id_technician 
where cs.id_order > 0 
        and ((ord.order_type <> 1 and pu = 0) or (ord.order_type = 1 and pu = 2))   
        and ds.spec_id = 107  
		and ((ord.orderCompletionDate between date_start and date_end and ord.status = 2 and closed) or   
             (ord.orderCreateDate between date_start and date_end and ord.status < 2 and not closed))  ; 

/* REPAIRS */ 
create temporary table if not exists orders_repair 
select id_order from orders_services os 
where ((os.order_type <> 1 and p_group like "%1%") or (os.order_type = 1 and pos_group like "%111%"))   
group by os.id_order; 
create temporary table if not exists repair_docs 
select id_doctor, id_tech, sum(service_cost * discount) as total from orders_services 
where id_order in (select id_order from orders_repair) and in_tech >= tech
group by case when tech = 0 then id_doctor else id_tech end; 

/* PROTEZ */ 
create temporary table if not exists orders_protez  
select id_order from orders_services os 
where ((os.order_type <> 1 and p_group like "%4%") or (os.order_type = 1 and pos_group like '%38%' or pos_group like '%39%' or pos_group like '%40%' or pos_group like '%41%')) 
        and os.id_order not in (select id_order from orders_repair) 
group by os.id_order; 
create temporary table if not exists protez_docs 
select id_doctor, id_tech, sum(service_cost * discount) as total from orders_services 
where id_order in (select id_order from orders_protez) and in_tech >= tech  
group by case when tech = 0 then id_doctor else id_tech end; 

/* CEMENT */ 
create temporary table if not exists orders_cement  
select id_order from orders_services os 
where ((os.order_type <> 1 and p_group like "%3%") or (os.order_type = 1 and pos_group like '%112%')) 
        and os.id_order not in (select id_order from orders_repair) 
        and os.id_order not in (select id_order from orders_protez) 
group by os.id_order; 
create temporary table if not exists cement_docs 
select id_doctor, id_tech, sum(service_cost * discount) as total from orders_services 
where id_order in (select id_order from orders_cement) and in_tech >= tech 
group by case when tech = 0 then id_doctor else id_tech end; 

/* IMPLANT */ 
create temporary table if not exists orders_implant   
select id_order from orders_services os 
where (os.order_type <> 1 and p_group like "%2%")   
        and os.id_order not in (select id_order from orders_repair) 
        and os.id_order not in (select id_order from orders_protez) 
        and os.id_order not in (select id_order from orders_cement) 
group by os.id_order; 
create temporary table if not exists implant_docs 
select id_doctor, id_tech, sum(service_cost * discount) as total from orders_services 
where id_order in (select id_order from orders_implant) and in_tech >= tech 
group by case when tech = 0 then id_doctor else id_tech end; 

/* ANEST */ 
create temporary table if not exists orders_anest  
select id_order from orders_services os 
where ((os.order_type <> 1 and p_code in ('51010', '51011')) or (os.order_type = 1 and pos_code = '1.6')) 
        and os.id_order not in (select id_order from orders_repair) 
        and os.id_order not in (select id_order from orders_protez) 
        and os.id_order not in (select id_order from orders_cement) 
        and os.id_order not in (select id_order from orders_implant) 
group by os.id_order; 
create temporary table if not exists anest_docs 
select id_doctor, id_tech, sum(service_cost * discount) as total from orders_services 
where id_order in (select id_order from orders_anest) and in_tech >= tech 
group by case when tech = 0 then id_doctor else id_tech end; 

/* OSMOTR */ 
create temporary table if not exists orders_osmotr 
select id_order from orders_services os 
where ((os.order_type <> 1 and p_code in ('51001', '51002')) or (os.order_type = 1 and pos_code in ('1.1','1.4'))) 
        and os.id_order not in (select id_order from orders_repair) 
        and os.id_order not in (select id_order from orders_protez) 
        and os.id_order not in (select id_order from orders_cement) 
        and os.id_order not in (select id_order from orders_implant) 
        and os.id_order not in (select id_order from orders_anest) 
group by os.id_order; 
create temporary table if not exists osmotr_docs 
select id_doctor, id_tech, sum(service_cost * discount) as total from orders_services 
where id_order in (select id_order from orders_osmotr) and in_tech >= tech 
group by case when tech = 0 then id_doctor else id_tech end; 

if tech = 0 then 
    select 	u.name as doc_name, coalesce(T1.total,0), coalesce(T2.total,0), coalesce(T3.total,0), 
        coalesce(T4.total,0), coalesce(T5.total,0), coalesce(T6.total,0), 
        coalesce(T1.total,0) + coalesce(T2.total,0) + coalesce(T3.total,0) + coalesce(T4.total,0) + coalesce(T5.total,0) + coalesce(T6.total,0)  
    from doctor_spec ds  
    left join users u on u.id = ds.user_id 
    left join protez_docs T1 on T1.id_doctor = ds.doctor_id 
    left join cement_docs T2 on T2.id_doctor = ds.doctor_id 
    left join repair_docs T3 on T3.id_doctor = ds.doctor_id 
    left join osmotr_docs T4 on T4.id_doctor = ds.doctor_id 
    left join anest_docs T5 on T5.id_doctor = ds.doctor_id 
    left join implant_docs T6 on T6.id_doctor = ds.doctor_id 
    where ds.spec_id = 107 and  ds.user_id <> 1 and coalesce(T1.total,0) + coalesce(T2.total,0) + coalesce(T3.total,0) + coalesce(T4.total,0) + coalesce(T5.total,0) + coalesce(T6.total,0) > 0 
    order by u.name ;
else 
    select 	t.name as tech_name, coalesce(T1.total,0), coalesce(T2.total,0), coalesce(T3.total,0), 
        coalesce(T4.total,0), coalesce(T5.total,0), coalesce(T6.total,0), 
        coalesce(T1.total,0) + coalesce(T2.total,0) + coalesce(T3.total,0) + coalesce(T4.total,0) + coalesce(T5.total,0) + coalesce(T6.total,0) 
    from technicians t   
    left join protez_docs T1 on T1.id_tech = t.id 
    left join cement_docs T2 on T2.id_tech = t.id 
    left join repair_docs T3 on T3.id_tech = t.id 
    left join osmotr_docs T4 on T4.id_tech = t.id 
    left join anest_docs T5 on T5.id_tech = t.id 
    left join implant_docs T6 on T6.id_tech = t.id 
    where coalesce(T1.total,0) + coalesce(T2.total,0) + coalesce(T3.total,0) + coalesce(T4.total,0) + coalesce(T5.total,0) + coalesce(T6.total,0) > 0 
    order by t.name ;
end if;

END;

create definer = test_user@`%` procedure xray_daily(IN date_start date, IN doc_id int, IN doc tinyint(1))
BEGIN

create temporary table if not exists doc_xrays 
select ro.id as ro_id, ro.dose, concat(pd.surname, " ", pd.name, " ", pd.second_name) as pat_name, pd.birthday, pa.unstruct_address,  
    ro.shots, 
		coalesce(concat(SUBSTRING_INDEX(u.name, ' ', 1), ' ', 
				 left(SUBSTRING_INDEX(SUBSTRING_INDEX(u.name, ' ', 2), ' ', -1), 1), '. ',            
		         left(SUBSTRING_INDEX(SUBSTRING_INDEX(u.name, ' ', -1), ' ', -1), 1), '.'), "Врач из другого ЛПУ") as send_doc, 
	di.date_value, di.teeth_value, di.child_ref_ids, di.id_arbitrary_text, di.value, art.text as arbitrary_text, ro.date as shot_date   
from roentgen ro 
left join directions di on di.id = ro.id_direction 
left join doctor_spec ds on ds.doctor_id = di.id_send_doctor 
left join users u on u.id = ds.user_id 
left join cases c on c.id_case = ro.case_id 
left join patient_data pd on pd.id_patient = c.id_patient 
left join patient_address pa on pa.id_address = pd.id_address_reg 
left join arbitrary_texts art on art.id = di.id_arbitrary_text 
where ((ro.date = date_start and ro.Assist_Doc_ID = doc_id) or doc) and ((ro.date_description = date_start and ro.Doc_id = doc_id) or not doc)  
    AND di.id_direction in (2,32); 

create temporary table if not exists xray_proto_1 
select T.ro_id, concat(T.s1, T.s2, T.s3) as val from       
(select dx.ro_id,  
	case when (locate(';50;', dx.child_ref_ids) > 0) then "срочно " else "" end as s1,   
	case when (locate(';60;', dx.child_ref_ids) > 0 or locate('810', dx.child_ref_ids) > 0) then "с описанием" else "" end as s2,  
	case when (locate(';70;', dx.child_ref_ids) > 0 or locate('820', dx.child_ref_ids) > 0) then "без описания" else "" end as s3 
from doc_xrays dx) T; 

create temporary table if not exists xray_proto_2 
select T.ro_id, concat(T.s1, T.s2, T.s3, T.s4, T.s5, T.s6, T.s7, T.s8, T.s9, T.s10, T.s11, T.s12, T.s13, T.s14, T.s15, T.s16, T.s17, T.s18, T.s19, T.s20, T.s21, T.s22, T.s23, T.s24, T.s25, T.s26) as val from       
(select dx.ro_id,  
	case when (locate(';90;', dx.child_ref_ids) > 0) then concat("внутриротовой снимок (плёночный) ", dx.teeth_value, " зуба(ов). ") else "" collate utf8_general_ci end as s1,   
	case when (locate('100', dx.child_ref_ids) > 0) then concat("внутриротовой снимок (цифровой) ", dx.teeth_value, " зуба(ов). ") else "" collate utf8_general_ci end as s2,   
	case when (locate('110', dx.child_ref_ids) > 0) then "нижняя челюсть справа внеротовой снимок. " else "" collate utf8_general_ci end as s3,   
	case when (locate('120', dx.child_ref_ids) > 0) then "нижняя челюсть слева внеротовой снимок. " else "" collate utf8_general_ci end as s4,   
	case when (locate('130', dx.child_ref_ids) > 0) then "твёрдое нёбо снимок «вприкус». " else "" collate utf8_general_ci end as s5,   
	case when (locate('140', dx.child_ref_ids) > 0) then "дно полости рта снимок «вприкус». " else "" collate utf8_general_ci end as s6,   
	case when (locate('150', dx.child_ref_ids) > 0) then "ОПТГ (плёночный). " else "" collate utf8_general_ci end as s7,   
	case when (locate('160', dx.child_ref_ids) > 0) then "ОПТГ (цифровой). " else "" collate utf8_general_ci end as s8,   
	case when (locate('161', dx.child_ref_ids) > 0) then "ОПТГ верхнечелюстные синусы (плёночный). " else "" collate utf8_general_ci end as s9,   
	case when (locate('162', dx.child_ref_ids) > 0) then "ОПТГ верхнечелюстные синусы (цифровой). " else "" collate utf8_general_ci end as s10,   
	case when (locate('190', dx.child_ref_ids) > 0) then "ОПТГ ВНЧС (открытый/закрытый рот) (плёночный). " else "" collate utf8_general_ci end as s11,   
	case when (locate('200', dx.child_ref_ids) > 0) then "ОПТГ ВНЧС (открытый/закрытый рот) (цифровой). " else "" collate utf8_general_ci end as s12,   
	case when (locate('205', dx.child_ref_ids) > 0) then "КЛКТ (КТ 3D). " else "" collate utf8_general_ci end as s13,   
	case when (locate('207', dx.child_ref_ids) > 0) then "КЛКТ (КТ 3D) всех придаточных пазух носа 13х15см. " else "" collate utf8_general_ci end as s14,   
	case when (locate('210', dx.child_ref_ids) > 0) then "КЛКТ (КТ 3D) верхняя и нижняя челюсти. " else "" collate utf8_general_ci end as s15,   
	case when (locate('220', dx.child_ref_ids) > 0) then "КЛКТ (КТ 3D) верхняя челюсть и верхнечелюстные синусы. " else "" collate utf8_general_ci end as s16,   
	case when (locate('224', dx.child_ref_ids) > 0) then "КЛКТ (КТ 3D) верхнечелюстного синуса справа. " else "" collate utf8_general_ci end as s17,   
	case when (locate('227', dx.child_ref_ids) > 0) then "КЛКТ (КТ 3D) верхнечелюстного синуса слева. " else "" collate utf8_general_ci end as s18,   
	case when (locate('230', dx.child_ref_ids) > 0) then "КЛКТ (КТ 3D) височно-нижнечелюстной сустав справа. " else "" collate utf8_general_ci end as s19,   
	case when (locate('240', dx.child_ref_ids) > 0) then "КЛКТ (КТ 3D) височно-нижнечелюстной сустав слева. " else "" collate utf8_general_ci end as s20,   
	case when (locate('241', dx.child_ref_ids) > 0) then "КЛКТ (КТ 3D) 2х височно-нижнечелюстных суставов 8х15см. " else "" collate utf8_general_ci end as s21,   
	case when (locate('250', dx.child_ref_ids) > 0) then "ТРГ прямая. " else "" collate utf8_general_ci end as s22,   
	case when (locate('260', dx.child_ref_ids) > 0) then "ТРГ боковая. " else "" collate utf8_general_ci end as s23,   
	case when (locate('840', dx.child_ref_ids) > 0) then concat("КЛКТ (КТ 3D) KaVo ", coalesce(dx.value, ""), ". ") else "" collate utf8_general_ci end as s24,   
	case when (locate('850', dx.child_ref_ids) > 0) then concat("КЛКТ (КТ 3D) Sirona ", coalesce(dx.value, ""), ". ") else "" collate utf8_general_ci end as s25,   
	case when (locate('930', dx.child_ref_ids) > 0) then "КЛКТ. " else "" collate utf8_general_ci end as s26   
from doc_xrays dx) T; 

create temporary table if not exists xray_proto_3 
select T.ro_id, concat(T.s1, T.s2, T.s3, T.s4, T.s5, T.s6, T.s7, T.s8, T.s9, T.s10) as val from       
(select dx.ro_id,  
	case when (locate('330', dx.child_ref_ids) > 0) then "ОМС" else "" end as s1,   
	case when (locate('340', dx.child_ref_ids) > 0) then "ДПУ" else "" end as s2,  
	case when (locate('350', dx.child_ref_ids) > 0) then "БЗП" else "" end as s3,  
	case when (locate('360', dx.child_ref_ids) > 0) then "Д/О" else "" end as s4,  
	case when (locate('370', dx.child_ref_ids) > 0) then "ЦЧЛХ" else "" end as s5,  
	case when (locate('380', dx.child_ref_ids) > 0) then "ЛОР ОМС" else "" end as s6,  
	case when (locate('390', dx.child_ref_ids) > 0) then "ЛОР ДПУ" else "" end as s7,  
	case when (locate('400', dx.child_ref_ids) > 0) then "СП" else "" end as s8,  
	case when (locate('401', dx.child_ref_ids) > 0) then "ВнДог" else "" end as s9,  
	case when (locate('920', dx.child_ref_ids) > 0) then "ГРСЦ" else "" end as s10  
from doc_xrays dx) T; 

set @num = 0;
select @num := @num + 1, T.* from 
(select dx.pat_name, dx.birthday, dx.unstruct_address, xr1.val v1, xr2.val v2, dx.shots, dx.shot_date, dx.send_doc, dx.dose, xr3.val v3, dx.arbitrary_text  
from doc_xrays dx 
left join xray_proto_1 xr1 on dx.ro_id = xr1.ro_id 
left join xray_proto_2 xr2 on dx.ro_id = xr2.ro_id 
left join xray_proto_3 xr3 on dx.ro_id = xr3.ro_id 
order by dx.ro_id ) T; 

END;

create definer = test_user@`%` procedure xray_month(IN date_start date, IN date_end date, IN doc_id int, IN doc tinyint(1))
BEGIN

create temporary table if not exists temp_r_report    
(select d.id_direction,  coalesce(r.Shots,0) as shots, 
	date_add(pd.birthday, interval 18 year) > r.date as child,  
	ds.spec_id, 
	locate(';90', d.child_ref_ids) > 0 as p90, 
	locate(';100', d.child_ref_ids) > 0 as p100, 
	locate(';110', d.child_ref_ids) > 0 as p110, 
	locate(';120', d.child_ref_ids) > 0 as p120, 
	locate(';130', d.child_ref_ids) > 0 as p130, 
	locate(';140', d.child_ref_ids) > 0 as p140, 
	locate(';150', d.child_ref_ids) > 0 as p150, 
	locate(';160', d.child_ref_ids) > 0 as p160, 
	locate(';161', d.child_ref_ids) > 0 as p161, 
	locate(';162', d.child_ref_ids) > 0 as p162, 
	locate(';170', d.child_ref_ids) > 0 as p170, 
	locate(';180', d.child_ref_ids) > 0 as p180, 
	locate(';190', d.child_ref_ids) > 0 as p190, 
	locate(';200', d.child_ref_ids) > 0 as p200, 
	locate(';205', d.child_ref_ids) > 0 as p205, 
	locate(';207', d.child_ref_ids) > 0 as p207, 
	locate(';210', d.child_ref_ids) > 0 as p210, 
	locate(';220', d.child_ref_ids) > 0 as p220, 
	locate(';224', d.child_ref_ids) > 0 as p224, 
	locate(';227', d.child_ref_ids) > 0 as p227, 
	locate(';230', d.child_ref_ids) > 0 as p230, 
	locate(';240', d.child_ref_ids) > 0 as p240, 
	locate(';241', d.child_ref_ids) > 0 as p241, 
	locate(';250', d.child_ref_ids) > 0 as p250, 
	locate(';260', d.child_ref_ids) > 0 as p260, 
	locate(';330', d.child_ref_ids) > 0 as p330, 
	locate(';340', d.child_ref_ids) > 0 as p340, 
	locate(';350', d.child_ref_ids) > 0 as p350, 
	locate(';360', d.child_ref_ids) > 0 as p360, 
	locate(';370', d.child_ref_ids) > 0 as p370, 
	locate(';380', d.child_ref_ids) > 0 as p380, 
	locate(';390', d.child_ref_ids) > 0 as p390, 
	locate(';400', d.child_ref_ids) > 0 as p400, 
	locate(';401', d.child_ref_ids) > 0 as p401, 
	locate(';560', d.child_ref_ids) > 0 as p560, 
	locate(';570', d.child_ref_ids) > 0 as p570, 
	locate(';580', d.child_ref_ids) > 0 as p580, 
	locate(';840', d.child_ref_ids) > 0 as p840, 
	locate(';850', d.child_ref_ids) > 0 as p850 
from roentgen r 
left join directions d on d.id = r.id_direction 
left join cases c on c.id_case = r.case_id 
left join patient_data pd on pd.id_patient = c.id_patient 
left join doctor_spec ds on ds.doctor_id = d.id_send_doctor 
left join doctor_spec ds_a on ds_a.doctor_id = r.Assist_Doc_ID  
left join doctor_spec ds_r on ds_r.doctor_id = r.Doc_ID  
where
	 (r.date between date_start and date_end
    and r.Assist_Doc_ID = doc_id or doc or (r.date between date_start and date_end and doc_id = 0 and ds_a.spec_id = 208))
    and (r.Date_description between date_start and date_end
           and r.Doc_ID = doc_id or not doc or (r.Date_description between date_start and date_end and doc_id = 0 and ds_r.spec_id = 60))
and r.dose > 0 and r.case_id > 0 and r.id_direction > 0 );

set @summ0 = (select coalesce(count(id_direction),0) from temp_r_report); 
set @summ1 = (select coalesce(count(id_direction),0) from temp_r_report where (p330 or p370 or p380) or id_direction = 32); 
set @summ2 = (select coalesce(count(id_direction),0) from temp_r_report where (p330 or p370 or p380 or id_direction = 32) and child = 0);     
set @summ3 = (select coalesce(count(id_direction),0) from temp_r_report where (p380) );    
set @summ4 = (select coalesce(count(id_direction),0) from temp_r_report where (p370) );    
set @summ5 = (select coalesce(count(id_direction),0) from temp_r_report where id_direction = 32 and child = 0);    
set @summ6 = (select coalesce(count(id_direction),0) from temp_r_report where id_direction = 32 and child = 1);    
set @summ7 = (select coalesce(count(id_direction),0) from temp_r_report where not (p330 or p370 or p380) and not p350 and id_direction = 2);    
set @summ8 = (select coalesce(count(id_direction),0) from temp_r_report where (not (p330 or p370 or p380) and not p350) and child = 0 and id_direction = 2);    
set @summ9 = (select coalesce(count(id_direction),0) from temp_r_report where (p390) );    
set @summ10 = (select coalesce(count(id_direction),0) from temp_r_report where (p370 and p340));    
set @summ11 = (select coalesce(count(id_direction),0) from temp_r_report where spec_id = 107 and child = 0 and not p350);    
set @summ12 = (select coalesce(count(id_direction),0) from temp_r_report where spec_id = 107 and child = 1 and not p350);    
set @summ13 = (select coalesce(count(id_direction),0) from temp_r_report where p350 and child = 0);    
set @summ14 = (select coalesce(count(id_direction),0) from temp_r_report where p350 and child = 1);    

set @summ0_1 = (select coalesce(SUM(Shots),0) from temp_r_report where p90);    
set @summ1_1 = (select coalesce(SUM(Shots),0) from temp_r_report where (p90 and (p330 or p370 or p380 or id_direction = 32))); 
set @summ2_1 = (select coalesce(SUM(Shots),0) from temp_r_report where (p90 and (p330 or p370 or p380 or id_direction = 32)) and child = 0);  
set @summ3_1 = (select coalesce(SUM(Shots),0) from temp_r_report where (p90 and p380) );  
set @summ4_1 = (select coalesce(SUM(Shots),0) from temp_r_report where (p90 and p370) );  
set @summ5_1 = (select coalesce(SUM(Shots),0) from temp_r_report where p90 and id_direction = 32 and child = 0);  
set @summ6_1 = (select coalesce(SUM(Shots),0) from temp_r_report where p90 and id_direction = 32 and child = 1);  
set @summ7_1 = (select coalesce(SUM(Shots),0) from temp_r_report where (not (p330 or p370 or p380) and not p350) and p90 and id_direction = 2);  
set @summ8_1 = (select coalesce(SUM(Shots),0) from temp_r_report where (not (p330 or p370 or p380) and not p350) and child = 0 and p90 and id_direction = 2);  
set @summ9_1 = (select coalesce(SUM(Shots),0) from temp_r_report where (p390 and p90) );  
set @summ10_1 = (select coalesce(SUM(Shots),0) from temp_r_report where (p370 and p340 and p90));  
set @summ11_1 = (select coalesce(SUM(Shots),0) from temp_r_report where spec_id = 107 and child = 0 and p90 and not p350);  
set @summ12_1 = (select coalesce(SUM(Shots),0) from temp_r_report where spec_id = 107 and child = 1 and p90 and not p350);  
set @summ13_1 = (select coalesce(SUM(Shots),0) from temp_r_report where p350 and child = 0 and p90);  
set @summ14_1 = (select coalesce(SUM(Shots),0) from temp_r_report where p350 and child = 1 and p90);  

set @summ0_2 = (select coalesce(SUM(Shots),0) from temp_r_report where (p110 or p120));    
set @summ1_2 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p110 or p120) and (p330 or p370 or p380 or id_direction = 32)));  
set @summ2_2 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p110 or p120) and (p330 or p370 or p380 or id_direction = 32)) and child = 0);  
set @summ3_2 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p110 or p120) and p380) );  
set @summ4_2 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p110 or p120) and p370) );  
set @summ5_2 = (select coalesce(SUM(Shots),0) from temp_r_report where (p110 or p120) and id_direction = 32 and child = 0);  
set @summ6_2 = (select coalesce(SUM(Shots),0) from temp_r_report where (p110 or p120) and id_direction = 32 and child = 1);  
set @summ7_2 = (select coalesce(SUM(Shots),0) from temp_r_report where (not (p330 or p370 or p380) and not p350) and (p110 or p120) and id_direction = 2);  
set @summ8_2 = (select coalesce(SUM(Shots),0) from temp_r_report where (not (p330 or p370 or p380) and not p350) and child = 0 and (p110 or p120) and id_direction = 2);  
set @summ9_2 = (select coalesce(SUM(Shots),0) from temp_r_report where (p390 and (p110 or p120)) );  
set @summ10_2 = (select coalesce(SUM(Shots),0) from temp_r_report where (p370 and p340 and (p110 or p120)));  
set @summ11_2 = (select coalesce(SUM(Shots),0) from temp_r_report where spec_id = 107 and child = 0 and (p110 or p120) and not p350);  
set @summ12_2 = (select coalesce(SUM(Shots),0) from temp_r_report where spec_id = 107 and child = 1 and (p110 or p120) and not p350);  
set @summ13_2 = (select coalesce(SUM(Shots),0) from temp_r_report where p350 and child = 0 and (p110 or p120));  
set @summ14_2 = (select coalesce(SUM(Shots),0) from temp_r_report where p350 and child = 1 and (p110 or p120));  

set @summ0_3 = (select coalesce(SUM(Shots),0) from temp_r_report where (p130 or p140));    
set @summ1_3 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p130 or p140) and (p330 or p370 or p380 or id_direction = 32)));  
set @summ2_3 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p130 or p140) and (p330 or p370 or p380 or id_direction = 32)) and child = 0);  
set @summ3_3 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p130 or p140) and p380) );  
set @summ4_3 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p130 or p140) and p370) );  
set @summ5_3 = (select coalesce(SUM(Shots),0) from temp_r_report where (p130 or p140) and id_direction = 32 and child = 0);  
set @summ6_3 = (select coalesce(SUM(Shots),0) from temp_r_report where (p130 or p140) and id_direction = 32 and child = 1);  
set @summ7_3 = (select coalesce(SUM(Shots),0) from temp_r_report where (not (p330 or p370 or p380) and not p350) and (p130 or p140) and id_direction = 2);  
set @summ8_3 = (select coalesce(SUM(Shots),0) from temp_r_report where (not (p330 or p370 or p380) and not p350) and child = 0 and (p130 or p140) and id_direction = 2);  
set @summ9_3 = (select coalesce(SUM(Shots),0) from temp_r_report where (p390 and (p130 or p140)) );  
set @summ10_3 = (select coalesce(SUM(Shots),0) from temp_r_report where (p370 and p340 and (p130 or p140)));  
set @summ11_3 = (select coalesce(SUM(Shots),0) from temp_r_report where spec_id = 107 and child = 0 and (p130 or p140) and not p350);  
set @summ12_3 = (select coalesce(SUM(Shots),0) from temp_r_report where spec_id = 107 and child = 1 and (p130 or p140) and not p350);  
set @summ13_3 = (select coalesce(SUM(Shots),0) from temp_r_report where p350 and child = 0 and (p130 or p140));  
set @summ14_3 = (select coalesce(SUM(Shots),0) from temp_r_report where p350 and child = 1 and (p130 or p140));  

set @summ0_4 = (select coalesce(SUM(Shots),0) from temp_r_report where (p150 or p161 or p170 or p180 or p190));    
set @summ1_4 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p150 or p161 or p170 or p180 or p190) and (p330 or p370 or p380 or id_direction = 32)));  
set @summ2_4 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p150 or p161 or p170 or p180 or p190) and (p330 or p370 or p380 or id_direction = 32)) and child = 0);  
set @summ3_4 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p150 or p161 or p170 or p180 or p190) and p380) ); 
set @summ4_4 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p150 or p161 or p170 or p180 or p190) and p370) );  
set @summ5_4 = (select coalesce(SUM(Shots),0) from temp_r_report where (p150 or p161 or p170 or p180 or p190) and id_direction = 32 and child = 0);  
set @summ6_4 = (select coalesce(SUM(Shots),0) from temp_r_report where (p150 or p161 or p170 or p180 or p190) and id_direction = 32 and child = 1);  
set @summ7_4 = (select coalesce(SUM(Shots),0) from temp_r_report where (not (p330 or p370 or p380) and not p350) and (p150 or p161 or p170 or p180 or p190) and id_direction = 2);  
set @summ8_4 = (select coalesce(SUM(Shots),0) from temp_r_report where (not (p330 or p370 or p380) and not p350) and child = 0 and (p150 or p161 or p170 or p180 or p190) and id_direction = 2);  
set @summ9_4 = (select coalesce(SUM(Shots),0) from temp_r_report where (p390 and (p150 or p161 or p170 or p180 or p190)) );  
set @summ10_4 = (select coalesce(SUM(Shots),0) from temp_r_report where (p370 and p340 and (p150 or p161 or p170 or p180 or p190)));  
set @summ11_4 = (select coalesce(SUM(Shots),0) from temp_r_report where spec_id = 107 and child = 0 and (p150 or p161 or p170 or p180 or p190) and not p350);  
set @summ12_4 = (select coalesce(SUM(Shots),0) from temp_r_report where spec_id = 107 and child = 1 and (p150 or p161 or p170 or p180 or p190) and not p350);  
set @summ13_4 = (select coalesce(SUM(Shots),0) from temp_r_report where p350 and child = 0 and (p150 or p161 or p170 or p180 or p190));  
set @summ14_4 = (select coalesce(SUM(Shots),0) from temp_r_report where p350 and child = 1 and (p150 or p161 or p170 or p180 or p190));  

set @summ0_5 = (select coalesce(SUM(Shots),0) from temp_r_report where p100);   
set @summ1_5 = (select coalesce(SUM(Shots),0) from temp_r_report where (p100 and (p330 or p370 or p380 or id_direction = 32)));  
set @summ2_5 = (select coalesce(SUM(Shots),0) from temp_r_report where (p100 and (p330 or p370 or p380 or id_direction = 32)) and child = 0);  
set @summ3_5 = (select coalesce(SUM(Shots),0) from temp_r_report where (p100 and p380) );  
set @summ4_5 = (select coalesce(SUM(Shots),0) from temp_r_report where (p100 and p370) );  
set @summ5_5 = (select coalesce(SUM(Shots),0) from temp_r_report where p100 and id_direction = 32 and child = 0);  
set @summ6_5 = (select coalesce(SUM(Shots),0) from temp_r_report where p100 and id_direction = 32 and child = 1) ;  
set @summ7_5 = (select coalesce(SUM(Shots),0) from temp_r_report where (not (p330 or p370 or p380) and not p350) and p100 and id_direction = 2);  
set @summ8_5 = (select coalesce(SUM(Shots),0) from temp_r_report where (not (p330 or p370 or p380) and not p350) and child = 0 and p100 and id_direction = 2);  
set @summ9_5 = (select coalesce(SUM(Shots),0) from temp_r_report where (p390 and p100) );  
set @summ10_5 = (select coalesce(SUM(Shots),0) from temp_r_report where (p370 and p340 and p100));  
set @summ11_5 = (select coalesce(SUM(Shots),0) from temp_r_report where spec_id = 107 and child = 0 and p100 and not p350);  
set @summ12_5 = (select coalesce(SUM(Shots),0) from temp_r_report where spec_id = 107 and child = 1 and p100 and not p350);  
set @summ13_5 = (select coalesce(SUM(Shots),0) from temp_r_report where p350 and child = 0 and p100);  
set @summ14_5 = (select coalesce(SUM(Shots),0) from temp_r_report where p350 and child = 1 and p100);  

set @summ0_6 = (select coalesce(count(id_direction),0) from temp_r_report where (p160 or p162 or p200));   
set @summ1_6 = (select coalesce(count(id_direction),0) from temp_r_report where ((p160 or p162 or p200) and (p330 or p370 or p380 or id_direction = 32)));  
set @summ2_6 = (select coalesce(count(id_direction),0) from temp_r_report where ((p160 or p162 or p200) and (p330 or p370 or p380 or id_direction = 32)) and child = 0);  
set @summ3_6 = (select coalesce(count(id_direction),0) from temp_r_report where ((p160 or p162 or p200) and p380) );  
set @summ4_6 = (select coalesce(count(id_direction),0) from temp_r_report where ((p160 or p162 or p200) and p370) );  
set @summ5_6 = (select coalesce(count(id_direction),0) from temp_r_report where (p160 or p162 or p200) and id_direction = 32 and child = 0);  
set @summ6_6 = (select coalesce(count(id_direction),0) from temp_r_report where (p160 or p162 or p200) and id_direction = 32 and child = 1) ;  
set @summ7_6 = (select coalesce(count(id_direction),0) from temp_r_report where (not (p330 or p370 or p380) and not p350) and (p160 or p162 or p200) and id_direction = 2);  
set @summ8_6 = (select coalesce(count(id_direction),0) from temp_r_report where (not (p330 or p370 or p380) and not p350) and child = 0 and (p160 or p162 or p200) and id_direction = 2);  
set @summ9_6 = (select coalesce(count(id_direction),0) from temp_r_report where (p390 and (p160 or p162 or p200)) );  
set @summ10_6 = (select coalesce(count(id_direction),0) from temp_r_report where (p370 and p340 and (p160 or p162 or p200)));  
set @summ11_6 = (select coalesce(count(id_direction),0) from temp_r_report where spec_id = 107 and child = 0 and (p160 or p162 or p200) and not p350);  
set @summ12_6 = (select coalesce(count(id_direction),0) from temp_r_report where spec_id = 107 and child = 1 and (p160 or p162 or p200) and not p350);  
set @summ13_6 = (select coalesce(count(id_direction),0) from temp_r_report where p350 and child = 0 and (p160 or p162 or p200));  
set @summ14_6 = (select coalesce(count(id_direction),0) from temp_r_report where p350 and child = 1 and (p160 or p162 or p200));  

set @summ0_60 = (select coalesce(SUM(Shots),0) from temp_r_report where (p560 or p570 or p580));   
set @summ1_60 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p560 or p570 or p580) and (p330 or p370 or p380 or id_direction = 32)));  
set @summ2_60 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p560 or p570 or p580) and (p330 or p370 or p380 or id_direction = 32)) and child = 0);  
set @summ3_60 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p560 or p570 or p580) and p380) );  
set @summ4_60 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p560 or p570 or p580) and p370) );  
set @summ5_60 = (select coalesce(SUM(Shots),0) from temp_r_report where (p560 or p570 or p580) and id_direction = 32 and child = 0);  
set @summ6_60 = (select coalesce(SUM(Shots),0) from temp_r_report where (p560 or p570 or p580) and id_direction = 32 and child = 1) ;  
set @summ7_60 = (select coalesce(SUM(Shots),0) from temp_r_report where (not (p330 or p370 or p380) and not p350) and (p560 or p570 or p580) and id_direction = 2);  
set @summ8_60 = (select coalesce(SUM(Shots),0) from temp_r_report where (not (p330 or p370 or p380) and not p350) and child = 0 and (p560 or p570 or p580) and id_direction = 2);  
set @summ9_60 = (select coalesce(SUM(Shots),0) from temp_r_report where (p390 and (p560 or p570 or p580)) );  
set @summ10_60 = (select coalesce(SUM(Shots),0) from temp_r_report where (p370 and p340 and (p560 or p570 or p580)));  
set @summ11_60 = (select coalesce(SUM(Shots),0) from temp_r_report where spec_id = 107 and child = 0 and (p560 or p570 or p580) and not p350);  
set @summ12_60 = (select coalesce(SUM(Shots),0) from temp_r_report where spec_id = 107 and child = 1 and (p560 or p570 or p580) and not p350);  
set @summ13_60 = (select coalesce(SUM(Shots),0) from temp_r_report where p350 and child = 0 and (p560 or p570 or p580));  
set @summ14_60 = (select coalesce(SUM(Shots),0) from temp_r_report where p350 and child = 1 and (p560 or p570 or p580));  

set @summ0_7 = (select coalesce(count(id_direction),0) from temp_r_report where (p250 or p260));    
set @summ1_7 = (select coalesce(count(id_direction),0) from temp_r_report where ((p250 or p260) and (p330 or p370 or p380 or id_direction = 32)));  
set @summ2_7 = (select coalesce(count(id_direction),0) from temp_r_report where ((p250 or p260) and (p330 or p370 or p380 or id_direction = 32)) and child = 0);  
set @summ3_7 = (select coalesce(count(id_direction),0) from temp_r_report where ((p250 or p260) and p380) );  
set @summ4_7 = (select coalesce(count(id_direction),0) from temp_r_report where ((p250 or p260) and p370) );  
set @summ5_7 = (select coalesce(count(id_direction),0) from temp_r_report where (p250 or p260) and id_direction = 32 and child = 0);  
set @summ6_7 = (select coalesce(count(id_direction),0) from temp_r_report where (p250 or p260) and id_direction = 32 and child = 1) ;  
set @summ7_7 = (select coalesce(count(id_direction),0) from temp_r_report where (not (p330 or p370 or p380) and not p350) and (p250 or p260) and id_direction = 2);  
set @summ8_7 = (select coalesce(count(id_direction),0) from temp_r_report where (not (p330 or p370 or p380) and not p350) and child = 0 and (p250 or p260) and id_direction = 2);  
set @summ9_7 = (select coalesce(count(id_direction),0) from temp_r_report where (p390 and (p250 or p260)) );  
set @summ10_7 = (select coalesce(count(id_direction),0) from temp_r_report where (p370 and p340 and (p250 or p260)));  
set @summ11_7 = (select coalesce(count(id_direction),0) from temp_r_report where spec_id = 107 and child = 0 and (p250 or p260) and not p350);  
set @summ12_7 = (select coalesce(count(id_direction),0) from temp_r_report where spec_id = 107 and child = 1 and (p250 or p260) and not p350);  
set @summ13_7 = (select coalesce(count(id_direction),0) from temp_r_report where p350 and child = 0 and (p250 or p260));  
set @summ14_7 = (select coalesce(count(id_direction),0) from temp_r_report where p350 and child = 1 and (p250 or p260));  

/* 21.02.19 Sysoev. Fixing KLKT+TRG problem */ 
create temporary table if not exists temp_r_report_1 
select * from temp_r_report;

set @summ0_8 = (select coalesce(SUM(Shots),0) from temp_r_report where (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241 or p840 or p850) and not p250 and not p260) + 
                (select coalesce(count(id_direction),0) from temp_r_report_1 where (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241 or p840 or p850) and (p250 or p260));    
                
set @summ1_8 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241 or p840 or p850) and not p250 and not p260 and (p330 or p370 or p380 or id_direction = 32))) + 
                (select coalesce(count(id_direction),0) from temp_r_report_1 where ((p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241 or p840 or p850) and (p250 or p260) and (p330 or p370 or p380 or id_direction = 32)));  
                
set @summ2_8 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241 or p840 or p850) and not p250 and not p260 and (p330 or p370 or p380 or id_direction = 32)) and child = 0) + 
                (select coalesce(count(id_direction),0) from temp_r_report_1 where ((p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241 or p840 or p850) and (p250 or p260) and (p330 or p370 or p380 or id_direction = 32)) and child = 0);  
                
set @summ3_8 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241 or p840 or p850) and p380) and not p250 and not p260) + 
                (select coalesce(count(id_direction),0) from temp_r_report_1 where ((p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241 or p840 or p850) and p380) and (p250 or p260));  
                
set @summ4_8 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241 or p840 or p850) and p370) and not p250 and not p260) + 
                (select coalesce(count(id_direction),0) from temp_r_report_1 where ((p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241 or p840 or p850) and p370) and (p250 or p260));  
                
set @summ5_8 = (select coalesce(SUM(Shots),0) from temp_r_report where (p840 or p850) and id_direction = 32 and child = 0 and not p250 and not p260) + 
                (select coalesce(count(id_direction),0) from temp_r_report_1 where (p840 or p850) and id_direction = 32 and child = 0 and (p250 or p260));  
                
set @summ6_8 = (select coalesce(SUM(Shots),0) from temp_r_report where (p840 or p850) and id_direction = 32 and child = 1 and not p250 and not p260) + 
                (select coalesce(count(id_direction),0) from temp_r_report_1 where (p840 or p850) and id_direction = 32 and child = 1 and (p250 or p260));  
                
set @summ7_8 = (select coalesce(SUM(Shots),0) from temp_r_report where (not (p330 or p370 or p380) and not p350) and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241) and id_direction = 2 and not p250 and not p260) + 
                (select coalesce(count(id_direction),0) from temp_r_report_1 where (not (p330 or p370 or p380) and not p350) and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241) and id_direction = 2 and (p250 or p260));  
                
set @summ8_8 = (select coalesce(SUM(Shots),0) from temp_r_report where (not (p330 or p370 or p380) and not p350) and child = 0 and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241) and id_direction = 2 and not p250 and not p260) + 
                (select coalesce(count(id_direction),0) from temp_r_report_1 where (not (p330 or p370 or p380) and not p350) and child = 0 and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241) and id_direction = 2 and (p250 or p260));  
                
set @summ9_8 = (select coalesce(SUM(Shots),0) from temp_r_report where (p390 and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241)) and not p250 and not p260) + 
                (select coalesce(count(id_direction),0) from temp_r_report_1 where (p390 and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241)) and (p250 or p260));  
                
set @summ10_8 = (select coalesce(SUM(Shots),0) from temp_r_report where (p370 and p340 and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241)) and not p250 and not p260) + 
                (select coalesce(count(id_direction),0) from temp_r_report_1 where (p370 and p340 and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241)) and (p250 or p260));  
                
set @summ11_8 = (select coalesce(SUM(Shots),0) from temp_r_report where spec_id = 107 and child = 0 and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241) and not p350 and not p250 and not p260) + 
                 (select coalesce(count(id_direction),0) from temp_r_report_1 where spec_id = 107 and child = 0 and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241) and not p350 and (p250 or p260));  
set @summ12_8 = (select coalesce(SUM(Shots),0) from temp_r_report where spec_id = 107 and child = 1 and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241) and not p350 and not p250 and not p260) + 
                 (select coalesce(count(id_direction),0) from temp_r_report_1 where spec_id = 107 and child = 1 and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241) and not p350 and (p250 or p260));  
set @summ13_8 = (select coalesce(SUM(Shots),0) from temp_r_report where p350 and child = 0 and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241) and not p250 and not p260) + 
                 (select coalesce(count(id_direction),0) from temp_r_report_1 where p350 and child = 0 and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241) and (p250 or p260));  
set @summ14_8 = (select coalesce(SUM(Shots),0) from temp_r_report where p350 and child = 1 and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241) and not p250 and not p260) + 
                 (select coalesce(count(id_direction),0) from temp_r_report_1 where p350 and child = 1 and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241) and (p250 or p260));  

/* 21.02.19 Sysoev. Old version with KLKT+TRG problem
set @summ0_8 = (select coalesce(SUM(Shots),0) from temp_r_report where (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241 or p840 or p850));    
set @summ1_8 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241 or p840 or p850) and (p330 or p370 or p380 or id_direction = 32)));  
set @summ2_8 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241 or p840 or p850) and (p330 or p370 or p380 or id_direction = 32)) and child = 0);  
set @summ3_8 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241 or p840 or p850) and p380) );  
set @summ4_8 = (select coalesce(SUM(Shots),0) from temp_r_report where ((p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241 or p840 or p850) and p370) );  
set @summ5_8 = (select coalesce(SUM(Shots),0) from temp_r_report where (p840 or p850) and id_direction = 32 and child = 0);  
set @summ6_8 = (select coalesce(SUM(Shots),0) from temp_r_report where (p840 or p850) and id_direction = 32 and child = 1) ;  
set @summ7_8 = (select coalesce(SUM(Shots),0) from temp_r_report where (not (p330 or p370 or p380) and not p350) and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241) and id_direction = 2);  
set @summ8_8 = (select coalesce(SUM(Shots),0) from temp_r_report where (not (p330 or p370 or p380) and not p350) and child = 0 and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241) and id_direction = 2);  
set @summ9_8 = (select coalesce(SUM(Shots),0) from temp_r_report where (p390 and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241)) );  
set @summ10_8 = (select coalesce(SUM(Shots),0) from temp_r_report where (p370 and p340 and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241)));  
set @summ11_8 = (select coalesce(SUM(Shots),0) from temp_r_report where spec_id = 107 and child = 0 and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241) and not p350);  
set @summ12_8 = (select coalesce(SUM(Shots),0) from temp_r_report where spec_id = 107 and child = 1 and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241) and not p350);  
set @summ13_8 = (select coalesce(SUM(Shots),0) from temp_r_report where p350 and child = 0 and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241));  
set @summ14_8 = (select coalesce(SUM(Shots),0) from temp_r_report where p350 and child = 1 and (p205 or p207 or p210 or p220 or p224 or p227 or p230 or p240 or p241));  
*/ 

set @total_summ0 = @summ0_1 + @summ0_2 + @summ0_3 + @summ0_4 + @summ0_5 + @summ0_6 + @summ0_60 + @summ0_7 + @summ0_8;    
set @total_summ1 = @summ1_1 + @summ1_2 + @summ1_3 + @summ1_4 + @summ1_5 + @summ1_6 + @summ1_60 + @summ1_7 + @summ1_8;    
set @total_summ2 = @summ2_1 + @summ2_2 + @summ2_3 + @summ2_4 + @summ2_5 + @summ2_6 + @summ2_60 + @summ2_7 + @summ2_8;    
set @total_summ3 = @summ3_1 + @summ3_2 + @summ3_3 + @summ3_4 + @summ3_5 + @summ3_6 + @summ3_60 + @summ3_7 + @summ3_8;    
set @total_summ4 = @summ4_1 + @summ4_2 + @summ4_3 + @summ4_4 + @summ4_5 + @summ4_6 + @summ4_60 + @summ4_7 + @summ4_8;    
set @total_summ5 = @summ5_1 + @summ5_2 + @summ5_3 + @summ5_4 + @summ5_5 + @summ5_6 + @summ5_60 + @summ5_7 + @summ5_8;    
set @total_summ6 = @summ6_1 + @summ6_2 + @summ6_3 + @summ6_4 + @summ6_5 + @summ6_6 + @summ6_60 + @summ6_7 + @summ6_8;    
set @total_summ7 = @summ7_1 + @summ7_2 + @summ7_3 + @summ7_4 + @summ7_5 + @summ7_6 + @summ7_60 + @summ7_7 + @summ7_8;    
set @total_summ8 = @summ8_1 + @summ8_2 + @summ8_3 + @summ8_4 + @summ8_5 + @summ8_6 + @summ8_60 + @summ8_7 + @summ8_8;    
set @total_summ9 = @summ9_1 + @summ9_2 + @summ9_3 + @summ9_4 + @summ9_5 + @summ9_6 + @summ9_60 + @summ9_7 + @summ9_8;    
set @total_summ10 = @summ10_1 + @summ10_2 + @summ10_3 + @summ10_4 + @summ10_5 + @summ10_6 + @summ10_60 + @summ10_7 + @summ10_8;    
set @total_summ11 = @summ11_1 + @summ11_2 + @summ11_3 + @summ11_4 + @summ11_5 + @summ11_6 + @summ11_60 + @summ11_7 + @summ11_8;    
set @total_summ12 = @summ12_1 + @summ12_2 + @summ12_3 + @summ12_4 + @summ12_5 + @summ12_6 + @summ12_60 + @summ12_7 + @summ12_8;    
set @total_summ13 = @summ13_1 + @summ13_2 + @summ13_3 + @summ13_4 + @summ13_5 + @summ13_6 + @summ13_60 + @summ13_7 + @summ13_8;    
set @total_summ14 = @summ14_1 + @summ14_2 + @summ14_3 + @summ14_4 + @summ14_5 + @summ14_6 + @summ14_60 + @summ14_7 + @summ14_8;    

set @uet_children = (select sum(coalesce(T.uet,0))
    from case_services cs 
    left join cases c on c.id_case = cs.id_case 
    left join patient_data pd on pd.id_patient = c.id_patient 
    left join doctor_spec ds on ds.doctor_id = cs.id_doctor 
	left join vmu_profile vp on vp.id_profile = cs.id_profile 
    left join  
	(select vt.uet, vt.id_profile, vt.id_zone_type,   
			vt.tariff_begin_date as d1, vt.tariff_end_date as d2,   
			vtp.tp_begin_date as d3, vtp.tp_end_date as d4    
        from vmu_tariff vt, vmu_tariff_plan vtp   
        where vtp.id_tariff_group = vt.id_lpu And vtp.id_lpu in (select id_lpu from mu_ident) ) T on  
							T.id_profile = vp.id_profile and cs.date_begin between T.d1 and T.d2   
							and cs.date_begin between T.d3 and T.d4   
							and T.id_zone_type = cs.id_net_profile and cs.is_oms = 1   
    where (cs.id_doctor = doc_id or doc_id = 0) and cs.date_begin between date_start and date_end 
		and ((ds.spec_id = 208 and not doc) or doc) and ((ds.spec_id = 60 and doc) or not doc) and cs.is_oms = 1 
		and DATE_ADD(pd.birthday, INTERVAL 18 year) > cs.date_begin 
    );
set @uet_adult = (select sum(coalesce(T.uet,0))
    from case_services cs 
    left join cases c on c.id_case = cs.id_case 
    left join patient_data pd on pd.id_patient = c.id_patient 
    left join doctor_spec ds on ds.doctor_id = cs.id_doctor 
	left join vmu_profile vp on vp.id_profile = cs.id_profile 
    left join  
	(select vt.uet, vt.id_profile, vt.id_zone_type,   
			vt.tariff_begin_date as d1, vt.tariff_end_date as d2,   
			vtp.tp_begin_date as d3, vtp.tp_end_date as d4    
        from vmu_tariff vt, vmu_tariff_plan vtp   
        where vtp.id_tariff_group = vt.id_lpu And vtp.id_lpu in (select id_lpu from mu_ident) ) T on  
							T.id_profile = vp.id_profile and cs.date_begin between T.d1 and T.d2   
							and cs.date_begin between T.d3 and T.d4   
							and T.id_zone_type = cs.id_net_profile and cs.is_oms = 1   
    where (cs.id_doctor = doc_id or doc_id = 0) and cs.date_begin between date_start and date_end 
		and ((ds.spec_id = 208 and not doc) or doc) and ((ds.spec_id = 60 and doc) or not doc) and cs.is_oms = 1 
		and DATE_ADD(pd.birthday, INTERVAL 18 year) <= cs.date_begin 
    );    

set @first_col = case doc when 1 then '2.Описано исследований' else '2.Снимки' end; 

select '1.Пациенты', 'Принято пациентов', @summ2, @summ1 - @summ2, @summ1, @summ3, @summ4, @summ5, @summ6, 
                                @summ8, @summ7 - @summ8, @summ7, @summ9, @summ10, @summ11, @summ12, @summ13, @summ14, @summ0  
UNION select @first_col, 'Дентальные 3x4 плёночные', @summ2_1, @summ1_1 - @summ2_1, @summ1_1, @summ3_1, @summ4_1, @summ5_1, @summ6_1, 
                                @summ8_1, @summ7_1 - @summ8_1, @summ7_1, @summ9_1, @summ10_1, @summ11_1, @summ12_1, @summ13_1, @summ14_1, @summ0_1  
UNION select @first_col, 'Внеротовые 13x18 плёночные', @summ2_2, @summ1_2 - @summ2_2, @summ1_2, @summ3_2, @summ4_2, @summ5_2, @summ6_2,  
                                @summ8_2, @summ7_2 - @summ8_2, @summ7_2, @summ9_2, @summ10_2, @summ11_2, @summ12_2, @summ13_2, @summ14_2, @summ0_2  
UNION select @first_col, '"Вприкус" плёночные', @summ2_3, @summ1_3 - @summ2_3, @summ1_3, @summ3_3, @summ4_3, @summ5_3, @summ6_3,  
                                @summ8_3, @summ7_3 - @summ8_3, @summ7_3, @summ9_3, @summ10_3, @summ11_3, @summ12_3, @summ13_3, @summ14_3, @summ0_3  
UNION select @first_col, 'ОПТГ плёночные', @summ2_4, @summ1_4 - @summ2_4, @summ1_4, @summ3_4, @summ4_4, @summ5_4, @summ6_4,  
                                @summ8_4, @summ7_4 - @summ8_4, @summ7_4, @summ9_4, @summ10_4, @summ11_4, @summ12_4, @summ13_4, @summ14_4, @summ0_4 
UNION select @first_col, 'Дентальные 3x4 цифровые', @summ2_5, @summ1_5 - @summ2_5, @summ1_5, @summ3_5, @summ4_5, @summ5_5, @summ6_5,  
                                @summ8_5, @summ7_5 - @summ8_5, @summ7_5, @summ9_5, @summ10_5, @summ11_5, @summ12_5, @summ13_5, @summ14_5, @summ0_5 
UNION select @first_col, 'ОПТГ цифровые', @summ2_6, @summ1_6 - @summ2_6, @summ1_6, @summ3_6, @summ4_6, @summ5_6, @summ6_6,  
                                @summ8_6, @summ7_6 - @summ8_6, @summ7_6, @summ9_6, @summ10_6, @summ11_6, @summ12_6, @summ13_6, @summ14_6, @summ0_6 
UNION select @first_col, 'Фрагм. ОПТГ цифровые', @summ2_60, @summ1_60 - @summ2_60, @summ1_60, @summ3_60, @summ4_60, @summ5_60, @summ6_60,  
                                @summ8_60, @summ7_60 - @summ8_60, @summ7_60, @summ9_60, @summ10_60, @summ11_60, @summ12_60, @summ13_60, @summ14_60, @summ0_60 
UNION select @first_col, 'ТРГ (телерентгенография)', @summ2_7, @summ1_7 - @summ2_7, @summ1_7, @summ3_7, @summ4_7, @summ5_7, @summ6_7,  
                                @summ8_7, @summ7_7 - @summ8_7, @summ7_7, @summ9_7, @summ10_7, @summ11_7, @summ12_7, @summ13_7, @summ14_7, @summ0_7 
UNION select @first_col, 'КЛКТ (компьютерная томография)', @summ2_8, @summ1_8 - @summ2_8, @summ1_8, @summ3_8, @summ4_8, @summ5_8, @summ6_8,  
                                @summ8_8, @summ7_8 - @summ8_8, @summ7_8, @summ9_8, @summ10_8, @summ11_8, @summ12_8, @summ13_8, @summ14_8, @summ0_8 
UNION select @first_col, 'ИТОГО:', @total_summ2, @total_summ1 - @total_summ2, @total_summ1, @total_summ3, @total_summ4, @total_summ5, @total_summ6,  
                                @total_summ8, @total_summ7 - @total_summ8, @total_summ7, @total_summ9, @total_summ10, @total_summ11, @total_summ12, @total_summ13, @total_summ14, @total_summ0 
UNION select '3.УЕТ', '', ROUND(COALESCE(@uet_adult,0), 2), ROUND(COALESCE(@uet_children,0),2), ROUND(COALESCE(@uet_adult,0),2) + ROUND(COALESCE(@uet_children,0),2), '', '', '', '', '', '', '', '', '', '', '', '', '', '' ;

END;

create definer = test_user@`%` procedure zn_tech_st8(IN date_start date, IN date_end_ date, IN closed int, IN pu int, IN techn_id int)
begin
    set @rn = 0;
select  @rn := @rn + 1, T.*
from
(select t.name as tech_name, ord.order_number, ord.orderCreateDate, ord.orderCompletionDate,
        case when p.group_ids like '%1%' then 'Починка'
             when p.group_ids like '%2%' then 'Имплантология'
             when p.group_ids like '%3%' then 'Сн. и цем. коронок'
             when pos.ID_GROUP like '%13%' then 'Починка'
             when pos.ID_GROUP like '%38%' then 'Сн. и цем. коронок'
             else 'Протезирование' end as group_name,
        sum(cs.service_cost) as total,
        u.name as doc_name
from case_services cs
left join orders ord on ord.id = cs.id_order
left join doctor_spec ds on ds.doctor_id = ord.id_doctor
left join users u on u.id = ds.user_id
left join technicians t on t.id = ord.id_technician
left join price p on p.id = cs.id_profile
left join price_orto_soc pos on pos.id = cs.id_profile
left join patient_data pd on pd.id_human = ord.id_human and pd.is_active = 1 and pd.date_end = '2200-01-01'
left join patient_address pa on pa.id_address = pd.id_address_reg and pd.id_address_reg <> 1
where ((ord.orderCompletionDate between date_start and date_end_ and ord.status = 2) * closed
    or (ord.orderCreateDate between date_start and date_end_ and ord.status < 2 and ord.orderCompletionDate is null) * (not closed))
  and (ord.id_technician = techn_id or techn_id = 0) and t.id <> 1
  and ((p.in_tech = 1 and pu in (0,2)) or (pos.in_tech = 1 and pu = 1))
  and ((ord.order_type = 0 and pu = 0) or (ord.order_type = 1 and pu = 1) or (ord.order_type = 2 and pu = 2))
 group by order_number
order by tech_name) T
    ;
end;

