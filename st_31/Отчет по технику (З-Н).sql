set @date_start = '2019-01-07';
set @date_end = '2019-07-07';
drop temporary table if  exists pzp;
create temporary table if not exists pzp (SELECT case
                                                     when ord.order_type = 1 then 'БЗП'
                                                         when ord.order_type = 0 then 'ПУ' end type,
                                                         short_name(tech.name) tech_name, short_name(concat_ws(' ', pd.SURNAME, pd.NAME, pd.SECOND_NAME)) pat,
                                                 ord.Order_Number, ord.id, ord.orderCreateDate, ord.orderCompletionDate,
                                                 case
                                                     when ord.order_type = 1
                                                         then SUM(cs.service_cost * cs.discount) end as bzp_sum,
                                                 case
                                                     when ord.order_type = 0
                                                         then SUM(cs.service_cost * cs.discount) end as pzp_sum,
                                                 short_name(u.name) as doc_name,
                                                 case
                                                     when ord.order_type = 1
                                                         then SUM(pos.UET) end as bzp_uet,
                                                 case
                                                     when ord.order_type = 0
                                                         then sum(p.UET) end as pzp_uet
                                          FROM case_services AS cs
                                                   left join orders ord on cs.id_order = ord.id
                                                   left join price p on p.id = cs.id_profile and ord.order_type = 0
                                                   left join price_orto_soc pos on cs.ID_PROFILE = pos.ID and ord.order_type = 1
                                                   left join doctor_spec ds on ds.doctor_id = ord.id_doctor
                                                   left join users u on u.id = ds.user_id
                                              left join  patient_data pd on ord.Id_Human = pd.ID_HUMAN and  pd.IS_ACTIVE = 1 and pd.DATE_END = '2200-01-01'
                                              left join technicians tech on ord.Id_technician = tech.ID
                                          WHERE cs.id_order >= 0 and  pd.IS_ACTIVE = 1 and pd.DATE_END = '2200-01-01'
                                            AND ord.status = 2
                                            and ord.orderCompletionDate between @date_start AND @date_end
                                          GROUP BY tech.ID, ord.id
                                          order by tech_name);
set @row = 0;
select @row := @row + 1, T.*
from (select case tech_name when '.Без .. т.' then 'б/т' else  tech_name end t_name, type, pat, Order_Number, date_format(orderCreateDate, '%d-%m-%Y'), date_format(orderCompletionDate, '%d-%m-%Y'),
             round(coalesce(bzp_sum, 0), 2), round(coalesce(pzp_sum, 0), 0),
             doc_name,
              round(coalesce(bzp_uet, 0), 2),
             round(coalesce(pzp_uet, 0), 2)
      from pzp
    group by t_name, type, pat, Order_Number
    order by t_name, type, pat) T;