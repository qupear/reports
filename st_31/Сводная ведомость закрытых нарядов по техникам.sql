set @date_start = '2019-01-07';
set @date_end = '2019-07-07';
drop temporary table if  exists pzp;
create temporary table if not exists pzp (SELECT short_name(tech.name) tech_name, ord.id,
                                                 case
                                                     when ord.order_type = 1
                                                         then SUM(cs.service_cost * cs.discount) end as bzp_sum,
                                                 case
                                                     when ord.order_type = 0
                                                         then SUM(cs.service_cost * cs.discount) end as pzp_sum,
                                                 case
                                                     when ord.order_type = 1
                                                         then SUM(pos.UET) end as bzp_uet,
                                                 case
                                                     when ord.order_type = 0
                                                         then sum(p.UET) end as pzp_uet
                                          FROM case_services AS cs
                                                   left join orders ord on cs.id_order = ord.id
                                                   left join price p on p.id = cs.id_profile and ord.order_type = 0
                                                   left join price_orto_soc pos on cs.ID_PROFILE = pos.ID and ord.order_type = 1
                                                   left join doctor_spec ds on ds.doctor_id = ord.id_doctor
                                                   left join users u on u.id = ds.user_id
                                              left join  patient_data pd on ord.Id_Human = pd.ID_HUMAN and  pd.IS_ACTIVE = 1 and pd.DATE_END = '2200-01-01'
                                              left join technicians tech on ord.Id_technician = tech.ID
                                          WHERE cs.id_order >= 0 and  pd.IS_ACTIVE = 1 and pd.DATE_END = '2200-01-01'
                                            AND ord.status = 2
                                            and ord.orderCompletionDate between @date_start AND @date_end

                                          GROUP BY tech.id, ord.id
                                          order by tech_name);
set @row = 0;
select @row := @row + 1, T.*
from (select case tech_name when '.Без .. т.' then 'б/т' else  tech_name end t_name,
             round(coalesce(sum(bzp_sum), 0), 2), round(coalesce(sum(pzp_sum), 0), 0),
             round(coalesce(sum(bzp_sum), 0), 2) + round(coalesce(sum(pzp_sum), 0), 0),
              round(coalesce(sum(bzp_uet), 0), 2),
             round(coalesce(sum(pzp_uet), 0), 2), round(coalesce(sum(bzp_uet), 0), 2) + round(coalesce(sum(pzp_uet), 0), 2)
      from pzp
    group by tech_name
    ) T;