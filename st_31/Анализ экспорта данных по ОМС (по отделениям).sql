SELECT
  dep.name AS dt,
  u.name AS doc,
  COUNT(DISTINCT c.date_begin) AS db, ap.app,
  COUNT(DISTINCT c.id_case) AS op,
  COUNT(DISTINCT c2.id_case) AS cl,
  COUNT(DISTINCT c1.id_case) AS exp,
  ROUND((COUNT(DISTINCT c2.id_case) / COUNT(DISTINCT c.date_begin)), 1) AS sred
FROM cases c
  JOIN case_services cs
    ON c.id_case = cs.id_case
    AND cs.is_oms = 1
  LEFT JOIN doctor_spec ds
    ON ds.doctor_id = c.ID_DOCTOR
  LEFT JOIN users u
    ON u.id = ds.user_id
  LEFT JOIN departments dep
    ON dep.id = ds.department_id
  LEFT JOIN cases c1
    ON c1.id_case = c.id_case
    AND c1.exported = 1
  LEFT JOIN cases c2
    ON c2.id_case = c.id_case
    AND c2.date_end IS NOT NULL
left join (select count(id) as app, doctor_id from appointments
where date(time) between @date_start and @date_end and case_id > 0
group by doctor_id) ap on ap.doctor_id = ds.doctor_id
WHERE c.DATE_BEGIN BETWEEN @date_start AND @date_end
GROUP BY dt,
         doc
ORDER BY dt, doc;