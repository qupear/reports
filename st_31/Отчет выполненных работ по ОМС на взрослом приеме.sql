set @date_start = '2018-08-20';
set @date_end = '2018-08-20';
set @doc_id = 0;
set @depart_id = 14;
set @code = null;

select d.name as dept_name, 
	u.name as doc_name, 
	vp.PROFILE_INFIS_CODE as codes,
	vp.profile_name,
	count(vp.PROFILE_INFIS_CODE) as code_count,
	T.uet,
	round((T.uet * count(vp.PROFILE_INFIS_CODE)), 2),
	T.price,
	round((T.price * count(vp.PROFILE_INFIS_CODE)), 2)
from
    case_services cs
	left join doctor_spec ds ON ds.doctor_id = cs.id_doctor
	left join users u ON u.id = ds.user_id
	left join departments d ON d.id = ds.department_id
	left join vmu_profile vp on cs.ID_PROFILE = vp.ID_PROFILE and cs.is_oms = 1
	left join (select vt.price, vt.uet, vt.id_profile, vt.id_zone_type,
                                vt.tariff_begin_date as d1,
                                vt.tariff_end_date as d2,
                                vtp.tp_begin_date as d3,
                                vtp.tp_end_date as d4
                         from vmu_tariff vt,
                              vmu_tariff_plan vtp
                         where vtp.id_tariff_group = vt.id_lpu
                           And vtp.id_lpu in (select id_lpu from mu_ident)) T
                        on T.id_profile = vp.id_profile and
                           cs.date_begin between T.d1 and T.d2 and
                           cs.date_begin between T.d3 and T.d4 and
                           T.id_zone_type = cs.id_net_profile and
                           cs.is_oms = 1
where
    cs.date_begin between @date_start and @date_end
	and cs.is_oms = 1
    and (d.id = @depart_id or @depart_id = 0)
    and ((FIND_IN_SET(vp.profile_infis_code, @code) or CHAR_LENGTH(@code) < 2) or @code = 0 or @code is null)
	and (ds.doctor_id = @doc_id or @doc_id = 0)
	and cs.ID_NET_PROFILE = 1
group by ds.doctor_id , cs.id_profile
order by dept_name, doc_name, codes;
