set @date_start = '2019-01-01';
set @date_end = '2019-02-01';
create temporary table if not exists ord_type (select o.ID, o.Order_Number
                                               from case_services cs
                                                        left join orders o on cs.ID_ORDER = o.ID
                                                        left join price pr on cs.ID_PROFILE = pr.ID
                                               where o.orderCompletionDate between @date_start AND @date_end
                                                 and o.order_type = 0
                                                 and o.STATUS = 2
                                                 and pr.code in
                                                     (739, 740, 741, 742, 743, 745, 746, 747, 736, 737,
                                                      748, 749, 750, 853)
                                               group by o.ID);
create temporary table if not exists pzp(SELECT u.name as doc_name,
                                                concat_ws(' ', pd.SURNAME, pd.NAME, pd.SECOND_NAME) pat,
                                                ord.Order_Number, ord.orderCreateDate, ord.orderCompletionDate,
                                                SUM(cs.service_cost * cs.discount) as total_sum,
                                                case
                                                    when  ot.id is null
														and p.code = 701
                                                        then 'консультация'
                                                    when ord.id = ot.id
                                                      then 'ремонт протеза'
                                                    else 'прочее' end ord_type,
                                                SUM(p.UET) as total_uet, ord.id,
                                                case TECH.name when '.Без т.' then 'б/т' else short_name(TECH.name) end as tech_name
                                         FROM case_services AS cs
                                                  left join orders ord on cs.id_order = ord.id
                                                  left join price p on p.id = cs.id_profile
                                                  left join doctor_spec ds on ds.doctor_id = ord.id_doctor
                                                  left join users u on u.id = ds.user_id
                                                  LEFT JOIN technicians as TECH ON TECH.id = ord.Id_technician
                                                  left join patient_data pd on ord.Id_Human = pd.ID_HUMAN
                                                   left join ord_type as ot on ot.id = ord.ID
                                         WHERE cs.id_order >= 0 and pd.IS_ACTIVE = 1 and pd.DATE_END = '2200-01-01'
                                           AND ord.status = 2
                                           and ord.orderCompletionDate between @date_start AND @date_end
                                           AND ord.order_type = 0
                                         GROUP BY doc_name, pat, ord.ID
                                         order by doc_name);
create temporary table if not exists bzp(SELECT u.name as doc_name,
                                                concat_ws(' ', pd.SURNAME, pd.NAME, pd.SECOND_NAME) pat,
                                                ord.Order_Number, ord.orderCreateDate, ord.orderCompletionDate,
                                                SUM(cs.service_cost * cs.discount) as total_sum,
                                                SUM(p.UET) as total_uet, ord.id,
                                                case TECH.name when '.Без т.' then 'б/т' else short_name(TECH.name) end as tech_name
                                         FROM case_services AS cs
                                                  left join orders ord on cs.id_order = ord.id
                                                  left join price_orto_soc p on p.id = cs.id_profile
                                                  left join doctor_spec ds on ds.doctor_id = ord.id_doctor
                                                  left join users u on u.id = ds.user_id
                                                  LEFT JOIN technicians as TECH ON TECH.id = ord.Id_technician
                                                  left join patient_data pd on ord.Id_Human = pd.ID_HUMAN
                                         WHERE cs.id_order >= 0 and pd.IS_ACTIVE = 1 and pd.DATE_END = '2200-01-01'
                                           AND ord.status = 2
                                           and ord.orderCompletionDate between @date_start AND @date_end
                                           AND ord.order_type = 1
                                         GROUP BY doc_name, pat, ord.ID
                                         order by doc_name);
set @row = 0;
select @row := @row + 1, T.*
from (select doc_name, 'ПУ', short_name(pat), Order_Number, date_format(orderCreateDate, '%d-%m-%Y'),
             date_format(orderCompletionDate, '%d-%m-%Y'), '' as s1, round(total_sum), ord_type, tech_name, 0 as s2,
             round(coalesce(total_uet, 0), 2)
      from pzp
      union
      select doc_name, 'БЗП', short_name(pat), Order_Number, date_format(orderCreateDate, '%d-%m-%Y'),
             date_format(orderCompletionDate, '%d-%m-%Y'), round(total_sum), '' as s1, '' as s2, tech_name,
             round(coalesce(total_uet, 0), 2), 0 as s3
      from bzp
    order by 1,2 desc ) T
