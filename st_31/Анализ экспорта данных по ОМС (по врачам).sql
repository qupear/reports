SELECT
  dep.name AS dt,
  u.name AS doc,
  sp.name AS spec,
  c.date_begin AS db,
  COUNT(DISTINCT c.id_case) AS open,
  COUNT(DISTINCT c2.id_case) AS close,
  COUNT(DISTINCT c1.id_case) AS exp
FROM cases c
  JOIN case_services cs
    ON c.id_case = cs.id_case
    AND cs.is_oms = 1
  LEFT JOIN doctor_spec ds
    ON ds.doctor_id = c.ID_DOCTOR
  LEFT JOIN users u
    ON u.id = ds.user_id
  LEFT JOIN speciality sp
    ON sp.id = ds.spec_id
  LEFT JOIN departments dep
    ON dep.id = ds.department_id
  LEFT JOIN cases c1
    ON c1.id_case = c.id_case
    AND c1.exported = 1
  LEFT JOIN cases c2
    ON c2.id_case = c.id_case
    AND c2.date_end IS NOT NULL
WHERE c.DATE_BEGIN BETWEEN @date_start AND @date_end
GROUP BY dt,
         doc,
         spec,
         db
ORDER BY dt, doc, spec, db;