set @row = 0;
   
  SELECT @row := @row + 1, CASE WHEN p.order_id = -1 THEN p.case_id ELSE o.order_number END case_id,
 case WHEN p.order_id = -1 THEN 
         concat_ws(' ', pd.surname, pd.name, pd.second_name) ELSE
 concat_ws(' ', pd_ord.surname, pd_ord.name, pd_ord.second_name) end pat , 
 u.name, CASE WHEN ch.nal = 1 THEN p.summ ELSE 0 END,
         CASE WHEN ch.nal = 0 THEN p.summ ELSE 0 END, ch.number, date_format(ch.date, '%d-%m-%Y')
  	FROM
  		prepayments                p
  			LEFT JOIN checks       ch ON p.check = ch.id
  			LEFT JOIN orders       o ON p.order_id = o.id AND p.case_id = -1
  			LEFT JOIN cases        c ON p.case_id = c.id_case AND p.order_id = -1
  			LEFT JOIN doctor_spec  ds ON p.doctor_id = ds.doctor_id
  			LEFT JOIN departments  dep ON ds.department_id = dep.id
  			LEFT JOIN users        u ON ds.user_id = u.id
  			left  JOIN patient_data pd ON c.id_patient = pd.id_patient 
 left join patient_data pd_ord on o.id_human = pd_ord.id_human  
 
  	WHERE ch.date between @date_start and @date_end
 AND (ds.doctor_id = @doc_id OR @doc_id = 0)
   AND (dep.id = @depart_id OR @depart_id = 0)
 group by ch.id
  	  
  	 ;