set @date_start = '2019-01-27';
set @date_end = '2019-07-17';

select case when o.order_type = 0 then 'ПЗП' when o.order_type =1 then 'БЗП' end as type,
u.name, case when o.order_type = 0 then p.CODE when o.order_type =1 then pos.CODE end as code,
       case when o.order_type = 0 then p.NAME when o.order_type = 1 then pos.NAME end as code_name,
       count(cs.ID_PROFILE), case when o.order_type = 0 then sum(p.UET) when o.order_type = 1 then sum(pos.UET) end as uet,
       coalesce(sum(p.MATERIALS), 0)
from case_services cs
    left join orders o on cs.ID_ORDER = o.ID
left join price p on cs.ID_PROFILE = p.ID and o.order_type = 0
left join price_orto_soc pos on cs.ID_PROFILE = pos.id  and o.order_type = 1
left join doctor_spec ds on cs.ID_DOCTOR = ds.doctor_id
left join users u on ds.user_id = u.id
where cs.DATE_BEGIN between @date_start and @date_end and cs.ID_ORDER > 0 and ds.spec_id = 107
group by o.order_type, ds.doctor_id, cs.ID_PROFILE