set @row = 0;
SELECT @row:=@row+1, concat_ws(' ', pa.lname, pa.fname), pa.phone, pa.createddate, date_format(coalesce(DeactivationDate, ''), '%d.%m.%Y'),
       case when idappointment > 0 then 'Пациент записан на прием'
           WHEN idappointment = -1 and requeststatus = -1 THEN 'Запись не обработана'
           WHEN requeststatus = 3 and deactivationreason = 1 THEN 'Отменена по инициативе пациента'
           WHEN requeststatus = 3 AND deactivationreason = 2 THEN 'Не удалось связаться с пациентом'
           WHEN requeststatus = 3 AND deactivationreason = 3 THEN 'Нет специалиста в МО'
           WHEN requeststatus = 3 AND deactivationreason = 4 THEN 'Реализована запись в другую МО'
           WHEN requeststatus = 3 AND deactivationreason = 5 THEN 'Прочее'
        end result
       FROM pa_list pa
WHERE createddate BETWEEN @date_start AND @date_end
;