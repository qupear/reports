select T1.med_name, T1.doc_name, T1.pat_name, T1.id_case, T1.cost, T1.disc_cost, T2.total
from (select sum(p.summ) as total, p.case_id, p.doctor_id
      from prepayments p
               left join checks ch on ch.id = p.check
               left join doctor_spec ds on ds.doctor_id = p.doctor_id
      where p.order_id = -1
        and ch.date between @date_start and @date_end
        and ds.spec_id = 212
      group by p.case_id) T2
         left join (select c.id_case, pe.id as med_id, cs.id_doctor, pe.name as med_name, u.name as doc_name,
                           sum(cs.SERVICE_COST) as cost, sum(cs.SERVICE_COST * cs.discount) as disc_cost,
                           concat(pd.card_number, ', ', pd.surname, ' ', pd.name, ' ', pd.second_name) as pat_name
                    from case_services cs
                             left join doctor_spec ds on ds.doctor_id = cs.id_doctor
                             left join cases c on c.id_case = cs.id_case
                             left join personal pe on pe.id = c.id_nurse
                             left join users u on u.id = ds.user_id
                             left join departments d on d.id = ds.department_id
                             left join patient_data pd on pd.id_patient = c.id_patient
                    where ds.spec_id = 212
                      and cs.is_oms = 0
                    group by c.id_case
                    order by med_name, doc_name) T1 on T1.id_case = T2.case_id and T1.id_doctor = T2.doctor_id
where T1.med_name is not null
  and T1.med_id <> 2;