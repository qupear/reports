select 
     T1.dept_name, T1.doc_name, T1.total_case, T1.total_uet
 from
     (select 
         d.name as dept_name,
             u.name as doc_name,
             ROUND(COALESCE(SUM(vt.price), 0), 2) as total_case,
             ROUND(COALESCE(SUM(vt.uet), 0), 2) as total_uet,
             ds.doctor_id as doc_id
     from
         case_services cs
 	left join vmu_tariff vt on vt.id_profile = cs.id_profile and vt.id_zone_type = cs.id_net_profile
 	left join vmu_tariff_plan vtp on vtp.id_tariff_group = vt.id_lpu
     left join doctor_spec ds ON ds.doctor_id = cs.id_doctor
     left join users u ON ds.user_id = u.id
     left join departments d ON d.id = ds.department_id
     where
         cs.date_begin between @date_start and @date_end
             and (cs.is_oms = 1 or cs.is_oms = 5)
             and cs.id_order = - 1
             and (d.id = @depart_id or @depart_id = 0)
             and vt.TARIFF_BEGIN_DATE <= @date_start and vt.TARIFF_END_DATE >= @date_end
             and vtp.TP_BEGIN_DATE <= @date_start and vtp.TP_END_DATE >= @date_end
 	group by ds.doctor_id
     order by dept_name , doc_name , doc_id) T1