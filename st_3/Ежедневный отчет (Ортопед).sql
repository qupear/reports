drop table if exists tmp; set @row = 0;
create temporary table tmp (select p.id_case, p.date_protocol, p.id_human
                            from protocols p
                            where p.id_doctor = @doc_id
                              and p.date_protocol = @date_start
                            group by p.id_case);
SELECT @row := @row + 1, T_MAIN.time, T_MAIN.pat_name, T_MAIN.birthday, T_MAIN.unstruct_address, T_MAIN.prim,
       T_MAIN.child, T_MAIN.opc, T_MAIN.bzpc
FROM (SELECT a.time, CONCAT(pd.surname, ' ', pd.name, ' ', pd.second_name) as pat_name, pd.birthday,
             pa.unstruct_address, coalesce((ord.cure_start = @date_start), 0) as prim,
             (CURDATE() < DATE_ADD(pd.birthday, INTERVAL 18 year)) as child, (count(op.id) > 0) as opc,
             (count(obzp.id) > 0) as bzpc, c.id_human
      FROM (SELECT id_case, id_human FROM tmp) c
               LEFT JOIN patient_data pd ON pd.id_human = c.id_human and pd.is_active = 1 and pd.date_end = '2200-01-01'
               LEFT JOIN patient_address pa ON pa.id_address = pd.id_address_reg
               LEFT JOIN appointments a ON a.case_id = c.id_case
               LEFT JOIN orders op ON op.id_human = c.id_human AND op.order_type <> 1
               LEFT JOIN orders obzp ON obzp.id_human = c.id_human AND obzp.order_type = 1
               LEFT JOIN (select min(orderCreateDate) as cure_start, id_human FROM orders group by id_human) ord
                         on ord.id_human = c.id_human
      GROUP BY c.id_case
      order by a.time) T_MAIN;



