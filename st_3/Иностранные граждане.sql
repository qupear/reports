 create temporary table if not exists foreigners   select vo.country_short_name, pd.id_patient, pd.id_human from patient_data pd    left join vmu_oksm vo on vo.code = pd.c_oksm where vo.code is not null and vo.code <> 643;   create temporary table if not exists foreign_cases select c.id_case, c.id_patient   from cases c  where c.date_begin  between @date_start and @date_end and c.id_patient in (select id_patient from foreigners);    create temporary table if not exists  foreign_cases1   select id_case from foreign_cases;
 select 'Стоматологическая помощь' as txt, case cs.is_oms when 0 then 'ПУ' else 'ОМС' end, f.country_short_name,
        count(distinct f.id_human), count(distinct fc.id_case), sum(coalesce(p.cost, 0) + coalesce(T.price, 0))
 from case_services cs
          left join foreign_cases fc ON fc.id_case = cs.id_case
          left join foreigners f ON f.id_patient = fc.id_patient
          left join price p ON p.id = cs.id_profile and cs.is_oms = 0
          left join vmu_profile vp ON vp.id_profile = cs.id_profile and cs.is_oms = 1
          left join (select vt.uet, vt.id_profile, vt.id_zone_type, vt.price, vt.tariff_begin_date as d1,
                            vt.tariff_end_date as d2, vtp.tp_begin_date as d3, vtp.tp_end_date as d4
                     from vmu_tariff vt,
                          vmu_tariff_plan vtp
                     where vtp.id_tariff_group = vt.id_lpu
                       And vtp.id_lpu in (select id_lpu from mu_ident)) T
                    ON T.id_profile = vp.id_profile and cs.date_begin between T.d1 and T.d2 and
                       cs.date_begin between T.d3 and T.d4 and T.id_zone_type = cs.id_net_profile and cs.is_oms = 1
 where cs.id_case in (select id_case from foreign_cases1)
 group by f.country_short_name, cs.is_oms
 order by f.country_short_name, cs.is_oms;