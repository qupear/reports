set @row = 0;
 SELECT @row := @row + 1, sds.text, sum(sd.value) 
 FROM stat_docdata_struct sds 
 left join stat_docdata sd on sd.id_entity = sds.id 
 left join doctor_spec ds on ds.doctor_id = sd.id_doctor 
 where (ds.spec_id = @spec_id or @spec_id = 0) and sd.entity_date between @date_start and @date_end 
 	and (ds.department_id = @depart_id or @depart_id = 0) 
 group by sd.id_entity;