set @zoloto = 14; set @tech_com = 15;
create temporary table if not exists gold (select cs.id_order
                                           from case_services cs
                                                    left join price p on cs.id_profile = p.id
                                                    left join orders ord on ord.id = cs.id_order
                                           where ord.orderCompletionDate between @date_start and @date_end
                                             and ord.order_type <> 1
                                             and ord.spec = 4
                                             and cs.id_order > 0
                                             and cs.is_oms = 0
                                             and p.group_ids = @zoloto
                                           group by cs.id_order);
create temporary table if not exists t_com (select cs.id_order
                                            from case_services cs
                                                     left join price p on cs.id_profile = p.id
                                                     left join orders ord on ord.id = cs.id_order
                                            where ord.orderCompletionDate between @date_start and @date_end
                                              and ord.order_type <> 1
                                              and ord.spec = 4
                                              and cs.id_order > 0
                                              and cs.is_oms = 0
                                              and p.group_ids = @tech_com
                                            group by cs.id_order);
create temporary table if not exists tab_no_drag (select ord.Id_technician as id, sum(cs.service_cost) as sum1
                                                  from orders ord
                                                           left join case_services cs on cs.id_order = ord.id
                                                           left join price p on p.id = cs.id_profile
                                                  where ord.orderCompletionDate between @date_start and @date_end
                                                    and (ord.order_type = 0 or ord.order_type = 2)
                                                    and ord.spec = 4
                                                    and p.in_tech = 1
                                                    and ord.id not in (select id_order from gold)
                                                    and ord.id not in (select id_order from t_com)
                                                  group by ord.Id_technician);
create temporary table if not exists tab_drag (select ord.Id_technician as id, sum(cs.service_cost) as sum1
                                               from orders ord
                                                        left join case_services cs on cs.id_order = ord.id
                                                        left join price p on p.id = cs.id_profile
                                               where ord.orderCompletionDate between @date_start and @date_end
                                                 and (ord.order_type = 0 or ord.order_type = 2)
                                                 and ord.spec = 4
                                                 and p.in_tech = 1
                                                 and ord.id in (select id_order from gold)
                                               group by ord.Id_technician);




create temporary table if not exists tab_tech (select ord.Id_technician as id, sum(cs.service_cost) as sum1
                                               from orders ord
                                                        left join case_services cs on cs.id_order = ord.id
                                                        left join price p on p.id = cs.id_profile
                                               where ord.orderCompletionDate between @date_start and @date_end
                                                 and (ord.order_type = 0 or ord.order_type = 2)
                                                 and ord.spec = 4
                                                 and p.in_tech = 1
                                                 and ord.id in (select id_order from t_com)
                                               group by ord.Id_technician);

select tec.name, coalesce(T1.sum1, 0), coalesce(T2.sum1, 0), coalesce(T3.sum1, 0),
       coalesce(T1.sum1, 0) + coalesce(T2.sum1, 0) + coalesce(T3.sum1, 0)
from technicians tec
         left join tab_no_drag T1 on tec.id = T1.id
         left join tab_drag T2 on T1.id = T2.id
         left join tab_tech T3 on T1.id = T3.id
where tec.position = 'Зубной техник'
  and coalesce(T1.sum1, 0) + coalesce(T2.sum1, 0) + coalesce(T3.sum1, 0) > 0
order by tec.name;