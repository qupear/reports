SELECT T.date_value, T.s1, T.s2, T.s3, T.s4, T.s5, T.s6, T.s7, T.s8, T.s9, T.s10, T.s11, T.s12, T.s13, T.s14, T.s15,
       T.s16, T.s17, T.s18, T.s19, T.s20, T.s21, T.s22, T.s23, T.s24, T.s25, T.s26, T.s27, T.s28, T.s29, T.s30, T.s31,
       T.s32, T.s33, T.s34, T.s35, T.s38, T.s39, T.s40, T.s41, T.s42, T.s43, T.s44, T.s45, T.s46, T.s47, T.s48, T.s49,
       T.s50, T.s51, T.s52, T.s53, T.s54, T.s55, T.s56, T.s100
FROM (SELECT atos.date_value, COALESCE(T1.summ, 0) as s1, COALESCE(T2.summ, 0) as s2, COALESCE(T3.summ, 0) as s3,
             COALESCE(T4.summ, 0) as s4, COALESCE(T5.summ, 0) as s5, COALESCE(T6.summ, 0) as s6,
             COALESCE(T7.summ, 0) as s7, COALESCE(T8.summ, 0) as s8, COALESCE(T9.summ, 0) as s9,
             COALESCE(T10.summ, 0) as s10, COALESCE(T11.summ, 0) as s11, COALESCE(T12.summ, 0) as s12,
             COALESCE(T13.summ, 0) as s13, COALESCE(T14.summ, 0) as s14, COALESCE(T15.summ, 0) as s15,
             COALESCE(T16.summ, 0) as s16, COALESCE(T17.summ, 0) as s17, COALESCE(T18.summ, 0) as s18,
             COALESCE(T19.summ, 0) as s19, COALESCE(T20.summ, 0) as s20, COALESCE(T21.summ, 0) as s21,
             COALESCE(T22.summ, 0) as s22, COALESCE(T23.summ, 0) as s23, COALESCE(T24.summ, 0) as s24,
             COALESCE(T25.summ, 0) as s25, COALESCE(T26.summ, 0) as s26, COALESCE(T27.summ, 0) as s27,
             COALESCE(T28.summ, 0) as s28, COALESCE(T29.summ, 0) as s29, COALESCE(T30.summ, 0) as s30,
             COALESCE(T31.summ, 0) as s31, COALESCE(T32.summ, 0) as s32, COALESCE(T33.summ, 0) as s33,
             COALESCE(T34.summ, 0) as s34, COALESCE(T35.summ, 0) as s35, COALESCE(T38.summ, 0) as s38,
             COALESCE(T39.summ, 0) as s39, COALESCE(T40.summ, 0) as s40, COALESCE(T41.summ, 0) as s41,
             COALESCE(T42.summ, 0) as s42, COALESCE(T43.summ, 0) as s43, COALESCE(T44.summ, 0) as s44,
             COALESCE(T45.summ, 0) as s45, COALESCE(T46.summ, 0) as s46, COALESCE(T47.summ, 0) as s47,
             COALESCE(T48.summ, 0) as s48, COALESCE(T49.summ, 0) as s49, COALESCE(T50.summ, 0) as s50,
             COALESCE(T51.summ, 0) as s51, COALESCE(T52.summ, 0) as s52, COALESCE(T53.summ, 0) as s53,
             COALESCE(T54.summ, 0) as s54, COALESCE(T55.summ, 0) as s55, COALESCE(T56.summ, 0) as s56,
             COALESCE(T100.uet, 0) as s100
      from aggregated_table_of_statistics atos
               LEFT JOIN (select count(T11.id_case) as summ, T11.date_begin as date_value
                          from (select cs.id_case, cs.date_begin
                                from case_services cs
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND (cs.is_oms = 0)
                                group by cs.id_case) T11
                          group by T11.date_begin) T1 on T1.date_value = atos.date_value
               LEFT JOIN (select count(T21.id_case) as summ, T21.date_begin as date_value
                          from (select cs.id_case, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND (cs.is_oms = 0)
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 15 year) > c.date_begin
                                group by cs.id_case) T21
                          group by T21.date_begin) T2 on T2.date_value = atos.date_value
               LEFT JOIN (select count(T31.id_case) as summ, T31.date_begin as date_value
                          from (select cs.id_case, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND (cs.is_oms = 0)
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 15 year) < c.date_begin
                                  and DATE_ADD(pd.birthday, INTERVAL 18 year) > c.date_begin
                                group by cs.id_case) T31
                          group by T31.date_begin) T3 on T3.date_value = atos.date_value
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 2
                            and name in ('1002')
                          group by date_value) T4 on T4.date_value = atos.date_value
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 2
                            and name in ('1002')
                            and type_age = 0
                          group by date_value) T5 on T5.date_value = atos.date_value
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 2
                            and name in ('1002')
                            and type_age = 1
                          group by date_value) T6 on T6.date_value = atos.date_value
               LEFT JOIN (select count(T71.id_case) as summ, T71.date_begin as date_value
                          from (select cs.id_case, cs.tooth_name, cs.date_begin
                                from case_services cs
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id
                                                        from price
                                                        where code in
                                                              ('1014', '1015', '1016', '1017', '1018', '1019', '1020',
                                                               '1027', '1028'))
                                  and cs.id_diagnosis IN (Select vd.id_diagnosis
                                                          From vmu_diagnosis vd
                                                          Where vd.diagnosis_code in
                                                                ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5',
                                                                 'k02.6', 'k02.7', 'k02.8', 'k02.9', 'k03.2', 'k04.0',
                                                                 'k04.1', 'k04.2', 'k04.3', 'k04.4', 'k04.5'))
                                group by cs.id_case, cs.tooth_name) T71
                          group by T71.date_begin) T7 on T7.date_value = atos.date_value
               LEFT JOIN (select count(T81.id_case) as summ, T81.date_begin as date_value
                          from (select cs.id_case, cs.tooth_name, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id
                                                        from price
                                                        where code in
                                                              ('1014', '1015', '1016', '1017', '1018', '1019', '1020',
                                                               '1027', '1028'))
                                  and cs.id_diagnosis IN (Select vd.id_diagnosis
                                                          From vmu_diagnosis vd
                                                          Where vd.diagnosis_code in
                                                                ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5',
                                                                 'k02.6', 'k02.7', 'k02.8', 'k02.9', 'k03.2', 'k04.0',
                                                                 'k04.1', 'k04.2', 'k04.3', 'k04.4', 'k04.5'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 15 year) > c.date_begin
                                group by cs.id_case, cs.tooth_name) T81
                          group by T81.date_begin) T8 on T8.date_value = atos.date_value
               LEFT JOIN (select count(T91.id_case) as summ, T91.date_begin as date_value
                          from (select cs.id_case, cs.tooth_name, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id
                                                        from price
                                                        where code in
                                                              ('1014', '1015', '1016', '1017', '1018', '1019', '1020',
                                                               '1027', '1028'))
                                  and cs.id_diagnosis IN (Select vd.id_diagnosis
                                                          From vmu_diagnosis vd
                                                          Where vd.diagnosis_code in
                                                                ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5',
                                                                 'k02.6', 'k02.7', 'k02.8', 'k02.9', 'k03.2', 'k04.0',
                                                                 'k04.1', 'k04.2', 'k04.3', 'k04.4', 'k04.5'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 15 year) < c.date_begin
                                  and DATE_ADD(pd.birthday, INTERVAL 18 year) > c.date_begin
                                group by cs.id_case, cs.tooth_name) T91
                          group by T91.date_begin) T9 on T9.date_value = atos.date_value
               LEFT JOIN (select count(T101.id_case) as summ, T101.date_begin as date_value
                          from (select cs.id_case, cs.tooth_name, cs.date_begin
                                from case_services cs
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id
                                                        from price
                                                        where code in ('1014', '1016', '1017', '1019', '1027', '1028'))
                                  and cs.id_diagnosis IN (Select vd.id_diagnosis
                                                          From vmu_diagnosis vd
                                                          Where vd.diagnosis_code in
                                                                ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5',
                                                                 'k02.6', 'k02.7', 'k02.8', 'k02.9', 'k03.2'))
                                group by cs.id_case, cs.tooth_name) T101
                          group by T101.date_begin) T10 ON T10.date_value = atos.date_value
               LEFT JOIN (select count(T111.id_case) as summ, T111.date_begin as date_value
                          from (select cs.id_case, cs.tooth_name, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id
                                                        from price
                                                        where code in ('1014', '1016', '1017', '1019', '1027', '1028'))
                                  and cs.id_diagnosis IN (Select vd.id_diagnosis
                                                          From vmu_diagnosis vd
                                                          Where vd.diagnosis_code in
                                                                ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5',
                                                                 'k02.6', 'k02.7', 'k02.8', 'k02.9', 'k03.2'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 15 year) > c.date_begin
                                group by cs.id_case, cs.tooth_name) T111
                          group by T111.date_begin) T11 ON T11.date_value = atos.date_value
               LEFT JOIN (select count(T121.id_case) as summ, T121.date_begin as date_value
                          from (select cs.id_case, cs.tooth_name, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id
                                                        from price
                                                        where code in ('1014', '1016', '1017', '1019', '1027', '1028'))
                                  and cs.id_diagnosis IN (Select vd.id_diagnosis
                                                          From vmu_diagnosis vd
                                                          Where vd.diagnosis_code in
                                                                ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5',
                                                                 'k02.6', 'k02.7', 'k02.8', 'k02.9', 'k03.2'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 15 year) < c.date_begin
                                  and DATE_ADD(pd.birthday, INTERVAL 18 year) > c.date_begin
                                group by cs.id_case, cs.tooth_name) T121
                          group by T121.date_begin) T12 ON T12.date_value = atos.date_value
               LEFT JOIN (select count(T131.id_case) as summ, T131.date_begin as date_value
                          from (select cs.id_case, cs.tooth_name, cs.date_begin
                                from case_services cs
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('1015', '1018', '1020'))
                                  and cs.id_diagnosis IN (Select vd.id_diagnosis
                                                          From vmu_diagnosis vd
                                                          Where vd.diagnosis_code in
                                                                ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5',
                                                                 'k02.6', 'k02.7', 'k02.8', 'k02.9', 'k03.2'))
                                group by cs.id_case, cs.tooth_name) T131
                          group by T131.date_begin) T13 ON T13.date_value = atos.date_value
               LEFT JOIN (select count(T141.id_case) as summ, T141.date_begin as date_value
                          from (select cs.id_case, cs.tooth_name, cs.date_begin
                                from case_services cs
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id
                                                        from price
                                                        where code in
                                                              ('1014', '1015', '1016', '1017', '1018', '1019', '1020',
                                                               '1027', '1028'))
                                  and cs.id_diagnosis IN (Select vd.id_diagnosis
                                                          From vmu_diagnosis vd
                                                          Where vd.diagnosis_code in
                                                                ('k04.0', 'k04.1', 'k04.2', 'k04.3', 'k04.4', 'k04.5'))
                                group by cs.id_case, cs.tooth_name) T141
                          group by T141.date_begin) T14 ON T14.date_value = atos.date_value
               LEFT JOIN (select count(T151.id_case) as summ, T151.date_begin as date_value
                          from (select cs.id_case, cs.tooth_name, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id
                                                        from price
                                                        where code in
                                                              ('1014', '1015', '1016', '1017', '1018', '1019', '1020',
                                                               '1027', '1028'))
                                  and cs.id_diagnosis IN (Select vd.id_diagnosis
                                                          From vmu_diagnosis vd
                                                          Where vd.diagnosis_code in
                                                                ('k04.0', 'k04.1', 'k04.2', 'k04.3', 'k04.4', 'k04.5'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 15 year) > c.date_begin
                                group by cs.id_case, cs.tooth_name) T151
                          group by T151.date_begin) T15 ON T15.date_value = atos.date_value
               LEFT JOIN (select count(T161.id_case) as summ, T161.date_begin as date_value
                          from (select cs.id_case, cs.tooth_name, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id
                                                        from price
                                                        where code in
                                                              ('1014', '1015', '1016', '1017', '1018', '1019', '1020',
                                                               '1027', '1028'))
                                  and cs.id_diagnosis IN (Select vd.id_diagnosis
                                                          From vmu_diagnosis vd
                                                          Where vd.diagnosis_code in
                                                                ('k04.0', 'k04.1', 'k04.2', 'k04.3', 'k04.4', 'k04.5'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 15 year) < c.date_begin
                                  and DATE_ADD(pd.birthday, INTERVAL 18 year) > c.date_begin
                                group by cs.id_case, cs.tooth_name) T161
                          group by T161.date_begin) T16 ON T16.date_value = atos.date_value
               LEFT JOIN (select count(T171.id_case) as summ, T171.date_begin as date_value
                          from (select cs.id_case, cs.tooth_name, cs.date_begin
                                from case_services cs
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id
                                                        from price
                                                        where code in
                                                              ('1014', '1015', '1016', '1017', '1018', '1019', '1020',
                                                               '1027', '1028'))
                                  and cs.id_diagnosis IN (Select vd.id_diagnosis
                                                          From vmu_diagnosis vd
                                                          Where vd.diagnosis_code in
                                                                ('k04.0', 'k04.1', 'k04.2', 'k04.3', 'k04.4', 'k04.5'))
                                  and cs.tooth_name in
                                      ('51', '52', '53', '54', '55', '61', '62', '63', '64', '65', '71', '72', '73',
                                       '74', '75', '81', '82', '83', '84', '85')
                                group by cs.id_case, cs.tooth_name) T171
                          group by T171.date_begin) T17 ON T17.date_value = atos.date_value
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 2
                            and name in
                                ('1027', '1028', '1029', '1031', '1033', '1034', '1039', '1040', '1041', '1042', '1043',
                                 '1044', '1045', '1046', '1047', '1048', '1049', '1050', '1052', '1054', '1058', '1059',
                                 '1062', '1073', '1074', '1092', '1093', '1094', '1095', '1096', '1097', '1098', '1099',
                                 '1100', '1101', '1102', '1103', '1104', '1105', '1106', '1107', '1108', '1109', '1110',
                                 '1111', '1118', '1119')
                          group by date_value) T18 on T18.date_value = atos.date_value
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 2
                            and name in ('1030', '1032', '1035', '1036')
                            and type_tooth = 0
                          group by date_value) T19 on T19.date_value = atos.date_value
               LEFT JOIN (select count(T201.id_case) as summ, T201.date_begin as date_value
                          from (select cs.id_case, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('2006', '2007'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 18 year) > c.date_begin
                                group by cs.id_case) T201
                          group by T201.date_begin) T20 ON T20.date_value = atos.date_value
               LEFT JOIN (select count(T211.id_case) as summ, T211.date_begin as date_value
                          from (select cs.id_case, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('2006', '2007'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 15 year) > c.date_begin
                                group by cs.id_case) T211
                          group by T211.date_begin) T21 ON T21.date_value = atos.date_value
               LEFT JOIN (select count(T221.id_case) as summ, T221.date_begin as date_value
                          from (select cs.id_case, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('2006', '2007'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 15 year) < c.date_begin
                                  and DATE_ADD(pd.birthday, INTERVAL 18 year) > c.date_begin
                                group by cs.id_case) T221
                          group by T221.date_begin) T22 ON T22.date_value = atos.date_value
               LEFT JOIN (select count(T231.id_case) as summ, T231.date_begin as date_value
                          from (select cs.id_case, cs.tooth_name, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('2006', '2007'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 18 year) > c.date_begin
                                group by cs.id_case, cs.tooth_name) T231
                          group by T231.date_begin) T23 ON T23.date_value = atos.date_value
               LEFT JOIN (select count(T241.id_case) as summ, T241.date_begin as date_value
                          from (select cs.id_case, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('2010'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 18 year) > c.date_begin
                                group by cs.id_case) T241
                          group by T241.date_begin) T24 ON T24.date_value = atos.date_value
               LEFT JOIN (select count(T251.id_case) as summ, T251.date_begin as date_value
                          from (select cs.id_case, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('2010'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 15 year) > c.date_begin
                                group by cs.id_case) T251
                          group by T251.date_begin) T25 ON T25.date_value = atos.date_value
               LEFT JOIN (select count(T261.id_case) as summ, T261.date_begin as date_value
                          from (select cs.id_case, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('2010'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 15 year) < c.date_begin
                                  and DATE_ADD(pd.birthday, INTERVAL 18 year) > c.date_begin
                                group by cs.id_case) T261
                          group by T261.date_begin) T26 ON T26.date_value = atos.date_value
               LEFT JOIN (select count(T271.id_case) as summ, T271.date_begin as date_value
                          from (select cs.id_case, cs.tooth_name, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('2010'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 18 year) > c.date_begin
                                group by cs.id_case, cs.tooth_name) T271
                          group by T271.date_begin) T27 ON T27.date_value = atos.date_value
               LEFT JOIN (select count(T281.id_case) as summ, T281.date_begin as date_value
                          from (select cs.id_case, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('2026', '2028', '2029'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 18 year) > c.date_begin
                                group by cs.id_case) T281
                          group by T281.date_begin) T28 ON T28.date_value = atos.date_value
               LEFT JOIN (select count(T291.id_case) as summ, T291.date_begin as date_value
                          from (select cs.id_case, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('2026', '2028', '2029'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 15 year) > c.date_begin
                                group by cs.id_case) T291
                          group by T291.date_begin) T29 ON T29.date_value = atos.date_value
               LEFT JOIN (select count(T301.id_case) as summ, T301.date_begin as date_value
                          from (select cs.id_case, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('2026', '2028', '2029'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 15 year) < c.date_begin
                                  and DATE_ADD(pd.birthday, INTERVAL 18 year) > c.date_begin
                                group by cs.id_case) T301
                          group by T301.date_begin) T30 ON T30.date_value = atos.date_value
               LEFT JOIN (select count(T311.id_case) as summ, T311.date_begin as date_value
                          from (select cs.id_case, cs.tooth_name, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('2026', '2028', '2029'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 18 year) > c.date_begin
                                group by cs.id_case, cs.tooth_name) T311
                          group by T311.date_begin) T31 ON T31.date_value = atos.date_value
               LEFT JOIN (select count(T321.id_case) as summ, T321.date_begin as date_value
                          from (select cs.id_case, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('1053'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 18 year) > c.date_begin
                                group by cs.id_case) T321
                          group by T321.date_begin) T32 ON T32.date_value = atos.date_value
               LEFT JOIN (select count(T331.id_case) as summ, T331.date_begin as date_value
                          from (select cs.id_case, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('1053'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 15 year) > c.date_begin
                                group by cs.id_case) T331
                          group by T331.date_begin) T33 ON T33.date_value = atos.date_value
               LEFT JOIN (select count(T341.id_case) as summ, T341.date_begin as date_value
                          from (select cs.id_case, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('1053'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 15 year) < c.date_begin
                                  and DATE_ADD(pd.birthday, INTERVAL 18 year) > c.date_begin
                                group by cs.id_case) T341
                          group by T341.date_begin) T34 ON T34.date_value = atos.date_value
               LEFT JOIN (select count(T351.id_case) as summ, T351.date_begin as date_value
                          from (select cs.id_case, cs.tooth_name, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('1053'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 18 year) > c.date_begin
                                group by cs.id_case, cs.tooth_name) T351
                          group by T351.date_begin) T35 ON T35.date_value = atos.date_value
               LEFT JOIN (select count(T381.id_case) as summ, T381.date_begin as date_value
                          from (select cs.id_case, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('1013'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 18 year) > c.date_begin
                                group by cs.id_case) T381
                          group by T381.date_begin) T38 ON T38.date_value = atos.date_value
               LEFT JOIN (select count(T391.id_case) as summ, T391.date_begin as date_value
                          from (select cs.id_case, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('1013'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 15 year) > c.date_begin
                                group by cs.id_case) T391
                          group by T391.date_begin) T39 ON T39.date_value = atos.date_value
               LEFT JOIN (select count(T401.id_case) as summ, T401.date_begin as date_value
                          from (select cs.id_case, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('1013'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 15 year) < c.date_begin
                                  and DATE_ADD(pd.birthday, INTERVAL 18 year) > c.date_begin
                                group by cs.id_case) T401
                          group by T401.date_begin) T40 ON T40.date_value = atos.date_value
               LEFT JOIN (select count(T411.id_case) as summ, T411.date_begin as date_value
                          from (select cs.id_case, cs.tooth_name, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('1013'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 18 year) > c.date_begin
                                group by cs.id_case, cs.tooth_name) T411
                          group by T411.date_begin) T41 ON T41.date_value = atos.date_value
               LEFT JOIN (select count(T421.id_case) as summ, T421.date_begin as date_value
                          from (select cs.id_case, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('2014', '2015', '2021'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 18 year) > c.date_begin
                                group by cs.id_case) T421
                          group by T421.date_begin) T42 ON T42.date_value = atos.date_value
               LEFT JOIN (select count(T431.id_case) as summ, T431.date_begin as date_value
                          from (select cs.id_case, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('2014', '2015', '2021'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 15 year) > c.date_begin
                                group by cs.id_case) T431
                          group by T431.date_begin) T43 ON T43.date_value = atos.date_value
               LEFT JOIN (select count(T441.id_case) as summ, T441.date_begin as date_value
                          from (select cs.id_case, cs.date_begin
                                from case_services cs,
                                     cases c,
                                     patient_data pd
                                where cs.id_doctor = @doc_id
                                  and cs.date_begin between @date_start and @date_end
                                  AND cs.is_oms = 0
                                  and cs.id_profile IN (select id from price where code in ('2014', '2015', '2021'))
                                  and c.id_case = cs.id_case
                                  and pd.id_patient = c.id_patient
                                  and DATE_ADD(pd.birthday, INTERVAL 15 year) < c.date_begin
                                  and DATE_ADD(pd.birthday, INTERVAL 18 year) > c.date_begin
                                group by cs.id_case) T441
                          group by T441.date_begin) T44 ON T44.date_value = atos.date_value
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 1
                            and name in ('18730', '80380')
                          group by date_value) T45 on T45.date_value = atos.date_value
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 1
                            and name in ('18730', '80380')
                            and type_age = 0
                          group by date_value) T46 on T46.date_value = atos.date_value
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 1
                            and name in ('18730', '80380')
                            and type_age = 1
                          group by date_value) T47 on T47.date_value = atos.date_value
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 1
                            and name in ('17239', '17251', '18720', '19010', '44170', '47780', '79760', '80370')
                          group by date_value) T48 on T48.date_value = atos.date_value
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 1
                            and name in ('17239', '17251', '18720', '19010', '44170', '47780', '79760', '80370')
                            and type_age = 0
                          group by date_value) T49 on T49.date_value = atos.date_value
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 1
                            and name in ('17239', '17251', '18720', '19010', '44170', '47780', '79760', '80370')
                            and type_age = 1
                          group by date_value) T50 on T50.date_value = atos.date_value
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 1
                            and name in ('17591', '17691')
                          group by date_value) T51 on T51.date_value = atos.date_value
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 1
                            and name in ('17591', '17691')
                            and type_age = 0
                          group by date_value) T52 on T52.date_value = atos.date_value
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 1
                            and name in ('17591', '17691')
                            and type_age = 1
                          group by date_value) T53 on T53.date_value = atos.date_value
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 1
                            and name in ('55791', '5761', '4781', '3790', '2350', '1392', '480')
                          group by date_value) T54 on T54.date_value = atos.date_value
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 1
                            and name in ('55791', '5761', '4781', '3790', '2350', '1392', '480')
                            and type_age = 0
                          group by date_value) T55 on T55.date_value = atos.date_value
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 1
                            and name in ('55791', '5761', '4781', '3790', '2350', '1392', '480')
                            and type_age = 1
                          group by date_value) T56 on T56.date_value = atos.date_value
               LEFT JOIN (SELECT tcs.date_begin as date_value, ROUND(COALESCE(SUM(tvp.UET), 0), 2) as uet
                          FROM case_services tcs
                                   LEFT JOIN (SELECT p.id, p.uet FROM price p) tvp ON tvp.id = tcs.id_profile
                          WHERE tcs.date_begin between @date_start AND @date_end
                            AND tcs.id_doctor = @doc_id
                            and tcs.is_oms = 0
                          GROUP BY tcs.date_begin) T100 ON T100.date_value = atos.date_value
      where atos.id_doctor = @doc_id
        and atos.date_value between @date_start and @date_end
      group by atos.date_value
      order by atos.date_value) T;

