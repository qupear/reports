SELECT d.name, u.name, SUM((coalesce(T1.total_case, 0) - coalesce(T2.summ, 0))) AS debt
FROM (SELECT cs.ID_CASE AS case_id, SUM(cs.SERVICE_COST * cs.discount) AS total_case, cs.id_doctor AS doctor_id,
             c.DATE_BEGIN, c.id_patient AS id_patient
      FROM case_services cs,
           cases c
      WHERE cs.ID_ORDER = -1
        AND cs.IS_OMS = 0
        AND cs.DATE_BEGIN BETWEEN @date_start AND @date_end
        AND c.ID_CASE = cs.ID_CASE
      GROUP BY cs.ID_CASE, cs.id_doctor) T1
         LEFT JOIN (SELECT p.case_id AS case_id, COALESCE(SUM(p.summ), 0) AS summ, p.doctor_id
                    FROM prepayments p
                    GROUP BY p.case_id, p.doctor_id) T2 ON T1.case_id = T2.case_id AND T1.doctor_id = T2.doctor_id
         LEFT JOIN patient_data pd ON pd.id_patient = T1.id_patient
         LEFT JOIN patient_address pa ON pa.ID_ADDRESS = pd.ID_ADDRESS_REG
         LEFT JOIN doctor_spec ds ON ds.doctor_id = T1.doctor_id
         LEFT JOIN users u ON u.id = ds.user_id
         LEFT JOIN departments d ON d.id = ds.department_id
WHERE coalesce(T1.total_case, 0) <> coalesce(T2.summ, 0)
  AND ((ds.doctor_id = @doc_id) OR (@doc_id = 0))
  AND ((d.id = @depart_id) OR (@depart_id = 0))
GROUP BY u.name;