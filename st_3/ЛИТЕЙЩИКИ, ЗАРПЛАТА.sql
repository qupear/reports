select tech.name, ord.order_number, sum(cs.service_cost) as total
 from orders ord
 left join case_services cs on cs.id_order = ord.id
 left join price p on p.id = cs.id_profile
 left join technicians tech on tech.id = ord.id_technician
 where ord.orderCompletionDate between @date_start and @date_end and ord.order_type = 0
 	and ord.Id_technician in (select id from technicians where position = "Литейщик") and p.in_tech = 1
 group by ord.id
 order by tech.name, ord.order_number  