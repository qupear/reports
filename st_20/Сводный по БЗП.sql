SELECT concat_ws(' ', 'Наряд:', o.order_number, 'Пациент:', pd.surname, pd.name, pd.second_name, 'Направление:',
                 o.bzp_direction_text, 'Адрес:', pa.unstruct_address, 'Телефон:', pd.remark),
       concat_ws(' ', 'Врач:', u.name, 'Техник:', tech.name), o.ordercreatedate, CASE o.ordercompletiondate
	                                                                                 WHEN '0001-01-01'
		                                                                                 THEN '-'
	                                                                                 ELSE o.ordercompletiondate
                                                                                 END, 'протезирование', pos.code,
       count(cs.id_profile), cs.service_cost, sum(cs.service_cost) 
	FROM
		orders                        o
			LEFT JOIN case_services   cs ON o.id = cs.id_order
			LEFT JOIN doctor_spec     ds ON cs.id_doctor = ds.doctor_id
			LEFT JOIN users           u ON ds.user_id = u.id
			LEFT JOIN patient_data    pd ON o.id_human = pd.id_human
			LEFT JOIN patient_address pa ON pd.id_address_reg = pa.id_address
			LEFT JOIN price_orto_soc  pos ON cs.id_profile = pos.id
			LEFT JOIN technicians     tech ON o.id_technician = tech.id
	WHERE ((o.ordercreatedate BETWEEN @date_start AND @date_end AND o.status = 2 AND @order_state) OR
	       (o.ordercreatedate BETWEEN @date_start AND @date_end AND o.status < 2 AND NOT @order_state))
	  AND o.order_type = 1
	  AND cs.is_oms = 0
	  AND pd.is_active = 1
	  AND pd.date_end = '2200-01-01'
	GROUP BY pd.surname, pd.name, pd.second_name, o.id, cs.id_profile
	ORDER BY pd.surname, pd.name, pd.second_name;
