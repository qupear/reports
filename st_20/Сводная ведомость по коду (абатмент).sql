select concat(pd.surname, ' ', pd.name, ' ', pd.second_name), o.order_number, u.name, cs.SERVICE_COST, count(cs.ID_PROFILE),
       sum(cs.service_cost),
       o.orderCreateDate, o.orderCompletionDate, pa.UNSTRUCT_ADDRESS, pd.remark
from case_services cs
         left join doctor_spec ds ON ds.doctor_id = cs.id_doctor
         left join users u ON u.id = ds.user_id
         left join orders o ON cs.id_order = o.id
         left join price p on p.id = cs.id_profile
         left join patient_data pd on o.id_human = pd.id_human and pd.is_active = 1 and pd.date_end = '2200-01-01'
         left join patient_address pa on pa.ID_ADDRESS = pd.ID_ADDRESS_LOC
where o.ordercreatedate between @date_start and @date_end
  and cs.id_order > 0
  and cs.is_oms = 0
  and @code = p.code
and (o.id_doctor = @doc_id or @doc_id = 0)
group by cs.id_order;
