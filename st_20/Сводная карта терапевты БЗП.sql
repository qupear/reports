set @date_start = '2019-01-01';
set @date_end = '2019-08-10';
select dep.name, u.name, o.Order_Number, short_name(concat_ws(' ', pd.SURNAME, pd.NAME, pd.SECOND_NAME)) pat,
       date_format(o.orderCreateDate, '%d-%m-%Y'),
 case when o.STATUS < 2 then sum(cs.SERVICE_COST)  else 0.00 end,
       coalesce(date_format(o.orderCompletionDate, '%d-%m-%Y'), '-'),
 case when o.STATUS = 2 and o.orderCreateDate between @date_start and @date_end
           and o.orderCompletionDate between @date_start and @date_end then sum(cs.SERVICE_COST) else 0.00 end,
case when o.STATUS < 2 then sum(cs.SERVICE_COST) else 0.00 end + case when o.STATUS = 2 then sum(cs.SERVICE_COST) else 0.00 end
       from case_services cs
left join price_orto_soc pos on cs.ID_PROFILE = pos.ID
left join doctor_spec ds on cs.ID_DOCTOR = ds.doctor_id
left join users u on ds.user_id = u.id
left join orders o on cs.ID_ORDER = o.ID
left join patient_data pd on o.Id_Human = pd.ID_HUMAN and pd.IS_ACTIVE = 1 and pd.DATE_END = '2200-01-01'
LEFT JOIN departments dep on ds.department_id = dep.id
where pos.CODE in ('15', '16', '24', '25', '26', '27')  and cs.ID_ORDER > 0 and ds.spec_id in (106, 104)
  and (ds.department_id = @depart_id or @depart_id = 0)
and cs.DATE_BEGIN between @date_start and @date_end
group by dep.id, ds.doctor_id, o.ID
order by dep.name, u.name, o.ID;
