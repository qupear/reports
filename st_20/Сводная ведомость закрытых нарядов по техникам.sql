set @date_start = '2019-05-01';
set @date_end = '2019-08-10';
set @row = 0;
select @row:=@row+1, T.* from (
select  t.name,
    sum(case when o.order_type = 1 then coalesce((cs.SERVICE_COST),0)  end),
      sum(case when o.order_type = 0 then coalesce((cs.SERVICE_COST), 0) end),
        sum(case when o.order_type = 0 then coalesce((cs.SERVICE_COST), 0) * cs.discount end) ,
              coalesce(sum(pos.UET_TECH), 0), coalesce(sum(p.UET_TECH), 0), coalesce(sum(pos.UET_TECH) + sum(p.UET_TECH), 0)
       from case_services cs
left join orders o on cs.ID_ORDER = o.ID
left join price_orto_soc pos on cs.ID_PROFILE = pos.id and o.order_type = 1
left join price p on cs.ID_PROFILE = p.ID and o.order_type = 0
left join technicians t on o.Id_technician = t.ID
where o.STATUS = 2 and o.orderCompletionDate between @date_start and @date_end
and o.order_type in (1,0)
group by t.id
order by t.name) T