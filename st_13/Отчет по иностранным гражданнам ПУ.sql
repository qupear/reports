set @date_start = '2019-01-01';
set @date_end = '2019-08-10';
DROP TEMPORARY TABLE IF EXISTS foreign_cases;
CREATE TEMPORARY TABLE IF NOT EXISTS foreign_cases
SELECT c.id_case, c.id_patient, c.date_begin,  vo.country_short_name
		FROM cases c
	JOIN patient_data pd ON c.id_patient = pd.id_patient
	JOIN vmu_oksm vo ON vo.code = pd.c_oksm AND vo.code IS NOT NULL AND vo.code <> 643
		WHERE c.date_begin BETWEEN @date_start AND @date_end;
SELECT '����������������� ������' AS txt,
       CASE cs.is_oms
	       WHEN 0
		       THEN '��'
	       ELSE '���'
       END omspu,
       CASE cs.id_net_profile
	       WHEN 1
		       THEN '�'
	       ELSE '�'
       END net,
       fc.country_short_name AS strana, count(DISTINCT fc.id_case) AS pos,
       COUNT(DISTINCT fc.id_patient) AS ppl
		, SUM(COALESCE(p.cost, 0) + COALESCE(t.price, 0)) AS plata
	FROM
		case_services                                          cs
			JOIN
			foreign_cases                                      fc ON fc.id_case = cs.id_case
			LEFT JOIN
			price                                              p ON p.id = cs.id_profile AND cs.is_oms = 0
			LEFT JOIN
			(
				SELECT vt.uet,
				       vt.id_profile,
				       vt.id_zone_type,
				       vt.price,
				       vt.tariff_begin_date AS d1,
				       vt.tariff_end_date AS d2,
				       vtp.tp_begin_date AS d3,
				       vtp.tp_end_date AS d4
					FROM
						vmu_tariff      vt,
						vmu_tariff_plan vtp
					WHERE vtp.id_tariff_group = vt.id_lpu
					  AND vtp.id_lpu IN (
						                    SELECT id_lpu
							                    FROM
								                    mu_ident)) t ON t.id_profile = cs.id_profile
				AND cs.date_begin BETWEEN t.d1 AND t.d2
				AND cs.date_begin BETWEEN t.d3 AND t.d4
				AND t.id_zone_type = cs.id_net_profile
				AND cs.is_oms = 1
	GROUP BY fc.country_short_name, cs.is_oms, cs.id_net_profile
	ORDER BY fc.country_short_name, cs.is_oms;

