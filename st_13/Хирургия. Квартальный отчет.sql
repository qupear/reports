set @date_start = '2020-01-01';
set @date_end = '2020-04-30';
set @doc_id = 113;

drop temporary table if exists doctor_services, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, temp_3_5, temp_6, temp_7_8, temp_9;

CREATE TEMPORARY TABLE if NOT EXISTS doctor_services
SELECT cs.id_service, cs.date_begin, cs.ID_DOCTOR, u.name, cs.id_case, vp.profile_infis_code, p.code, vd.diagnosis_code, T.uet, cs.is_oms, pd.BIRTHDAY
FROM case_services cs
	LEFT JOIN cases c ON cs.id_case = c.id_case
	LEFT JOIN doctor_spec ds ON cs.id_doctor = ds.doctor_id
	Left join users u on u.id = ds.user_id
	LEFT JOIN patient_data pd ON c.ID_PATIENT = pd.id_patient
	LEFT JOIN vmu_profile vp ON vp.id_profile = cs.id_profile
	LEFT JOIN price p ON p.id = cs.id_profile
	LEFT JOIN vmu_diagnosis vd ON cs.id_diagnosis = vd.id_diagnosis
	LEFT JOIN
		(SELECT vt.uet, vt.id_profile, vt.id_zone_type,
			vt.tariff_begin_date AS d1, vt.tariff_end_date AS d2,
			vtp.tp_begin_date AS d3, vtp.tp_end_date AS d4
		FROM vmu_tariff vt, vmu_tariff_plan vtp
		WHERE vtp.id_tariff_group = vt.id_lpu
			AND vtp.id_lpu in (SELECT id_lpu FROM mu_ident)) T
				ON T.id_profile = vp.id_profile
					AND	cs.date_begin BETWEEN T.d1 AND T.d2
					AND cs.date_begin BETWEEN T.d3 AND T.d4
					AND T.id_zone_type = cs.id_net_profile
					AND cs.is_oms = 1
WHERE (cs.id_doctor = @doc_id or @doc_id = 0)
	AND ds.spec_id = 108
	AND cs.date_begin BETWEEN @date_start AND @date_end;

/* 4.Посещений всего */
CREATE TEMPORARY TABLE IF NOT EXISTS t1
SELECT id_doctor, count( id_case) AS val
FROM doctor_services ds
WHERE profile_infis_code IN	('стх001', 'стх002')
GROUP BY id_doctor;

/* 5.Из них первичных */
CREATE TEMPORARY TABLE IF NOT EXISTS t2
SELECT id_doctor, count(id_case) AS val
FROM doctor_services ds
WHERE profile_infis_code IN	('стх001')
GROUP BY id_doctor;

/* Для протоколов столбца 6 и 8 */
create temporary table if not exists temp_3_5 (
	SELECT date_begin, ds.id_case
	FROM doctor_services ds
	left join protocols p on p.id_case = ds.id_case
	WHERE profile_infis_code in ('стх030') and child_ref_ids like '%168530%'
	ORDER BY date_begin);

/* 6.Удалено зубов у взр. Постоянных */
CREATE TEMPORARY TABLE IF NOT EXISTS t3
SELECT id_doctor, count( id_case) AS val
FROM doctor_services ds
WHERE (profile_infis_code IN ('стх031', 'стх034')
	or (profile_infis_code IN ('стх030') and id_case not in (select id_case from temp_3_5)))
	and date_add(ds.birthday, interval 18 year) < ds.date_begin
GROUP BY id_doctor;

/* 7.Удалено зубов у взр. Молочных */
CREATE TEMPORARY TABLE IF NOT EXISTS t4
SELECT id_doctor, count(id_case) AS val
FROM doctor_services ds
WHERE profile_infis_code IN	('стх029')
	and date_add(ds.birthday, interval 18 year) < ds.date_begin
GROUP BY id_doctor;

/* 8.Удалено зубов у детей. Постоянных. По поводу осложнённого кариеса */
CREATE TEMPORARY TABLE IF NOT EXISTS t5
SELECT id_doctor, count(id_case) AS val
FROM doctor_services ds
WHERE (profile_infis_code IN ('стх031', 'стх034')
	or (profile_infis_code IN ('стх030') and id_case not in (select id_case from temp_3_5)))
	and date_add(ds.birthday, interval 18 year) > ds.date_begin
GROUP BY id_doctor;

/* Для протоколов столбца 9 */
create temporary table if not exists temp_6 (
	SELECT date_begin, ds.id_case
	FROM doctor_services ds
	left join protocols p on p.id_case = ds.id_case
	WHERE profile_infis_code in ('стх030', 'стх031', 'стх034') and child_ref_ids regexp '165650|165670|165690|165710|165730|165750|165770|165790|165810'
	ORDER BY date_begin);

/* 9.Удалено зубов у детей. Постоянных. По ортодонтическим показ. */
CREATE TEMPORARY TABLE IF NOT EXISTS t6
SELECT id_doctor, count(id_case) AS val
FROM doctor_services ds
WHERE profile_infis_code IN	('стх030', 'стх031', 'стх034') and (id_case in (select id_case from temp_6))
	and date_add(ds.birthday, interval 18 year) > ds.date_begin
GROUP BY id_doctor;

/* Для протоколов столбца 10 и 11 */
create temporary table if not exists temp_7_8 (
	SELECT date_begin, ds.id_case
	FROM doctor_services ds
	left join protocols p on p.id_case = ds.id_case
	WHERE profile_infis_code in ('стх029') and child_ref_ids like '%166030%'
	ORDER BY date_begin);

/* 10.Удалено зубов у детей. Молочных. По поводу осложнённого кариеса */
CREATE TEMPORARY TABLE IF NOT EXISTS t7
SELECT id_doctor, count(id_case) AS val
FROM doctor_services ds
WHERE profile_infis_code IN	('стх029') and diagnosis_code IN ('K04.5') and (id_case not in (select id_case from temp_7_8))
	and date_add(ds.birthday, interval 18 year) > ds.date_begin
GROUP BY id_doctor;

/* 11.Удалено зубов у детей. Молочных. Физиологическая резорбция корней */
CREATE TEMPORARY TABLE IF NOT EXISTS t8
SELECT id_doctor, count(id_case) AS val
FROM doctor_services ds
WHERE profile_infis_code IN	('стх029') and diagnosis_code IN ('K04.5') and (id_case in (select id_case from temp_7_8))
	and date_add(ds.birthday, interval 18 year) > ds.date_begin
GROUP BY id_doctor;

/* Для протоколов столбца 12 */
create temporary table if not exists temp_9
	SELECT date_begin, ds.id_case
	FROM doctor_services ds
	left join protocols p on p.id_case = ds.id_case
	WHERE profile_infis_code in ('стх021', 'стх022', 'стх023', 'стх034', 'стх039', 'стх050') and child_ref_ids regexp '107570|107668|109990|110683|111128'
	ORDER BY date_begin;

/* 12.Выполнено операций */
CREATE TEMPORARY TABLE IF NOT EXISTS t9
SELECT id_doctor, count(id_case) AS val
FROM doctor_services ds
WHERE (profile_infis_code IN ('стх033', 'стх047', 'стх048')
	or (profile_infis_code IN ('стх021', 'стх022', 'стх023', 'стх034', 'стх039', 'стх050') and id_case in (select id_case from temp_9)))
GROUP BY id_doctor;

/* 13.УЕТ */
CREATE TEMPORARY TABLE IF NOT EXISTS t10
select id_doctor, ROUND(COALESCE(SUM(UET),0), 2) AS uet  from (
SELECT *
FROM doctor_services ds
GROUP BY id_doctor, id_service) as fromm group by fromm.id_doctor;

SELECT
    name, '' as 'Количество смен', '' as 'Количество часов',
    COALESCE(t1.val, 0), COALESCE(t2.val, 0), COALESCE(t3.val, 0),
    COALESCE(t4.val, 0), COALESCE(t5.val, 0), COALESCE(t6.val, 0),
    COALESCE(t7.val, 0), COALESCE(t8.val, 0), COALESCE(t9.val, 0),
    COALESCE(t10.uet, 0)
FROM
    doctor_services ds
    LEFT JOIN t1 ON ds.id_doctor = t1.id_doctor
	LEFT JOIN t2 ON ds.id_doctor = t2.id_doctor
	LEFT JOIN t3 ON ds.id_doctor = t3.id_doctor
	LEFT JOIN t4 ON ds.id_doctor = t4.id_doctor
	LEFT JOIN t5 ON ds.id_doctor = t5.id_doctor
	LEFT JOIN t6 ON ds.id_doctor = t6.id_doctor
	LEFT JOIN t7 ON ds.id_doctor = t7.id_doctor
	LEFT JOIN t8 ON ds.id_doctor = t8.id_doctor
	LEFT JOIN t9 ON ds.id_doctor = t9.id_doctor
	LEFT JOIN t10 ON ds.id_doctor = t10.id_doctor
group by ds.ID_DOCTOR
ORDER BY ds.name;