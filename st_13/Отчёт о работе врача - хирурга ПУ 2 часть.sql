set @date_start = '2020-01-10';
set @date_end = '2020-01-15';
set @doc_id = 0;

drop temporary table if exists doctor_services,  first_row, t24,t25,t26,t27,t28,t29,t30,t31,t32,t33,t34,t35,t36,t37,t38,t39,t40,t41,t42,t43,t44,t45,t46,t47,t48;


CREATE TEMPORARY TABLE if NOT EXISTS doctor_services
SELECT cs.id_service, cs.date_begin, cs.ID_DOCTOR, u.name, cs.id_case, p.code, p.cost, p.date_end as s_end, vd.diagnosis_code, cs.is_oms, pd.BIRTHDAY
FROM case_services cs
	LEFT JOIN cases c ON cs.id_case = c.id_case
	LEFT JOIN doctor_spec ds ON cs.id_doctor = ds.doctor_id
	Left join users u on u.id = ds.user_id
	LEFT JOIN patient_data pd ON c.ID_PATIENT = pd.id_patient
	LEFT JOIN price p ON p.id = cs.id_profile
	LEFT JOIN vmu_diagnosis vd ON cs.id_diagnosis = vd.id_diagnosis
WHERE (cs.id_doctor = @doc_id or @doc_id = 0)
	AND ds.spec_id = 108
	and cs.is_oms in (0, 3, 4)
	and cs.id_order = -1
	AND cs.date_begin BETWEEN @date_start AND @date_end;

set @row = 0;

CREATE TEMPORARY TABLE if NOT EXISTS first_row (
	SELECT @ROW := @ROW + 1 AS st_num, T.date_begin AS st_date
	FROM (SELECT date_begin FROM doctor_services ds
			GROUP BY date_begin
			ORDER BY date_begin)T);

/* 30.Пластики уздечки верхней  губы (под местной анестезией) */
CREATE TEMPORARY TABLE IF NOT EXISTS t24
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('115')
GROUP BY date_begin
ORDER BY date_begin;

/* 31.Пластики уздечки нижней губы (под местной анестезией) */
CREATE TEMPORARY TABLE IF NOT EXISTS t25
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('115/а')
GROUP BY date_begin
ORDER BY date_begin;

/* 32.Пластики уздечки языка (под местной анестезией) */
CREATE TEMPORARY TABLE IF NOT EXISTS t26
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('116')
GROUP BY date_begin
ORDER BY date_begin;

/* 17.Иссечение новообразований мягких тканей */
CREATE TEMPORARY TABLE IF NOT EXISTS t27
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('102')
GROUP BY date_begin
ORDER BY date_begin;


/* 28.Остеотомия челюсти (освобождение коронковой части зуба) */
CREATE TEMPORARY TABLE IF NOT EXISTS t28
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('113')
GROUP BY date_begin
ORDER BY date_begin;

/* 7.Местная анестезия ("Лидокаин") */
CREATE TEMPORARY TABLE IF NOT EXISTS t29
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('91')
GROUP BY date_begin
ORDER BY date_begin;

/* 13.Удаление постоянного зуба ("Piezotome") */
create temporary table if not exists t30
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('97')
GROUP BY date_begin
ORDER BY date_begin;

/* 15.Наложение повязки при операциях в ЧЛО */
CREATE TEMPORARY TABLE IF NOT EXISTS t31
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('100')
GROUP BY date_begin
ORDER BY date_begin;

/* 16.Снятие шины с одной челюсти ( снятие шин с одной челюсти) */
CREATE TEMPORARY TABLE IF NOT EXISTS t32
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('101')
GROUP BY date_begin
ORDER BY date_begin;

/* 29.Остеотомия челюсти (хирургическое удлинение коронковой части зуба, «Piezotome»)*/
CREATE TEMPORARY TABLE IF NOT EXISTS t33
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('114')
GROUP BY date_begin
ORDER BY date_begin;

/* 18.Промывание протока слюнной железы */
CREATE TEMPORARY TABLE IF NOT EXISTS t34
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('103')
GROUP BY date_begin
ORDER BY date_begin;

/* 19.Наложение иммобилизационной повязки при вывихах зубов */
CREATE TEMPORARY TABLE IF NOT EXISTS t35
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('104')
GROUP BY date_begin
ORDER BY date_begin;

/* 22.Отсроченный кюретаж лунки удаленного зуба */
CREATE TEMPORARY TABLE IF NOT EXISTS t36
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('107')
GROUP BY date_begin
ORDER BY date_begin;

/* 39.Коррекция объема и формы альвеолярного отростка (использование костно-заменяющего материала) */
CREATE TEMPORARY TABLE IF NOT EXISTS t37
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('123')
GROUP BY date_begin
ORDER BY date_begin;

/* 40.Коррекция объема и формы альвеолярного отростка (использование остеопластического материала) */
CREATE TEMPORARY TABLE IF NOT EXISTS t38
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('124')
GROUP BY date_begin
ORDER BY date_begin;

/* 41.Коррекция объема и формы альвеолярного отростка (мембраны размером 15х20мм) */
CREATE TEMPORARY TABLE IF NOT EXISTS t39
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('125')
GROUP BY date_begin
ORDER BY date_begin;

/* 26.Цистотомия или цистэктомия (без резекции верхушки корня, под местной анестезией) */
CREATE TEMPORARY TABLE IF NOT EXISTS t40
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('111')
GROUP BY date_begin
ORDER BY date_begin;

/* 33.Гемисекция зуба (под местной анестезией) */
CREATE TEMPORARY TABLE IF NOT EXISTS t41
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('117')
GROUP BY date_begin
ORDER BY date_begin;

/* 34.Коррекция объема и формы альвеолярного отростка */
CREATE TEMPORARY TABLE IF NOT EXISTS t42
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('118')
GROUP BY date_begin
ORDER BY date_begin;

/* 10.Местная анестезия (приинфильтрационной, проводниковой анестезии) */
CREATE TEMPORARY TABLE IF NOT EXISTS t43
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('94')
GROUP BY date_begin
ORDER BY date_begin;

/* 6.Местная анестезия ("Новокаин") */
CREATE TEMPORARY TABLE IF NOT EXISTS t44
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('90')
GROUP BY date_begin
ORDER BY date_begin;


/* 43.Коррекция объема и формы альвеолярного отростка (мембраны размером 20х30мм) */
CREATE TEMPORARY TABLE IF NOT EXISTS t45
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('127')
GROUP BY date_begin
ORDER BY date_begin;

/* 44.Коррекция объема и формы альвеолярного отростка (мембраны размером 30х40мм) */
CREATE TEMPORARY TABLE IF NOT EXISTS t46
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('128')
GROUP BY date_begin
ORDER BY date_begin;

/* 46.Синус – лифтинг (костная пластика, остеопластика) («Piezotome») */
CREATE TEMPORARY TABLE IF NOT EXISTS t47
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('130')
GROUP BY date_begin
ORDER BY date_begin;

/* 50.Всего? */
CREATE TEMPORARY TABLE IF NOT EXISTS t48
SELECT date_begin, sum(ds.cost) AS val
FROM doctor_services ds
GROUP BY date_begin
ORDER BY date_begin;

select
	fr.st_num, fr.st_date,
    coalesce(t24.val, 0), coalesce(t25.val, 0),
    coalesce(t26.val, 0), coalesce(t27.val, 0),
    coalesce(t28.val, 0), coalesce(t29.val, 0),
    coalesce(t30.val, 0), coalesce(t31.val, 0),
    coalesce(t32.val, 0), coalesce(t33.val, 0),
    coalesce(t34.val, 0), coalesce(t35.val, 0),
	coalesce(t36.val, 0), coalesce(t37.val, 0),
    coalesce(t38.val, 0), coalesce(t39.val, 0),
    coalesce(t40.val, 0), coalesce(t41.val, 0),
	coalesce(t42.val, 0), coalesce(t43.val, 0),
    coalesce(t44.val, 0), coalesce(t45.val, 0),
    coalesce(t46.val, 0), coalesce(t47.val, 0),
    coalesce(t48.val, 0)
from first_row fr
	left join t24 on t24.date_begin = fr.st_date
	left join t25 on t25.date_begin = fr.st_date
	left join t26 on t26.date_begin = fr.st_date	
	left join t27 on t27.date_begin = fr.st_date
	left join t28 on t28.date_begin = fr.st_date
	left join t29 on t29.date_begin = fr.st_date
	left join t30 on t30.date_begin = fr.st_date
	left join t31 on t31.date_begin = fr.st_date
	left join t32 on t32.date_begin = fr.st_date
	left join t33 on t33.date_begin = fr.st_date
	left join t34 on t34.date_begin = fr.st_date
	left join t35 on t35.date_begin = fr.st_date
	left join t36 on t36.date_begin = fr.st_date
	left join t37 on t37.date_begin = fr.st_date
	left join t38 on t38.date_begin = fr.st_date
	left join t39 on t39.date_begin = fr.st_date
	left join t40 on t40.date_begin = fr.st_date
	left join t41 on t41.date_begin = fr.st_date
	left join t42 on t42.date_begin = fr.st_date
	left join t43 on t43.date_begin = fr.st_date
	left join t44 on t44.date_begin = fr.st_date
	left join t45 on t45.date_begin = fr.st_date
	left join t46 on t46.date_begin = fr.st_date
	left join t47 on t47.date_begin = fr.st_date
	left join t48 on t48.date_begin = fr.st_date
order by st_num;