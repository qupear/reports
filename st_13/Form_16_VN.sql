set @date_start = '2020-01-09';
set @date_end = '2020-02-26';

drop temporary table if exists all_data, C00_C97_m,C00_C97_f,C00_D48_f,C00_D48_m,S00_T98_f,S00_T98_m,K00_K93_f,K00_K93_m;


create temporary table if not exists all_data
select
		sum(p.value) as value,
		c.id_case,
		c.id_patient,
		pd.sex,
		c.ID_DIAGNOSIS,
		case
			when ((pd.birthday + interval 15 YEAR - interval 1 DAY) < c.date_end and (pd.birthday + interval 20 year) > c.date_end) then 1
			else 0
		end as a15_19,
		case
			when ((pd.birthday + interval 20 YEAR - interval 1 DAY) < c.date_end and (pd.birthday + interval 25 year) > c.date_end) then 1
			else 0
		end as a20_24,
		case
			when ((pd.birthday + interval 25 YEAR - interval 1 DAY) < c.date_end and (pd.birthday + interval 30 year) > c.date_end) then 1
			else 0
		end as a25_29,
		case
			when ((pd.birthday + interval 30 YEAR - interval 1 DAY) < c.date_end and (pd.birthday + interval 35 year) > c.date_end) then 1
			else 0
		end as a30_34,
		case
			when ((pd.birthday + interval 35 YEAR - interval 1 DAY) < c.date_end and (pd.birthday + interval 40 year) > c.date_end) then 1
			else 0
		end as a35_39,
		case
			when ((pd.birthday + interval 40 YEAR - interval 1 DAY) < c.date_end and (pd.birthday + interval 45 year) > c.date_end) then 1
			else 0
		end as a40_44,
		case
			when ((pd.birthday + interval 45 YEAR - interval 1 DAY) < c.date_end and (pd.birthday + interval 50 year) > c.date_end) then 1
			else 0
		end as a45_49,
		case
			when ((pd.birthday + interval 50 YEAR - interval 1 DAY) < c.date_end and (pd.birthday + interval 55 year) > c.date_end) then 1
			else 0
		end as a50_54,
		case
			when ((pd.birthday + interval 55 YEAR - interval 1 DAY) < c.date_end and (pd.birthday + interval 60 year) > c.date_end) then 1
			else 0
		end as a55_59,
		case
			when ((birthday + interval 60 year - interval 1 day) < c.date_end) then 1
			else 0
		end as a60_older
	from
		cases c
	left join protocols p on p.id_case = c.id_case
	join patient_data pd on pd.ID_PATIENT = c.ID_PATIENT
	join vmu_diagnosis vd on vd.ID_DIAGNOSIS = c.ID_DIAGNOSIS
	where
	/* Данные протоеолы для нашей базы */
		p.id_ref in (88582,88583,88584,88585)
	/* Протоколы для 13 поликлиники */
		p.id_ref in (117424,117426,117428,117429)
		and c.DATE_BEGIN between @date_start and @date_end
group by c.id_case;

/* Новообразования C00-D48 */
create temporary table if not exists C00_D48_f
select 
	sum(value) as value,
	count(distinct id_case) count_id_case,
	id_patient,
	ad.ID_DIAGNOSIS,
	case when (a15_19 > 0) then count(distinct id_patient) end as a15_19,
	case when (a20_24 > 0) then count(distinct id_patient) end as a20_24,
	case when (a25_29 > 0) then count(distinct id_patient) end as a25_29,
	case when (a30_34 > 0) then count(distinct id_patient) end as a30_34,
	case when (a35_39 > 0) then count(distinct id_patient) end as a35_39,
	case when (a40_44 > 0) then count(distinct id_patient) end as a40_44,
	case when (a45_49 > 0) then count(distinct id_patient) end as a45_49,
	case when (a50_54 > 0) then count(distinct id_patient) end as a50_54,
	case when (a55_59 > 0) then count(distinct id_patient) end as a55_59,
	case when (a60_older > 0) then count(distinct id_patient) end as a60_older
from
	all_data ad
join vmu_diagnosis vd on vd.ID_DIAGNOSIS = ad.ID_DIAGNOSIS
where 
	sex = 'ж'
	and vd.DIAGNOSIS_CODE >= 'C00' and vd.DIAGNOSIS_CODE < 'D49';

set @s1_0 = 'Новообразования';
set @s1_1 = 'C00-D48';
set @s1_2 = 'ж';
set @s1_3 = (select	value	from C00_D48_f);
set @s1_4 = (select	count_id_case	from C00_D48_f);
set @s1_5 = (select a15_19	from C00_D48_f);
set @s1_6 = (select a20_24	from C00_D48_f);
set @s1_7 = (select a25_29	from C00_D48_f);
set @s1_8 = (select a30_34	from C00_D48_f);
set @s1_9 = (select a35_39	from C00_D48_f);
set @s1_10 = (select a40_44	from C00_D48_f);
set @s1_11 = (select a45_49	from C00_D48_f);
set @s1_12 = (select a50_54	from C00_D48_f);
set @s1_13 = (select a55_59	from C00_D48_f);
set @s1_14 = (select a60_older	from C00_D48_f);

create temporary table if not exists C00_D48_m
select 
	sum(value) as value,
	count(distinct id_case) count_id_case,
	id_patient,
	ad.ID_DIAGNOSIS,
	case when (a15_19 > 0) then count(distinct id_patient) end as a15_19,
	case when (a20_24 > 0) then count(distinct id_patient) end as a20_24,
	case when (a25_29 > 0) then count(distinct id_patient) end as a25_29,
	case when (a30_34 > 0) then count(distinct id_patient) end as a30_34,
	case when (a35_39 > 0) then count(distinct id_patient) end as a35_39,
	case when (a40_44 > 0) then count(distinct id_patient) end as a40_44,
	case when (a45_49 > 0) then count(distinct id_patient) end as a45_49,
	case when (a50_54 > 0) then count(distinct id_patient) end as a50_54,
	case when (a55_59 > 0) then count(distinct id_patient) end as a55_59,
	case when (a60_older > 0) then count(distinct id_patient) end as a60_older
from
	all_data ad
join vmu_diagnosis vd on vd.ID_DIAGNOSIS = ad.ID_DIAGNOSIS
where 
	sex = 'м'
	and vd.DIAGNOSIS_CODE >= 'C00' and vd.DIAGNOSIS_CODE < 'D49';

set @s2_0 = 'Новообразования';
set @s2_1 = 'C00-D48';
set @s2_2 = 'м';
set @s2_3 = (select	value	from C00_D48_m);
set @s2_4 = (select	count_id_case	from C00_D48_m);
set @s2_5 = (select a15_19	from C00_D48_m);
set @s2_6 = (select a20_24	from C00_D48_m);
set @s2_7 = (select a25_29	from C00_D48_m);
set @s2_8 = (select a30_34	from C00_D48_m);
set @s2_9 = (select a35_39	from C00_D48_m);
set @s2_10 = (select a40_44	from C00_D48_m);
set @s2_11 = (select a45_49	from C00_D48_m);
set @s2_12 = (select a50_54	from C00_D48_m);
set @s2_13 = (select a55_59	from C00_D48_m);
set @s2_14 = (select a60_older	from C00_D48_m);

/* из них: злокачественные новообразования C00-C97 */

create temporary table if not exists C00_C97_f
select 
	sum(value) as value,
	count(distinct id_case) count_id_case,
	id_patient,
	ad.ID_DIAGNOSIS,
	case when (a15_19 > 0) then count(distinct id_patient) end as a15_19,
	case when (a20_24 > 0) then count(distinct id_patient) end as a20_24,
	case when (a25_29 > 0) then count(distinct id_patient) end as a25_29,
	case when (a30_34 > 0) then count(distinct id_patient) end as a30_34,
	case when (a35_39 > 0) then count(distinct id_patient) end as a35_39,
	case when (a40_44 > 0) then count(distinct id_patient) end as a40_44,
	case when (a45_49 > 0) then count(distinct id_patient) end as a45_49,
	case when (a50_54 > 0) then count(distinct id_patient) end as a50_54,
	case when (a55_59 > 0) then count(distinct id_patient) end as a55_59,
	case when (a60_older > 0) then count(distinct id_patient) end as a60_older
from
	all_data ad
join vmu_diagnosis vd on vd.ID_DIAGNOSIS = ad.ID_DIAGNOSIS
where 
	sex = 'ж'
	and vd.DIAGNOSIS_CODE >= 'C00' and vd.DIAGNOSIS_CODE < 'D00';

set @s3_0 = ' из них: злокачественные образования';
set @s3_1 = 'C00-C97';
set @s3_2 = 'ж';
set @s3_3 = (select	value	from C00_C97_f);
set @s3_4 = (select	count_id_case from C00_C97_f);
set @s3_5 = (select a15_19	from C00_C97_f);
set @s3_6 = (select a20_24	from C00_C97_f);
set @s3_7 = (select a25_29	from C00_C97_f);
set @s3_8 = (select a30_34	from C00_C97_f);
set @s3_9 = (select a35_39	from C00_C97_f);
set @s3_10 = (select a40_44	from C00_C97_f);
set @s3_11 = (select a45_49	from C00_C97_f);
set @s3_12 = (select a50_54	from C00_C97_f);
set @s3_13 = (select a55_59	from C00_C97_f);
set @s3_14 = (select a60_older	from C00_C97_f);

create temporary table if not exists C00_C97_m
select 
	sum(value) as value,
	count(distinct id_case) count_id_case,
	id_patient,
	ad.ID_DIAGNOSIS,
	case when (a15_19 > 0) then count(distinct id_patient) end as a15_19,
	case when (a20_24 > 0) then count(distinct id_patient) end as a20_24,
	case when (a25_29 > 0) then count(distinct id_patient) end as a25_29,
	case when (a30_34 > 0) then count(distinct id_patient) end as a30_34,
	case when (a35_39 > 0) then count(distinct id_patient) end as a35_39,
	case when (a40_44 > 0) then count(distinct id_patient) end as a40_44,
	case when (a45_49 > 0) then count(distinct id_patient) end as a45_49,
	case when (a50_54 > 0) then count(distinct id_patient) end as a50_54,
	case when (a55_59 > 0) then count(distinct id_patient) end as a55_59,
	case when (a60_older > 0) then count(distinct id_patient) end as a60_older
from
	all_data ad
join vmu_diagnosis vd on vd.ID_DIAGNOSIS = ad.ID_DIAGNOSIS
where 
	sex = 'м'
	and vd.DIAGNOSIS_CODE >= 'C00' and vd.DIAGNOSIS_CODE < 'D00';

set @s4_0 = ' из них: злокачественные образования';
set @s4_1 = 'C00-C97';
set @s4_2 = 'м';
set @s4_3 = (select value	from C00_C97_m);
set @s4_4 = (select count_id_case	from C00_C97_m);
set @s4_5 = (select a15_19	from C00_C97_m);
set @s4_6 = (select a20_24	from C00_C97_m);
set @s4_7 = (select a25_29	from C00_C97_m);
set @s4_8 = (select a30_34	from C00_C97_m);
set @s4_9 = (select a35_39	from C00_C97_m);
set @s4_10 = (select a40_44	from C00_C97_m);
set @s4_11 = (select a45_49	from C00_C97_m);
set @s4_12 = (select a50_54	from C00_C97_m);
set @s4_13 = (select a55_59	from C00_C97_m);
set @s4_14 = (select a60_older	from C00_C97_m);

/* Болезни органов пищеварения K00-K92, K93 */

create temporary table if not exists K00_K93_f
select 
	sum(value) as value,
	count(distinct id_case) count_id_case,
	id_patient,
	ad.ID_DIAGNOSIS,
	case when (a15_19 > 0) then count(distinct id_patient) end as a15_19,
	case when (a20_24 > 0) then count(distinct id_patient) end as a20_24,
	case when (a25_29 > 0) then count(distinct id_patient) end as a25_29,
	case when (a30_34 > 0) then count(distinct id_patient) end as a30_34,
	case when (a35_39 > 0) then count(distinct id_patient) end as a35_39,
	case when (a40_44 > 0) then count(distinct id_patient) end as a40_44,
	case when (a45_49 > 0) then count(distinct id_patient) end as a45_49,
	case when (a50_54 > 0) then count(distinct id_patient) end as a50_54,
	case when (a55_59 > 0) then count(distinct id_patient) end as a55_59,
	case when (a60_older > 0) then count(distinct id_patient) end as a60_older
from
	all_data ad
join vmu_diagnosis vd on vd.ID_DIAGNOSIS = ad.ID_DIAGNOSIS
where 
	sex = 'ж'
	and vd.DIAGNOSIS_CODE >= 'K00' and vd.DIAGNOSIS_CODE < 'L00';

set @s5_0 = 'Болезни органов пищеварения';
set @s5_1 = 'K00-K92, K93';
set @s5_2 = 'ж';
set @s5_3 = (select	value	from K00_K93_f);
set @s5_4 = (select	count_id_case	from K00_K93_f);
set @s5_5 = (select a15_19	from K00_K93_f);
set @s5_6 = (select a20_24	from K00_K93_f);
set @s5_7 = (select a25_29	from K00_K93_f);
set @s5_8 = (select a30_34	from K00_K93_f);
set @s5_9 = (select a35_39	from K00_K93_f);
set @s5_10 = (select a40_44	from K00_K93_f);
set @s5_11 = (select a45_49	from K00_K93_f);
set @s5_12 = (select a50_54	from K00_K93_f);
set @s5_13 = (select a55_59	from K00_K93_f);
set @s5_14 = (select a60_older	from K00_K93_f);

create temporary table if not exists K00_K93_m
select 
	sum(value) as value,
	count(distinct id_case) count_id_case,
	id_patient,
	ad.ID_DIAGNOSIS,
	case when (a15_19 > 0) then count(distinct id_patient) end as a15_19,
	case when (a20_24 > 0) then count(distinct id_patient) end as a20_24,
	case when (a25_29 > 0) then count(distinct id_patient) end as a25_29,
	case when (a30_34 > 0) then count(distinct id_patient) end as a30_34,
	case when (a35_39 > 0) then count(distinct id_patient) end as a35_39,
	case when (a40_44 > 0) then count(distinct id_patient) end as a40_44,
	case when (a45_49 > 0) then count(distinct id_patient) end as a45_49,
	case when (a50_54 > 0) then count(distinct id_patient) end as a50_54,
	case when (a55_59 > 0) then count(distinct id_patient) end as a55_59,
	case when (a60_older > 0) then count(distinct id_patient) end as a60_older
from
	all_data ad
join vmu_diagnosis vd on vd.ID_DIAGNOSIS = ad.ID_DIAGNOSIS
where 
	sex = 'м'
	and vd.DIAGNOSIS_CODE >= 'K00' and vd.DIAGNOSIS_CODE < 'L00';

set @s6_0 = 'Болезни органов пищеварения';
set @s6_1 = 'K00-K92, K93';
set @s6_2 = 'м';
set @s6_3 = (select	value	from K00_K93_m);
set @s6_4 = (select	count_id_case	from K00_K93_m);
set @s6_5 = (select a15_19	from K00_K93_m);
set @s6_6 = (select a20_24	from K00_K93_m);
set @s6_7 = (select a25_29	from K00_K93_m);
set @s6_8 = (select a30_34	from K00_K93_m);
set @s6_9 = (select a35_39	from K00_K93_m);
set @s6_10 = (select a40_44	from K00_K93_m);
set @s6_11 = (select a45_49	from K00_K93_m);
set @s6_12 = (select a50_54	from K00_K93_m);
set @s6_13 = (select a55_59	from K00_K93_m);
set @s6_14 = (select a60_older	from K00_K93_m);

/* Травмы, отравления и некоторые другие последствия воздействия внешних причин S00-T98 */

create temporary table if not exists S00_T98_f
select 
	sum(value) as value,
	count(distinct id_case) count_id_case,
	id_patient,
	ad.ID_DIAGNOSIS,
	case when (a15_19 > 0) then count(distinct id_patient) end as a15_19,
	case when (a20_24 > 0) then count(distinct id_patient) end as a20_24,
	case when (a25_29 > 0) then count(distinct id_patient) end as a25_29,
	case when (a30_34 > 0) then count(distinct id_patient) end as a30_34,
	case when (a35_39 > 0) then count(distinct id_patient) end as a35_39,
	case when (a40_44 > 0) then count(distinct id_patient) end as a40_44,
	case when (a45_49 > 0) then count(distinct id_patient) end as a45_49,
	case when (a50_54 > 0) then count(distinct id_patient) end as a50_54,
	case when (a55_59 > 0) then count(distinct id_patient) end as a55_59,
	case when (a60_older > 0) then count(distinct id_patient) end as a60_older
from
	all_data ad
join vmu_diagnosis vd on vd.ID_DIAGNOSIS = ad.ID_DIAGNOSIS
where 
	sex = 'ж'
	and vd.DIAGNOSIS_CODE >= 'S00' and vd.DIAGNOSIS_CODE < 'V00';

set @s7_0 = 'Травмы, отравления и некоторые другие последствия воздействия внешних причин';
set @s7_1 = 'S00-T98';
set @s7_2 = 'ж';
set @s7_3 = (select	value	from S00_T98_f);
set @s7_4 = (select	count_id_case	from S00_T98_f);
set @s7_5 = (select a15_19	from S00_T98_f);
set @s7_6 = (select a20_24	from S00_T98_f);
set @s7_7 = (select a25_29	from S00_T98_f);
set @s7_8 = (select a30_34	from S00_T98_f);
set @s7_9 = (select a35_39	from S00_T98_f);
set @s7_10 = (select a40_44	from S00_T98_f);
set @s7_11 = (select a45_49	from S00_T98_f);
set @s7_12 = (select a50_54	from S00_T98_f);
set @s7_13 = (select a55_59	from S00_T98_f);
set @s7_14 = (select a60_older	from S00_T98_f);

create temporary table if not exists S00_T98_m
select 
	sum(value) as value,
	count(distinct id_case) count_id_case,
	id_patient,
	ad.ID_DIAGNOSIS,
	case when (a15_19 > 0) then count(distinct id_patient) end as a15_19,
	case when (a20_24 > 0) then count(distinct id_patient) end as a20_24,
	case when (a25_29 > 0) then count(distinct id_patient) end as a25_29,
	case when (a30_34 > 0) then count(distinct id_patient) end as a30_34,
	case when (a35_39 > 0) then count(distinct id_patient) end as a35_39,
	case when (a40_44 > 0) then count(distinct id_patient) end as a40_44,
	case when (a45_49 > 0) then count(distinct id_patient) end as a45_49,
	case when (a50_54 > 0) then count(distinct id_patient) end as a50_54,
	case when (a55_59 > 0) then count(distinct id_patient) end as a55_59,
	case when (a60_older > 0) then count(distinct id_patient) end as a60_older
from
	all_data ad
join vmu_diagnosis vd on vd.ID_DIAGNOSIS = ad.ID_DIAGNOSIS
where 
	sex = 'м'
	and vd.DIAGNOSIS_CODE >= 'S00' and vd.DIAGNOSIS_CODE < 'V00';

set @s8_0 = 'Травмы, отравления и некоторые другие последствия воздействия внешних причин';
set @s8_1 = 'S00-T98';
set @s8_2 = 'м';
set @s8_3 = (select	value	from S00_T98_m);
set @s8_4 = (select	count_id_case	from S00_T98_m);
set @s8_5 = (select a15_19	from S00_T98_m);
set @s8_6 = (select a20_24	from S00_T98_m);
set @s8_7 = (select a25_29	from S00_T98_m);
set @s8_8 = (select a30_34	from S00_T98_m);
set @s8_9 = (select a35_39	from S00_T98_m);
set @s8_10 = (select a40_44	from S00_T98_m);
set @s8_11 = (select a45_49	from S00_T98_m);
set @s8_12 = (select a50_54	from S00_T98_m);
set @s8_13 = (select a55_59	from S00_T98_m);
set @s8_14 = (select a60_older	from S00_T98_m);

/* ИТОГО ПО ВСЕМ ПРИЧИНАМ */

set @s9_0 = 'Всего по заболеваниям';
set @s9_1 = '';
set @s9_2 = 'ж';
set @s10_0 = 'Всего по заболеваниям';
set @s10_1 = '';
set @s10_2 = 'м';

/* C00-D48 ж */
select @s1_0, @s1_1, @s1_2, @s1_3, @s1_4, @s1_5, @s1_6, @s1_7, @s1_8, @s1_9, @s1_10, @s1_11, @s1_12, @s1_13, @s1_14
union
/* C00-D48 м */
select @s2_0, @s2_1, @s2_2, @s2_3, @s2_4, @s2_5, @s2_6, @s2_7, @s2_8, @s2_9, @s2_10, @s2_11, @s2_12, @s2_13, @s2_14
union
/* -- C00-C97 ж */
select @s3_0, @s3_1, @s3_2, @s3_3, @s3_4, @s3_5, @s3_6, @s3_7, @s3_8, @s3_9, @s3_10, @s3_11, @s3_12, @s3_13, @s3_14
union
/* C00-C97 м */
select @s4_0, @s4_1, @s4_2, @s4_3, @s4_4, @s4_5, @s4_6, @s4_7, @s4_8, @s4_9, @s4_10, @s4_11, @s4_12, @s4_13, @s4_14
union
/* K00-K92, K93 ж */
select @s5_0, @s5_1, @s5_2, @s5_3, @s5_4, @s5_5, @s5_6, @s5_7, @s5_8, @s5_9, @s5_10, @s5_11, @s5_12, @s5_13, @s5_14
union
/* K00-K92, K93 м */
select @s6_0, @s6_1, @s6_2, @s6_3, @s6_4, @s6_5, @s6_6, @s6_7, @s6_8, @s6_9, @s6_10, @s6_11, @s6_12, @s6_13, @s6_14
union
/* S00-T98 ж */
select @s7_0, @s7_1, @s7_2, @s7_3, @s7_4, @s7_5, @s7_6, @s7_7, @s7_8, @s7_9, @s7_10, @s7_11, @s7_12, @s7_13, @s7_14
union
/* S00-T98 м */
select @s8_0, @s8_1, @s8_2, @s8_3, @s8_4, @s8_5, @s8_6, @s8_7, @s8_8, @s8_9, @s8_10, @s8_11, @s8_12, @s8_13, @s8_14
union
/* Итого */
select @s9_0, @s9_1, @s9_2,
	(coalesce(@s1_3, 0) + coalesce(@s5_3, 0) + coalesce(@s7_3,0)),
	(coalesce(@s1_4, 0) + coalesce(@s5_4, 0) + coalesce(@s7_4, 0)),
	(coalesce(@s1_5, 0) + coalesce(@s5_5, 0) + coalesce(@s7_5, 0)),
	(coalesce(@s1_6, 0) + coalesce(@s5_6, 0) + coalesce(@s7_6, 0)),
	(coalesce(@s1_7, 0) + coalesce(@s5_7, 0) + coalesce(@s7_7, 0)),
	(coalesce(@s1_8, 0) + coalesce(@s5_8, 0) + coalesce(@s7_8, 0)),
	(coalesce(@s1_9, 0) + coalesce(@s5_9, 0) + coalesce(@s7_9, 0)),
	(coalesce(@s1_10, 0) + coalesce(@s5_10, 0) + coalesce(@s7_10, 0)),
	(coalesce(@s1_11, 0) + coalesce(@s5_11, 0) + coalesce(@s7_11, 0)),
	(coalesce(@s1_12, 0) + coalesce(@s5_12, 0) + coalesce(@s7_12, 0)),
	(coalesce(@s1_13, 0) + coalesce(@s5_13, 0) + coalesce(@s7_13, 0)),
	(coalesce(@s1_14, 0) + coalesce(@s5_14, 0) + coalesce(@s7_14, 0))
union
select @s10_0, @s10_1, @s10_2,
	(coalesce(@s2_3, 0) + coalesce(@s6_3, 0) + coalesce(@s8_3,0)),
	(coalesce(@s2_4, 0) + coalesce(@s6_4, 0) + coalesce(@s8_4, 0)),
	(coalesce(@s2_5, 0) + coalesce(@s6_5, 0) + coalesce(@s8_5, 0)),
	(coalesce(@s2_6, 0) + coalesce(@s6_6, 0) + coalesce(@s8_6, 0)),
	(coalesce(@s2_7, 0) + coalesce(@s6_7, 0) + coalesce(@s8_7, 0)),
	(coalesce(@s2_8, 0) + coalesce(@s6_8, 0) + coalesce(@s8_8, 0)),
	(coalesce(@s2_9, 0) + coalesce(@s6_9, 0) + coalesce(@s8_9, 0)),
	(coalesce(@s2_10, 0) + coalesce(@s6_10, 0) + coalesce(@s8_10, 0)),
	(coalesce(@s2_11, 0) + coalesce(@s6_11, 0) + coalesce(@s8_11, 0)),
	(coalesce(@s2_12, 0) + coalesce(@s6_12, 0) + coalesce(@s8_12, 0)),
	(coalesce(@s2_13, 0) + coalesce(@s6_13, 0) + coalesce(@s8_13, 0)),
	(coalesce(@s2_14, 0) + coalesce(@s6_14, 0) + coalesce(@s8_14, 0));