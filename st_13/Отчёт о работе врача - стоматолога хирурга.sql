set @date_start = '2020-01-01';
set @date_end = '2020-04-30';
set @doc_id = 113;

drop temporary table if exists T1, T2, T3,T4,T5,T6,T7,T8,T9,
T10,T11,T12,T13,T14,T15,T16,T17,T18,T19, T19_1,T20,T21,T22,T23,T24,T25,
T26,T27,T28,T29,T30,T31,T32,T33,T34,T35,T36,
t_all_1,doctor_services,first_row,patient_count;

CREATE TEMPORARY TABLE if NOT EXISTS doctor_services 
(SELECT cs.date_begin, cs.id_case, vp.profile_infis_code, p.code,
 vd.diagnosis_code, cs.tooth_name, T.uet, cs.is_oms, pd.birthday, CASE WHEN YEAR(CS.DATE_BEGIN) - YEAR(pd.BIRTHDAY)
 - (DATE_FORMAT(CS.DATE_BEGIN, '%m%d') < DATE_FORMAT(pd.BIRTHDAY, '%m%d')) >= 60 AND pd.SEX = 'м' THEN 0 WHEN YEAR(CS.DATE_BEGIN) - YEAR(pd.BIRTHDAY)
 - (DATE_FORMAT(CS.DATE_BEGIN, '%m%d') < DATE_FORMAT(pd.BIRTHDAY, '%m%d')) >= 55 AND pd.SEX = 'ж' THEN 1 ELSE -1 END AS old_people, 
pd.ID_PATIENT
	FROM case_services cs
	LEFT JOIN cases c ON cs.id_case = c.id_case
	LEFT JOIN patient_data pd ON c.ID_PATIENT = pd.id_patient
	LEFT JOIN doctor_spec ds ON ds.doctor_id = cs.id_doctor
	LEFT JOIN vmu_profile vp ON vp.id_profile = cs.id_profile AND cs.is_oms = 1
	LEFT JOIN price p ON p.id = cs.id_profile AND cs.is_oms in (0, 3)
	LEFT JOIN vmu_diagnosis vd ON cs.id_diagnosis = vd.id_diagnosis
	LEFT JOIN (
		SELECT vt.uet, vt.id_profile, vt.id_zone_type,
			vt.tariff_begin_date AS d1,
			vt.tariff_end_date AS d2,
			vtp.tp_begin_date AS d3,
			vtp.tp_end_date AS d4
		FROM vmu_tariff vt,
			vmu_tariff_plan vtp
		WHERE vtp.id_tariff_group = vt.id_lpu 
		AND vtp.id_lpu in (SELECT id_lpu FROM mu_ident)) T ON T.id_profile = vp.id_profile 
		AND cs.date_begin BETWEEN T.d1 AND T.d2 
		AND cs.date_begin BETWEEN T.d3 AND T.d4 
		AND	T.id_zone_type = cs.id_net_profile 
		AND cs.is_oms = 1
WHERE (cs.id_doctor = @doc_id OR @doc_id = 0) 
	AND ds.spec_id in (108) 
	AND cs.date_begin BETWEEN @date_start AND @date_end);

set @row = 0;

CREATE TEMPORARY TABLE if NOT EXISTS first_row (
	SELECT @ROW := @ROW + 1 AS st_num, T.date_begin AS st_date
	FROM (SELECT date_begin FROM doctor_services ds
			WHERE ds.is_oms = 1
			GROUP BY date_begin
			ORDER BY date_begin)T);

/*Количество пациентов*/
CREATE TEMPORARY TABLE if NOT EXISTS patient_count (
	select date_begin, count(distinct Id_patient) as val
	from doctor_services
	GROUP BY date_begin
	ORDER BY date_begin);

/*Первичный прием*/
CREATE TEMPORARY TABLE if NOT EXISTS T1 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх001')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Повторный прием*/
CREATE TEMPORARY TABLE if NOT EXISTS T2 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх002')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Аппликационная анестезия*/
CREATE TEMPORARY TABLE if NOT EXISTS T3 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('сто001')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Инфильтрационная анестезия*/
CREATE TEMPORARY TABLE if NOT EXISTS T4 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('сто002')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Анестезия проводниковая*/
CREATE TEMPORARY TABLE if NOT EXISTS T5 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('сто003')
	GROUP BY date_begin
	ORDER BY date_begin);
 
/*Удаление зуба простое*/
CREATE TEMPORARY TABLE if NOT EXISTS T6 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх030')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Удаление зуба сложное с разъединением корней*/
CREATE TEMPORARY TABLE if NOT EXISTS T7 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх031')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Операция удаления ретинированного, дистопированного, сверхкомплектного зуба*/
CREATE TEMPORARY TABLE if NOT EXISTS T8 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх034')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Вскрытие подслизистого или поднадкостничного очага воспаления полости рта*/
CREATE TEMPORARY TABLE if NOT EXISTS T9 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх015')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Отсроченный кюретаж лунки удаленного зуба*/
create temporary table if not exists T10 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх017')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Лечение перикоронита (промывание, рассечение, иссечение капюшона)*/
create temporary table if not exists T11 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх025')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Вскрытие и дренирование абсцесса полости рта*/
create temporary table if not exists T12 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх018')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Наложение  шва на слизистую оболочку*/
create temporary table if not exists T13 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх040')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Снятие послеоперационных швов (лигатур)*/
create temporary table if not exists T14 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх042')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Снятие шины с одной челюсти*/
create temporary table if not exists T15 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх003')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Бужирование протока слюнной железы*/
create temporary table if not exists T16 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх008')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Удаление камней из протоков слюнных желез*/
create temporary table if not exists T17 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх026')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Хирургическая обработка раны или инфицированной ткани*/
create temporary table if not exists T18 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх012')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Физиологическая резорбция корней protocol_id = 166030*/
create temporary table if not exists T19_1 (
	SELECT date_begin, ds.id_case
	FROM doctor_services ds
	left join protocols p on p.id_case = ds.id_case
	WHERE profile_infis_code in ('стх029') and child_ref_ids like '%166030%'
	ORDER BY date_begin);

/*Физиологическая резорбция корней*/
create temporary table if not exists T19 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх029') and diagnosis_code IN ('K04.5') and id_case in (select id_case from T19_1)
	and date_add(birthday, interval 18 year) > date_begin
	GROUP BY date_begin
	ORDER BY date_begin);

/*Удаление временного зуба*/
create temporary table if not exists T20 
(SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх029')
	and diagnosis_code IN ('K04.5') 
	and id_case not in (select id_case from T19_1)
	and date_add(birthday, interval 18 year) > date_begin
	GROUP BY date_begin
	ORDER BY date_begin);

/*Пластика уздечки верхней губы*/
create temporary table if not exists T21 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх021')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Пластика уздечки нижней губы)*/
create temporary table if not exists T22 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх022')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Пластика уздечки языка*/
create temporary table if not exists T23 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх023')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Пластика перфорации верхнечелюстной пазухи*/
create temporary table if not exists T24 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх046')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Резекция верхушки корня*/
create temporary table if not exists T25 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх048')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Иссечение новообразования мягких тканей*/
create temporary table if not exists T26 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх028')
	GROUP BY date_begin
	ORDER BY date_begin);
/*Цистотомия или цистэктомия*/
create temporary table if not exists T27 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх033')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Иссечение грануляций*/
create temporary table if not exists T28 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх039')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Остановка луночковго кровотечения без наложения швов методом тампонады*/
create temporary table if not exists T29 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх043')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Остановка луночковго кровотечения без наложения швовс использованием гемостатических материалов*/
create temporary table if not exists T30 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх044')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Наложение иммобилизационной повязки при при вывихах и подвывихах суставов*/
create temporary table if not exists T31 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх005')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Коррекция обьема и формы альвеолярного отростка*/
create temporary table if not exists T32 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх019')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Вправление вывиха сустава*/
create temporary table if not exists T33 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх037')
	GROUP BY date_begin
	ORDER BY date_begin);

/*Назначение лекарственных препаратов при заболеваниях полости рта и зубов*/
create temporary table if not exists T34 (
	SELECT date_begin, COUNT( id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('сто011')
	GROUP BY date_begin
	ORDER BY date_begin);

/*YET*/
create temporary table if not exists T35
    (select date_begin, ROUND(COALESCE(SUM(UET), 0), 2) as val
     from doctor_services
     group by date_begin
     order by date_begin);

create temporary table if not exists T36
    (SELECT date_begin, COUNT(id_case) AS val
	FROM doctor_services
	WHERE profile_infis_code in ('стх001', 'стх002', 'сто001', 'сто002', 'сто003', 'стх030',
		'стх031', 'стх034', 'стх015', 'стх017', 'стх025', 'стх018', 'стх040', 'стх042',
		'стх003', 'стх008', 'стх026', 'стх012', 'стх029', 'стх021', 'стх022', 'стх023',
		'стх046', 'стх048', 'стх028', 'стх033', 'стх039', 'стх043', 'стх044', 'стх005',
		'стх019', 'стх037', 'сто011')
	GROUP BY date_begin
	ORDER BY date_begin);

create temporary table if not exists t_all_1
select fr.st_num, fr.st_date, coalesce(pc.val, 0), coalesce(T1.val, 0) as 'стх001',
		coalesce(T2.val, 0), coalesce(T3.val, 0),
		coalesce(T4.val, 0), coalesce(T5.val, 0),
		coalesce(T6.val, 0), coalesce(T7.val, 0), 
		coalesce(T8.val, 0), coalesce(T9.val, 0), 
		coalesce(T10.val, 0), coalesce(T11.val, 0),
		coalesce(T12.val, 0), coalesce(T13.val, 0), 
		coalesce(T14.val, 0), coalesce(T15.val, 0),
		coalesce(T16.val, 0), coalesce(T17.val, 0)
from first_row fr
	left join patient_count pc on pc.date_begin = fr.st_date
	left join T1 on T1.date_begin = fr.st_date
	left join T2 on T2.date_begin = fr.st_date
	left join T3 on T3.date_begin = fr.st_date
	left join T4 on T4.date_begin = fr.st_date
	left join T5 on T5.date_begin = fr.st_date
	left join T6 on T6.date_begin = fr.st_date
	left join T7 on T7.date_begin = fr.st_date
	left join T8 on T8.date_begin = fr.st_date
	left join T9 on T9.date_begin = fr.st_date
	left join T10 on T10.date_begin = fr.st_date
	left join T11 on T11.date_begin = fr.st_date
	left join T12 on T12.date_begin = fr.st_date
	left join T13 on T13.date_begin = fr.st_date
	left join T14 on T14.date_begin = fr.st_date
	left join T15 on T15.date_begin = fr.st_date
	left join T16 on T16.date_begin = fr.st_date
	left join T17 on T17.date_begin = fr.st_date

order by st_num;

select 
    ta1 . *,
    coalesce(T18.val, 0),
    coalesce(T19.val, 0) as 'Физиологическая резорбция корней',
    coalesce(T20.val, 0) as 'Удаление временного зуба',
    coalesce(T21.val, 0),
    coalesce(T22.val, 0),
    coalesce(T23.val, 0),
    coalesce(T24.val, 0),
    coalesce(T25.val, 0),
    coalesce(T26.val, 0),
    coalesce(T27.val, 0),
    coalesce(T28.val, 0),
    coalesce(T29.val, 0),
    coalesce(T30.val, 0),
    coalesce(T31.val, 0),
    coalesce(T32.val, 0),
    coalesce(T33.val, 0),
    coalesce(T34.val, 0),
    coalesce(T35.val, 0),
	coalesce(T36.val, 0)
from
    t_all_1 ta1
        left join
    T18 ON T18.date_begin = ta1.st_date
        left join
    T19 ON T19.date_begin = ta1.st_date
        left join
    T20 ON T20.date_begin = ta1.st_date
        left join
    T21 ON T21.date_begin = ta1.st_date
        left join
    T22 ON T22.date_begin = ta1.st_date
        left join
    T23 ON T23.date_begin = ta1.st_date
        left join
    T24 ON T24.date_begin = ta1.st_date
        left join
    T25 ON T25.date_begin = ta1.st_date
        left join
    T26 ON T26.date_begin = ta1.st_date
        left join
    T27 ON T27.date_begin = ta1.st_date
        left join
    T28 ON T28.date_begin = ta1.st_date
        left join
    T29 ON T29.date_begin = ta1.st_date
		left join
    T30 ON T30.date_begin = ta1.st_date
        left join
    T31 ON T31.date_begin = ta1.st_date
        left join
    T32 ON T32.date_begin = ta1.st_date
        left join
    T33 ON T33.date_begin = ta1.st_date
        left join
    T34 ON T34.date_begin = ta1.st_date
        left join
    T35 ON T35.date_begin = ta1.st_date
        left join
    T36 ON T36.date_begin = ta1.st_date
order by st_num;