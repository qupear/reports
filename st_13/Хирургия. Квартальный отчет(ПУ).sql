set @date_start = '2020-01-01';
set @date_end = '2020-04-30';
set @doc_id = 113;

drop temporary table if exists doctor_services, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, temp_3_5, temp_6, temp_7_8, temp_9;

CREATE TEMPORARY TABLE if NOT EXISTS doctor_services
SELECT cs.id_service, cs.date_begin, cs.ID_DOCTOR, u.name, cs.id_case, p.code, vd.diagnosis_code, cs.is_oms, pd.BIRTHDAY
FROM case_services cs
	LEFT JOIN cases c ON cs.id_case = c.id_case
	LEFT JOIN doctor_spec ds ON cs.id_doctor = ds.doctor_id
	Left join users u on u.id = ds.user_id
	LEFT JOIN patient_data pd ON c.ID_PATIENT = pd.id_patient
	LEFT JOIN price p ON p.id = cs.id_profile
	LEFT JOIN vmu_diagnosis vd ON cs.id_diagnosis = vd.id_diagnosis

WHERE (cs.id_doctor = @doc_id or @doc_id = 0)
	AND ds.spec_id = 108
	and cs.is_oms in (0, 3, 4)
	AND cs.date_begin BETWEEN @date_start AND @date_end;

/* 4.Посещений всего */
CREATE TEMPORARY TABLE IF NOT EXISTS t1
SELECT id_doctor, count( id_case) AS val
FROM doctor_services ds
WHERE code IN	('88')
GROUP BY id_doctor;

/* 5.Из них первичных */
CREATE TEMPORARY TABLE IF NOT EXISTS t2
SELECT id_doctor, count(id_case) AS val
FROM doctor_services ds
WHERE code IN	('89')
GROUP BY id_doctor;

/* Для протоколов столбца 6 и 8 */
create temporary table if not exists temp_3_5 (
	SELECT date_begin, ds.id_case
	FROM doctor_services ds
	left join protocols p on p.id_case = ds.id_case
	WHERE code in ('96', '98', '105') and child_ref_ids regexp '165650|165670|165690|165710|165730|165750|165770|165790|165810'
	ORDER BY date_begin);

/* 6.Удалено зубов у взр. Постоянных */
CREATE TEMPORARY TABLE IF NOT EXISTS t3
SELECT id_doctor, count( id_case) AS val
FROM doctor_services ds
WHERE (code IN ('96', '98', '105'))
	and date_add(ds.birthday, interval 18 year) < ds.date_begin
GROUP BY id_doctor;

/* 7.Удалено зубов у взр. Молочных */
CREATE TEMPORARY TABLE IF NOT EXISTS t4
SELECT id_doctor, count(id_case) AS val
FROM doctor_services ds
WHERE code IN	('95')
	and date_add(ds.birthday, interval 18 year) < ds.date_begin
GROUP BY id_doctor;

/* 8.Удалено зубов у детей. Постоянных. По поводу осложнённого кариеса */
CREATE TEMPORARY TABLE IF NOT EXISTS t5
SELECT id_doctor, count(id_case) AS val
FROM doctor_services ds
WHERE (code IN ('96', '98')
	and (id_case not in (select id_case from temp_3_5)))
	and date_add(ds.birthday, interval 18 year) > ds.date_begin
GROUP BY id_doctor;

/* 9.Удалено зубов у детей. Постоянных. По ортодонтическим показ. */
CREATE TEMPORARY TABLE IF NOT EXISTS t6
SELECT ds.id_doctor, count(ds.id_case) AS val
FROM doctor_services ds
left join protocols p on p.id_case = ds.id_case
WHERE code in ('96', '98', '105') and child_ref_ids regexp '165650|165670|165690|165710|165730|165750|165770|165790|165810'
	and date_add(ds.birthday, interval 18 year) > ds.date_begin
GROUP BY ds.id_doctor;

/* 10.Удалено зубов у детей. Молочных. По поводу осложнённого кариеса */
CREATE TEMPORARY TABLE IF NOT EXISTS t7
SELECT id_doctor, count(id_case) AS val
FROM doctor_services ds
WHERE code IN	('95') and diagnosis_code IN ('K04.5')
	and date_add(ds.birthday, interval 18 year) > ds.date_begin
GROUP BY id_doctor;

/* 11.Удалено зубов у детей. Молочных. Физиологическая резорбция корней */
CREATE TEMPORARY TABLE IF NOT EXISTS t8
SELECT id_doctor, count(id_case) AS val
FROM doctor_services ds
WHERE code IN	('95') and diagnosis_code IN ('K04.5')
	and date_add(ds.birthday, interval 18 year) > ds.date_begin
GROUP BY id_doctor;

/* 12.Выполнено операций  */
create temporary table if not exists t9
	SELECT ds.id_doctor, count(ds.id_case) as val
	FROM doctor_services ds
	left join protocols p on p.id_case = ds.id_case
	WHERE code in ('70', '72', '73', '74', '75', '76', '86', '87', '102', '105', '108', '109', '113', '114', '115', '11', '116',
 '117', '118', '119', '120', '121', '122', '129', '132', '133') and child_ref_ids regexp '107570|107668|109990|110683|111128'
	GROUP BY ds.id_doctor;


/* 14.проведено анастезий */
CREATE TEMPORARY TABLE IF NOT EXISTS t10
SELECT id_doctor, count( id_case) AS val
FROM doctor_services ds
WHERE code IN	('93')
GROUP BY id_doctor;

SELECT
    name, '' as 'Количество смен', '' as 'Количество часов',
    COALESCE(t1.val, 0), COALESCE(t2.val, 0), COALESCE(t3.val, 0),
    COALESCE(t4.val, 0), COALESCE(t5.val, 0), COALESCE(t6.val, 0),
    COALESCE(t7.val, 0), COALESCE(t8.val, 0), COALESCE(t9.val, 0),
    '' as 'УЕТ', coalesce(t10.val, 0)
FROM
    doctor_services ds
    LEFT JOIN t1 ON ds.id_doctor = t1.id_doctor
	LEFT JOIN t2 ON ds.id_doctor = t2.id_doctor
	LEFT JOIN t3 ON ds.id_doctor = t3.id_doctor
	LEFT JOIN t4 ON ds.id_doctor = t4.id_doctor
	LEFT JOIN t5 ON ds.id_doctor = t5.id_doctor
	LEFT JOIN t6 ON ds.id_doctor = t6.id_doctor
	LEFT JOIN t7 ON ds.id_doctor = t7.id_doctor
	LEFT JOIN t8 ON ds.id_doctor = t8.id_doctor
	LEFT JOIN t9 ON ds.id_doctor = t9.id_doctor
	LEFT JOIN t10 ON ds.id_doctor = t10.id_doctor
group by ds.ID_DOCTOR
ORDER BY ds.name;