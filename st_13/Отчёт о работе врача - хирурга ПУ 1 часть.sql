set @date_start = '2020-01-10';
set @date_end = '2020-01-15';
set @doc_id = 0;

drop temporary table if exists doctor_services,  first_row, t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16,t17,t18,t19,t20,t21,t22,t23;

CREATE TEMPORARY TABLE if NOT EXISTS doctor_services
SELECT cs.id_service, cs.date_begin, cs.ID_DOCTOR, u.name, cs.id_case, p.code, p.cost, p.date_end as s_end, vd.diagnosis_code, cs.is_oms, pd.BIRTHDAY
FROM case_services cs
	LEFT JOIN cases c ON cs.id_case = c.id_case
	LEFT JOIN doctor_spec ds ON cs.id_doctor = ds.doctor_id
	Left join users u on u.id = ds.user_id
	LEFT JOIN patient_data pd ON c.ID_PATIENT = pd.id_patient
	LEFT JOIN price p ON p.id = cs.id_profile
	LEFT JOIN vmu_diagnosis vd ON cs.id_diagnosis = vd.id_diagnosis
WHERE (cs.id_doctor = @doc_id or @doc_id = 0)
	AND ds.spec_id = 108
	and cs.is_oms in (0, 3, 4)
	AND cs.date_begin BETWEEN @date_start AND @date_end;

set @row = 0;

CREATE TEMPORARY TABLE if NOT EXISTS first_row (
	SELECT @ROW := @ROW + 1 AS st_num, T.date_begin AS st_date
	FROM (SELECT date_begin FROM doctor_services ds
			GROUP BY date_begin
			ORDER BY date_begin)T);

/* 3.Количество пациентов*/
CREATE TEMPORARY TABLE IF NOT EXISTS t1
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('88','89')
GROUP BY date_begin
ORDER BY date_begin;

/* 8.Местная анестезия (импортного производства") */
CREATE TEMPORARY TABLE IF NOT EXISTS t2
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('92')
GROUP BY date_begin
ORDER BY date_begin;

/* 9.Местная анестезия (импортного производства, без учета стоимости) */
CREATE TEMPORARY TABLE IF NOT EXISTS t3
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('93')
GROUP BY date_begin
ORDER BY date_begin;

/* 11.Удаление временного зуба (под местной анестезией) */
CREATE TEMPORARY TABLE IF NOT EXISTS t4
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('95')
GROUP BY date_begin
ORDER BY date_begin;

/* 12.Удаление постоянного зуба (под местной анестезией) */
CREATE TEMPORARY TABLE IF NOT EXISTS t5
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('96')
GROUP BY date_begin
ORDER BY date_begin;

/* 14.Удаление зуба сложное с разъединением корней (под местной анестезией) */
create temporary table if not exists t6
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('98')
GROUP BY date_begin
ORDER BY date_begin;

/* 20.Операция удаления ретенированного, дистопированного или сверхкомплектного зуба */
CREATE TEMPORARY TABLE IF NOT EXISTS t7
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('105')
GROUP BY date_begin
ORDER BY date_begin;

/* 27.Лечение перикоронита */
CREATE TEMPORARY TABLE IF NOT EXISTS t8
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('112')
GROUP BY date_begin
ORDER BY date_begin;

/* 4.Из них первичных */
CREATE TEMPORARY TABLE IF NOT EXISTS t9
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('88')
GROUP BY date_begin
ORDER BY date_begin;

/* 5.Из них повторных */
CREATE TEMPORARY TABLE IF NOT EXISTS t10
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('89')
GROUP BY date_begin
ORDER BY date_begin;

/* 21.Вскрытие и дренирование одонтогенного абсцесса */
CREATE TEMPORARY TABLE IF NOT EXISTS t11
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('106')
GROUP BY date_begin
ORDER BY date_begin;

/* 36.Внутрикостная дентальная имплантация (установка имплантата импортной системы) */
CREATE TEMPORARY TABLE IF NOT EXISTS t12
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('120')
GROUP BY date_begin
ORDER BY date_begin;

/* 35.Внутрикостная дентальная имплантация (установка ФДМ) */
CREATE TEMPORARY TABLE IF NOT EXISTS t13
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('119')
GROUP BY date_begin
ORDER BY date_begin;

/* 49.Внутрикостная дентальная имплантация( установка миниимпланта ортодонтического под местной анестезией) */
CREATE TEMPORARY TABLE IF NOT EXISTS t14
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('133')
GROUP BY date_begin
ORDER BY date_begin;

/* 37.Внутрикостная дентальная имплантация (удаление ранее установленного имплантата) */
CREATE TEMPORARY TABLE IF NOT EXISTS t15
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('121')
GROUP BY date_begin
ORDER BY date_begin;

/* 47.Коррекция объема и формы альвеолярного отростка (использование пина/фиксирующего винта) */
CREATE TEMPORARY TABLE IF NOT EXISTS t16
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('131')
GROUP BY date_begin
ORDER BY date_begin;

/* 23.Резекция верхушки корня (с цистэктомией, под местной анестезией) */
CREATE TEMPORARY TABLE IF NOT EXISTS t17
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('108')
GROUP BY date_begin
ORDER BY date_begin;

/* 24.Резекция верхушки корня (с цистотомией, под местной анестезией) */
CREATE TEMPORARY TABLE IF NOT EXISTS t18
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('109')
GROUP BY date_begin
ORDER BY date_begin;

/* 25.Резекция верхушки корня (за резекцию каждого последующего зуба, под местной анестезией) */
CREATE TEMPORARY TABLE IF NOT EXISTS t19
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('110')
GROUP BY date_begin
ORDER BY date_begin;

/* 42.Коррекция объема и формы альвеолярного отростка (использование нерезорбируемой мембраны) */
CREATE TEMPORARY TABLE IF NOT EXISTS t20
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('126')
GROUP BY date_begin
ORDER BY date_begin;

/* 45.Синус – лифтинг (костная пластика, остеопластика) (костная пластика) */
CREATE TEMPORARY TABLE IF NOT EXISTS t21
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('129')
GROUP BY date_begin
ORDER BY date_begin;

/* 48.Синус - лифтинг (костная пластика, остеопластика) (закрытый синус - лифтинг) */
CREATE TEMPORARY TABLE IF NOT EXISTS t22
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('132')
GROUP BY date_begin
ORDER BY date_begin;

/* 38.Синус – лифтинг (костная пластика, остеопластика) (открытый синус – лифтинг) */
CREATE TEMPORARY TABLE IF NOT EXISTS t23
SELECT date_begin, count( id_case) AS val
FROM doctor_services ds
WHERE code IN ('122')
GROUP BY date_begin
ORDER BY date_begin;

select fr.st_num, fr.st_date, coalesce(t1.val, 0),
		coalesce(t9.val, 0), coalesce(t10.val, 0),
		coalesce(t2.val, 0), coalesce(t3.val, 0),
		coalesce(t4.val, 0), coalesce(t5.val, 0),
		coalesce(t6.val, 0), coalesce(t7.val, 0), 
		coalesce(t8.val, 0), coalesce(t11.val, 0),
		coalesce(t12.val, 0), coalesce(t13.val, 0), 
		coalesce(t14.val, 0), coalesce(t15.val, 0),
		coalesce(t16.val, 0), coalesce(t17.val, 0),
		coalesce(t18.val, 0), coalesce(t19.val, 0),
		coalesce(t20.val, 0), coalesce(t21.val, 0),
		coalesce(t22.val, 0), coalesce(t23.val, 0)
from first_row fr
	left join t1 on t1.date_begin = fr.st_date
	left join t2 on t2.date_begin = fr.st_date
	left join t3 on t3.date_begin = fr.st_date	
	left join t4 on t4.date_begin = fr.st_date
	left join t5 on t5.date_begin = fr.st_date
	left join t6 on t6.date_begin = fr.st_date
	left join t7 on t7.date_begin = fr.st_date
	left join t8 on t8.date_begin = fr.st_date
	left join t9 on t9.date_begin = fr.st_date
	left join t10 on t10.date_begin = fr.st_date
	left join t11 on t11.date_begin = fr.st_date
	left join t12 on t12.date_begin = fr.st_date
	left join t13 on t13.date_begin = fr.st_date
	left join t14 on t14.date_begin = fr.st_date
	left join t15 on t15.date_begin = fr.st_date
	left join t16 on t16.date_begin = fr.st_date
	left join t17 on t17.date_begin = fr.st_date
	left join t18 on t18.date_begin = fr.st_date
	left join t19 on t19.date_begin = fr.st_date
	left join t20 on t20.date_begin = fr.st_date
	left join t21 on t21.date_begin = fr.st_date
	left join t22 on t22.date_begin = fr.st_date
	left join t23 on t23.date_begin = fr.st_date
order by st_num;