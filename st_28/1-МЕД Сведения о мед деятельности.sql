drop temporary table if exists OKATO, foreign_cases, count_people, pu_nal_beznal, t_final;

create temporary table if not exists OKATO
select OKATO
from lpu l, mu_ident mu
where l.id_lpu = mu.id_lpu and l.is_active = 1;

CREATE TEMPORARY TABLE IF NOT EXISTS foreign_cases
	SELECT 
		c.id_case,
		c.id_patient,
		pd.BIRTHDAY,
		date_add(pd.birthday, interval 18 year) > c.date_begin as is_child,
		c.date_begin,
		c.date_end, 
		vo.country_short_name,
		vo.code,
		c.id_dept
	FROM 
		cases c 	
	JOIN patient_data pd ON c.id_patient = pd.id_patient 	
	JOIN vmu_oksm vo ON vo.code = pd.c_oksm AND vo.code IS NOT NULL AND vo.code <> 643
	WHERE c.date_begin BETWEEN @date_start AND @date_end and pd.ID_DOC_TYPE <> 16;

CREATE TEMPORARY TABLE if NOT EXISTS count_people
select fc.country_short_name, fc.code, count(distinct id_patient) ppl
from foreign_cases fc
group by fc.code;

CREATE TEMPORARY TABLE if NOT EXISTS pu_nal_beznal
	select
		cs.id_service, fc.id_case, fc.id_patient, ch.nal, SERVICE_COST as summ
	from foreign_cases fc
	join case_services cs on cs.id_case = fc.id_case and cs.is_oms <> 1
	left join checks ch on ch.id = cs.check_id
	left join price p on p.id = cs.ID_PROFILE and cs.is_oms in (0,3);

CREATE TEMPORARY TABLE if NOT EXISTS t_final
SELECT
	ok.okato,
	date_format(cs.date_begin, '%m') m,
    fc.code,
	case fc.is_child
		when 1 then 'AGE01'
		when 0 then 'AGE02'
	end age_cat,
	'TMC01' as 'Вид мед/пом',
	'CMC01' as 'Условие оказания мед/пом',
	'' as 'Форма мед/пом',
	'DMC01' as 'Продолжительность',
	case
		when fc.IS_CHILD = 1 then 'CMP32'
		when fc.IS_CHILD = 0 then 
			case
				when fc.id_dept in (18,25) then 'CMP3301'
				else 'CMP33' end
	end as 'Профиль мед/пом',
	count(DISTINCT fc.id_case) AS pos,
    CASE
        WHEN cs.is_oms = 1 THEN 'SFC03'
        WHEN cs.is_oms in (0,3) THEN 'SFC05'
    END pu_oms,
	case
		when nb.nal = 1 then 'PAY01'
		when nb.nal = 0 then 'PAY03'
		else ''
	end nal_beznal,
	case
		when cs.is_oms in (0,3) then sum(nb.summ)
		when cs.is_oms = 1 then ROUND(COALESCE(SUM(T.price), 0), 2)
	end sum,
	cp.ppl AS ppl
from 
	OKATO ok, case_services cs
	join foreign_cases fc on fc.id_case = cs.id_case
	left join count_people cp on cp.code = fc.code
	left join pu_nal_beznal nb on nb.id_case = fc.id_case and cs.is_oms in (0,3) and nb.id_service = cs.ID_SERVICE
	LEFT JOIN vmu_profile vp ON vp.id_profile = cs.id_profile AND cs.is_oms = 1
	left join (select vt.price, vt.uet, vt.id_profile, vt.id_zone_type,
                                   vt.tariff_begin_date as d1,
                                   vt.tariff_end_date as d2,
                                   vtp.tp_begin_date as d3,
                                   vtp.tp_end_date as d4
			from vmu_tariff vt, vmu_tariff_plan vtp
			where vtp.id_tariff_group = vt.id_lpu
				And vtp.id_lpu in (select id_lpu from mu_ident)) T
                           on T.id_profile = vp.id_profile and
                              cs.date_begin between T.d1 and T.d2 and
                              cs.date_begin between T.d3 and T.d4 and
                              T.id_zone_type = cs.id_net_profile and
                              cs.is_oms = 1
where cs.DATE_BEGIN between @date_start and @date_end and cs.is_oms not in (2,4)
group by m,pu_oms, nal_beznal, fc.code, age_cat
order by m, fc.code,age_cat, pu_oms;

set @row = 0;

select @row := @row + 1, t.*
from t_final t;