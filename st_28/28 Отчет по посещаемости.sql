set @date_start = '2019-05-20';
set @date_end = '2019-05-25';
set @doc_id = 0;
set @depart_id = 24;

drop temporary table if exists internet_ap, registr_ap, doctor_ap, internet_black_list;

create temporary table if not exists internet_ap 
select date(ap.time) date_ap, ap.user_id, count(distinct ap.id) inet_count
from appointments ap
left join doctor_spec ds on ds.user_id = ap.user_id
where ap.time between @date_start and @date_end + interval 1 day and ap.exported = 1 and ap.author = 'r' 
and (ds.department_id = @depart_id or @depart_id = 0) 
group by date_ap, user_id;

create temporary table if not exists registr_ap 
select date(ap.time) date_ap, ap.user_id, count(distinct ap.id) registr_count 
from appointments ap
left join doctor_spec ds on ds.user_id = ap.user_id 
where ap.time between @date_start and @date_end  + interval 1 day and ap.exported = 0 and ap.author = 'r' 
and (ds.department_id = @depart_id or @depart_id = 0)
group by  date_ap, user_id;

create temporary table if not exists doctor_ap 
select date(ap.time) date_ap, ap.user_id, count(distinct ap.id) as doctor_count
from appointments ap
left join doctor_spec ds on ds.user_id = ap.user_id 
where ap.time between @date_start and @date_end  + interval 1 day  and ap.author = 'd' 
and (ds.department_id = @depart_id or @depart_id = 0)
group by  date_ap, user_id;

create temporary table if not exists internet_black_list
select ap.user_id, date(ap.time) date_ap, count(ap.id) cancel
from appointments ap
left join black_list bl on bl.id_appointment = ap.id
where ap.time between @date_start and (@date_end+ interval 1 day) and bl.id_appointment = ap.id
group by date_ap, user_id;


select dep.name, date(ap.time) date_ap, u.name,
(coalesce(ip.inet_count, 0) + coalesce(dp.doctor_count, 0) + coalesce(rp.registr_count, 0)) total,
ip.inet_count, (ip.inet_count - ibl.cancel) existance, cancel, dp.doctor_count, rp.registr_count
from appointments ap
		left join doctor_spec ds on ds.doctor_id = ap.doctor_id
        left join users u ON u.id = ap.user_id 
        left join internet_ap ip ON ip.user_id = ap.user_id and date(ap.time) = ip.date_ap
		left join internet_black_list ibl ON ibl.user_id = ap.user_id and date(ap.time) = ibl.date_ap
        left join registr_ap rp ON rp.user_id = ap.user_id and date(ap.time) = rp.date_ap
        left join doctor_ap dp ON dp.user_id = ap.user_id  and date(ap.time) = dp.date_ap
        left join departments dep ON dep.id = ds.department_id
where
        (ap.doctor_id = @doc_id or @doc_id = 0 or @doc_id is null)
        and (dep.id = @depart_id or @depart_id = 0 or @depart_id is null)
		and u.id <> 1
		and ap.time between @date_start and @date_end + interval 1 day
		and (coalesce(ip.inet_count, 0) + coalesce(dp.doctor_count, 0) + coalesce(rp.registr_count, 0)) > 0
group by date_ap, u.id
order by date_ap, u.name;