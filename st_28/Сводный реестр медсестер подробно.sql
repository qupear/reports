SET @row=0;
SELECT @row := @row + 1, t.*
	FROM
		(			SELECT doc,
			       reses.person_name,
			       id_case,
			       reses.percent,
			       sum(reses.summ_cost) AS summa,
			       sum(summ_cost_with_disc),
			       round((sum(reses.summ_cost) / 100 * reses.percent), 2)
				FROM
					(   SELECT u.name         AS                            doc,
						       p.id           AS                            id_person,
						       p.name         AS                            person_name,
						       dep.proc_nurse AS                            percent,
						       c.id_case      AS                            id_case,
						       SUM(cs.service_cost)                         summ_cost,
						       round(SUM(cs.service_cost * cs.discount), 2) summ_cost_with_disc,
						       ch.number      AS                            check_numb,
						       ds.doc_percent
							FROM
								cases                       c
									LEFT JOIN case_services cs ON c.id_case = cs.id_case AND cs.id_order = - 1
									LEFT JOIN doctor_spec   ds ON c.id_doctor = ds.doctor_id
									LEFT JOIN departments   dep ON ds.department_id = dep.id
									LEFT JOIN personal      p ON c.id_nurse = p.id
									LEFT JOIN prepayments   pr ON c.id_case = pr.case_id
									LEFT JOIN checks        ch ON pr.check = ch.id
									LEFT JOIN users         u ON ds.user_id = u.id
							WHERE ch.date BETWEEN @date_start AND @date_end
							  AND (c.id_doctor = @doc_id OR @doc_id = 0)
							  AND (ds.department_id = @depart_id OR @depart_id = 0)
							  AND dep.proc_nurse IS NOT NULL
							  AND dep.proc_nurse <> 0
							  AND p.is_active = 1
							GROUP BY id_case
							ORDER BY person_name, id_case) AS reses
				GROUP BY reses.person_name, doc, id_case) t;

SELECT *
	FROM
		cases                       c
			LEFT JOIN case_services cs ON c.id_case = cs.id_case AND cs.id_order = - 1
			LEFT JOIN doctor_spec   ds ON c.id_doctor = ds.doctor_id
			LEFT JOIN departments   dep ON ds.department_id = dep.id
LEFT JOIN prepayments   pr ON c.id_case = pr.case_id
									LEFT JOIN checks        ch ON pr.check = ch.id
									LEFT JOIN users         u ON ds.user_id = u.id
LEFT JOIN appointments ap on c.id_case = ap.case_id
LEFT JOIN doctor_staff_match dsm on c.id_doctor = dsm.doctor and ap.time < dsm.date_start
							WHERE c.id_nurse = 0 AND c.id_janitor = 0 AND ch.date BETWEEN @date_start AND @date_end
							  AND (c.id_doctor = @doc_id OR @doc_id = 0)
							  AND (ds.department_id = @depart_id OR @depart_id = 0)
							  AND dep.proc_nurse IS NOT NULL
							  AND dep.proc_nurse <> 0


	;


