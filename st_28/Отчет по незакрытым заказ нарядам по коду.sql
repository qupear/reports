set @date_start = '';
set @date_end = '';
set @code = '';
select o.Order_Number as numb, o.ordercreatedate as o_date, o.bzp_direction_text as o_text,  u.name as doc,
concat(pd.SURNAME,' ', pd.name, ' ', pd.second_name) as pat
from orders o
left join patient_data pd on o.id_human = pd.id_human and pd.DATE_END = '2200-01-01'
left join doctor_spec ds on ds.doctor_id = o.id_doctor
left join users u on u.id = ds.user_id
left join case_services cs on cs.id_order = o.id
LEFT JOIN price_orto_soc pos on cs.id_profile = pos.id and o.order_type = 1
LEFT JOIN price p on CS.id_profile = p.id AND o.order_type = 0
where o.orderCreateDate between @date_start AND @date_end and o.status < 2
and (ds.department_id = @depart_id or @depart_id = 0)
  and (ds.doctor_id = @doc_id or @doc_id =0)
and (o.id_technician = @techn_id or @techn_id = 0)
and case WHEN order_type = 0 then p.code = @code
    WHEN order_type = 1 THEN pos.code = @code END
group by numb
order by o.ID