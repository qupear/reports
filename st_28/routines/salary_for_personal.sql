CREATE DEFINER=`root`@`localhost` PROCEDURE `salary_for_personal`(in dataStart date, in dateEnd date,in doc_id integer, in depart_id integer, in pers_spec integer, in detail integer)
BEGIN
    /*pers_spec: 0 - медсестра, 1 - санитарка; detail: 0 - подробно, 1 - коротко */
	DECLARE done int DEFAULT FALSE;
	DECLARE a, b, i, f integer;
	DECLARE c datetime;
	DECLARE cursor_cases CURSOR FOR SELECT id_case, id_doctor, time from cases_without_personal;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	DROP TEMPORARY TABLE IF EXISTS personal_for_case;
	CREATE TEMPORARY TABLE IF NOT EXISTS personal_for_case (
	                                                       id_case integer,
	                                                       nurse integer,
	                                                       jantor integer
	);
	DROP TEMPORARY TABLE IF EXISTS cases_without_personal;
	CREATE TEMPORARY TABLE IF NOT EXISTS cases_without_personal
	    (SELECT c.id_case, c.id_doctor, ap.time, sum(cs.service_cost) cost, sum(cs.service_cost * cs.discount) cost_2,
       short_name(u.name) doc_name,
	            case pers_spec when 0 then dep.proc_nurse
	                WHEN  1 THEN dep.proc_janitor else 0 end pers_proc,
	            case pers_spec when 0 then round((sum(cs.service_cost) * (dep.proc_nurse / 100)), 2)
	                WHEN  1 THEN round(sum(cs.service_cost) * (dep.proc_janitor / 100), 2) else 0 end salary
	    FROM 	cases                       c
			LEFT JOIN case_services cs ON c.id_case = cs.id_case AND cs.id_order = - 1
			LEFT JOIN doctor_spec   ds ON c.id_doctor = ds.doctor_id
			LEFT JOIN departments   dep ON ds.department_id = dep.id
LEFT JOIN prepayments   pr ON c.id_case = pr.case_id
									LEFT JOIN checks        ch ON pr.check = ch.id
									LEFT JOIN users         u ON ds.user_id = u.id
LEFT JOIN appointments ap on c.id_case = ap.case_id
							WHERE c.id_nurse = 0 AND c.id_janitor = 0
							  AND ch.date BETWEEN dataStart AND dateEnd
							  AND (c.id_doctor = doc_id OR doc_id = 0)
							  AND (ds.department_id = depart_id OR depart_id = 0)
 							  AND dep.proc_nurse IS NOT NULL and dep.proc_janitor is not null
						  AND dep.proc_nurse <> 0 and dep.proc_janitor <> 0
	    GROUP BY c.id_case);
    DROP TEMPORARY TABLE IF EXISTS cases_with_personal;
    CREATE TEMPORARY TABLE IF NOT EXISTS cases_with_personal
        (SELECT     case WHEN pers_spec =  0 then p.name WHEN pers_spec = 1 then p1.name else '' end p_name,
short_name(u.name)         AS                            doc,
                 c.id_case      AS                            id_case,
                 case pers_spec when 0 then dep.proc_nurse
	                WHEN  1 THEN dep.proc_janitor else 0 end pers_proc,

						       SUM(cs.service_cost)                         cost,
						       round(SUM(cs.service_cost * cs.discount), 2) cost_2,
                	            case pers_spec when 0 then round(sum(cs.service_cost) * (dep.proc_nurse/ 100), 2)
	                WHEN  1 THEN round(sum(cs.service_cost) * (dep.proc_janitor / 100), 2) else 0 end salary,
                c.id_doctor,     case pers_spec WHEN 0 then p.id WHEN 1 then p1.id end id
							FROM
								cases                       c
									LEFT JOIN case_services cs ON c.id_case = cs.id_case AND cs.id_order = - 1
									LEFT JOIN doctor_spec   ds ON c.id_doctor = ds.doctor_id
									LEFT JOIN departments   dep ON ds.department_id = dep.id
									LEFT JOIN personal      p ON c.id_nurse = p.id and p.id_spec = 2
								    LEFT JOIN personal p1 on c.id_janitor = p1.id and p1.id_spec = 4
									LEFT JOIN prepayments   pr ON c.id_case = pr.case_id
									LEFT JOIN checks        ch ON pr.check = ch.id
									LEFT JOIN users         u ON ds.user_id = u.id
							WHERE
							      ch.date BETWEEN dataStart AND dateEnd
							  AND (c.id_doctor = doc_id OR doc_id = 0)
							  AND (ds.department_id = depart_id OR depart_id = 0)
						  AND dep.proc_nurse IS NOT NULL
						  AND dep.proc_nurse <> 0
							  AND p.is_active = 1
							GROUP BY id_case
							ORDER BY p_name, id_case);
SELECT count(*) from cases_without_personal into f;
	OPEN cursor_cases;

	REPEAT
		FETCH cursor_cases INTO a, b, c;
		set i = 0;
		IF i < f THEN
	    INSERT INTO personal_for_case (SELECT a, nurse, janitor from doctor_staff_match where doctor = b and  c < date_start ORDER BY id DESC LIMIT 1);
		set i = i +1;
		end if;
	UNTIL done END REPEAT;
CLOSE cursor_cases;
    set @row= 0;
    IF detail = 0 then
	   SELECT @row:=@row+1, T.* from
	    (SELECT case WHEN pers_spec =  0 then p.name WHEN pers_spec = 1 then p1.name else '' end p_name,
	           cwp.doc_name, PFC.id_case, CWP.pers_proc, cwp.cost, round(CWP.cost_2, 2), round(CWP.salary, 2) ,
	           CASE WHEN pers_spec = 0 THEN p.id WHEN pers_spec = 1 THEN p1.id end id, CWP.id_doctor
	    from personal_for_case pfc
	LEFT JOIN cases_without_personal cwp on pfc.id_case = CWP.id_case
	LEFT JOIN personal p on PFC.nurse = p.id and p.id_spec = 2
	    LEFT JOIN personal p1 on PFC.jantor = p1.id AND p1.id_spec = 4
	GROUP BY id, id_doctor, PFC.id_case
	    UNION ALL
	    SELECT * from cases_with_personal
    GROUP BY id, id_doctor, id_case
    ORDER BY p_name, doc_name, id_case) T;
	    ELSEIF detail = 1 THEN
	        DROP TEMPORARY TABLE IF EXISTS test;
	        CREATE TEMPORARY TABLE IF NOT EXISTS test (  p_name varchar(255), id_case integer, pers_proc decimal(5,2),
	        cost decimal(15,2), cost_2 decimal(15,2), salary decimal(15,2), id integer);
	        INSERT into test
	        SELECT case WHEN pers_spec =  0 then p.name WHEN pers_spec = 1 then p1.name else '' end p_name,
	           count(distinct PFC.id_case), CWP.pers_proc, sum(cwp.cost), sum(CWP.cost_2), round(sum(CWP.salary), 2),
	        CASE WHEN pers_spec = 0 THEN p.id WHEN pers_spec = 1 THEN p1.id end id
	        from personal_for_case pfc
	LEFT JOIN cases_without_personal cwp on pfc.id_case = CWP.id_case
	LEFT JOIN personal p on PFC.nurse = p.id and p.id_spec = 2
	    LEFT JOIN personal p1 on PFC.jantor = p1.id AND p1.id_spec = 4
	GROUP BY id
	    UNION ALL
	    SELECT  p_name, count(DISTINCT id_case), pers_proc, sum(cost), sum(cost_2), round(sum(salary), 2), id from cases_with_personal
    GROUP BY id
    ORDER BY id;
	        SELECT @row:=@row+1, T.* from
	        (SELECT   p_name, sum(id_case), pers_proc, sum(cost), sum(cost_2), round(sum(salary), 2), id from test GROUP BY id
	            ORDER BY p_name) T;
	        END IF
	;
END;