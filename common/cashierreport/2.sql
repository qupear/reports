SET @date_start='2020-12-07';
SET @date_end='2020-12-07';
SELECT T1.case_id, T1.order_id, '' AS Order_number, cases.ID_PATIENT AS patient_id, patient_data.BIRTHDAY AS birthd, CONCAT_WS(' ', patient_data.Surname, patient_data.Name, patient_data.Second_name) AS patient_name, T1.doctor_id, users.name AS doctor_name, T1.cost, T1.payed_cost, T2.summ, T2.ch_number, T2.check_date, T2.id_check, T2.nal
FROM (
SELECT prepayments.case_id AS case_id, prepayments.doctor_id AS doctor_id, SUM(prepayments.summ) AS summ, prepayments.order_id AS order_id, checks.number AS ch_number, checks.date AS check_date, checks.id AS id_check, checks.nal
FROM prepayments, checks
WHERE checks.id = prepayments.check AND checks.date >= @date_start AND checks.date <= @date_end AND prepayments.order_id = -1
GROUP BY nal, case_id, doctor_id, ch_number) AS T2
LEFT JOIN (
SELECT case_services.ID_CASE AS case_id, case_services.ID_ORDER AS order_id, case_services.ID_DOCTOR AS doctor_id, SUM(case_services.SERVICE_COST * case_services.discount) AS cost, SUM(case_services.SERVICE_COST * case_services.discount * (case_services.is_paid > 0)) AS payed_cost
FROM case_services
WHERE case_services.IS_OMS = 0 AND case_services.ID_ORDER = -1
GROUP BY case_id, doctor_id
ORDER BY case_id) AS T1 ON T1.case_id = T2.case_id AND T1.doctor_id = T2.doctor_id, cases, patient_data, doctor_spec, users
WHERE T1.doctor_id = doctor_spec.doctor_id AND users.id = doctor_spec.user_id AND cases.ID_CASE = T1.case_id AND patient_data.ID_PATIENT = cases.ID_PATIENT UNION
SELECT T2.order_id AS case_id, T2.order_id, orders.order_number AS Order_number, patient_data.ID_PATIENT AS patient_id, patient_data.BIRTHDAY AS birthd, CONCAT_WS(' ', patient_data.Surname, patient_data.Name, patient_data.Second_name) AS patient_name, orders.id_doctor AS doctor_id, users.name AS doctor_name, T1.cost, T1.payed_cost, T2.summ, T2.ch_number, T2.check_date, T2.id_check, T2.nal
FROM (
SELECT SUM(prepayments.summ) AS summ, prepayments.order_id AS order_id, checks.number AS ch_number, checks.date AS check_date, checks.id AS id_check, checks.nal
FROM prepayments, checks
WHERE checks.id = prepayments.check AND checks.date >= @date_start AND checks.date <= @date_end AND prepayments.order_id <> -1
GROUP BY nal, order_id, ch_number) AS T2
LEFT JOIN (
SELECT case_services.ID_ORDER AS order_id, SUM(case_services.SERVICE_COST * case_services.discount) AS cost, SUM(case_services.SERVICE_COST * case_services.discount * (case_services.is_paid > 0)) AS payed_cost
FROM case_services
WHERE case_services.IS_OMS = 0 AND case_services.ID_ORDER <> -1
GROUP BY order_id
ORDER BY order_id) AS T1 ON T1.order_id = T2.order_id, patient_data, doctor_spec, users, orders
WHERE orders.id = T2.order_id AND orders.id_doctor = doctor_spec.doctor_id AND users.id = doctor_spec.user_id AND patient_data.ID_HUMAN = orders.ID_HUMAN AND patient_data.is_active = 1 AND patient_data.date_end = '2200-01-01'
ORDER BY id_check, patient_name