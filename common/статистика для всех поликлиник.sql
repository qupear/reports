set @date_start = '2019-06-23';
set @date_end = '2019-06-25';

drop temporary table if exists all_data;

create temporary table if not exists all_data
/*Общее количество законченных случаев лечения, зарегистрированных в МИС*/
select count(distinct id_case) as MES, 0 as app_count, 0 as other_medical_services, 0 as hosp_count, 0 as cons_count, 0 as lab_count, 0 as instr_count, 0 as EMK 
from case_services cs
where is_oms = 5 and DATE_END between @date_start and @date_end

UNION
/*зарегистрированных посещений врачей*/
select 0 as MES, count(ID_CASE) as app_count, 0 as other_medical_services, 0 as hosp_count, 0 as cons_count, 0 as lab_count, 0 as instr_count, 0 as EMK
from cases 
where DATE_END between @date_start and @date_end

UNION
/*прочих медицинских услуг*/
select 0 as MES, 0 as app_count, 0 as other_medical_services, 0 as hosp_count, 0 as cons_count, 0 as lab_count, 0 as instr_count, 0 as EMK 

UNION
/*направлений на госпитализацию*/
select 0 as MES, 0 as app_count, 0 as other_medical_services, count(id) as hosp_count, 0 as cons_count, 0 as lab_count, 0 as instr_count, 0 as EMK 
from mq_referrals
where ReferralDate between @date_start and @date_end and ReferralType = 1

UNION
/*направлений на консультацию*/
select 0 as MES, 0 as app_count, 0 as other_medical_services, 0 as hosp_count, count(id) as cons_count, 0 as lab_count, 0 as instr_count, 0 as EMK
from mq_referrals
where ReferralDate between @date_start and @date_end and (ReferralType = 2 or ReferralType = 4)

UNION
/*направлений на лабораторные исследования В РАЗРАБОТКЕ*/
select 0 as MES, 0 as app_count, 0 as other_medical_services, 0 as hosp_count, 0 as cons_count, 0 as lab_count, 0 as instr_count, 0 as EMK

UNION
/*направлений на инструментальные исследования*/
select 0 as MES, 0 as app_count, 0 as other_medical_services, 0 as hosp_count, 0 as cons_count, 0 as lab_count, count(distinct id_case) as instr_count, 0 as EMK
from directions d
left join doctor_spec ds on ds.doctor_id = d.id_doctor
where date_direction between @date_start and @date_end and (ds.spec_id = 60 or ds.spec_id = 208)

UNION
/*ЕМК*/
select 0 as MES, 0 as app_count, 0 as other_medical_services, 0 as hosp_count, 0 as cons_count, 0 as lab_count, 0 as instr_count, count(distinct id_doctor) as EMK
from cases 
where DATE_END between @date_start and @date_end;

select sum(MES), sum(app_count), sum(other_medical_services), sum(hosp_count), sum(cons_count), sum(lab_count), sum(instr_count), sum(EMK)
from all_data;