create temporary table if not exists prot
(select id_human as id_h, id_ref, concat(date_format(date_protocol, '%d-%m-%Y'), '  ', u.name) date_protocol, id_doctor,
       case when ds.spec_id = 106 then 'терапия' when ds.spec_id = 108 then 'хирургия' end doc_spec
from protocols prot
         left join doctor_spec ds on prot.id_doctor = ds.doctor_id
         left join users u on ds.user_id = u.id
where prot.id_ref in (46161, 48241, 49531, 52281, 52851, 65861, 66961, 67901, 69101, 70181, 71351,115961, 115011, 77221,
                      116060, 76631, 72811, 63451, 62521, 61411, 60521, 116070, 13724, 22553, 34541)
  and date_protocol >= @date_start
group by id_human, id_doctor);
create temporary table if not exists TER (select id_human as id_h, id_ref, date_protocol as d3
                                          from protocols prot
                                          where prot.id_ref in (9492, 8461)
                                            and date_protocol >= @date_start
                                          group by id_human, id_ref); set @row = 0;


select @row := @row + 1, T.*
from (select doc_spec, concat_ws(' ', pd.SURNAME, pd.name, pd.SECOND_NAME) as pat, pd.card_number,
             date_format(o.ordercreatedate, '%d-%m-%Y') date, pa.UNSTRUCT_ADDRESS as adr, pd.remark,
             concat(substring_index(u.name, ' ', 1), ' ', substring(substring_index(u.name, ' ', - 2), 1, 1), '. ',
                    substring(substring_index(u.name, ' ', - 1), 1, 1), '.') doc, ter.d3,
             coalesce(date_format(prot.date_protocol, '%d-%m-%Y'), '') date_p, '' string, o.ordercreatedate
      from orders o
               left join patient_data pd ON pd.id_human = o.id_human
               left join doctor_spec ds ON ds.doctor_id = o.id_doctor
               left join users u ON ds.user_id = u.id
               left join case_services cs ON cs.id_order = o.id
               left join patient_address pa on pd.ID_ADDRESS_REG = pa.ID_ADDRESS
               left join price_orto_soc pos ON cs.ID_PROFILE = pos.id
               left join prot on o.Id_Human = prot.id_h and ds.doctor_id = prot.id_doctor
               left join ter on ter.id_h = pd.id_human
               left join (select count(*) as counter, id_order
                          from case_services
                          where id_order > 0
                          group by id_order) C ON cs.id_order = C.id_order
      where prot.date_protocol is not null
        and o.orderCreateDate between @date_start and CURDATE()
        and (pos.CODE = '1.1' or pos.code = '1.4')
        and o.order_type = 1
        and C.counter = 1
      group by order_number
      order by o.ordercreatedate, pat) T;
