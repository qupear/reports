set @date_start = '2020-01-20';
set @date_end = '2020-02-25';
set @doc_id = 0;
drop temporary table if exists T1, T2, T3,T4,T5,T6,T7,T8,T9,T34_1,
T10,T11,T12,T13,T14,T15,T16,T17,T18,T19,T20,T21,T22,T23,T24,T29_5,T25,
T26,T27,T28,T29,T30,T31,T32,T33,T34,T35,
t_all_1,doctor_services,first_row,t56,t57;
create temporary table if not exists doctor_services (select cs.date_begin, cs.id_case, p.code,
                                                             vd.diagnosis_code, cs.tooth_name, p.uet, cs.is_oms,
                                                             case
                                                                 when YEAR(CS.DATE_BEGIN) - YEAR(pd.BIRTHDAY)
    - (DATE_FORMAT(CS.DATE_BEGIN, '%m%d') < DATE_FORMAT(pd.BIRTHDAY, '%m%d'))  >= 60 and pd.SEX = 'м'
                                                                     then 0
                                                                 WHEN YEAR(CS.DATE_BEGIN) - YEAR(pd.BIRTHDAY)
    - (DATE_FORMAT(CS.DATE_BEGIN, '%m%d') < DATE_FORMAT(pd.BIRTHDAY, '%m%d'))  >= 55 and pd.SEX = 'ж'
                                                                     then 1 else -1 end as old_people, pd.ID_PATIENT, p.cost as summ
                                                      from case_services cs
                                                               left join cases c on cs.id_case = c.id_case
                                                               left join patient_data pd on c.ID_PATIENT = pd.id_patient
															   left join doctor_spec ds on ds.doctor_id = cs.id_doctor
                                                               left join price p on p.id = cs.id_profile and cs.is_oms in (0, 3)
                                                               left join vmu_diagnosis vd on cs.id_diagnosis = vd.id_diagnosis
                                                      where cs.is_oms in (0,3)
							and ds.spec_id in (7,108)
							and (cs.id_doctor = @doc_id or @doc_id = 0) 
							and (ds.department_id = @depart_id or @depart_id = 0)
                                                        and cs.date_begin between @date_start and @date_end);

set @row = 0;
create temporary table if not exists first_row (select @row := @row + 1 as st_num, T.date_begin as st_date
                                                from (select date_begin
                                                      from doctor_services ds
where ds.is_oms <> 1
                                                      group by date_begin
                                                      order by date_begin) T);
/*принято больных*/
create temporary table if not exists T1 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where code in ('3001', '3002')
                                         group by date_begin
                                         order by date_begin);
/*из них первичных*/
create temporary table if not exists T2 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where code in ('3001')
                                         group by date_begin
                                         order by date_begin);
/*осмотры*/
create temporary table if not exists T3 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where code in ('3001', '3002')
                                         group by date_begin
                                         order by date_begin);
/*всего*/
create temporary table if not exists T4 (select date_begin, count(id_case) as val
			from doctor_services
			where code in ('3119', '3118', '3125', '3122')
			group by date_begin
			order by date_begin);

/*обезболивание*/
create temporary table if not exists t56
     (select date_begin, id_case as val
           from doctor_services
           where code in ('3109')
           group by id_case, tooth_name
           order by date_begin) ;
     
/*инъекции*/
/*create temporary table if not exists t57
      (select date_begin, id_case as val
           from doctor_services
           where code in ('3109')
           group by id_case, tooth_name
           order by date_begin) ;*/

/*без обезболивания*/
create temporary table if not exists T5
(select date_begin, count(id_case) as val
		from doctor_services
		where code in ('3119', '3118', '3125', '3122')
		and id_case not in (select val from t56)
group by date_begin
order by date_begin);

/*с обьезболиванием*/
create temporary table if not exists T6
 (select date_begin, count(id_case) as val
      from doctor_services
      where code in ('3119', '3118', '3125', '3122')
       and id_case in (select val from t56)
group by date_begin
order by date_begin);

/*инъекции*/
create temporary table if not exists T7
(select date_begin, count(id_case) as val
      from doctor_services
      where code in ('3109')
group by date_begin
order by date_begin);

/*периодонтит*/
create temporary table if not exists T8
    (select date_begin, count(id_case) as val
     from doctor_services
     where diagnosis_code in ('K04.5', 'K04.7')
	 and code in ('3119', '3118', '3125', '3122')
     group by date_begin
     order by date_begin);
/*пародонтоз*/
create temporary table if not exists T9
    (select date_begin, count(distinct id_case) as val
     from doctor_services
     where diagnosis_code in ('K05.3', 'K05.4')
	 and code in ('3119', '3118', '3122')
     group by date_begin
     order by date_begin);
/*перикоронит*/
create temporary table if not exists T10
    (select date_begin, count(id_case) as val
     from doctor_services
     where diagnosis_code in ('K05.22', 'K05.32')
	and code in ('3125', '3119', '3118')
     group by date_begin
     order by date_begin);
/*Кисты*/
create temporary table if not exists T11
    (select date_begin, count(id_case) as val
     from doctor_services
     where diagnosis_code in ('K09.9')
	 and code in ('3001', '3002')
     group by date_begin
     order by date_begin);
/*фрактура зуба*/
create temporary table if not exists T12
    (select date_begin, count(id_case) as val
     from doctor_services
     where diagnosis_code in ('S02.5')
	 and code in ('3001', '3002')
     group by date_begin
     order by date_begin);
/*Ретенции*/
create temporary table if not exists T13
    (select date_begin, count(id_case) as val
     from doctor_services
     where diagnosis_code in ('K07.3', 'K01.0')
	 and code in ('3125')
     group by date_begin
     order by date_begin);
/*разрезы*/
create temporary table if not exists T14
    (select date_begin, count(id_case) as val
     from doctor_services
     where code in ('3133', '3134', '3135')
     group by date_begin
     order by date_begin);
/*незлокачественные*/
create temporary table if not exists T15
    (select date_begin, count(id_case) as val
     from doctor_services
     where diagnosis_code in ('D17.0')
	 and code in ('3001', '3002')
     group by date_begin
     order by date_begin);
/*Альвеолит*/
create temporary table if not exists T16
    (select date_begin, count(id_case) as val
     from doctor_services
     where code in ('3126')
		and diagnosis_code in ('K10.3')
     group by date_begin
     order by date_begin);
/*край альвеолы*/
create temporary table if not exists T17
    (select date_begin, count(id_case) as val
     from doctor_services
     where code in ('7209')
		and diagnosis_code in ('K10.8')
     group by date_begin
     order by date_begin);
/*кровотечения*/
create temporary table if not exists T18
    (select date_begin, count(id_case) as val
     from doctor_services
     where code in ('3124')
		and diagnosis_code in ('T81.0')
     group by date_begin
     order by date_begin);
/*иссечение образования слизистой*/
create temporary table if not exists T19
    (select date_begin, count(id_case) as val
     from doctor_services
     where code in ('3142')
     group by date_begin
     order by date_begin);
/*имплантация*/
create temporary table if not exists T20
    (select date_begin, count(id_case) as val
     from doctor_services
     where code in ('7211', '7212', '7213', '7214')
     group by date_begin
     order by date_begin);
/*Резекция верхушки корня*/
create temporary table if not exists T21
    (select date_begin, count(id_case) as val
     from doctor_services
     where code in ('3143', '3144', '3145', '3146', '3147')
     group by date_begin
     order by date_begin);
/*Синус-Лифтинг*/
create temporary table if not exists T22
    (select date_begin, count(id_case) as val
     from doctor_services
     where code in ('7205', '7206')
     group by date_begin
     order by date_begin);
/*имплантация*/
create temporary table if not exists T23
    (select date_begin, count(id_case) as val
     from doctor_services
     where code in ('7211', '7212', '7213', '7214')
     group by date_begin
     order by date_begin);
/*наложение шва на слизистую*/
create temporary table if not exists T24
    (select date_begin, count(id_case) as val
     from doctor_services
     where code in ('3171')
     group by date_begin
     order by date_begin);
/*установка формирователя десен*/
create temporary table if not exists T25
    (select date_begin, count(id_case) as val
     from doctor_services
     where code in ('7215')
     group by date_begin
     order by date_begin);
/*м старше 60*/
create temporary table if not exists T26 (select date_begin, count(distinct ID_PATIENT) as val
                                          from doctor_services
where old_people = 0 and is_oms <> 1 and code in ('3001', '3002')
                                          group by date_begin
                                          order by date_begin);
/*ж старше 55*/
create temporary table if not exists T27 (select date_begin, count(distinct ID_PATIENT) as val
                                          from doctor_services
where old_people = 1  and is_oms <> 1 and code in ('3001', '3002')
                                          group by date_begin
                                          order by date_begin);

create temporary table if not exists T28_1
(select id from protocols p
where child_ref_ids like '%119110(%' or child_ref_ids like '%119130(%'
or child_ref_ids like '%119150(%' or child_ref_ids like '%119290(%'
or child_ref_ids like '%127740(%' or child_ref_ids like '%119310(%'
or child_ref_ids like '%119330(%' or child_ref_ids like '%127760(%'
or child_ref_ids like '%119350(%' or child_ref_ids like '%127780(%'
or child_ref_ids like '%119400(%' or child_ref_ids like '%127820(%'
or child_ref_ids like '%127880(%' or child_ref_ids like '%119170(%'
or child_ref_ids like '%119190(%' or child_ref_ids like '%119210(%'
or child_ref_ids like '%119230(%' or child_ref_ids like '%119250(%'
or child_ref_ids like '%119270(%' );
/*Беременные*/
create temporary table if not exists T28 (select count(distinct ID_PATIENT)  as val, date_begin
                                          from doctor_services ds
left join protocols p on ds.id_case = p.id_case
where ds.is_oms <> 1
and code in ('3001','3002')  
and p.id_ref in (119110, 119130, 119150, 119290,
				 127740, 119310, 127760, 119330,
				 127780, 119350, 127820, 119400,
				 127880, 119170, 119190, 119210,
				 119230, 119250, 119270)
or p.id in (select id from T28_1)
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T29_1
(select id from protocols p
where child_ref_ids like '%103312(%'
or child_ref_ids like '%3464(%' or child_ref_ids like '%4124(%'
or child_ref_ids like '%73842(%' or child_ref_ids like '%4582(%'
or child_ref_ids like '%74702(%' or child_ref_ids like '%5383(%'
or child_ref_ids like '%114012(%' or child_ref_ids like '%39522(%'
or child_ref_ids like '%115133(%' or child_ref_ids like '%127800(%'
or child_ref_ids like '%63562(%' or child_ref_ids like '%127850(%'
or child_ref_ids like '%59842(%' or child_ref_ids like '%60682(%'
or child_ref_ids like '%61623(%' or child_ref_ids like '%62702(%'
or child_ref_ids like '%72192(%' or child_ref_ids like '%73022(%');
/*ВОВ*/
create temporary table if not exists T29 (select count(distinct ID_PATIENT)  as val, date_begin
                                          from doctor_services ds
left join protocols p on ds.id_case = p.id_case
where ds.is_oms <> 1
and code in ('3001','3002')  
and p.id_ref in (103312, 3464, 4124, 73842, 4582,
				 74702, 5383, 114012, 39522,
				 115133, 127800, 63562, 127850,
				 59842, 60682, 61623, 62702,
				 72192, 73022)
or p.id in (select id from T29_1)
                                          group by date_begin
                                          order by date_begin);
/*YET*/
create temporary table if not exists T30
    (select date_begin, ROUND(COALESCE(SUM(UET), 0), 2) as val
     from doctor_services
     group by date_begin
     order by date_begin);

/*сумма*/
create temporary table if not exists T31
    (select date_begin, ROUND(COALESCE(SUM(summ), 0), 2) as val
     from doctor_services
     group by date_begin
     order by date_begin);

create temporary table if not exists t_all_1
select fr.st_num, fr.st_date, coalesce(T1.val, 0), coalesce(T2.val, 0), coalesce(T3.val, 0),
       coalesce(T4.val, 0), coalesce(T5.val, 0), coalesce(T6.val, 0), coalesce(T7.val, 0), coalesce(T8.val, 0),
       coalesce(T9.val, 0), coalesce(T10.val, 0),  coalesce(T11.val, 0),
       coalesce(T12.val, 0), coalesce(T13.val, 0), coalesce(T14.val, 0), coalesce(T15.val, 0), coalesce(T16.val, 0),
       coalesce(T17.val, 0)
from first_row fr
         left join T1 on T1.date_begin = fr.st_date
         left join T2 on T2.date_begin = fr.st_date
         left join T3 on T3.date_begin = fr.st_date
         left join T4 on T4.date_begin = fr.st_date
         left join T5 on T5.date_begin = fr.st_date
         left join T6 on T6.date_begin = fr.st_date
         left join T7 on T7.date_begin = fr.st_date
         left join T8 on T8.date_begin = fr.st_date
         left join T9 on T9.date_begin = fr.st_date
         left join T10 on T10.date_begin = fr.st_date
         left join T11 on T11.date_begin = fr.st_date
         left join T12 on T12.date_begin = fr.st_date
         left join T13 on T13.date_begin = fr.st_date
         left join T14 on T14.date_begin = fr.st_date
         left join T15 on T15.date_begin = fr.st_date
         left join T16 on T16.date_begin = fr.st_date
         left join T17 on T17.date_begin = fr.st_date
         
order by fr.st_num;

select 
    ta1.*,
    coalesce(T18.val, 0),
    coalesce(T19.val, 0),
    coalesce(T20.val, 0),
    coalesce(T21.val, 0),
    coalesce(T22.val, 0),
    coalesce(T23.val, 0),
    coalesce(T24.val, 0),
    coalesce(T25.val, 0),
    coalesce(T26.val, 0),
    coalesce(T27.val, 0),
    coalesce(T28.val, 0),
    coalesce(T29.val, 0),
    coalesce(T30.val, 0),
    coalesce(T31.val, 0)
from
    t_all_1 ta1
        left join
    T18 ON T18.date_begin = ta1.st_date
        left join
    T19 ON T19.date_begin = ta1.st_date
        left join
    T20 ON T20.date_begin = ta1.st_date
        left join
    T21 ON T21.date_begin = ta1.st_date
        left join
    T22 ON T22.date_begin = ta1.st_date
        left join
    T23 ON T23.date_begin = ta1.st_date
        left join
    T24 ON T24.date_begin = ta1.st_date
        left join
    T25 ON T25.date_begin = ta1.st_date
        left join
    T26 ON T26.date_begin = ta1.st_date
        left join
    T27 ON T27.date_begin = ta1.st_date
        left join
    T28 ON T28.date_begin = ta1.st_date
        left join
    T29 ON T29.date_begin = ta1.st_date
		left join
    T30 ON T30.date_begin = ta1.st_date
        left join
    T31 ON T31.date_begin = ta1.st_date
order by ta1.st_num;