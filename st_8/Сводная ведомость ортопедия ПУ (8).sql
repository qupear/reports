set @date_start = '2020-09-01';
set @date_end = '2020-09-30';
set @doc_id = 0;
set @depart_id = 0;

drop temporary table if exists doctor_services, pervichnie, first_row, bzp_patients_with_fix,bzp_patients_no_fix,t_all_1,
t123,T1, T2, T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,T16,T17,T18,T19,T20,T21,T22,T23,T24,
T25,T26,T27,T28,T29,T30,T31,T32,T33,T34,T35,T36,T37,T38,T39,T40,T41,T42,t40_1;

create temporary table if not exists doctor_services 
(select cs.date_begin, cs.id_case as id_case, p.code, vd.diagnosis_code,
                                                             cs.tooth_name, p.uet, cs.is_oms,o.ordercompletiondate,cs.id_order, pd.id_human
                                                      from case_services cs
                                                               left join cases c on cs.id_case = c.id_case
                                                               left join patient_data pd on c.ID_PATIENT = pd.id_patient
                                                               left join price p on p.id = cs.id_profile and cs.is_oms in (0, 3, 4)
                                                               left join vmu_diagnosis vd on cs.id_diagnosis = vd.id_diagnosis
															   left join doctor_spec ds on ds.doctor_id = cs.id_doctor
															   left join orders o on o.id = cs.ID_ORDER
                                                      where (cs.id_doctor = @doc_id or @doc_id = 0)
							and (ds.department_id = @depart_id or @depart_id = 0)
							and ds.department_id in (17,18)
                                                        and o.orderCompletionDate between @date_start and @date_end
                                                        and cs.is_oms in (0, 3, 4)
														and o.order_type in (0));

create temporary table if not exists pervichnie
(select cs.date_begin, cs.id_case as id_case, p.code, vd.diagnosis_code,
                                                             cs.tooth_name, p.uet, cs.is_oms,cs.id_order, pd.id_human
                                                      from case_services cs
                                                               left join cases c on cs.id_case = c.id_case
                                                               left join patient_data pd on c.ID_PATIENT = pd.id_patient
                                                               left join price p on p.id = cs.id_profile and cs.is_oms in (0, 3, 4)
                                                               left join vmu_diagnosis vd on cs.id_diagnosis = vd.id_diagnosis
															   left join doctor_spec ds on ds.doctor_id = cs.id_doctor
                                                      where (cs.id_doctor = @doc_id or @doc_id = 0)
							and (ds.department_id = @depart_id or @depart_id = 0)
							and ds.department_id in (17,18)
                                                        and cs.is_oms in (0, 3, 4) and p.code in ('7077', '7078')
														and pd.id_human in (select id_human from doctor_services));


create temporary table if not exists first_row 
(select  T.date_begin as st_date
from (select date_begin
                                                      from doctor_services
                                                      group by date_begin
                                                      order by date_begin) T);
/* анастезия*/
create temporary table if not exists T1 (select date_begin, count(id_case) as val
                                         from doctor_services
										 where code in ('7079', '7080')
                                         group by date_begin
                                         order by date_begin);
/*принято всего*/
create temporary table if not exists T2 (select date_begin, count( id_case) as val
                                         from pervichnie
                                         where code in ('7078', '7077')
                                         group by date_begin
                                         order by date_begin);
/*повторные*/
create temporary table if not exists T3 (select date_begin, count( id_case) as val
                                         from pervichnie
                                         where code in ('7078')
                                         group by date_begin
                                         order by date_begin);
/*первичные*/                                           
create temporary table if not exists T4 (select date_begin, count( id_case) as val
                                         from pervichnie
                                         where code in ('7077')
                                         group by date_begin
                                         order by date_begin);
/*вкладки*/
create temporary table if not exists T5 (select date_begin, count(id_case) as val
                                               from doctor_services
                                               where code in
                                                     ('7103', '7104', '7105', '7106', '7107')
                                         group by date_begin
										order by date_begin);
/*металлические*/
create temporary table if not exists T6 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where code in ('7108', '7109')
                                         group by date_begin
                                         order by date_begin);
/*литые*/
create temporary table if not exists T7 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where code in ('7111')
                                         group by date_begin
                                         order by date_begin);
/*пластмассовые*/
create temporary table if not exists T8 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where code in ('7139', '7138')
                                         group by date_begin
                                         order by date_begin);
/*м/к*/
create temporary table if not exists T9 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where code in ('7112', '7222')
                                         group by date_begin
                                         order by date_begin);
/*м/к на импл*/
create temporary table if not exists T10 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where code in ('7124', '7125', '7126', '7127', '7120', '7121')
                                         group by date_begin
                                         order by date_begin);
/*безмет*/
create temporary table if not exists T11 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where code in ('7117', '7116', '7115', '7114')
                                         group by date_begin
                                         order by date_begin);
/*безмет на импл*/
create temporary table if not exists T13 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where code in ('7128', '7129', '7130', '7131', '7132', '7133', '7134')
                                         group by date_begin
                                         order by date_begin);
/*всего мостовидные*/
create temporary table if not exists T14 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('2006', '2007')
                                          group by date_begin
                                          order by date_begin);
/*цельнолитые*/
create temporary table if not exists T16 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('')
                                          group by date_begin
                                          order by date_begin);
/*коронок металл*/
create temporary table if not exists T17 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('7108', '7109')
                                          group by date_begin
                                          order by date_begin);
/*коронок м/к*/
create temporary table if not exists T18 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('')
                                          group by date_begin
                                          order by date_begin);
/*м/к на импл*/
create temporary table if not exists T19 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('7122')
                                          group by date_begin
                                          order by date_begin);
/*м/к*/
create temporary table if not exists T20 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('')
                                          group by date_begin
                                          order by date_begin);
/*литых*/
create temporary table if not exists T21 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('')
                                          group by date_begin
                                          order by date_begin);
/*протезы частичные отеч*/
create temporary table if not exists T22 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('7148', '7149', '7218')
                                          group by date_begin
                                          order by date_begin);
/*импорт*/
create temporary table if not exists T23 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('7149', '7151', '7152', '7285')
                                          group by date_begin
                                          order by date_begin);
/*термопл*/
create temporary table if not exists T24 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('7150', '7153')
                                          group by date_begin
                                          order by date_begin);
/*полные отеч*/
create temporary table if not exists T25 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('7219')
                                          group by date_begin
                                          order by date_begin);
/*импорт*/
create temporary table if not exists T26 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('7159', '7160', '7165')
                                          group by date_begin
                                          order by date_begin);
/*термопл*/
create temporary table if not exists T27 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('7177')
                                          group by date_begin
                                          order by date_begin);
/*клам*/
create temporary table if not exists T28 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('7226')
                                          group by date_begin
                                          order by date_begin);
/*замки*/
create temporary table if not exists T29 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('7227', '7226')
                                          group by date_begin
                                          order by date_begin);
/*Ketak*/
create temporary table if not exists T30 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('7143', '7147', '7145')
                                          group by date_begin
                                          order by date_begin);
/*цементировка свет*/
create temporary table if not exists T31 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('7146')
                                          group by date_begin
                                          order by date_begin);
/*снятие коронок(импл)*/
create temporary table if not exists T32 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('7175')
                                          group by date_begin
                                          order by date_begin);
/*снятие коронок (м/к)*/
create temporary table if not exists T33 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('7086')
                                          group by date_begin
                                          order by date_begin);
/*снятие коронок(штмап)*/
create temporary table if not exists T34 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('7084')
                                          group by date_begin
                                          order by date_begin);
/*альг Массы*/
create temporary table if not exists T35 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('7090')
                                          group by date_begin
                                          order by date_begin);
/*speedex*/
create temporary table if not exists T36 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('7091', '7092')
                                          group by date_begin
                                          order by date_begin);
/*а-силикон*/
create temporary table if not exists T37 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('7093')
                                          group by date_begin
                                          order by date_begin);
/*инд лож speedex*/
create temporary table if not exists T38 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('7095', '7097')
                                          group by date_begin
                                          order by date_begin);
/*починки протезов*/
create temporary table if not exists T39 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('7167', '7168', '7169', '7170', '7171', '7178')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists t40_1
(select ds.id_case,ds.date_begin,ds.ordercompletiondate from doctor_services ds
/*left join protocols p on p.id_case = ds.id_case
where (child_ref_ids like '%124161(%' or child_ref_ids like '%123431(%'
or child_ref_ids like '%123011(%' or child_ref_ids like '%129070(%'
or child_ref_ids like '%129080(%' or child_ref_ids like '%129090(%'
or child_ref_ids like '%129100(%' or child_ref_ids like '%129110(%'
or child_ref_ids like '%129120(%' or child_ref_ids like '%129130(%'
or child_ref_ids like '%136511(%')
and date_protocol between @date_start and @date_end*/
group by ds.id_case);
/*закончили*/
create temporary table if not exists T40 (select date_begin, count(distinct ds.id_case) as val
                                          from doctor_services ds
										  /*left join protocols p on ds.id_case = p.id_case
                                          where p.id in (select id from t40_1)*/
                                          where ds.ordercompletiondate is not null
										  group by date_begin
                                          order by date_begin);
/*УЕТ*/
 create temporary table if not exists T41 (select date_begin, ROUND(COALESCE(SUM(UET), 0), 2) as val
									from doctor_services
									group by date_begin
									order by date_begin);

create temporary table if not exists t_all_1
select fr.st_date, 
	coalesce(T1.val, 0), coalesce(T2.val, 0), coalesce(T3.val, 0),
    coalesce(T4.val, 0), coalesce(T5.val, 0), coalesce(T6.val, 0),
	coalesce(T7.val, 0), coalesce(T8.val, 0), coalesce(T9.val, 0),
	coalesce(T10.val, 0), coalesce(T11.val, 0), coalesce(T13.val, 0),
	coalesce(T14.val, 0), coalesce(T16.val, 0),
	coalesce(T17.val, 0), coalesce(T18.val, 0), coalesce(T19.val, 0),
	coalesce(T20.val, 0), coalesce(T21.val, 0), coalesce(T22.val, 0)
from first_row fr
         left join T1 on T1.date_begin = fr.st_date
         left join T2 on T2.date_begin = fr.st_date
         left join T3 on T3.date_begin = fr.st_date
         left join T4 on T4.date_begin = fr.st_date
         left join T5 on T5.date_begin = fr.st_date
         left join T6 on T6.date_begin = fr.st_date
         left join T7 on T7.date_begin = fr.st_date
         left join T8 on T8.date_begin = fr.st_date
         left join T9 on T9.date_begin = fr.st_date
         left join T10 on T10.date_begin = fr.st_date
         left join T11 on T11.date_begin = fr.st_date
         left join T13 on T13.date_begin = fr.st_date
         left join T14 on T14.date_begin = fr.st_date
         left join T16 on T16.date_begin = fr.st_date
         left join T17 on T17.date_begin = fr.st_date
         left join T18 on T18.date_begin = fr.st_date
         left join T19 on T19.date_begin = fr.st_date
         left join T20 on T20.date_begin = fr.st_date
         left join T21 on T21.date_begin = fr.st_date
         left join T22 on T22.date_begin = fr.st_date
order by fr.st_date ;

select ta1.*, coalesce(T23.val, 0), coalesce(T24.val, 0), coalesce(T25.val, 0),
	coalesce(T26.val, 0), coalesce(T27.val, 0), coalesce(T28.val, 0),
	coalesce(T29.val, 0), coalesce(T30.val, 0), coalesce(T31.val, 0),
	coalesce(T32.val, 0), coalesce(T33.val, 0), coalesce(T34.val, 0),
	coalesce(T35.val, 0), coalesce(T36.val, 0), coalesce(T37.val, 0),
	coalesce(T38.val, 0), coalesce(T39.val, 0), coalesce(T40.val, 0),
	coalesce(T41.val, 0)
from t_all_1 ta1
         left join T23 on T23.date_begin = ta1.st_date
         left join T24 on T24.date_begin = ta1.st_date
         left join T25 on T25.date_begin = ta1.st_date
         left join T26 on T26.date_begin = ta1.st_date
         left join T27 on T27.date_begin = ta1.st_date
         left join T28 on T28.date_begin = ta1.st_date
         left join T29 on T29.date_begin = ta1.st_date
         left join T30 on T30.date_begin = ta1.st_date
         left join T31 on T31.date_begin = ta1.st_date
         left join T32 on T32.date_begin = ta1.st_date
         left join T33 on T33.date_begin = ta1.st_date
         left join T34 on T34.date_begin = ta1.st_date
         left join T35 on T35.date_begin = ta1.st_date
         left join T36 on T36.date_begin = ta1.st_date
         left join T37 on T37.date_begin = ta1.st_date
         left join T38 on T38.date_begin = ta1.st_date
         left join T39 on T39.date_begin = ta1.st_date
         left join T40 on T40.date_begin = ta1.st_date
         left join T41 on T41.date_begin = ta1.st_date
order by ta1.st_date ;