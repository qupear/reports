create temporary table if not exists doctor_services (select cs.date_begin, cs.id_case, vp.profile_infis_code, p.code,
                                                             vd.diagnosis_code, cs.tooth_name, T.uet, cs.is_oms,
                                                             case
                                                                 when YEAR(CS.DATE_BEGIN) - YEAR(pd.BIRTHDAY)
    - (DATE_FORMAT(CS.DATE_BEGIN, '%m%d') < DATE_FORMAT(pd.BIRTHDAY, '%m%d'))  >= 60 and pd.SEX = 'м'
                                                                     then 0
                                                                 WHEN YEAR(CS.DATE_BEGIN) - YEAR(pd.BIRTHDAY)
    - (DATE_FORMAT(CS.DATE_BEGIN, '%m%d') < DATE_FORMAT(pd.BIRTHDAY, '%m%d'))  >= 55 and pd.SEX = 'ж'
                                                                     then 1 else -1 end as old_people, pd.ID_PATIENT
                                                      from case_services cs
                                                               left join cases c on cs.id_case = c.id_case
                                                               left join patient_data pd on c.ID_PATIENT = pd.id_patient
                                                               left join vmu_profile vp on vp.id_profile = cs.id_profile and cs.is_oms = 1
                                                               left join price p on p.id = cs.id_profile and cs.is_oms in (0, 3)
                                                               left join vmu_diagnosis vd on cs.id_diagnosis = vd.id_diagnosis
															   left join doctor_spec ds on cs.id_doctor = ds.doctor_id
                                                               left join (select vt.uet, vt.id_profile, vt.id_zone_type,
                                                                                 vt.tariff_begin_date as d1,
                                                                                 vt.tariff_end_date as d2,
                                                                                 vtp.tp_begin_date as d3,
                                                                                 vtp.tp_end_date as d4
                                                                          from vmu_tariff vt,
                                                                               vmu_tariff_plan vtp
                                                                          where vtp.id_tariff_group = vt.id_lpu
                                                                            And vtp.id_lpu in (select id_lpu from mu_ident)) T
                                                                         on T.id_profile = vp.id_profile and
                                                                            cs.date_begin between T.d1 and T.d2 and
                                                                            cs.date_begin between T.d3 and T.d4 and
                                                                            T.id_zone_type = cs.id_net_profile and
                                                                            cs.is_oms = 1
                                                      where (cs.id_doctor = @doc_id or @doc_id = 0) 
														and (ds.department_id = @depart_id or @depart_id = 0)
                                                        and cs.date_begin between @date_start and @date_end);

create temporary table if not exists doctor_protocols (select ds.id_case, ds.date_begin,
                                                              locate('17239', p.child_ref_ids) > 0 as p17239,
                                                              locate('17251', p.child_ref_ids) > 0 as p17251,
                                                              locate('18720', p.child_ref_ids) > 0 as p18720,
                                                              locate('19010', p.child_ref_ids) > 0 as p19010,
                                                              locate('44170', p.child_ref_ids) > 0 as p44170,
                                                              locate('47780', p.child_ref_ids) > 0 as p47780,
                                                              locate('79760', p.child_ref_ids) > 0 as p79760,
                                                              locate('80370', p.child_ref_ids) > 0 as p80370,
                                                              locate('18730', p.child_ref_ids) > 0 as p18730,
                                                              locate('80380', p.child_ref_ids) > 0 as p80380,
                                                              locate('55791', p.child_ref_ids) > 0 as p55791,
                                                              locate(';5761', p.child_ref_ids) > 0 as p5761,
                                                              locate(';4781', p.child_ref_ids) > 0 as p4781,
                                                              locate(';3790', p.child_ref_ids) > 0 as p3790,
                                                              locate(';2350', p.child_ref_ids) > 0 as p2350,
                                                              locate(';1392', p.child_ref_ids) > 0 as p1392,
                                                              locate(';480', p.child_ref_ids) > 0 as p480
                                                       from doctor_services ds
                                                                left join protocols p on p.id_case = ds.id_case
                                                       where ds.is_oms = 1
                                                         and (p.id_doctor = @doc_id or @doc_id = 0));

set @row = 0;
create temporary table if not exists first_row (select @row := @row + 1 as st_num, T.date_begin as st_date
                                                from (select date_begin
                                                      from doctor_services ds
where ds.is_oms = 1
                                                      group by date_begin
                                                      order by date_begin) T);
/*принято больных*/
create temporary table if not exists T1 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where profile_infis_code in ('стт005', 'стт006', 'стт054', 'стт055', 'нстт005', 'нстт006', 'нстт054', 'нстт055')
                                         group by date_begin
                                         order by date_begin);
/*из них первичных*/
create temporary table if not exists T2 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where profile_infis_code in ('стт005', 'нстт005', 'стт054', 'нстт054')
                                         group by date_begin
                                         order by date_begin);
/*запломбировано всего*/
create temporary table if not exists T3
    (select T.date_begin, count(T.val) as val
     from (select date_begin, count(id_case) as val
           from doctor_services
           where profile_infis_code in ('стт025', 'стт028', 'стт031')
           group by id_case, tooth_name
           order by date_begin) T
     group by date_begin);
/*кариес*/
create temporary table if not exists T4
(select T.date_begin, count(T.val) as val
from (select date_begin, count(id_case) as val
      from doctor_services
      where profile_infis_code in ('стт025', 'стт028', 'стт031')
        and diagnosis_code in
            ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5', 'k02.6', 'k02.7', 'k02.8', 'k02.9', 'k03.2')
      group by id_case, tooth_name
      order by date_begin) T
group by date_begin);
/*пульпит*/
create temporary table if not exists T5
(select T.date_begin, count(T.val) as val
from (select date_begin, count(id_case) as val
      from doctor_services
      where profile_infis_code in ('стт025', 'стт028', 'стт031')
        and diagnosis_code in ('k04.0', 'k04.1', 'k04.2', 'k04.3')
      group by id_case, tooth_name
      order by date_begin) T
group by date_begin);
/*периодонтит*/
create temporary table if not exists T6
(select T.date_begin, count(T.val) as val
from (select date_begin, count(id_case) as val
      from doctor_services
      where profile_infis_code in ('стт025', 'стт028', 'стт031')
        and diagnosis_code in ('k04.4', 'k04.5')
      group by id_case, tooth_name
      order by date_begin) T
group by date_begin);
/*девит*/
create temporary table if not exists T7
(select T.date_begin, count(T.val) as val
from (select date_begin, count(id_case) as val
      from doctor_services
      where profile_infis_code in ('стт053', 'нстт053')
		and diagnosis_code in ('k04.0')
      group by id_case, tooth_name
      order by date_begin) T
group by date_begin);

/*анестезия*/
create temporary table if not exists T8
    (select date_begin, count(id_case) as val
     from doctor_services
     where profile_infis_code in ('сто001', 'сто002', 'сто003', 'нсто001', 'нсто002', 'нсто003')
     group by date_begin
     order by date_begin);
/*сложный кариес*/
create temporary table if not exists T9_1
   (select ds.date_begin, ds.id_case as val
           from doctor_services ds
           where profile_infis_code in ('стт026', 'стт029', 'стт027', 'стт030', 'стт032', 'стт033')
			and diagnosis_code in ('k04.0')
     group by ds.date_begin, ds.id_case
 order by ds.date_begin);
create temporary table if not exists T9_2
   (select ds.date_begin, ds.id_case as val
           from doctor_services ds
			inner join T9_1 on T9_1.val = ds.id_case
           where ds.profile_infis_code in ('стт025', 'стт028', 'стт031')
     group by ds.date_begin,ds.id_case
 order by ds.date_begin);
create temporary table if not exists T9_3
   (select ds.date_begin, ds.id_case as val
           from doctor_services ds
			inner join T9_2 on  T9_2.val = ds.id_case
           where ds.profile_infis_code in ('стт037')
     group by ds.date_begin,ds.id_case
 order by ds.date_begin);
create temporary table if not exists T9_4
   (select ds.date_begin, ds.id_case as val
           from doctor_services ds
			inner join T9_3 on T9_3.val = ds.id_case
           where ds.profile_infis_code in ('стт038', 'стт039')
     group by ds.date_begin,ds.id_case
 order by ds.date_begin);
create temporary table if not exists T9_5
   (select ds.date_begin, ds.id_case as val
           from doctor_services ds
			inner join T9_3 on T9_3.val = ds.id_case
           where ds.profile_infis_code in ('стт044')
     group by ds.date_begin,ds.id_case
 order by ds.date_begin);
create temporary table if not exists T9_6
   (select ds.date_begin, ds.id_case as val
           from doctor_services ds
			inner join T9_3 on T9_3.val = ds.id_case
           where ds.profile_infis_code in ('стт045')
     group by ds.date_begin,ds.id_case
 order by ds.date_begin);
create temporary table if not exists T9_7
   (select ds.date_begin, ds.id_case as val
           from doctor_services ds
           where profile_infis_code in ('нстт026', 'нстт029', 'нстт027', 'нстт030', 'нстт032', 'нстт033')
			and diagnosis_code in ('k04.0')
     group by ds.date_begin,ds.id_case
 order by ds.date_begin);
create temporary table if not exists T9_8
   (select ds.date_begin, ds.id_case as val
           from doctor_services ds
			inner join T9_7 on T9_7.val = ds.id_case
           where ds.profile_infis_code in ('нстт025', 'нстт028', 'нстт031')
     group by ds.date_begin,ds.id_case
 order by ds.date_begin);
create temporary table if not exists T9_9
   (select ds.date_begin, ds.id_case as val
           from doctor_services ds
			inner join T9_8 on T9_8.val = ds.id_case
           where ds.profile_infis_code in ('нстт037', 'нстт038', 'нстт039')
     group by ds.date_begin,ds.id_case
 order by ds.date_begin);
create temporary table if not exists T9
   (select ds.date_begin, count(distinct id_case) as val
           from doctor_services ds
           where (ds.id_case in (select val from T9_4)) or (ds.id_case in (select val from T9_5)) or 
			(ds.id_case in (select val from T9_6)) or (ds.id_case in (select val from T9_9))
     group by date_begin
 order by date_begin);
/*поставлено пломб всего */
create temporary table if not exists T10
   (select date_begin, count(id_case) as val
           from doctor_services
           where profile_infis_code in ('стт034', 'стт026', 'стт029', 'стт027', 'стт030', 'стт032', 'стт033', 'нстт034')
     group by date_begin
 order by date_begin);
/*временные пломбы*/
create temporary table if not exists T11
   (select date_begin, count(id_case) as val
           from doctor_services
           where profile_infis_code in ('стт034', 'нстт034')
     group by date_begin
order by date_begin);
/*цемент*/
create temporary table if not exists T12
(select date_begin, count(id_case) as val
           from doctor_services
           where profile_infis_code in ('стт026', 'стт029', 'стт032')
     group by date_begin
order by date_begin);
/*композит*/
create temporary table if not exists T13
 (select date_begin, count(id_case) as val
           from doctor_services
           where profile_infis_code in ('стт027', 'стт030', 'стт033')
     group by date_begin
order by date_begin);
/*снято зубных отложений*/
create temporary table if not exists T14
    (select date_begin, count(distinct id_case) as val
     from doctor_services
     where profile_infis_code in ('стт041', 'стт048')
     group by date_begin
     order by date_begin);
/*санировано*/
create temporary table if not exists T15
    (select count(distinct ds.id_case) as val, ds.date_begin
     from doctor_services ds
              left join protocols p on ds.id_case = p.id_case
     where ds.is_oms = 1
       and p.id_ref in
           (52282, 52852, 66962, 65862, 67902, 69102, 70182, 71352, 118451, 118051, 46162, 48242, 49532, 113791, 113141,
            112461, 111861, 111251, 110271)
     group by date_begin
     order by date_begin);
/*м старше 60*/
create temporary table if not exists T16 (select date_begin, count(distinct ID_PATIENT) as val
                                          from doctor_services
where old_people = 0 and is_oms = 1
                                          group by date_begin
                                          order by date_begin);
/*ж старше 55*/
create temporary table if not exists T17 (select date_begin, count(distinct ID_PATIENT) as val
                                          from doctor_services
where old_people = 1  and is_oms = 1
                                          group by date_begin
                                          order by date_begin);
/*беременные child_ref*/
create temporary table if not exists t18_1_id
(select id from protocols p
where (child_ref_ids like '%118730(%' or child_ref_ids like '%118750(%'
or child_ref_ids like '%118770(%' or child_ref_ids like '%118790(%'
or child_ref_ids like '%118810(%' or child_ref_ids like '%118830(%'
or child_ref_ids like '%118850(%' or child_ref_ids like '%118870(%'
or child_ref_ids like '%118890(%' or child_ref_ids like '%118910(%'
or child_ref_ids like '%118930(%' or child_ref_ids like '%118950(%'
or child_ref_ids like '%118970(%' or child_ref_ids like '%118990(%'
or child_ref_ids like '%119010(%' or child_ref_ids like '%119030(%'
or child_ref_ids like '%119050(%' or child_ref_ids like '%119070(%'
or child_ref_ids like '%119090(%' or child_ref_ids like '%119110(%'
or child_ref_ids like '%119130(%' or child_ref_ids like '%119150(%'
or child_ref_ids like '%119170(%' or child_ref_ids like '%119190(%'
or child_ref_ids like '%119210(%' or child_ref_ids like '%119230(%'
or child_ref_ids like '%119250(%' or child_ref_ids like '%119270(%'
or child_ref_ids like '%119290(%' or child_ref_ids like '%119310(%'
or child_ref_ids like '%119330(%' or child_ref_ids like '%119350(%'
or child_ref_ids like '%119400(%' or child_ref_ids like '%119420(%'
or child_ref_ids like '%119440(%' or child_ref_ids like '%119460(%'
or child_ref_ids like '%119480(%' or child_ref_ids like '%119500(%'
or child_ref_ids like '%119520(%' or child_ref_ids like '%119550(%'
or child_ref_ids like '%119590(%' or child_ref_ids like '%119630(%'
or child_ref_ids like '%119670(%' or child_ref_ids like '%119710(%'
or child_ref_ids like '%119750(%' or child_ref_ids like '%119790(%'
or child_ref_ids like '%119830(%' or child_ref_ids like '%119870(%')
and date_protocol between @date_start and @date_end);

/*беременные*/
create temporary table if not exists T18_1 (select count(distinct ds.ID_case)  as val, date_begin
                                          from doctor_services ds
left join protocols p on ds.id_case = p.id_case
where ds.is_oms = 1 and p.id in (select id from t18_1_id)
                                          
                                          order by date_begin);

/*ИВОВ chil ref*/
create temporary table if not exists t18_2_id
(select id from protocols p
where (child_ref_ids like '%3464(%' or child_ref_ids like '%4124(%'
or child_ref_ids like '%44342(%' or child_ref_ids like '%46472(%'
or child_ref_ids like '%51732(%' or child_ref_ids like '%52382(%'
or child_ref_ids like '%54402(%' or child_ref_ids like '%55392(%'
or child_ref_ids like '%56222(%' or child_ref_ids like '%57012(%'
or child_ref_ids like '%57842(%' or child_ref_ids like '%58692(%'
or child_ref_ids like '%59842(%' or child_ref_ids like '%60682(%'
or child_ref_ids like '%61623(%' or child_ref_ids like '%62702(%'
or child_ref_ids like '%63562(%' or child_ref_ids like '%65142(%'
or child_ref_ids like '%66072(%' or child_ref_ids like '%67152(%'
or child_ref_ids like '%68312(%' or child_ref_ids like '%69172(%'
or child_ref_ids like '%70322(%' or child_ref_ids like '%72192(%'
or child_ref_ids like '%73022(%' or child_ref_ids like '%73842(%'
or child_ref_ids like '%74702(%' or child_ref_ids like '%103312(%'
or child_ref_ids like '%106652(%' or child_ref_ids like '%107212(%'
or child_ref_ids like '%107742(%' or child_ref_ids like '%108272(%'
or child_ref_ids like '%108712(%' or child_ref_ids like '%109172(%'
or child_ref_ids like '%109642(%' or child_ref_ids like '%114012(%'
or child_ref_ids like '%115133(%' or child_ref_ids like '%116572(%'
or child_ref_ids like '%117242(%' or child_ref_ids like '%119560(%'
or child_ref_ids like '%119600(%' or child_ref_ids like '%119640(%'
or child_ref_ids like '%119680(%' or child_ref_ids like '%119720(%'
or child_ref_ids like '%119760(%' or child_ref_ids like '%119800(%'
or child_ref_ids like '%119840(%' or child_ref_ids like '%119880(%')
and date_protocol between @date_start and @date_end);
/*ИВОВ*/
create temporary table if not exists T18_2 (select count(distinct ds.id_case)  as val, date_begin
                                          from doctor_services ds
left join protocols p on ds.id_case = p.id_case
where ds.is_oms = 1 and p.id in (select id from t18_2_id)
                                          group by date_begin
                                          order by date_begin);
/*YET*/
create temporary table if not exists T19
    (select date_begin, ROUND(COALESCE(SUM(UET), 0), 2) as val
     from doctor_services
     group by date_begin
     order by date_begin);
 
select fr.st_num, fr.st_date, coalesce(T1.val, 0), coalesce(T2.val, 0), coalesce(T3.val, 0),
       coalesce(T4.val, 0), coalesce(T5.val, 0), coalesce(T6.val, 0), coalesce(T7.val, 0) as 'девит', coalesce(T8.val, 0),
       coalesce(T9.val, 0) as 'осложн', coalesce(T10.val, 0),  coalesce(T11.val, 0),
       coalesce(T12.val, 0), coalesce(T13.val, 0), coalesce(T14.val, 0), coalesce(T15.val, 0), coalesce(T16.val, 0),
       coalesce(T17.val, 0), coalesce(T18_1.val, 0), coalesce(T18_2.val, 0), coalesce(T19.val, 0)
from first_row fr
         left join T1 on T1.date_begin = fr.st_date
         left join T2 on T2.date_begin = fr.st_date
         left join T3 on T3.date_begin = fr.st_date
         left join T4 on T4.date_begin = fr.st_date
         left join T5 on T5.date_begin = fr.st_date
         left join T6 on T6.date_begin = fr.st_date
         left join T7 on T7.date_begin = fr.st_date
         left join T8 on T8.date_begin = fr.st_date
         left join T9 on T9.date_begin = fr.st_date
         left join T10 on T10.date_begin = fr.st_date
         left join T11 on T11.date_begin = fr.st_date
         left join T12 on T12.date_begin = fr.st_date
         left join T13 on T13.date_begin = fr.st_date
         left join T14 on T14.date_begin = fr.st_date
         left join T15 on T15.date_begin = fr.st_date
         left join T16 on T16.date_begin = fr.st_date
         left join T17 on T17.date_begin = fr.st_date
         left join T18_1 on T18_1.date_begin = fr.st_date
		 left join T18_2 on t18_2.date_begin = fr.st_date
         left join T19 on T19.date_begin = fr.st_date
order by st_num;
