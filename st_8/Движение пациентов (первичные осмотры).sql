
set @date_start = '2019-07-01';
set @date_end = '2019-07-31';
set @doc_id = 0;
set @depart_id = 0;
set @closed = 0;
set @fin_type = 0;
set @order_state = 0;
  create temporary table if not exists pzp(select c.ID_PATIENT, min(cs.date_begin) as date_begin, c.ID_DOCTOR, pd.id_human
 from case_services cs                                                    left join price p on cs.ID_PROFILE = p.ID
                                                     left join cases c on cs.ID_CASE = c.ID_CASE
                                            left join orders o on o.id = cs.id_order
 											left join patient_data pd on pd.id_patient = c.id_patient
                         where cs.DATE_BEGIN between @date_start and @date_end
                     and p.code in ('51001', '7077') and ((cs.id_order > 0 and o.order_type in (0, 2)) or (cs.id_order = -1 and cs.is_oms = 0))
                                           group by pd.id_human);
 create temporary table if not exists bzp(select o.Id_Human, min(cs.date_begin) as date_begin, o.id_doctor
                                           from case_services cs
                                        left join price_orto_soc pos on cs.ID_PROFILE = pos.id
                                                    left join orders o on cs.ID_ORDER = o.id
                                        where cs.DATE_BEGIN between @date_start and @date_end
                                             and pos.code in ('1.4', '1.1')
                 and o.order_type = 1        group by o.id_human);

  create temporary table if not exists pzp_1 (select * from pzp);
 create temporary table if not exists bzp_1 (select * from bzp);
 set @row_pat = 0;
 create temporary table if not exists row_pat select  @row_pat:=@row_pat+1 as row_p, T.* from (select pd.id_human, concat_ws(' ', pd.SURNAME, pd.NAME, pd.SECOND_NAME) pat
 from orders o
 left join patient_data pd on pd.id_human = o.id_human
 left join pzp on pzp.id_human = pd.id_human and pd.is_active = 1 and pd.DATE_END = '2200-01-01'
 left join bzp on bzp.id_human = o.id_human
 left join doctor_spec ds on ds.doctor_id = o.id_doctor
 where (@fin_type and bzp.id_human is not null) or (not @fin_type and pzp.id_patient is not null)
 and (ds.doctor_id = @doc_id or @doc_id = 0)
             and ((o.STATUS < 2 and not @order_state)
             or (o.STATUS = 2 and @order_state))
 and (ds.department_id = @depart_id
             or @depart_id = 0)
 and (o.orderCreateDate >= @date_start)
 group by o.id_human
 order by pat) T;
 set @row = 0;
 select
     @row:=@row + 1, T . *
 from
     (select
         u.name,
             pd.CARD_NUMBER,
             o.Order_Number,
             o.orderCreateDate,
             (date_format(coalesce(o.orderCompletionDate, '-'), '%d-%m-%Y')),
             concat(pat, ' /пациент № ', rp.row_p) pat,
             coalesce(pos.date_begin, s.date_begin),
             concat(pa.UNSTRUCT_ADDRESS, ' ', pd.REMARK),
             sum(cs.SERVICE_COST)
     from
         orders o
     left join case_services cs ON cs.ID_ORDER = o.id
     left join patient_data pd ON o.Id_Human = pd.ID_HUMAN
         and pd.IS_ACTIVE = 1
         and pd.DATE_END = '2200-01-01'
     left join doctor_spec ds ON o.id_doctor = ds.doctor_id
     left join users u ON ds.user_id = u.id
     left join patient_address pa ON pd.ID_ADDRESS_REG = pa.ID_ADDRESS
     left join price_orto_soc pos2 ON cs.ID_PROFILE = pos2.id
         and o.order_type = 1
     join row_pat rp ON rp.id_human = pd.id_human
     left join pzp_1 s ON pd.ID_PATIENT = s.ID_PATIENT
         and not @fin_type
     left join bzp_1 pos ON pd.ID_human = pos.ID_human and @fin_type
     where
         (ds.doctor_id = @doc_id or @doc_id = 0)
             and ((o.STATUS < 2 and not @order_state)
             or (o.STATUS = 2 and @order_state))
             and ((o.order_type in (0, 2) and not @fin_type)
             or (o.order_type = 1 and @fin_type
             and pos2.code <> '1.4'))
             and (ds.department_id = @depart_id
             or @depart_id = 0)
             and (o.orderCreateDate >= @date_start)
     group by pd.ID_HUMAN , ds.doctor_id , o.ID
     order by pd.surname , pd.name , pd.SECOND_NAME , u.name , o.id) T;