set @date_start = '2019-01-01';
set @date_end = '2019-07-07';
select dep.name,
       vo.COUNTRY_SHORT_NAME, short_name(concat_ws(' ', pd.SURNAME, pd.name, pd.SECOND_NAME)) as pat,
       pd.POLIS_NUMBER,
       case when cs.IS_OMS = 1 then sum(cs.SERVICE_COST) end as oms,
       case when cs.IS_OMS = 0 then sum(cs.SERVICE_COST) end as pu
       from case_services cs
left join cases c on cs.ID_CASE = c.ID_CASE
left join patient_data pd on c.ID_PATIENT = pd.ID_PATIENT
left join vmu_oksm vo on pd.C_OKSM = vo.CODE
           left join doctor_spec ds on cs.ID_DOCTOR = ds.doctor_id
left join departments dep on ds.department_id = dep.id
where cs.DATE_BEGIN between @date_start and @date_end and c.IS_CLOSED = 1
and pd.C_OKSM <> 643 and pd.IS_ACTIVE = 1 and pd.DATE_END = '2200-01-01'
group by dep.name, vo.ID_OKSM, pd.ID_PATIENT
order by dep.name, vo.COUNTRY_SHORT_NAME, pat