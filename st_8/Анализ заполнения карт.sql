set @date_start = '2021-01-21';
set @date_end = '2021-01-21';

drop temporary table if exists main , novivistors;

create temporary table if not exists main
select 
    dep.name depart,
    u.name user,
    count(distinct ap.case_id) sl,
    count(distinct p.id_case) prot,
    count(DISTINCT sig.ID) AS signed
from
    appointments ap
        left join protocols p ON p.id_case = ap.case_id
        left join doctor_spec ds ON ds.doctor_id = ap.doctor_id
        left join departments dep ON dep.id = ds.department_id
        left join users u ON u.id = ds.user_id
        LEFT JOIN SIGNATURES sig ON ap.CASE_ID = sig.ID_CASE
where
    Date(ap.time) >= @date_start
        AND Date(ap.time) <= @date_end
        and ap.case_id > - 1
	and ap.done <> 2
group by u.name , dep.name
order by depart , user;

create temporary table if not exists nonvivistors
select
    dep.name depart,
    u.name user,
    count(distinct ap.case_id) sl1

from
    appointments ap
        left join protocols p ON p.id_case = ap.case_id
        left join doctor_spec ds ON ds.doctor_id = ap.doctor_id
        left join departments dep ON dep.id = ds.department_id
        left join users u ON u.id = ds.user_id
        LEFT JOIN SIGNATURES sig ON ap.CASE_ID = sig.ID_CASE
where
    Date(ap.time) >= @date_start
        AND Date(ap.time) <= @date_end
        and ap.case_id > - 1
	and ap.done <> 2
	and p.id_ref in (8484, 30641, 33740, 33750, 33760, 33770, 33780, 33790, 33800, 34140,
						34280, 34370, 37331, 145640, 145650, 145660, 145670, 145680) 
group by u.name , dep.name
order by depart , user;

select m.*, 
	 case when nv.sl1 is null then "0" else nv.sl1 end as nvstr

from main m
left join nonvivistors nv on m.depart = nv.depart and m.user = nv.user
order by depart,user