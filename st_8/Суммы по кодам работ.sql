select concat(coalesce(p.code, ''), coalesce(pos.code, '')), concat(coalesce(p.name, ''), coalesce(pos.name, '')),
       coalesce(p.cost, 0) + coalesce(pos.cost, 0), count(cs.id_profile), sum(coalesce(cs.service_cost, 0))
from orders ord
         left join case_services cs on cs.id_order = ord.id
         left join price p on p.id = cs.id_profile and ord.order_type <> 1
         left join price_orto_soc pos on pos.id = cs.id_profile and ord.order_type = 1
LEFT JOIN doctor_spec ds on cs.id_doctor = ds.doctor_id
where ((ord.orderCompletionDate between @date_start and @date_end and ord.status = 2 and @order_state) or
       (ord.orderCreateDate between @date_start and @date_end and ord.status < 2 and not @order_state))
  and ((ord.order_type = 1 and @fin_type = 2) or (ord.order_type = 0 and @fin_type = 0) or (ord.order_type = 2 and @fin_type = 3))
  and cs.id_service is not null
  and (ord.id_doctor = @doc_id or @doc_id = 0 or @doc_id is null)
AND (ds.department_id = @depart_id OR @depart_id = 0)
AND (@techn_id = ORD.id_technician OR @techn_id = 0)
group by cs.id_profile
order by concat(coalesce(p.code, ''), coalesce(pos.code, ''));