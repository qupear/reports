set @row=0; set @row_pat = 0;   drop temporary table if exists ord_with_cerv;
  drop temporary table if exists cost;
   drop temporary table if exists row_pat;create temporary table if not exists ord_with_cerv
  (select o.id_human as id_h,   min(o.ordercreatedate) as date,   o.order_number,
  		concat_ws(' ', pd.surname, pd.name, pd.SECOND_NAME, pd.card_number) as pat,
 o.bzp_num, o.bzp_date, o.bzp_region, o.id i
  		from orders o
  left join case_services cs on o.id = cs.id_order
  left join patient_data pd on o.id_human = pd.id_human
  where o.order_type = 1 and cs.id_service is not null
  and  (( o.status = 2 and @order_state)
  or (o.status < 2 and not @order_state))
  group by o.id_human, o.bzp_num
 -- , o.bzp_date, o.bzp_region
   having (min(o.ordercreatedate) between @date_start and @date_end)
 order by pat, ordercreatedate)  ;

  create temporary table if not exists row_pat
 select @row_pat:=@row_pat+1 as rowp, T.* from (select *, min(date) as d from ord_with_cerv owc
 	group by id_h, bzp_num, bzp_date, bzp_region
 order by pat,date) T
  ;

  create temporary table if not exists cost
  	(select o.id_human, cs.id_order, o.order_number, concat('ОСЗН ', d.name, ' № ', o.bzp_num, ' от ', o.bzp_date) bzp, o.orderCreateDate,
   sum(cs.service_cost) as summ, o.bzp_num, o.bzp_date, o.bzp_region
  from case_services cs
  left join orders o on o.id = cs.id_order
 left join districts d on o.bzp_region = d.id
  where cs.id_order > 0 and o.order_type = 1
  group by o.id_human, cs.id_order);

  select @row:=@row+1 , T.* from (select rp.rowp, concat(' ', owc.pat, ' , дата начала санации ',
  date_format(rp.d, '%d-%m-%Y')) ro,
   cost.order_number, date_format(cost.ordercreatedate, '%d-%m-%Y'), cost.bzp, cost.summ, cost.id_human
  from ord_with_cerv owc
  left join
  cost on owc.bzp_num = cost.bzp_num and owc.bzp_date = cost.bzp_date and owc.bzp_region = cost.bzp_region
 left join row_pat rp on owc.bzp_num = rp.bzp_num and owc.bzp_date = rp.bzp_date and owc.bzp_region = rp.bzp_region
  group by owc.pat, rp.d, cost.id_order
  order by owc.pat,  cost.ordercreatedate, cost.order_number ) T