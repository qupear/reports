CREATE TEMPORARY TABLE IF NOT EXISTS foreigners
	(SELECT vo.country_short_name, pd.id_patient, pd.id_human
		FROM
			patient_data           pd
				LEFT JOIN vmu_oksm vo ON vo.code = pd.c_oksm
		WHERE vo.code IS NOT NULL AND vo.code <> 643);
CREATE TEMPORARY TABLE IF NOT EXISTS foreign_cases
	(SELECT c.id_case, c.id_patient
		FROM cases c
		WHERE c.date_begin BETWEEN @date_start AND @date_end AND c.id_patient IN (SELECT id_patient FROM foreigners));
CREATE TEMPORARY TABLE IF NOT EXISTS foreign_cases1
	(SELECT id_case FROM foreign_cases);



select 'Стоматологическая помощь' as txt, case cs.is_oms when 0 then 'ПУ' else 'ОМС' end, f.country_short_name,
       count(distinct f.id_human), sum(coalesce(p.cost, 0) + coalesce(T.price, 0))
from case_services cs
         left join foreign_cases fc on fc.id_case = cs.id_case
         left join foreigners f on f.id_patient = fc.id_patient
         left join price p on p.id = cs.id_profile and cs.is_oms = 0
         left join vmu_profile vp on vp.id_profile = cs.id_profile and cs.is_oms = 1
         left join (select vt.uet, vt.id_profile, vt.id_zone_type, vt.price, vt.tariff_begin_date as d1,
                           vt.tariff_end_date as d2, vtp.tp_begin_date as d3, vtp.tp_end_date as d4
                    from vmu_tariff vt,
                         vmu_tariff_plan vtp
                    where vtp.id_tariff_group = vt.id_lpu
                      And vtp.id_lpu in (select id_lpu from mu_ident)) T
                   on T.id_profile = vp.id_profile and cs.date_begin between T.d1 and T.d2 and
                      cs.date_begin between T.d3 and T.d4 and T.id_zone_type = cs.id_net_profile and cs.is_oms = 1
where cs.id_case in (select id_case from foreign_cases1)
group by f.country_short_name, cs.is_oms
order by f.country_short_name, cs.is_oms;