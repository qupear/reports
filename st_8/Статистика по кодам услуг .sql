select p.code, p.name as serv_name, count(p.code), sum(p.uet), sum(p.materials)
from case_services cs
         left join price p on cs.id_profile = p.id
         left join doctor_spec ds on cs.id_doctor = ds.doctor_id
         left join users u on u.id = ds.user_id
where cs.is_oms = 0
  and cs.date_begin between @date_start and @date_end
  and cs.id_doctor = @doc_id
GROUP BY P.code, CS.id_doctor
ORDER BY cs.id_doctor, p.code;
