
  set @row = 0; create temporary table if not exists RG
  (select id_human as id_h, id_ref, date_protocol as d1
                         from protocols prot
                         where prot.id_ref in (9470, 8459, 8464)
 and date_protocol >= @date_start
                         group by id_human, id_ref);
  create temporary table if not exists ALLER
  (select id_human as id_h, id_ref, date_protocol as d2
                         from protocols prot
                         where prot.id_ref in (9491, 8480)
                        and date_protocol >= @date_start
  group by id_human, id_ref);
  create temporary table if not exists TER
  (select id_human as id_h, id_ref, date_protocol as d3
                         from protocols prot
                         where prot.id_ref in (9492, 8461)
                        and date_protocol >= @date_start
  group by id_human, id_ref);
  create temporary table if not exists SURG
  (select id_human as id_h, id_ref, date_protocol as d4
                         from protocols prot
                         where prot.id_ref in (9493, 8470, 8462)
                         and date_protocol >= @date_start
 group by id_human, id_ref);
 
  create temporary table if not exists pu_primaries 
 (select pd.id_patient, pd.id_human 
 from case_services cs 
 left join cases c on c.id_case = cs.id_case 
 left join patient_data pd on pd.id_patient = c.id_patient 
 left join price p on p.id = cs.id_profile 
 where cs.date_begin between @date_start  and @date_end
   and cs.is_oms = 0 and cs.id_order = -1  and cs.is_paid > 0 
   and p.code = '51001'   
 group by pd.id_patient  );
 
 create temporary table if not exists pu_continued_cure 
 (select pd.id_patient, pd.id_human 
 from case_services cs 
 left join cases c on c.id_case = cs.id_case 
 left join patient_data pd on pd.id_patient = c.id_patient 
 left join orders ord on ord.id = cs.id_order 
 left join price p on p.id = cs.id_profile 
 where cs.date_begin between @date_start  and @date_end
   and cs.is_oms = 0 and ord.order_type <> 1 
   and p.code <> '51001' and pd.id_patient in (select id_patient from pu_primaries)  
 group by pd.id_patient  );
  
  create temporary table if not exists main
 (select pd.id_human,
     short_name(u.name) as doc_name,
     concat(pd.surname, ' ', pd.name, ' ', pd.second_name, ' д.р. ', pd.birthday) as pat,
 pd.card_number, cs.date_begin,  pa.unstruct_address, pd.remark   
 from case_services cs 
 left join cases c on c.id_case = cs.id_case 
 left join patient_data pd on pd.id_patient = c.id_patient 
 left join patient_address pa on pa.id_address = pd.id_address_reg 
 left join doctor_spec ds on ds.doctor_id = cs.id_doctor 
 left join users u on ds.user_id = u.id and u.id <> 1 
 left join departments dep on ds.department_id = dep.id
 left join price p on p.id = cs.id_profile 
  where cs.date_begin between @date_start  and @date_end and ds.user_id <> 1 and
 cs.is_oms = 0 and cs.id_order = -1 and ds.spec_id = 107
 and p.code = '51001' and pd.id_patient not in (select id_patient from pu_continued_cure)
 and (dep.id = @depart_id or @depart_id = 0)
 group by pd.id_patient  
 order by dep.name, doc_name, pd.surname, pd.name, pd.second_name );
 
  create temporary table if not exists visits
 (select pd.id_human, concat(date_format(cs.DATE_BEGIN, '%d-%m-%Y'), '  ', short_name(u.name)) doc_v
 from case_services cs
 left join cases c on cs.ID_CASE = c.ID_CASE
 left join patient_data pd on c.ID_PATIENT = pd.ID_PATIENT
 left join main m on pd.ID_human = m.id_human
 left join doctor_spec ds on cs.ID_DOCTOR = ds.doctor_id
 left join users u on ds.user_id = u.id
 where cs.date_begin >= m.date_begin
 and (ds.spec_id = 106 or ds.spec_id = 108 or ds.spec_id = 208 or ds.spec_id = 211)
 group by pd.id_human, cs.id_case, cs.id_doctor
 order by pd.id_human, cs.DATE_BEGIN);
  
  select @row := @row+1, T.* from(
 select m.doc_name, m.pat, m.card_number, m.unstruct_address, m.remark, date_format(m.date_begin, '%d-%m-%Y'), date_format(RG.d1, '%d-%m-%Y'), date_format(ALLER.d2, '%d-%m-%Y'), date_format(TER.d3, '%d-%m-%Y'),
  date_format(SURG.d4, '%d-%m-%Y'), group_concat(v.doc_v) 
 from main m
 left join RG on m.id_human = RG.id_h
 left join ALLER on m.id_human = ALLER.id_h
 left join TER on m.id_human = TER.id_h
 left join SURG on m.id_human = SURG.id_h
 left join visits v on m.id_human = v.id_human 
 where RG.d1 is not null or aller.d2 is not null or ter.d3 is not null or surg.d4 is not null
  group by m.id_human 
 order by m.doc_name, pat) T