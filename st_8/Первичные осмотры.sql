select dep.name,
       short_name(u.name) as doc_name,
       round(coalesce(sum(cs.SERVICE_COST), 0), 2)
from case_services cs
         left join doctor_spec ds on ds.doctor_id = cs.id_doctor
         left join users u on ds.user_id = u.id and u.id <> 1
         left join departments dep on ds.department_id = dep.id
         left join price p on p.id = cs.id_profile
where cs.date_begin between @date_start and @date_end
  and ds.user_id <> 1
  and (dep.id = @depart_id OR @depart_id = 0 OR @depart_id is null)
  and cs.is_oms = 0
  and cs.id_order = -1
  and cs.is_paid > 0
  and p.code in ('51001', '7077')
group by dep.id, cs.ID_DOCTOR
order by dep.name, doc_name;
