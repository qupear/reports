set @date_start = '2020-07-01';
set @date_end = '2020-07-31';
set @depart_id = 0;
set @doc_id = 0;

drop temporary table if exists f_date, cost, rg, refuse, ter, surg, orthopedist,
	implant, privilege, start_bzp_ter_surg, orto_spec, visits;

CREATE TEMPORARY TABLE IF NOT EXISTS f_date 
SELECT pd.id_human, MIN(cs.date_begin) AS date_begin, cs.id_doctor
FROM case_services cs
LEFT JOIN price p ON cs.id_profile = p.id 
LEFT JOIN cases c ON cs.id_case = c.id_case
LEFT JOIN patient_data pd ON pd.id_patient = c.id_patient
WHERE cs.date_begin BETWEEN @date_start AND @date_end
	AND (cs.id_doctor = @doc_id or @doc_id = 0)
	AND p.code IN ('1')
GROUP BY pd.id_human;

/*Сумма по услугам*/
CREATE TEMPORARY TABLE IF NOT EXISTS cost 	
SELECT pd.id_human, COALESCE(SUM(cs.service_cost), 0) summ
FROM case_services cs
left join cases c on c.id_case = cs.id_case
left join patient_data pd on pd.id_patient = c.id_patient
left join price p on cs.id_profile = p.id
JOIN f_date fd ON fd.id_human = pd.id_human
WHERE cs.DATE_BEGIN >= fd.date_begin 
	and cs.is_oms = 0
	and p.group_ids = '130'
GROUP BY fd.id_human;

/*RG*/
CREATE TEMPORARY TABLE IF NOT EXISTS rg 
SELECT prot.id_human AS id_h, id_ref, date_protocol AS d1
FROM 			 protocols prot
JOIN f_date fd ON prot.id_human = fd.id_human
WHERE prot.id_ref IN (141770,80260) AND date_protocol >= @date_start
GROUP BY id_h, id_ref;

/*Ортодонт от врача-специалиста*/
CREATE TEMPORARY TABLE IF NOT EXISTS orto_spec 
SELECT prot.id_human AS id_h, id_ref, date_protocol AS d_orto_spec
FROM 			 protocols prot
JOIN f_date fd ON prot.id_human = fd.id_human
WHERE prot.id_ref IN (8471,141940,45145,46169,47405,48249,
				49539,52288,52859,69109,70189,71358) 
	AND date_protocol >= @date_start
GROUP BY id_h, id_ref;

/*Терап. санация*/
CREATE TEMPORARY TABLE IF NOT EXISTS ter 
SELECT prot.id_human AS id_h, id_ref, date_protocol AS d3
FROM protocols prot
WHERE prot.id_ref IN (141780,80160)
	AND date_protocol >= @date_start
GROUP BY id_h, id_ref;

/*Хирург. санация*/
CREATE TEMPORARY TABLE IF NOT EXISTS surg 
SELECT prot.id_human AS id_h, id_ref, date_protocol AS d4
FROM protocols prot
JOIN f_date fd ON prot.id_human = fd.id_human
WHERE prot.id_ref IN (141800,80170) AND date_protocol >= @date_start
GROUP BY id_h, id_ref;

/*Направлен к ортопеду*/
CREATE TEMPORARY TABLE IF NOT EXISTS orthopedist 
SELECT prot.id_human AS id_h, id_ref, date_protocol AS d5
FROM protocols prot
JOIN f_date fd ON prot.id_human = fd.id_human
WHERE prot.id_ref IN (141810) AND date_protocol >= @date_start
GROUP BY id_h, id_ref;

/*Отметка о заверш. сан-и (тер/хирург)*/
CREATE TEMPORARY TABLE IF NOT EXISTS start_bzp_ter_surg 
SELECT prot.id_human AS id_h, id_ref, date_protocol AS d4,
	short_name(u.name) doc_name
FROM 			 protocols prot
JOIN f_date fd ON prot.id_human = fd.id_human
LEFT JOIN doctor_spec ds ON prot.id_doctor = ds.doctor_id
LEFT JOIN users u ON u.id = ds.user_id
WHERE prot.id_ref IN (52285,52855,65865,66965,67905,69105,70185,
				71355,118454,118054,46165,48245,49535,34441,34374,
				113794,113144,112464,111893,111254,110272,107181,
				115964,115014,77222,124900,76632,72812,63452,62522,
				61411,60522,116404,23441,13729,22557,34543)
	AND date_protocol >= @date_start
GROUP BY prot.id_doctor, id_h, id_ref;

/*Отказ от лечения*/
CREATE TEMPORARY TABLE IF NOT EXISTS refuse 
SELECT prot.id_human AS id_h, id_ref, date_protocol AS d_refuse
FROM 			 protocols prot
JOIN f_date fd ON prot.id_human = fd.id_human
WHERE prot.id_ref IN (80029)
	AND date_protocol >= @date_start
GROUP BY prot.id_human, id_ref;

/*Даты санаций*/
CREATE TEMPORARY TABLE IF NOT EXISTS visits 
SELECT pd.id_human,
	CONCAT(DATE_FORMAT(cs.date_begin, '%d-%m-%Y'), '  ', short_name(u.name)) doc_v
FROM case_services cs
LEFT JOIN cases c ON cs.id_case = c.id_case
LEFT JOIN patient_data pd ON c.id_patient = pd.id_patient
LEFT JOIN doctor_spec ds ON cs.id_doctor = ds.doctor_id
LEFT JOIN users u ON ds.user_id = u.id
JOIN f_date fd ON pd.id_human = fd.id_human
WHERE cs.date_begin >= @date_start
	AND (ds.spec_id = 106 OR ds.spec_id = 108 OR ds.spec_id = 208 OR ds.spec_id = 211)
GROUP BY pd.id_human, cs.id_case, cs.id_doctor
ORDER BY pd.id_human, cs.date_begin; 

SET @ROW = 0;

SELECT @ROW := @ROW + 1, t.*
FROM
 (
	SELECT CONCAT_WS(' ', pd.surname, pd.name, pd.second_name) AS pat,
		DATE_FORMAT(fd.date_begin, '%d-%m-%Y') f_date,
		short_name(u.name) doc,
		COALESCE(DATE_FORMAT(rg.d1, '%d-%m-%Y'), '') 'RG',
		COALESCE(DATE_FORMAT(ter.d3, '%d-%m-%Y'), '') 'Ter_san',
		COALESCE(DATE_FORMAT(surg.d4, '%d-%m-%Y'), '') 'Surg_san',
		COALESCE(DATE_FORMAT(os.d_orto_spec, '%d-%m-%Y'), '') 'Orto_spec',
		COALESCE(DATE_FORMAT(ort.d5, '%d-%m-%Y'), '') 'To_orthopedist',
		GROUP_CONCAT(DISTINCT v.doc_v) 'Date_san',
		CONCAT(DATE_FORMAT(sbts.d4, '%d-%m-%Y'), ' ', sbts.doc_name) 'Finish_san',
		COALESCE(DATE_FORMAT(refuse.d_refuse, '%d-%m-%Y'), '') 'Refuse',
		COALESCE((cost.summ), 0) 'Summ'
	FROM
	 f_date fd
	LEFT JOIN cost ON fd.id_human = cost.id_human
	LEFT JOIN patient_data pd ON pd.id_human = fd.id_human AND pd.is_active = 1 AND pd.date_end = '2200-01-01'
	LEFT JOIN doctor_spec ds ON ds.doctor_id = fd.id_doctor
	LEFT JOIN users u ON ds.user_id = u.id
	LEFT JOIN rg ON pd.id_human = rg.id_h
	LEFT JOIN orto_spec os ON pd.id_human = os.id_h
	LEFT JOIN ter ON pd.id_human = ter.id_h
	LEFT JOIN surg ON pd.id_human = surg.id_h
	LEFT JOIN orthopedist ort ON pd.id_human = ort.id_h
	LEFT JOIN start_bzp_ter_surg sbts ON sbts.id_h = pd.id_human
	LEFT JOIN visits v ON pd.id_human = v.id_human
	LEFT JOIN refuse ON pd.id_human = refuse.id_h
	WHERE
	 ds.department_id = @depart_id OR @depart_id = 0
	GROUP BY fd.id_human
	ORDER BY u.name, pat) t;