set @row = 0;
SELECT @row := @row + 1, T_MAIN.time, T_MAIN.pat_name, T_MAIN.birthday, T_MAIN.unstruct_address, T_MAIN.prim,
       T_MAIN.child, T_MAIN.diagnosis_code, T_MAIN.cf, T_MAIN.codes_oms, T_MAIN.codes_pu, COALESCE(T_MAIN.sanir, 0),
       T_MAIN.uet_oms, T_MAIN.uet_pu
FROM (SELECT a.time, CONCAT(pd.surname, ' ', pd.name, ' ', pd.second_name) as pat_name, pd.birthday,
             pa.unstruct_address, (count(cs.id_case) > 0) as prim,
             (count(cs.id_case) * (CURDATE() < DATE_ADD(pd.birthday, INTERVAL 15 year)) > 0) as child,
             vd.diagnosis_code, case COAG_FLAG when 1 then 'К' when 0 then ' ' end as cf,
             COALESCE(T1.codes, '') as codes_oms, COALESCE(T2.codes, '') as codes_pu, prot_sanir.sanir as sanir,
             ROUND(T1.uet, 2) as uet_oms, ROUND(T2.uet, 2) as uet_pu
      FROM (SELECT id_case FROM case_services WHERE date_begin = @date_start AND id_doctor = @doc_id) CS_MAIN
               LEFT JOIN cases c ON c.id_case = CS_MAIN.id_case
               LEFT JOIN vmu_diagnosis vd ON vd.id_diagnosis = c.id_diagnosis
               LEFT JOIN patient_data pd ON pd.id_patient = c.id_patient
               LEFT JOIN patient_address pa ON pa.id_address = pd.id_address_reg
               LEFT JOIN appointments a ON a.case_id = c.id_case
               LEFT JOIN case_services cs ON cs.id_case = c.id_case AND cs.id_profile IN (select id_profile
                                                                                          from vmu_profile
                                                                                          where profile_infis_code = 'стт001'
                                                                                             or profile_infis_code = 'стт005'
                                                                                             or profile_infis_code = 'стт013')
               LEFT JOIN (SELECT T11.id_case, T11.id_doctor,
                                 GROUP_CONCAT(CONCAT(T11.profile_infis_code, ' (', T11.cnt, ')') SEPARATOR
                                              ', ') as codes, SUM(T11.UET) as uet
                          FROM (SELECT tcs.id_case, tcs.id_doctor, tcs.id_profile, count(tcs.id_profile) as cnt,
                                       tvp.profile_infis_code, COALESCE(SUM(tvp.UET), 0) as uet
                                FROM case_services tcs
                                         LEFT JOIN (SELECT ttvp.profile_infis_code, tvt.uet, ttvp.id_profile,
                                                           tvt.id_zone_type
                                                    FROM vmu_profile ttvp,
                                                         vmu_tariff tvt,
                                                         vmu_tariff_plan vtp
                                                    WHERE ttvp.id_profile = tvt.id_profile
                                                      and vtp.id_tariff_group = tvt.id_lpu
                                                      AND tvt.tariff_begin_date <= @date_start
                                                      AND tvt.tariff_end_date >= @date_start
                                                      and vtp.tp_begin_date <= @date_start
                                                      and vtp.tp_end_date >= @date_start) tvp
                                                   ON tvp.id_profile = tcs.id_profile and tcs.is_oms = 1 AND
                                                      tvp.id_zone_type = tcs.id_net_profile
                                WHERE tcs.date_begin = @date_start
                                  AND tcs.id_doctor = @doc_id
                                GROUP BY tcs.id_case, tcs.id_profile) T11
                          GROUP BY T11.id_case) T1 ON T1.id_case = c.id_case
               LEFT JOIN (SELECT T12.id_case, T12.id_doctor,
                                 GROUP_CONCAT(CONCAT(T12.code, ' (', T12.cnt, ')') SEPARATOR ', ') as codes,
                                 SUM(T12.UET) as uet
                          FROM (SELECT tcs.id_case, tcs.id_doctor, tcs.id_profile, count(tcs.id_profile) as cnt,
                                       tp.code, COALESCE(SUM(tp.UET), 0) as uet
                                FROM case_services tcs
                                         LEFT JOIN price tp
                                                   ON tp.id = tcs.id_profile and tp.date_end = '2200-01-01' and tcs.is_oms = 0
                                WHERE tcs.date_begin = @date_start
                                  AND tcs.id_doctor = @doc_id
                                GROUP BY tcs.id_case, tcs.id_profile) T12
                          GROUP BY T12.id_case) T2 ON T2.id_case = c.id_case
               LEFT JOIN (SELECT id_case, count(id_ref) as sanir, id_diag, id_doctor
                          FROM protocols p
                          where date_protocol = @date_start
                            and id_doctor = @doc_id
                            and (id_ref = 55791 or id_ref = 5761 or id_ref = 4781 or id_ref = 3790 or id_ref = 2350 or
                                 id_ref = 1392 or id_ref = 480)
                          group by id_case) prot_sanir ON prot_sanir.id_case = c.id_case
      GROUP BY CS_MAIN.id_case
      order by a.time) T_MAIN;

