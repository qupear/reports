select 'Количество пациентов за период', count(distinct pd.ID_HUMAN)
 from cases c
 left join patient_data pd on c.ID_PATIENT = pd.ID_PATIENT
 where c.DATE_BEGIN between @date_start and @date_end