set @date_start = '2020-06-01';
set @date_end = '2020-06-30';
set @doc_id = 0;
set @depart_id = 13;

drop temporary table if exists doctor_services, docs, fio , has_work_days, work_days, doctor_protocols,T1, T2, T3,T4,T5,T6,T7,T8,
	T9_1, T9_2, T9_3, T9_4, T9_5, T9_6, T9_7, T9_8, T9_9, T9,T10,T11,T12,T13,T14,T15,T16,T17,T18_1,T18_2, t18_1_id, t18_2_id, T19;

CREATE TEMPORARY TABLE IF NOT EXISTS fio (
	                                         SELECT id, concat(SUBSTRING_INDEX(name, ' ', 1), ' ',
	                                                           left(SUBSTRING_INDEX(SUBSTRING_INDEX(name, ' ', 2), ' ', -1), 1),
	                                                           '. ', left(
			                                                           SUBSTRING_INDEX(SUBSTRING_INDEX(name, ' ', -1), ' ', -1),
			                                                           1), '.') AS short_name
		                                         FROM users);

create temporary table if not exists doctor_services (select fio.short_name, cs.id_doctor, cs.date_begin, cs.id_case, vp.profile_infis_code, p.code,
                                                             vd.diagnosis_code, cs.tooth_name, T.uet, cs.is_oms, dep.name depname, ds.department_id,
                                                             case
                                                                 when YEAR(CS.DATE_BEGIN) - YEAR(pd.BIRTHDAY)
    - (DATE_FORMAT(CS.DATE_BEGIN, '%m%d') < DATE_FORMAT(pd.BIRTHDAY, '%m%d'))  >= 60 and pd.SEX = 'м'
                                                                     then 0
                                                                 WHEN YEAR(CS.DATE_BEGIN) - YEAR(pd.BIRTHDAY)
    - (DATE_FORMAT(CS.DATE_BEGIN, '%m%d') < DATE_FORMAT(pd.BIRTHDAY, '%m%d'))  >= 55 and pd.SEX = 'ж'
                                                                     then 1 else -1 end as old_people, pd.ID_PATIENT
                                                      from case_services cs
															left join cases c on cs.id_case = c.id_case
															left join patient_data pd on c.ID_PATIENT = pd.id_patient
															left join vmu_profile vp on vp.id_profile = cs.id_profile and cs.is_oms = 1
															left join price p on p.id = cs.id_profile and cs.is_oms in (0, 3)
															left join vmu_diagnosis vd on cs.id_diagnosis = vd.id_diagnosis
															left join doctor_spec ds on cs.id_doctor = ds.doctor_id
															left join departments dep on dep.id = ds.department_id
															LEFT JOIN users ON ds.user_id = users.id
															LEFT JOIN fio ON users.id = fio.id
															left join (select vt.uet, vt.id_profile, vt.id_zone_type,
                                                                                 vt.tariff_begin_date as d1,
                                                                                 vt.tariff_end_date as d2,
                                                                                 vtp.tp_begin_date as d3,
                                                                                 vtp.tp_end_date as d4
                                                                          from vmu_tariff vt,
                                                                               vmu_tariff_plan vtp
                                                                          where vtp.id_tariff_group = vt.id_lpu
                                                                            And vtp.id_lpu in (select id_lpu from mu_ident)) T
                                                                         on T.id_profile = vp.id_profile and
                                                                            cs.date_begin between T.d1 and T.d2 and
                                                                            cs.date_begin between T.d3 and T.d4 and
                                                                            T.id_zone_type = cs.id_net_profile and
                                                                            cs.is_oms = 1
                                                      where (cs.id_doctor = @doc_id or @doc_id = 0) 
														and (ds.department_id = @depart_id or @depart_id = 0)
                                                        and cs.date_begin between @date_start and @date_end);

create temporary table if not exists doctor_protocols 
															(select ds.id_case, ds.id_doctor,
                                                              locate('17239', p.child_ref_ids) > 0 as p17239,
                                                              locate('17251', p.child_ref_ids) > 0 as p17251,
                                                              locate('18720', p.child_ref_ids) > 0 as p18720,
                                                              locate('19010', p.child_ref_ids) > 0 as p19010,
                                                              locate('44170', p.child_ref_ids) > 0 as p44170,
                                                              locate('47780', p.child_ref_ids) > 0 as p47780,
                                                              locate('79760', p.child_ref_ids) > 0 as p79760,
                                                              locate('80370', p.child_ref_ids) > 0 as p80370,
                                                              locate('18730', p.child_ref_ids) > 0 as p18730,
                                                              locate('80380', p.child_ref_ids) > 0 as p80380,
                                                              locate('55791', p.child_ref_ids) > 0 as p55791,
                                                              locate(';5761', p.child_ref_ids) > 0 as p5761,
                                                              locate(';4781', p.child_ref_ids) > 0 as p4781,
                                                              locate(';3790', p.child_ref_ids) > 0 as p3790,
                                                              locate(';2350', p.child_ref_ids) > 0 as p2350,
                                                              locate(';1392', p.child_ref_ids) > 0 as p1392,
                                                              locate(';480', p.child_ref_ids) > 0 as p480
                                                       from doctor_services ds
                                                                left join protocols p on p.id_case = ds.id_case
																left join doctor_spec docs on ds.id_doctor = docs.doctor_id
                                                       where ds.is_oms = 1
														 and (docs.department_id = @depart_id or @depart_id = 0)
                                                         and (p.id_doctor = @doc_id or @doc_id = 0));

/*Рабочие дни, вспомогательная*/
create temporary table if not exists has_work_days
SELECT c.id_case has_work_day, c.id_doctor
 FROM stomadb.cases c
where date_begin between @date_start and @date_end 
	and (id_dept = @depart_id or @depart_id = 0) 
	and date_end is not null
group by id_doctor, date_begin;

/*Рабочие дни*/
create temporary table if not exists work_days
select id_doctor, round(count(has_work_day) * 6.6 / 5.5, 1) work_days
from has_work_days
group by id_doctor;

/*принято больных*/
create temporary table if not exists T1 (select id_doctor, count(distinct id_case) as val
                                         from doctor_services
                                         where profile_infis_code in ('стт005', 'стт006', 'стт054', 'стт055', 'нстт005', 'нстт006', 'нстт054', 'нстт055')
                                         group by id_doctor);

/*из них первичных*/
create temporary table if not exists T2 (select id_doctor, count(distinct id_case) as val
                                         from doctor_services
                                         where profile_infis_code in ('стт005', 'нстт005', 'стт054', 'нстт054')
                                         group by id_doctor);

/*запломбировано всего*/
create temporary table if not exists T3
	select T.id_doctor, count(T.val) as val
	from (select id_doctor, count(id_case) as val
           from doctor_services
           where profile_infis_code in ('стт025', 'стт028', 'стт031')
           group by id_case, tooth_name) T
	group by id_doctor;

/*кариес*/
create temporary table if not exists T4
(select T.id_doctor, count(T.val) as val
from (select id_doctor, count(id_case) as val
      from doctor_services
      where profile_infis_code in ('стт025', 'стт028', 'стт031')
        and diagnosis_code in
            ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5', 'k02.6', 'k02.7', 'k02.8', 'k02.9', 'k03.2')
      group by id_case, tooth_name) T
group by id_doctor);

/*по осложн. кариесу*/
create temporary table if not exists T5
(select T.id_doctor, count(T.val) as val
from (select id_doctor, count(id_case) as val
      from doctor_services
      where profile_infis_code in ('стт025', 'стт028', 'стт031')
        and diagnosis_code in ('k04.0', 'k04.1', 'k04.2', 'k04.3', 'k04.4', 'k04.5')
      group by id_case, tooth_name) T
group by id_doctor);

/*анестезия*/
create temporary table if not exists T8
    (select id_doctor, count(id_case) as val
     from doctor_services
     where profile_infis_code in ('сто001', 'сто002', 'сто003', 'нсто001', 'нсто002', 'нсто003')
     group by id_doctor);

/*вылечено зубов в одно посещение*/
create temporary table if not exists T9_1
   (select ds.id_doctor, ds.id_case as val
           from doctor_services ds
           where profile_infis_code in ('стт026', 'стт029', 'стт027', 'стт030', 'стт032', 'стт033')
			and diagnosis_code in ('k04.0')
     group by ds.id_doctor, ds.id_case);

create temporary table if not exists T9_2
   (select ds.id_doctor, ds.id_case as val
           from doctor_services ds
			inner join T9_1 on T9_1.val = ds.id_case
           where ds.profile_infis_code in ('стт025', 'стт028', 'стт031')
     group by ds.id_doctor,ds.id_case);

create temporary table if not exists T9_3
   (select ds.id_doctor, ds.id_case as val
           from doctor_services ds
			inner join T9_2 on  T9_2.val = ds.id_case
           where ds.profile_infis_code in ('стт037')
     group by ds.id_doctor,ds.id_case);

create temporary table if not exists T9_4
   (select ds.id_doctor, ds.id_case as val
           from doctor_services ds
			inner join T9_3 on T9_3.val = ds.id_case
           where ds.profile_infis_code in ('стт038', 'стт039')
     group by ds.id_doctor,ds.id_case);

create temporary table if not exists T9_5
   (select ds.id_doctor, ds.id_case as val
           from doctor_services ds
			inner join T9_3 on T9_3.val = ds.id_case
           where ds.profile_infis_code in ('стт044')
     group by ds.id_doctor,ds.id_case);

create temporary table if not exists T9_6
   (select ds.id_doctor, ds.id_case as val
           from doctor_services ds
			inner join T9_3 on T9_3.val = ds.id_case
           where ds.profile_infis_code in ('стт045')
     group by ds.id_doctor,ds.id_case);

create temporary table if not exists T9_7
   (select ds.id_doctor, ds.id_case as val
           from doctor_services ds
           where profile_infis_code in ('нстт026', 'нстт029', 'нстт027', 'нстт030', 'нстт032', 'нстт033')
			and diagnosis_code in ('k04.0')
     group by ds.id_doctor,ds.id_case);

create temporary table if not exists T9_8
   (select ds.id_doctor, ds.id_case as val
           from doctor_services ds
			inner join T9_7 on T9_7.val = ds.id_case
           where ds.profile_infis_code in ('нстт025', 'нстт028', 'нстт031')
     group by ds.id_doctor,ds.id_case);

create temporary table if not exists T9_9
   (select ds.id_doctor, ds.id_case as val
           from doctor_services ds
			inner join T9_8 on T9_8.val = ds.id_case
           where ds.profile_infis_code in ('нстт037', 'нстт038', 'нстт039')
     group by ds.id_doctor,ds.id_case);

create temporary table if not exists T9
   (select ds.id_doctor, count(distinct id_case) as val
           from doctor_services ds
           where (ds.id_case in (select val from T9_4)) or (ds.id_case in (select val from T9_5)) or 
			(ds.id_case in (select val from T9_6)) or (ds.id_case in (select val from T9_9))
     group by id_doctor);

/*поставлено пломб всего */
create temporary table if not exists T10
   (select id_doctor, count(id_case) as val
           from doctor_services
           where profile_infis_code in ('стт034', 'стт026', 'стт029', 'стт027', 'стт030', 'стт032', 'стт033', 'нстт034')
     group by id_doctor);

/*временные пломбы*/
create temporary table if not exists T11
   (select id_doctor, count(id_case) as val
           from doctor_services
           where profile_infis_code in ('стт026', 'стт029', 'стт032', 'стт034', 'нстт034')
     group by id_doctor);

/*композит*/
create temporary table if not exists T13
 (select id_doctor, count(id_case) as val
           from doctor_services
           where profile_infis_code in ('стт027', 'стт030', 'стт033')
     group by id_doctor);

/*снято зубных отложений*/
create temporary table if not exists T14
    (select id_doctor, count(distinct id_case) as val
     from doctor_services
     where profile_infis_code in ('стт041')
     group by id_doctor);

/*санировано*/
create temporary table if not exists T15
    (select count(distinct ds.id_case) as val, ds.id_doctor
     from doctor_services ds
              left join protocols p on ds.id_case = p.id_case
     where ds.is_oms = 1
       and p.id_ref in
           (52282, 52852, 66962, 65862, 67902, 69102, 70182, 71352, 118451, 118051, 46162, 48242, 49532, 113791, 113141,
            112461, 111861, 111251, 110271)
     group by id_doctor);

/*м старше 60*/
create temporary table if not exists T16 (select id_doctor, count(distinct ID_PATIENT) as val
                                          from doctor_services
where old_people = 0 and is_oms = 1
                                          group by id_doctor);
/*ж старше 55*/
create temporary table if not exists T17 (select id_doctor, count(distinct ID_PATIENT) as val
                                          from doctor_services
where old_people = 1  and is_oms = 1
                                          group by id_doctor);

/*беременные child_ref*/
create temporary table if not exists t18_1_id
(select id from protocols p
where (child_ref_ids like '%118730(%' or child_ref_ids like '%118750(%'
or child_ref_ids like '%118770(%' or child_ref_ids like '%118790(%'
or child_ref_ids like '%118810(%' or child_ref_ids like '%118830(%'
or child_ref_ids like '%118850(%' or child_ref_ids like '%118870(%'
or child_ref_ids like '%118890(%' or child_ref_ids like '%118910(%'
or child_ref_ids like '%118930(%' or child_ref_ids like '%118950(%'
or child_ref_ids like '%118970(%' or child_ref_ids like '%118990(%'
or child_ref_ids like '%119010(%' or child_ref_ids like '%119030(%'
or child_ref_ids like '%119050(%' or child_ref_ids like '%119070(%'
or child_ref_ids like '%119090(%' or child_ref_ids like '%119110(%'
or child_ref_ids like '%119130(%' or child_ref_ids like '%119150(%'
or child_ref_ids like '%119170(%' or child_ref_ids like '%119190(%'
or child_ref_ids like '%119210(%' or child_ref_ids like '%119230(%'
or child_ref_ids like '%119250(%' or child_ref_ids like '%119270(%'
or child_ref_ids like '%119290(%' or child_ref_ids like '%119310(%'
or child_ref_ids like '%119330(%' or child_ref_ids like '%119350(%'
or child_ref_ids like '%119400(%' or child_ref_ids like '%119420(%'
or child_ref_ids like '%119440(%' or child_ref_ids like '%119460(%'
or child_ref_ids like '%119480(%' or child_ref_ids like '%119500(%'
or child_ref_ids like '%119520(%' or child_ref_ids like '%119550(%'
or child_ref_ids like '%119590(%' or child_ref_ids like '%119630(%'
or child_ref_ids like '%119670(%' or child_ref_ids like '%119710(%'
or child_ref_ids like '%119750(%' or child_ref_ids like '%119790(%'
or child_ref_ids like '%119830(%' or child_ref_ids like '%119870(%')
and date_protocol between @date_start and @date_end);

/*беременные*/
create temporary table if not exists T18_1 
(select count(distinct ds.ID_case)  as val, ds.id_doctor
                                          from doctor_services ds
left join protocols p on ds.id_case = p.id_case
where ds.is_oms = 1 and p.id in (select id from t18_1_id)
group by id_doctor);

/*ИВОВ chil ref*/
create temporary table if not exists t18_2_id
(select id from protocols p
where (child_ref_ids like '%3464(%' or child_ref_ids like '%4124(%'
or child_ref_ids like '%44342(%' or child_ref_ids like '%46472(%'
or child_ref_ids like '%51732(%' or child_ref_ids like '%52382(%'
or child_ref_ids like '%54402(%' or child_ref_ids like '%55392(%'
or child_ref_ids like '%56222(%' or child_ref_ids like '%57012(%'
or child_ref_ids like '%57842(%' or child_ref_ids like '%58692(%'
or child_ref_ids like '%59842(%' or child_ref_ids like '%60682(%'
or child_ref_ids like '%61623(%' or child_ref_ids like '%62702(%'
or child_ref_ids like '%63562(%' or child_ref_ids like '%65142(%'
or child_ref_ids like '%66072(%' or child_ref_ids like '%67152(%'
or child_ref_ids like '%68312(%' or child_ref_ids like '%69172(%'
or child_ref_ids like '%70322(%' or child_ref_ids like '%72192(%'
or child_ref_ids like '%73022(%' or child_ref_ids like '%73842(%'
or child_ref_ids like '%74702(%' or child_ref_ids like '%103312(%'
or child_ref_ids like '%106652(%' or child_ref_ids like '%107212(%'
or child_ref_ids like '%107742(%' or child_ref_ids like '%108272(%'
or child_ref_ids like '%108712(%' or child_ref_ids like '%109172(%'
or child_ref_ids like '%109642(%' or child_ref_ids like '%114012(%'
or child_ref_ids like '%115133(%' or child_ref_ids like '%116572(%'
or child_ref_ids like '%117242(%' or child_ref_ids like '%119560(%'
or child_ref_ids like '%119600(%' or child_ref_ids like '%119640(%'
or child_ref_ids like '%119680(%' or child_ref_ids like '%119720(%'
or child_ref_ids like '%119760(%' or child_ref_ids like '%119800(%'
or child_ref_ids like '%119840(%' or child_ref_ids like '%119880(%')
and date_protocol between @date_start and @date_end);

/*ИВОВ*/
create temporary table if not exists T18_2 
(select count(distinct ds.id_case)  as val, ds.id_doctor
                                          from doctor_services ds
left join protocols p on ds.id_case = p.id_case
where ds.is_oms = 1 and p.id in (select id from t18_2_id)
                                          group by ds.id_doctor);

/*YET*/
create temporary table if not exists T19
    (select id_doctor, ROUND(COALESCE(SUM(UET), 0), 2) as val
     from doctor_services
     group by id_doctor);

CREATE TEMPORARY TABLE IF NOT EXISTS docs
	(SELECT depname, short_name, id_doctor
	FROM doctor_services GROUP BY id_doctor);

select
		d.short_name, wd.work_days as 'Рабочие дни',
		coalesce(T1.val, 0) as 'Принято больных', coalesce(T2.val, 0) as 'Из них первичных',
		coalesce(T3.val, 0) as 'Запломбировано всего',
		coalesce(T4.val, 0) as 'Кариес', coalesce(T5.val, 0) as 'По ослож кариесу',
		coalesce(T9.val, 0) as 'вылечено в одно посещение',	coalesce(T10.val, 0) as 'поставлено пломб всего',
		coalesce(T11.val, 0) as 'временные', coalesce(T13.val, 0) as 'композит',
		coalesce(T14.val, 0) as 'снято з/о', coalesce(T15.val, 0) as 'санировано',
		'' as 'Санировано в I посещ.', coalesce(T19.val, 0) as 'YET',
		round(coalesce(T1.val, 0) / wd.work_days, 2) as 'Средн. посещ. в день',
		round((coalesce(T11.val, 0) + coalesce(T13.val, 0)) / wd.work_days, 2) as 'Средн. Пломб в день',
		round(coalesce(T15.val, 0) / wd.work_days, 2) as 'Средн. Санаций в день',
		round(coalesce(T19.val, 0) / wd.work_days, 2) as 'Средн. УЕТ в день',
		'' as 'Осмотры', 
		coalesce(T17.val, 0) as 'ж старше 55', coalesce(T16.val, 0) as 'м старше 60',
		coalesce(T18_1.val, 0) as 'беременные', coalesce(T18_2.val, 0) as 'ИВОВ',
		'' as 'Прочие', coalesce(T8.val, 0) as 'анестезия'
from  docs d
		left join work_days wd on wd.id_doctor = d.id_doctor
		left join T1 on T1.id_doctor = d.id_doctor
		left join T2 on T2.id_doctor = d.id_doctor
		left join T3 on T3.id_doctor = d.id_doctor
		left join T4 on T4.id_doctor = d.id_doctor
		left join T5 on T5.id_doctor = d.id_doctor
		left join T8 on T8.id_doctor = d.id_doctor
		left join T9 on T9.id_doctor = d.id_doctor
		left join T10 on T10.id_doctor = d.id_doctor
		left join T11 on T11.id_doctor = d.id_doctor
		left join T13 on T13.id_doctor = d.id_doctor
		left join T14 on T14.id_doctor = d.id_doctor
		left join T15 on T15.id_doctor = d.id_doctor
		left join T16 on T16.id_doctor = d.id_doctor
		left join T17 on T17.id_doctor = d.id_doctor
		left join T18_1 on T18_1.id_doctor = d.id_doctor
		left join T18_2 on t18_2.id_doctor = d.id_doctor
		left join T19 on T19.id_doctor = d.id_doctor
group by d.id_doctor
order by d.short_name;
