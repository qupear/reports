set @date_start = '2019-07-01';
set @date_end = '2019-07-02';
set @row=  0;
 select @row:=@row+1, T.* from

 (SELECT case when NetUserPosition=2 then 'Call-центр' when NetUserPosition=4 then 'Портал'  else o.NetUserName end as iname,
 case when NetUserPosition='портал' then 4 when NetUserPosition='оператор' then 2 else NetUserPosition end as itype,
 count(o.NetUserPosition) as kolvo FROM on_record o where o.NetUserPosition is not null and o.id_appoint > 0 and o.date between @date_start and @date_end
 group by itype
 union all
 select case when ap.author='d' then 'Доктор' when ap.author='r' then 'Регистратура' when ap.author='o' then 'Регистратура' end as iname,
 case when ap.author='d' then 10 when ap.author='r' then 11 when ap.author='o' then 11 end as itype, count(ap.id) as kolvo
 from appointments ap
 where ap.time between @date_start and @date_end and ap.id_patient >-1 and ap.author <> 'i'
 and ap.id not in (select id_appoint from on_record o where o.date between '2019-07-01' and '2019-10-21')
 group by itype
 order by itype) T