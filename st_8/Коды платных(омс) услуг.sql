set @date_start = '2019-07-01';
set @date_end = '2019-07-22';
set @fin_type = 0;
set @code = 'all';
select
    d.name as dept_name,
case when not @fin_type then p.code
when @fin_type then vp.PROFILE_INFIS_CODE END
 as codes,
    u.name as doc_name,
case when not @fin_type then count(p.code)
when @fin_type then count(vp.PROFILE_INFIS_CODE) END
 as code_count,
       case when not @fin_type then sum(p.COST)
when @fin_type then sum(T.price) END
 as code_sum
from
    case_services cs
        left join
    doctor_spec ds ON ds.doctor_id = cs.id_doctor
        left join
    users u ON u.id = ds.user_id
        left join
    departments d ON d.id = ds.department_id
        left join
    price p ON p.id = cs.id_profile and cs.is_oms = 0
left join vmu_profile vp on cs.ID_PROFILE = vp.ID_PROFILE and cs.is_oms = 1
left join (select vt.price, vt.uet, vt.id_profile, vt.id_zone_type,
                                vt.tariff_begin_date as d1,
                                vt.tariff_end_date as d2,
                                vtp.tp_begin_date as d3,
                                vtp.tp_end_date as d4
                         from vmu_tariff vt,
                              vmu_tariff_plan vtp
                         where vtp.id_tariff_group = vt.id_lpu
                           And vtp.id_lpu in (select id_lpu from mu_ident)) T
                        on T.id_profile = vp.id_profile and
                           cs.date_begin between T.d1 and T.d2 and
                           cs.date_begin between T.d3 and T.d4 and
                           T.id_zone_type = cs.id_net_profile and
                           cs.is_oms = 1
where
    cs.date_begin between @date_start and @date_end
        and ((cs.is_oms = 0 and not @fin_type) or (cs.is_oms = 1 and  @fin_type))
        and (d.id = @depart_id or @depart_id = 0)
        and ((
            ((FIND_IN_SET(p.code, @code) and not @fin_type)
                  or (FIND_IN_SET(vp.profile_infis_code, @code) and @fin_type))
        or CHAR_LENGTH(@code) < 2)
        or @code = 'all')
group by ds.doctor_id , cs.id_profile
order by dept_name , p.code , doc_name;

