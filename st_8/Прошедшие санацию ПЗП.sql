create temporary table if not exists prot (select id_human as id_h, id_ref,
                                                  concat(date_format(date_protocol, '%d-%m-%Y'), '  ', u.name) date_protocol,
                                                  id_doctor, case
                                                                 when ds.spec_id = 106 then 'Терапия'
                                                                 when ds.spec_id = 108 then 'Хирургия' end doc_spec
                                           from protocols prot
                                                    left join doctor_spec ds on prot.id_doctor = ds.doctor_id
                                                    left join users u on ds.user_id = u.id
                                           where prot.id_ref in
                                                 (46161, 48241, 49531, 52281, 52851, 65861, 66961, 67901, 69101, 70181,
                                                  71351,115961, 115011, 77221, 116060, 76631, 72811, 63451, 62521, 61411, 60521, 116070, 13724, 22553, 34541)
                                             and date_protocol >= @date_start
                                           group by id_human, id_doctor);
create temporary table if not exists TER (select id_human as id_h, id_ref, date_protocol as d3
                                          from protocols prot
                                          where prot.id_ref in (9492, 8461)
                                            and date_protocol >= @date_start
                                          group by id_human, id_ref); set @row = 0;


select @row := @row + 1, T.*
from (select prot.doc_spec, concat_ws(' ', pd.surname, pd.name, pd.second_name) pat, pd.card_number,
             date_format(cs.date_begin, '%d-%m-%Y') date_begin, pa.unstruct_address, pd.remark,
             concat(SUBSTRING_INDEX(u.name, ' ', 1), ' ',
                    left(SUBSTRING_INDEX(SUBSTRING_INDEX(u.name, ' ', 2), ' ', -1), 1), '. ',
                    left(SUBSTRING_INDEX(SUBSTRING_INDEX(u.name, ' ', -1), ' ', -1), 1), '.') as doc_name,
             date_format(ter.d3, '%d-%m-%Y') d3, prot.date_protocol, '' string
      from case_services cs
               left join doctor_spec ds on ds.doctor_id = cs.id_doctor
               left join users u on ds.user_id = u.id and u.id <> 1
               left join price p on p.id = cs.id_profile
               left join cases c on cs.ID_CASE = c.ID_CASE
               left join patient_data pd on c.ID_PATIENT = pd.ID_PATIENT
               left join patient_address pa on pd.ID_ADDRESS_REG = pa.ID_ADDRESS
               left join prot on prot.id_h = pd.id_human
               left join ter on pd.id_human = ter.id_h
      where cs.date_begin >= @date_start
        and ds.user_id <> 1
        and ds.spec_id = 107
        and cs.is_oms = 0
        and cs.id_order = -1
        and cs.is_paid > 0
        and p.code = '51001'
        and date_protocol is not null
      group by doc_spec, cs.ID_DOCTOR, id_human
      order by doc_name) T;
