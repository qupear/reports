select
    d.name as dept_name,
case when not @fin_type then p.code
when @fin_type then vp.PROFILE_INFIS_CODE END
 as codes,
    u.name as doc_name,
case when not @fin_type then count(p.code)
when @fin_type then count(vp.PROFILE_INFIS_CODE) END
 as code_count
from
    case_services cs
        left join
    doctor_spec ds ON ds.doctor_id = cs.id_doctor
        left join
    users u ON u.id = ds.user_id
        left join
    departments d ON d.id = ds.department_id
        left join
    price p ON p.id = cs.id_profile and cs.is_oms = 0
left join vmu_profile vp on cs.ID_PROFILE = vp.ID_PROFILE and cs.is_oms = 1
where
    cs.date_begin between @date_start and @date_end
        and ((cs.is_oms = 0 and not @fin_type) or (cs.is_oms = 1 and  @fin_type))
        and (d.id = @depart_id or @depart_id = 0)
        and ((
            ((FIND_IN_SET(p.code, @code) and not @fin_type)
                  or (FIND_IN_SET(vp.profile_infis_code, @code) and @fin_type))
        or CHAR_LENGTH(@code) < 2)
        or @code = 'all')
group by ds.doctor_id , cs.id_profile
order by dept_name , p.code , doc_name;

