set @date_start = '2019-05-01';
set @date_end = '2019-05-30';

set @fin_type = 0;
set @order_state = 1;
set @row = 0;
create temporary table if not exists pzp(
select c.ID_PATIENT from case_services cs
left join price p on cs.ID_PROFILE = p.ID
left join cases c on cs.ID_CASE = c.ID_CASE
where cs.DATE_BEGIN between @date_start and @date_end
and p.code = '5_____');
create temporary table if not exists bzp(
    select o.Id_Human from case_services cs
left join price_orto_soc pos on cs.ID_PROFILE = pos.id
left join orders o on cs.ID_ORDER = o.id
where cs.DATE_BEGIN between @date_start and @date_end
and pos.code = '1.4' and o.order_type = 1
group by 1);
select @row:=@row  + 1, T.* from(
select u.name,
       pd.CARD_NUMBER, o.Order_Number, date_format(o.orderCreateDate, '%d-%m-%Y'),
(date_format(coalesce(o.orderCompletionDate, '-'), '%d-%m-%Y')),
       concat_ws(' ', pd.SURNAME, pd.NAME, pd.SECOND_NAME) pat,
       case when @fin_type then date_format(pos.DATE_BEGIN, '%d-%m-%Y') else
       date_format(s.DATE_BEGIN, '%d-%m-%Y') end,
       concat(pa.UNSTRUCT_ADDRESS, ' ', pd.REMARK),
       sum(cs.SERVICE_COST)
       from orders o
           left join case_services cs on cs.ID_ORDER = o.id
left join patient_data pd on o.Id_Human = pd.ID_HUMAN
left join doctor_spec ds on o.id_doctor = ds.doctor_id
left join users u on ds.user_id = u.id
left join patient_address pa on pd.ID_ADDRESS_REG = pa.ID_ADDRESS
left join price p on cs.ID_PROFILE = p.id
left join price_orto_soc pos2 on cs.ID_PROFILE = pos2.id
left join (select c.ID_PATIENT, cs.DATE_BEGIN from case_services cs
left join price p on cs.ID_PROFILE = p.ID
left join cases c on cs.ID_CASE = c.ID_CASE
where cs.DATE_BEGIN between @date_start and @date_end
and p.code = '51001'
group by 1) s on pd.ID_PATIENT = s.ID_PATIENT
left join (select o.Id_Human, cs.DATE_BEGIN from case_services cs
left join price_orto_soc pos on cs.ID_PROFILE = pos.id
left join orders o on cs.ID_ORDER = o.id
where cs.DATE_BEGIN between @date_start and @date_end
and pos.code = '1.4' and o.order_type = 1
group by 1) pos on pd.ID_human = pos.ID_human
where cs.DATE_BEGIN between @date_start and @date_end and pd.IS_ACTIVE = 1
and (ds.doctor_id = @doc_id or @doc_id = 0)
and ((o.STATUS < 2 and  not @order_state) or (o.STATUS = 2 and  @order_state))
 and ((o.order_type = 0 and not @fin_type and pd.ID_PATIENT = any (select ID_PATIENT from pzp)) or
    (o.order_type =1 and @fin_type and pd.ID_HUMAN = any (select Id_Human from bzp)) and pos2.code <> '1.4')
and pd.IS_ACTIVE = 1 and pd.DATE_END = '2200-01-01'
and (o.orderCreateDate >= @date_start)
group by ds.doctor_id, pd.ID_HUMAN, o.ID
order by u.name, pat, o.id) T;