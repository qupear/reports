SET @date_start = '2020-01-20'; 
SET @date_end = '2020-02-25'; 
SET @doc_id = 0;

DROP TEMPORARY TABLE if EXISTS T1,T2,T3,T4,T5,T6,T7,T34_1,dental_dep,doctor_services,first_row;

CREATE TEMPORARY TABLE if NOT EXISTS doctor_services (
SELECT cs.date_begin, cs.id_case, p.code,
 vd.diagnosis_code, cs.tooth_name, p.uet, cs.is_oms, 
CASE WHEN YEAR(CS.DATE_BEGIN) - YEAR(pd.BIRTHDAY)
 - (DATE_FORMAT(CS.DATE_BEGIN, '%m%d') < DATE_FORMAT(pd.BIRTHDAY, '%m%d')) >= 60 AND pd.SEX = 'м' THEN 0 WHEN YEAR(CS.DATE_BEGIN) - YEAR(pd.BIRTHDAY)
 - (DATE_FORMAT(CS.DATE_BEGIN, '%m%d') < DATE_FORMAT(pd.BIRTHDAY, '%m%d')) >= 55 AND pd.SEX = 'ж' THEN 1 ELSE -1 END AS old_people,
 pd.ID_PATIENT
FROM case_services cs
LEFT JOIN cases c ON cs.id_case = c.id_case
LEFT JOIN patient_data pd ON c.ID_PATIENT = pd.id_patient
LEFT JOIN doctor_spec ds ON ds.doctor_id = cs.id_doctor
LEFT JOIN price p ON p.id = cs.id_profile AND cs.is_oms in (0, 3)
LEFT JOIN vmu_diagnosis vd ON cs.id_diagnosis = vd.id_diagnosis
LEFT JOIN departments dep ON dep.id = ds.department_id
WHERE (cs.id_doctor = @doc_id OR @doc_id = 0) 
AND (dep.id = @depart_id OR @depart_id = 0) 
AND cs.date_begin BETWEEN @date_start AND @date_end
and ds.spec_id = 211);

SET @ROW = 0;

CREATE TEMPORARY TABLE if NOT EXISTS first_row (
SELECT @ROW := @ROW + 1 AS st_num, T.date_begin AS st_date
FROM (
SELECT date_begin
FROM doctor_services ds
WHERE ds.is_oms <> 1
GROUP BY date_begin
ORDER BY date_begin) T);

/*приянто больных*/
CREATE TEMPORARY TABLE if NOT EXISTS T1 (
SELECT date_begin, COUNT(DISTINCT id_case) AS val
FROM doctor_services
WHERE code in ('2001', '2002')
GROUP BY date_begin
ORDER BY date_begin);

/*из них первичных*/
CREATE TEMPORARY TABLE if NOT EXISTS T2 (
SELECT date_begin, COUNT(DISTINCT id_case) AS val
FROM doctor_services
WHERE code in ('2001')
GROUP BY date_begin
ORDER BY date_begin);

/*пенс*/
CREATE TEMPORARY TABLE if NOT EXISTS T3 (
SELECT date_begin, COUNT(DISTINCT ID_PATIENT) AS val
FROM doctor_services
WHERE (old_people = 0 OR old_people = 1) AND is_oms <> 1
GROUP BY date_begin
ORDER BY date_begin);

/*заболевания пародонта*/
CREATE TEMPORARY TABLE if NOT EXISTS T4
 (
SELECT date_begin, COUNT(DISTINCT id_case) AS val
FROM doctor_services
WHERE diagnosis_code in ('K05.3', 'K05.31', 'K05.1', 'K05.4')
GROUP BY date_begin
ORDER BY date_begin);

/*слизистая*/
CREATE TEMPORARY TABLE if NOT EXISTS T5
 (
SELECT date_begin, COUNT(DISTINCT id_case) AS val
FROM doctor_services
WHERE diagnosis_code in ('K12.1', 'K12.2', 'K13.1', 'K13.2', 'K13.5', 'K13.6', 'K13.7', 'L43.0')
GROUP BY date_begin
ORDER BY date_begin);

/*снято зубных отложений*/
CREATE TEMPORARY TABLE if NOT EXISTS dental_dep
 (
SELECT date_begin, COUNT(DISTINCT id_case) AS val
FROM doctor_services
WHERE code in ('1201', '2107', '2108')
GROUP BY date_begin
ORDER BY date_begin);

/*help*/
CREATE TEMPORARY TABLE if NOT EXISTS T34_1
(
SELECT id
FROM protocols p
WHERE child_ref_ids LIKE '%55161(%' 
OR child_ref_ids LIKE '%71682(%' 
OR child_ref_ids LIKE '%102782(%' 
OR child_ref_ids LIKE '%78522(%' 
OR child_ref_ids LIKE '%127890(%' 
OR child_ref_ids LIKE '%127900(%' 
OR child_ref_ids LIKE '%127910(%' 
OR child_ref_ids LIKE '%125521(%' 
OR child_ref_ids LIKE '%127920(%' 
OR child_ref_ids LIKE '%127930(%' 
OR child_ref_ids LIKE '%127940(%' 
OR child_ref_ids LIKE '%20130(%' 
OR child_ref_ids LIKE '%127960(%' 
OR child_ref_ids LIKE '%127950(%' 
OR child_ref_ids LIKE '%127970(%' 
OR child_ref_ids LIKE '%127980(%' 
OR child_ref_ids LIKE '%127990(%');

/*завершено*/
CREATE TEMPORARY TABLE if NOT EXISTS T6 (
SELECT COUNT(DISTINCT ds.id_case) AS val, date_begin
FROM doctor_services ds
LEFT JOIN protocols p ON ds.id_case = p.id_case
WHERE ds.is_oms <> 1 AND p.id in (
SELECT id
FROM t34_1)
GROUP BY date_begin
ORDER BY date_begin);

/*YET*/
CREATE TEMPORARY TABLE if NOT EXISTS T7
 (
SELECT date_begin, ROUND(COALESCE(SUM(UET), 0), 2) AS val
FROM doctor_services
GROUP BY date_begin
ORDER BY date_begin);

SELECT fr.st_num, fr.st_date,
COALESCE(T1.val, 0), COALESCE(T2.val, 0), COALESCE(T3.val, 0), 
COALESCE(T4.val, 0), COALESCE(T5.val, 0), COALESCE(dd.val, 0), 
COALESCE(T6.val, 0), COALESCE(T7.val, 0)
FROM first_row fr
LEFT JOIN T1 ON T1.date_begin = fr.st_date
LEFT JOIN T2 ON T2.date_begin = fr.st_date
LEFT JOIN T3 ON T3.date_begin = fr.st_date
LEFT JOIN T4 ON T4.date_begin = fr.st_date
LEFT JOIN T5 ON T5.date_begin = fr.st_date
LEFT JOIN T6 ON T6.date_begin = fr.st_date
LEFT JOIN T7 ON T7.date_begin = fr.st_date
LEFT JOIN dental_dep dd ON dd.date_begin = fr.st_date
ORDER BY st_num;