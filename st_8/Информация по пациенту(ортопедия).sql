set @date_start = '2020-07-01';
set @date_end = '2020-07-31';
set @fin_type = 0;
set @depart_id = 0;

drop temporary table if exists f_date, cost, rg, refuse, ter, surg, orto,
	implant, privilege, start_bzp_ter_surg, orto_spec, visits;

CREATE TEMPORARY TABLE IF NOT EXISTS f_date (
SELECT pd.id_human, MIN(cs.date_begin) AS date_begin, cs.id_doctor
FROM 			 case_services cs
LEFT JOIN orders o ON o.id = cs.id_order
LEFT JOIN price p ON cs.id_profile = p.id AND ((cs.id_order > 0 AND o.order_type = 0) OR (cs.id_order = -1 AND cs.is_oms = 0))
LEFT JOIN price_orto_soc pos ON cs.id_profile = pos.id AND o.order_type = 1
LEFT JOIN cases c ON cs.id_case = c.id_case
LEFT JOIN patient_data pd ON pd.id_patient = c.id_patient
WHERE cs.date_begin BETWEEN @date_start AND @date_end
	AND ((p.code IN ('51001', '7077') AND NOT @fin_type) 
		OR (pos.code in ('1.4', '1.1') AND @fin_type))
GROUP BY pd.id_human);

/*Сумма по нарядам*/
CREATE TEMPORARY TABLE IF NOT EXISTS cost 	(
SELECT o.id_human, COALESCE(SUM(cs.service_cost), 0) summ
FROM 			orders o
JOIN f_date fd ON fd.id_human = o.id_human
LEFT JOIN case_services cs ON cs.id_order = o.id
WHERE o.ordercreatedate >= fd.date_begin AND o.order_type = (@fin_type > 0)
GROUP BY o.id_human);

/*RG*/
CREATE TEMPORARY TABLE IF NOT EXISTS rg (
SELECT prot.id_human AS id_h, id_ref, date_protocol AS d1
FROM 			 protocols prot
JOIN f_date fd ON prot.id_human = fd.id_human
WHERE prot.id_ref IN (8457, 8458, 8459) AND date_protocol >= @date_start
GROUP BY id_h, id_ref);

/*Ортопед от врача-специалиста*/
CREATE TEMPORARY TABLE IF NOT EXISTS orto_spec (
SELECT prot.id_human AS id_h, id_ref, date_protocol AS d_orto_spec
FROM 			 protocols prot
JOIN f_date fd ON prot.id_human = fd.id_human
WHERE prot.id_ref IN (141510,45144,46168,47404,48248,49538,52287,52858,69108,
					70188,71357,141520,141530,34548,23446,116431,141540,141550,141560) 
	AND date_protocol >= @date_start
GROUP BY id_h, id_ref);

/*Терап. санация*/
CREATE TEMPORARY TABLE IF NOT EXISTS ter (
SELECT prot.id_human AS id_h, id_ref, date_protocol AS d3
FROM protocols prot
WHERE prot.id_ref IN (8461,8680,8700,8710,8720,8730,8740,8750,8760,8770,8790) 
	AND date_protocol >= @date_start
GROUP BY id_h, id_ref);

/*Хирург. санация*/
CREATE TEMPORARY TABLE IF NOT EXISTS surg (
SELECT prot.id_human AS id_h, id_ref, date_protocol AS d4
FROM 			 protocols prot
JOIN f_date fd ON prot.id_human = fd.id_human
WHERE prot.id_ref IN (8470,8810,8820,8830) AND date_protocol >= @date_start
GROUP BY id_h, id_ref);

/*Направлен к ортопеду*/
CREATE TEMPORARY TABLE IF NOT EXISTS orto (
SELECT prot.id_human AS id_h, id_ref, 
	CASE WHEN SUBSTRING_INDEX(SUBSTRING_INDEX(prot.child_ref_ids, ';', -1), '(', 1) 
	IN (124430, 124440, 124450, 124460, 124470,
		124480, 124490, 124500, 124820, 124830,
		120100, 128250, 127600, 138550, 141490)
	THEN CONCAT(DATE_FORMAT(date_protocol,'%d-%m-%Y'), ' ', if(rp.text = '@ABC', at.text, rp.text)) END AS d5
FROM protocols prot
left join arbitrary_texts at on at.id = prot.id_arbitrary_text
JOIN f_date fd ON prot.id_human = fd.id_human
JOIN ref_protocols rp ON SUBSTRING_INDEX(SUBSTRING_INDEX(prot.child_ref_ids, ';', -1), '(', 1) = rp.id AND prot.id_ref = 8463 AND rp.id <> 8463
WHERE prot.id_ref IN (8463) AND date_protocol >= @date_start
GROUP BY prot.id_human, id_ref);

/*Имплантация*/
CREATE TEMPORARY TABLE IF NOT EXISTS implant (
SELECT prot.id_human AS id_h, id_ref, date_protocol AS d6
FROM 			 protocols prot
JOIN f_date fd ON prot.id_human = fd.id_human
WHERE prot.id_ref IN (8472,124720, 124730, 124740) AND date_protocol >= @date_start
GROUP BY prot.id_human, id_ref);

/*Льгота*/
CREATE TEMPORARY TABLE IF NOT EXISTS privilege (
SELECT prot.id_human AS id_h, id_ref, date_protocol AS d7
FROM 			 protocols prot
JOIN f_date fd ON prot.id_human = fd.id_human
WHERE prot.id_ref IN (8489) AND date_protocol >= @date_start
GROUP BY prot.id_human, id_ref);

/*Отметка о заверш. сан-и (тер/хирург)*/
CREATE TEMPORARY TABLE IF NOT EXISTS start_bzp_ter_surg (
SELECT prot.id_human AS id_h, id_ref, date_protocol AS d4,
	short_name(u.name) doc_name
FROM 			 protocols prot
JOIN f_date fd ON prot.id_human = fd.id_human
LEFT JOIN doctor_spec ds ON prot.id_doctor = ds.doctor_id
LEFT JOIN users u ON u.id = ds.user_id
WHERE (prot.id_ref IN (52285, 52855, 65865, 66965, 67905, 69105, 70185,
				71355, 118454, 118054, 46165,48245, 49535, 34441, 34374, 113794, 113144,
				112464, 111893, 111254, 110272, 107181, 115964,	115014, 77222, 124900,
				76632, 72812, 63452, 62522, 61411, 60522, 116404, 23441, 13729,	22557, 34543)
		AND NOT @fin_type)
	OR (prot.id_ref IN (113791,113141,112461,111861,111251,110271,107182,
						118051,118451,71352,70182,69102,67902,66962,65862, 52852,52282,
						49532,48242,46162,115965,115015,77223,116061,76633,72813,23921,
						23442,34544,63453,62523,116405,124910,22558)
		AND @fin_type)
	AND date_protocol >= @date_start
GROUP BY prot.id_doctor, id_h, id_ref);

/*Отказ от протезирования*/
CREATE TEMPORARY TABLE IF NOT EXISTS refuse (
SELECT prot.id_human AS id_h, id_ref, date_protocol AS d_refuse
FROM 			 protocols prot
JOIN f_date fd ON prot.id_human = fd.id_human
WHERE prot.id_ref IN (137691)
	AND date_protocol >= @date_start
GROUP BY prot.id_human, id_ref);

/*Даты санаций*/
CREATE TEMPORARY TABLE IF NOT EXISTS visits (
SELECT pd.id_human,
	CONCAT(DATE_FORMAT(cs.date_begin, '%d-%m-%Y'), '  ', short_name(u.name)) doc_v
FROM case_services cs
LEFT JOIN cases c ON cs.id_case = c.id_case
LEFT JOIN patient_data pd ON c.id_patient = pd.id_patient
LEFT JOIN doctor_spec ds ON cs.id_doctor = ds.doctor_id
LEFT JOIN users u ON ds.user_id = u.id
JOIN f_date fd ON pd.id_human = fd.id_human
WHERE cs.date_begin >= @date_start
	AND (ds.spec_id = 106 OR ds.spec_id = 108 OR ds.spec_id = 208 OR ds.spec_id = 211)
GROUP BY pd.id_human, cs.id_case, cs.id_doctor
ORDER BY pd.id_human, cs.date_begin); 

SET @ROW = 0;

SELECT @ROW := @ROW + 1, t.*
FROM
 (
	SELECT CONCAT_WS(' ', pd.surname, pd.name, pd.second_name) AS pat,
		DATE_FORMAT(fd.date_begin, '%d-%m-%Y') f_date,
		short_name(u.name) doc,
		COALESCE(DATE_FORMAT(rg.d1, '%d-%m-%Y'), '') 'RG',
		COALESCE(DATE_FORMAT(ter.d3, '%d-%m-%Y'), '') 'Ter_san',
		COALESCE(DATE_FORMAT(surg.d4, '%d-%m-%Y'), '') 'Surg_san',
		COALESCE(DATE_FORMAT(os.d_orto_spec, '%d-%m-%Y'), '') 'Orto_spec',
		COALESCE(orto.d5, '') 'Orto', COALESCE(implant.d6, '') 'Implant',
		COALESCE(PRIV.d7, '') 'Privilege',
		GROUP_CONCAT(DISTINCT v.doc_v) 'Date_san',
		CONCAT(DATE_FORMAT(sbts.d4, '%d-%m-%Y'), ' ', sbts.doc_name) 'Finish_san',
		COALESCE(DATE_FORMAT(refuse.d_refuse, '%d-%m-%Y'), '') 'Refuse',
		COALESCE((cost.summ), 0) 'Summ'
	FROM
	 f_date fd
	LEFT JOIN cost ON fd.id_human = cost.id_human
	LEFT JOIN patient_data pd ON pd.id_human = fd.id_human AND pd.is_active = 1 AND pd.date_end = '2200-01-01'
	LEFT JOIN doctor_spec ds ON ds.doctor_id = fd.id_doctor
	LEFT JOIN users u ON ds.user_id = u.id
	LEFT JOIN rg ON pd.id_human = rg.id_h
	LEFT JOIN orto_spec os ON pd.id_human = os.id_h
	LEFT JOIN ter ON pd.id_human = ter.id_h
	LEFT JOIN surg ON pd.id_human = surg.id_h
	LEFT JOIN orto ON pd.id_human = orto.id_h
	LEFT JOIN start_bzp_ter_surg sbts ON sbts.id_h = pd.id_human
	LEFT JOIN visits v ON pd.id_human = v.id_human
	LEFT JOIN implant ON PD.id_human = implant.id_h
	LEFT JOIN privilege priv ON PD.id_human = PRIV.id_h
	LEFT JOIN refuse ON pd.id_human = refuse.id_h
	WHERE
	 ds.department_id = @depart_id OR @depart_id = 0
	GROUP BY fd.id_human
	ORDER BY u.name, pat) t;