set @date_start = '2021-02-01';
set @date_end = '2021-02-10';
 drop temporary table if exists doctor_services, doctor_directions, T1, T2, T3, T4, T5, T6;



create temporary table if not exists doctor_services (select cs.date_begin, cs.id_case, vp.profile_infis_code, p.code, cs.is_oms, ds.doctor_id, u.name
                                                      from case_services cs
                                                               left join vmu_profile vp on vp.id_profile = cs.id_profile and cs.is_oms = 1
                                                               left join price p on p.id = cs.id_profile and cs.is_oms in (0, 3, 4)
                                                               left join cases c on c.id_case = cs.id_case
                                                               
															   left join doctor_spec ds on cs.id_doctor = ds.doctor_id
																left join users u on u.id = ds.user_id
                                                               left join (select vt.uet, vt.id_profile, vt.id_zone_type,
                                                                                 vt.tariff_begin_date as d1,
                                                                                 vt.tariff_end_date as d2,
                                                                                 vtp.tp_begin_date as d3,
                                                                                 vtp.tp_end_date as d4
                                                                          from vmu_tariff vt,
                                                                               vmu_tariff_plan vtp
                                                                          where vtp.id_tariff_group = vt.id_lpu) T
                                                                         on T.id_profile = vp.id_profile and
                                                                            cs.date_begin between T.d1 and T.d2 and
                                                                            cs.date_begin between T.d3 and T.d4 and
                                                                            T.id_zone_type = cs.id_net_profile and
                                                                            cs.is_oms = 1
                                                      where (cs.date_begin between @date_start and @date_end)
														and ds.department_id = 14
                                                        and (p.code in ('5002', '5004', '5003', '5011', '5005', '5006', '5007', '5008', '5010')
                                                        or vp.profile_infis_code in ('сто018')));

create temporary table if not exists doctor_directions (select d.id_case, d.date_direction as date_begin, u.name,
                                                              locate(';90', d.child_ref_ids) > 0 as pPR1,
                                                              locate(';100', d.child_ref_ids) > 0 as pPR2,
                                                              locate(';160', d.child_ref_ids) > 0 as pOPTG,
                                                              locate(';205', d.child_ref_ids) > 0 as pKLKT
                                                       from directions d
                                                                left join doctor_spec ds on d.id_doctor = ds.doctor_id
																left join users u on u.id = ds.user_id
                                                                where d.date_direction between @date_start and @date_end
                                                                and d.active = 0
                                                                group by id_case);
                                                                
create temporary table if not exists doctor_descriptions (select d.id_case, d.date_direction as date_begin, u.name, d.id,
                                                              locate(';90', d.child_ref_ids) > 0 as pPR1,
                                                              locate(';100', d.child_ref_ids) > 0 as pPR2,
                                                              locate(';160', d.child_ref_ids) > 0 as pOPTG,
                                                              locate(';205', d.child_ref_ids) > 0 as pKLKT
                                                       from directions d
                                                                left join doctor_spec ds on d.id_doctor = ds.doctor_id
																left join users u on u.id = ds.user_id
                                                                left join roentgen r on d.id = r.id_direction
                                                                where d.date_direction between @date_start and @date_end
                                                                and d.active = 0
                                                                and r.date_description is not null and r.doc_id = 351
                                                                group by id_case);


create temporary table if not exists T1 (select name, count(id_case) as val
                                         from doctor_services
											where code in ('5002', '5004')
												or profile_infis_code in ('сто018')
                                         group by name
                                         order by name);
create temporary table if not exists T2 (select name, count(id_case) as val
                                         from doctor_services
                                         where code in ('5003', '5011')
                                         group by name
                                         order by name);
create temporary table if not exists T3 (select name, count(id_case) as val
                                         from doctor_services
                                         where code in ('5005', '5006', '5007', '5008', '5010')
                                         group by name
                                         order by name);
create temporary table if not exists T4 (select name, count(id_case) as val
                                         from doctor_directions
                                         where pPR1 or pPR2
                                         group by name
                                         order by name);
create temporary table if not exists T5 (select name, count(id_case) as val
                                         from doctor_directions
                                         where pOPTG
                                         group by name
                                         order by name);
create temporary table if not exists T6 (select name, count(id_case) as val
                                         from doctor_directions
                                         where pKLKT
                                         group by name
                                         order by name);
create temporary table if not exists T7 (select name, count(id_case) as val
                                         from doctor_descriptions
                                         where pPR1 or pPR2
                                         group by name
                                         order by name);
create temporary table if not exists T8 (select name, count(id_case) as val
                                         from doctor_descriptions
                                         where pOPTG
                                         group by name
                                         order by name);
create temporary table if not exists T9 (select name, count(id_case) as val
                                         from doctor_descriptions
                                         where pKLKT
                                         group by name
                                         order by name);
                                         
                                         
select ds.name, coalesce(T1.val , 0), coalesce(T2.val , 0), coalesce(T3.val , 0), coalesce(T4.val , 0), coalesce(T5.val , 0), coalesce(T6.val , 0), 
			coalesce(T7.val , 0) , coalesce(T8.val , 0) , coalesce(T9.val , 0)      
									from doctor_services ds 
                                    left join T1 on T1.name = ds.name
                                    left join T2 on T2.name = ds.name
                                    left join T3 on T3.name = ds.name
                                    left join T4 on T4.name = ds.name
                                    left join T5 on T5.name = ds.name
                                    left join T6 on T6.name = ds.name
                                    left join T7 on T7.name = ds.name
                                    left join T8 on T8.name = ds.name
                                    left join T9 on T9.name = ds.name
                                    group by ds.name
                                    order by ds.name
                                    