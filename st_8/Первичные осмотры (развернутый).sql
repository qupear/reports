set @row_num = 0;
select @row_num := @row_num + 1, T.dep_name, T.doc_name, T.pat, T.card_number, T.unstruct_address, T.remark,
       T.date_begin, T.summ, coalesce(T.cost, 0)
from (select dep.name as dep_name, short_name(u.name) as doc_name,
             concat(pd.surname, ' ', pd.name, ' ', pd.second_name, ', д.р. ', pd.birthday) as pat, pd.card_number,
             cs.date_begin, round(coalesce(sum(cs.SERVICE_COST), 0), 2) as summ, pa.unstruct_address, pd.remark,
             o_cost.cost as cost
    from case_services cs
               left join cases c on c.id_case = cs.id_case
               left join patient_data pd on pd.id_patient = c.id_patient
               left join patient_address pa on pa.id_address = pd.id_address_reg
               left join doctor_spec ds on ds.doctor_id = cs.id_doctor
               left join users u on ds.user_id = u.id and u.id <> 1
               left join departments dep on ds.department_id = dep.id
               left join price p on p.id = cs.id_profile
left join (select sum(cs.SERVICE_COST) as cost, o.Id_Human from case_services cs
    left join orders o on cs.ID_ORDER = o.ID
where cs.date_begin >= @date_start
group by id_human) o_cost on pd.ID_HUMAN = o_cost.Id_Human
      where cs.date_begin between @date_start and @date_end
        and ds.user_id <> 1
        and (dep.id = @depart_id OR @depart_id = 0 OR @depart_id is null)
        and (ds.doctor_id = @doc_id OR @doc_id = 0 OR @doc_id is null)
        and cs.is_oms = 0
        and cs.id_order = -1
        and cs.is_paid > 0
        and p.code in ('51001', '7077')
      group by pd.id_patient
      order by dep.name, doc_name, pd.surname, pd.name, pd.second_name) T;