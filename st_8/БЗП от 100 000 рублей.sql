set @date_start = '2019-01-01';
set @date_end = '2020-09-30';
set @order_state = 0;
set @doc_id = 0;


drop temporary table if exists bzp_100k_1, bzp_100k;

create temporary table if not exists bzp_100k_1  
select id_human from 
(select ord.id_human, sum(cs.service_cost) as summ 
from orders ord 
left join case_services cs on cs.id_order = ord.id 
where ord.order_type = 1
and ((ord.ordercreatedate between @date_start and @date_end) or (ord.ordercompletiondate between @date_start and @date_end)) 
group by ord.id_human) T 
where T.summ > 100000;

set @row_pat = 0;

create temporary table if not exists bzp_100k 
select T.id_human, @row_pat := @row_pat + 1 as r_pat, T.pat_name as pat_name, T.orderCreateDate,
 T.ordercompletiondate from 
(select ord.id_human, concat(pd.surname, " ", pd.name, " ", pd.second_name) as pat_name,
ord.orderCreateDate,
 ord.ordercompletiondate   
from orders ord 
left join case_services cs on cs.id_order = ord.id 
left join patient_data pd on pd.id_human = ord.id_human and pd.is_active = 1 and pd.date_end = '2200-01-01' 
where ord.order_type = 1 
	and ord.id_human in (select id_human from bzp_100k_1)
	and cs.id_service is not null 
    
	and (
			(ord.orderCreateDate between @date_start and @date_end
			and @order_state = 0
			and ord.ordercompletiondate is null
			) 
	
		or (
			(ord.orderCompletionDate between @date_start and @date_end and @order_state = 1)
			and (year(ord.ordercompletiondate) - year(ord.orderCreateDate)  <= 1)
			)	
		)
	
	and (ord.id_doctor = @doc_id or @doc_id = 0 or @doc_id is null)  
group by ord.id_human order by pat_name) T; 

set @row = 0;

select T.r_pat,
@row := @row + 1,
T.pat, 
T.order_number,
T.orderCreateDate,
T.doc_name,
T.tech_name,
T.complete_date,
T.total
from 
(select concat(b100.pat_name, " / ", pa.unstruct_address, " / Направление №", ord.bzp_num, " от ", ord.bzp_date) as pat, b100.r_pat, 
		ord.order_number, ord.orderCreateDate, u.name as doc_name, 
        case when t.name = ".Без техника" then "" else t.name end as tech_name, 
        coalesce(ord.orderCompletionDate, "") as complete_date, sum(cs.service_cost) as total   
from orders ord 
left join patient_data pd on pd.id_human = ord.id_human and pd.is_active = 1 and pd.date_end = '2200-01-01' 
left join patient_address pa on pa.id_address = pd.id_address_reg 
left join doctor_spec ds on ds.doctor_id = ord.id_doctor 
left join users u on u.id = ds.user_id 
left join technicians t on t.id = ord.id_technician  
left join case_services cs on cs.id_order = ord.id 
left join bzp_100k b100 on b100.id_human = ord.id_human 
where ord.order_type = 1 and b100.id_human is not null and cs.id_service is not null     
group by ord.id order by pat) T 