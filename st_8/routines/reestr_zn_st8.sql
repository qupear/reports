DELIMITER $$
CREATE DEFINER=`test_user`@`%` PROCEDURE `reestr_zn_st8`(IN date_start date, IN date_end date, IN closed int, IN pu int)
begin
    set @rn = 0;
select  T.name, T.ord_num, T.pat, T.adr,
 T.direction,
    case when T.service_type = 100 then 'Починка'
         when T.service_type = 90 then 'Протезирование'
         when T.service_type = 75 then 'Сн. и цем. коронок'
         when T.service_type = 50 then 'Имплантология'
    end, T.orderCreateDate, T.orderCompletionDate, T.summ
from
(select o.id, sr.name, group_concat(distinct o.order_number separator  ',') as ord_num,
       concat(pd.surname,' ', pd.name,' ', pd.second_name) as pat, pa.UNSTRUCT_ADDRESS as adr,
       case  when o.bzp_direction_text is not null then  (select o.bzp_direction_text)
when o.bzp_direction_text = '' then (select o.bzp_direction_text)
else 'Не указан' end as direction
,  max(case when p.group_ids like '%1%' then 100 /*'Починка'*/
                        when p.group_ids like '%4%' then 90 /*'Протезирование'*/
                        when p.group_ids like '%3%' then 75 /*'Сн. и цем. коронок'*/
                        when p.group_ids like '%2%' then 50 /*'Имплантология'*/
                        when pos.ID_GROUP like '%111%' then 100 /*'Починка'*/
                        when pos.ID_GROUP like '%38%' or pos.ID_GROUP like '%39%' or pos.ID_GROUP like '%40%' or pos.ID_GROUP like '%41%' then 90 /*'Протезирование'*/
                        when pos.ID_GROUP like '%112%' then 75 /*'Сн. и цем. коронок'*/
                        else 90 /*'Протезирование'*/ end)
                    as service_type , o.orderCreateDate,  o.orderCompletionDate, sum(cs.SERVICE_COST ) as summ
FROM orders o
    left join case_services cs on cs.id_order = o.id
    left join patient_data pd on o.id_human= pd.id_human and pd.is_active = 1 and pd.date_end = '2200-01-01'
    left join patient_address pa on pd.ID_ADDRESS_REG = pa.ID_ADDRESS
    left join discounts dis on dis.id = o.id_discount
    left join price_orto_soc pos on cs.id_profile = pos.id and o.order_type = 1
    left join price p on cs.id_profile = p.id and o.order_type <> 1
    left join spb_regions sr on o.bzp_region = sr.id
WHERE  ((o.orderCompletionDate between date_start and date_end and o.status = 2) * closed
			or (o.orderCreateDate between date_start and date_end and o.status < 2 and o.orderCompletionDate is null) * (not closed)) and
  ((o.order_type = 0 and pu = 0) or (o.order_type = 2 and pu = 1) or (o.order_type = 1 and pu = 2))
group by sr.name, pd.id_human order by pat)  T
union
select '', '', '', '', 'Итого: сумма по заказ-нарядам', '', '', '', sum(cs.SERVICE_COST ) as summ
FROM orders o
left join case_services cs on cs.id_order = o.id
left join patient_data pd on o.id_human= pd.id_human and pd.is_active = 1 and pd.date_end = '2200-01-01'
    left join patient_address pa on pd.ID_ADDRESS_REG = pa.ID_ADDRESS
    left join discounts dis on dis.id = o.id_discount
    left join price_orto_soc pos on cs.id_profile = pos.id
    left join price p on cs.id_profile = p.id
    WHERE  ((o.orderCompletionDate between date_start and date_end and o.status = 2) * closed
			or (o.orderCreateDate between date_start and date_end and o.status < 2 and o.orderCompletionDate is null ) * (not closed))
 and   ((o.order_type = 0 and pu = 0) or (o.order_type = 2 and pu = 1) or (o.order_type = 1 and pu = 2))
union
select '','','','','', 'Из них починок: ', coalesce(count(T.count_hum),0), 'На сумму: ', sum(T.summ) from
( select  count(distinct o.id_human) as count_hum, max(case when p.group_ids like '%1%' then 100 /*'Починка'*/
                        when p.group_ids like '%4%' then 90 /*'Протезирование'*/
                        when p.group_ids like '%3%' then 75 /*'Сн. и цем. коронок'*/
                        when p.group_ids like '%2%' then 50 /*'Имплантология'*/
                        when pos.ID_GROUP like '%111%' then 100 /*'Починка'*/
                        when pos.ID_GROUP like '%38%' or pos.ID_GROUP like '%39%' or pos.ID_GROUP like '%40%' or pos.ID_GROUP like '%41%' then 90 /*'Протезирование'*/
                        when pos.ID_GROUP like '%112%' then 75 /*'Сн. и цем. коронок'*/
                        else 90 /*'Протезирование'*/ end)
                    as service_type ,  sum(cs.SERVICE_COST ) as summ
FROM orders o
left join case_services cs on cs.id_order = o.id
left join patient_data pd on o.id_human= pd.id_human and pd.is_active = 1 and pd.date_end = '2200-01-01'
    left join patient_address pa on pd.ID_ADDRESS_REG = pa.ID_ADDRESS
    left join discounts dis on dis.id = o.id_discount
    left join price_orto_soc pos on cs.id_profile = pos.id
    left join price p on cs.id_profile = p.id
      WHERE  ((o.orderCompletionDate between date_start and date_end and o.status = 2) * closed
			or (o.orderCreateDate between date_start and date_end and o.status < 2 and o.orderCompletionDate is null ) * (not closed))
 and   ((o.order_type = 0 and pu = 0) or (o.order_type = 2 and pu = 1) or (o.order_type = 1 and pu = 2))
            group by o.id_human
 having service_type = 100) T
union
select '', '', '', '', 'Итого: всего пациентов', '', '', '', count(distinct o.Id_Human)
FROM orders o
    WHERE  ((o.orderCompletionDate between date_start and date_end and o.status = 2) * closed
			or (o.orderCreateDate between date_start and date_end and o.status < 2 and o.orderCompletionDate is null ) * (not closed))
 and   ((o.order_type = 0 and pu = 0) or (o.order_type = 2 and pu = 1) or (o.order_type = 1 and pu = 2))
union
select '', '', '', '','Итого: всего заказ-нарядов' , '', '', '', count(o.ID)
FROM orders o
    WHERE  ((o.orderCompletionDate between date_start and date_end and o.status = 2) * closed
			or (o.orderCreateDate between date_start and date_end and o.status < 2 and o.orderCompletionDate is null) * (not closed))
 and   ((o.order_type = 0 and pu = 0) or (o.order_type = 2 and pu = 1) or (o.order_type = 1 and pu = 2))
;
  end$$
DELIMITER ;
