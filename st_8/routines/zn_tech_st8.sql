-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
DELIMITER $$

CREATE DEFINER=`test_user`@`%` PROCEDURE `zn_tech_st8`(IN date_start date, IN date_end_ date, IN closed int, IN pu int, IN techn_id int, depart_id int)
begin
    set @rn = 0;
select  @rn := @rn + 1, T.tech_name, T.order_number, T.orderCreateDate, T.orderCompletionDate, 
    case when T.service_type = 100 then 'Починка' 
		when T.service_type = 90 then 'Протезирование'     
		when T.service_type = 75 then 'Имплантология' 
		when T.service_type = 50 then 'Сн. и цем. коронок' 
		else 'Другое' end as group_name, 
    T.total, T.doc_name, T.pat 
from
(select t.name as tech_name, ord.order_number, ord.orderCreateDate, ord.orderCompletionDate,
        MAX(case when p.group_ids like '1;%' or p.code in ('7167', '7168', '7169', '7172', '7173', '7174') then 100 /*'Починка'*/ 
                        when p.group_ids like '4;%' or
(p.code between '7077' and '7174' and p.code not in ( '7084', '7085', '7086', '7144', '7145', '7146', '7147', '7077', '7078',
 '7217', '7079', '7080', '7167', '7168', '7169', '7172', '7173', '7174')) then 90 /*'Протезирование'*/ 
                        when p.group_ids like '2;%' then 75 /*'Имплантология'*/ 
                        when p.group_ids like '3;%' or p.code in ('7144', '7145', '7146', '7147') then 50 /*'Сн. и цем. коронок'*/  
             when pos.ID_GROUP like '%111%' then 100 /*'Починка'*/ 
             when pos.ID_GROUP like '%38%' or pos.ID_GROUP like '%39%' or pos.ID_GROUP like '%40%' or pos.ID_GROUP like '%41%' then 90 /*'Протезирование'*/              
             when pos.ID_GROUP like '%112%' then 50 /*'Сн. и цем. коронок'*/ 
             else 0 /*'Другое'*/ end) as service_type,
        sum(cs.service_cost) as total,
        short_name(u.name) as doc_name, short_name(concat_ws(' ', pd.surname, pd.name, pd.second_name)) as pat
from case_services cs
left join orders ord on ord.id = cs.id_order
left join doctor_spec ds on ds.doctor_id = ord.id_doctor
left join users u on u.id = ds.user_id
left join technicians t on t.id = ord.id_technician
left join price p on p.id = cs.id_profile and ord.order_type <> 1 
left join price_orto_soc pos on pos.id = cs.id_profile and ord.order_type = 1 
left join patient_data pd on pd.id_human = ord.id_human and pd.is_active = 1 and pd.date_end = '2200-01-01'
left join patient_address pa on pa.id_address = pd.id_address_reg and pd.id_address_reg <> 1
LEFT JOIN departments d ON d.id = ds.department_id
where ((ord.orderCompletionDate between date_start and date_end_ and ord.status = 2) * closed
    or (ord.orderCreateDate between date_start and date_end_ and ord.status < 2 ) * (not closed))
  and (ord.id_technician = techn_id or techn_id = 0) and t.id <> 1
  and ((p.in_tech = 1 and pu in (0,1)) or (pos.in_tech = 1 and pu = 2))
  and ((ord.order_type = 0 and pu = 0) or (ord.order_type = 1 and pu = 2) or (ord.order_type = 2 and pu = 1))
AND (d.id = depart_id OR depart_id = 0)
 group by ord.id 
order by tech_name, doc_name, pat) T;
end