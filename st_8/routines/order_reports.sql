-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `orders_report`(date_start DATE, date_end DATE, doc_id INT, depart_id INT, techn_id INT, closed BOOL, pu INT, spec INT, tech BOOL, detail INT)
BEGIN
	if detail = 0 then
		select d.name as dept_name, 
			case tech when True then t.name else u.name end as doc_name,  
			case tech when True Then 
				concat('Наряд ', ord.Order_Number, ' (пациент №', pd.card_number, ' ', pd.surname, ' ', pd.name, ' ', pd.second_name, '), врач ', u.name )
			else
				concat('Наряд ', ord.Order_Number, ' (пациент №', pd.card_number, ' ', pd.surname, ' ', pd.name, ' ', pd.second_name, '), техник ', t.name )
			end as order_name,  
            case pu when 0 then p.code when 1 then p.code else pos.code end as serv_code, 
            case pu when 0 then p.name when 1 then p.name else pos.name end as serv_name, 
            count(cs.id_profile) as cnt, sum(cs.service_cost * cs.discount) +    
			sum(cs.SERVICE_COST * (1 - cs.discount)*(cs.discount=0.3 or cs.discount=0)) as sum     
		from orders ord 
		left join case_services cs on cs.id_order = ord.id 
		left join doctor_spec ds on ds.doctor_id = ord.id_doctor 
		left join departments d on d.id = ds.department_id 
		left join users u on u.id = ds.user_id 
		left join patient_data pd on pd.id_human = ord.id_human  and pd.IS_ACTIVE = 1 and pd.DATE_END = '2200-01-01' 
		left join technicians t on t.id = ord.id_technician 
		left join price p on p.id = cs.id_profile and ord.order_type in (0,2) 
		left join price_orto_soc pos on pos.id = cs.id_profile and ord.order_type = 1  
		where ((ord.orderCompletionDate between date_start and date_end and ord.status = 2) * closed 
			or (ord.orderCreateDate between date_start and date_end and ord.status < 2) * (not closed))
			and (d.id = depart_id or depart_id = 0)  
			and (ds.doctor_id = doc_id or doc_id = 0)  
			and (ord.id_technician = techn_id or techn_id = 0) 
                    and ((tech <> 1) or ((p.in_tech = 1 and pu in (0,1)) or (pos.in_tech = 1 and pu = 2)))  
			and ((ord.order_type = 0 and pu = 0) or (ord.order_type = 2 and pu = 1) or (ord.order_type = 1 and pu = 2)) 
			and ds.spec_id = spec 
			group by ord.id, cs.id_profile 
			having cnt > 0
			order by dept_name, doc_name, ord.id ;
	elseif detail = 1 then 
		set @row_num = 0;
		set @row_pat = 0;
        drop temporary table if exists row_pat;
        create temporary table if not exists row_pat
        (select @row_pat :=@row_pat +1 as row, P.* from (select 
ord.id_human, ord.id_doctor, ord.id_technician, concat(pd.surname, ' ', pd.name, ' ', pd.second_name) as pat,
        sum(cs.service_cost * cs.discount) +    
				sum(cs.SERVICE_COST * (1 - cs.discount)*(cs.discount=0.3 or cs.discount=0)) as sum 
        from orders ord
        left join case_services cs on cs.id_order = ord.id 
		left join doctor_spec ds on ds.doctor_id = ord.id_doctor 
		left join departments d on d.id = ds.department_id 
		left join users u on u.id = ds.user_id 
		left join patient_data pd on pd.id_human = ord.id_human  and pd.IS_ACTIVE = 1 and pd.DATE_END = '2200-01-01' 
		left join technicians t on t.id = ord.id_technician  
        left join price p on p.id = cs.id_profile and ord.order_type in (0,2) 
		left join price_orto_soc pos on pos.id = cs.id_profile and ord.order_type = 1 
		  where ((ord.orderCompletionDate between date_start and date_end and ord.status = 2) * closed 
			or (ord.orderCreateDate between date_start and date_end and ord.status < 2) * (not closed))
			and (d.id = depart_id or depart_id = 0)  
			and (ds.doctor_id = doc_id or doc_id = 0)  
			and (ord.id_technician = techn_id or techn_id = 0)  
                and ((tech <> 1) or ((p.in_tech = 1 and pu in (0,1)) or (pos.in_tech = 1 and pu = 2)))    
			and ((ord.order_type = 0 and pu = 0) or (ord.order_type = 2 and pu = 1) or (ord.order_type = 1 and pu = 2))  
			and (ds.spec_id = spec or spec = 0)
          group by ds.doctor_id , t.id ,  pd.id_human -- changed grouping
			having sum > 0
            order by case 
                when tech then t.name 
                else u.name end, pat)P);
            
	select T.dept_name, T.doc_name, concat( @row_num := @row_num+1, '.   ', T.order_name), T.sum from
		(select d.name as dept_name, 
			case tech when True then t.name else u.name end as doc_name,  
			case tech when True Then 
				concat('|Пац-т № ', row_pat.row, '|  ', pd.surname, ' ', pd.name, ' ', pd.second_name, ', карта №', pd.card_number, ', Наряд ', ord.Order_Number, ', врач ', u.name )
			else
				concat('|Пац-т № ', row_pat.row, '|  ', pd.surname, ' ', pd.name, ' ', pd.second_name, ', карта №', pd.card_number, ', Наряд ', ord.Order_Number, ', техник ', t.name )
			end as order_name,  
			sum(cs.service_cost * cs.discount) +    
				sum(cs.SERVICE_COST * (1 - cs.discount)*(cs.discount=0.3 or cs.discount=0)) as sum     
		from orders ord 
		left join case_services cs on cs.id_order = ord.id 
		left join doctor_spec ds on ds.doctor_id = ord.id_doctor 
		left join departments d on d.id = ds.department_id 
		left join users u on u.id = ds.user_id 
		left join patient_data pd on pd.id_human = ord.id_human  and pd.IS_ACTIVE = 1 and pd.DATE_END = '2200-01-01' 
		left join technicians t on t.id = ord.id_technician  
        left join price p on p.id = cs.id_profile and ord.order_type in (0,2) 
		left join price_orto_soc pos on pos.id = cs.id_profile and ord.order_type = 1 
        left join row_pat on ord.id_human = row_pat.id_human and ord.id_doctor = row_pat.id_doctor   and ord.Id_technician = row_pat.id_technician
		where ((ord.orderCompletionDate between date_start and date_end and ord.status = 2) * closed 
			or (ord.orderCreateDate between date_start and date_end and ord.status < 2) * (not closed))
			and (d.id = depart_id or depart_id = 0)  
			and (ds.doctor_id = doc_id or doc_id = 0)  
			and (ord.id_technician = techn_id or techn_id = 0)  
                and ((tech <> 1) or ((p.in_tech = 1 and pu in (0,1)) or (pos.in_tech = 1 and pu = 2)))    
			and ((ord.order_type = 0 and pu = 0) or (ord.order_type = 2 and pu = 1) or (ord.order_type = 1 and pu = 2))  
			and (ds.spec_id = spec or spec = 0)
			group by ord.id  
			having sum > 0
			order by dept_name, doc_name, pd.surname, pd.name, pd.second_name, ord.id ) T
            ;
	else 
		select d.name as dept_name, 
			case tech when True then t.name else u.name end as doc_name,  
			sum(cs.service_cost * cs.discount) +    
				sum(cs.SERVICE_COST * (1 - cs.discount)*(cs.discount=0.3 or cs.discount=0)) as sum, 
                            case pu when 0 then sum(p.uet) when 1 then 0 else sum(pos.uet) end       
		from orders ord 
		left join case_services cs on cs.id_order = ord.id 
		left join doctor_spec ds on ds.doctor_id = ord.id_doctor 
		left join departments d on d.id = ds.department_id 
		left join users u on u.id = ds.user_id 
		left join patient_data pd on pd.id_human = ord.id_human  and pd.IS_ACTIVE = 1 and pd.DATE_END = '2200-01-01' 
		left join technicians t on t.id = ord.id_technician 
		left join price p on p.id = cs.id_profile and ord.order_type in (0,2) 
		left join price_orto_soc pos on pos.id = cs.id_profile and ord.order_type = 1  
		where ((ord.orderCompletionDate between date_start and date_end and ord.status = 2) * closed 
			or (ord.orderCreateDate between date_start and date_end and ord.status < 2) * (not closed))
			and (d.id = depart_id or depart_id = 0)  
			and (ds.doctor_id = doc_id or doc_id = 0)  
			and (ord.id_technician = techn_id or techn_id = 0) 
                and ((tech <> 1) or ((p.in_tech = 1 and pu in (0,1)) or (pos.in_tech = 1 and pu = 2)))  
			and ((ord.order_type = 0 and pu = 0) or (ord.order_type = 2 and pu = 1) or (ord.order_type = 1 and pu = 2)) 
			and ds.spec_id = spec 
			group by case 
                when tech then t.id 
                else ds.doctor_id   
                end
			order by dept_name, doc_name, pd.surname, pd.name, pd.second_name, ord.id;
	end if;
   END