-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `stoma_8_work_types`(date_start DATE, date_end DATE, closed INT, pu INT, tech INT, doc int, techn int, depart_id int)
BEGIN
drop temporary table if exists orders_services;
drop temporary table if exists orders_repair;
drop temporary table if exists orders_protez;
drop temporary table if exists orders_cement;
drop temporary table if exists orders_implant;
drop temporary table if exists cement_docs;
drop temporary table if exists repair_docs;
drop temporary table if exists protez_docs;
drop temporary table if exists implant_docs;
drop temporary table if exists anest_docs;
drop temporary table if exists osmotr_docs;
drop temporary table if exists orders_anest;
drop temporary table if exists orders_osmotr;
/*2019-04-29 shamshurina added sort by doc and techn*/
/*2019-07-30 shamshurina изменена логика сортировки по типам работ, коды 7*** */
create temporary table if not exists orders_services
(select cs.id_order, ord.id_doctor, cs.service_cost, cs.discount,
    ord.order_type, cs.id_profile as id_profile, p.code as p_code, p.group_ids as p_group, pos.code as pos_code, pos.id_group as pos_group,
        coalesce(p.in_tech,0) + coalesce(pos.in_tech,0) as in_tech, t.id as id_tech, t.name as tech_name
from case_services cs
left join orders ord on ord.id = cs.id_order
left join price p on p.id = cs.id_profile and ord.order_type <> 1
left join price_orto_soc pos on pos.id = cs.id_profile and ord.order_type = 1
left join doctor_spec ds on ds.doctor_id = ord.id_doctor
left join technicians t on t.id = ord.id_technician
LEFT JOIN departments d ON d.id = ds.department_id
where cs.id_order > 0
        and ((ord.order_type <> 1 and pu = 0) or (ord.order_type = 1 and pu = 2))
        and ds.spec_id = 107
and ((doc = ds.doctor_id or doc = 0)
 and (techn = t.id or techn = 0))
AND (d.id = depart_id OR depart_id = 0)
 		and ((ord.orderCompletionDate between date_start and date_end and ord.status = 2 and closed) or
             (ord.orderCreateDate between date_start and date_end and ord.status < 2 and not closed)))  ;

/* REPAIRS */
create temporary table if not exists orders_repair
(select id_order from orders_services os
where ((os.order_type <> 1 and (p_group like '1;%' or p_code in ('7167', '7168', '7169', '7172', '7173', '7174'))) or (os.order_type = 1 and pos_group like '%111%'))
group by os.id_order);
create temporary table if not exists repair_docs
(select id_doctor, id_tech, sum(service_cost * discount) as total from orders_services
where id_order in (select id_order from orders_repair) and in_tech >= tech
group by case when tech = 0 then id_doctor else id_tech end);

/* PROTEZ */
create temporary table if not exists orders_protez
(select id_order from orders_services os
where ((os.order_type <> 1 and (p_group like '4;%' or
(p_code between '7077' and '7174' or p_code in ('7222', '7218', '7224', '7225', '7226', '7220', '7219', '7221') and p_code not in ( '7084', '7085', '7086', '7144', '7145', '7146', '7147', '7077', '7078',
 '7217', '7079', '7080', '7167', '7168', '7169', '7172', '7173', '7174')))) 
or (os.order_type = 1 and pos_group like '%38%' or pos_group like '%39%' or pos_group like '%40%' or pos_group like '%41%'))
        and os.id_order not in (select id_order from orders_repair)
group by os.id_order);
create temporary table if not exists protez_docs
(select id_doctor, id_tech, sum(service_cost * discount) as total from orders_services
where id_order in (select id_order from orders_protez) and in_tech >= tech
group by case when tech = 0 then id_doctor else id_tech end);

/* CEMENT */
create temporary table if not exists orders_cement
(select id_order from orders_services os
where ((os.order_type <> 1 and (p_group like '3;%' or p_code in ('7144', '7145', '7146', '7147'))) or (os.order_type = 1 and pos_group like '%112%'))
        and os.id_order not in (select id_order from orders_repair)
        and os.id_order not in (select id_order from orders_protez)
group by os.id_order);
create temporary table if not exists cement_docs
(select id_doctor, id_tech, sum(service_cost * discount) as total from orders_services
where id_order in (select id_order from orders_cement) and in_tech >= tech
group by case when tech = 0 then id_doctor else id_tech end);

/* IMPLANT */
create temporary table if not exists orders_implant
(select id_order from orders_services os
where (os.order_type <> 1 and p_group like '2;%')
        and os.id_order not in (select id_order from orders_repair)
        and os.id_order not in (select id_order from orders_protez)
        and os.id_order not in (select id_order from orders_cement)
group by os.id_order);
create temporary table if not exists implant_docs
(select id_doctor, id_tech, sum(service_cost * discount) as total from orders_services
where id_order in (select id_order from orders_implant) and in_tech >= tech
group by case when tech = 0 then id_doctor else id_tech end);

/* ANEST */
create temporary table if not exists orders_anest
select id_order from orders_services os
where ((os.order_type <> 1 and p_code in ('51010', '51011','7079', '7080')) or (os.order_type = 1 and pos_code = '1.6'))
        and os.id_order not in (select id_order from orders_repair)
        and os.id_order not in (select id_order from orders_protez)
        and os.id_order not in (select id_order from orders_cement)
        and os.id_order not in (select id_order from orders_implant)
group by os.id_order;
create temporary table if not exists anest_docs
(select id_doctor, id_tech, sum(service_cost * discount) as total from orders_services
where id_order in (select id_order from orders_anest) and in_tech >= tech
group by case when tech = 0 then id_doctor else id_tech end);

/* OSMOTR */
create temporary table if not exists orders_osmotr
(select id_order from orders_services os
where ((os.order_type <> 1 and p_code in ('51001', '51002', '7077', '7078', '7217')) or (os.order_type = 1 and pos_code in ('1.1','1.4')))
        and os.id_order not in (select id_order from orders_repair)
        and os.id_order not in (select id_order from orders_protez)
        and os.id_order not in (select id_order from orders_cement)
        and os.id_order not in (select id_order from orders_implant)
        and os.id_order not in (select id_order from orders_anest)
group by os.id_order);
create temporary table if not exists osmotr_docs
(select id_doctor, id_tech, sum(service_cost * discount) as total from orders_services
where id_order in (select id_order from orders_osmotr) and in_tech >= tech
group by case when tech = 0 then id_doctor else id_tech end);

if tech = 0 then
    select 	u.name as doc_name, coalesce(T1.total,0), coalesce(T2.total,0), coalesce(T3.total,0),
        coalesce(T4.total,0), coalesce(T5.total,0), coalesce(T6.total,0),
        coalesce(T1.total,0) + coalesce(T2.total,0) + coalesce(T3.total,0) + coalesce(T4.total,0) + coalesce(T5.total,0) + coalesce(T6.total,0)
    from doctor_spec ds
    left join users u on u.id = ds.user_id
    left join protez_docs T1 on T1.id_doctor = ds.doctor_id
    left join cement_docs T2 on T2.id_doctor = ds.doctor_id
    left join repair_docs T3 on T3.id_doctor = ds.doctor_id
    left join osmotr_docs T4 on T4.id_doctor = ds.doctor_id
    left join anest_docs T5 on T5.id_doctor = ds.doctor_id
    left join implant_docs T6 on T6.id_doctor = ds.doctor_id
    where ds.spec_id = 107 and  ds.user_id <> 1 and coalesce(T1.total,0) + coalesce(T2.total,0) + coalesce(T3.total,0) + coalesce(T4.total,0) + coalesce(T5.total,0) + coalesce(T6.total,0) > 0
    order by u.name ;
else
    select 	t.name as tech_name, coalesce(T1.total,0), coalesce(T2.total,0), coalesce(T3.total,0),
        coalesce(T4.total,0), coalesce(T5.total,0), coalesce(T6.total,0),
        coalesce(T1.total,0) + coalesce(T2.total,0) + coalesce(T3.total,0) + coalesce(T4.total,0) + coalesce(T5.total,0) + coalesce(T6.total,0)
    from technicians t
    left join protez_docs T1 on T1.id_tech = t.id
    left join cement_docs T2 on T2.id_tech = t.id
    left join repair_docs T3 on T3.id_tech = t.id
    left join osmotr_docs T4 on T4.id_tech = t.id
    left join anest_docs T5 on T5.id_tech = t.id
    left join implant_docs T6 on T6.id_tech = t.id
    where coalesce(T1.total,0) + coalesce(T2.total,0) + coalesce(T3.total,0) + coalesce(T4.total,0) + coalesce(T5.total,0) + coalesce(T6.total,0) > 0
    order by t.name ;
end if;

END