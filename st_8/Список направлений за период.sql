set @date_start = '2019-01-01';
set @date_end = '2019-07-10';
drop temporary table if  exists new_bzp_humans;
 create temporary table if not exists new_bzp_humans
 select T.id_human, T.min_date, T.id from
 (select min(o.orderCreateDate) as min_date, o.id_human, o.id from orders o
 where o.order_type = 1
group by o.id_human, o.bzp_num, o.bzp_date ) T
 where T.min_date between @date_start and @date_end;


 set @row = 0;

 select  @row :=  @row + 1, T.* from (
 select concat('Направление № ', o.bzp_num, ' от ', date_format(o.bzp_date, '%d-%m-%Y')), date_format(nbh.min_date, '%d-%m-%Y'), concat_ws(' ', pd.SURNAME, pd.NAME, pd.SECOND_NAME) as pat,
        pa.UNSTRUCT_ADDRESS, pd.REMARK
 from orders o
 left join patient_data pd on pd.ID_HUMAN = o.Id_Human and pd.is_active = 1 and pd.date_end = '2200-01-01'
 left join patient_address pa on pd.ID_ADDRESS_REG = pa.ID_ADDRESS
 left join new_bzp_humans nbh on nbh.id_human = o.id_human and o.id = nbh.id
 where nbh.id_human is not null 
 and o.order_type = 1 group by o.id_human order by pat) T