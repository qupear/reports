select T1.dept_name, T1.total_case, T1.total_uet, T2.total_check  
 from  
     (select d.name as dept_name, sum(cs.service_cost) as total_case, sum(p.uet) as total_uet, d.id as dept_id          
     from case_services cs 
     left join doctor_spec ds on ds.doctor_id = cs.id_doctor 
     left join departments d on d.id = ds.department_id 
     left join price p on p.id = cs.id_profile  
     where cs.date_begin between @date_start and @date_end and cs.is_oms = 0 and  
 		(cs.id_order = -1 or cs.id_order in (select id from orders where order_type <> 1))  
     group by d.id  
     order by dept_name ) T1  
 left join 
     (select sum(p.summ) as total_check, d.id as dept_id from prepayments p, checks ch, doctor_spec ds, departments d 
     where ch.id = p.check and ch.date between @date_start and @date_end and  
 		(p.order_id = -1 or p.order_id in (select id from orders where order_type <> 1))  
         and ds.doctor_id = p.doctor_id and d.id = ds.department_id  
     group by d.id) T2 on T1.dept_id = T2.dept_id 
 