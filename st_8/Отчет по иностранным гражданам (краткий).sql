set @date_start = '2020-01-01';
set @date_end = '2020-03-01';
set @fin_type = 0;

select vo.COUNTRY_SHORT_NAME,
	count(distinct pd.id_human),
	count(distinct c.id_case),
       sum(cs.SERVICE_COST) as summ
       from case_services cs
left join cases c on cs.ID_CASE = c.ID_CASE
left join patient_data pd on c.ID_PATIENT = pd.ID_PATIENT
left join vmu_oksm vo on pd.C_OKSM = vo.CODE
where cs.DATE_BEGIN between @date_start and @date_end and c.IS_CLOSED = 1
and pd.C_OKSM <> 643 and pd.IS_ACTIVE = 1 and pd.DATE_END = '2200-01-01'
and ((cs.IS_OMS = 0 and not @fin_type)  or (cs.IS_OMS = 1 and  @fin_type ))
group by vo.ID_OKSM
order by vo.COUNTRY_SHORT_NAME