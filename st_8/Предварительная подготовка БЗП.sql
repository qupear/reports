set @row = 0;
 select @row:=@row+1, T.* from(
 select concat_ws(' ', pd.SURNAME, pd.name, pd.SECOND_NAME) as pat, o.bzp_direction_text, date_format(o.ordercreatedate, '%d-%m-%Y'), o.Order_Number
 from  case_services cs
 left join orders o on cs.ID_ORDER = o.ID
 left join price_orto_soc pos on cs.ID_PROFILE = pos.id
 left join patient_data pd on pd.id_human = o.id_human
 left join (select count(*) as counter, id_order from case_services
 where id_order > 0
 group by id_order) C on cs.id_order = C.id_order
 where year(o.orderCreateDate) = @year
 and (pos.CODE = '1.1' or pos.code = '1.4')
 and o.order_type = 1
 and C.counter =1
 group by pos.code, order_number
 order by pat)T