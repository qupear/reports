create temporary table if not exists doctor_services (select cs.date_begin, cs.id_case, vp.profile_infis_code, p.code,
                                                             vd.diagnosis_code, cs.tooth_name, T.uet, cs.is_oms, case
                                                                 when YEAR(CS.DATE_BEGIN) - YEAR(pd.BIRTHDAY)
    - (DATE_FORMAT(CS.DATE_BEGIN, '%m%d') < DATE_FORMAT(pd.BIRTHDAY, '%m%d'))  >= 60 and pd.SEX = 'м'
                                                                     then 0
                                                                 WHEN YEAR(CS.DATE_BEGIN) - YEAR(pd.BIRTHDAY)
    - (DATE_FORMAT(CS.DATE_BEGIN, '%m%d') < DATE_FORMAT(pd.BIRTHDAY, '%m%d'))  >= 55 and pd.SEX = 'ж'
                                                                     then 1 else -1 end as old_people,
                                                          pd.ID_PATIENT
                                                      from case_services cs
                                                               left join cases c on cs.id_case = c.id_case
                                                               left join patient_data pd on c.ID_PATIENT = pd.id_patient
															   left join doctor_spec ds on ds.doctor_id = cs.id_doctor
                                                               left join vmu_profile vp on vp.id_profile = cs.id_profile and cs.is_oms = 1
                                                               left join price p on p.id = cs.id_profile and cs.is_oms in (0, 3)
                                                               left join vmu_diagnosis vd on cs.id_diagnosis = vd.id_diagnosis
                                                               left join (select vt.uet, vt.id_profile, vt.id_zone_type,
                                                                                 vt.tariff_begin_date as d1,
                                                                                 vt.tariff_end_date as d2,
                                                                                 vtp.tp_begin_date as d3,
                                                                                 vtp.tp_end_date as d4
                                                                          from vmu_tariff vt,
                                                                               vmu_tariff_plan vtp
                                                                          where vtp.id_tariff_group = vt.id_lpu
                                                                            And vtp.id_lpu in (select id_lpu from mu_ident)) T
                                                                         on T.id_profile = vp.id_profile and
                                                                            cs.date_begin between T.d1 and T.d2 and
                                                                            cs.date_begin between T.d3 and T.d4 and
                                                                            T.id_zone_type = cs.id_net_profile and
                                                                            cs.is_oms = 1
                                                      where (cs.id_doctor = @doc_id or @doc_id = 0 )
                                                        and cs.date_begin between @date_start and @date_end);



set @row = 0;
create temporary table if not exists first_row (select @row := @row + 1 as st_num, T.date_begin as st_date
                                                from (select date_begin
                                                      from doctor_services ds
where ds.is_oms = 1
                                                      group by date_begin
                                                      order by date_begin) T);
/*приянто больных*/
create temporary table if not exists T1 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where profile_infis_code in ('стт005', 'стт006')
                                         group by date_begin
                                         order by date_begin);
/*из них первичных*/
create temporary table if not exists T2 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where profile_infis_code in ('стт005')
                                         group by date_begin
                                         order by date_begin);
/*пенс*/
create temporary table if not exists T3 (select date_begin, count(distinct ID_PATIENT) as val
                                          from doctor_services
where (old_people = 0 or old_people = 1) and is_oms = 1
                                          group by date_begin
                                          order by date_begin);
/*заболевания пародонта*/
Create temporary table if not exists T4
    (select date_begin, count(distinct id_case) as val
     from doctor_services
     where diagnosis_code in ('K05.3', 'K05.31', 'K05.1', 'K05.4')
     group by date_begin
     order by date_begin);
/*слизистая*/
Create temporary table if not exists T5
    (select date_begin, count(distinct id_case) as val
     from doctor_services
     where diagnosis_code in ('K12.1', 'K12.2', 'K13.1', 'K13.2', 'K13.5', 'K13.6', 'K13.7', 'L43.0')
     group by date_begin
     order by date_begin);
/*help*/
create temporary table if not exists T34_1
(select id from protocols p
where child_ref_ids like '%55161(%' or child_ref_ids like '%71682(%'
or child_ref_ids like '%102782(%' or child_ref_ids like '%78522(%'
or child_ref_ids like '%127890(%' or child_ref_ids like '%127900(%'
or child_ref_ids like '%127910(%' or child_ref_ids like '%125521(%'
or child_ref_ids like '%127920(%' or child_ref_ids like '%127930(%'
or child_ref_ids like '%127940(%' or child_ref_ids like '%20130(%'
or child_ref_ids like '%127960(%' or child_ref_ids like '%127950(%'
or child_ref_ids like '%127970(%' or child_ref_ids like '%127980(%'
or child_ref_ids like '%127990(%');


/*завершено*/
create temporary table if not exists T6 (select count(distinct ds.id_case)  as val, date_begin
                                          from doctor_services ds
left join protocols p on ds.id_case = p.id_case
where ds.is_oms = 1 and p.id in (select id from t34_1)
 and is_oms = 1
                                          group by date_begin
                                          order by date_begin);



/*YET*/
create temporary table if not exists T7
    (select date_begin, ROUND(COALESCE(SUM(UET), 0), 2) as val
     from doctor_services
     group by date_begin
     order by date_begin);



select fr.st_num, fr.st_date, coalesce(T1.val, 0), coalesce(T2.val, 0), coalesce(T3.val, 0),
       coalesce(T4.val, 0), coalesce(T5.val, 0), coalesce(T6.val, 0), coalesce(T7.val, 0)
from first_row fr
         left join T1 on T1.date_begin = fr.st_date
         left join T2 on T2.date_begin = fr.st_date
         left join T3 on T3.date_begin = fr.st_date
         left join T4 on T4.date_begin = fr.st_date
         left join T5 on T5.date_begin = fr.st_date
         left join T6 on T6.date_begin = fr.st_date
         left join T7 on T7.date_begin = fr.st_date
order by st_num;