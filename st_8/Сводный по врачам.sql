select TM.dept_name, TM.doc_name, coalesce(T1.total_case, 0) as t1, coalesce(T1.total_uet, 0) as u1,
       coalesce(T2.total_check, 0) as t2
from (select d.name as dept_name, concat(u.name, ' (', coalesce(s.name, ''), ')') as doc_name, ds.doctor_id as doc_id
      from doctor_spec ds
               left join users u on ds.user_id = u.id
               left join departments d on d.id = ds.department_id
               left join speciality s on s.id = ds.spec_id
      where (d.id = @depart_id or @depart_id = 0)
        and ds.active = 1
        and (u.id is not null)
        and (d.id is not null)
      order by dept_name, doc_name, doc_id) TM
         left join (select sum(cs.service_cost) as total_case, sum(p.uet) as total_uet, cs.id_doctor as doc_id
                    from case_services cs
                             left join price p on p.id = cs.id_profile
                    where cs.date_begin between @date_start and @date_end
                      and cs.is_oms = 0
                      and (cs.id_order = -1 or cs.id_order in (select id from orders where order_type <> 1))
                    group by cs.id_doctor) T1 on T1.doc_id = TM.doc_id
         left join (select sum(p.summ) as total_check, d.id as dept_id, ds.doctor_id as doc_id
                    from prepayments p,
                         checks ch,
                         doctor_spec ds,
                         departments d
                    where ch.id = p.check
                      and ch.date between @date_start and @date_end
                      and (p.order_id = -1 or p.order_id in (select id from orders where order_type <> 1))
                      and ds.doctor_id = p.doctor_id
                      and d.id = ds.department_id
                    group by ds.doctor_id) T2 on TM.doc_id = T2.doc_id
where coalesce(T1.total_case, 0) + coalesce(T2.total_check, 0) > 0;