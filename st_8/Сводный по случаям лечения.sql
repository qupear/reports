select T1.dept_name, T1.doc_name, T1.case_date, T1.case_num, T1.card_number, T1.patient_name, T1.total_case,
       T1.total_uet, T2.total_check
from (select d.name as dept_name, u.name as doc_name,
             (case cs.id_order when -1 then c.date_begin else ord.orderCreateDate end) as case_date,
             (case cs.id_order when -1 then cs.id_case else ord.order_number end) as case_num, pd.card_number,
             concat(pd.surname, ' ', LEFT(pd.name, 1), '.', LEFT(pd.second_name, 1), '.') as patient_name,
             sum(cs.service_cost) as total_case, sum(p.uet) as total_uet, ds.doctor_id as doc_id, cs.id_order
      from case_services cs
               left join cases c on cs.id_case = c.id_case
               left join orders ord on cs.id_order = ord.id
               left join patient_data pd on pd.id_patient = c.id_patient
               left join doctor_spec ds on ds.doctor_id = cs.id_doctor
               left join users u on ds.user_id = u.id
               left join departments d on d.id = ds.department_id
               left join price p on p.id = cs.id_profile
      where ((cs.id_order > 0 and ord.orderCreateDate between @date_start and @date_end) or
             (cs.id_order = -1 and c.date_begin between @date_start and @date_end))
        and cs.is_oms = 0
        and (cs.id_order = -1 or ord.order_type <> 1)
        and (d.id = @depart_id or @depart_id = 0)
        and (ds.doctor_id = @doc_id or @doc_id = 0)
      group by case_num, cs.id_doctor
      order by dept_name, doc_name, doc_id, case_num) T1
         left join (select sum(p.summ) as total_check, d.id as dept_id, ds.doctor_id as doc_id,
                           (case p.order_id when -1 then p.case_id else ord.order_number end) as case_num
                    from (prepayments p, checks ch, doctor_spec ds, departments d)
                             left join orders ord on ord.id = p.order_id
                    where ch.id = p.check
                      and ch.date between @date_start and @date_end
                      and (p.order_id = -1 or ord.order_type <> 1)
                      and ds.doctor_id = p.doctor_id
                      and d.id = ds.department_id
                    group by case_num, ds.doctor_id) T2 on T1.case_num = T2.case_num and T1.doc_id = T2.doc_id
UNION
select d.name as dept_name, u.name as doc_name,
       (case T2.case_id when -1 then ord.orderCreateDate else c.date_begin end) as case_date, T2.case_num,
       (case T2.case_id when -1 then pd1.card_number else pd.card_number end) as card_number, (case T2.case_id
                                                                                                   when -1 then concat(
                                                                                                           pd1.surname,
                                                                                                           ' ',
                                                                                                           LEFT(pd1.name, 1),
                                                                                                           '.',
                                                                                                           LEFT(pd1.second_name, 1),
                                                                                                           '.')
                                                                                                   else concat(
                                                                                                           pd.surname,
                                                                                                           ' ',
                                                                                                           LEFT(pd.name, 1),
                                                                                                           '.',
                                                                                                           LEFT(pd.second_name, 1),
                                                                                                           '.') end) as patient_name,
       0, 0, T2.total_check
from (select sum(p.summ) as total_check, d.id as dept_id, ds.doctor_id as doc_id,
             (case p.order_id when -1 then p.case_id else ord.order_number end) as case_num, p.case_id, p.order_id
      from (prepayments p, checks ch, doctor_spec ds, departments d)
               left join orders ord on ord.id = p.order_id
      where ch.id = p.check
        and ch.date between @date_start and @date_end
        and p.summ <> 0
        and (p.order_id = -1 or ord.order_type <> 1)
        and ds.doctor_id = p.doctor_id
        and d.id = ds.department_id
      group by case_num, ds.doctor_id) T2
         left join doctor_spec ds on ds.doctor_id = T2.doc_id
         left join users u on u.id = ds.user_id
         left join departments d on d.id = ds.department_id
         left join cases c on c.id_case = T2.case_id
         left join orders ord on ord.id = T2.order_id
         left join patient_data pd on pd.id_patient = c.id_patient
         left join patient_data pd1 on pd1.id_human = ord.id_human and pd1.date_end = '2200-01-01' and pd1.is_active = 1
where ((T2.order_id > 0 and ord.orderCreateDate not between @date_start and @date_end) or
       (T2.order_id = -1 and c.date_begin not between @date_start and @date_end))
  and (d.id = @depart_id or @depart_id = 0)
  and (ds.doctor_id = @doc_id or @doc_id = 0);