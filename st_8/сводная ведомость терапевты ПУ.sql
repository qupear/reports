create temporary table if not exists doctor_services (select cs.date_begin, cs.id_case, p.code, vd.diagnosis_code,
                                                             cs.tooth_name, p.uet, cs.is_oms, cs.id_service
                                                      from case_services cs
                                                               left join cases c on cs.id_case = c.id_case
                                                               left join patient_data pd on c.ID_PATIENT = pd.id_patient
                                                               left join price p on p.id = cs.id_profile and cs.is_oms in (0, 3, 4)
                                                               left join vmu_diagnosis vd on cs.id_diagnosis = vd.id_diagnosis
															   left join doctor_spec ds on ds.doctor_id = cs.id_doctor
                                                      where (cs.id_doctor = @doc_id or @doc_id = 0)
														and (ds.department_id = @depart_id or @depart_id = 0)
                                                        and cs.date_begin between @date_start and @date_end
                                                        and cs.is_oms in (0, 3, 4));

create temporary table if not exists first_row 
(select  T.date_begin as st_date
from (select date_begin
                                                      from doctor_services
                                                      group by date_begin
                                                      order by date_begin) T);
/*принято всего*/
create temporary table if not exists T1 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
										 where code in ('1001', '1002')
                                         group by date_begin
                                         order by date_begin);
/*первичные*/
create temporary table if not exists T2 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where code in ('1001')
                                         group by date_begin
                                         order by date_begin);
/*запломбировано всего*/
create temporary table if not exists T3 (select date_begin, count(id_case) as val
                                               from doctor_services
                                               where (code in
                                                     ('1266', '1267', '1268', '1269', '1270', '1271', '1501', '1503', '1506', '1248', '1249', '1250', '1285', '1286')
												and diagnosis_code in ('K02.1', 'K02.2', 'K02.8')) or (code in ('1266', '1267', '1268', '1269', '1270', '1271', '1501', '1503', '1506', '1248', '1249', '1250', '1285', '1286')
											and diagnosis_code in ('K04.0', 'K04.01', 'K04.03')) or (code in ('1266', '1267', '1268', '1269', '1270', '1271', '1501', '1503', '1506', '1248', '1249', '1250', '1285', '1286')
											and diagnosis_code in ('K04.4', 'K04.5', 'K04.6', 'K04.7'))
                                         group by date_begin
										order by date_begin);

/*Kариес*/
create temporary table if not exists T4 (select date_begin, count(id_case) as val
                                               from doctor_services
                                               where code in
                                                     ('1266', '1267', '1268', '1269', '1270', '1271', '1501', '1503', '1506', '1248', '1249', '1250', '1285', '1286')
												and diagnosis_code in ('K02.1', 'K02.2', 'K02.8')
                                         group by date_begin
										order by date_begin);
/*пульпит*/
create temporary table if not exists T5 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where code in ('1266', '1267', '1268', '1269', '1270', '1271', '1501', '1503', '1506', '1248', '1249', '1250', '1285', '1286')
											and diagnosis_code in ('K04.0', 'K04.01', 'K04.03')
                                         group by date_begin
                                         order by date_begin);
/*периодонтит*/
create temporary table if not exists T6 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where code in ('1266', '1267', '1268', '1269', '1270', '1271', '1501', '1503', '1506', '1248', '1249', '1250', '1285', '1286')
											and diagnosis_code in ('K04.4', 'K04.5', 'K04.6', 'K04.7')
                                         group by date_begin
                                         order by date_begin);

/*девит*/
create temporary table if not exists T7 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where code in ('1291')
                                         group by date_begin
                                         order by date_begin);
/*Запломбировано постоянным материалом корневых каналов */
create temporary table if not exists T8_1 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where code in ('1295', '1296')
											and diagnosis_code in ('K04.0', 'K04.01', 'K04.03', 'K04.4', 'K04.5', 'K04.6', 'K04.7') 
                                         group by date_begin
                                         order by date_begin);

/*Запломбировано временным пломбировочным материалом корневых каналов*/
create temporary table if not exists T8_2 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where code in ('1300')
											and diagnosis_code in ('K04.0', 'K04.01', 'K04.03', 'K04.4', 'K04.5', 'K04.6', 'K04.7') 
                                         group by date_begin
                                         order by date_begin);

/*анастезия*/
create temporary table if not exists T8_3 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where code in ('1211')
                                         group by date_begin
                                         order by date_begin);
/*ниже следует форменное помешательство*/
create temporary table if not exists T9_1 (select date_begin, count(distinct id_case) as val, code
                                         from doctor_services
                                         where code in ('1266', '1267', '1268', '1269', '1270', '1271', '1501', '1503', '1506', '1248', '1249', '1250', '1285', '1286')
											and diagnosis_code in ('K04.0', 'K04.01', 'K04.03')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T9_2 (select ds.date_begin,ds.id_case as val, ds.code
                                         from doctor_services ds
										 inner join T9_1 on T9_1.date_begin = ds.date_begin
                                         where ds.code in ('1304')
                                         group by ds.date_begin
                                         order by ds.date_begin);
create temporary table if not exists T9_3 (select ds.date_begin,ds.id_case as val, ds.code
                                         from doctor_services ds
										 inner join T9_2 on T9_2.date_begin = ds.date_begin
                                         where ds.code in ('1295')
                                         group by ds.date_begin
                                         order by ds.date_begin);

create temporary table if not exists T9_4 (select ds.date_begin,ds.id_case as val, ds.code
                                         from doctor_services ds
										 inner join T9_3 on T9_3.date_begin = ds.date_begin
                                         where ds.code in ('1305')
                                         group by ds.date_begin
                                         order by ds.date_begin);

create temporary table if not exists T9_5 (select date_begin,id_case as val, code
                                         from doctor_services
                                         where code in ('1266', '1267', '1268', '1269', '1270', '1271', '1501', '1503', '1506', '1248', '1249', '1250', '1285', '1286')
											and diagnosis_code in ('К04.4', 'К04.5', 'К04.6', 'К04.7')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T9_6 (select ds.date_begin,ds.id_case as val, ds.code
                                         from doctor_services ds
										 inner join T9_5 on T9_5.date_begin = ds.date_begin
                                         where ds.code in ('1314')
                                         group by ds.date_begin
                                         order by ds.date_begin);
create temporary table if not exists T9_7 (select ds.date_begin,ds.id_case as val, ds.code
                                         from doctor_services ds
										 inner join T9_6 on T9_6.date_begin = ds.date_begin
                                         where ds.code in ('1295')
                                         group by ds.date_begin
                                         order by ds.date_begin);
create temporary table if not exists T9_8 (select ds.date_begin,ds.id_case as val, ds.code
                                         from doctor_services ds
										 inner join T9_6 on T9_6.date_begin = ds.date_begin
                                         where ds.code in ('1296')
                                         group by ds.date_begin
                                         order by ds.date_begin);
create temporary table if not exists T9_9 (select ds.date_begin,ds.id_case as val, ds.code
                                         from doctor_services ds
										 inner join T9_3 on T9_3.date_begin = ds.date_begin
                                         where ds.code in ('1308')
                                         group by ds.date_begin
                                         order by ds.date_begin);
create temporary table if not exists T9_10 (select ds.date_begin,ds.id_case as val, ds.code
                                         from doctor_services ds
										 inner join T9_3 on T9_3.date_begin = ds.date_begin
                                         where ds.code in ('1343')
                                         group by ds.date_begin
                                         order by ds.date_begin);
create temporary table if not exists T9_11 (select ds.date_begin,ds.id_case as val, ds.code
                                         from doctor_services ds
										 inner join T9_2 on T9_2.date_begin = ds.date_begin
                                         where ds.code in ('1296')
                                         group by ds.date_begin
                                         order by ds.date_begin);
create temporary table if not exists T9_12 (select ds.date_begin,ds.id_case as val, ds.code
                                         from doctor_services ds
										 inner join T9_11 on T9_11.date_begin = ds.date_begin
                                         where ds.code in ('1305')
                                         group by ds.date_begin
                                         order by ds.date_begin);
create temporary table if not exists T9_13 (select ds.date_begin,ds.id_case as val, ds.code
                                         from doctor_services ds
										 inner join T9_11 on T9_11.date_begin = ds.date_begin
                                         where ds.code in ('1307')
                                         group by ds.date_begin
                                         order by ds.date_begin);
create temporary table if not exists T9_14 (select ds.date_begin,ds.id_case as val, ds.code
                                         from doctor_services ds
										 inner join T9_11 on T9_11.date_begin = ds.date_begin
                                         where ds.code in ('1343')
                                         group by ds.date_begin
                                         order by ds.date_begin);
/*вылучено зубов одно посещение*/
create temporary table if not exists T9 (select ds.date_begin,count(ds.id_case) as val
                                         from doctor_services ds
										where (ds.id_case in (select val from T9_4)) or (ds.id_case in (select val from T9_7)) or (ds.id_case in (select val from T9_8)) or 
											(ds.id_case in (select val from T9_9)) or (ds.id_case in (select val from T9_10)) or (ds.id_case in (select val from T9_12)) or 
											(ds.id_case in (select val from T9_13)) or (ds.id_case in (select val from T9_14))  
                                         group by ds.date_begin
                                         order by ds.date_begin);


/*пломб всего*/
create temporary table if not exists T10 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where code in ('1234', '1235', '1248', '1249', '1250', '1285', '1286', '1266', '1267', '1268', '1269', '1270', '1271', '1501', '1503', '1506')
                                         group by date_begin
                                         order by date_begin);
/*временные*/
create temporary table if not exists T11 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('1234', '1235')
                                          group by date_begin
                                          order by date_begin);
/*цемент*/
create temporary table if not exists T12 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('1248', '1249', '1250', '1285', '1286')
                                          group by date_begin
                                          order by date_begin);
/*Kомпозит*/
create temporary table if not exists T13 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('1266', '1267', '1268', '1269', '1270', '1271', '1501', '1503', '1506')
                                          group by date_begin
                                          order by date_begin);
/*снято отложений*/
create temporary table if not exists T14 (select date_begin, count(distinct id_case) as val
                                          from doctor_services
                                          where code in ('2106' '2107', '1209', '1101')
											and diagnosis_code in ('K03.6')
                                          group by date_begin
                                          order by date_begin);
/*протоKолы санировано*/
create temporary table if not exists t15_1
(select id from protocols p
where (child_ref_ids like '%12660(%' or child_ref_ids like '%13340(%'
or child_ref_ids like '%14010(%' or child_ref_ids like '%34371(%'
or child_ref_ids like '%34421(%' or child_ref_ids like '%46162(%'
or child_ref_ids like '%48242(%' or child_ref_ids like '%49532(%'
or child_ref_ids like '%52282(%' or child_ref_ids like '%52852(%'
or child_ref_ids like '%65862(%' or child_ref_ids like '%66962(%'
or child_ref_ids like '%67902(%' or child_ref_ids like '%69102(%'
or child_ref_ids like '%70182(%' or child_ref_ids like '%71352(%'
or child_ref_ids like '%107182(%' or child_ref_ids like '%110271(%'
or child_ref_ids like '%111251(%' or child_ref_ids like '%111861(%'
or child_ref_ids like '%112461(%' or child_ref_ids like '%113141(%'
or child_ref_ids like '%113791(%' or child_ref_ids like '%118051(%'
or child_ref_ids like '%118451(%' or child_ref_ids like '%128210(%')
and date_protocol between @date_start and @date_end);
/*санирвоано*/
create temporary table if not exists T15 (select date_begin, count(distinct ds.id_case) as val
                                          from doctor_services ds
										  left join protocols p on ds.id_case = p.id_case
                                          where p.id in (select id from t15_1) and code in ('1001', '1002')
                                          group by date_begin
                                          order by date_begin);

create temporary table if not exists T16 (select date_begin, ROUND(COALESCE(SUM(UET), 0), 2) as val
                                          from doctor_services
                                          group by date_begin
                                          order by date_begin);



select fr.st_date, 
	coalesce(T1.val, 0), coalesce(T2.val, 0), coalesce(T3.val, 0),
    coalesce(T4.val, 0), coalesce(T5.val, 0), coalesce(T6.val, 0),
	coalesce(T7.val, 0), coalesce(T8_1.val, 0), coalesce(T8_2.val, 0),  coalesce(T8_3.val, 0),
	coalesce(T9.val, 0), coalesce(T10.val, 0), coalesce(T11.val, 0), coalesce(T12.val, 0),
	coalesce(T13.val, 0), coalesce(T14.val, 0), coalesce(T15.val, 0), coalesce(T16.val, 0)
from first_row fr
         left join T1 on T1.date_begin = fr.st_date
         left join T2 on T2.date_begin = fr.st_date
         left join T3 on T3.date_begin = fr.st_date
         left join T4 on T4.date_begin = fr.st_date
         left join T5 on T5.date_begin = fr.st_date
         left join T6 on T6.date_begin = fr.st_date
         left join T7 on T7.date_begin = fr.st_date
         left join T8_1 on T8_1.date_begin = fr.st_date
         left join T8_2 on T8_2.date_begin = fr.st_date
         left join T8_3 on T8_3.date_begin = fr.st_date
         left join T9 on T9.date_begin = fr.st_date
         left join T10 on T10.date_begin = fr.st_date
         left join T11 on T11.date_begin = fr.st_date
         left join T12 on T12.date_begin = fr.st_date
         left join T13 on T13.date_begin = fr.st_date
         left join T14 on T14.date_begin = fr.st_date
         left join T15 on T15.date_begin = fr.st_date
		 left join T16 on T16.date_begin = fr.st_date

order by fr.st_date ;
