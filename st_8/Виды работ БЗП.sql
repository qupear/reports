create temporary table if not exists orders_services 
 (select case ds.active when 1 then '' when 0 then 'Уволенные'end active,
 cs.id_order, ord.order_number, ord.id_human, ord.id_doctor, u.name, cs.service_cost,
       cs.id_profile ,   pos.code as pos_code, pos.id_group as pos_group, ord.status, c.counter 
 from case_services cs  
 left join orders ord on ord.id = cs.id_order  
 left join price_orto_soc pos on pos.id = cs.id_profile and ord.order_type = 1  
 left join doctor_spec ds on ds.doctor_id = ord.id_doctor 
 left join users u on ds.user_id = u.id  
 left join 
 (select count(*) as counter, id_order 
 from case_services   where id_order > 0   group by id_order) C 
 on cs.id_order = C.id_order 
 where cs.id_order > 0 and pos.code is not null and ord.order_type = 1 
         and ds.spec_id = 107   		
 and ((ord.orderCompletionDate BETWEEN CONCAT(YEAR(CURDATE()),'-01-01') and @date_start and ord.status = 2 ) or 
                 (ord.orderCreateDate <= @date_start and ord.status < 2 ))  		) ;
 create temporary table if not exists doc
 select max(ord.id) as id, ord.id_human
 from orders ord   
 where  ord.order_type = 1 
 and ((ord.orderCompletionDate BETWEEN CONCAT(YEAR(CURDATE()),'-01-01') and @date_start and ord.status = 2 ) or 
                 (ord.orderCreateDate <= @date_start and ord.status < 2 )) 
 group by ord.Id_Human;
 
 
 
    create temporary table if not exists col 
 (select id_order from orders_services where pos_group like '%111%' group by id_order ); 
 create temporary table if not exists col_2 
 (select id_order, sum(service_cost) summ, count(distinct id_human) id_human, id_doctor 
 from orders_services 
 where status < 2  and id_order in (select id_order from col)  and counter > 1 group by id_doctor ); 
  create temporary table if not exists col_22
 (select id_doctor, count(distinct os.id_human) id_human from orders_services os
  join doc on os.id_order = doc.id 
 where status < 2  and id_order in (select id_order from col)  and counter > 1
 group by os.id_doctor) ;
 create temporary table if not exists col_11
 (select id_doctor, count(distinct os.id_human) id_human from orders_services os
  join doc on os.id_order = doc.id 
 where status < 2  and id_order not in (select id_order from col)  and counter > 1
 group by os.id_doctor );
 create temporary table if not exists col_1 
 (select sum(service_cost) summ, id_doctor 
 from orders_services 
 where status < 2  and id_order not in (select id_order from col)  and counter > 1 group by id_doctor ); 
 create temporary table if not exists col_3 
 (select count(distinct os.id_human) id_h, id_doctor 
 from orders_services os
 where status < 2 and os.id_human and counter = 1 group by id_doctor  ); 
 create temporary table if not exists col_5 
 (select sum(service_cost) summ, count(distinct id_human)  id_human, id_doctor 
 from orders_services 
 where status = 2 and id_order  in (select id_order from col) group by id_doctor ); 
 create temporary table if not exists col_44 
 (select count(distinct os.id_human)  id_human, id_doctor 
 from orders_services os
  join doc on os.id_order = doc.id
 where status = 2 and id_order not in (select id_order from col) group by id_doctor ); 
 
 create temporary table if not exists col_4 
 (select sum(service_cost) summ,count(distinct id_human)  id_human, id_doctor 
 from orders_services 
 where status = 2 and id_order not in (select id_order from col) and counter > 1 group by id_doctor  );
 select 
     active,
     name,
     coalesce(c1.summ, 0),
     coalesce(c11.id_human, 0),
     coalesce(c2.summ, 0),
     coalesce(c2.id_human, 0),
     coalesce(c3.id_h, 0),
     coalesce(c4.summ, 0),
     coalesce(c44.id_human, 0),
     coalesce(c5.summ, 0),
     coalesce(c5.id_human, 0),
     coalesce(c1.summ, 0) + coalesce(c2.summ, 0) + coalesce(c4.summ, 0) + coalesce(c5.summ, 0),
     coalesce(c11.id_human, 0) + coalesce(c2.id_human, 0) + coalesce(c3.id_h, 0) + coalesce(c4.id_human, 0) + coalesce(c5.id_human, 0)
 from
     orders_services os
         left join
     col_1 c1 ON os.id_doctor = c1.id_doctor
 left join
     col_11 c11 ON os.id_doctor = c11.id_doctor
         left join
     col_2 c2 ON os.id_doctor = c2.id_doctor
         left join
     col_3 c3 ON os.id_doctor = c3.id_doctor
         left join
     col_4 c4 ON os.id_doctor = c4.id_doctor
         left join
     col_44 c44 ON os.id_doctor = c44.id_doctor
         left join
     col_5 c5 ON os.id_doctor = c5.id_doctor
 group by active , os.id_doctor
 order by active , name 