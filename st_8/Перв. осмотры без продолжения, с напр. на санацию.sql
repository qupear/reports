set @row_num = 0; 
create temporary table if not exists RG
  (select id_human as id_h, id_ref, date_protocol as d1
                         from protocols prot
                         where prot.id_ref in (9470, 8459, 8464)
 and date_protocol >= @date_start
                         group by id_human, id_ref);
  create temporary table if not exists ALLER
  (select id_human as id_h, id_ref, date_protocol as d2
                         from protocols prot
                         where prot.id_ref in (9491, 8480)
                        and date_protocol >= @date_start
  group by id_human, id_ref);
  create temporary table if not exists TER
  (select id_human as id_h, id_ref, date_protocol as d3
                         from protocols prot
                         where prot.id_ref in (9492, 8461)
                        and date_protocol >= @date_start
  group by id_human, id_ref);
  create temporary table if not exists SURG
  (select id_human as id_h, id_ref, date_protocol as d4
                         from protocols prot
                         where prot.id_ref in (9493, 8470, 8462)
                         and date_protocol >= @date_start
 group by id_human, id_ref); 
create temporary table if not exists pu_primaries  
	(select pd.id_patient, pd.id_human  from case_services cs  
		left join cases c on c.id_case = cs.id_case  
		left join patient_data pd on pd.id_patient = c.id_patient  
		left join price p on p.id = cs.id_profile  
		where cs.date_begin between @date_start and @date_end   	
		and cs.is_oms = 0 and cs.id_order = -1  and cs.is_paid > 0  	
		and p.code = '51001'    
		group by pd.id_patient  );  
create temporary table if not exists pu_continued_cure  
	(select pd.id_patient, pd.id_human  from case_services cs  
		left join cases c on c.id_case = cs.id_case  
		left join patient_data pd on pd.id_patient = c.id_patient  
		left join orders ord on ord.id = cs.id_order  
		left join price p on p.id = cs.id_profile  
		where cs.date_begin between @date_start and @date_end  	
		and cs.is_oms = 0 and ord.order_type <> 1  	
		and p.code <> '51001' and pd.id_patient in (select id_patient from pu_primaries)   
		group by pd.id_patient  );   

	select @row_num := @row_num + 1, T.dep_name, T.doc_name, T.pat, T.card_number, T.unstruct_address, 
	T.remark, T.date_begin, T.summ from  
	(select dep.name as dep_name,      
		concat(SUBSTRING_INDEX(u.name, ' ', 1), ' ',         
			left(SUBSTRING_INDEX(SUBSTRING_INDEX(u.name, ' ', 2), ' ', -1), 1), '. ',         
			left(SUBSTRING_INDEX(SUBSTRING_INDEX(u.name, ' ', -1), ' ', -1), 1), '.') as doc_name,      
		concat(pd.surname, " ", pd.name, " ", pd.second_name, " д.р. ", pd.birthday) as pat, pd.card_number, cs.date_begin,   
		round(coalesce(sum(cs.SERVICE_COST), 0), 2) as summ, pa.unstruct_address, pd.remark    
		from case_services cs  
		left join cases c on c.id_case = cs.id_case  
		left join patient_data pd on pd.id_patient = c.id_patient  
		left join patient_address pa on pa.id_address = pd.id_address_reg  
		left join doctor_spec ds on ds.doctor_id = cs.id_doctor  
		left join users u on ds.user_id = u.id and u.id <> 1  
		left join departments dep on ds.department_id = dep.id 
		left join price p on p.id = cs.id_profile   
		where cs.date_begin between @date_start and @date_end and ds.user_id <> 1 and  
		(dep.id = @depart_id OR @depart_id = 0 OR @depart_id is null) 
		and (ds.doctor_id = @doc_id OR @doc_id = 0 OR @doc_id is null)  
		and cs.is_oms = 0 and cs.id_order = -1  and p.code = '51001' 
		and pd.id_patient not in (select id_patient from pu_continued_cure) 
		and pd.id_human not in (select id_h from RG)
		and pd.id_human not in (select id_h from ALLER)   
		and pd.id_human not in (select id_h from TER)   
		and pd.id_human not in (select id_h from SURG)   
		group by pd.id_patient   order by dep.name, doc_name, pd.surname, pd.name, pd.second_name ) T; 