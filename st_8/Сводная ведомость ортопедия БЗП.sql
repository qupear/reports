drop temporary table if exists fio, T123,protocol_data, docs, T1, T2, T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,T16,T17,T18,T19,T20,T21,T22,T23,T24,T25,T26,T27;

create temporary table if not exists fio     
(SELECT id, concat(SUBSTRING_INDEX(name, ' ', 1), ' ', 
	left(SUBSTRING_INDEX(SUBSTRING_INDEX(name, ' ', 2), ' ', -1), 1), '. ',            
	left(SUBSTRING_INDEX(SUBSTRING_INDEX(name, ' ', -1), ' ', -1), 1), '.') as short_name FROM users); 
    
create temporary table if not exists T123              
(select fio.short_name, 
		ord.id_doctor, 
		cs.ID_CASE, 
		ord.orderCompletionDate,
		ord.orderCreateDate,
		cs.service_cost * cs.discount * (cs.discount > 0.3) + cs.service_cost * (cs.discount <= 0.3) as cost, 
		cs.ID_PROFILE,
		concat(coalesce(p.code, ""), coalesce(pos.code,"")) as code,
		ds.spec_id, 
		pd.id_human,
		dep.id as depid,
		p.UET,
		pos.UET as orto_uet
from case_services cs    
left join orders ord on cs.ID_ORDER = ord.id        
left join price_orto_soc pos on pos.id = cs.id_profile and (cs.is_oms = 2 or ord.order_type = 1)       
left join price p on p.id = cs.id_profile and cs.is_oms = 0 and ord.order_type <> 1       
left join doctor_spec ds on cs.id_doctor = ds.doctor_id                
left join fio on ds.user_id = fio.id               
left join departments dep on ds.department_id = dep.id                
left join patient_data pd on ord.Id_Human = pd.ID_HUMAN  and pd.is_active = 1 and pd.date_end = '2200-01-01'   
where cs.id_order > 0
	and (ord.orderCreateDate between @date_start and @date_end)  
	and (order_type = 1) 
	and (dep.id = @depart_id or @depart_id = 0 or @depart_id is null)
	and (ord.id_doctor = @doc_id or @doc_id = 0)
	and ds.spec_id in (107) and cs.is_oms in (0)); 

Create temporary table if not exists protocol_data
select ord.id_doctor, pt.id_case, pt.id_ref, ord.orderCreateDate
from protocols pt
left join case_services cs on cs.id_case = pt.id_case
left join orders ord on ord.id = cs.ID_ORDER
left join doctor_spec ds on cs.id_doctor = ds.doctor_id
where cs.id_order > 0 
	and (ord.orderCreateDate between @date_start and @date_end) 
	and (ord.id_doctor = @doc_id or @doc_id = 0)
	and (order_type = 1)
	and ds.spec_id in (107)
	and cs.is_oms in (0);

Create temporary table if not exists docs
select short_name, id_doctor from T123 group by id_doctor;

set @row = 0;
create temporary table if not exists first_row (select @row := @row + 1 as st_num, T.orderCreateDate
                                                from (select orderCreateDate
                                                      from T123
                                                      group by orderCreateDate
                                                      order by orderCreateDate) T);

Create temporary table if not exists T1
select id_doctor, orderCreateDate, count(distinct id_human) as count
from T123
group by orderCreateDate;


Create temporary table if not exists T2
select id_doctor, orderCreateDate, count(distinct id_human) as count
from T123
where code in ('1.2')
group by orderCreateDate;


Create temporary table if not exists T3
select id_doctor, orderCreateDate, count(distinct id_human) as count
from T123
where code in ('1.1')
group by orderCreateDate;

Call create_temp_table("T4", "('13.8', '13.23', '13.26')");
Call create_temp_table("T5", "('9.1')");
Call create_temp_table("T6", "('13.2')");
Call create_temp_table("T7", "('9.4')");
Call create_temp_table("T8", "('13.3')");
Call create_temp_table("T9", "('9.2')");

Create temporary table if not exists T10
select id_doctor, orderCreateDate, count(distinct id_case) as count
from protocol_data
where id_ref in (102480, 102481, 102482, 102483)
group by orderCreateDate;

Call create_temp_table("T11", "('13.2')");
Call create_temp_table("T12", "('9.1')");
Call create_temp_table("T13", "('13.3')");
Call create_temp_table("T14", "('13.4', '10.1', '10.2')");
Call create_temp_table("T15", "('13.5')");
Call create_temp_table("T16", "('3.1', '3.2', '3.7', '5.1', '5.2')");
Call create_temp_table("T17", "('3.4', '3.5', '3.7', '3.20', '5.3', '5.4')");
Call create_temp_table("T18", "('7.2', '7.3', '8.2', '8.3')");
Call create_temp_table("T19", "('2.1', '2.2')");
Call create_temp_table("T20", "('12.2', '15.2')");
Call create_temp_table("T21", "('15.3')");
Call create_temp_table("T22", "('15.6')");
Call create_temp_table("T23", "('15.5')");
Call create_temp_table("T24", "('3.6', '15.5')");
Call create_temp_table("T25", "('4.1', '4.2', '4.3', '4.4', '4.5', '4.6', '4.9', '4.10', '4.11', '6.1', '6.2', '6.3', '6.4', '6.5', '6.6', '6.9', '6.10')");

Create temporary table if not exists T26
select id_doctor, orderCreateDate, count(distinct id_case) as count
from protocol_data
where id_ref in (124161, 123431, 123011, 129070, 129080, 129090, 129100, 129110, 129120, 129130)
group by orderCreateDate;

Create temporary table if not exists T27
select orderCreateDate, round(sum(coalesce(uet, 0) + coalesce(orto_uet, 0)), 2) as val
	from T123
group by orderCreateDate;

SELECT fr.orderCreateDate,
 COALESCE(T1.count, 0), COALESCE(T2.count, 0), COALESCE(T3.count, 0),
 COALESCE(T4.count, 0), COALESCE(T5.count, 0), COALESCE(T6.count, 0), 
 COALESCE(T7.count, 0), COALESCE(T8.count, 0), COALESCE(T9.count, 0),
 COALESCE(T10.count, 0), COALESCE(T11.count, 0), COALESCE(T12.count, 0),
 COALESCE(T13.count, 0), COALESCE(T14.count, 0), COALESCE(T15.count, 0),
 COALESCE(T16.count, 0), COALESCE(T17.count, 0), COALESCE(T18.count, 0),
 COALESCE(T19.count, 0), COALESCE(T20.count, 0), COALESCE(T21.count, 0),
 COALESCE(T22.count, 0), COALESCE(T23.count, 0), COALESCE(T24.count, 0),
 COALESCE(T25.count, 0), COALESCE(T26.count, 0), COALESCE(T27.val)
FROM first_row fr
LEFT JOIN T1 ON fr.orderCreateDate = T1.orderCreateDate
LEFT JOIN T2 ON fr.orderCreateDate = T2.orderCreateDate
LEFT JOIN T3 ON fr.orderCreateDate = T3.orderCreateDate
LEFT JOIN T4 ON fr.orderCreateDate = T4.orderCreateDate
LEFT JOIN T5 ON fr.orderCreateDate = T5.orderCreateDate
LEFT JOIN T6 ON fr.orderCreateDate = T6.orderCreateDate
LEFT JOIN T7 ON fr.orderCreateDate = T7.orderCreateDate
LEFT JOIN T8 ON fr.orderCreateDate = T8.orderCreateDate
LEFT JOIN T9 ON fr.orderCreateDate = T9.orderCreateDate
LEFT JOIN T10 ON fr.orderCreateDate = T10.orderCreateDate
LEFT JOIN T11 ON fr.orderCreateDate = T11.orderCreateDate
LEFT JOIN T12 ON fr.orderCreateDate = T12.orderCreateDate
LEFT JOIN T13 ON fr.orderCreateDate = T13.orderCreateDate
LEFT JOIN T14 ON fr.orderCreateDate = T14.orderCreateDate
LEFT JOIN T15 ON fr.orderCreateDate = T15.orderCreateDate
LEFT JOIN T16 ON fr.orderCreateDate = T16.orderCreateDate
LEFT JOIN T17 ON fr.orderCreateDate = T17.orderCreateDate
LEFT JOIN T18 ON fr.orderCreateDate = T18.orderCreateDate
LEFT JOIN T19 ON fr.orderCreateDate = T19.orderCreateDate
LEFT JOIN T20 ON fr.orderCreateDate = T20.orderCreateDate
LEFT JOIN T21 ON fr.orderCreateDate = T21.orderCreateDate
LEFT JOIN T22 ON fr.orderCreateDate = T22.orderCreateDate
LEFT JOIN T23 ON fr.orderCreateDate = T23.orderCreateDate
LEFT JOIN T24 ON fr.orderCreateDate = T24.orderCreateDate
LEFT JOIN T25 ON fr.orderCreateDate = T25.orderCreateDate
LEFT JOIN T26 ON fr.orderCreateDate = T26.orderCreateDate
LEFT JOIN T27 ON fr.orderCreateDate = T27.orderCreateDate
ORDER BY fr.orderCreateDate;