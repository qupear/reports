set @date_start = '2019-01-01';
set @date_end = '2019-06-17';
set @row = 0;
create temporary table if not exists RG (select id_human as id_h, id_ref, date_protocol as d1
                                         from protocols prot
                                         where prot.id_ref in (9470, 8459, 8464, 8458)
                                           and date_protocol >= @date_start
                                         group by id_human, id_ref);
create temporary table if not exists ALLER (select id_human as id_h, id_ref, date_protocol as d2
                                            from protocols prot
                                            where prot.id_ref in (9491, 8480)
                                              and date_protocol >= @date_start
                                            group by id_human, id_ref);
create temporary table if not exists TER (select id_human as id_h, id_ref, date_protocol as d3
                                          from protocols prot
                                          where prot.id_ref in (9492, 8461)
                                            and date_protocol >= @date_start
                                          group by id_human, id_ref);
create temporary table if not exists SURG (select id_human as id_h, id_ref, date_protocol as d4
                                           from protocols prot
                                           where prot.id_ref in (9493, 8470, 8462)
                                             and date_protocol >= @date_start
                                           group by id_human, id_ref);
create temporary table if not exists start_bzp (select id_human as id_h, id_ref, date_protocol as d4
                                           from protocols prot
                                           where prot.id_ref in (1769, 1869, 2179, 1589, 1629, 1351, 1361,
                                                                 1681, 1770, 1970, 2180, 2190, 2201, 9060,
                                                                 9582, 9586, 9760, 9800, 121790, 121930, 122050,
                                                                 122230, 122270, 122560, 122700, 123070, 123200,
                                                                 123820, 123890)
                                             and date_protocol >= @date_start
                                           group by id_human, id_ref);

 create temporary table if not exists main (select pd.id_human, o.ordercreatedate,
                                                   concat_ws(' ', pd.SURNAME, pd.name, pd.SECOND_NAME) as pat,
                                                   substring_index(o.bzp_direction_text, ' ', - 4) as bzp,
                                                   date_format(o.ordercreatedate, '%d-%m-%Y') as date,
                                                   pa.UNSTRUCT_ADDRESS as adr, pd.remark,
                                                   short_name(u.name) doc,
                                                   coalesce(date_format(RG.d1, '%d-%m-%Y'), '') d1,
                                                   coalesce(date_format(ALLER.d2, '%d-%m-%Y'), '') d2,
                                                   coalesce(date_format(SURG.d4, '%d-%m-%Y'), '') d3,
                                                   coalesce(date_format(TER.d3, '%d-%m-%Y'), '') d4, '' string
                                            from orders o
                                                     left join patient_data pd ON pd.id_human = o.id_human
                                                     left join doctor_spec ds ON ds.doctor_id = o.id_doctor
                                                     left join users u ON ds.user_id = u.id
                                                     left join case_services cs ON cs.id_order = o.id
                                                     left join patient_address pa on pd.ID_ADDRESS_REG = pa.ID_ADDRESS
                                                     left join price_orto_soc pos ON cs.ID_PROFILE = pos.id and o.order_type = 1
                                                left join price p on cs.ID_PROFILE = p.id and o.order_type = 0
                                                left join RG on o.Id_Human = RG.id_h
                                                     left join ALLER on o.Id_Human = ALLER.id_h
                                                     left join TER on o.Id_Human = TER.id_h
                                                     left join SURG on o.Id_Human = SURG.id_h
                                                left join (select count(*) as counter, id_order
                                                                from case_services
                                                                where id_order > 0
                                                                group by id_order) C ON cs.id_order = C.id_order
 where o.orderCreateDate between @date_start and @date_end
   and
  ((pos.CODE = '1.1' or pos.code = '1.4') and c.counter = 1) and   o.Id_Human  not in (select id_h from start_bzp)
   and o.order_type = 1
 group by o.id
 order by o.ordercreatedate, pat);
 create temporary table if not exists main2 (select date_format(RG.d1, '%d-%m-%Y') d1,
                                                    date_format(ALLER.d2, '%d-%m-%Y') d2,
                                                    date_format(SURG.d4, '%d-%m-%Y') d3,
                                                    date_format(TER.d3, '%d-%m-%Y') d4
                                             from orders o
                                                      left join patient_data pd ON pd.id_human = o.id_human
                                                      left join doctor_spec ds ON ds.doctor_id = o.id_doctor
                                                      left join users u ON ds.user_id = u.id
                                                      left join case_services cs ON cs.id_order = o.id
                                                      left join patient_address pa on pd.ID_ADDRESS_REG = pa.ID_ADDRESS
                                                      left join price_orto_soc pos ON cs.ID_PROFILE = pos.id
                                                 left join price p on cs.ID_PROFILE = p.id
                                                      left join RG on o.Id_Human = RG.id_h
                                                      left join ALLER on o.Id_Human = ALLER.id_h
                                                      left join TER on o.Id_Human = TER.id_h
                                                      left join SURG on o.Id_Human = SURG.id_h
  left join (select count(*) as counter, id_order
                                                                from case_services
                                                                where id_order > 0
                                                                group by id_order) C ON cs.id_order = C.id_order
                                             where o.orderCreateDate between @date_start and @date_end
                                               and o.order_type = 1 and
  ((pos.CODE = '1.1' or pos.code = '1.4') and c.counter = 1) and   o.Id_Human  not in (select id_h from start_bzp)
                                             group by o.id);


 create temporary table if not exists visits (select pd.id_human, concat(date_format(cs.DATE_BEGIN, '%d-%m-%Y'), '  ', short_name(u.name))
 																	doc_v
                                              from case_services cs
                                                       left join cases c on cs.ID_CASE = c.ID_CASE
                                                       left join patient_data pd on c.ID_PATIENT = pd.ID_PATIENT
                                                       left join main on pd.ID_HUMAN = main.id_human
                                                       left join doctor_spec ds on cs.ID_DOCTOR = ds.doctor_id
                                                       left join users u on ds.user_id = u.id
                                              where cs.date_begin >= main.ordercreatedate
                                                and (ds.spec_id = 106 or ds.spec_id = 108 or ds.spec_id = 208 or
                                                     ds.spec_id = 211)
                                              group by pd.id_human, cs.id_case, cs.id_doctor
                                              order by pd.id_human, cs.DATE_BEGIN);

 (select @row := @row + 1, T.*
  from (select m.pat, m.bzp, date_format(m.ordercreatedate, '%d-%m-%Y'), m.adr, m.remark, m.doc, m.d1, m.d2, m.d3, m.d4,
               group_concat(v.doc_v order by 1), m.string
        from main m
                 left join visits v on m.id_human = v.id_human
        group by m.ordercreatedate, m.pat
        order by m.ordercreatedate) T)
 union
 (select '', 'Дата создания отчета', '-', date_format(curdate(), '%d-%m-%Y'), '', '', '', count(d1), count(d2),
         count(d3), count(d4), '', ''
  from main2
  where d2 <> ''
     or d1 <> ''
     or d3 <> ''
     or d4 <> '');