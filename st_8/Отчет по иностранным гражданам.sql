set @date_start = '2020-01-01';
set @date_end = '2020-02-01';
set @depart_id = 0;
set @fin_type = 0;

drop temporary table if exists pu_sum;

CREATE TEMPORARY TABLE if NOT EXISTS pu_sum
	select
		cs.id_service, cs.id_case, c.id_patient, SERVICE_COST as summ
	from cases c
	join case_services cs on cs.id_case = c.id_case and cs.is_oms <> 1
	left join price p on p.id = cs.ID_PROFILE and cs.is_oms in (0,3)
	JOIN patient_data pd ON c.id_patient = pd.id_patient 	
	JOIN vmu_oksm vo ON vo.code = pd.c_oksm AND vo.code IS NOT NULL AND vo.code <> 643
	WHERE c.date_begin BETWEEN @date_start AND @date_end;

select dep.name,
	vo.COUNTRY_SHORT_NAME,
	short_name(concat_ws(' ', pd.SURNAME, pd.name, pd.SECOND_NAME)) as pat,
	count(distinct c.id_case),
	pd.POLIS_NUMBER,
	(coalesce(sum(cs.SERVICE_COST), 0) - coalesce(sum(ps.summ), 0)) as sum_oms,
	coalesce(sum(ps.summ), 0) as sum_pu
       from case_services cs
	left join cases c on cs.ID_CASE = c.ID_CASE
	left join patient_data pd on c.ID_PATIENT = pd.ID_PATIENT
	left join vmu_oksm vo on pd.C_OKSM = vo.CODE
	left join pu_sum ps on ps.id_case = cs.id_case and cs.is_oms in (0,3) and ps.id_service = cs.ID_SERVICE
	left join doctor_spec ds on cs.ID_DOCTOR = ds.doctor_id
	left join departments dep on ds.department_id = dep.id
where cs.DATE_BEGIN between @date_start and @date_end and c.IS_CLOSED = 1 
	and pd.C_OKSM <> 643 and pd.IS_ACTIVE = 1 and pd.DATE_END = '2200-01-01'
group by dep.name, vo.ID_OKSM, pd.id_human
order by dep.name, vo.COUNTRY_SHORT_NAME, pat;