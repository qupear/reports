set @date_start = '2019-07-01';
set @date_end = '2019-07-02';
set @doc_id = 0;
set @depart_id = 0;
SELECT dep.name, u.name, date_format(time, '%d-%m-%Y') visit_date,
       TIME_FORMAT(time, '%H:%i') visit_time, case author WHEN 'r' then 'Р'
WHEN 'd' THEN 'Д' WHEN 'i' THEN 'И' ELSE ' ' end author,
        AP.patient_name,
       pa.unstruct_address, ap.phone
FROM appointments ap
LEFT JOIN patient_data pd on ap.id_patient = pd.id_patient
LEFT JOIN patient_address pa ON pd.id_address_reg = pa.id_address
    JOIN doctor_spec ds on ap.doctor_id = ds.doctor_id
JOIN departments dep    on ds.department_id = dep.id
JOIN users u on ap.user_id = u.id
WHERE DATE(time) BETWEEN @date_start AND @date_end
AND ap.patient_name <> ''
AND (AP.doctor_id = @doc_id OR @doc_id = 0)
AND (DEP.ID = @depart_id OR @depart_id = 0)
ORDER BY dep.name, u.name, visit_date, VISIT_TIME