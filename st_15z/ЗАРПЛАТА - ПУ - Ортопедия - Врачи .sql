SET @litie = 16; SET @orto_spec = 107; SET @implant_spec = 212; SET @surgeon_spec = 108;
CREATE TEMPORARY TABLE if NOT EXISTS gold_pu (
SELECT cs.id_order
FROM case_services cs
LEFT JOIN price p ON cs.id_profile = p.id
LEFT JOIN orders ORD ON ord.id = cs.id_order
WHERE ord.orderCompletionDate BETWEEN @date_start AND @date_end AND ord.order_type <> 1 AND ord.spec = 4 AND cs.id_order > 0 AND cs.is_oms = 0 
AND CONVERT(p.code, UNSIGNED INTEGER) BETWEEN 78100 AND 78500
GROUP BY cs.id_order);
CREATE TEMPORARY TABLE if NOT EXISTS gold_bzp (
SELECT cs.id_order
FROM case_services cs
LEFT JOIN price p ON cs.id_profile = p.id
LEFT JOIN orders ORD ON ord.id = cs.id_order
WHERE ord.orderCompletionDate BETWEEN @date_start AND @date_end AND ord.order_type <> 1 AND ord.spec = 4 AND cs.id_order > 0 AND cs.is_oms = 0 
AND CONVERT(p.code, UNSIGNED INTEGER) BETWEEN 78510 AND 78605
GROUP BY cs.id_order);
CREATE TEMPORARY TABLE if NOT EXISTS foundry (
SELECT cs.id_order
FROM case_services cs
LEFT JOIN price p ON cs.id_profile = p.id
LEFT JOIN orders ORD ON ord.id = cs.id_order
WHERE ord.orderCompletionDate BETWEEN @date_start AND @date_end AND ord.order_type <> 1 AND ord.spec = 4 AND cs.id_order > 0 AND cs.is_oms = 0 
AND p.group_ids = @litie
GROUP BY cs.id_order);
CREATE TEMPORARY TABLE if NOT EXISTS table_1 (
SELECT u.id, d.name AS dept_name, u.name AS doc_name, SUM(cs.service_cost * cs.discount) AS sum1, 
SUM(cs.SERVICE_COST * (1 - cs.discount)*(cs.discount=0.3 OR cs.discount=0)) AS dms
FROM orders ORD
LEFT JOIN case_services cs ON cs.id_order = ord.id
LEFT JOIN doctor_spec ds ON ds.doctor_id = ord.id_doctor
LEFT JOIN departments d ON d.id = ds.department_id
LEFT JOIN users u ON u.id = ds.user_id
WHERE ord.orderCompletionDate BETWEEN @date_start AND @date_end AND (ord.order_type = 0 OR ord.order_type = 2) 
AND ord.spec = 4 AND (d.id = @depart_id OR @depart_id = 0) AND ds.spec_id = @orto_spec AND ord.id NOT in 
(SELECT id_order
FROM gold_pu) AND ord.id NOT in (
SELECT id_order
FROM gold_bzp) AND ord.id NOT in (
SELECT id_order
FROM foundry)
GROUP BY u.id
ORDER BY dept_name, doc_name);
CREATE TEMPORARY TABLE if NOT EXISTS table_2_1 
(SELECT u.id, d.name AS dept_name, u.name AS doc_name, SUM(cs.service_cost) AS sum1
FROM orders ORD
LEFT JOIN case_services cs ON cs.id_order = ord.id
LEFT JOIN doctor_spec ds ON ds.doctor_id = ord.id_doctor
LEFT JOIN departments d ON d.id = ds.department_id
LEFT JOIN users u ON u.id = ds.user_id
WHERE ord.orderCompletionDate BETWEEN @date_start AND @date_end AND (ord.order_type = 0 OR ord.order_type = 2) 
AND ord.spec = 4 AND (d.id = @depart_id OR @depart_id = 0) AND ds.spec_id = @orto_spec AND ord.id in (
SELECT id_order
FROM gold_pu)
GROUP BY u.id
ORDER BY dept_name, doc_name);
CREATE TEMPORARY TABLE if NOT EXISTS table_2_2 (
SELECT u.id, d.name AS dept_name, u.name AS doc_name, SUM(cs.service_cost) AS sum1, 
SUM(cs.SERVICE_COST * (1 - cs.discount)*(cs.discount=0.3 OR cs.discount=0)) AS dms
FROM orders ORD
LEFT JOIN case_services cs ON cs.id_order = ord.id
LEFT JOIN doctor_spec ds ON ds.doctor_id = ord.id_doctor
LEFT JOIN departments d ON d.id = ds.department_id
LEFT JOIN users u ON u.id = ds.user_id
WHERE ord.orderCompletionDate BETWEEN @date_start AND @date_end AND (ord.order_type = 0 
OR ord.order_type = 2) AND ord.spec = 4 AND (d.id = @depart_id OR @depart_id = 0) AND ds.spec_id = @orto_spec AND ord.id in (
SELECT id_order
FROM gold_bzp)
GROUP BY u.id
ORDER BY dept_name, doc_name);
CREATE TEMPORARY TABLE if NOT EXISTS table_3 (
SELECT u.id, d.name AS dept_name, u.name AS doc_name, SUM(cs.service_cost * cs.discount) AS sum1, 
SUM(cs.SERVICE_COST * (1 - cs.discount)*(cs.discount=0.3 OR cs.discount=0)) AS dms
FROM orders ORD
LEFT JOIN case_services cs ON cs.id_order = ord.id
LEFT JOIN doctor_spec ds ON ds.doctor_id = ord.id_doctor
LEFT JOIN departments d ON d.id = ds.department_id
LEFT JOIN users u ON u.id = ds.user_id
WHERE ord.orderCompletionDate BETWEEN @date_start AND @date_end AND ord.order_type = 0 
AND ord.spec = 4 AND (d.id = @depart_id OR @depart_id = 0) AND ds.spec_id = @orto_spec AND ord.id in (
SELECT id_order
FROM foundry)
GROUP BY u.id
ORDER BY dept_name, doc_name);
CREATE TEMPORARY TABLE if NOT EXISTS table_4 (
SELECT u.id, d.name AS dept_name, u.name AS doc_name, SUM(p.summ) AS sum1, 
SUM(cs.SERVICE_COST * (1 - cs.discount)*(cs.discount=0.3 OR cs.discount=0)) AS dms
FROM prepayments p
LEFT JOIN case_services cs ON cs.id_order = p.case_id
LEFT JOIN doctor_spec ds ON ds.doctor_id = p.doctor_id
LEFT JOIN departments d ON d.id = ds.department_id
LEFT JOIN users u ON u.id = ds.user_id
LEFT JOIN checks ch ON ch.id = p.check
WHERE ch.date BETWEEN @date_start AND @date_end AND p.order_id = -1 AND (d.id = @depart_id OR @depart_id = 0) AND ds.spec_id = @implant_spec
GROUP BY p.doctor_id
ORDER BY dept_name, doc_name);
CREATE TEMPORARY TABLE if NOT EXISTS table_5 (
SELECT u.id, d.name AS dept_name, u.name AS doc_name, SUM(p.summ) AS sum1, 
SUM(cs.SERVICE_COST * (1 - cs.discount)*(cs.discount=0.3 OR cs.discount=0)) AS dms
FROM prepayments p
LEFT JOIN case_services cs ON cs.id_order = p.case_id
LEFT JOIN doctor_spec ds ON ds.doctor_id = p.doctor_id
LEFT JOIN departments d ON d.id = ds.department_id
LEFT JOIN users u ON u.id = ds.user_id
LEFT JOIN checks ch ON ch.id = p.check
WHERE ch.date BETWEEN @date_start AND @date_end AND p.order_id = -1 AND (d.id = @depart_id OR @depart_id = 0) AND ds.spec_id = @surgeon_spec
GROUP BY p.doctor_id
ORDER BY dept_name, doc_name);
CREATE TEMPORARY TABLE if NOT EXISTS t_main
SELECT d.name AS dept_name, u.name AS doc_name, ds.user_id, ds.doctor_id
FROM doctor_spec ds
LEFT JOIN users u ON u.id = ds.user_id
LEFT JOIN departments d ON d.id = ds.department_id
WHERE ds.active = 1 AND (d.id = @depart_id OR @depart_id = 0) AND u.id IS NOT NULL AND d.id IS NOT NULL
GROUP BY u.id
ORDER BY dept_name, doc_name;
SELECT 
    TM.dept_name,
    TM.doc_name,
    COALESCE(T1.sum1, 0),
    COALESCE(T1.dms, 0),
    COALESCE(T1.sum1, 0) + COALESCE(T1.dms, 0) AS sum1,
    COALESCE(T21.sum1, 0),
    COALESCE(T22.sum1, 0),
    COALESCE(T3.sum1, 0),
    COALESCE(T3.dms, 0),
    COALESCE(T3.sum1, 0) + COALESCE(T3.dms, 0) AS sum2,
    COALESCE(T4.sum1, 0),
    COALESCE(T4.dms, 0),
    COALESCE(T4.sum1, 0) + COALESCE(T4.dms, 0) AS sum3,
    COALESCE(T5.sum1, 0),
    COALESCE(T5.dms, 0),
    COALESCE(T5.sum1, 0) + COALESCE(T5.dms, 0) AS sum4,
    COALESCE(T1.sum1, 0) + COALESCE(T1.dms, 0) + COALESCE(T21.sum1, 0) + COALESCE(T22.sum1, 0) 
+ COALESCE(T3.sum1, 0) + COALESCE(T3.dms, 0) + COALESCE(T4.sum1, 0) + COALESCE(T5.sum1, 0) + COALESCE(T4.dms, 0) + COALESCE(T5.dms, 0)
FROM
    t_main TM
        LEFT JOIN
    table_1 T1 ON T1.id = TM.user_id
        LEFT JOIN
    table_2_1 T21 ON TM.user_id = T21.id
        LEFT JOIN
    table_2_2 T22 ON TM.user_id = T22.id
        LEFT JOIN
    table_3 T3 ON TM.user_id = T3.id
        LEFT JOIN
    table_4 T4 ON TM.user_id = T4.id
        LEFT JOIN
    table_5 T5 ON TM.user_id = T5.id
WHERE
    COALESCE(T1.sum1, 0) + COALESCE(T1.dms, 0) + COALESCE(T21.sum1, 0) + COALESCE(T22.sum1, 0) + 
COALESCE(T3.sum1, 0) + COALESCE(T3.dms, 0) + COALESCE(T4.sum1, 0) + COALESCE(T5.sum1, 0) + COALESCE(T4.dms, 0) + COALESCE(T5.dms, 0) > 0
ORDER BY TM.dept_name , TM.doc_name