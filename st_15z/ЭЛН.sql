SELECT e.eln_number nomer , CONCAT(e.pat_surname, ' ', e.pat_name, ' ', e.pat_second_name) fio , e.pat_snils AS sni, pd.CARD_NUMBER AS canu, e.eln_date sozdan , e.doctor_name doc , es.name status , e.fss_sync_date redakt, CASE WHEN et.signature_xml_vk <> '' THEN '��'
ELSE '-' END AS isvk
FROM eln e
JOIN eln_states es ON
e.eln_state = es.id
LEFT JOIN eln_treats et ON
e.id = et.id_eln
LEFT JOIN patient_data pd ON
e.pat_id = pd.ID_PATIENT
WHERE e.eln_date BETWEEN @date_start AND @date_end
ORDER BY fio;