DROP TEMPORARY TABLE IF EXISTS mkbstatistics, mkbNextStat;

CALL mkbstat('K10.3', @date_start, @date_end);

SELECT lefter.`��� ��������`, lefter.`����`, lefter.`��� �����`, joiner.`��� �����` AS `��� ��������`
FROM (SELECT pd.ID_PATIENT , concat(pd.SURNAME, ' ', pd.NAME, ' ', pd.SECOND_NAME) AS `��� ��������`, ca.DATE_end AS `����`, u.name AS `��� �����`
FROM mkbstatistics AS mk
LEFT JOIN cases ca ON
mk.resCas = ca.ID_CASE
AND ca.DATE_END BETWEEN @date_start AND @date_end
LEFT JOIN doctor_spec ds ON
ca.ID_DOCTOR = ds.doctor_id
LEFT JOIN users u ON
ds.user_id = u.id
LEFT JOIN patient_data pd ON
ca.ID_PATIENT = pd.ID_PATIENT) AS lefter
LEFT JOIN (SELECT pd.ID_PATIENT , concat(pd.SURNAME, ' ', pd.NAME, ' ', pd.SECOND_NAME) AS `��� ��������`, ca.DATE_end AS `����`, u.name AS `��� �����`
FROM mkbnextstat AS mk
LEFT JOIN cases ca ON
mk.resCas = ca.ID_CASE
AND ca.DATE_END BETWEEN @date_start AND @date_end
LEFT JOIN doctor_spec ds ON
ca.ID_DOCTOR = ds.doctor_id
LEFT JOIN users u ON
ds.user_id = u.id
LEFT JOIN patient_data pd ON
ca.ID_PATIENT = pd.ID_PATIENT) AS joiner ON
lefter.id_patient = joiner.id_patient
WHERE joiner.`��� �����` IS NOT NULL
UNION SELECT concat('��������� ', cm.countAlveolit), '2200-01-01', '�������', cm.countDelete
FROM counterMkb cm;