SELECT 
 TD.dept_name,
 TD.doc_name, COALESCE(TM.total_sum, 0), 
COALESCE(T1_pay.total_sum, 0), 
COALESCE(TM.total_uet, 0), 
COALESCE(T2.total, 0), 
coalesce(tmo.total_uet,0),
COALESCE(T3.total, 0), 
COALESCE(T1_pay.total_sum, 0) + COALESCE(T3.total, 0)
FROM
		
		 ( 
		SELECT 
		 d.name AS dept_name, u.name AS doc_name, ds.doctor_id as id_doctor
		FROM
		 doctor_spec ds
		LEFT JOIN users u ON u.id = ds.user_id
		LEFT JOIN departments d ON d.id = ds.department_id
		left join prepayments p on p.doctor_id = ds.doctor_id
		LEFT JOIN checks ch ON ch.id = p.check
		WHERE
		 ds.spec_id = 103 AND
ch.date BETWEEN @date_start AND @date_end
and u.id IS NOT NULL AND u.id <> 1
		ORDER BY dept_name, doc_name) TD 
left join
		 (
		SELECT 
		 cs.id_doctor, SUM(cs.service_cost) AS total_sum, SUM(p.uet) AS total_uet
		FROM
			 case_services cs
			LEFT JOIN doctor_spec ds ON ds.doctor_id = cs.id_doctor
			LEFT JOIN price p ON p.id = cs.id_profile
			WHERE
			 cs.id_order < 0 
			AND ds.spec_id = 103 
			AND cs.is_oms = 0 
			AND cs.date_begin BETWEEN @date_start AND @date_end
			GROUP BY cs.id_doctor) TM on tm.id_doctor = td.id_doctor
left join 
 (
		SELECT 
		 cs.id_doctor,SUM(p.uet) AS total_uet
		FROM
			 case_services cs
			LEFT JOIN doctor_spec ds ON ds.doctor_id = cs.id_doctor
			LEFT JOIN price p ON p.id = cs.id_profile
			left join orders o on o.id = cs.id_order
			WHERE
			 cs.id_order > 0 
			AND ds.spec_id = 103 
			AND cs.is_oms = 0 
			and o.status = 2
			AND cs.date_begin BETWEEN @date_start AND @date_end
			GROUP BY cs.id_doctor) TMo on tmo.id_doctor = td.id_doctor

	LEFT JOIN
		 (
		SELECT 
		 p.doctor_id, SUM(p.summ) AS total_sum
		FROM
		 prepayments p
		LEFT JOIN checks ch ON ch.id = p.check
		LEFT JOIN doctor_spec ds ON ds.doctor_id = p.doctor_id
		WHERE
		 p.order_id < 0 
		AND ds.spec_id = 103 
		AND ch.date BETWEEN @date_start AND @date_end
		GROUP BY p.doctor_id) T1_pay ON T1_pay.doctor_id = Td.id_doctor
	LEFT JOIN
		 (
		SELECT 
		 cs.id_doctor, SUM(cs.service_cost) AS total
		FROM
		 case_services cs
		LEFT JOIN stomadb.orders ORD ON ord.id = cs.id_order
		LEFT JOIN doctor_spec ds ON ds.doctor_id = ord.id_doctor
		WHERE
		 ord.order_type = 0 
			AND ds.spec_id = 103 
			AND ord.status = 2 
			AND orderCompletionDate BETWEEN @date_start AND @date_end 
			AND cs.id_order > 0
		GROUP BY cs.id_doctor) T2 ON T2.id_doctor = Td.id_doctor
	LEFT JOIN
		 (
		SELECT SUM(p.summ) AS total, p.doctor_id, p.order_id
		FROM
		 prepayments p
	LEFT JOIN checks ch ON ch.id = p.check
WHERE
 ch.date BETWEEN @date_start AND @date_end AND p.order_id > 0 
GROUP BY p.doctor_id) T3 ON T3.doctor_id = Td.id_doctor
group by td.id_doctor