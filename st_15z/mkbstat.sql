CREATE DEFINER=`test_user`@`%` PROCEDURE `stomadb`.`mkbstat`(
IN `IN_STR_MKB` varchar(255),date_start date, date_end date)
BEGIN
  DECLARE rowCountDescription INT DEFAULT 0;
  DECLARE rowCountTitle INT DEFAULT 0;
  DECLARE updateDescription CURSOR FOR
    SELECT id FROM books WHERE description IS NULL OR CHAR_LENGTH(description) < 10;
  DECLARE updateTitle CURSOR FOR
    SELECT id FROM books WHERE title IS NULL OR CHAR_LENGTH(title) <= 10;

  DECLARE cursNext CURSOR FOR 
  SELECT rp.id FROM ref_protocols rp WHERE rp.is_active =1 AND rp.`text` LIKE '%������%���%' AND rp.`text` NOT LIKE '%����%' AND rp.`text` NOT LIKE '%�����%' AND rp.`text` NOT LIKE '%������%' AND rp.`text` NOT LIKE '%������%' AND rp.`text` NOT LIKE '%����%' AND rp.`text` NOT LIKE '%������%' AND rp.`text` NOT LIKE '%�����%' AND rp.`text` NOT LIKE '%����%' AND rp.`text` NOT LIKE '%����%' AND rp.id_razd =5;
  
  DECLARE cursMkb CURSOR FOR 
  SELECT p.id FROM protocols p WHERE locate(8771,p.child_ref_ids)>0 OR  locate(8773,p.child_ref_ids)>0 OR locate(8794,p.child_ref_ids)>0 OR locate(8795,p.child_ref_ids)>0;

  DROP TEMPORARY TABLE IF EXISTS tblResults;
  CREATE TEMPORARY TABLE IF NOT EXISTS tblResults (
    resId int, resCas int
  );
  
  DROP TEMPORARY TABLE IF EXISTS nextResults;
  CREATE TEMPORARY TABLE IF NOT EXISTS nextResults (
    resId int, resCas int
  );
  
  DROP TEMPORARY TABLE IF EXISTS counterMkb;
  CREATE TEMPORARY TABLE IF NOT EXISTS counterMkb (
    countDelete int, countAlveolit int
  );

  OPEN cursNext;
  BEGIN
      DECLARE exit_flag INT DEFAULT 0;
      DECLARE rawId INT(10);
      DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;

      cursNextLoop: LOOP
        FETCH cursNext INTO rawId;
            IF exit_flag THEN LEAVE cursNextLoop; 
            END IF;
            INSERT INTO tblResults (resId,resCas)
            SELECT p.id,p.id_case FROM protocols p WHERE locate(rawId,p.child_ref_ids)>0 
        AND p.date_protocol BETWEEN date_start AND date_end;
        SET rowCountDescription = rowCountDescription + 1;
      END LOOP;
  END;
  CLOSE cursNext;
/*
  DECLARE cursMkb CURSOR FOR 
  SELECT resid FROM tblResults r;   
  */
  
  OPEN cursMkb;
  BEGIN
      DECLARE exit_flag INT DEFAULT 0;
      DECLARE book_id INT(10);
      DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;

      cursMkbLoop: LOOP
        FETCH cursMkb INTO book_id;
            IF exit_flag THEN LEAVE cursMkbLoop; 
            END IF;
            INSERT INTO nextResults(resId,resCas)
            SELECT p.id,p.id_case FROM protocols p WHERE p.id=book_id;
        SET rowCountTitle = rowCountTitle + 1;
      END LOOP;
  END;
  CLOSE cursMkb;

--  SELECT 'number of descriptions updated =', rowCountDescription;
 DROP TEMPORARY TABLE IF EXISTS mkbNextStat;
  CREATE TEMPORARY TABLE IF NOT EXISTS mkbNextStat
  SELECT
    *
  FROM tblResults r;
  DROP TEMPORARY TABLE IF EXISTS mkbstatistics;
  CREATE TEMPORARY TABLE IF NOT EXISTS mkbstatistics
  SELECT
    *
  FROM nextResults r;
  INSERT INTO counterMkb
    SELECT rowCountDescription,rowCountTitle;
END