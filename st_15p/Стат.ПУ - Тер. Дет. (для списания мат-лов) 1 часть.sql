set @date_start = '2019-12-12';
set @date_end = '2019-12-24';
set @depart_id = 0;

drop temporary table if exists T1, T2, T3, T4, T5, T6, T7, T8, T9, T10,
T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, T23, T24, T25;

/*Выборка показателей по определенным кодам*/
create temporary table if not exists T1
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 10100
 and cs.date_end between @date_start and @date_end;
  
  create temporary table if not exists T2
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 10105
 and cs.date_end between @date_start and @date_end;
  
 create temporary table if not exists T3
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 10120
 and cs.date_end between @date_start and @date_end;
  
  create temporary table if not exists T4
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 10220
 and cs.date_end between @date_start and @date_end;
  
  create temporary table if not exists T5
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 10270
 and cs.date_end between @date_start and @date_end;
  
  create temporary table if not exists T6
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 10315
 and cs.date_end between @date_start and @date_end;
  
  create temporary table if not exists T7
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 10320
 and cs.date_end between @date_start and @date_end;
  
  create temporary table if not exists T8
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 10350
 and cs.date_end between @date_start and @date_end;
  
  create temporary table if not exists T9
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 12150
 and cs.date_end between @date_start and @date_end;
  
  create temporary table if not exists T10
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 12190
 and cs.date_end between @date_start and @date_end;
  
  create temporary table if not exists T11
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 12250
 and cs.date_end between @date_start and @date_end;
  
  create temporary table if not exists T12
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 12270
 and cs.date_end between @date_start and @date_end;
  
  create temporary table if not exists T13
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 12280
 and cs.date_end between @date_start and @date_end;

create temporary table if not exists T14
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 12300
 and cs.date_end between @date_start and @date_end;

create temporary table if not exists T15
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 12350
 and cs.date_end between @date_start and @date_end;

create temporary table if not exists T16
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 12355
 and cs.date_end between @date_start and @date_end;

create temporary table if not exists T17
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 12380
 and cs.date_end between @date_start and @date_end;

create temporary table if not exists T18
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 12390
 and cs.date_end between @date_start and @date_end;

create temporary table if not exists T19
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 12400
 and cs.date_end between @date_start and @date_end;

create temporary table if not exists T20
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 12410
 and cs.date_end between @date_start and @date_end;

create temporary table if not exists T21
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 12420
 and cs.date_end between @date_start and @date_end;

create temporary table if not exists T22
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 12430
 and cs.date_end between @date_start and @date_end;

create temporary table if not exists T23
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 12440
 and cs.date_end between @date_start and @date_end;

create temporary table if not exists T24
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 12460
 and cs.date_end between @date_start and @date_end;

create temporary table if not exists T25
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 12470
 and cs.date_end between @date_start and @date_end;
  
  select d.name, u.name,
  count(T1.id_service), count(T2.id_service), count(T3.id_service),
  count(T4.id_service), count(T5.id_service), count(T6.id_service),
  count(T7.id_service), count(T8.id_service), count(T9.id_service),
  count(T10.id_service), count(T11.id_service), count(T12.id_service),
  count(T13.id_service), count(T14.id_service), count(T15.id_service),
  count(T16.id_service), count(T17.id_service), count(T18.id_service),
  count(T19.id_service), count(T20.id_service), count(T21.id_service),
  count(T22.id_service), count(T23.id_service), count(T24.id_service),
  count(T25.id_service)
  from case_services cs
  left join doctor_spec ds on ds.doctor_id = cs.ID_DOCTOR
  left join users u on u.id = ds.user_id
  left join departments d on d.id = ds.department_id
  left join T1 on T1.id_service = cs.ID_SERVICE
  left join T2 on T2.id_service = cs.ID_SERVICE
  left join T3 on T3.id_service = cs.ID_SERVICE
  left join T4 on T4.id_service = cs.ID_SERVICE
  left join T5 on T5.id_service = cs.ID_SERVICE
  left join T6 on T6.id_service = cs.ID_SERVICE
  left join T7 on T7.id_service = cs.ID_SERVICE
  left join T8 on T8.id_service = cs.ID_SERVICE
  left join T9 on T9.id_service = cs.ID_SERVICE
  left join T10 on T10.id_service = cs.ID_SERVICE
  left join T11 on T11.id_service = cs.ID_SERVICE
  left join T12 on T12.id_service = cs.ID_SERVICE
  left join T13 on T13.id_service = cs.ID_SERVICE
  left join T14 on T14.id_service = cs.ID_SERVICE
  left join T15 on T15.id_service = cs.ID_SERVICE
  left join T16 on T16.id_service = cs.ID_SERVICE
  left join T17 on T17.id_service = cs.ID_SERVICE
  left join T18 on T18.id_service = cs.ID_SERVICE
  left join T19 on T19.id_service = cs.ID_SERVICE
  left join T20 on T20.id_service = cs.ID_SERVICE
  left join T21 on T21.id_service = cs.ID_SERVICE
  left join T22 on T22.id_service = cs.ID_SERVICE
  left join T23 on T23.id_service = cs.ID_SERVICE
  left join T24 on T24.id_service = cs.ID_SERVICE
  left join T25 on T25.id_service = cs.ID_SERVICE
  
where cs.date_end between @date_start and @date_end
  and (d.id = @depart_id or @depart_id = 0)
 and (coalesce(T1.id_service, 0) + 
 coalesce(T2.id_service, 0) +
 coalesce(T3.id_service, 0) +
 coalesce(T4.id_service, 0) +
 coalesce(T5.id_service, 0) +
 coalesce(T6.id_service, 0) +
 coalesce(T7.id_service, 0) +
 coalesce(T8.id_service, 0) +
 coalesce(T9.id_service, 0) +
 coalesce(T10.id_service, 0) +
 coalesce(T11.id_service, 0) +
 coalesce(T12.id_service, 0) +
 coalesce(T13.id_service, 0) +
 coalesce(T14.id_service, 0) +
 coalesce(T15.id_service, 0) +
 coalesce(T16.id_service, 0) +
 coalesce(T17.id_service, 0) +
 coalesce(T18.id_service, 0) +
 coalesce(T19.id_service, 0) +
 coalesce(T20.id_service, 0) +
 coalesce(T21.id_service, 0) +
 coalesce(T22.id_service, 0) +
 coalesce(T23.id_service, 0) +
 coalesce(T24.id_service, 0) +
 coalesce(T25.id_service, 0)) <> 0
group by cs.ID_DOCTOR
 order by u.name;