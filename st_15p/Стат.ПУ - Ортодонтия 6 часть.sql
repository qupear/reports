set @date_start = '2019-12-12';
set @date_end = '2019-12-24';
set @depart_id = 0;

drop temporary table if exists T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13;

/*Выборка показателей по определенным кодам*/
create temporary table if not exists T1
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 43160
 and cs.date_end between @date_start and @date_end;
  
  create temporary table if not exists T2
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 43100
 and cs.date_end between @date_start and @date_end;
  
 create temporary table if not exists T3
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 43170
 and cs.date_end between @date_start and @date_end;
  
  create temporary table if not exists T4
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 43110
 and cs.date_end between @date_start and @date_end;
  
  create temporary table if not exists T5
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 43180
 and cs.date_end between @date_start and @date_end;
  
  create temporary table if not exists T6
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 43120
 and cs.date_end between @date_start and @date_end;
  
  create temporary table if not exists T7
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 43190
 and cs.date_end between @date_start and @date_end;
  
  create temporary table if not exists T8
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 43200
 and cs.date_end between @date_start and @date_end;
  
  create temporary table if not exists T9
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 43130
 and cs.date_end between @date_start and @date_end;
  
  create temporary table if not exists T10
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 43210
 and cs.date_end between @date_start and @date_end;
  
  create temporary table if not exists T11
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 43140
 and cs.date_end between @date_start and @date_end;
  
  create temporary table if not exists T12
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 43220
 and cs.date_end between @date_start and @date_end;
  
  create temporary table if not exists T13
  select id_service
  from case_services cs
  left join price p on p.id = cs.ID_PROFILE
  where p.code = 43150
 and cs.date_end between @date_start and @date_end;
  
  select d.name, u.name,
  count(T1.id_service), count(T2.id_service), count(T3.id_service),
  count(T4.id_service), count(T5.id_service), count(T6.id_service),
  count(T7.id_service), count(T8.id_service), count(T9.id_service),
  count(T10.id_service), count(T11.id_service), count(T12.id_service),
  count(T13.id_service)
  from case_services cs
  left join doctor_spec ds on ds.doctor_id = cs.ID_DOCTOR
  left join users u on u.id = ds.user_id
  left join departments d on d.id = ds.department_id
  left join T1 on T1.id_service = cs.ID_SERVICE
  left join T2 on T2.id_service = cs.ID_SERVICE
  left join T3 on T3.id_service = cs.ID_SERVICE
  left join T4 on T4.id_service = cs.ID_SERVICE
  left join T5 on T5.id_service = cs.ID_SERVICE
  left join T6 on T6.id_service = cs.ID_SERVICE
  left join T7 on T7.id_service = cs.ID_SERVICE
  left join T8 on T8.id_service = cs.ID_SERVICE
  left join T9 on T9.id_service = cs.ID_SERVICE
  left join T10 on T10.id_service = cs.ID_SERVICE
  left join T11 on T11.id_service = cs.ID_SERVICE
  left join T12 on T12.id_service = cs.ID_SERVICE
  left join T13 on T13.id_service = cs.ID_SERVICE
  where cs.date_end between @date_start and @date_end
  and (d.id = @depart_id or @depart_id = 0)
 and (coalesce(T1.id_service, 0) + 
 coalesce(T2.id_service, 0) +
 coalesce(T3.id_service, 0) +
 coalesce(T4.id_service, 0) +
 coalesce(T5.id_service, 0) +
 coalesce(T6.id_service, 0) +
 coalesce(T7.id_service, 0) +
 coalesce(T8.id_service, 0) +
 coalesce(T9.id_service, 0) +
 coalesce(T10.id_service, 0) +
 coalesce(T11.id_service, 0) +
 coalesce(T12.id_service, 0) +
 coalesce(T13.id_service, 0)) <> 0
 group by cs.ID_DOCTOR
 order by u.name;