drop temporary table if exists t123;

CREATE TEMPORARY TABLE IF NOT EXISTS fio (
	                                         SELECT id, concat(SUBSTRING_INDEX(name, ' ', 1), ' ',
	                                                           left(SUBSTRING_INDEX(SUBSTRING_INDEX(name, ' ', 2), ' ', -1), 1),
	                                                           '. ', left(
			                                                           SUBSTRING_INDEX(SUBSTRING_INDEX(name, ' ', -1), ' ', -1),
			                                                           1), '.') AS short_name
		                                         FROM users);
CREATE TEMPORARY TABLE IF NOT EXISTS t123 (
	                                          SELECT fio.short_name, cs.id_doctor, cs.id_case, cs.date_begin,
	                                                 cs.id_profile, vp.profile_infis_code, vd.diagnosis_code, t.uet uet,
	                                                 t.price price, pd.id_human, dep.name AS depname, dep.id AS depid
		                                          FROM
			                                          case_services                                                            cs
				                                          LEFT JOIN vmu_profile                                                vp ON vp.id_profile = cs.id_profile
				                                          LEFT JOIN doctor_spec                                                ds ON cs.id_doctor = ds.doctor_id
				                                          LEFT JOIN users ON ds.user_id = users.id
				                                          LEFT JOIN vmu_diagnosis                                              vd ON cs.id_diagnosis = vd.id_diagnosis
				                                          LEFT JOIN departments                                                dep ON ds.department_id = dep.id
				                                          LEFT JOIN fio ON users.id = fio.id
				                                          LEFT JOIN cases                                                      c ON c.id_case = cs.id_case
				                                          LEFT JOIN patient_data                                               pd ON pd.id_patient = c.id_patient
				                                          LEFT JOIN (
					                                                    SELECT vt.uet, vt.price, vt.id_profile,
					                                                           vt.id_zone_type,
					                                                           vt.tariff_begin_date AS d1,
					                                                           vt.tariff_end_date AS d2,
					                                                           vtp.tp_begin_date AS d3,
					                                                           vtp.tp_end_date AS d4
						                                                    FROM vmu_tariff vt, vmu_tariff_plan vtp
						                                                    WHERE vtp.id_tariff_group = vt.id_lpu
							                                                  AND vtp.id_lpu IN (SELECT id_lpu FROM mu_ident)) t
				                                                    ON t.id_profile = vp.id_profile AND
				                                                       cs.date_begin BETWEEN t.d1 AND t.d2 AND
				                                                       cs.date_begin BETWEEN t.d3 AND t.d4 AND
				                                                       t.id_zone_type = cs.id_net_profile AND
				                                                       cs.is_oms = 1
		                                          WHERE cs.date_begin BETWEEN @date_start AND @date_end
			                                        AND (dep.id = @depart_id OR @depart_id = 0 OR @depart_id IS NULL)
			                                        AND cs.is_oms = 1);
CREATE TEMPORARY TABLE IF NOT EXISTS t2 (
	                                        SELECT id_doctor, count(profile_infis_code) AS t22
		                                        FROM t123
		                                        WHERE profile_infis_code IN
		                                              ('стт005', 'стт006', 'стт007', 'стт008', 'стт009', 'стт010',
		                                               'стт011', 'стт012', 'стт013', 'стт014', 'стт054', 'стт055',
		                                               'стт056')
		                                        GROUP BY id_doctor);
CREATE TEMPORARY TABLE IF NOT EXISTS t3 (
	                                        SELECT id_doctor, count(profile_infis_code) AS t33
		                                        FROM t123
		                                        WHERE profile_infis_code IN
		                                              ('стт005', 'стт007', 'стт008', 'стт009', 'стт011', 'стт012',
		                                               'стт013', 'стт054', 'стт056')
		                                        GROUP BY id_doctor);
CREATE TEMPORARY TABLE IF NOT EXISTS t4 (
	                                        SELECT id_doctor, count(diagnosis_code) AS t44
		                                        FROM t123
		                                        WHERE diagnosis_code IN
		                                              ('K02.0', 'K02.1', 'K02.3', 'K02.8', 'K04.0', 'K04.1', 'K04.4',
		                                               'K04.5')
			                                      AND profile_infis_code IN ('стт025', 'стт028', 'стт031')
		                                        GROUP BY id_doctor);
CREATE TEMPORARY TABLE IF NOT EXISTS t5 (
	                                        SELECT id_doctor, count(diagnosis_code) AS t55
		                                        FROM t123
		                                        WHERE diagnosis_code IN ('K02.0', 'K02.1', 'K02.3', 'K02.8')
			                                      AND profile_infis_code IN ('стт025', 'стт028', 'стт031')
		                                        GROUP BY id_doctor);
CREATE TEMPORARY TABLE IF NOT EXISTS t6 (
	                                        SELECT id_doctor, count(diagnosis_code) AS t66
		                                        FROM t123
		                                        WHERE diagnosis_code IN ('K04.0', 'K04.1', 'K04.4', 'K04.5')
			                                      AND profile_infis_code IN ('стт025', 'стт028', 'стт031')
		                                        GROUP BY id_doctor);
CREATE TEMPORARY TABLE IF NOT EXISTS t7 (
	                                        SELECT id_doctor, count(diagnosis_code) AS t77
		                                        FROM t123
		                                        WHERE diagnosis_code IN ('K04.0', 'K04.1')
			                                      AND profile_infis_code IN ('стт025', 'стт028', 'стт031')
		                                        GROUP BY id_doctor);
CREATE TEMPORARY TABLE IF NOT EXISTS t8 (
	                                        SELECT id_doctor, count(profile_infis_code) AS t88
		                                        FROM t123
		                                        WHERE profile_infis_code IN ('стт039')
		                                        GROUP BY id_doctor);
CREATE TEMPORARY TABLE IF NOT EXISTS t9 (
	                                        SELECT id_doctor, count(diagnosis_code) AS t99
		                                        FROM t123
		                                        WHERE diagnosis_code IN ('K04.4', 'K04.5')
			                                      AND profile_infis_code IN ('стт025', 'стт028', 'стт031')
		                                        GROUP BY id_doctor);
CREATE TEMPORARY TABLE IF NOT EXISTS t10 (
	                                         SELECT id_doctor, count(profile_infis_code) AS t100
		                                         FROM t123
		                                         WHERE profile_infis_code IN ('стт046')
		                                         GROUP BY id_doctor);
CREATE TEMPORARY TABLE IF NOT EXISTS t11 (
	                                         SELECT id_doctor, count(profile_infis_code) AS t111
		                                         FROM t123
		                                         WHERE profile_infis_code IN
		                                               ('стт026', 'стт027', 'стт029', 'стт030', 'стт032', 'стт033')
		                                         GROUP BY id_doctor);
CREATE TEMPORARY TABLE IF NOT EXISTS t12 (
	                                         SELECT id_doctor, count(profile_infis_code) AS t112
		                                         FROM t123
		                                         WHERE profile_infis_code IN ('стт034')
		                                         GROUP BY id_doctor);
CREATE TEMPORARY TABLE IF NOT EXISTS t13 (
	                                         SELECT id_doctor, count(profile_infis_code) AS t113
		                                         FROM t123
		                                         WHERE profile_infis_code IN ('стт041')
		                                         GROUP BY id_doctor);
CREATE TEMPORARY TABLE IF NOT EXISTS t14 (
	                                         SELECT id_doctor, count(profile_infis_code) AS t114
		                                         FROM t123
		                                         WHERE profile_infis_code IN ('сто002', 'сто003')
		                                         GROUP BY id_doctor);
CREATE TEMPORARY TABLE IF NOT EXISTS t15 (SELECT id_doctor, sum(coalesce(uet, 0)) AS uet1 FROM t123 GROUP BY id_doctor);
CREATE TEMPORARY TABLE IF NOT EXISTS t16 (
	                                         SELECT id_doctor, sum(coalesce(price, 0)) AS summ
		                                         FROM t123
		                                         GROUP BY id_doctor);
CREATE TEMPORARY TABLE IF NOT EXISTS docs
	(SELECT depname, short_name, id_doctor, depid FROM t123 GROUP BY id_doctor);
SELECT depname, short_name, coalesce(t2.t22, 0), coalesce(t3.t33, 0), coalesce(t4.t44, 0), coalesce(t5.t55, 0),
       coalesce(t6.t66, 0), coalesce(t7.t77, 0), coalesce(t8.t88, 0), coalesce(t9.t99, 0), coalesce(t10.t100, 0),
       coalesce(t11.t111, 0), coalesce(t12.t112, 0), coalesce(t13.t113, 0), coalesce(t14.t114, 0),
       round(coalesce(t15.uet1, 0), 2), round(coalesce(t16.summ, 0), 2)
	FROM
		docs
			LEFT JOIN t2 ON docs.id_doctor = t2.id_doctor
			LEFT JOIN t3 ON docs.id_doctor = t3.id_doctor
			LEFT JOIN t4 ON docs.id_doctor = t4.id_doctor
			LEFT JOIN t5 ON docs.id_doctor = t5.id_doctor
			LEFT JOIN t6 ON docs.id_doctor = t6.id_doctor
			LEFT JOIN t7 ON docs.id_doctor = t7.id_doctor
			LEFT JOIN t8 ON docs.id_doctor = t8.id_doctor
			LEFT JOIN t9 ON docs.id_doctor = t9.id_doctor
			LEFT JOIN t10 ON docs.id_doctor = t10.id_doctor
			LEFT JOIN t11 ON docs.id_doctor = t11.id_doctor
			LEFT JOIN t12 ON docs.id_doctor = t12.id_doctor
			LEFT JOIN t13 ON docs.id_doctor = t13.id_doctor
			LEFT JOIN t14 ON docs.id_doctor = t14.id_doctor
			LEFT JOIN t15 ON docs.id_doctor = t15.id_doctor
			LEFT JOIN t16 ON docs.id_doctor = t16.id_doctor
	WHERE depid = 27 OR depid = 28 OR depid = 24 OR depid = 26 OR depid = 1
	GROUP BY short_name, depname
	ORDER BY short_name ;
























