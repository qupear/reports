select T1.dept_name, T1.doc_name, T1.total_cost, T1.total_uet
from (select d.name as dept_name,
            u.name as doc_name,
			ROUND(COALESCE(SUM(T.price), 0), 2) AS total_cost,
			ROUND(COALESCE(SUM(T.uet), 0), 2) as total_uet,
            ds.doctor_id as doc_id
    from case_services cs
    left join cases c ON c.id_case = cs.id_case
    left join doctor_spec ds ON ds.doctor_id = cs.id_doctor
    left join users u ON ds.user_id = u.id
    left join departments d ON d.id = ds.department_id
	left join vmu_profile vp on vp.id_profile = cs.id_profile and cs.is_oms = 1
	left join vmu_diagnosis vd on cs.id_diagnosis = vd.id_diagnosis
    left join (select vt.price, vt.uet, vt.id_profile, vt.id_zone_type,
				vt.tariff_begin_date as d1,
				vt.tariff_end_date as d2,
				vtp.tp_begin_date as d3,
				vtp.tp_end_date as d4
			from vmu_tariff vt, vmu_tariff_plan vtp
			where vtp.id_tariff_group = vt.id_lpu
				and vtp.id_lpu in (select id_lpu from mu_ident)) T
		on T.id_profile = vp.id_profile and
			cs.date_begin between T.d1 and T.d2 and
			cs.date_begin between T.d3 and T.d4 and
			T.id_zone_type = cs.id_net_profile and
			cs.is_oms in (1,5) 
	where c.date_end between @date_start and @date_end
		and cs.is_oms in (1,5)
		and cs.id_order = -1
		and c.is_closed = 1
		and (d.id = @depart_id or @depart_id = 0)
		and (cs.id_doctor = @doc_id or @doc_id = 0)
	group by ds.doctor_id
    order by dept_name , doc_name , doc_id) T1;