set @date_start = '2019-01-01';
set @date_end = '2019-08-10';
SELECT
  dep.name AS dt,
  u.name AS doc,
  sp.name AS spec,
  c.date_begin AS db,
  COUNT(DISTINCT c.id_case) AS open,
  COUNT(DISTINCT PROT.id_case) AS protocols
FROM cases c
  JOIN case_services cs
    ON c.id_case = cs.id_case
    AND cs.is_oms = 1
  LEFT JOIN doctor_spec ds
    ON ds.doctor_id = c.ID_DOCTOR
  LEFT JOIN users u
    ON u.id = ds.user_id
  LEFT JOIN speciality sp
    ON sp.id = ds.spec_id
  LEFT JOIN departments dep
    ON dep.id = ds.department_id
  LEFT JOIN protocols prot
    ON PROT.id_case = c.id_case
WHERE c.DATE_BEGIN BETWEEN @date_start AND @date_end
GROUP BY dt,
         doc,
         spec,
         db
ORDER BY dt, doc, spec, db;