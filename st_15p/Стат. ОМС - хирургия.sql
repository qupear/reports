CREATE TEMPORARY TABLE if NOT EXISTS fio (
SELECT id, CONCAT(SUBSTRING_INDEX(name, ' ', 1), ' ',
LEFT(SUBSTRING_INDEX(SUBSTRING_INDEX(name, ' ', 2), ' ', -1), 1), '. ',
LEFT(SUBSTRING_INDEX(SUBSTRING_INDEX(name, ' ', -1), ' ', -1), 1), '.') AS short_name
FROM users);

CREATE TEMPORARY TABLE if NOT EXISTS doctors (
SELECT ds.spec_id,
	fio.short_name,
	cs.id_doctor,
	cs.ID_CASE,
	cs.DATE_BEGIN,
	cs.ID_PROFILE,
	vp.PROFILE_INFIS_CODE,
	vd.diagnosis_code,
	T.UET, T.PRICE,
	(DATE_ADD(pd.birthday, INTERVAL 18 YEAR) > cs.date_begin) AS child,
	(LEFT(vp.PROFILE_INFIS_CODE, 1) = 'н') AS emerg,
	ds.department_id as depart
FROM case_services cs
	LEFT JOIN vmu_profile vp ON vp.id_profile = cs.id_profile
	LEFT JOIN vmu_diagnosis vd ON cs.id_diagnosis = vd.id_diagnosis
	LEFT JOIN doctor_spec ds ON cs.id_doctor = ds.doctor_id
	LEFT JOIN users ON ds.user_id = users.id
	LEFT JOIN fio ON users.id = fio.id
	LEFT JOIN departments dep ON ds.department_id = dep.id
	LEFT JOIN cases ON cs.id_case = cases.id_case
	LEFT JOIN patient_data pd ON cases.id_patient = pd.id_patient
	LEFT JOIN (
		SELECT vt.uet, vt.PRICE,
		vt.id_profile,
		vt.id_zone_type,
		vt.tariff_begin_date AS d1,
		vt.tariff_end_date AS d2,
		vtp.tp_begin_date AS d3,
		vtp.tp_end_date AS d4
		FROM vmu_tariff vt, vmu_tariff_plan vtp
		WHERE vtp.id_tariff_group = vt.id_lpu
		AND vtp.id_lpu in ( SELECT id_lpu FROM mu_ident)) T
			ON T.id_profile = vp.id_profile AND
			cs.date_begin BETWEEN T.d1 AND T.d2 AND
			cs.date_begin BETWEEN T.d3 AND T.d4 AND
			T.id_zone_type = cs.id_net_profile AND
			cs.is_oms = 1
	WHERE cs.date_begin BETWEEN @date_start AND @date_end
	AND ds.spec_id in (108, 211)
	AND ds.department_id in (3)
	AND cs.is_oms = 1);

CREATE TEMPORARY TABLE if NOT EXISTS first_row
SELECT "Хирургия взрослая" AS subsection, 108 AS specialty, 0 AS child_f, 0 AS emerg_f,3 as depf  UNION
SELECT "Хирургия детская" AS subsection, 108 AS specialty, 1 AS child_f, 0 AS emerg_f, 3 as depf UNION
SELECT "Хирургия неотложная" AS subsection, 108 AS specialty, 1 AS child_f, 1 AS emerg_f,3 as depf UNION
SELECT "Хирургия неотложная" AS subsection, 108 AS specialty, 0 AS child_f, 1 AS emerg_f,3 as depf UNION
SELECT "Пародонтология" AS subsection, 211 AS specialty, 0 AS child_f, 0 AS emerg_f,3 as depf UNION
SELECT "Пародонтология" AS subsection, 211 AS specialty, 0 AS child_f, 1 AS emerg_f,3 as depf UNION
SELECT "Пародонтология" AS subsection, 211 AS specialty, 1 AS child_f, 0 AS emerg_f,3 as depf UNION
SELECT "Пародонтология" AS subsection, 211 AS specialty, 1 AS child_f, 1 AS emerg_f,3 as depf;

CREATE TEMPORARY TABLE if NOT EXISTS doctors_2
SELECT *
FROM doctors d
LEFT JOIN first_row fr ON fr.specialty = d.spec_id AND fr.child_f = d.child AND fr.emerg_f = d.emerg and fr.depf = d.dep;
CREATE TEMPORARY TABLE if NOT EXISTS T1 (
SELECT id_doctor, subsection, COUNT(profile_infis_code) AS all_p
FROM doctors_2
WHERE profile_infis_code in ('стх001', 'стх002', 'нстх001', 'нстх002', 'стт005', 'стт006', 'стт007', 'стт008', 'стт009', 'стт010', 'стт011', 
'стт012', 'стт013', 'стт054', 'стт056', 'стх001', 'стх002')
GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE if NOT EXISTS T2 (
SELECT id_doctor, subsection, COUNT(profile_infis_code) AS first_p
FROM doctors_2
WHERE profile_infis_code in ('стх001', 'нстх001', 'стт005', 'стт007', 'стт008', 'стт009', 'стт010', 'стт011', 'стт012', 'стт013', 'стт054', 'стт056', 'стх001')
GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE if NOT EXISTS T3 (
SELECT id_doctor, subsection, COUNT(profile_infis_code) AS qw
FROM doctors_2
WHERE profile_infis_code in ('стх029', 'нстх029')
GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE if NOT EXISTS T4 (
SELECT id_doctor, subsection, COUNT(profile_infis_code) AS qw
FROM doctors_2
WHERE profile_infis_code in ('стх030', 'нстх030')
GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE if NOT EXISTS T5 (
SELECT id_doctor, subsection, COUNT(profile_infis_code) AS qw
FROM doctors_2
WHERE profile_infis_code in ('стх031', 'нстх031')
GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE if NOT EXISTS T6 (
SELECT id_doctor, subsection, COUNT(profile_infis_code) AS qw
FROM doctors_2
WHERE profile_infis_code in ('стх034')
GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE if NOT EXISTS T7 (
SELECT id_doctor, subsection, COUNT(profile_infis_code) AS qw
FROM doctors_2
WHERE profile_infis_code in ('стх029', 'стх030', 'стх031', 'стх034', 'нстх029', 'нстх030', 'нстх031', 'стх034')
GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE if NOT EXISTS T8 (
SELECT id_doctor, subsection, COUNT(profile_infis_code) AS qw
FROM doctors_2
WHERE profile_infis_code in ('стх048', 'стх033', 'стх033', 'стх048')
GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE if NOT EXISTS T9 (
SELECT id_doctor, subsection, COUNT(profile_infis_code) AS qw
FROM doctors_2
WHERE profile_infis_code in ('стх028')
GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE if NOT EXISTS T10 (
SELECT id_doctor, subsection, COUNT(profile_infis_code) AS qw
FROM doctors_2
WHERE profile_infis_code in ('стт047', 'стх050')
GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE if NOT EXISTS T11 (
SELECT id_doctor, subsection, COUNT(profile_infis_code) AS qw
FROM doctors_2
WHERE profile_infis_code in ('стх047')
GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE if NOT EXISTS T12 (
SELECT id_doctor, subsection, COUNT(profile_infis_code) AS qw
FROM doctors_2
WHERE profile_infis_code in ('стх019', 'стх021', 'стх022', 'стх023', 'стх026', 'стх039', 'стх045', 'стх046', 'стх049', 'стх051', 'нстх019', 'нстх026', 'нстх046')
GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE if NOT EXISTS T13 (
SELECT id_doctor, subsection, COUNT(profile_infis_code) AS qw
FROM doctors_2
WHERE profile_infis_code in ('стх048', 'стх033', 'стх033', 'стх048', 'стх028', 'стт047', 'стх050', 'стх019', 'стх021', 'стх022', 'стх023', 
'стх026', 'стх039', 'стх045', 'стх046', 'стх049', 'стх051', 'нстх019', 'нстх026', 'нстх046')
GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE if NOT EXISTS T14 (
SELECT id_doctor, subsection, COUNT(profile_infis_code) AS qw
FROM doctors_2
WHERE profile_infis_code in ('стх015', 'стх016', 'стх018', 'нстх015', 'нстх016', 'нстх018')
GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE if NOT EXISTS T15 (
SELECT id_doctor, subsection, COUNT(profile_infis_code) AS qw
FROM doctors_2
WHERE profile_infis_code in ('сто002', 'сто003', 'нсто002', 'нсто003')
GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE if NOT EXISTS T16 (
SELECT id_doctor, subsection, COUNT(profile_infis_code) AS qw
FROM doctors_2
WHERE profile_infis_code in ('нстт041', 'стт041')
GROUP BY id_doctor, subsection);

CREATE TEMPORARY TABLE IF NOT EXISTS T17 (
	                                        SELECT id_doctor, subsection, count(diagnosis_code) AS qw
		                                        FROM doctors_2
		                                        WHERE diagnosis_code IN
		                                              ('K02.0', 'K02.1', 'K02.3', 'K02.8', 'K04.0', 'K04.1', 'K04.4',
		                                               'K04.5')
			                                      AND profile_infis_code IN ('стт025', 'стт028', 'стт031')
		                                        GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE IF NOT EXISTS T18 (
	                                        SELECT id_doctor, subsection, count(diagnosis_code) AS qw
		                                        FROM doctors_2
		                                        WHERE diagnosis_code IN ('K02.0', 'K02.1', 'K02.3', 'K02.8')
			                                      AND profile_infis_code IN ('стт025', 'стт028', 'стт031')
		                                        GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE IF NOT EXISTS T19 (
	                                        SELECT id_doctor, subsection, count(diagnosis_code) AS qw
		                                        FROM doctors_2
		                                        WHERE diagnosis_code IN ('K04.0', 'K04.1', 'K04.4', 'K04.5')
			                                      AND profile_infis_code IN ('стт025', 'стт028', 'стт031')
		                                        GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE IF NOT EXISTS T20 (
	                                        SELECT id_doctor, subsection, count(diagnosis_code) AS qw
		                                        FROM doctors_2
		                                        WHERE diagnosis_code IN ('K04.0', 'K04.1')
			                                      AND profile_infis_code IN ('стт025', 'стт028', 'стт031')
		                                        GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE IF NOT EXISTS T21 (
	                                        SELECT id_doctor, subsection, count(profile_infis_code) AS qw
		                                        FROM doctors_2
		                                        WHERE profile_infis_code IN ('стт039')
		                                        GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE IF NOT EXISTS T22 (
	                                        SELECT id_doctor, subsection, count(diagnosis_code) AS qw
		                                        FROM doctors_2
		                                        WHERE diagnosis_code IN ('K04.4', 'K04.5')
			                                      AND profile_infis_code IN ('стт025', 'стт028', 'стт031')
		                                        GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE IF NOT EXISTS T23 (
	                                         SELECT id_doctor, subsection, count(profile_infis_code) AS qw
		                                         FROM doctors_2
		                                         WHERE profile_infis_code IN ('стт046')
		                                         GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE if NOT EXISTS T24 (
SELECT id_doctor, subsection, SUM(uet) AS s_uet
FROM doctors_2
GROUP BY id_doctor, subsection);
CREATE TEMPORARY TABLE if NOT EXISTS T25 (
SELECT id_doctor, subsection, SUM(price) AS summ
FROM doctors_2
GROUP BY id_doctor, subsection);

SELECT doc.subsection, doc.short_name, 
COALESCE(T1.all_p,0), COALESCE(T2.first_p,0),
COALESCE(T3.qw,0), COALESCE(T4.qw,0), COALESCE(T5.qw,0),
COALESCE(T6.qw,0), COALESCE(T7.qw,0), COALESCE(T8.qw,0),
COALESCE(T9.qw,0), COALESCE(T10.qw,0), COALESCE(T11.qw,0),
COALESCE(T12.qw,0),COALESCE(T13.qw,0), COALESCE(T14.qw,0),
COALESCE(T15.qw,0), COALESCE(T16.qw,0), COALESCE(T17.qw,0),
COALESCE(T18.qw,0), COALESCE(T19.qw,0), COALESCE(T20.qw,0),
COALESCE(T21.qw,0), COALESCE(T22.qw,0), COALESCE(T23.qw,0),
ROUND(COALESCE(T24.s_uet,0), 2) AS uets,
ROUND(COALESCE(T25.summ,0), 2) AS summ
FROM doctors_2 doc
LEFT JOIN T1 ON doc.id_doctor = T1.id_doctor AND doc.subsection = T1.subsection
LEFT JOIN T2 ON doc.id_doctor = T2.id_doctor AND doc.subsection = T2.subsection
LEFT JOIN T3 ON doc.id_doctor = T3.id_doctor AND doc.subsection = T3.subsection
LEFT JOIN T4 ON doc.id_doctor = T4.id_doctor AND doc.subsection = T4.subsection
LEFT JOIN T5 ON doc.id_doctor = T5.id_doctor AND doc.subsection = T5.subsection
LEFT JOIN T6 ON doc.id_doctor = T6.id_doctor AND doc.subsection = T6.subsection
LEFT JOIN T7 ON doc.id_doctor = T7.id_doctor AND doc.subsection = T7.subsection
LEFT JOIN T8 ON doc.id_doctor = T8.id_doctor AND doc.subsection = T8.subsection
LEFT JOIN T9 ON doc.id_doctor = T9.id_doctor AND doc.subsection = T9.subsection
LEFT JOIN T10 ON doc.id_doctor = T10.id_doctor AND doc.subsection = T10.subsection
LEFT JOIN T11 ON doc.id_doctor = T11.id_doctor AND doc.subsection = T11.subsection
LEFT JOIN T12 ON doc.id_doctor = T12.id_doctor AND doc.subsection = T12.subsection
LEFT JOIN T13 ON doc.id_doctor = T13.id_doctor AND doc.subsection = T13.subsection
LEFT JOIN T14 ON doc.id_doctor = T14.id_doctor AND doc.subsection = T14.subsection
LEFT JOIN T15 ON doc.id_doctor = T15.id_doctor AND doc.subsection = T15.subsection
LEFT JOIN T16 ON doc.id_doctor = T16.id_doctor AND doc.subsection = T16.subsection
LEFT JOIN T17 ON doc.id_doctor = T17.id_doctor AND doc.subsection = T17.subsection
LEFT JOIN T18 ON doc.id_doctor = T18.id_doctor AND doc.subsection = T18.subsection
LEFT JOIN T19 ON doc.id_doctor = T19.id_doctor AND doc.subsection = T19.subsection
LEFT JOIN T20 ON doc.id_doctor = T20.id_doctor AND doc.subsection = T20.subsection
LEFT JOIN T21 ON doc.id_doctor = T21.id_doctor AND doc.subsection = T21.subsection
LEFT JOIN T22 ON doc.id_doctor = T22.id_doctor AND doc.subsection = T22.subsection
LEFT JOIN T23 ON doc.id_doctor = T23.id_doctor AND doc.subsection = T23.subsection
LEFT JOIN T24 ON doc.id_doctor = T24.id_doctor AND doc.subsection = T24.subsection
LEFT JOIN T25 ON doc.id_doctor = T25.id_doctor AND doc.subsection = T25.subsection
GROUP BY doc.subsection, doc.id_doctor;