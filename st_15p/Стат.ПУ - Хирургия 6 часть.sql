CREATE TEMPORARY TABLE if NOT EXISTS fio (
SELECT id, CONCAT(SUBSTRING_INDEX(name, ' ', 1), ' ',
LEFT(SUBSTRING_INDEX(SUBSTRING_INDEX(name, ' ', 2), ' ', -1), 1), '. ',
LEFT(SUBSTRING_INDEX(SUBSTRING_INDEX(name, ' ', -1), ' ', -1), 1), '.') AS short_name
FROM users);

CREATE TEMPORARY TABLE if NOT EXISTS t123 (
SELECT fio.short_name, cs.id_doctor,
cs.ID_CASE, cs.DATE_BEGIN,
cs.ID_PROFILE, p.code, ds.spec_id, pd.id_human,
if (spec_id=108, 'ХИРУРГИЯ', 'ПАРОДОНТОЛОГИЯ') AS depname,
dep.id AS depid, DATE_ADD(pd.birthday, INTERVAL 18 YEAR) > c.date_begin AS child
FROM case_services cs
LEFT JOIN price p ON p.id = cs.id_profile
LEFT JOIN doctor_spec ds ON cs.id_doctor = ds.doctor_id
LEFT JOIN users ON ds.user_id = users.id
LEFT JOIN departments dep ON ds.department_id = dep.id
LEFT JOIN fio ON users.id = fio.id
LEFT JOIN cases c ON c.id_case = cs.id_case
LEFT JOIN patient_data pd ON pd.id_patient = c.id_patient
WHERE cs.date_begin BETWEEN @date_start AND @date_end
AND (dep.id = @depart_id OR @depart_id = 0 OR @depart_id IS NULL)
AND ds.spec_id in (211, 108) AND cs.is_oms in (0,3));

CREATE TEMPORARY TABLE if NOT EXISTS T1 (
SELECT id_doctor, COUNT(code) AS COUNT
FROM t123
WHERE code in (10310)
GROUP BY id_doctor);

CREATE TEMPORARY TABLE if NOT EXISTS T2 (
SELECT id_doctor, COUNT(code) AS COUNT
FROM t123
WHERE code in (10315)
GROUP BY id_doctor);

CREATE TEMPORARY TABLE if NOT EXISTS T3 (
SELECT id_doctor, COUNT(code) AS COUNT
FROM t123
WHERE code in (10320)
GROUP BY id_doctor);

CREATE TEMPORARY TABLE if NOT EXISTS T4 (
SELECT id_doctor, COUNT(code) AS COUNT
FROM t123
WHERE code in (10470)
GROUP BY id_doctor);

CREATE TEMPORARY TABLE if NOT EXISTS T5 (
SELECT id_doctor, COUNT(code) AS COUNT
FROM t123
WHERE code in (10480)
GROUP BY id_doctor);

CREATE TEMPORARY TABLE if NOT EXISTS docs
SELECT depname,short_name, id_doctor
FROM t123
GROUP BY id_doctor;

SELECT 
 depname,
 short_name, COALESCE(t1.count, 0), COALESCE(t2.count, 0), COALESCE(t3.count, 0),
	COALESCE(t1.count, 0) + COALESCE(t2.count, 0) + COALESCE(t3.count, 0),
	COALESCE(t4.count, 0), COALESCE(t5.count, 0),
	COALESCE(t4.count, 0) + COALESCE(t5.count, 0)
FROM
 docs
LEFT JOIN
 t1 ON docs.id_doctor = t1.id_doctor
LEFT JOIN
 t2 ON docs.id_doctor = t2.id_doctor
LEFT JOIN
 t3 ON docs.id_doctor = t3.id_doctor
LEFT JOIN
 t4 ON docs.id_doctor = t4.id_doctor
LEFT JOIN
 t5 ON docs.id_doctor = t5.id_doctor
ORDER BY depname, short_name