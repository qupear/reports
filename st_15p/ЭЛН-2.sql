DROP TEMPORARY TABLE IF EXISTS subtemp
;

CREATE TEMPORARY TABLE IF NOT EXISTS subtemp AS
  ( SELECT DISTINCT
      e.eln_number nomer, concat(e.pat_surname,' ',e.pat_name,' ',e.pat_second_name) fio, timestampdiff(YEAR,e.pat_birthday,CURRENT_DATE) AS `vozr`, CASE
        WHEN lpu_name<>'��� ���� "����������������� ����������� �9"'
          THEN et.date_begin
          ELSE e.eln_date
      END AS date_sozdan, CASE
        WHEN lpu_name<>'��� ���� "����������������� ����������� �15"'
          THEN et.doc_name
          ELSE short_name(e.doctor_name)
      END AS doc, CASE
        WHEN e.lpu_name LIKE '%��������� ��������%'
          THEN REPLACE(e.lpu_name,"��������� ��������","��")
        WHEN e.lpu_name LIKE '%����������������� �����������%'
          THEN REPLACE(e.lpu_name,"����������������� �����������","��")
          ELSE e.lpu_name
      END AS `lpuname`, CASE
        WHEN dis.id>0
          THEN '�������'
          ELSE es.name
      END AS status, short_name(u.name) fio_redakt, concat('� ',et.date_begin,' �� ',et.date_end) AS `period`, et.date_end AS dateend, CASE
        WHEN es.code='010'
          THEN 10
        WHEN es.code='020'
          THEN 20
        WHEN es.code='030'
          THEN 30
        WHEN es.code='090'
          THEN 90
      END AS `escode`, CASE
        WHEN es.code='030'
          THEN 1
          ELSE 0
      END AS `zakrit`
    FROM
      eln                           e
      JOIN eln_states               es  ON e.eln_state=es.id
      LEFT JOIN eln_disable_reasons dis ON dis.id     =e.disable_reason
      LEFT JOIN eln_treats          et  ON et.id_eln  =e.id
      LEFT JOIN users               u   ON u.id       =e.user_modify
    WHERE
      ((e.eln_date         >=@date_start
          AND e.eln_date   <=@date_end)
        OR(et.date_begin   >=@date_start
          AND et.date_begin<=@date_end))
    ORDER BY
      fio, nomer
  )
;

SET @zakritih=0;
SELECT
  count(DISTINCT nomer)
INTO
  @zakritih
FROM
  subtemp
WHERE zakrit=1
AND dateend<=date_sozdan
;

SELECT
  outerser.nomer, outerser.fio    , outerser.vozr      , outerser.date_sozdan
, outerser.doc  , outerser.lpuname, outerser.fio_redakt, outerser.period
, CASE
    WHEN outerser.escode=10
      THEN '������'
    WHEN outerser.escode=20
      THEN '������'
    WHEN outerser.escode=30
      THEN '������'
    WHEN outerser.escode=90
      THEN '�������� ����������'
  END AS `status`
FROM
  ( SELECT
      subsel.nomer       AS `nomer`, subsel.fio AS `fio`        , subsel.vozr AS `vozr`            , subsel.date_sozdan AS `date_sozdan`
    , subsel.doc         AS `doc`  , subsel.lpuname AS `lpuname`, subsel.fio_redakt AS `fio_redakt`, subsel.period AS `period`
    , MAX(subsel.escode) AS `escode`
    FROM
      ( SELECT *
        FROM
          subtemp) AS `subsel`
    GROUP BY
      subsel.nomer, subsel.fio    , subsel.vozr      , subsel.date_sozdan
    , subsel.doc  , subsel.lpuname, subsel.fio_redakt, subsel.period) AS `outerser`
UNION
SELECT
  '','',''      ,''
,'' ,'','����� ','�������� :'
, @zakritih
;