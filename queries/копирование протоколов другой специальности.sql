set @offs = 1000000;
select concat(
               'insert into ref_protocols (id, parent_id, id_spec, id_diag, id_razd, id_text, text, by_tooth, sort_id) values (',
               id + @offs, ',', parent_id + @offs, ',', 9, ',', id_diag, ',', id_razd, ',', id_text, ','', text, '',',
               by_tooth, ',', coalesce(sort_id, 0), ')') as txt
-- после добавления строк необходимо изменить id_diag, parent_id на 0 (для разделов), возможно придется снять активность разделов, т.к. они созданы для всей специальности
/*добавление разделов диагнозов*/
set @offs = 105000;
set @id_diag =127;
select concat(
               'insert into ref_protocols (id, parent_id, id_spec, id_diag, id_razd, id_text, text, profile_oms, mkd_diagn, by_tooth, sort_id) values (',
               @offs:=@offs+10, ',', 0, ',', id_spec, ',', @id_diag, ',', id_razd, ',', id_text, ',', '"', text, '"', ',', '"', coalesce(profile_oms, ''),
 '"', ',', '"', coalesce(mkb_diagn, ''),  '"', ',',
               by_tooth, ',', coalesce(sort_id, 0), ')', ';') as txt
from ref_protocols where id_diag = 1 and parent_id = 0 and is_active =1