select dep.name depart, u.name user, count(distinct ap.case_id) sl, count(distinct p.id_case) prot from appointments ap
    left join protocols p on p.id_case=ap.case_id
    left join doctor_spec ds on ds.doctor_id=ap.doctor_id
    left join departments dep on dep.id = ds.department_id
    left join users u on u.id=ds.user_id
where Date(ap.time) >= @date_start AND Date(ap.time) <= @date_end
  and ap.case_id>-1 group by u.name order by depart, user;
commit

