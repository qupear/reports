set @row = 0;
SELECT @row := @row + 1, T.date_value, T.s1, T.s2, T.s3, T.s4, T.s5, T.s6, T.s7, T.s8
FROM (SELECT atos.date_value, COALESCE(T1.summ, 0) as s1, COALESCE(T2.summ, 0) as s2, COALESCE(T3.summ, 0) as s3,
             COALESCE(T4.summ, 0) as s4, COALESCE(T5.summ, 0) as s5,
             COALESCE(T6.summ, 0) as s6, COALESCE(T7.summ, 0) as s7, COALESCE(T8.uet, 0) as s8
      from aggregated_table_of_statistics atos
               LEFT JOIN
           (select count(T11.id_case) as summ, T11.date_begin as date_value
            from (select cs.id_case, cs.date_begin
                  from case_services cs
                  where cs.id_doctor = @doc_id
                    and cs.date_begin between @date_start and @date_end
                    AND cs.is_oms = 0
                  group by cs.id_case) T11
            group by T11.date_begin) T1 on T1.date_value = atos.date_value
               LEFT JOIN
           (SELECT date_value, sum(amount) as summ
            FROM aggregated_table_of_statistics
            where id_doctor = @doc_id
              and date_value between @date_start and @date_end
              and id_sourse = 2
              and name in ('7001')
            group by date_value) T2 on T2.date_value = atos.date_value
               LEFT JOIN
           (SELECT date_value, sum(amount) as summ
            FROM aggregated_table_of_statistics
            where id_doctor = @doc_id
              and date_value between @date_start and @date_end
              and id_sourse = 2
              and name in ('7005', '7006')
            group by date_value) T3 on T3.date_value = atos.date_value
               LEFT JOIN
           (SELECT date_value, sum(amount) as summ
            FROM aggregated_table_of_statistics
            where id_doctor = @doc_id
              and date_value between @date_start and @date_end
              and id_sourse = 2
              and name in ('7007')
            group by date_value) T4 on T4.date_value = atos.date_value
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 2
                            and name in ('7008')
                          group by date_value) T5 ON T5.date_value = atos.date_value
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 2
                            and name in ('7009')
                          group by date_value) T6 ON T6.date_value = atos.date_value
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 2
                            and name in ('7011')
                          group by date_value) T7 ON T7.date_value = atos.date_value
               LEFT JOIN (SELECT tcs.date_begin as date_value, ROUND(COALESCE(SUM(tvp.UET), 0), 2) as uet
                          FROM case_services tcs
                                   LEFT JOIN (SELECT p.id, p.uet FROM price p) tvp ON tvp.id = tcs.id_profile
                          WHERE tcs.date_begin between @date_start AND @date_end
                            AND tcs.id_doctor = @doc_id
                            and tcs.is_oms = 0
                          GROUP BY tcs.date_begin) T8 ON T8.date_value = atos.date_value
      where atos.id_doctor = @doc_id
        and atos.date_value between @date_start and @date_end
      group by atos.date_value
      order by atos.date_value) T;








