set @date_start = '2019-04-27';
set @date_end = '2019-07-17';
set @row=0;
SELECT @row:=@row+1, T.* from
(SELECT case when NetUserPosition=2 then 'Call-центр' when NetUserPosition=4 then 'Портал'  else o.NetUserName end as iname,  count(o.NetUserPosition) as kolvo,
        case when NetUserPosition='портал' then 4 when NetUserPosition='оператор' then 2 else NetUserPosition end as itype
FROM on_record o where o.NetUserPosition is not null and o.id_appoint > 0 and o.date between @date_start and @date_end
group by itype
union all
select case when ap.author='d' then 'Доктор' when ap.author='r' then 'Регистратура' when ap.author='o' then 'Регистратура' end as iname,
 count(ap.id) as kolvo,
       case when ap.author='d' then 10 when ap.author='r' then 11 when ap.author='o' then 11 end as itype
from appointments ap
where date(ap.time) between @date_start and @date_end and ap.id_patient >-1 and ap.author <> 'i'
group by itype
order by itype) T