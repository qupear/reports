drop table if exists tmp;  set @row = 0;
create temporary table if not exists tmp (SELECT count(tmp1.id_case) as summ, tmp1.date_protocol
                                          FROM (select p.id_case, p.date_protocol, p.id_human
                                                from protocols p
                                                         left join (select min(orderCreateDate) as cure_start, id_human
                                                                    FROM orders
                                                                    WHERE order_type = 1
                                                                    group by id_human) ord on ord.id_human = p.id_human
                                                where p.id_doctor = @doc_id
                                                  and p.date_protocol between @date_start and @date_end
                                                  and ord.id_human is not null
                                                group by p.id_case) tmp1
                                          GROUP BY tmp1.date_protocol);
SELECT @row := @row + 1, T.date_value, T.s1, T.s2, T.s3, T.s4, T.s5, T.s6, T.s7, T.s8, T.s9, T.s10, T.s11, T.s12, T.s13,
       T.s15
FROM (SELECT tmp.date_protocol as date_value, COALESCE(T1.summ, 0) as s1,
             COALESCE(tmp.summ, 0) - COALESCE(T1.summ, 0) as s2, COALESCE(T3.summ, 0) as s3, COALESCE(T4.summ, 0) as s4,
             COALESCE(T5.summ, 0) as s5, COALESCE(T6.summ, 0) as s6, COALESCE(T7.summ, 0) as s7,
             COALESCE(T8.summ, 0) as s8, COALESCE(T9.summ, 0) as s9, COALESCE(T10.summ, 0) as s10,
             COALESCE(T11.summ, 0) as s11, COALESCE(T12.summ, 0) as s12, COALESCE(T13.summ, 0) as s13,
             COALESCE(T15.uet, 0) as s15
      from tmp
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 3
                            and name in ('1.1')
                          group by date_value) T1 on T1.date_value = tmp.date_protocol
               LEFT JOIN (select count(T11.id_case) as summ, T11.date_protocol as date_value
                          from (select p.id_case, p.date_protocol
                                from protocols p,
                                     orders o
                                where p.id_doctor = @doc_id
                                  and p.date_protocol between @date_start and @date_end
                                  AND p.id_human = o.id_human
                                  and o.order_type = 1
                                group by p.id_case) T11
                          group by T11.date_protocol) T2 on T2.date_value = tmp.date_protocol
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 3
                            and name in ('13.8')
                          group by date_value) T3 on T3.date_value = tmp.date_protocol
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 3
                            and name in ('13.2')
                          group by date_value) T4 on T4.date_value = tmp.date_protocol
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 3
                            and name in ('3.4', '3.5', '3.7', '5.3', '5.4')
                          group by date_value) T5 ON T5.date_value = tmp.date_protocol
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 3
                            and name in ('3.6')
                          group by date_value) T6 ON T6.date_value = tmp.date_protocol
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 3
                            and name in ('3.1', '3.2', '3.17', '5.1', '5.2')
                          group by date_value) T7 ON T7.date_value = tmp.date_protocol
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 3
                            and name in ('12.2')
                          group by date_value) T8 ON T8.date_value = tmp.date_protocol
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 3
                            and name in ('4.13')
                          group by date_value) T9 on T9.date_value = tmp.date_protocol
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 3
                            and name in ('15.3')
                          group by date_value) T10 on T10.date_value = tmp.date_protocol
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 3
                            and name in ('15.7')
                          group by date_value) T11 on T11.date_value = tmp.date_protocol
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 3
                            and name = '15.6'
                          group by date_value) T12 on T12.date_value = tmp.date_protocol
               LEFT JOIN (SELECT date_value, sum(amount) as summ
                          FROM aggregated_table_of_statistics
                          where id_doctor = @doc_id
                            and date_value between @date_start and @date_end
                            and id_sourse = 3
                            and name in ('15.5')
                          group by date_value) T13 on T13.date_value = tmp.date_protocol
               LEFT JOIN (SELECT tcs.date_begin as date_value, ROUND(COALESCE(SUM(tvp.UET), 0), 2) as uet
                          FROM case_services tcs
                                   LEFT JOIN (SELECT p.id, p.uet FROM price_orto_soc p) tvp ON tvp.id = tcs.id_profile
                          WHERE tcs.date_begin between @date_start AND @date_end
                            AND tcs.id_doctor = @doc_id
                            and tcs.is_oms = 0
                          GROUP BY tcs.date_begin) T15 ON T15.date_value = tmp.date_protocol
      group by tmp.date_protocol
      order by tmp.date_protocol) T;



