create temporary table if not exists pat_before (select distinct id_human from (select distinct pd.id_human from appointments a  left join patient_data pd on pd.id_patient = a.id_patient where a.id_patient > 0 and date(a.time) < @date_start UNION  select distinct pd.id_human from cases c left join patient_data pd on pd.id_patient = c.id_patient where c.date_begin < @date_start) T); create temporary table if not exists pat_now  (select distinct pd.id_human from appointments a  left join patient_data pd on pd.id_patient = a.id_patient where a.id_patient > 0 and date(a.time) between @date_start and @date_end); create temporary table if not exists pat_primary  (select pn.id_human from pat_now pn where pn.id_human not in (select id_human from pat_before)); create temporary table if not exists pat_primary_ids (select id_patient from patient_data where id_human in (select id_human from pat_primary)); create temporary table if not exists pat_docs   (select doctor_id, id_patient from appointments where id_patient > 0 and doctor_id > 0 and id_patient in (select id_patient from pat_primary_ids) group by doctor_id, id_patient);
SELECT d.name dept_name, u.name AS doc_name, CONCAT(pd.surname, ' ', pd.name, ' ', pd.SECOND_NAME) AS pat_name,
       app.visit_time, pd.card_number, pa.UNSTRUCT_ADDRESS, pd.REMARK, COALESCE(PAY.summ, 0)
FROM pat_docs p_d
         LEFT JOIN patient_data pd ON pd.id_patient = p_d.id_patient
         LEFT JOIN (SELECT pd.id_human, a.doctor_id, MIN(a.time) AS visit_time
                    FROM appointments a,
                         patient_data pd
                    WHERE date(a.time) BETWEEN @date_start AND @date_end
                      AND a.id_patient > 0
                      AND pd.id_patient = a.id_patient
                    group BY pd.id_human, a.doctor_id) app
                   ON app.id_human = pd.id_human and app.doctor_id = p_d.doctor_id
         LEFT JOIN doctor_spec ds ON ds.doctor_id = p_d.doctor_id
         LEFT JOIN departments d ON d.id = ds.department_id
         LEFT JOIN users u ON u.id = ds.user_id
         LEFT JOIN patient_address pa ON pd.ID_ADDRESS_REG = pa.ID_ADDRESS
         LEFT JOIN (SELECT p.doctor_id, SUM(p.summ) AS summ,
                           coalesce(pd.id_human, 0) + coalesce(ord.id_human, 0) as id_hum
                    FROM prepayments p
                             left join checks ch on ch.id = p.check
                             left join cases c on c.id_case = p.case_id and p.case_id > 0
                             left join orders ord on ord.id = p.order_id and p.order_id > 0
                             left join patient_data pd on pd.id_patient = c.id_patient and p.case_id > 0
                    WHERE ch.date BETWEEN @date_start AND @date_end
                    GROUP BY p.doctor_id, id_hum) PAY ON PAY.id_hum = pd.id_human and PAY.doctor_id = ds.doctor_id
WHERE pay.summ > 0
  AND ((ds.doctor_id = @doc_id) OR (@doc_id = 0))
  AND ((d.id = @depart_id) OR (@depart_id = 0))
ORDER BY d.name, u.name;