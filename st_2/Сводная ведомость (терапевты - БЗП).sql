create temporary table if not exists doctor_services (select cs.date_begin, cs.id_case, vp.profile_infis_code, p.code,
                                                             vd.diagnosis_code, cs.tooth_name, T.uet as uet_oms,
                                                             p.uet as uet_bzp, cs.is_oms
                                                      from case_services cs
                                                               left join vmu_profile vp on vp.id_profile = cs.id_profile and cs.is_oms = 1
                                                               left join price_orto_soc p on p.id = cs.id_profile and cs.is_oms = 2
                                                               left join vmu_diagnosis vd on cs.id_diagnosis = vd.id_diagnosis
                                                               left join cases c on c.id_case = cs.id_case
                                                               left join patient_data pd on pd.id_patient = c.id_patient
                                                               left join (select id_human, count(id) as bzp_ord_count
                                                                          from orders
                                                                          where order_type = 1
                                                                          group by id_human) ord
                                                                         on ord.id_human = pd.id_human
                                                               left join (select vt.uet, vt.id_profile, vt.id_zone_type,
                                                                                 vt.tariff_begin_date as d1,
                                                                                 vt.tariff_end_date as d2,
                                                                                 vtp.tp_begin_date as d3,
                                                                                 vtp.tp_end_date as d4
                                                                          from vmu_tariff vt,
                                                                               vmu_tariff_plan vtp
                                                                          where vtp.id_tariff_group = vt.id_lpu) T
                                                                         on T.id_profile = vp.id_profile and
                                                                            cs.date_begin between T.d1 and T.d2 and
                                                                            cs.date_begin between T.d3 and T.d4 and
                                                                            T.id_zone_type = cs.id_net_profile and
                                                                            cs.is_oms = 1
                                                      where cs.id_doctor = @doc_id
                                                        and cs.date_begin between @date_start and @date_end
                                                        and cs.is_oms in (1, 2)
                                                        and coalesce(ord.bzp_ord_count, 0) > 0);
create temporary table if not exists doctor_protocols (select ds.id_case, ds.date_begin,
                                                              locate('17239', p.child_ref_ids) > 0 as p17239,
                                                              locate('17251', p.child_ref_ids) > 0 as p17251,
                                                              locate('18720', p.child_ref_ids) > 0 as p18720,
                                                              locate('19010', p.child_ref_ids) > 0 as p19010,
                                                              locate('44170', p.child_ref_ids) > 0 as p44170,
                                                              locate('47780', p.child_ref_ids) > 0 as p47780,
                                                              locate('79760', p.child_ref_ids) > 0 as p79760,
                                                              locate('80370', p.child_ref_ids) > 0 as p80370,
                                                              locate('18730', p.child_ref_ids) > 0 as p18730,
                                                              locate('80380', p.child_ref_ids) > 0 as p80380,
                                                              locate('55791', p.child_ref_ids) > 0 as p55791,
                                                              locate(';5761', p.child_ref_ids) > 0 as p5761,
                                                              locate(';4781', p.child_ref_ids) > 0 as p4781,
                                                              locate(';3790', p.child_ref_ids) > 0 as p3790,
                                                              locate(';2350', p.child_ref_ids) > 0 as p2350,
                                                              locate(';1392', p.child_ref_ids) > 0 as p1392,
                                                              locate(';480', p.child_ref_ids) > 0 as p480
                                                       from doctor_services ds
                                                                left join protocols p on p.id_case = ds.id_case);
set @row = 0;
create temporary table if not exists first_row (select @row := @row + 1 as st_num, T.date_begin as st_date
                                                from (select date_begin
                                                      from doctor_services
                                                      group by date_begin
                                                      order by date_begin) T);
create temporary table if not exists T1 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T2 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where profile_infis_code in ('стт005')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T3 (select date_begin, count(distinct id_case) as val
                                         from doctor_protocols
                                         where p17239
                                            or p17251
                                            or p18720
                                            or p19010
                                            or p44170
                                            or p47780
                                            or p79760
                                            or p80370
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T4 (select date_begin, count(distinct id_case) as val
                                         from doctor_protocols
                                         where p18730
                                            or p80380
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T5
(select T.date_begin, count(T.val) as val
from (select date_begin, count(id_case) as val
      from doctor_services
      where code in ('2.5', '2.6', '2.7')
      group by id_case, tooth_name
      order by date_begin) T
group by date_begin);
create temporary table if not exists T6
(select T.date_begin, count(T.val) as val
from (select date_begin, count(id_case) as val
      from doctor_services
      where code in ('2.5', '2.6', '2.7')
        and diagnosis_code in
            ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5', 'k02.6', 'k02.7', 'k02.8', 'k02.9', 'k03.2')
      group by id_case, tooth_name
      order by date_begin) T
group by date_begin);
create temporary table if not exists T7
(select T.date_begin, count(T.val) as val
from (select date_begin, count(id_case) as val
      from doctor_services
      where code in ('2.5', '2.6', '2.7')
        and diagnosis_code in ('k04.0', 'k04.1', 'k04.2', 'k04.3')
      group by id_case, tooth_name
      order by date_begin) T
group by date_begin);
create temporary table if not exists T8
(select T.date_begin, count(T.val) as val
from (select date_begin, count(id_case) as val
      from doctor_services
      where code in ('2.5', '2.6', '2.7')
        and diagnosis_code in ('k04.4', 'k04.5')
      group by id_case, tooth_name
      order by date_begin) T
group by date_begin);
create temporary table if not exists T11 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where profile_infis_code in
                                                ('стт026', 'стт027', 'стт029', 'стт030', 'стт032', 'стт033', 'стт034',
                                                 'стт057', 'стт058')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T12 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('2.5', '2.6', '2.7')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T13 (select date_begin, count(distinct id_case) as val
                                          from doctor_protocols
                                          where p55791
                                             or p5761
                                             or p4781
                                             or p3790
                                             or p2350
                                             or p1392
                                             or p480
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T18 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('1.5', '1.6')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T19 (select date_begin, ROUND(COALESCE(SUM(UET_OMS), 0), 2) as val
                                          from doctor_services
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T20 (select date_begin, ROUND(COALESCE(SUM(UET_BZP), 0), 2) as val
                                          from doctor_services
                                          group by date_begin
                                          order by date_begin);
select fr.st_num, fr.st_date, coalesce(T1.val, 0), coalesce(T2.val, 0), 0, coalesce(T3.val, 0), coalesce(T4.val, 0),
       coalesce(T5.val, 0), coalesce(T6.val, 0), coalesce(T7.val, 0), coalesce(T8.val, 0),
       coalesce(T11.val, 0) + coalesce(T12.val, 0), coalesce(T11.val, 0), coalesce(T12.val, 0), coalesce(T13.val, 0),
       coalesce(T18.val, 0), coalesce(T19.val, 0) + coalesce(T20.val, 0), coalesce(T19.val, 0), coalesce(T20.val, 0)
from first_row fr
         left join T1 on T1.date_begin = fr.st_date
         left join T2 on T2.date_begin = fr.st_date
         left join T3 on T3.date_begin = fr.st_date
         left join T4 on T4.date_begin = fr.st_date
         left join T5 on T5.date_begin = fr.st_date
         left join T6 on T6.date_begin = fr.st_date
         left join T7 on T7.date_begin = fr.st_date
         left join T8 on T8.date_begin = fr.st_date
         left join T11 on T11.date_begin = fr.st_date
         left join T12 on T12.date_begin = fr.st_date
         left join T13 on T13.date_begin = fr.st_date
         left join T18 on T18.date_begin = fr.st_date
         left join T19 on T19.date_begin = fr.st_date
         left join T20 on T20.date_begin = fr.st_date
order by st_num ;  ;




















