create temporary table if not exists doctor_services
    (select cs.date_begin, cs.id_case, p.code, vd.diagnosis_code,
            cs.tooth_name, p.uet, cs.is_oms, pd.birthday
     from case_services cs
              left join price p on p.id = cs.id_profile and cs.is_oms in (0, 3, 4)
              left join vmu_diagnosis vd on cs.id_diagnosis = vd.id_diagnosis
              left join cases c on c.id_case = cs.id_case
              left join patient_data pd on pd.id_patient = c.id_patient
     where cs.id_doctor = @doc_id
       and cs.date_begin between @date_start and @date_end
       and cs.is_oms in (0, 3, 4));
create temporary table if not exists first_row
    (select date_begin as st_date, ROUND(COALESCE(SUM(UET),0), 2) as st_uet
    from doctor_services
    group by date_begin
    order by date_begin);
create temporary table if not exists T1 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T2 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where DATE_ADD(birthday, INTERVAL 15 year) > date_begin
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T3 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where DATE_ADD(birthday, INTERVAL 15 year) < date_begin
                                           and DATE_ADD(birthday, INTERVAL 18 year) > date_begin
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T4
(select T.date_begin, count(T.val) as val
from (select date_begin, count(id_case) as val
      from doctor_services
      where code in ('3001', '3003', '3005')
      group by id_case, tooth_name
      order by date_begin) T
group by date_begin);
create temporary table if not exists T5
(select T.date_begin, count(T.val) as val
from (select date_begin, count(id_case) as val
      from doctor_services
      where code in ('3001', '3003', '3005')
        and DATE_ADD(birthday, INTERVAL 15 year) > date_begin
      group by id_case, tooth_name
      order by date_begin) T
group by date_begin);
create temporary table if not exists T6
(select T.date_begin, count(T.val) as val
from (select date_begin, count(id_case) as val
      from doctor_services
      where code in ('3001', '3003', '3005')
        and DATE_ADD(birthday, INTERVAL 15 year) < date_begin
        and DATE_ADD(birthday, INTERVAL 18 year) > date_begin
      group by id_case, tooth_name
      order by date_begin) T
group by date_begin);
create temporary table if not exists T7
(select date_begin, count(id_case) as val
from doctor_services
where code in ('3009', '3010', '3011', '3015', '3016', '3017', '3019', '3020', '3012', '3013', '3014')
group by date_begin);
create temporary table if not exists T8
(select date_begin, count(id_case) as val
from doctor_services
where code in ('3009', '3010', '3011', '3015', '3016', '3017', '3019', '3020', '3012', '3013', '3014')
  and DATE_ADD(birthday, INTERVAL 18 year) <= date_begin
group by date_begin);
create temporary table if not exists T9
(select date_begin, count(id_case) as val
from doctor_services
where code in ('3009', '3010', '3011', '3015', '3016', '3017', '3019', '3020', '3012', '3013', '3014')
  and DATE_ADD(birthday, INTERVAL 15 year) > date_begin
group by date_begin);
create temporary table if not exists T10
(select date_begin, count(id_case) as val
from doctor_services
where code in ('3009', '3010', '3011', '3015', '3016', '3017', '3019', '3020', '3012', '3013', '3014')
  and DATE_ADD(birthday, INTERVAL 15 year) > date_begin
group by date_begin);
create temporary table if not exists T11
(select date_begin, count(id_case) as val
from doctor_services
where code in ('3009', '3010', '3011', '3015', '3016', '3017', '3019', '3020', '3012', '3013', '3014')
  and DATE_ADD(birthday, INTERVAL 15 year) < date_begin
  and DATE_ADD(birthday, INTERVAL 18 year) > date_begin
group by date_begin);
create temporary table if not exists T12
(select date_begin, count(id_case) as val
from doctor_services
where code in ('3009', '3010', '3011')
group by date_begin);
create temporary table if not exists T13
(select date_begin, count(id_case) as val
from doctor_services
where code in ('3009', '3010', '3011')
  and DATE_ADD(birthday, INTERVAL 15 year) > date_begin
group by date_begin);
create temporary table if not exists T14 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3015', '3016', '3017', '3019', '3020')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T15 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3015', '3016', '3017', '3019', '3020')
                                            and DATE_ADD(birthday, INTERVAL 18 year) <= date_begin
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T16 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3015', '3016', '3017', '3019', '3020')
                                            and DATE_ADD(birthday, INTERVAL 18 year) > date_begin
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T17 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3012', '3013', '3014')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T18 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3012', '3013', '3014')
                                            and DATE_ADD(birthday, INTERVAL 18 year) <= date_begin
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T19 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3012', '3013', '3014')
                                            and DATE_ADD(birthday, INTERVAL 18 year) > date_begin
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T20 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3007', '3008', '30081')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T21 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3007', '3008', '30081')
                                            and DATE_ADD(birthday, INTERVAL 18 year) <= date_begin
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T22 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3007', '3008', '30081')
                                            and DATE_ADD(birthday, INTERVAL 18 year) > date_begin
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T23 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3023', '3024', '3025')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T24 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3031', '3032', '3033', '3079')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T25 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3034', '3035', '3036', '3037', '3038', '3039')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T26 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3040', '3041', '3042')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T27 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3043', '3044', '3045')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T28 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3046', '3047', '3048')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T29 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3049', '3050', '3051')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T30 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3053', '3054', '3055')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T31 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3058')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T32 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3056')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T33 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3057')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T34 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3064', '3065', '3066')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T35 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3069', '3070', '3071')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T36 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3072', '3080', '3081', '5013', '5014', '5015', '5016', '5032')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T37 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3073', '3074', '3075')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T38 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3082')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T39 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('3090')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T40 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('5001', '5002', '5003')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T41 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('5017')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T42 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in
                                                ('5018', '5019', '5020', '5021', '5022', '3083', '3084', '3085', '3086',
                                                 '3087', '3089')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T43 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in
                                                ('5034', '5035', '3060', '3061', '3063', '3089', '30631', '30601',
                                                 '3093', '3094', '3095', '3096')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T44 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in
                                                ('5004', '5005', '5006', '5007', '5008', '5009', '5010', '5012', '5011',
                                                 '5029', '5030', '5031', '3088')
                                          group by date_begin
                                          order by date_begin);
create temporary table tmp_1_21
(select fr.st_date, coalesce(T1.val, 0) as v1, coalesce(T2.val, 0) as v2, coalesce(T3.val, 0) as v3,
       coalesce(T4.val, 0) as v4, coalesce(T5.val, 0) as v5, coalesce(T6.val, 0) as v6, coalesce(T7.val, 0) as v7,
       coalesce(T8.val, 0) as v8, coalesce(T9.val, 0) as v9, coalesce(T10.val, 0) as v10, coalesce(T11.val, 0) as v11,
       coalesce(T12.val, 0) as v12, coalesce(T13.val, 0) as v13, coalesce(T14.val, 0) as v14,
       coalesce(T15.val, 0) as v15, coalesce(T16.val, 0) as v16, coalesce(T17.val, 0) as v17,
       coalesce(T18.val, 0) as v18, coalesce(T19.val, 0) as v19, coalesce(T20.val, 0) as v20,
       coalesce(T21.val, 0) as v21, fr.st_uet
from first_row fr
         left join T1 on T1.date_begin = fr.st_date
         left join T2 on T2.date_begin = fr.st_date
         left join T3 on T3.date_begin = fr.st_date
         left join T4 on T4.date_begin = fr.st_date
         left join T5 on T5.date_begin = fr.st_date
         left join T6 on T6.date_begin = fr.st_date
         left join T7 on T7.date_begin = fr.st_date
         left join T8 on T8.date_begin = fr.st_date
         left join T9 on T9.date_begin = fr.st_date
         left join T10 on T10.date_begin = fr.st_date
         left join T11 on T11.date_begin = fr.st_date
         left join T12 on T12.date_begin = fr.st_date
         left join T13 on T13.date_begin = fr.st_date
         left join T14 on T14.date_begin = fr.st_date
         left join T15 on T15.date_begin = fr.st_date
         left join T16 on T16.date_begin = fr.st_date
         left join T17 on T17.date_begin = fr.st_date
         left join T18 on T18.date_begin = fr.st_date
         left join T19 on T19.date_begin = fr.st_date
         left join T20 on T20.date_begin = fr.st_date
         left join T21 on T21.date_begin = fr.st_date) ;
create temporary table tmp_22_42
(select tt.st_date, tt.v1, tt.v2, tt.v3, tt.v4, tt.v5, tt.v6, tt.v7, tt.v8, tt.v9, tt.v10, tt.v11, tt.v12, tt.v13,
       tt.v14, tt.v15, tt.v16, tt.v17, tt.v18, tt.v19, tt.v20, tt.v21, coalesce(T22.val, 0), coalesce(T23.val, 0),
       coalesce(T24.val, 0), coalesce(T25.val, 0), coalesce(T26.val, 0), coalesce(T27.val, 0), coalesce(T28.val, 0),
       coalesce(T29.val, 0), coalesce(T30.val, 0), coalesce(T31.val, 0), coalesce(T32.val, 0), coalesce(T33.val, 0),
       coalesce(T34.val, 0), coalesce(T35.val, 0), coalesce(T36.val, 0), coalesce(T37.val, 0), coalesce(T38.val, 0),
       coalesce(T39.val, 0), coalesce(T40.val, 0), coalesce(T41.val, 0), coalesce(T42.val, 0), coalesce(T43.val, 0),
       coalesce(T44.val, 0), tt.st_uet
from tmp_1_21 tt
         left join T22 on T22.date_begin = tt.st_date
         left join T23 on T23.date_begin = tt.st_date
         left join T24 on T24.date_begin = tt.st_date
         left join T25 on T25.date_begin = tt.st_date
         left join T26 on T26.date_begin = tt.st_date
         left join T27 on T27.date_begin = tt.st_date
         left join T28 on T28.date_begin = tt.st_date
         left join T29 on T29.date_begin = tt.st_date
         left join T30 on T30.date_begin = tt.st_date
         left join T31 on T31.date_begin = tt.st_date
         left join T32 on T32.date_begin = tt.st_date
         left join T33 on T33.date_begin = tt.st_date
         left join T34 on T34.date_begin = tt.st_date
         left join T35 on T35.date_begin = tt.st_date
         left join T36 on T36.date_begin = tt.st_date
         left join T37 on T37.date_begin = tt.st_date
         left join T38 on T38.date_begin = tt.st_date
         left join T39 on T39.date_begin = tt.st_date
         left join T40 on T40.date_begin = tt.st_date
         left join T41 on T41.date_begin = tt.st_date
         left join T42 on T42.date_begin = tt.st_date
         left join T43 on T43.date_begin = tt.st_date
         left join T44 on T44.date_begin = tt.st_date) ;
select * from tmp_22_42 order by 1;





















































