select  u.name, count(distinct cs.case_id), count(distinct p.id_case) from appointments cs 
 left join protocols p on p.id_case=cs.case_id
 left join doctor_spec ds on ds.doctor_id=cs.doctor_id
 left join users u on u.id=ds.user_id  
  where Date(cs.time) >= @date_start AND Date(cs.time) <= @date_end  and cs.case_id>-1 group by u.name