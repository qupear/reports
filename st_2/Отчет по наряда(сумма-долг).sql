set @row = 0;
select @row := @row + 1, T.*
from (select Order_Number, concat_ws(' ', pd.SURNAME, pd.NAME, pd.SECOND_NAME) as pat, o.orderCreateDate as date,
             u.name, coalesce(sum(cs.SERVICE_COST * cs.discount), 0) as cost, coalesce(P.pay, 0),
             coalesce(sum(cs.SERVICE_COST * cs.discount), 0) - coalesce(P.pay, 0)
      from orders o
               left join patient_data pd on o.Id_Human = pd.ID_HUMAN
               left join doctor_spec ds on o.id_doctor = ds.doctor_id
               left join users u on ds.user_id = u.id
               left join case_services cs on o.ID = cs.ID_ORDER
               left join (select order_id, coalesce(sum(summ), 0) as pay from prepayments group by order_id) P
                         on o.ID = P.order_id
      where o.orderCreateDate between @date_start and @date_end
        and o.STATUS < 2
        and o.order_type = 0
      group by cs.ID_ORDER) T;


