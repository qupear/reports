set @row = 0;
SELECT @row := @row + 1, T_MAIN.time, T_MAIN.pat_name, T_MAIN.birthday, T_MAIN.prim, T_MAIN.secondary, T_MAIN.bzp,
       T_MAIN.diagnosis_code, T_MAIN.codes_oms, T_MAIN.codes_pu, T_MAIN.codes_bzp, '', roentgen,
       COALESCE(T_MAIN.phys, 0), COALESCE(T_MAIN.cons, 0), COALESCE(T_MAIN.stac, 0), T_MAIN.uet_oms, T_MAIN.uet_pu,
       T_MAIN.uet_bzp, T_MAIN.price_oms, T_MAIN.price_pu, T_MAIN.price_bzp
FROM (SELECT a.time, CONCAT(pd.surname, ' ', pd.name, ' ', pd.second_name) as pat_name, pd.birthday,
             count(cs.id_case) as prim, 1 - count(cs.id_case) as secondary, count(cs_bzp.id_case) > 0 as bzp,
             vd.diagnosis_code, COALESCE(T1.codes, '') as codes_oms, COALESCE(T2.codes, '') as codes_pu,
             COALESCE(T3.codes, '') as codes_bzp, (COALESCE(COUNT(d_roentgen.id), 0) > 0) as roentgen,
             prot_phys.phys as phys, prot_cons.cons as cons, prot_stac.stac as stac, ROUND(T1.uet, 2) as uet_oms,
             ROUND(T2.uet, 2) as uet_pu, ROUND(T3.uet, 2) as uet_bzp, T1.price as price_oms, T2.price as price_pu,
             T3.price as price_bzp
      FROM (SELECT id_case FROM case_services WHERE date_begin = @date_start AND id_doctor = @doc_id) CS_MAIN
               LEFT JOIN cases c ON c.id_case = CS_MAIN.id_case
               LEFT JOIN vmu_diagnosis vd ON vd.id_diagnosis = c.id_diagnosis
               LEFT JOIN patient_data pd ON pd.id_patient = c.id_patient
               LEFT JOIN appointments a ON a.case_id = c.id_case
               LEFT JOIN case_services cs ON cs.id_case = c.id_case AND cs.id_profile IN (select id_profile
                                                                                          from vmu_profile
                                                                                          where profile_infis_code = 'стт001'
                                                                                             or profile_infis_code = 'стт005')
               LEFT JOIN case_services cs_bzp ON cs_bzp.id_case = c.id_case AND cs_bzp.is_oms = 2
               LEFT JOIN (SELECT T11.id_case, T11.id_doctor,
                                 GROUP_CONCAT(CONCAT(T11.profile_infis_code, ' (', T11.cnt, ')') SEPARATOR
                                              ', ') as codes, SUM(T11.UET) as uet, sum(t11.price) as price
                          FROM (SELECT tcs.id_case, tcs.id_doctor, tcs.id_profile, count(tcs.id_profile) as cnt,
                                       tvp.profile_infis_code, COALESCE(SUM(tvp.UET), 0) as uet,
                                       COALESCE(SUM(tvp.price), 0) as price
                                FROM case_services tcs
                                         LEFT JOIN (SELECT ttvp.profile_infis_code, tvt.uet, tvt.price, ttvp.id_profile,
                                                           tvt.id_zone_type
                                                    FROM vmu_profile ttvp,
                                                         vmu_tariff tvt,
                                                         vmu_tariff_plan vtp
                                                    WHERE ttvp.id_profile = tvt.id_profile
                                                      and vtp.id_tariff_group = tvt.id_lpu
                                                      AND tvt.tariff_begin_date <= @date_start
                                                      AND tvt.tariff_end_date >= @date_start
                                                      and vtp.tp_begin_date <= @date_start
                                                      and vtp.tp_end_date >= @date_start
                                                      And vtp.id_lpu in (select id_lpu from mu_ident)) tvp
                                                   ON tvp.id_profile = tcs.id_profile and tcs.is_oms = 1 AND
                                                      tvp.id_zone_type = tcs.id_net_profile
                                WHERE tcs.date_begin = @date_start
                                  AND tcs.id_doctor = @doc_id
                                GROUP BY tcs.id_case, tcs.id_profile) T11
                          GROUP BY T11.id_case) T1 ON T1.id_case = c.id_case
               LEFT JOIN (SELECT T12.id_case, T12.id_doctor,
                                 GROUP_CONCAT(CONCAT(T12.code, ' (', T12.cnt, ')') SEPARATOR ', ') as codes,
                                 SUM(T12.UET) as uet, sum(t12.price) as price
                          FROM (SELECT tcs.id_case, tcs.id_doctor, tcs.id_profile, count(tcs.id_profile) as cnt,
                                       tp.code, COALESCE(SUM(tp.UET), 0) as uet, COALESCE(SUM(tp.cost), 0) as price
                                FROM case_services tcs
                                         LEFT JOIN price tp
                                                   ON tp.id = tcs.id_profile and tp.date_end = '2200-01-01' and tcs.is_oms = 0
                                WHERE tcs.date_begin = @date_start
                                  AND tcs.id_doctor = @doc_id
                                GROUP BY tcs.id_case, tcs.id_profile) T12
                          GROUP BY T12.id_case) T2 ON T2.id_case = c.id_case
               LEFT JOIN (SELECT T13.id_case, T13.id_doctor,
                                 GROUP_CONCAT(CONCAT(T13.code, ' (', T13.cnt, ')') SEPARATOR ', ') as codes,
                                 SUM(T13.UET) as uet, sum(t13.price) as price
                          FROM (SELECT tcs.id_case, tcs.id_doctor, tcs.id_profile, count(tcs.id_profile) as cnt,
                                       tpos.code, COALESCE(SUM(tpos.UET), 0) as uet,
                                       COALESCE(SUM(tpos.cost), 0) as price
                                FROM case_services tcs
                                         LEFT JOIN price_orto_soc tpos
                                                   ON tpos.id = tcs.id_profile and tpos.date_end = '2200-01-01' and
                                                      tcs.is_oms = 2
                                WHERE tcs.date_begin = @date_start
                                  AND tcs.id_doctor = @doc_id
                                GROUP BY tcs.id_case, tcs.id_profile) T13
                          GROUP BY T13.id_case) T3 ON T3.id_case = c.id_case
               LEFT JOIN directions d_roentgen ON d_roentgen.id_case = c.id_case and d_roentgen.id_direction = 2
               LEFT JOIN (SELECT id_case, count(id_ref) as phys, id_diag, id_doctor
                          FROM protocols p
                          where date_protocol = @date_start
                            and id_doctor = @doc_id
                            and (id_ref = 9111 or id_ref = 9112 or id_ref = 9113 or id_ref = 9114)
                          group by id_case) prot_phys ON prot_phys.id_case = c.id_case
               LEFT JOIN (SELECT id_case, count(id_ref) as cons, id_diag, id_doctor
                          FROM protocols p
                          where date_protocol = @date_start
                            and id_doctor = @doc_id
                            and (id_ref = 9110 or id_ref = 9100 or id_ref = 9090 or id_ref = 9080 or id_ref = 9070)
                          group by id_case) prot_cons ON prot_cons.id_case = c.id_case
               LEFT JOIN (SELECT id_case, count(id_ref) as stac, id_diag, id_doctor
                          FROM protocols p
                          where date_protocol = @date_start
                            and id_doctor = @doc_id
                            and (id_ref = 9060)
                          group by id_case) prot_stac ON prot_stac.id_case = c.id_case
      GROUP BY CS_MAIN.id_case
      order by a.time) T_MAIN;


