select u.name as doc_name, HOUR(sec_to_time(sum(time_to_sec(T1.shift_time))))
from (select a.doctor_id, DATE(a.time) as date_value, SUBTIME(TIME(MAX(a.time_end)), TIME(MIN(a.time))) as shift_time
      from appointments a
      where date(a.time) between @date_start and @date_end
        and TIME(a.time) > '06:00'
        and a.done <> 2
      group by date_value, a.doctor_id) T1
         left join doctor_spec ds on ds.doctor_id = T1.doctor_id
         left join users u on u.id = ds.user_id
         left join departments d on d.id = ds.department_id
where u.id <> 1
group by ds.user_id
order by doc_name;