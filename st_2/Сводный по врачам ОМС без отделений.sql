SELECT T1.doc_name, T1.total_case, T1.total_uet
FROM (SELECT d.name AS dept_name, u.name AS doc_name, ROUND(COALESCE(SUM(vt.price), 0), 2) AS total_case,
             ROUND(COALESCE(SUM(vt.uet), 0), 2) AS total_uet, ds.doctor_id AS doc_id
      FROM (case_services cs, vmu_tariff vt, vmu_tariff_plan vtp)
               LEFT JOIN doctor_spec ds ON ds.doctor_id = cs.id_doctor
               LEFT JOIN users u ON ds.user_id = u.id
               LEFT JOIN departments d ON d.id = ds.department_id
      WHERE cs.date_begin BETWEEN @date_start AND @date_end
        AND (cs.is_oms = 1 OR cs.is_oms = 5)
        AND cs.id_order = - 1
        AND (ds.doctor_id = @doc_id OR @doc_id = 0)
        AND vt.id_profile = cs.id_profile
        AND vt.id_zone_type = cs.id_net_profile
        AND cs.date_begin BETWEEN vt.tariff_begin_date AND vt.tariff_end_date
        AND vtp.id_tariff_group = vt.id_lpu
        AND cs.date_begin BETWEEN vtp.tp_begin_date AND vtp.tp_end_date
        AND vtp.id_lpu IN (SELECT id_lpu FROM mu_ident)
      GROUP BY u.id
      ORDER BY doc_name, doc_id) T1;