set @date_start = '2020-03-01';
set @date_end = '2020-03-05';
set @doc_id = 0;
set @depart_id = 0;


drop temporary table if exists fio,all_bzp_patients, bzp_patients_with_fix,bzp_patients_no_fix,t123,T1, T2, T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,T16,T17,T18,T19,T20,T21,T22,T23,T24,docs;


create temporary table if not exists fio
(SELECT id, concat(SUBSTRING_INDEX(name, ' ', 1), ' ',
	left(SUBSTRING_INDEX(SUBSTRING_INDEX(name, ' ', 2), ' ', -1), 1), '. ',
	left(SUBSTRING_INDEX(SUBSTRING_INDEX(name, ' ', -1), ' ', -1), 1), '.') as short_name FROM users);

create temporary table if not exists t123
(select fio.short_name,
		ord.id_doctor,
		cs.ID_CASE,
		ord.orderCreateDate as ord_date,
		cs.service_cost * cs.discount * (cs.discount > 0.3) + cs.service_cost * (cs.discount <= 0.3) as cost,
		cs.ID_PROFILE,
		concat(coalesce(p.code, ''), coalesce(pos.code,'')) as code,
		ds.spec_id,
		pd.id_human,
		dep.id as depid
from case_services cs
left join orders ord on cs.ID_ORDER = ord.id
left join price_orto_soc pos on pos.id = cs.id_profile and (cs.is_oms = 2 or ord.order_type = 1)
left join price p on p.id = cs.id_profile and cs.is_oms = 0 and ord.order_type <> 1
left join doctor_spec ds on cs.id_doctor = ds.doctor_id
left join fio on ds.user_id = fio.id
left join departments dep on ds.department_id = dep.id
left join patient_data pd on ord.Id_Human = pd.ID_HUMAN  and pd.is_active = 1 and pd.date_end = '2200-01-01'
where cs.id_order > 0 and
	(ord.orderCreateDate between @date_start and @date_end and ord.status < 2)
	and (order_type = 1)
    and (ord.id_doctor = @doc_id or @doc_id = 0)
	and (dep.id = @depart_id or @depart_id = 0 or @depart_id is null)
	and ds.spec_id in (107) and cs.is_oms in (0)
);

Create temporary table if not exists docs
select 
	short_name,
	id_doctor,
	ord_date
from t123
group by id_doctor, ord_date;

CALL create_temp_table_with_date("T1", "('1.2', '1.4', '12', '14')");
CALL create_temp_table_with_date("T2", "('1.4', '14')");
CALL create_temp_table_with_date("T3", "('1.2', '12')");
CALL create_temp_table_with_date("T4", "('16.1', '161')");
CALL create_temp_table_with_date("T5", "('2.5', '2.6', '2.7', '25', '26', '27')"); 
CALL create_temp_table_with_date("T6", "(' ')");
CALL create_temp_table_with_date("T7", "('13.10', '13.9', '13.8', '1310', '139', '138')");
CALL create_temp_table_with_date("T8", "('11.1', '111')");
CALL create_temp_table_with_date("T9", "(' ')"); 
CALL create_temp_table_with_date("T10", "(' ')"); 
CALL create_temp_table_with_date("T11", "(' ')"); 
CALL create_temp_table_with_date("T12", "('9.7', '97')"); 
CALL create_temp_table_with_date("T13", "('9.3', '93')"); 
CALL create_temp_table_with_date("T14", "('13.2', '132')"); 
CALL create_temp_table_with_date("T15", "('9.2', '92')"); 
CALL create_temp_table_with_date("T16", "(' ')");
CALL create_temp_table_with_date("T17", "(' ')");
CALL create_temp_table_with_date("T18", "('9.4', '94')");
CALL create_temp_table_with_date("T19", "('9.5', '95')");

SELECT short_name, docs.ord_date,
	COALESCE(t1.count, 0), COALESCE(t2.count, 0),
    COALESCE(t3.count, 0), COALESCE(t4.count, 0),
    COALESCE(t5.count, 0), COALESCE(t6.count, 0),
    COALESCE(t7.count, 0), COALESCE(t8.count, 0),
    COALESCE(t9.count, 0), COALESCE(t10.count, 0),
    COALESCE(t11.count, 0), COALESCE(t12.count, 0),
    COALESCE(t13.count, 0), COALESCE(t14.count, 0),
    COALESCE(t15.count, 0), COALESCE(t16.count, 0),
    COALESCE(t17.count, 0), COALESCE(t18.count, 0),
    COALESCE(t19.count, 0)
FROM docs
LEFT JOIN t1 ON docs.id_doctor = t1.id_doctor and docs.ord_date = t1.ord_date
LEFT JOIN t2 ON docs.id_doctor = t2.id_doctor and docs.ord_date = t2.ord_date
LEFT JOIN t3 ON docs.id_doctor = t3.id_doctor and docs.ord_date = t3.ord_date
LEFT JOIN t4 ON docs.id_doctor = t4.id_doctor and docs.ord_date = t4.ord_date
LEFT JOIN t5 ON docs.id_doctor = t5.id_doctor and docs.ord_date = t5.ord_date
LEFT JOIN t6 ON docs.id_doctor = t6.id_doctor and docs.ord_date = t6.ord_date
LEFT JOIN t7 ON docs.id_doctor = t7.id_doctor and docs.ord_date = t7.ord_date
LEFT JOIN t8 ON docs.id_doctor = t8.id_doctor and docs.ord_date = t8.ord_date
LEFT JOIN t9 ON docs.id_doctor = t9.id_doctor and docs.ord_date = t9.ord_date
LEFT JOIN t10 ON docs.id_doctor = t10.id_doctor and docs.ord_date = t10.ord_date
LEFT JOIN t11 ON docs.id_doctor = t11.id_doctor and docs.ord_date = t11.ord_date
LEFT JOIN t12 ON docs.id_doctor = t12.id_doctor and docs.ord_date = t12.ord_date
LEFT JOIN t13 ON docs.id_doctor = t13.id_doctor and docs.ord_date = t13.ord_date
LEFT JOIN t14 ON docs.id_doctor = t14.id_doctor and docs.ord_date = t14.ord_date
LEFT JOIN t15 ON docs.id_doctor = t15.id_doctor and docs.ord_date = t15.ord_date
LEFT JOIN t16 ON docs.id_doctor = t16.id_doctor and docs.ord_date = t16.ord_date
LEFT JOIN t17 ON docs.id_doctor = t17.id_doctor and docs.ord_date = t17.ord_date
LEFT JOIN t18 ON docs.id_doctor = t18.id_doctor and docs.ord_date = t18.ord_date
LEFT JOIN t19 ON docs.id_doctor = t19.id_doctor and docs.ord_date = t19.ord_date
group by docs.id_doctor,docs.ord_date
ORDER BY short_name, docs.ord_date;