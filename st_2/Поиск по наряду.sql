select concat_ws(' ', pd.SURNAME, pd.name, pd.SECOND_NAME) as pat,
       concat('Наряд № ', o.Order_Number, ', оплачено по наряду: ', pr.summ),
       case order_type when 0 then 'ПЗП' when 1 then 'БЗП' end order_type, o.orderCreateDate,
       coalesce(o.orderCompletionDate, 'наряд открыт'), u.name, cs.DATE_BEGIN,
       case order_type when 0 then p.NAME when 1 then pos.NAME end service, count(cs.ID_PROFILE),
       count(cs.ID_PROFILE) * cs.SERVICE_COST
from orders o
         left join doctor_spec ds on o.id_doctor = ds.doctor_id
         left join users u on ds.user_id = u.id
         left join patient_data pd on o.Id_Human = pd.ID_HUMAN
         left join case_services cs on o.ID = cs.ID_ORDER
         left join price p on cs.ID_PROFILE = p.ID and o.order_type = 0
         left join price_orto_soc pos on cs.ID_PROFILE = pos.ID and o.order_type = 1
         left join (select sum(summ) summ, order_id from prepayments pr where case_id = -1 group by order_id) pr
                   on cs.ID_ORDER = pr.order_id
where o.Id_Human in (select id_human
                     from case_services cs
                              left join orders o on cs.ID_ORDER = o.ID
                     where o.Order_Number = @code
                       and year(o.orderCreateDate) = @year)
  and pd.IS_ACTIVE = 1
  and pd.DATE_END = '2200-01-01'
group by o.ID, cs.DATE_BEGIN, cs.ID_PROFILE;
