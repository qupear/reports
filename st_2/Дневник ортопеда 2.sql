set @date_start = '2020-03-01';
set @date_end = '2020-03-05';
set @doc_id = 0;
set @depart_id = 0;


drop temporary table if exists fio,all_bzp_patients, bzp_patients_with_fix,bzp_patients_no_fix,
t123,T1, T2, T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,T16,T17,T18,T19,T20,T21,T22,T23,T24,
T25,T26,T27,T28,T29,T30,T31,T32,T33,T34,T35,T36,T37,T38,T39,T40,T41,T42,
T43,T44,T45,T46,T47,T48,T49,T50,T51,T52,T53,T54,T55,T56,T57,T58,T59,T60docs;


create temporary table if not exists fio
(SELECT id, concat(SUBSTRING_INDEX(name, ' ', 1), ' ',
	left(SUBSTRING_INDEX(SUBSTRING_INDEX(name, ' ', 2), ' ', -1), 1), '. ',
	left(SUBSTRING_INDEX(SUBSTRING_INDEX(name, ' ', -1), ' ', -1), 1), '.') as short_name FROM users);

create temporary table if not exists t123
(select fio.short_name,
		ord.id_doctor,
		cs.ID_CASE,
		ord.orderCreateDate as ord_date,
		cs.service_cost * cs.discount * (cs.discount > 0.3) + cs.service_cost * (cs.discount <= 0.3) as cost,
		cs.ID_PROFILE,
		concat(coalesce(p.code, ''), coalesce(pos.code,'')) as code,
		ds.spec_id,
		pd.id_human,
		dep.id as depid
from case_services cs
left join orders ord on cs.ID_ORDER = ord.id
left join price_orto_soc pos on pos.id = cs.id_profile and (cs.is_oms = 2 or ord.order_type = 1)
left join price p on p.id = cs.id_profile and cs.is_oms = 0 and ord.order_type <> 1
left join doctor_spec ds on cs.id_doctor = ds.doctor_id
left join fio on ds.user_id = fio.id
left join departments dep on ds.department_id = dep.id
left join patient_data pd on ord.Id_Human = pd.ID_HUMAN  and pd.is_active = 1 and pd.date_end = '2200-01-01'
where cs.id_order > 0 and
	(ord.orderCreateDate between @date_start and @date_end and ord.status < 2)
	and (order_type = 1)
    and (ord.id_doctor = @doc_id or @doc_id = 0)
	and (dep.id = @depart_id or @depart_id = 0 or @depart_id is null)
	and ds.spec_id in (107) and cs.is_oms in (0)
);

Create temporary table if not exists docs
select 
	short_name,
	id_doctor,
	ord_date
from t123
group by id_doctor, ord_date;

CALL create_temp_table_with_date("T20", "('9.1', '9.2', '9.3', '91', '92', '93')");
CALL create_temp_table_with_date("T21", "('9.1', '91')");
CALL create_temp_table_with_date("T22", "('9.2', '92')");
CALL create_temp_table_with_date("T23", "('9.3', '93')");
CALL create_temp_table_with_date("T24", "('11.1', '111')"); 
CALL create_temp_table_with_date("T25", "('10.2', '102')");
CALL create_temp_table_with_date("T26", "('10.1', '101')");
CALL create_temp_table_with_date("T27", "('13.5', '135')");
CALL create_temp_table_with_date("T28", "(' ')"); 
CALL create_temp_table_with_date("T29", "(' ')"); 
CALL create_temp_table_with_date("T30", "(' ')"); 
CALL create_temp_table_with_date("T31", "(' ')"); 
CALL create_temp_table_with_date("T32", "('11.1', '111')"); 
CALL create_temp_table_with_date("T33", "(' ')"); 
CALL create_temp_table_with_date("T34", "('2.2', '22')"); 
CALL create_temp_table_with_date("T35", "('13.3', '133')");
CALL create_temp_table_with_date("T36", "(' ')");
CALL create_temp_table_with_date("T37", "('13.10', '13.4', '1310', '134')");

SELECT short_name, docs.ord_date,
	COALESCE(t20.count, 0), COALESCE(t21.count, 0),
    COALESCE(t22.count, 0), COALESCE(t23.count, 0),
    COALESCE(t24.count, 0), COALESCE(t25.count, 0),
    COALESCE(t26.count, 0), COALESCE(t27.count, 0),
    COALESCE(t28.count, 0), COALESCE(t29.count, 0),
    COALESCE(t30.count, 0), COALESCE(t31.count, 0),
    COALESCE(t32.count, 0), COALESCE(t33.count, 0),
    COALESCE(t34.count, 0), COALESCE(t35.count, 0),
    COALESCE(t36.count, 0), COALESCE(t37.count, 0)

FROM docs
LEFT JOIN t20 ON docs.id_doctor = t20.id_doctor and docs.ord_date = t20.ord_date
LEFT JOIN t21 ON docs.id_doctor = t21.id_doctor and docs.ord_date = t21.ord_date
LEFT JOIN t22 ON docs.id_doctor = t22.id_doctor and docs.ord_date = t22.ord_date
LEFT JOIN t23 ON docs.id_doctor = t23.id_doctor and docs.ord_date = t23.ord_date
LEFT JOIN t24 ON docs.id_doctor = t24.id_doctor and docs.ord_date = t24.ord_date
LEFT JOIN t25 ON docs.id_doctor = t25.id_doctor and docs.ord_date = t25.ord_date
LEFT JOIN t26 ON docs.id_doctor = t26.id_doctor and docs.ord_date = t26.ord_date
LEFT JOIN t27 ON docs.id_doctor = t27.id_doctor and docs.ord_date = t27.ord_date
LEFT JOIN t28 ON docs.id_doctor = t28.id_doctor and docs.ord_date = t28.ord_date
LEFT JOIN t29 ON docs.id_doctor = t29.id_doctor and docs.ord_date = t29.ord_date
LEFT JOIN t30 ON docs.id_doctor = t30.id_doctor and docs.ord_date = t30.ord_date
LEFT JOIN t31 ON docs.id_doctor = t31.id_doctor and docs.ord_date = t31.ord_date
LEFT JOIN t32 ON docs.id_doctor = t32.id_doctor and docs.ord_date = t32.ord_date
LEFT JOIN t33 ON docs.id_doctor = t33.id_doctor and docs.ord_date = t33.ord_date
LEFT JOIN t34 ON docs.id_doctor = t34.id_doctor and docs.ord_date = t34.ord_date
LEFT JOIN t35 ON docs.id_doctor = t35.id_doctor and docs.ord_date = t35.ord_date
LEFT JOIN t36 ON docs.id_doctor = t36.id_doctor and docs.ord_date = t36.ord_date
LEFT JOIN t37 ON docs.id_doctor = t37.id_doctor and docs.ord_date = t37.ord_date
group by docs.id_doctor,docs.ord_date
ORDER BY short_name, docs.ord_date;