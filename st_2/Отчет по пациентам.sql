set @row = 0;
 SELECT
     @row:=@row + 1, T.* from (select
     CASE
         WHEN p.order_id = - 1 THEN p.case_id
         ELSE o.Order_Number
     END case_id,
     CONCAT_WS(' ', pd.SURNAME, pd.name, pd.SECOND_NAME),
     u.name,
     CASE
         WHEN ch.nal = 1 THEN p.summ
         ELSE 0
     END,
     CASE
         WHEN ch.nal = 0 THEN p.summ
         ELSE 0
     END,
     ch.number,
     DATE_FORMAT(ch.date, '%d-%m-%Y')
 FROM
     prepayments p
         LEFT JOIN
     checks ch ON p.check = ch.id
         LEFT JOIN
     orders o ON p.order_id = o.id AND p.case_id = - 1
         LEFT JOIN
     cases c ON p.case_id = c.ID_CASE
         AND p.order_id = - 1
         LEFT JOIN
     doctor_spec ds ON p.doctor_id = ds.doctor_id
         LEFT JOIN
     departments dep ON ds.department_id = dep.id
         LEFT JOIN
     users u ON ds.user_id = u.id
         LEFT JOIN
     patient_data pd ON c.ID_PATIENT = pd.ID_PATIENT
         OR o.Id_Human = pd.ID_HUMAN
 WHERE
     ch.date BETWEEN @date_start AND @date_end
         AND (ds.doctor_id = @doc_id OR @doc_id = 0)
          AND (dep.id = @depart_id OR @depart_id = 0)
 group by case_id,ch.id) T
