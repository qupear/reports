set @date_start = '2020-02-14';
set @date_end = '2020-02-14';
set @doc_id = 0;
set @depart_id = 0;


drop temporary table if exists fio,all_bzp_patients, bzp_patients_with_fix,bzp_patients_no_fix,
t123,T1, T2, T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,T16,T17,T18,T19,T20,T21,T22,T23,T24,
T25,T26,T27,T28,T29,T30,T31,T32,T33,T34,T35,T36,T37,T38,T39,T40,T41,T42,
T43,T44,T45,T46,T47,T48,T49,T50,T51,T52,T53,T54,T55,T56,T57,T58,T59,T60,docs;


create temporary table if not exists fio
(SELECT id, concat(SUBSTRING_INDEX(name, ' ', 1), ' ',
	left(SUBSTRING_INDEX(SUBSTRING_INDEX(name, ' ', 2), ' ', -1), 1), '. ',
	left(SUBSTRING_INDEX(SUBSTRING_INDEX(name, ' ', -1), ' ', -1), 1), '.') as short_name FROM users);

create temporary table if not exists t123
(select fio.short_name,
		ord.id_doctor,
		cs.ID_CASE,
		ord.orderCreateDate as ord_date,
		cs.service_cost * cs.discount * (cs.discount > 0.3) + cs.service_cost * (cs.discount <= 0.3) as cost,
		cs.ID_PROFILE,
		concat(coalesce(p.code, ''), coalesce(pos.code,'')) as code,
		ds.spec_id,
		pd.id_human,
		dep.id as depid,
        p.UET,
		pos.UET as orto_uet
from case_services cs
left join orders ord on cs.ID_ORDER = ord.id
left join price_orto_soc pos on pos.id = cs.id_profile and (cs.is_oms = 2 or ord.order_type = 1)
left join price p on p.id = cs.id_profile and cs.is_oms = 0 and ord.order_type <> 1
left join doctor_spec ds on cs.id_doctor = ds.doctor_id
left join fio on ds.user_id = fio.id
left join departments dep on ds.department_id = dep.id
left join patient_data pd on ord.Id_Human = pd.ID_HUMAN  and pd.is_active = 1 and pd.date_end = '2200-01-01'
where cs.id_order > 0 and
	(ord.orderCreateDate between @date_start and @date_end and ord.status < 2)
	and (order_type = 1)
    and (ord.id_doctor = @doc_id or @doc_id = 0)
	and (dep.id = @depart_id or @depart_id = 0 or @depart_id is null)
	and ds.spec_id in (107) and cs.is_oms in (0)
);

Create temporary table if not exists docs
select 
	short_name,
	id_doctor,
	ord_date
from t123
group by id_doctor, ord_date;

CALL create_temp_table_with_date("T39", "(' ')");
CALL create_temp_table_with_date("T39", "(' ')");
CALL create_temp_table_with_date("T40", "(' ')");
CALL create_temp_table_with_date("T41", "('4.16', '416')");
CALL create_temp_table_with_date("T42", "('3.6', '36')");
CALL create_temp_table_with_date("T43", "('3.1', '3.2', '5.1', '5.2', '31', '32', '56', '52')"); 
CALL create_temp_table_with_date("T44", "(' ')");
CALL create_temp_table_with_date("T45", "(' ')");
CALL create_temp_table_with_date("T46", "('4.13', '413')");
CALL create_temp_table_with_date("T47", "('8.2', '8.3', '8.4', '8.5', '82', '83', '84', '85')"); 
CALL create_temp_table_with_date("T48", "(' ')"); 
CALL create_temp_table_with_date("T49", "('14.2', '142')"); 
CALL create_temp_table_with_date("T50", "('2.1', '21')"); 
CALL create_temp_table_with_date("T51", "(' ')"); 
CALL create_temp_table_with_date("T52", "(' ')"); 
CALL create_temp_table_with_date("T53", "(' ')"); 
CALL create_temp_table_with_date("T54", "('15.3', '153')");
CALL create_temp_table_with_date("T55", "('15.5', '155')");
CALL create_temp_table_with_date("T56", "('12.1', '12.2', '121', '122')");
CALL create_temp_table_with_date("T57", "('4.2', '4.4', '4.5', '4.6', '4.7', '4.8', '4.9', '4.10', '4.11', '4.12', '4.14', '4.15', '42', '44', '45', '46', '47', '48', '49', '410', '411', '412', '414', '415')");
CALL create_temp_table_with_date("T58", "('10.4', '104')");

Create temporary table if not exists T59
select id_doctor, ord_date, round(sum(coalesce(uet, 0) + coalesce(orto_uet, 0)), 2) as val
	from T123
group by id_doctor, ord_date;

SELECT short_name, docs.ord_date,
	COALESCE(t39.count, 0), COALESCE(t40.count, 0),
    COALESCE(t41.count, 0), COALESCE(t42.count, 0),
    COALESCE(t43.count, 0), COALESCE(t44.count, 0),
    COALESCE(t45.count, 0), COALESCE(t46.count, 0),
    COALESCE(t47.count, 0), COALESCE(t48.count, 0),
    COALESCE(t49.count, 0), COALESCE(t50.count, 0),
    COALESCE(t51.count, 0), COALESCE(t52.count, 0),
    COALESCE(t53.count, 0), COALESCE(t54.count, 0),
    COALESCE(t55.count, 0), COALESCE(t56.count, 0),
    COALESCE(t57.count, 0), COALESCE(t58.count, 0),
    COALESCE(t59.val, 0)
FROM docs
LEFT JOIN t39 ON docs.id_doctor = t39.id_doctor and docs.ord_date = t39.ord_date
LEFT JOIN t40 ON docs.id_doctor = t40.id_doctor and docs.ord_date = t40.ord_date
LEFT JOIN t41 ON docs.id_doctor = t41.id_doctor and docs.ord_date = t41.ord_date
LEFT JOIN t42 ON docs.id_doctor = t42.id_doctor and docs.ord_date = t42.ord_date
LEFT JOIN t43 ON docs.id_doctor = t43.id_doctor and docs.ord_date = t43.ord_date
LEFT JOIN t44 ON docs.id_doctor = t44.id_doctor and docs.ord_date = t44.ord_date
LEFT JOIN t45 ON docs.id_doctor = t45.id_doctor and docs.ord_date = t45.ord_date
LEFT JOIN t46 ON docs.id_doctor = t46.id_doctor and docs.ord_date = t46.ord_date
LEFT JOIN t47 ON docs.id_doctor = t47.id_doctor and docs.ord_date = t47.ord_date
LEFT JOIN t48 ON docs.id_doctor = t48.id_doctor and docs.ord_date = t48.ord_date
LEFT JOIN t49 ON docs.id_doctor = t49.id_doctor and docs.ord_date = t49.ord_date
LEFT JOIN t50 ON docs.id_doctor = t50.id_doctor and docs.ord_date = t50.ord_date
LEFT JOIN t51 ON docs.id_doctor = t51.id_doctor and docs.ord_date = t51.ord_date
LEFT JOIN t52 ON docs.id_doctor = t52.id_doctor and docs.ord_date = t52.ord_date
LEFT JOIN t53 ON docs.id_doctor = t53.id_doctor and docs.ord_date = t53.ord_date
LEFT JOIN t54 ON docs.id_doctor = t54.id_doctor and docs.ord_date = t54.ord_date
LEFT JOIN t55 ON docs.id_doctor = t55.id_doctor and docs.ord_date = t55.ord_date
LEFT JOIN t56 ON docs.id_doctor = t56.id_doctor and docs.ord_date = t56.ord_date
LEFT JOIN t57 ON docs.id_doctor = t57.id_doctor and docs.ord_date = t57.ord_date
LEFT JOIN t58 ON docs.id_doctor = t58.id_doctor and docs.ord_date = t58.ord_date
LEFT JOIN t59 ON docs.id_doctor = t59.id_doctor and docs.ord_date = t59.ord_date
group by docs.id_doctor,docs.ord_date
ORDER BY short_name, docs.ord_date;