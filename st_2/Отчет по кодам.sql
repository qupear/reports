select T0.id_source, T0.dept_name, T0.doc_name, T0.quantity from
 (select stp.id_source, d.name as dept_name, u.name as doc_name, count(cs.id_service) as quantity from selected_stat_parameters stp
 left join vmu_profile vp on vp.profile_infis_code = stp.id_source
 left join case_services cs on cs.date_begin between @date_start and @date_end and cs.id_profile = vp.id_profile and cs.is_oms = 1
 left join doctor_spec ds on ds.doctor_id = cs.id_doctor
 left join departments d on d.id = ds.department_id
 left join users u on u.id = ds.user_id
 where stp.source = 0 and stp.id_group = @stat_group_id
 		and (d.id = @depart_id or @depart_id = 0)
         and (ds.doctor_id = @doc_id or @doc_id = 0)
 		and ds.doctor_id > 0
 group by ds.doctor_id, stp.id_source
 order by stp.id_source, dept_name, doc_name ) T0

 UNION

 select T1.descr, T1.dept_name, T1.doc_name, T1.quantity from
 (select rp.text as descr, d.name as dept_name, u.name as doc_name, sum(atos.amount) as quantity from selected_stat_parameters stp
 left join aggregated_table_of_statistics atos on atos.name = stp.id_source and atos.id_sourse = 1
 			and atos.date_value between @date_start and @date_end
 left join ref_protocols rp on rp.id = stp.id_source
 left join doctor_spec ds on ds.doctor_id = atos.id_doctor
 left join departments d on d.id = ds.department_id
 left join users u on u.id = ds.user_id
 where stp.source = 1 and stp.id_group = @stat_group_id
 		and (d.id = @depart_id or @depart_id = 0)
         and (ds.doctor_id = @doc_id or @doc_id = 0)
 		and ds.doctor_id > 0
 group by ds.doctor_id, stp.id_source
 order by descr, dept_name, doc_name ) T1

 UNION

 select T2.id_source, T2.dept_name, T2.doc_name, T2.quantity from
 (select stp.id_source, d.name as dept_name, u.name as doc_name, count(cs.id_service) as quantity from selected_stat_parameters stp
 left join price p on p.code = stp.id_source
 left join case_services cs on cs.date_begin between @date_start and @date_end and cs.id_profile = p.id and cs.is_oms = 0
 left join doctor_spec ds on ds.doctor_id = cs.id_doctor
 left join departments d on d.id = ds.department_id
 left join users u on u.id = ds.user_id
 where stp.source = 2 and stp.id_group = @stat_group_id
 		and (d.id = @depart_id or @depart_id = 0)
         and (ds.doctor_id = @doc_id or @doc_id = 0)
 		and ds.doctor_id > 0
 group by ds.doctor_id, stp.id_source
 order by stp.id_source, dept_name, doc_name ) T2

 UNION

 select T3.id_source, T3.dept_name, T3.doc_name, T3.quantity from
 (select stp.id_source, d.name as dept_name, u.name as doc_name, count(cs.id_service) as quantity from selected_stat_parameters stp
 left join price_orto_soc p on p.code = stp.id_source
 left join case_services cs on cs.date_begin between @date_start and @date_end and cs.id_profile = p.id and cs.is_oms = 0
 left join doctor_spec ds on ds.doctor_id = cs.id_doctor
 left join departments d on d.id = ds.department_id
 left join users u on u.id = ds.user_id
 where stp.source = 3 and stp.id_group = @stat_group_id
 		and (d.id = @depart_id or @depart_id = 0)
         and (ds.doctor_id = @doc_id or @doc_id = 0)
 		and ds.doctor_id > 0
 group by ds.doctor_id, stp.id_source
 order by stp.id_source, dept_name, doc_name ) T3

 UNION

 select T4.id_source, T4.dept_name, T4.doc_name, T4.quantity from
 (select stp.id_source, d.name as dept_name, u.name as doc_name, count(c.id_case) as quantity from selected_stat_parameters stp
 left join vmu_diagnosis vd on vd.diagnosis_code = stp.id_source
 left join cases c on c.date_begin between @date_start and @date_end and c.id_diagnosis = vd.id_diagnosis
 left join doctor_spec ds on ds.doctor_id = c.id_doctor
 left join departments d on d.id = ds.department_id
 left join users u on u.id = ds.user_id
 where stp.source = 4 and stp.id_group = @stat_group_id
 		and (d.id = @depart_id or @depart_id = 0)
         and (ds.doctor_id = @doc_id or @doc_id = 0)
 		and ds.doctor_id > 0
 group by ds.doctor_id, stp.id_source
 order by stp.id_source, dept_name, doc_name ) T4