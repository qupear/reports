set @zoloto = 56;set @litie = 55;set @orto_spec = 107;set @implant_spec = 7;set @surgeon_spec = 108;
create temporary table if not exists gold(select cs.id_order
                                          from case_services cs
                                                   left join price p on cs.id_profile = p.id
                                                   left join orders ord on ord.id = cs.id_order
                                          where ord.orderCompletionDate between @date_start and @date_end
                                            and ord.order_type <> 1
                                            and ord.spec = 4
                                            and cs.id_order > 0
                                            and cs.is_oms = 0
                                            and p.group_ids = @zoloto
                                          group by cs.id_order);
create temporary table if not exists foundry(select cs.id_order
                                             from case_services cs
                                                      left join price p on cs.id_profile = p.id
                                                      left join orders ord on ord.id = cs.id_order
                                             where ord.orderCompletionDate between @date_start and @date_end
                                               and ord.order_type <> 1
                                               and ord.spec = 4
                                               and cs.id_order > 0
                                               and cs.is_oms = 0
                                               and p.group_ids = @litie
                                             group by cs.id_order);
create temporary table if not exists table_1 (select u.id, d.name as dept_name, u.name as doc_name,
                                                     sum(cs.service_cost) as sum1, sum(p.uet) as sum2
                                              from orders ord
                                                       left join case_services cs on cs.id_order = ord.id
                                                       left join doctor_spec ds on ds.doctor_id = ord.id_doctor
                                                       left join departments d on d.id = ds.department_id
                                                       left join users u on u.id = ds.user_id
                                                       left join price p on p.id = cs.id_profile
                                              where ord.orderCompletionDate between @date_start and @date_end
                                                and (ord.order_type = 0 or ord.order_type = 2)
                                                and ord.spec = 4
                                                and (d.id = @depart_id or @depart_id = 0)
                                                and ds.spec_id = @orto_spec
                                                and ord.id not in (select id_order from gold)
                                                and ord.id not in (select id_order from foundry)
                                              group by u.id
                                              order by dept_name, doc_name);
create temporary table if not exists table_2 (select u.id, d.name as dept_name, u.name as doc_name,
                                                     sum(cs.service_cost) as sum1, sum(p.uet) as sum2
                                              from orders ord
                                                       left join case_services cs on cs.id_order = ord.id
                                                       left join doctor_spec ds on ds.doctor_id = ord.id_doctor
                                                       left join departments d on d.id = ds.department_id
                                                       left join users u on u.id = ds.user_id
                                                       left join price p on p.id = cs.id_profile
                                              where ord.orderCompletionDate between @date_start and @date_end
                                                and (ord.order_type = 0 or ord.order_type = 2)
                                                and ord.spec = 4
                                                and (d.id = @depart_id or @depart_id = 0)
                                                and ds.spec_id = @orto_spec
                                                and ord.id in (select id_order from gold)
                                              group by u.id
                                              order by dept_name, doc_name);
create temporary table if not exists table_3 (select u.id, d.name as dept_name, u.name as doc_name,
                                                     sum(cs.service_cost) as sum1, sum(p.uet) as sum2
                                              from orders ord
                                                       left join case_services cs on cs.id_order = ord.id
                                                       left join doctor_spec ds on ds.doctor_id = ord.id_doctor
                                                       left join departments d on d.id = ds.department_id
                                                       left join users u on u.id = ds.user_id
                                                       left join price p on p.id = cs.id_profile
                                              where ord.orderCompletionDate between @date_start and @date_end
                                                and (ord.order_type = 0 or ord.order_type = 2)
                                                and ord.spec = 4
                                                and (d.id = @depart_id or @depart_id = 0)
                                                and ds.spec_id = @orto_spec
                                                and ord.id in (select id_order from foundry)
                                              group by u.id
                                              order by dept_name, doc_name);
create temporary table if not exists table_4 (select u.id, d.name as dept_name, u.name as doc_name,
                                                     sum(cs.service_cost) as sum1, sum(p.uet) as sum2
                                              from orders ord
                                                       left join case_services cs on cs.id_order = ord.id
                                                       left join doctor_spec ds on ds.doctor_id = ord.id_doctor
                                                       left join departments d on d.id = ds.department_id
                                                       left join users u on u.id = ds.user_id
                                                       left join price p on p.id = cs.id_profile
                                              where ord.orderCompletionDate between @date_start and @date_end
                                                and (ord.order_type = 0 or ord.order_type = 2)
                                                and ord.spec = 4
                                                and (d.id = @depart_id or @depart_id = 0)
                                                and ds.spec_id = @implant_spec
                                              group by u.id
                                              order by dept_name, doc_name);






create temporary table if not exists table_5 (select u.id, d.name as dept_name, u.name as doc_name,
                                                     sum(cs.service_cost) as sum1, sum(p.uet) as sum2
                                              from orders ord
                                                       left join case_services cs on cs.id_order = ord.id
                                                       left join doctor_spec ds on ds.doctor_id = ord.id_doctor
                                                       left join departments d on d.id = ds.department_id
                                                       left join users u on u.id = ds.user_id
                                                       left join price p on p.id = cs.id_profile
                                              where ord.orderCompletionDate between @date_start and @date_end
                                                and (ord.order_type = 0 or ord.order_type = 2)
                                                and ord.spec = 4
                                                and (d.id = @depart_id or @depart_id = 0)
                                                and ds.spec_id = @surgeon_spec
                                              group by u.id
                                              order by dept_name, doc_name);

select T1.dept_name, T1.doc_name, coalesce(T1.sum1, 0), coalesce(T2.sum1, 0), coalesce(T3.sum1, 0),
       coalesce(T4.sum1, 0), coalesce(T5.sum1, 0),
       coalesce(T1.sum1, 0) + coalesce(T2.sum1, 0) + coalesce(T3.sum1, 0) + coalesce(T4.sum1, 0) + coalesce(T5.sum1, 0)
from table_1 T1
         left join table_2 T2 on T1.id = T2.id
         left join table_3 T3 on T1.id = T3.id
         left join table_4 T4 on T1.id = T4.id
         left join table_5 T5 on T1.id = T5.id;