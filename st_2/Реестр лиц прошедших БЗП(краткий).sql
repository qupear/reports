set @row= 0;
select @row := @row + 1, T.*
from (select u.name, concat_ws(' ', pd.surname, pd.name, pd.second_name), pa.UNSTRUCT_ADDRESS, bzp_direction_text,
             sum(cs.SERVICE_COST) as summ
      from case_services cs
               left join orders ord on ord.id = cs.id_order
               left join patient_data pd on ord.id_human = pd.id_human
               left join price_orto_soc pos on cs.id_profile = pos.id
               left join doctor_spec ds on ord.id_doctor = ds.doctor_id
               left join users u on u.id = ds.user_id
               left join patient_address pa on pd.ID_ADDRESS_LOC = pa.ID_ADDRESS
      where cs.id_order > 0
        and ord.order_type = 1
        and ord.orderCompletionDate between @date_start and @date_end
        and ord.status = 2
        and pd.IS_ACTIVE = 1
        and pd.DATE_END = '2200-01-01'
      group by ds.doctor_id, ord.id
      order by u.name) T;