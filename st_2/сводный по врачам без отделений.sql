
 SELECT  TM.doc_name, COALESCE(T1.total_case, 0) AS t1, COALESCE(T2.total_check, 0) AS t2 
 FROM (SELECT d.name AS dept_name, CONCAT(u.name, ' (', COALESCE(s.name, ''), ')') AS doc_name, ds.doctor_id AS doc_id, ds.spec_id, u.id as user_id      
       FROM doctor_spec ds     
       LEFT JOIN users u ON ds.user_id = u.id     
       LEFT JOIN departments d ON d.id = ds.department_id     
       LEFT JOIN speciality s ON s.id = ds.spec_id     
       WHERE (d.id = @depart_id OR @depart_id = 0) AND ds.active = 1 AND (u.id IS NOT NULL) AND (d.id IS NOT NULL)     
       group by u.id, spec_id 
       ORDER BY dept_name , doc_name , spec_id) TM         
 LEFT JOIN (SELECT SUM(cs.service_cost) AS total_case, SUM(p.uet) AS total_uet, cs.id_doctor AS doc_id , u.id as user_id, ds.spec_id as spec_id     
            FROM case_services cs     
            LEFT JOIN price p ON p.id = cs.id_profile   
            LEFT JOIN doctor_spec ds ON ds.doctor_id = cs.id_doctor
            LEFT JOIN users u ON u.id=ds.user_id  
            WHERE cs.date_begin BETWEEN @date_start AND @date_end AND cs.is_oms = 0 AND (cs.id_order = -1 OR cs.id_order IN (SELECT id FROM orders WHERE order_type <> 1))     
            GROUP BY u.id, ds.spec_id) T1 ON T1.spec_id = TM.spec_id and T1.user_id = TM.user_id         
 LEFT JOIN (SELECT SUM(p.summ) AS total_check, d.id AS dept_id, ds.doctor_id AS doc_id, ds.spec_id, u.id as user_id      
            FROM prepayments p, checks ch, doctor_spec ds, departments d, users u     
            WHERE ch.id = p.check AND ch.date BETWEEN @date_start AND @date_end AND (p.order_id = -1 OR p.order_id IN (SELECT id FROM orders WHERE order_type <> 1)) 
                 AND ds.doctor_id = p.doctor_id AND d.id = ds.department_id  and u.id=ds.user_id   
            GROUP BY u.id, ds.spec_id) T2 ON T2.spec_id = TM.spec_id and T2.user_id = TM.user_id 
 WHERE COALESCE(T1.total_case, 0) + COALESCE(T2.total_check, 0) <> 0 