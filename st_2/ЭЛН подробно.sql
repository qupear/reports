set @date_start = '2019-01-01';
set @date_end = '2019-07-07';
SELECT   e.eln_number nomer,         CONCAT(e.pat_surname,' ',e.pat_name,' ',e.pat_second_name) fio   ,
       timestampdiff(year,  e.pat_birthday, current_date),  pa.UNSTRUCT_ADDRESS, e.pat_employer, e.eln_diagnos_code,
       short_name(e.doctor_name), short_name(ET_e.doc_name), date_format(ET_s.date_begin, '%d-%m-%Y'), date_format(ET_e.date_end, '%d-%m-%Y'),
       datediff(ET_e.date_end, ET_s.date_begin ) + 1,
       case when dis.id > 0 then 'Отменен' else es.name end as status
FROM eln e
           JOIN eln_states es ON e.eln_state=es.id
           left join patient_data pd on e.pat_id = pd.ID_PATIENT
           left join patient_address pa on pd.ID_ADDRESS_REG = pa.ID_ADDRESS
           left join eln_disable_reasons dis on dis.id = e.disable_reason
           left join (select doc_name, id_eln, date_end from eln_treats where id in
           (select  max(id) from eln_treats et
                     group by id_eln )) ET_e on e.id = ET_e.id_eln
           left join (select id_eln, date_begin from eln_treats where id in
           (select  min(id) from eln_treats et
                     group by id_eln )) ET_s on e.id = ET_s.id_eln
WHERE e.eln_date BETWEEN @date_start AND @date_end;