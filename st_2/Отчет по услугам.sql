select u.name, count( u.name),  CASE
     WHEN cs.IS_OMS=0  THEN p.code
     WHEN cs.IS_OMS=1 THEN v.PROFILE_INFIS_CODE
     WHEN cs.IS_OMS=2 THEN o.code
     ELSE p.CODE
   END CODE,
   CASE
     WHEN cs.IS_OMS=0  THEN p.NAME
     WHEN cs.IS_OMS=1  THEN v.PROFILE_NAME
     WHEN cs.IS_OMS=2 THEN o.name
     ELSE p.NAME
   END CodeName

   from case_services cs
 left join price p on p.id=cs.ID_PROFILE
 left join vmu_profile v on v.id_profile=cs.ID_PROFILE
 left join price_orto_soc o on o.id=cs.ID_PROFILE
 left join doctor_spec ds on ds.doctor_id=cs.id_doctor
 left join users u on u.id=ds.user_id
   where (date(cs.date_begin) between  @date_start  and @date_end)  and cs.id_order=-1 group by u.name, code, codename union all
  select u.name, count( u.name),  CASE
     WHEN ort.order_type=0  THEN p.code
     WHEN ort.order_type=1 THEN o.code
   END CODE,
   CASE
     WHEN ort.order_type=0  THEN p.name
     WHEN ort.order_type=1 THEN o.name
   END CodeName

   from case_services cs
 left join price p on p.id=cs.ID_PROFILE
 left join vmu_profile v on v.id_profile=cs.ID_PROFILE
 left join price_orto_soc o on o.id=cs.ID_PROFILE
 left join orders ort on ort.id=cs.ID_ORDER
 left join doctor_spec ds on ds.doctor_id=cs.id_doctor
 left join users u on u.id=ds.user_id
  where (date(cs.date_begin) between  @date_start  and @date_end) and cs.ID_ORDER>-1 group by u.name, code, codename