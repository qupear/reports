select dep.name, c. date_begin, count(distinct id_case) from cases c
 left join doctor_spec ds on c.id_doctor = ds.doctor_id
 left join departments dep on ds.department_id = dep.id
 where c.date_begin between  @date_start and @date_end and (dep.id = @depart_id or @depart_id = 0) group by dep.id, date_begin