select dep.name, cs.date_begin, cs.id_case, u.name doc_name,
       concat(pd.surname, ' ', pd.name, ' ', pd.second_name) as pat_name
from cases c
         join appointments app on app.case_id = c.id_case
         left join case_services cs on cs.id_case = c.id_case
         left join doctor_spec ds on ds.doctor_id = app.doctor_id
         left join departments dep on ds.department_id = dep.id
         left join users u on u.id = ds.user_id
         left join patient_data pd on pd.ID_PATIENT = c.ID_PATIENT
where cs.date_begin between @date_start and @date_end
  and cs.id_order = -1
  and c.DATE_END is null
  and (dep.id = @depart_id or @depart_id = 0 or @depart_id is null)
  and (ds.doctor_id = @doc_id or @doc_id = 0 or @doc_id is null)
group by cs.ID_CASE, ds.doctor_id
order by dep.name, u.name, pat_name, cs.ID_CASE, cs.DATE_BEGIN;
