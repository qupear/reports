create temporary table if not exists doctor_services (select cs.date_begin, cs.id_case, vp.profile_infis_code, p.code,
                                                             vd.diagnosis_code, cs.tooth_name, cs.is_oms, pd.coag_flag
                                                      from case_services cs
                                                               left join vmu_profile vp on vp.id_profile = cs.id_profile and cs.is_oms in (1, 5)
                                                               left join price p on p.id = cs.id_profile and cs.is_oms in (0, 2)
                                                               left join vmu_diagnosis vd on cs.id_diagnosis = vd.id_diagnosis
                                                               left join cases c on c.id_case = cs.ID_CASE
                                                               left join patient_data pd on c.ID_PATIENT = pd.ID_PATIENT
                                                      where cs.id_doctor = @doc_id
                                                        and cs.date_begin between @date_start and @date_end);
create temporary table if not exists mes_cases_opened (select count(distinct pd.id_human) as humans, cs.date_begin
                                                       from case_services cs
                                                                left join cases c on c.id_case = cs.id_case
                                                                left join patient_data pd on pd.id_patient = c.id_patient
                                                                left join vmu_profile vp on vp.id_profile = cs.id_profile
                                                       where cs.id_doctor = @doc_id
                                                         and cs.date_begin between @date_start and @date_end
                                                         and cs.is_oms = 5
                                                         and vp.profile_infis_code = '781177'
                                                       group by cs.date_begin);
create temporary table if not exists mes_cases_closed (select count(T.id_human) as humans, T.date_end,
                                                              sum(datediff(T.date_end, T.date_begin) + 1) as hosp_days
                                                       from (select pd.id_human, c.date_begin, c.date_end
                                                             from case_services cs
                                                                      left join cases c on c.id_case = cs.id_case
                                                                      left join patient_data pd on pd.id_patient = c.id_patient
                                                                      left join vmu_profile vp on vp.id_profile = cs.id_profile
                                                             where cs.id_doctor = @doc_id
                                                               and c.date_end is not null
                                                               and c.date_end between @date_start and @date_end
                                                               and cs.is_oms = 5
                                                               and vp.profile_infis_code = '781177'
                                                             group by pd.id_human, c.date_end) T
                                                       group by T.date_end);
set @row = 0;
create temporary table if not exists first_row (select @row := @row + 1 as st_num, T.date_begin as st_date
                                                from (select date_begin
                                                      from doctor_services
                                                      group by date_begin
                                                      order by date_begin) T);
create temporary table if not exists T5 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where profile_infis_code in ('аОтол')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T6 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where profile_infis_code in ('мЗ004б')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T7 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where profile_infis_code in ('оЭ004')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T8 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where profile_infis_code in ('оЭ004а')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T9 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where profile_infis_code in ('оЭ004б')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T10 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where profile_infis_code in ('оЭ004в')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T11 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where profile_infis_code in ('оЭ004г')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T12 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where profile_infis_code in ('оЭ004д')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T13 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where profile_infis_code in ('оЭ004е')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T14 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where profile_infis_code in
                                                ('мЗ004б', 'оЭ004а', 'оЭ004б', 'оЭ004в', 'оЭ004г', 'оЭ004д', 'оЭ004е')
                                          group by date_begin
                                          order by date_begin);

select fr.st_num, fr.st_date, coalesce(T1.humans, 0), coalesce(T2.humans, 0), coalesce(T2.hosp_days, 0),
       coalesce(T5.val, 0), coalesce(T6.val, 0), coalesce(T7.val, 0), coalesce(T8.val, 0), coalesce(T9.val, 0),
       coalesce(T10.val, 0), coalesce(T11.val, 0), coalesce(T12.val, 0), coalesce(T13.val, 0), coalesce(T14.val, 0)
from first_row fr
         left join mes_cases_opened T1 on T1.date_begin = fr.st_date
         left join mes_cases_closed T2 on T2.date_end = fr.st_date
         left join T5 on T5.date_begin = fr.st_date
         left join T6 on T6.date_begin = fr.st_date
         left join T7 on T7.date_begin = fr.st_date
         left join T8 on T8.date_begin = fr.st_date
         left join T9 on T9.date_begin = fr.st_date
         left join T10 on T10.date_begin = fr.st_date
         left join T11 on T11.date_begin = fr.st_date
         left join T12 on T12.date_begin = fr.st_date
         left join T13 on T13.date_begin = fr.st_date
         left join T14 on T14.date_begin = fr.st_date
order by st_num ;
