select  u.name, T.p_dose,  Z.z_dose
 from roentgen ro
 left join directions dir on ro.id_direction = dir.id
   inner join doctor_spec ds on ro.assist_doc_id = ds.doctor_id
   left join users u on u.id = ds.user_id
 left join
   (select sum(dose) as p_dose, ro.assist_doc_id
 from roentgen ro
 left join directions dir on ro.id_direction = dir.id
 where ro.date between @date_start and @date_end and ro.dose > -1 and
     (locate('150', dir.child_ref_ids) > 0 or
   locate('190', dir.child_ref_ids) > 0 or
   locate('161', dir.child_ref_ids) > 0 or
   locate(';90', dir.child_ref_ids) > 0)
      group by ro.assist_doc_id) T on ro.assist_doc_id = T.assist_doc_id
 left join
      (select sum(dose) as z_dose, ro.assist_doc_id
 from roentgen ro
 left join directions dir on ro.id_direction = dir.id
 where ro.date between @date_start and @date_end and ro.dose > -1 and
  (
 locate('930', dir.child_ref_ids) > 0 or
   locate('205', dir.child_ref_ids) > 0 or
   locate('241', dir.child_ref_ids) > 0 or
   locate('840', dir.child_ref_ids) > 0 or
   locate('850', dir.child_ref_ids) > 0 or
   locate('227', dir.child_ref_ids) > 0 or
   locate('224', dir.child_ref_ids) > 0 or
   locate('210', dir.child_ref_ids) > 0 or
   locate('220', dir.child_ref_ids) > 0 or
   locate('240', dir.child_ref_ids) > 0 or
   locate('230', dir.child_ref_ids) > 0 or
   locate('207', dir.child_ref_ids) > 0 or
   locate('160', dir.child_ref_ids) > 0 or
   locate('200', dir.child_ref_ids) > 0 or
   locate('162', dir.child_ref_ids) > 0 or
   locate('260', dir.child_ref_ids) > 0 or
   locate('250', dir.child_ref_ids) > 0 or
   locate('100', dir.child_ref_ids) > 0)
   group by ro.assist_doc_id) Z on ro.assist_doc_id = Z.assist_doc_id
 where dir.id_direction in (2, 32)
  and ds.spec_id = 208 group by ro.assist_doc_id
  order by u.name