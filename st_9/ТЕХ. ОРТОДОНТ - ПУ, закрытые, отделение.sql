select d.name as dept_name, t.name as doc_name, sum(cs.service_cost), sum(p.uet)
from orders ord
         left join case_services cs on cs.id_order = ord.id
         left join doctor_spec ds on ds.doctor_id = ord.id_doctor
         left join departments d on d.id = ds.department_id
         left join price p on p.id = cs.id_profile
         left join technicians t on t.id = ord.id_technician
where ord.orderCompletionDate between @date_start and @date_end
  and (ord.order_type = 0 or ord.order_type = 2)
  and ord.spec = 5
  and (d.id = @depart_id or @depart_id = 0)
group by t.id
order by dept_name, doc_name;