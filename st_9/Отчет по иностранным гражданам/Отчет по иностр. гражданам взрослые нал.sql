set @date_start = '2020-02-19';
set @date_end = '2020-03-19';

drop temporary table if exists foreign_cases, foreign_cases1, foreigners, mes_hosp_days,age,adult,children,pu_nal,pu_beznal;

CREATE TEMPORARY TABLE IF NOT EXISTS foreign_cases 
	SELECT 
		c.id_case,
		c.id_patient,
		pd.BIRTHDAY,
		c.date_begin,
		c.date_end, 
		vo.country_short_name,
		vo.code	
	FROM 
		cases c 	
	JOIN patient_data pd ON c.id_patient = pd.id_patient 	
	JOIN vmu_oksm vo ON vo.code = pd.c_oksm AND vo.code IS NOT NULL AND vo.code <> 643
	WHERE (c.date_begin BETWEEN @date_start AND @date_end) and date_add(pd.birthday, interval 18 year) < c.date_begin ;

CREATE TEMPORARY TABLE if NOT EXISTS mes_hosp_days
	(SELECT 
		T.id_case,
		T.date_begin,
		DATEDIFF(T.date_end, T.date_begin) + 1 AS hosp_days
	FROM 
		(SELECT pd.id_human,
			cs.id_case,
			c.date_begin,
			c.date_end,
			pd.C_OKSM
		FROM 
			case_services cs
		LEFT JOIN cases c ON c.id_case = cs.id_case
		LEFT JOIN patient_data pd ON pd.id_patient = c.id_patient
		LEFT JOIN vmu_profile vp ON vp.id_profile = cs.id_profile
		WHERE c.date_end IS NOT NULL
			AND pd.C_OKSM <> 643
			AND c.date_end BETWEEN @date_start AND @date_end
			AND cs.is_oms = 5) T
	where T.date_begin BETWEEN @date_start AND @date_end
	GROUP BY id_case);


CREATE TEMPORARY TABLE if NOT EXISTS pu_nal
	select
		cs.id_service, fc.id_case, fc.id_patient, ch.nal, SERVICE_COST as summ
	from foreign_cases fc
	join case_services cs on cs.id_case = fc.id_case and cs.is_oms <> 1
	left join checks ch on ch.id = cs.check_id
	left join price p on p.id = cs.ID_PROFILE and cs.is_oms in (0,3)
	where cs.check_id is not null and ch.nal = 1;

SELECT 
    CASE cs.is_oms
		WHEN 5 THEN 'МЭС'
		ELSE 'Стоматологическая помощь'
	END txt,
    CASE cs.is_oms
        WHEN 0 THEN 'ПУ'
		WHEN 3 THEN 'ПУ'
        WHEN 1 THEN 'ОМС'
		WHEN 5 THEN 'ОМС'
    END omspu,
    fc.country_short_name AS country,
	fc.code,
	COUNT(DISTINCT fc.id_patient) AS ppl,
	count(DISTINCT fc.id_case) AS pos,
	hd.hosp_days,
	ROUND(COALESCE(SUM(T.price), 0), 2) AS total_oms,
	sum(pn.summ)
from 
	case_services cs
	join foreign_cases fc on fc.id_case = cs.id_case
	left join mes_hosp_days hd on hd.id_case = fc.id_case
	left join pu_nal pn on pn.id_case = fc.id_case and cs.is_oms in (0,3) and pn.id_service = cs.ID_SERVICE
	LEFT JOIN vmu_profile vp ON vp.id_profile = cs.id_profile AND cs.is_oms = 1
	left join checks ch on ch.id = cs.check_id and ch.nal = 1
	left join (select vt.price, vt.uet, vt.id_profile, vt.id_zone_type,
                                   vt.tariff_begin_date as d1,
                                   vt.tariff_end_date as d2,
                                   vtp.tp_begin_date as d3,
                                   vtp.tp_end_date as d4
			from vmu_tariff vt, vmu_tariff_plan vtp
			where vtp.id_tariff_group = vt.id_lpu
				And vtp.id_lpu in (select id_lpu from mu_ident)) T
                           on T.id_profile = vp.id_profile and
                              cs.date_begin between T.d1 and T.d2 and
                              cs.date_begin between T.d3 and T.d4 and
                              T.id_zone_type = cs.id_net_profile and
                              cs.is_oms = 1
where cs.DATE_BEGIN between @date_start and @date_end and cs.is_oms not in (2,4) and (cs.check_id is null or ch.nal = 1)
group by txt, omspu, fc.code
order by txt, omspu, country;