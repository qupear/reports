SET @ROW = 0;

CREATE TEMPORARY TABLE IF NOT EXISTS main_table 
SELECT 
	T.smo_name,
    short_name,
    COUNT(case_count) as case_count,
    SUM(total_cost) as total_cost,
    T.material_price,
    SUM(total_paid) as total_paid,
	/*(SUM(total_cost) - SUM(total_paid)),*/
	0.00 as left_to_pay,
    /*p_perc, проценты докторов, если не указаны в doctor_spec*/
	sum(summ_for_salary),
	doc_percent,
	SUM(total_doc) as total_doc
FROM
    (SELECT 
			T1.smo_name,
			T1.doctor_id,
            T1.short_name,
            T1.case_id as case_count,
            T1.cost as total_cost,
            T1.paid as total_paid,
            (T1.cost * T1.percent) / 100 as total_doc,
            T1.percent as doc_percent,
            T1.p_percent as p_perc,
            T1.material_price,
			(T1.cost - T1.material_price) as summ_for_salary
    FROM
        (SELECT 
			case_services.ID_CASE as case_id,
            case_services.ID_ORDER as order_id,
            case_services.ID_DOCTOR as doctor_id,
            doctor_spec.user_id as user_id,
            doctor_spec.doc_percent_dogovor as percent,
            price.doctor_percent as p_percent,
            SUM(case_services.SERVICE_COST * case_services.discount) as cost,
            SUM(case_services.SERVICE_COST * case_services.discount * (case_services.is_paid > 0)) as paid,
            price.material_price,
			coalesce(dms.long_name, 'УФСИН') as smo_name,
			concat(SUBSTRING_INDEX(users.name, ' ', 1), ' ', 
			left(SUBSTRING_INDEX(SUBSTRING_INDEX(users.name, ' ', 2), ' ', -1), 1), '. ', 
			left(SUBSTRING_INDEX(SUBSTRING_INDEX(users.name, ' ', -1), ' ', -1), 1), '.') as short_name 
    FROM
        (doctor_spec, case_services, users, price)
	left join cases c on c.id_case = case_services.id_case
	left join patient_data pd on pd.ID_PATIENT = c.ID_PATIENT
	left join smo_dms dms ON dms.id_smo = pd.dms_id_smo 
    WHERE
        case_services.ID_DOCTOR = doctor_spec.doctor_id
           And doctor_spec.doctor_id = doctor_id
            AND users.id = doctor_spec.user_id
            AND case_services.IS_OMS = 3
            AND case_services.ID_ORDER = - 1
            AND case_services.date_end >= @date_start
            AND case_services.date_end <= @date_end
            AND case_services.id_profile = price.id
    GROUP BY case_id , doctor_id, dms.long_name
    ORDER BY case_id) as T1) as T
GROUP BY smo_name, doctor_id
ORDER BY smo_name, short_name;

select @ROW := @ROW + 1 AS st_num,
	mt.*
from main_table mt;