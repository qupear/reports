 /*      Some general preparations... */ create temporary table if not exists pu_patients
(select c.id_patient
 from case_services cs
          left join cases c on c.id_case = cs.id_case
left join doctor_spec ds on ds.doctor_id = c.ID_DOCTOR
 where (ds.spec_id = 83 or ds.spec_id = 212)
   and cs.is_oms = 0
   and cs.date_begin between @date_start and @date_end
 group by c.id_patient);
 create temporary table if not exists chlh_patients
(select c.id_patient
 from cases c
          left join doctor_spec ds on ds.doctor_id = c.id_doctor
 where date_add(c.DATE_BEGIN, interval 1 month) >= @date_start
   and ds.spec_id = 109
 group by c.id_patient);
 create temporary table if not exists lor_patients(select c.id_patient
                                                   from cases c
                                                            left join doctor_spec ds on ds.doctor_id = c.id_doctor
                                                   where date_add(c.DATE_BEGIN, interval 1 month) >= @date_start
                                                     and ds.spec_id = 40
                                                   group by c.id_patient);
 create temporary table if not exists proc_units_pu
     select '4001' as c, 1.5 as ua, 2 as uc, 1.5 as uet
		union
    select '4002' as c, 1.5 as ua, 2 as uc, 1.5 as uet
     union
     select '4003' as c, 2 as ua, 2.5 as uc, 1 as uet
     union
    select '4004' as c, 1 as ua, 1.5 as uc, 1.25 as uet
     union
     select '4005' as c, 1 as ua, 1.5 as uc, 2 as uet
     union
     select '4006' as c, 2 as ua, 2.5 as uc, 2 as uet
     union
     select '4007' as c, 2 as ua, 2 as uc, 1.5 as uet
     union
     select '4008' as c, 1 as ua, 1.5 as uc, 1.25 as uet
     union
     select '4009' as c, 1 as ua, 2 as uc, 1.25 as uet
     union
     select '4010' as c, 1.5 as ua, 2 as uc, 1.67 as uet
     union
     select '4011' as c, 1 as ua, 2 as uc, 1.3 as uet
     union
     select '4012' as c, 1 as ua, 1.5 as uc, 1.25 as uet
     union
     select '7013' as c, 2 as ua, 3 as uc, 1.67 as uet
     union
     select '4014' as c, 1.5 as ua, 2 as uc, 1.01 as uet
     union
     select '4015' as c, 1 as ua, 1.5 as uc, 2 as uet
     union
     select '4019' as c, 1 as ua, 1.5 as uc, 2 as uet
     union
     select '4020' as c, 1 as ua, 1.5 as uc, 2 as uet
     union
     select '4021' as c, 1 as ua, 1.5 as uc, 2 as uet
     union
     select '4022' as c, 2 as ua, 2 as uc, 1.5 as uet
     union
     select '4023' as c, 2 as ua, 2 as uc, 1.5 as uet
     union
     select '4024' as c, 2 as ua, 2 as uc, 1.5 as uet
     union
     select '4027' as c, 2 as ua, 2.5 as uc, 1 as uet;

 create temporary table if not exists doctor_services (select cs.date_begin, cs.id_case, p.code, vd.diagnosis_code,
                                                              pu.uet, pu.ua, pu.uc, c.id_patient
                                                       from case_services cs
                                                                left join cases c on cs.id_case = c.id_case
                                                                left join vmu_diagnosis vd on cs.id_diagnosis = vd.id_diagnosis
                                                                left join price p on p.id = cs.id_profile
                                                                left join proc_units_pu pu on pu.c = p.code
																left join doctor_spec ds on ds.doctor_id = c.ID_DOCTOR
														 where (ds.spec_id = 83 or ds.spec_id = 212)
                                                         and cs.is_oms = 0
                                                         and cs.date_begin between @date_start and @date_end);
 create temporary table if not exists doctor_protocols (select c.id_case, c.date_begin, c.id_patient,
                                                               sum(locate('31670', p.child_ref_ids) > 0) as p_no_change,
                                                               sum(locate('31660', p.child_ref_ids) > 0) as p_cured,
                                                               sum(locate('31680', p.child_ref_ids) > 0) as p_better,
                                                               sum(locate('31690', p.child_ref_ids) > 0) as p_worse,
                                                               sum(locate('31640', p.child_ref_ids) > 0) as p_cancelled
                                                        from protocols p
                                                                 left join cases c on p.id_case = c.id_case
																 left join doctor_spec ds on ds.doctor_id = c.ID_DOCTOR
                                                        where p.id_doctor = c.ID_DOCTOR
														  and (ds.spec_id = 83 or ds.spec_id = 212)
                                                          and c.id_patient in (select id_patient from pu_patients)
                                                          and c.date_begin between @date_start and @date_end
                                                        group by c.id_patient);
/* The 1st string - unique patients */
create temporary table if not exists unique_patients
 (select pd.id_patient, date_add(pd.birthday, interval 18 year) > @date_end as child,
        (chlh.id_patient is not null) as chlh, (lor.id_patient is not null) as lor
 from cases c
          left join patient_data pd on pd.id_patient = c.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
		  left join doctor_spec ds on ds.doctor_id = c.ID_DOCTOR
 where c.date_begin between @date_start and @date_end
   and (ds.spec_id = 83 or ds.spec_id = 212)
   and pd.id_patient in (select id_patient from pu_patients)
 group by pd.id_patient);
set @s1_0 = '1 Принято человек';
 set @s1_1 = (select count(id_patient)
              from unique_patients
              where child = 0 and chlh = 0 and lor = 0);
 set @s1_2 = (select count(id_patient)
              from unique_patients
              where child = 1 and chlh = 0 and lor = 0);
 set @s1_3 = (select count(id_patient)
              from unique_patients
              where chlh = 1);
 set @s1_4 = (select count(id_patient)
              from unique_patients
              where lor = 1);
 set @s1_5 = (select count(id_patient)
              from unique_patients);
/*     The 1.1st string - unique patients  doc    */
create temporary table if not exists unique_patients_doc
 (select pd.id_patient, date_add(pd.birthday, interval 18 year) > @date_end as child,
        (chlh.id_patient is not null) as chlh, (lor.id_patient is not null) as lor
 from cases c
          left join patient_data pd on pd.id_patient = c.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
		  left join doctor_spec ds on ds.doctor_id = c.ID_DOCTOR
 where c.date_begin between @date_start and @date_end
   and (ds.spec_id = 83)
   and pd.id_patient in (select id_patient from pu_patients)
 group by pd.id_patient);
set @s1_1_0 = '1.1 Принято человек врачом';
 set @s1_1_1 = (select count(id_patient)
              from unique_patients_doc
              where child = 0 and chlh = 0 and lor = 0);
 set @s1_1_2 = (select count(id_patient)
              from unique_patients_doc
              where child = 1 and chlh = 0 and lor = 0);
 set @s1_1_3 = (select count(id_patient)
              from unique_patients_doc
              where chlh = 1);
 set @s1_1_4 = (select count(id_patient)
              from unique_patients_doc
              where lor = 1);
 set @s1_1_5 = (select count(id_patient)
              from unique_patients_doc);
/*     The 1.2st string - unique patients  medsis    */
create temporary table if not exists unique_patients_sis
 (select pd.id_patient, date_add(pd.birthday, interval 18 year) > @date_end as child,
        (chlh.id_patient is not null) as chlh, (lor.id_patient is not null) as lor
 from cases c
          left join patient_data pd on pd.id_patient = c.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
		  left join doctor_spec ds on ds.doctor_id = c.ID_DOCTOR
 where c.date_begin between @date_start and @date_end
   and (ds.spec_id = 212)
   and pd.id_patient in (select id_patient from pu_patients)
 group by pd.id_patient);
set @s1_5_0 = '1.2 Принято человек медсестрой';
 set @s1_5_1 = (select count(id_patient)
              from unique_patients_sis
              where child = 0 and chlh = 0 and lor = 0);
 set @s1_5_2 = (select count(id_patient)
              from unique_patients_sis
              where child = 1 and chlh = 0 and lor = 0);
 set @s1_5_3 = (select count(id_patient)
              from unique_patients_sis
              where chlh = 1);
 set @s1_5_4 = (select count(id_patient)
              from unique_patients_sis
              where lor = 1);
 set @s1_5_5 = (select count(id_patient)
              from unique_patients_sis);
/*     The 2st string - visits      */
 create temporary table if not exists visits (select c.id_case,
                                                     pd.id_patient,
                                                     date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                     (chlh.id_patient is not null) as chlh,
                                                     (lor.id_patient is not null) as lor
                                              from cases c
                                                       left join patient_data pd on pd.id_patient = c.id_patient
                                                       left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                       left join lor_patients lor on lor.id_patient = pd.id_patient
													   left join doctor_spec ds on ds.doctor_id = c.ID_DOCTOR
                                              where c.date_begin between @date_start and @date_end
                                                and (ds.spec_id = 83 or ds.spec_id = 212)
                                                and c.id_patient in (select id_patient from pu_patients)
                                              group by c.id_case);
set @s2_0 = '2. Всего посещений';
 set @s2_1 = (select count(id_case)
              from visits
              where child = 0);
 set @s2_2 = (select count(id_case)
              from visits
              where child = 1);
 set @s2_3 = (select count(id_case)
              from visits
              where chlh = 1);
 set @s2_4 = (select count(id_case)
              from visits
              where lor = 1); set @s2_5 = (select count(id_case) from visits);
/*     The 3rd string - visits doc      */
 create temporary table if not exists visits_doc (select ds.id_case, pd.id_patient,
                                                         date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                         (chlh.id_patient is not null) as chlh,
                                                         (lor.id_patient is not null) as lor
                                                  from doctor_services ds
                                                           left join patient_data pd on pd.id_patient = ds.id_patient
                                                           left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                           left join lor_patients lor on lor.id_patient = pd.id_patient
                                                  where ds.code in ('4001', '4002')
                                                  group by ds.id_case);
set @s3_0 = '2.1 в т.ч. Врача';
 set @s3_1 = (select count(id_case)
              from visits_doc
              where child = 0);
 set @s3_2 = (select count(id_case)
              from visits_doc
              where child = 1);
 set @s3_3 = (select count(id_case)
              from visits_doc
               where chlh = 1);
 set @s3_4 = (select count(id_case)
              from visits_doc
              where lor = 1);
 set @s3_5 = (select count(id_case)
              from visits_doc);
/*     The 4th string - primary visits doc      */
 create temporary table if not exists primary_visits_doc (select ds.id_case, pd.id_patient,
                                                                 date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                                 (chlh.id_patient is not null) as chlh,
                                                                 (lor.id_patient is not null) as lor
                                                          from doctor_services ds
                                                                   left join patient_data pd on pd.id_patient = ds.id_patient
                                                                   left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                                   left join lor_patients lor on lor.id_patient = pd.id_patient
                                                          where ds.code in ('4001')
                                                          group by ds.id_case);
 set @s4_0 = '2.2 в т.ч. первичных';
 set @s4_1 = (select count(id_case)
              from primary_visits_doc
              where child = 0);
 set @s4_2 = (select count(id_case)
              from primary_visits_doc
              where child = 1);
 set @s4_3 = (select count(id_case)
              from primary_visits_doc
              where chlh = 1);
 set @s4_4 = (select count(id_case)
              from primary_visits_doc
              where lor = 1); set @s4_5 = (select count(id_case) from primary_visits_doc);
/*коагулопатия*/
create temporary table if not exists coag_flag_visits
(select ds.id_case, pd.id_patient, date_add(pd.birthday, interval 18 year) > @date_end as child,
        (chlh.id_patient is not null) as chlh, (lor.id_patient is not null) as lor
 from doctor_services ds
          left join patient_data pd on pd.id_patient = ds.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where pd.coag_flag = 1

 group by ds.id_case);

set @s4_5_0 = '2.3 в т.ч. с коагулопатией';
 set @s4_5_1 = (select count(id_case)
              from coag_flag_visits
              where child = 0 and chlh = 0 and lor = 0);
 set @s4_5_2 = (select count(id_case)
              from coag_flag_visits
              where child = 1 and chlh = 0 and lor = 0);
 set @s4_5_3 = (select count(id_case)
              from coag_flag_visits
              where chlh = 1);
 set @s4_5_4 = (select count(id_case)
              from coag_flag_visits
              where lor = 1);
 set @s4_5_5 = (select count(id_case)
              from coag_flag_visits);
/*     The 5th string - finished the cure      */
 create temporary table if not exists finished_cure (select pd.id_patient,
                                                            date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                            (chlh.id_patient is not null) as chlh,
                                                            (lor.id_patient is not null) as lor
                                                     from doctor_protocols dp
                                                              left join patient_data pd on pd.id_patient = dp.id_patient
                                                              left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                              left join lor_patients lor on lor.id_patient = pd.id_patient
                                                     where dp.p_no_change + dp.p_cured + dp.p_better + dp.p_worse > 0
                                                     group by dp.id_patient);
 set @s5_0 = '3. Закончили лечение';
 set @s5_1 = (select count(id_patient)
              from finished_cure
              where child = 0);
 set @s5_2 = (select count(id_patient)
              from finished_cure
              where child = 1);
 set @s5_3 = (select count(id_patient)
              from finished_cure
              where chlh = 1);
 set @s5_4 = (select count(id_patient)
              from finished_cure
              where lor = 1); set @s5_5 = (select count(id_patient) from finished_cure);
/*     The 6th string - finished the cure, no change      */
 create temporary table if not exists finished_cure_1 (select pd.id_patient,
                                                              date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                              (chlh.id_patient is not null) as chlh,
                                                              (lor.id_patient is not null) as lor
                                                       from doctor_protocols dp
                                                                left join patient_data pd on pd.id_patient = dp.id_patient
                                                                left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                                left join lor_patients lor on lor.id_patient = pd.id_patient
                                                       where dp.p_no_change > 0
                                                       group by dp.id_patient);
set @s6_0 = '3.1 без изменений';
 set @s6_1 = (select count(id_patient)
              from finished_cure_1
              where child = 0);
 set @s6_2 = (select count(id_patient)
              from finished_cure_1
               where child = 1);
 set @s6_3 = (select count(id_patient)
              from finished_cure_1
              where chlh = 1);
 set @s6_4 = (select count(id_patient)
              from finished_cure_1
              where lor = 1); set @s6_5 = (select count(id_patient) from finished_cure_1);
/*     The 7th string - finished the cure, cured      */
 create temporary table if not exists finished_cure_2 (select pd.id_patient,
                                                              date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                              (chlh.id_patient is not null) as chlh,
                                                              (lor.id_patient is not null) as lor
                                                       from doctor_protocols dp
                                                                left join patient_data pd on pd.id_patient = dp.id_patient
                                                                left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                                left join lor_patients lor on lor.id_patient = pd.id_patient
                                                       where dp.p_cured > 0
                                                       group by dp.id_patient);
 set @s7_0 = '3.2 с выздоровлением';
 set @s7_1 = (select count(id_patient)
              from finished_cure_2
              where child = 0);
 set @s7_2 = (select count(id_patient)
              from finished_cure_2
              where child = 1);
 set @s7_3 = (select count(id_patient)
              from finished_cure_2
              where chlh = 1);
 set @s7_4 = (select count(id_patient)
              from finished_cure_2
              where lor = 1);
 set @s7_5 = (select count(id_patient)
              from finished_cure_2);
/*     The 8th string - finished the cure, better      */
 create temporary table if not exists finished_cure_3 (select pd.id_patient,
                                                              date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                              (chlh.id_patient is not null) as chlh,
                                                              (lor.id_patient is not null) as lor
                                                       from doctor_protocols dp
                                                                left join patient_data pd on pd.id_patient = dp.id_patient
                                                                left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                                left join lor_patients lor on lor.id_patient = pd.id_patient
                                                       where dp.p_better > 0
                                                       group by dp.id_patient);
 set @s8_0 = '3.3 с улучшением';
 set @s8_1 = (select count(id_patient)
              from finished_cure_3
              where child = 0);
 set @s8_2 = (select count(id_patient)
              from finished_cure_3
              where child = 1);
 set @s8_3 = (select count(id_patient)
              from finished_cure_3
              where chlh = 1);
 set @s8_4 = (select count(id_patient)
              from finished_cure_3
              where lor = 1);
 set @s8_5 = (select count(id_patient)
              from finished_cure_3);

/*     The 9th string - finished the cure, worse      */
 create temporary table if not exists finished_cure_4 (select pd.id_patient,
                                                              date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                              (chlh.id_patient is not null) as chlh,
                                                              (lor.id_patient is not null) as lor
                                                       from doctor_protocols dp
                                                                left join patient_data pd on pd.id_patient = dp.id_patient
                                                                left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                                left join lor_patients lor on lor.id_patient = pd.id_patient
                                                       where dp.p_worse > 0
                                                       group by dp.id_patient);
 set @s9_0 = '3.4 с ухудшением';
 set @s9_1 = (select count(id_patient)
              from finished_cure_4
              where child = 0);
 set @s9_2 = (select count(id_patient)
              from finished_cure_4
              where child = 1);
 set @s9_3 = (select count(id_patient)
              from finished_cure_4
              where chlh = 1);
 set @s9_4 = (select count(id_patient)
              from finished_cure_4
              where lor = 1); set @s9_5 = (select count(id_patient) from finished_cure_4);
/*     The 10th string - finished the cure, cancelled      */
 create temporary table if not exists finished_cure_5 (select pd.id_patient,
                                                              date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                              (chlh.id_patient is not null) as chlh,
                                                              (lor.id_patient is not null) as lor
                                                       from doctor_protocols dp
                                                                left join patient_data pd on pd.id_patient = dp.id_patient
                                                                left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                                left join lor_patients lor on lor.id_patient = pd.id_patient
                                                       where dp.p_cancelled > 0
                                                       group by dp.id_patient);
set @s10_0 = '4. Прервали лечение';
 set @s10_1 = (select count(id_patient)
               from finished_cure_5
               where child = 0);
 set @s10_2 = (select count(id_patient)
               from finished_cure_5
               where child = 1);
 set @s10_3 = (select count(id_patient)
               from finished_cure_5
               where chlh = 1);
 set @s10_4 = (select count(id_patient)
               from finished_cure_5
               where lor = 1);
 set @s10_5 = (select count(id_patient)
               from finished_cure_5);
/*     The 11th string - all procedures      */
 create temporary table if not exists proc_all (select ds.id_case,
                                                       date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                       (chlh.id_patient is not null) as chlh,
                                                       (lor.id_patient is not null) as lor
                                                from doctor_services ds
                                                         left join patient_data pd on pd.id_patient = ds.id_patient
                                                         left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                         left join lor_patients lor on lor.id_patient = pd.id_patient) ;
set @s11_0 = '5. Отпущено процедур всего';
 set @s11_1 = (select count(id_case)
               from proc_all
               where child = 0);
 set @s11_2 = (select count(id_case)
               from proc_all
               where child = 1);
 set @s11_3 = (select count(id_case)
               from proc_all
               where chlh = 1);
 set @s11_4 = (select count(id_case)
               from proc_all
               where lor = 1);
 set @s11_5 = (select count(id_case)
               from proc_all);
/*     The 12th string - electrotherapy      */
 create temporary table if not exists proc_all_1 (select ds.id_case,
                                                         date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                         (chlh.id_patient is not null) as chlh,
                                                         (lor.id_patient is not null) as lor
                                                  from doctor_services ds
                                                           left join patient_data pd on pd.id_patient = ds.id_patient
                                                           left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                           left join lor_patients lor on lor.id_patient = pd.id_patient
                                                  where ds.code in
                                                        ('4010', '7013', '4007', '4022', '4023', '4024', '4014', '4006'));
 set @s12_0 = '5.1 Электротерапия, всего '; set @s12_1 = (select count(id_case) from proc_all_1 where child = 0);
 set @s12_2 = (select count(id_case)
               from proc_all_1
               where child = 1);
 set @s12_3 = (select count(id_case)
               from proc_all_1
               where chlh = 1);

 set @s12_4 = (select count(id_case)
               from proc_all_1
               where lor = 1);
 set @s12_5 = (select count(id_case)
               from proc_all_1);
/*     The 13th string - electrotherapy, 4010      */
 create temporary table if not exists proc_all_11 (select ds.id_case,
                                                          date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                          (chlh.id_patient is not null) as chlh,
                                                          (lor.id_patient is not null) as lor
                                                   from doctor_services ds
                                                            left join patient_data pd on pd.id_patient = ds.id_patient
                                                            left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                            left join lor_patients lor on lor.id_patient = pd.id_patient
                                                   where ds.code in ('4010'));
set @s13_0 = '5.1.1 Флюктуоризация';
 set @s13_1 = (select count(id_case)
               from proc_all_11
               where child = 0);
 set @s13_2 = (select count(id_case)
               from proc_all_11
               where child = 1);
 set @s13_3 = (select count(id_case)
               from proc_all_11

               where chlh = 1);
 set @s13_4 = (select count(id_case)
               from proc_all_11
               where lor = 1);
 set @s13_5 = (select count(id_case)
               from proc_all_11);
/*     The 14th string - electrotherapy, 7013      */
 create temporary table if not exists proc_all_12 (select ds.id_case,
                                                          date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                          (chlh.id_patient is not null) as chlh,
                                                          (lor.id_patient is not null) as lor
                                                   from doctor_services ds
                                                            left join patient_data pd on pd.id_patient = ds.id_patient
                                                            left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                            left join lor_patients lor on lor.id_patient = pd.id_patient
                                                   where ds.code in ('7013'));
 set @s14_0 = '5.1.2 Амплипульстерапия (СМТ-терапия)';
 set @s14_1 = (select count(id_case)
               from proc_all_12
               where child = 0);
 set @s14_2 = (select count(id_case)
               from proc_all_12
               where child = 1);
 set @s14_3 = (select count(id_case)
               from proc_all_12
               where chlh = 1);
 set @s14_4 = (select count(id_case)
               from proc_all_12
               where lor = 1);
 set @s14_5 = (select count(id_case)
               from proc_all_12);

/*     The 15th string - electrotherapy, '4007', '4022', '4023', '4024'      */
 create temporary table if not exists proc_all_13 (select ds.id_case,
                                                          date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                          (chlh.id_patient is not null) as chlh,
                                                          (lor.id_patient is not null) as lor
                                                   from doctor_services ds
                                                            left join patient_data pd on pd.id_patient = ds.id_patient
                                                            left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                            left join lor_patients lor on lor.id_patient = pd.id_patient
                                                   where ds.code in ('4007', '4022', '4023', '4024'));
 set @s15_0 = '5.1.3 Гальванизация и электрофорез';
 set @s15_1 = (select count(id_case)
               from proc_all_13
               where child = 0);
 set @s15_2 = (select count(id_case)
               from proc_all_13
               where child = 1);
 set @s15_3 = (select count(id_case)
               from proc_all_13
               where chlh = 1);
 set @s15_4 = (select count(id_case)
               from proc_all_13
               where lor = 1);
 set @s15_5 = (select count(id_case)
               from proc_all_13);
/*     The 16th string - electrotherapy, 4014      */
 create temporary table if not exists proc_all_14 (select ds.id_case,
                                                          date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                          (chlh.id_patient is not null) as chlh,
                                                          (lor.id_patient is not null) as lor
                                                   from doctor_services ds
                                                            left join patient_data pd on pd.id_patient = ds.id_patient
                                                            left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                            left join lor_patients lor on lor.id_patient = pd.id_patient
                                                   where ds.code in ('4014'));
set @s16_0 = '5.1.4 Ионофорез';
 set @s16_1 = (select count(id_case)
               from proc_all_14
               where child = 0);
 set @s16_2 = (select count(id_case)
               from proc_all_14
               where child = 1);
 set @s16_3 = (select count(id_case)
               from proc_all_14
               where chlh = 1);
 set @s16_4 = (select count(id_case)
               from proc_all_14
               where lor = 1);
 set @s16_5 = (select count(id_case)
               from proc_all_14);
/*     The 17th string - electrotherapy, 4006      */
 create temporary table if not exists proc_all_15 (select ds.id_case,
                                                          date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                          (chlh.id_patient is not null) as chlh,
                                                          (lor.id_patient is not null) as lor
                                                   from doctor_services ds
                                                            left join patient_data pd on pd.id_patient = ds.id_patient
                                                            left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                            left join lor_patients lor on lor.id_patient = pd.id_patient
                                                   where ds.code in ('4006'));
 set @s17_0 = '5.1.6 Местная дарсонвализация';
 set @s17_1 = (select count(id_case)
               from proc_all_15
               where child = 0);
 set @s17_2 = (select count(id_case)
               from proc_all_15
               where child = 1);
 set @s17_3 = (select count(id_case)
               from proc_all_15
               where chlh = 1);
 set @s17_4 = (select count(id_case)
               from proc_all_15
               where lor = 1);
 set @s17_5 = (select count(id_case)
               from proc_all_15);
/*     The 18th string - electromagnetotherapy      */
 create temporary table if not exists proc_all_16 (select ds.id_case,
                                                          date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                          (chlh.id_patient is not null) as chlh,
                                                          (lor.id_patient is not null) as lor
                                                   from doctor_services ds
                                                            left join patient_data pd on pd.id_patient = ds.id_patient
                                                            left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                            left join lor_patients lor on lor.id_patient = pd.id_patient
                                                   where ds.code in ('4008', '4009', '4011', '4012'));
 set @s18_0 = '5.2 Электромагнитотерапия, всего ';
 set @s18_1 = (select count(id_case)
               from proc_all_16
               where child = 0);
 set @s18_2 = (select count(id_case)
               from proc_all_16
               where child = 1);

 set @s18_3 = (select count(id_case)
               from proc_all_16
               where chlh = 1);
 set @s18_4 = (select count(id_case)
               from proc_all_16
               where lor = 1); set @s18_5 = (select count(id_case) from proc_all_16);
/*     The 19th string - electromagnetotherapy, 4008      */
 create temporary table if not exists proc_all_2 (select ds.id_case,
                                                         date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                         (chlh.id_patient is not null) as chlh,
                                                         (lor.id_patient is not null) as lor
                                                  from doctor_services ds
                                                           left join patient_data pd on pd.id_patient = ds.id_patient
                                                           left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                           left join lor_patients lor on lor.id_patient = pd.id_patient
                                                  where ds.code in ('4008'));
 set @s19_0 = '5.2.1 Воздействие токами УВЧ ';
 set @s19_1 = (select count(id_case)
               from proc_all_2
               where child = 0);
 set @s19_2 = (select count(id_case)
               from proc_all_2
               where child = 1);
 set @s19_3 = (select count(id_case)
               from proc_all_2
               where chlh = 1);
 set @s19_4 = (select count(id_case)
               from proc_all_2
               where lor = 1);
 set @s19_5 = (select count(id_case)
               from proc_all_2);
/*     The 20th string - electromagnetotherapy, 4009      */
 create temporary table if not exists proc_all_21 (select ds.id_case,
                                                          date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                          (chlh.id_patient is not null) as chlh,
                                                          (lor.id_patient is not null) as lor
                                                   from doctor_services ds
                                                            left join patient_data pd on pd.id_patient = ds.id_patient
                                                            left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                            left join lor_patients lor on lor.id_patient = pd.id_patient
                                                   where ds.code in ('4009'));
 set @s20_0 = '5.2.2 СМВ-терапия ';
 set @s20_1 = (select count(id_case)
               from proc_all_21
               where child = 0);
 set @s20_2 = (select count(id_case)
               from proc_all_21
               where child = 1);
 set @s20_3 = (select count(id_case)
               from proc_all_21
               where chlh = 1);
 set @s20_4 = (select count(id_case)
               from proc_all_21
               where lor = 1);
 set @s20_5 = (select count(id_case)
               from proc_all_21);
/*     The 21th string - electromagnetotherapy, 4011      */
 create temporary table if not exists proc_all_22 (select ds.id_case,
                                                          date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                          (chlh.id_patient is not null) as chlh,
                                                          (lor.id_patient is not null) as lor
                                                   from doctor_services ds
                                                            left join patient_data pd on pd.id_patient = ds.id_patient
                                                            left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                            left join lor_patients lor on lor.id_patient = pd.id_patient
                                                   where ds.code in ('4011'));
 set @s21_0 = '5.2.3 Магнитотерапия низкочастотная ';
 set @s21_1 = (select count(id_case)
               from proc_all_22
               where child = 0);
 set @s21_2 = (select count(id_case)
               from proc_all_22
               where child = 1);
 set @s21_3 = (select count(id_case)
               from proc_all_22
               where chlh = 1);
 set @s21_4 = (select count(id_case)
               from proc_all_22
               where lor = 1);
 set @s21_5 = (select count(id_case)
               from proc_all_22);
/*     The 22th string - electromagnetotherapy, 4012      */
 create temporary table if not exists proc_all_23 (select ds.id_case,
                                                          date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                          (chlh.id_patient is not null) as chlh,
                                                          (lor.id_patient is not null) as lor
                                                   from doctor_services ds
                                                            left join patient_data pd on pd.id_patient = ds.id_patient
                                                            left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                            left join lor_patients lor on lor.id_patient = pd.id_patient
                                                   where ds.code in ('4012'));
 set @s22_0 = '5.2.4 УВЧ-индуктотермия ';
 set @s22_1 = (select count(id_case)
               from proc_all_23
               where child = 0);
 set @s22_2 = (select count(id_case)
               from proc_all_23
               where child = 1);
 set @s22_3 = (select count(id_case)
               from proc_all_23
               where chlh = 1);
 set @s22_4 = (select count(id_case)
               from proc_all_23
               where lor = 1);
 set @s22_5 = (select count(id_case)
               from proc_all_23);
/*     The 23th string - phototherapy      */
 create temporary table if not exists proc_all_3 (select ds.id_case,
                                                         date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                         (chlh.id_patient is not null) as chlh,
                                                         (lor.id_patient is not null) as lor
                                                  from doctor_services ds
                                                           left join patient_data pd on pd.id_patient = ds.id_patient
                                                           left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                           left join lor_patients lor on lor.id_patient = pd.id_patient
                                                  where ds.code in ('4004', '4005', '4015', '4019', '4020', '4021'));
 set @s23_0 = '5.3 Фототерапия, всего ';
 set @s23_1 = (select count(id_case)
               from proc_all_3
               where child = 0);
 set @s23_2 = (select count(id_case)
               from proc_all_3
               where child = 1);
 set @s23_3 = (select count(id_case)
               from proc_all_3
               where chlh = 1);
 set @s23_4 = (select count(id_case)
               from proc_all_3
               where lor = 1);
 set @s23_5 = (select count(id_case)
               from proc_all_3);
/*     The 24th string - phototherapy, KUF      */
 create temporary table if not exists proc_all_31 (select ds.id_case,
                                                          date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                          (chlh.id_patient is not null) as chlh,
                                                          (lor.id_patient is not null) as lor
                                                   from doctor_services ds
                                                            left join patient_data pd on pd.id_patient = ds.id_patient
                                                            left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                            left join lor_patients lor on lor.id_patient = pd.id_patient
                                                   where ds.code in ('4004'));
 set @s24_0 = '5.3.1 КУФ ';
 set @s24_1 = (select count(id_case)
               from proc_all_31
               where child = 0);
 set @s24_2 = (select count(id_case)
               from proc_all_31
               where child = 1);
 set @s24_3 = (select count(id_case)
               from proc_all_31
               where chlh = 1);
 set @s24_4 = (select count(id_case)
               from proc_all_31
               where lor = 1);
 set @s24_5 = (select count(id_case)
               from proc_all_31);
/*     The 25th string - phototherapy, laser      */
 create temporary table if not exists proc_all_32 (select ds.id_case,
                                                          date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                          (chlh.id_patient is not null) as chlh,
                                                          (lor.id_patient is not null) as lor
                                                   from doctor_services ds
                                                            left join patient_data pd on pd.id_patient = ds.id_patient
                                                            left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                            left join lor_patients lor on lor.id_patient = pd.id_patient
                                                   where ds.code in ('4005', '4015', '4019', '4020', '4021'));
 set @s25_0 = '5.3.2 Воздействие красным и инфракрасным когерентным излучением (лазер) ';
 set @s25_1 = (select count(id_case)
               from proc_all_32
               where child = 0);
 set @s25_2 = (select count(id_case)
               from proc_all_32
               where child = 1);

 set @s25_3 = (select count(id_case)
               from proc_all_32
               where chlh = 1);
 set @s25_4 = (select count(id_case)
               from proc_all_32
               where lor = 1);
 set @s25_5 = (select count(id_case)
               from proc_all_32);
/*     The 26th string - mechanotherapy      */
 create temporary table if not exists proc_all_4 (select ds.id_case,
                                                         date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                         (chlh.id_patient is not null) as chlh,
                                                         (lor.id_patient is not null) as lor
                                                  from doctor_services ds
                                                           left join patient_data pd on pd.id_patient = ds.id_patient
                                                           left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                           left join lor_patients lor on lor.id_patient = pd.id_patient
                                                  where ds.code in ('4003', '4027'));
 set @s26_0 = '5.4 Механотерапия, всего ';
 set @s26_1 = (select count(id_case)
               from proc_all_4
               where child = 0);
 set @s26_2 = (select count(id_case)
               from proc_all_4
               where child = 1);
 set @s26_3 = (select count(id_case)
               from proc_all_4
               where chlh = 1);
 set @s26_4 = (select count(id_case)
               from proc_all_4
               where lor = 1);
 set @s26_5 = (select count(id_case)
               from proc_all_4);
/*     The 27th string - mechanotherapy, 4003      */
 create temporary table if not exists proc_all_41 (select ds.id_case,
                                                          date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                          (chlh.id_patient is not null) as chlh,
                                                          (lor.id_patient is not null) as lor
                                                   from doctor_services ds
                                                            left join patient_data pd on pd.id_patient = ds.id_patient
                                                            left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                            left join lor_patients lor on lor.id_patient = pd.id_patient
                                                   where ds.code in ('4003'));
 set @s27_0 = '5.4.1 Ультрафонофорез ';
 set @s27_1 = (select count(id_case)
               from proc_all_41
               where child = 0);
 set @s27_2 = (select count(id_case)
               from proc_all_41
               where child = 1);
 set @s27_3 = (select count(id_case)
               from proc_all_41
               where chlh = 1);
 set @s27_4 = (select count(id_case)
               from proc_all_41
               where lor = 1);
 set @s27_5 = (select count(id_case)
               from proc_all_41);
/*     The 28th string - mechanotherapy, 4027      */
 create temporary table if not exists proc_all_42 (select ds.id_case,
                                                          date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                          (chlh.id_patient is not null) as chlh,
                                                          (lor.id_patient is not null) as lor
                                                   from doctor_services ds
                                                            left join patient_data pd on pd.id_patient = ds.id_patient
                                                            left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                            left join lor_patients lor on lor.id_patient = pd.id_patient
                                                   where ds.code in ('4027'));
 set @s28_0 = '5.4.2 Локальная гипербария (вакуум-терапия) ';
 set @s28_1 = (select count(id_case)
               from proc_all_42
               where child = 0);
 set @s28_2 = (select count(id_case)
               from proc_all_42
               where child = 1);
 set @s28_3 = (select count(id_case)
               from proc_all_42
               where chlh = 1);
 set @s28_4 = (select count(id_case)
               from proc_all_42
               where lor = 1);
 set @s28_5 = (select count(id_case)
               from proc_all_42);
/*     The 29th string - P-Units      */
 create temporary table if not exists proc_un (select ds.ua, ds.uc, ds.uet,
                                                      date_add(pd.birthday, interval 18 year) > @date_end as child,
                                                      (chlh.id_patient is not null) as chlh,
                                                      (lor.id_patient is not null) as lor
                                               from doctor_services ds
                                                        left join patient_data pd on pd.id_patient = ds.id_patient
                                                        left join chlh_patients chlh on chlh.id_patient = pd.id_patient
                                                        left join lor_patients lor on lor.id_patient = pd.id_patient) ;
 set @s29_0 = '6. Процедурных единиц ';
 set @s29_1 = (select sum(coalesce(ua, 0))
               from proc_un
               where child = 0);

 set @s29_2 = (select sum(coalesce(uc, 0))
               from proc_un
               where child = 1);
 set @s29_3_1 = (select sum(coalesce(ua, 0))
                 from proc_un
                 where chlh = 1
                   and child = 0);
 set @s29_3_2 = (select sum(coalesce(uc, 0))
                 from proc_un
                 where chlh = 1
                   and child = 1);
 set @s29_3 = coalesce(@s29_3_1, 0) + coalesce(@s29_3_2, 0);
 set @s29_4_1 = (select sum(coalesce(ua, 0))
                 from proc_un
                 where lor = 1
                   and child = 0);
 set @s29_4_2 = (select sum(coalesce(uc, 0))
                 from proc_un
                 where lor = 1
                   and child = 1);
 set @s29_4 = coalesce(@s29_4_1, 0) + coalesce(@s29_4_2, 0);
 set @s29_5 = coalesce(@s29_1, 0) + coalesce(@s29_2, 0);
/*     The 30th string - UET      */
   set @s30_0 = '7. УЕТ ';
 set @s30_1 = (select sum(coalesce(uet, 0))
               from proc_un
               where child = 0);
 set @s30_2 = (select sum(coalesce(uet, 0))
               from proc_un
               where child = 1);
 set @s30_3 = (select sum(coalesce(uet, 0))
               from proc_un
               where chlh = 1);
 set @s30_4 = (select sum(coalesce(uet, 0))
               from proc_un
               where lor = 1);
 set @s30_5 = coalesce(@s30_1, 0) + coalesce(@s30_2, 0);
/*     The Final(select       */
select @s1_0, @s1_1, @s1_2, @s1_3, @s1_4, @s1_5
 union
select @s1_1_0, @s1_1_1, @s1_1_2, @s1_1_3, @s1_1_4, @s1_1_5
 union
select @s1_5_0, @s1_5_1, @s1_5_2, @s1_5_3, @s1_5_4, @s1_5_5
 union
select @s2_0, @s2_1, @s2_2, @s2_3, @s2_4, @s2_5
union
select @s3_0, @s3_1, @s3_2, @s3_3, @s3_4, @s3_5
union
select @s4_0, @s4_1, @s4_2, @s4_3, @s4_4, @s4_5
 union
 select @s4_5_0, @s4_5_1, @s4_5_2, @s4_5_3, @s4_5_4, @s4_5_5
union
select @s5_0, @s5_1, @s5_2, @s5_3, @s5_4, @s5_5
union
select @s6_0, @s6_1, @s6_2, @s6_3, @s6_4, @s6_5
union
select @s7_0, @s7_1, @s7_2, @s7_3, @s7_4, @s7_5
union
select @s8_0, @s8_1, @s8_2, @s8_3, @s8_4, @s8_5
union
select @s9_0, @s9_1, @s9_2, @s9_3, @s9_4, @s9_5
union
select @s10_0, @s10_1, @s10_2, @s10_3, @s10_4, @s10_5
union
select @s11_0, @s11_1, @s11_2, @s11_3, @s11_4, @s11_5
union
select @s12_0, @s12_1, @s12_2, @s12_3, @s12_4, @s12_5
union
select @s13_0, @s13_1, @s13_2, @s13_3, @s13_4, @s13_5
union
select @s14_0, @s14_1, @s14_2, @s14_3, @s14_4, @s14_5     
union
select @s15_0, @s15_1, @s15_2, @s15_3, @s15_4, @s15_5
union
select @s16_0, @s16_1, @s16_2, @s16_3, @s16_4, @s16_5
union
select @s17_0, @s17_1, @s17_2, @s17_3, @s17_4, @s17_5
union
select @s18_0, @s18_1, @s18_2, @s18_3, @s18_4, @s18_5
union
select @s19_0, @s19_1, @s19_2, @s19_3, @s19_4, @s19_5
union
select @s20_0, @s20_1, @s20_2, @s20_3, @s20_4, @s20_5
union
select @s21_0, @s21_1, @s21_2, @s21_3, @s21_4, @s21_5
union
select @s22_0, @s22_1, @s22_2, @s22_3, @s22_4, @s22_5
union
select @s23_0, @s23_1, @s23_2, @s23_3, @s23_4, @s23_5
union
select @s24_0, @s24_1, @s24_2, @s24_3, @s24_4, @s24_5
union
select @s25_0, @s25_1, @s25_2, @s25_3, @s25_4, @s25_5
union
select @s26_0, @s26_1, @s26_2, @s26_3, @s26_4, @s26_5
union
select @s27_0, @s27_1, @s27_2, @s27_3, @s27_4, @s27_5
union
select @s28_0, @s28_1, @s28_2, @s28_3, @s28_4, @s28_5
union
select @s29_0, @s29_1, @s29_2, @s29_3, @s29_4, @s29_5
union
select @s30_0, @s30_1, @s30_2, @s30_3, @s30_4, @s30_5;
