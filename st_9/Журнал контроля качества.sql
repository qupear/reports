set @date_start = '2020-08-01';
set @date_end = '2020-08-30';
set @doc_id = 0 ;

drop temporary table if exists pat_data, pat_dir, fr, pat_eln, pat_ep, elns;

create temporary table if not exists Pat_data (select concat(pd.surname, " ", pd.name, " ", pd.second_name) as pat_name,
													app.id_patient as idpat,pd.id_human as idhum, app.doctor_id, date(app.time) as appdate, app.id
													from appointments app
													left join patient_data pd on pd.id_patient = app.id_patient
													left join people ppl on ppl.id_human = pd.ID_HUMAN
													left join addresses ads on ads.ID_ADDRESS_RECORD = ppl.ID_ADDRESS_LOCATION
													left join doctor_spec ds on ds.doctor_id = app.doctor_id
													where date(app.time) between @date_start and @date_end
														and ds.spec_id in (40, 109)
														and app.id_patient > 0
														and (ds.doctor_id = @doc_id or @doc_id = 0)
													group by idpat);

set @row = 0;
create temporary table if not exists fr (select @row := @row + 1 as st_num, appdate, pat_name
											from pat_data
											order by appdate, pat_name);

create temporary table if not exists pat_dir (select idpat, appdate, pat_name,
												case when (pd.id in (select id_appointment from directions)) then "Да"
													else "Нет"
													end as dircheck,
												case when (pd.id in (select id_appointment from directions) and dir.id_ref = 15) then "Да"
													else "Нет"
													end as dirrg,
												case when (pd.id in (select id_appointment from directions) and dir.id_ref = 6666) then "Да"
													else "Нет"
													end as dirfto,	
												case when (pd.id in (select id_appointment from directions) and dir.id_ref = 777) then "Да"
													else "Нет"
													end as dirvk					
												from pat_data pd
												left join directions dir on pd.id = dir.id_appointment
												group by pd.id
												order by appdate, pat_name);

create temporary table if not exists elns (select e.id, e.pat_id, (timestampdiff(day,min(et.date_begin), max(et.date_end)) + 1) as bold
											from pat_data pd
											left join eln e on e.pat_id = pd.idpat
											left join eln_treats et on et.id_eln = e.id
											where date(e.fss_sync_date) between @date_start and @date_end
											group by pd.idpat);
create temporary table if not exists pat_eln (select idpat, appdate, pd.pat_name,
											case when (idpat = es.pat_id) then es.bold
												else "Нет"
												end as elncheck
											from pat_data pd
											left join elns es on es.pat_id = pd.idpat

											group by pd.id
											order by appdate, pd.pat_name);

create temporary table if not exists pat_ep (select idpat, appdate, pat_name,
												case when (1=1) then "Да"
													else "Нет"
													end as ep_start,
												case when (2=2) then "Да"
													else "Нет"
													end as ep_fin
												from pat_data pd
												left join epikriz ep on ep.id_human = pd.idhum
											group by pd.id
											order by appdate, pd.pat_name);

select fr.st_num, fr.pat_name, pdir.dircheck, pep.ep_start, pdir.dirrg, pdir.dirfto, pep.ep_fin, peln.elncheck, pdir.dirvk
	from fr fr
	left join pat_dir pdir on pdir.pat_name = fr.pat_name
	left join pat_eln peln on peln.pat_name  = fr.pat_name
	left join pat_ep pep on pep.pat_name = fr.pat_name
	group by fr.st_num
	order by fr.st_num






