create temporary table if not exists doc_serv (index (DATE_BEGIN, ID_CASE))
 (select  cs.DATE_BEGIN, cs.ID_CASE, T.uet, p.uet as pu_uet,
        case when cs.IS_OMS = 0 then p.CODE when cs.IS_OMS = 1 then vp.PROFILE_INFIS_CODE
          end as code,
        case  when DATE_ADD(pd.birthday, INTERVAL 15 year) > c.date_begin then 'age_14'
          when (DATE_ADD(pd.birthday, INTERVAL 15 year) <= c.date_begin) then 'adult'
          end as age
 from case_services cs  force index (CASE_SERV_DATE_IND)
 left join doctor_spec ds on cs.ID_DOCTOR = ds.doctor_id
 left join cases c on cs.ID_CASE = c.ID_CASE
 left join patient_data pd on c.ID_PATIENT = pd.id_patient
 left join vmu_profile vp on vp.id_profile = cs.id_profile and cs.is_oms = 1
 left join price p on p.id = cs.id_profile and cs.is_oms in (0, 3)
 left join
 	(select vt.uet, vt.id_profile, vt.id_zone_type,
 			vt.tariff_begin_date as d1, vt.tariff_end_date as d2,
 			vtp.tp_begin_date as d3, vtp.tp_end_date as d4
 	from vmu_tariff vt, vmu_tariff_plan vtp
 	where vtp.id_tariff_group = vt.id_lpu And vtp.id_lpu in (select id_lpu from mu_ident) ) T on
 							(T.id_profile = vp.id_profile and cs.date_begin between T.d1 and T.d2
 							and cs.date_begin between T.d3 and T.d4
 							and T.id_zone_type = cs.id_net_profile and cs.is_oms = 1)
 where ds.spec_id = 103 and (cs.id_doctor = @doc_id or @doc_id = 0)
   and ((cs.IS_OMS = 0 and not @fin_type)  or (cs.IS_OMS = 1 and  @fin_type ))
 and cs.DATE_BEGIN between @date_start and @date_end);
 
 create temporary table if not exists col_2
        (select date_begin, count(distinct ID_CASE) as ID_CASE
 from doc_serv
 where code in ('6005', '6042', 'стот001')
          group by date_begin);
 
 create temporary table if not exists col_3
        (select date_begin, count(distinct ID_CASE) as ID_CASE
 from doc_serv
 where code in ('601401', '6019', '6045', '6026', '6047', 'стот002')
          group by date_begin);
 
 create temporary table if not exists col_4
        (select DATE_BEGIN, count(distinct ID_CASE) as ID_CASE
 from doc_serv
 where code in ('6001', 'стот003')
          group by DATE_BEGIN);
 
 create temporary table if not exists col_5
        (select ds.DATE_BEGIN, count(distinct id_case) as id_case
 from doc_serv ds
 where code in ('6005', '6042', 'стот001', '6001', 'стот003')  and
  ds.age = 'adult'
         group by DATE_BEGIN);
 
 create temporary table if not exists col_6
        (select ds.DATE_BEGIN, count(distinct id_case) as id_case
 from doc_serv ds
 where code in ('6005', '6042', 'стот001', '6001', 'стот003')  and
  ds.age = 'age_14'
          group by DATE_BEGIN);
 
 create temporary table if not exists col_7
        (select DATE_BEGIN, count(distinct ID_CASE) as ID_CASE
 from doc_serv
 where code in ('6001', 'стот003')
          group by DATE_BEGIN);
 
 create temporary table if not exists col_8
        (select DATE_BEGIN, count(distinct ID_CASE) as ID_CASE
 from doc_serv
 where code in ('6001', 'стот003') and age = 'age_14'
          group by DATE_BEGIN);
 
 create temporary table if not exists col_9
        (select date_begin, count(distinct ID_CASE) as ID_CASE
 from doc_serv
 where code in ('6005', '6042', 'стот001')
          group by date_begin);
 
 create temporary table if not exists col_10
        (select DATE_BEGIN, count(distinct ID_CASE) as ID_CASE
 from doc_serv
 where code in ('6005', '6042', 'стот001') and age = 'age_14'
          group by DATE_BEGIN);
 
 create temporary table if not exists col_11
        (select doc_serv.DATE_BEGIN,
                 coalesce(col_5.ID_CASE, 0) + coalesce(col_6.id_case,0) -  coalesce(col_7.id_case,0) as id_case
 from doc_serv
 left join col_5 on col_5.date_begin = doc_serv.date_begin
 left join col_6 on col_6.date_begin = doc_serv.date_begin
 left join col_7 on col_7.date_begin = doc_serv.date_begin
          group by doc_serv.DATE_BEGIN);
 
 create temporary table if not exists col_12
        (select doc_serv.DATE_BEGIN,
                 coalesce(col_5.ID_CASE, 0) + coalesce(col_6.id_case,0) -  coalesce(col_7.id_case,0) as id_case
 from doc_serv
 left join col_5 on col_5.date_begin = doc_serv.date_begin
 left join col_6 on col_6.date_begin = doc_serv.date_begin
 left join col_7 on col_7.date_begin = doc_serv.date_begin
  where doc_serv.age = 'age_14'
          group by doc_serv.DATE_BEGIN);
 
 create temporary table if not exists col_14
        (select DATE_BEGIN, count(distinct id_case) as id_case
 from doc_serv
  where code in ('стот010','стот011','стот012','6060','60601','6053','60531',
  '6054','60541','6055','60551','6056','60561')
          group by DATE_BEGIN);
 
 create temporary table if not exists col_15
        (select DATE_BEGIN, count(distinct id_case) as id_case
 from doc_serv
  where code in ('6060','60601','6053','60531',
  '6054','60541','6055','60551','6056','60561')
          group by DATE_BEGIN);
 
 create temporary table if not exists col_16
        (select DATE_BEGIN, count(distinct id_case) as id_case
 from doc_serv
  where code in ('6060','60601','60611','607101',
  '607102','стот010','стот011','стот012','6048')
          group by DATE_BEGIN);
 
 create temporary table if not exists col_17
        (select DATE_BEGIN, count(distinct id_case) as id_case
 from doc_serv
  where code in ('6026','6032','6033','6035',
  '6036','6037','6038')
          group by DATE_BEGIN);
 
 create temporary table if not exists col_18
        (select DATE_BEGIN, count(distinct id_case) as id_case
 from doc_serv
  where code in ('6046','6052','6086')
          group by DATE_BEGIN);
 
 create temporary table if not exists col_19
        (select DATE_BEGIN, count(distinct id_case) as id_case
 from doc_serv
  where code in ('стот015','стот016')
          group by DATE_BEGIN);
 
 create temporary table if not exists col_20
        (select DATE_BEGIN, count(distinct id_case) as id_case
 from doc_serv
  where code in ('6049','60491','6050','60501')
          group by DATE_BEGIN);
 
 create temporary table if not exists col_21
        (select DATE_BEGIN, count(distinct id_case) as id_case
 from doc_serv
  where code in ('6084','6016','6017','6085')
          group by DATE_BEGIN);
 
 create temporary table if not exists col_22
        (select DATE_BEGIN, count(distinct id_case) as id_case
 from doc_serv
  where code in ('6075','стот008','стот009')
          group by DATE_BEGIN);
 
 create temporary table if not exists doc_prot
        (select ds.DATE_BEGIN,ds.id_case,
         locate('101950', p.child_ref_ids) > 0 as 'p101950',
         locate('101960', p.child_ref_ids) > 0 as 'p101960',
         locate('101970', p.child_ref_ids) > 0 as 'p101970'
 from doc_serv ds
 left join protocols p on ds.id_case = p.id_case
 having p101950 > 0 or p101960 > 0 or p101970 > 0);
 
 create temporary table if not exists col_24
        (select DATE_BEGIN, count(distinct id_case) as id_case
        from doc_prot
        where p101950
        group by date_begin);
 
 create temporary table if not exists col_25
        (select DATE_BEGIN, count(distinct id_case) as id_case
        from doc_prot
        where p101960
        group by date_begin);
 
 create temporary table if not exists col_26
        (select DATE_BEGIN, count(distinct id_case) as id_case
        from doc_prot
        where p101970
        group by date_begin);
 
 create temporary table if not exists col_27
        (select DATE_BEGIN, round(sum(uet),2) as uet
        from doc_serv
        group by date_begin);
 
 create temporary table if not exists col_28
        (select DATE_BEGIN, sum(pu_uet) as pu_uet
        from doc_serv
        group by date_begin);
 
 select ds.date_begin, coalesce(c2.id_case,0)+coalesce(c3.id_case,0)+coalesce(c4.id_case,0),
 coalesce(c2.id_case,0), coalesce(c3.id_case,0), coalesce(c4.id_case,0),
  coalesce(c5.id_case,0), coalesce(c6.id_case,0),
 coalesce(c7.id_case,0), coalesce(c8.id_case,0), coalesce(c9.id_case,0), coalesce(c10.id_case,0),
 coalesce(c11.id_case,0), coalesce(c12.id_case,0),
 coalesce(c14.id_case,0) + coalesce(c15.id_case,0) + coalesce(c16.id_case,0)
  + coalesce(c17.id_case,0) + coalesce(c18.id_case,0) + coalesce(c19.id_case,0) + coalesce(c20.id_case,0)
  + coalesce(c21.id_case,0) + coalesce(c22.id_case,0),
  coalesce(c14.id_case,0), coalesce(c15.id_case,0), coalesce(c16.id_case,0), coalesce(c17.id_case,0),
   coalesce(c18.id_case,0), coalesce(c19.id_case,0), coalesce(c20.id_case,0), coalesce(c21.id_case,0),
   coalesce(c22.id_case,0), coalesce(c24.id_case,0) + coalesce(c25.id_case,0) + coalesce(c26.id_case,0),
    coalesce(c24.id_case,0), coalesce(c25.id_case,0), coalesce(c26.id_case,0), coalesce(c27.uet,0),
    coalesce(c28.pu_uet,0)
 from doc_serv ds
 left join col_2 c2 on ds.date_begin = c2.date_begin
 left join col_3 c3 on ds.date_begin = c3.date_begin
 left join col_4 c4 on ds.date_begin = c4.date_begin
 left join col_5 c5 on ds.date_begin = c5.date_begin
 left join col_6 c6 on ds.date_begin = c6.date_begin
 left join col_7 c7 on ds.date_begin = c7.date_begin
 left join col_8 c8 on ds.date_begin = c8.date_begin
 left join col_9 c9 on ds.date_begin = c9.date_begin
 left join col_10 c10 on ds.date_begin = c10.date_begin
 left join col_11 c11 on ds.date_begin = c11.date_begin
 left join col_12 c12 on ds.date_begin = c12.date_begin
 left join col_14 c14 on ds.date_begin = c14.date_begin
 left join col_15 c15 on ds.date_begin = c15.date_begin
 left join col_16 c16 on ds.date_begin = c16.date_begin
 left join col_17 c17 on ds.date_begin = c17.date_begin
 left join col_18 c18 on ds.date_begin = c18.date_begin
 left join col_19 c19 on ds.date_begin = c19.date_begin
 left join col_20 c20 on ds.date_begin = c20.date_begin
 left join col_21 c21 on ds.date_begin = c21.date_begin
 left join col_22 c22 on ds.date_begin = c22.date_begin
 left join col_24 c24 on ds.date_begin = c24.date_begin
 left join col_25 c25 on ds.date_begin = c25.date_begin
 left join col_26 c26 on ds.date_begin = c26.date_begin
 left join col_27 c27 on ds.date_begin = c27.date_begin
 left join col_28 c28 on ds.date_begin = c28.date_begin
 group by ds.date_begin
 order by ds.date_begin;
 