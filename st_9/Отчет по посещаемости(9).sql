set @date_start = '2020-03-01';
set @date_end = '2020-03-04';
set @depart_id = 0;
set @doc_id = 0;

drop temporary table if exists internet_black_list, doctor_ap, registr_ap, internet_ap;

create temporary table if not exists internet_ap 
	select ds.department_id dep_id, date(ap.time) date_ap, count(ap.id) inet_count
	from appointments ap
	left join doctor_spec ds on ds.doctor_id = ap.doctor_id
	where ap.time between @date_start and @date_end + interval 1 day
		and ap.exported = 1
		and ap.patient_name is not null
		and ap.patient_name <> '' 
		and (ds.department_id = @depart_id or @depart_id = 0) 
	group by ds.department_id, date_ap;

create temporary table if not exists registr_ap 
	select ds.department_id dep_id, date(ap.time) date_ap, count(ap.id) registr_count 
	from appointments ap
	left join doctor_spec ds on ds.doctor_id = ap.doctor_id
	where (ap.time between @date_start and @date_end  + interval 1 day)
	and ap.exported = 0
	and ap.author = 'r'
	and ap.patient_name <> ''
	and ap.patient_name is not null
	and (ds.department_id = @depart_id or @depart_id = 0)
	group by ds.department_id, date_ap;

create temporary table if not exists doctor_ap 
	select ds.department_id dep_id, date(ap.time) date_ap, count(ap.id) as doctor_count
	from appointments ap
	left join doctor_spec ds on ds.doctor_id = ap.doctor_id
	where (ap.time between @date_start and @date_end  + interval 1 day)
	and ap.exported = 0
	and ap.author = 'd'
	and ap.patient_name <> ''
	and ap.patient_name is not null
	and (ds.department_id = @depart_id or @depart_id = 0)
	group by ds.department_id, date_ap;

select dep.name, 
	date(ap.time) date_ap,
	rp.registr_count,
	ip.inet_count,
	dp.doctor_count, 
	(coalesce(ip.inet_count, 0) + coalesce(dp.doctor_count, 0) + coalesce(rp.registr_count, 0)) total
from appointments ap
		left join doctor_spec ds on ds.doctor_id = ap.doctor_id
        left join users u ON u.id = ap.user_id 
        left join internet_ap ip ON date(ap.time) = ip.date_ap and ip.dep_id = ds.department_id
        left join registr_ap rp ON date(ap.time) = rp.date_ap and rp.dep_id = ds.department_id
        left join doctor_ap dp ON date(ap.time) = dp.date_ap and dp.dep_id = ds.department_id
        left join departments dep ON dep.id = ds.department_id
where
        (ap.doctor_id = @doc_id or @doc_id = 0 or @doc_id is null)
        and (dep.id = @depart_id or @depart_id = 0 or @depart_id is null)
		and ap.time between @date_start and @date_end + interval 1 day
		and (coalesce(ip.inet_count, 0) + coalesce(dp.doctor_count, 0) + coalesce(rp.registr_count, 0)) > 0
group by dep.id, date_ap
order by dep.name, date_ap;