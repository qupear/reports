create temporary table if not exists doctor_services (select cs.date_begin, cs.id_case, vp.profile_infis_code, p.code,
                                                             vd.diagnosis_code, cs.tooth_name, T.uet, cs.is_oms,
                                                             c.is_closed, pd.coag_flag 
                                                      from case_services cs
                                                               left join cases c on c.id_case = cs.id_case
                                                               left join vmu_profile vp on vp.id_profile = cs.id_profile and cs.is_oms = 1
                                                               left join price p on p.id = cs.id_profile and cs.is_oms in (0, 3, 4)
                                                               left join vmu_diagnosis vd on cs.id_diagnosis = vd.id_diagnosis
                                                               left join doctor_spec ds on cs.id_doctor = ds.doctor_id
															   left join patient_data pd on pd.id_patient = c.id_patient
                                                               left join (select vt.uet, vt.id_profile, vt.id_zone_type,
                                                                                 vt.tariff_begin_date as d1,
                                                                                 vt.tariff_end_date as d2,
                                                                                 vtp.tp_begin_date as d3,
                                                                                 vtp.tp_end_date as d4
                                                                          from vmu_tariff vt,
                                                                               vmu_tariff_plan vtp
                                                                          where vtp.id_tariff_group = vt.id_lpu
                                                                            And vtp.id_lpu in (select id_lpu from mu_ident)) T
                                                                         on T.id_profile = vp.id_profile and
                                                                            cs.date_begin between T.d1 and T.d2 and
                                                                            cs.date_begin between T.d3 and T.d4 and
                                                                            T.id_zone_type = cs.id_net_profile and
                                                                            cs.is_oms = 1
                                                      where (ds.department_id = @depart_id or @depart_id = 0)
                                                        and ds.spec_id = 106
                                                        and cs.date_begin between @date_start and @date_end);
create temporary table if not exists doctor_protocols (select ds.id_case, ds.date_begin,
                                                              locate('17239', p.id_ref) = 1 as p17239,
                                                              locate('17251', p.id_ref) = 1 as p17251,
                                                              locate('18720', p.id_ref) = 1 as p18720,
                                                              locate('19010', p.id_ref) = 1 as p19010,
                                                              locate('44170', p.id_ref) = 1 as p44170,
                                                              locate('47780', p.id_ref) = 1 as p47780,
                                                              locate('79760', p.id_ref) = 1 as p79760,
                                                              locate('80370', p.id_ref) = 1 as p80370,
                                                              locate('18730', p.id_ref) = 1 as p18730,
                                                              locate('80380', p.id_ref) = 1 as p80380,
                                                              locate('55791', p.id_ref) = 1 as p55791,
                                                              locate('5761', p.id_ref) = 1 as p5761,
                                                              locate('4781', p.id_ref) = 1 as p4781,
                                                              locate('3790', p.id_ref) = 1 as p3790,
                                                              locate('2350', p.id_ref) = 1 as p2350,
                                                              locate('1392', p.id_ref) = 1 as p1392,
                                                              locate('480', p.id_ref) = 1 as p480,
                                                              locate('168282', p.id_ref) = 1 as p168282,
                                                              locate('164781', p.id_ref) = 1 as p164781,
                                                              locate('143652', p.id_ref) = 1 as p143652,
                                                              locate('141172', p.id_ref) = 1 as p141172,
                                                              locate('138632', p.id_ref) = 1 as p138632,
                                                              locate('136072', p.id_ref) = 1 as p136072,
                                                              locate('133582', p.id_ref) = 1 as p133582,
                                                              locate('130161', p.id_ref) = 1 as p130161,
                                                              locate('127931', p.id_ref) = 1 as p127931,
                                                              locate('125691', p.id_ref) = 1 as p125691,
                                                              locate('122602', p.id_ref) = 1 as p122602,
                                                              locate('119952', p.id_ref) = 1 as p119952,
                                                              locate('117292', p.id_ref) = 1 as p117292,
                                                              locate('114632', p.id_ref) = 1 as p114632,
                                                              locate('109102', p.id_ref) = 1 as p109102,
                                                              locate('107001', p.id_ref) = 1 as p107001,
                                                              locate('56733', p.id_ref) = 1 as p56733,
                                                              locate('55796', p.id_ref) = 1 as p55796,
                                                              locate('2351', p.id_ref) = 1 as p2351,
                                                              locate('1396', p.id_ref) = 1 as p1396,
                                                              locate('481', p.id_ref) = 1 as p481
                                                       from doctor_services ds
                                                                left join protocols p on p.id_case = ds.id_case
                                                       where ds.is_oms = 1);    set @row = 0;
create temporary table if not exists first_row (select @row := @row + 1 as st_num, T.date_begin as st_date
                                                from (select date_begin
                                                      from doctor_services
                                                      group by date_begin
                                                      order by date_begin) T);
/* create temporary table if not exists T1  (select date_begin, count(distinct id_case) as val from doctor_services where profile_infis_code in
   ('стт005', 'стт006', 'стт007', 'стт008')   group by date_begin order by date_begin);   */
create temporary table if not exists T1 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where is_closed = 1
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T2 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where profile_infis_code in ('стт005', 'нстт005')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T3 (select date_begin, count(distinct id_case) as val
                                         from doctor_protocols
                                         where p17239
                                            or p17251
                                            or p18720
                                            or p19010
                                            or p44170
                                            or p47780
                                            or p79760
                                            or p80370
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T4 (select date_begin, count(distinct id_case) as val
                                         from doctor_protocols
                                         where p18730
                                            or p80380
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T5
(select T.date_begin, count(T.val) as val
from (select date_begin, count(id_case) as val
      from doctor_services
      where profile_infis_code in ('стт025', 'стт028', 'стт031', 'нстт025', 'нстт028', 'нстт031')
      group by id_case, tooth_name
      order by date_begin) T
group by date_begin);
create temporary table if not exists T6 (select T.date_begin, count(T.val) as val
                                         from (select date_begin, count(id_case) as val
                                               from doctor_services
                                               where profile_infis_code in ('стт025', 'стт028', 'стт031', 'нстт025', 'нстт028', 'нстт031')
                                                 and diagnosis_code in
                                                     ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5', 'k02.6',
                                                      'k02.7', 'k02.8', 'k02.9', 'k03.2')
                                               group by id_case, tooth_name
                                               order by date_begin) T
                                         group by date_begin);
create temporary table if not exists T7 (select T.date_begin, count(T.val) as val
                                         from (select date_begin, count(id_case) as val
                                               from doctor_services
                                               where profile_infis_code in ('стт025', 'стт028', 'стт031', 'нстт025', 'нстт028', 'нстт031')
                                                 and diagnosis_code in ('k04.0', 'k04.1', 'k04.2', 'k04.3')
                                               group by id_case, tooth_name
                                               order by date_begin) T
                                         group by date_begin);
create temporary table if not exists T8 (select T.date_begin, count(T.val) as val
                                         from (select date_begin, count(id_case) as val
                                               from doctor_services
                                               where profile_infis_code in ('стт025', 'стт028', 'стт031', 'нстт025', 'нстт028', 'нстт031')
                                                 and diagnosis_code in ('k04.4', 'k04.5')
                                               group by id_case, tooth_name
                                               order by date_begin) T
                                         group by date_begin);
create temporary table if not exists T9 (select T.date_begin, count(T.val) as val
                                         from (select date_begin, count(id_case) as val
                                               from doctor_services
                                               where profile_infis_code in ('стт053', 'нстт053')
                                               group by id_case, tooth_name
                                               order by date_begin) T
                                         group by date_begin);
create temporary table if not exists T10 (select T.date_begin, count(T.val) as val
                                          from (select date_begin, count(id_case) as val
                                                from doctor_services
                                                where profile_infis_code in
                                                      ('стт026', 'стт027', 'стт029', 'стт030', 'стт032', 'стт033',
                                                       'стт057', 'стт058')
                                                  and diagnosis_code in ('k04.0', 'k04.1', 'k04.2', 'k04.3', 'k04.4', 'k04.5')
                                                group by id_case, tooth_name
                                                order by date_begin) T
                                          group by date_begin);
create temporary table if not exists T11 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where profile_infis_code in
                                                ('стт026', 'стт027', 'стт029', 'стт030', 'стт032', 'стт033', 'стт034',
                                                 'стт057', 'стт058', 'нстт034')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T12 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in
                                                ('1027', '1028', '1029', '1031', '1033', '1034', '1039', '1040', '1041',
                                                 '1042', '1043', '1044', '1045', '1046', '1047', '1048', '1049', '1050',
                                                 '1052', '1054', '1058', '1059', '1062', '1073', '1074', '1092', '1093',
                                                 '1094', '1095', '1096', '1097', '1098', '1099', '1100', '1101', '1102',
                                                 '1103', '1104', '1105', '1106', '1107', '1108', '1109', '1110', '1111',
                                                 '1118', '1119')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T13 (select date_begin, count(distinct id_case) as val
                                          from doctor_protocols
                                          where p55791
                                             or p5761
                                             or p4781
                                             or p3790
                                             or p2350
                                             or p1392
                                             or p480
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T13_5 (select date_begin, count(distinct id_case) as val
                                          from doctor_protocols
                                          where p168282
                                             or p164781
                                             or p143652
                                             or p141172
                                             or p138632
                                             or p136072
                                             or p133582
											 or p130161
                                             or p127931
                                             or p125691
                                             or p122602
                                             or p119952
                                             or p117292
                                             or p114632
                                             or p109102
                                             or p107001
                                             or p56733
                                             or p55796
                                             or p2351
                                             or p1396
                                             or p481
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T14 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where profile_infis_code in ('стт041', 'нстт041')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T15 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where profile_infis_code in ('сто022')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T16 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where profile_infis_code in ('стт020', 'нстт020')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T17 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where profile_infis_code in ('стт022', 'нстт022')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T18 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where profile_infis_code in ('сто001', 'сто002', 'сто003', 'нсто001', 'нсто002', 'нсто003')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T19 (select date_begin, ROUND(COALESCE(SUM(UET), 0), 2) as val
                                          from doctor_services
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T21 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where profile_infis_code in ('стт008')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists t_cf (SELECT date_begin, count(distinct id_case) as val
                             from doctor_services
							 where coag_flag = 1
                             group by date_begin
                             order by date_begin);
select fr.st_num, fr.st_date, coalesce(T1.val, 0), coalesce(T2.val, 0), coalesce(t_cf.val, 0), coalesce(T3.val, 0), coalesce(T4.val, 0),
       coalesce(T5.val, 0), coalesce(T6.val, 0), coalesce(T7.val, 0), coalesce(T8.val, 0), coalesce(T9.val, 0),
       coalesce(T10.val, 0), coalesce(T11.val, 0) + coalesce(T12.val, 0), coalesce(T11.val, 0), coalesce(T12.val, 0),
       coalesce(T13.val, 0), coalesce(T13_5.val, 0), coalesce(T21.val, 0), coalesce(T14.val, 0), coalesce(T15.val, 0), coalesce(T16.val, 0), coalesce(T17.val, 0),
       coalesce(T18.val, 0), coalesce(T19.val, 0)
from first_row fr
         left join T1 on T1.date_begin = fr.st_date
         left join T2 on T2.date_begin = fr.st_date
		 left join t_cf on t_cf.date_begin = fr.st_date
         left join T3 on T3.date_begin = fr.st_date
         left join T4 on T4.date_begin = fr.st_date
         left join T5 on T5.date_begin = fr.st_date
         left join T6 on T6.date_begin = fr.st_date
         left join T7 on T7.date_begin = fr.st_date
         left join T8 on T8.date_begin = fr.st_date
         left join T9 on T9.date_begin = fr.st_date
         left join T10 on T10.date_begin = fr.st_date
         left join T11 on T11.date_begin = fr.st_date
         left join T12 on T12.date_begin = fr.st_date
         left join T13 on T13.date_begin = fr.st_date
         left join T13_5 on T13_5.date_begin = fr.st_date
         left join T14 on T14.date_begin = fr.st_date
         left join T15 on T15.date_begin = fr.st_date
         left join T16 on T16.date_begin = fr.st_date
         left join T17 on T17.date_begin = fr.st_date
         left join T18 on T18.date_begin = fr.st_date
         left join T19 on T19.date_begin = fr.st_date
         left join T21 on T21.date_begin = fr.st_date
order by st_num;























