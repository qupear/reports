DROP TEMPORARY TABLE if EXISTS doctor_services;
CREATE TEMPORARY TABLE IF NOT EXISTS doctor_services (
SELECT cs.date_begin, cs.id_case, vp.profile_infis_code, pos.code AS pos_code, vd.diagnosis_code, cs.tooth_name, T.uet, cs.is_oms, pd.birthday, pd.coag_flag
FROM case_services cs
LEFT JOIN vmu_profile vp ON vp.id_profile = cs.id_profile AND (cs.is_oms = 1 OR cs.is_oms = 5)
LEFT JOIN price_orto_soc pos ON pos.id = cs.id_profile AND cs.is_oms = 2
LEFT JOIN vmu_diagnosis vd ON cs.id_diagnosis = vd.id_diagnosis
LEFT JOIN cases c ON c.id_case = cs.id_case
LEFT JOIN patient_data pd ON pd.id_patient = c.id_patient
LEFT JOIN doctor_spec ds ON cs.id_doctor = ds.doctor_id
LEFT JOIN (
SELECT vt.uet, vt.id_profile, vt.id_zone_type, vt.tariff_begin_date AS d1, vt.tariff_end_date AS d2, vtp.tp_begin_date AS d3, vtp.tp_end_date AS d4
FROM vmu_tariff vt, vmu_tariff_plan vtp
WHERE vtp.id_tariff_group = vt.id_lpu AND vtp.id_lpu IN (
SELECT id_lpu
FROM mu_ident)) T ON T.id_profile = vp.id_profile AND cs.date_begin BETWEEN T.d1 AND T.d2 AND cs.date_begin BETWEEN T.d3 AND T.d4 AND T.id_zone_type = cs.id_net_profile AND (cs.is_oms = 1 OR cs.is_oms = 5)
WHERE (ds.department_id = @depart_id OR @depart_id = 0) AND ds.spec_id = 108 AND cs.date_begin BETWEEN @date_start AND @date_end 
AND cs.is_oms IN (1, 2, 5) and pd.coag_flag = 1); SET @ROW = 0;
DROP TEMPORARY TABLE if EXISTS first_row;
CREATE TEMPORARY TABLE IF NOT EXISTS first_row (
SELECT @ROW := @ROW + 1 AS st_num, T.date_begin AS st_date, T.uet_sum AS st_uet
FROM (
SELECT date_begin, ROUND(COALESCE(SUM(UET), 0), 2) AS uet_sum
FROM doctor_services
GROUP BY date_begin
ORDER BY date_begin) T);
DROP TEMPORARY TABLE if EXISTS t1;
CREATE TEMPORARY TABLE IF NOT EXISTS T1 (
SELECT date_begin, COUNT(DISTINCT id_case) AS val
FROM doctor_services
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS t2;
CREATE TEMPORARY TABLE IF NOT EXISTS T2 (
SELECT date_begin, COUNT(DISTINCT id_case) AS val
FROM doctor_services
WHERE DATE_ADD(birthday, INTERVAL 15 YEAR) > date_begin
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS t3;
CREATE TEMPORARY TABLE IF NOT EXISTS T3 (
SELECT date_begin, COUNT(DISTINCT id_case) AS val
FROM doctor_services
WHERE DATE_ADD(birthday, INTERVAL 15 YEAR) < date_begin AND DATE_ADD(birthday, INTERVAL 18 YEAR) > date_begin
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS t4;
CREATE TEMPORARY TABLE IF NOT EXISTS T4
SELECT T.date_begin, COUNT(T.val) AS val
FROM (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх001", "нстх001")
GROUP BY id_case, tooth_name
ORDER BY date_begin) T
GROUP BY date_begin;
DROP TEMPORARY TABLE if EXISTS t5;
CREATE TEMPORARY TABLE IF NOT EXISTS T5
SELECT T.date_begin, COUNT(T.val) AS val
FROM (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх001", "нстх001") AND DATE_ADD(birthday, INTERVAL 15 YEAR) > date_begin
GROUP BY id_case, tooth_name
ORDER BY date_begin) T
GROUP BY date_begin;
DROP TEMPORARY TABLE if EXISTS t6;
CREATE TEMPORARY TABLE IF NOT EXISTS T6
SELECT T.date_begin, COUNT(T.val) AS val
FROM (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх001", "нстх001") AND DATE_ADD(birthday, INTERVAL 15 YEAR) < date_begin AND DATE_ADD(birthday, INTERVAL 18 YEAR) > date_begin
GROUP BY id_case, tooth_name
ORDER BY date_begin) T
GROUP BY date_begin;
DROP TEMPORARY TABLE if EXISTS t7;
CREATE TEMPORARY TABLE IF NOT EXISTS T7
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх029", "стх030", "стх031", "стх034", "нстх029", "нстх030", "нстх031")
GROUP BY date_begin;
DROP TEMPORARY TABLE if EXISTS t8;
CREATE TEMPORARY TABLE IF NOT EXISTS T8
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх029", "стх030", "стх031", "стх034", "нстх029", "нстх030", "нстх031") AND DATE_ADD(birthday, INTERVAL 18 YEAR) <= date_begin
GROUP BY date_begin;
DROP TEMPORARY TABLE if EXISTS t9;
CREATE TEMPORARY TABLE IF NOT EXISTS T9
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх029", "стх030", "стх031", "стх034", "нстх029", "нстх030", "нстх031") AND DATE_ADD(birthday, INTERVAL 15 YEAR) > date_begin
GROUP BY date_begin;
DROP TEMPORARY TABLE if EXISTS t10;
CREATE TEMPORARY TABLE IF NOT EXISTS T10
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх030", "стх031", "стх034", "нстх030", "нстх031") AND DATE_ADD(birthday, INTERVAL 15 YEAR) > date_begin
GROUP BY date_begin;
DROP TEMPORARY TABLE if EXISTS t11;
CREATE TEMPORARY TABLE IF NOT EXISTS T11
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх030", "стх031", "стх034", "нстх030", "нстх031") AND DATE_ADD(birthday, INTERVAL 15 YEAR) < date_begin AND DATE_ADD(birthday, INTERVAL 18 YEAR) > date_begin
GROUP BY date_begin;
DROP TEMPORARY TABLE if EXISTS t12;
CREATE TEMPORARY TABLE IF NOT EXISTS T12
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх030", "стх029", "нстх029", "нстх030")
GROUP BY date_begin;
DROP TEMPORARY TABLE if EXISTS t13;
CREATE TEMPORARY TABLE IF NOT EXISTS T13
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх030", "стх029", "нстх029", "нстх030") AND DATE_ADD(birthday, INTERVAL 15 YEAR) > date_begin
GROUP BY date_begin;
DROP TEMPORARY TABLE if EXISTS t14;
CREATE TEMPORARY TABLE IF NOT EXISTS T14 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх034")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS t15;
CREATE TEMPORARY TABLE IF NOT EXISTS T15 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх034") AND DATE_ADD(birthday, INTERVAL 18 YEAR) <= date_begin
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS t16;
CREATE TEMPORARY TABLE IF NOT EXISTS T16 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх034") AND DATE_ADD(birthday, INTERVAL 18 YEAR) > date_begin
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS t17;
CREATE TEMPORARY TABLE IF NOT EXISTS T17 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх031", "нстх031")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS t18;
CREATE TEMPORARY TABLE IF NOT EXISTS T18 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх031", "нстх031") AND DATE_ADD(birthday, INTERVAL 18 YEAR) <= date_begin
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS t19;
CREATE TEMPORARY TABLE IF NOT EXISTS T19 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх031", "нстх031") AND DATE_ADD(birthday, INTERVAL 18 YEAR) > date_begin
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS t20;
CREATE TEMPORARY TABLE IF NOT EXISTS T20 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("сто001", "сто002", "сто003", "нсто001", "нсто002", "нсто003")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS t21;
CREATE TEMPORARY TABLE IF NOT EXISTS T21 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("сто001", "сто002", "сто003", "нсто001", "нсто002", "нсто003") AND DATE_ADD(birthday, INTERVAL 18 YEAR) <= date_begin
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS t22;
CREATE TEMPORARY TABLE IF NOT EXISTS T22 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("сто001", "сто002", "сто003", "нсто001", "нсто002", "нсто003") AND DATE_ADD(birthday, INTERVAL 18 YEAR) > date_begin
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS t22_dir;
CREATE TEMPORARY TABLE IF NOT EXISTS T22_dir (
SELECT date_begin, COUNT(dsv.id_case) AS val
FROM doctor_services dsv
JOIN directions drc ON dsv.id_case=drc.id_case
WHERE profile_infis_code IN ("сто001", "сто002", "сто003", "нсто001", "нсто002", "нсто003") AND DATE_ADD(birthday, INTERVAL 18 YEAR) > date_begin AND drc.id_ref=10 AND drc.active =0
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS t23;
CREATE TEMPORARY TABLE IF NOT EXISTS T23 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE pos_code IN ("1.6")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS t24;
CREATE TEMPORARY TABLE IF NOT EXISTS T24 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх003", "нстх003")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS t25;
CREATE TEMPORARY TABLE IF NOT EXISTS T25 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх005", "нстх005")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS t26;
CREATE TEMPORARY TABLE IF NOT EXISTS T26 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх006", "нстх006")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS t27;
CREATE TEMPORARY TABLE IF NOT EXISTS T27 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх007")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS T28;
CREATE TEMPORARY TABLE IF NOT EXISTS T28 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх008", "нстх008")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS T29;
CREATE TEMPORARY TABLE IF NOT EXISTS T29 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх009", "нстх009")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS T30;
CREATE TEMPORARY TABLE IF NOT EXISTS T30 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх012", "нстх012")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS T31;
CREATE TEMPORARY TABLE IF NOT EXISTS T31 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх015", "нстх015")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS T32;
CREATE TEMPORARY TABLE IF NOT EXISTS T32 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх016", "нстх016")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS T33;
CREATE TEMPORARY TABLE IF NOT EXISTS T33 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх017", "нстх017")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS T34;
CREATE TEMPORARY TABLE IF NOT EXISTS T34 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх018", "нстх018")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS T35;
CREATE TEMPORARY TABLE IF NOT EXISTS T35 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх019")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS T36;
CREATE TEMPORARY TABLE IF NOT EXISTS T36 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх025", "нстх025")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS T37;
CREATE TEMPORARY TABLE IF NOT EXISTS T37 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх026", "нстх026")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS T38;
CREATE TEMPORARY TABLE IF NOT EXISTS T38 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх027", "нстх027")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS T39;
CREATE TEMPORARY TABLE IF NOT EXISTS T39 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх033")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS T40;
CREATE TEMPORARY TABLE IF NOT EXISTS T40 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх037", "нстх037")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS T41;
CREATE TEMPORARY TABLE IF NOT EXISTS T41 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх038")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS T42;
CREATE TEMPORARY TABLE IF NOT EXISTS T42 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code IN ("стх039")
GROUP BY date_begin
ORDER BY date_begin);
DROP TEMPORARY TABLE if EXISTS tmp_1_21;
CREATE TEMPORARY TABLE tmp_1_21
SELECT fr.st_num, fr.st_date, COALESCE(T1.val, 0) AS v1, COALESCE(T2.val, 0) AS v2, COALESCE(T3.val, 0) AS v3, 
COALESCE(T4.val, 0) AS v4, COALESCE(T5.val, 0) AS v5, COALESCE(T6.val, 0) AS v6, COALESCE(T7.val, 0) AS v7, 
COALESCE(T8.val, 0) AS v8, COALESCE(T9.val, 0) AS v9, COALESCE(T10.val, 0) AS v10, COALESCE(T11.val, 0) AS v11, 
COALESCE(T12.val, 0) AS v12, COALESCE(T13.val, 0) AS v13, COALESCE(T14.val, 0) AS v14, COALESCE(T15.val, 0) AS v15,
 COALESCE(T16.val, 0) AS v16, COALESCE(T17.val, 0) AS v17, COALESCE(T18.val, 0) AS v18, COALESCE(T19.val, 0) AS v19, 
COALESCE(T20.val, 0) AS v20, COALESCE(T21.val, 0) AS v21, fr.st_uet
FROM first_row fr
LEFT JOIN T1 ON T1.date_begin = fr.st_date
LEFT JOIN T2 ON T2.date_begin = fr.st_date
LEFT JOIN T3 ON T3.date_begin = fr.st_date
LEFT JOIN T4 ON T4.date_begin = fr.st_date
LEFT JOIN T5 ON T5.date_begin = fr.st_date
LEFT JOIN T6 ON T6.date_begin = fr.st_date
LEFT JOIN T7 ON T7.date_begin = fr.st_date
LEFT JOIN T8 ON T8.date_begin = fr.st_date
LEFT JOIN T9 ON T9.date_begin = fr.st_date
LEFT JOIN T10 ON T10.date_begin = fr.st_date
LEFT JOIN T11 ON T11.date_begin = fr.st_date
LEFT JOIN T12 ON T12.date_begin = fr.st_date
LEFT JOIN T13 ON T13.date_begin = fr.st_date
LEFT JOIN T14 ON T14.date_begin = fr.st_date
LEFT JOIN T15 ON T15.date_begin = fr.st_date
LEFT JOIN T16 ON T16.date_begin = fr.st_date
LEFT JOIN T17 ON T17.date_begin = fr.st_date
LEFT JOIN T18 ON T18.date_begin = fr.st_date
LEFT JOIN T19 ON T19.date_begin = fr.st_date
LEFT JOIN T20 ON T20.date_begin = fr.st_date
LEFT JOIN T21 ON T21.date_begin = fr.st_date
ORDER BY st_num ;
DROP TEMPORARY TABLE if EXISTS tmp_22_42;
CREATE TEMPORARY TABLE tmp_22_42
SELECT tt.st_num, tt.st_date, tt.v1, tt.v2, tt.v3, tt.v4, tt.v5, tt.v6, tt.v7, tt.v8, tt.v9, tt.v10, tt.v11, tt.v12, 
tt.v13, tt.v14, tt.v15, tt.v16, tt.v17, tt.v18, tt.v19, tt.v20, COALESCE(T22_dir.val, 0) AS V22dir, tt.v21, 
COALESCE(T22.val, 0) AS v22, COALESCE(T23.val, 0) AS v23, COALESCE(T24.val, 0) AS v24, COALESCE(T25.val, 0) AS v25, 
COALESCE(T26.val, 0) AS v26, COALESCE(T27.val, 0) AS v27, COALESCE(T28.val, 0) AS v28, COALESCE(T29.val, 0) AS v29,
 COALESCE(T30.val, 0) AS v30, COALESCE(T31.val, 0) AS v31, COALESCE(T32.val, 0) AS v32, COALESCE(T33.val, 0) AS v33, 
COALESCE(T34.val, 0) AS v34, COALESCE(T35.val, 0) AS v35, COALESCE(T36.val, 0) AS v36, COALESCE(T37.val, 0) AS v37, 
COALESCE(T38.val, 0) AS v38, COALESCE(T39.val, 0) AS v39, COALESCE(T40.val, 0) AS v40, COALESCE(T41.val, 0) AS v41, COALESCE(T42.val, 0) AS v42, tt.st_uet
FROM tmp_1_21 tt
LEFT JOIN T22 ON T22.date_begin = tt.st_date
LEFT JOIN T22_dir ON T22_dir.date_begin=tt.st_date
LEFT JOIN T23 ON T23.date_begin = tt.st_date
LEFT JOIN T24 ON T24.date_begin = tt.st_date
LEFT JOIN T25 ON T25.date_begin = tt.st_date
LEFT JOIN T26 ON T26.date_begin = tt.st_date
LEFT JOIN T27 ON T27.date_begin = tt.st_date
LEFT JOIN T28 ON T28.date_begin = tt.st_date
LEFT JOIN T29 ON T29.date_begin = tt.st_date
LEFT JOIN T30 ON T30.date_begin = tt.st_date
LEFT JOIN T31 ON T31.date_begin = tt.st_date
LEFT JOIN T32 ON T32.date_begin = tt.st_date
LEFT JOIN T33 ON T33.date_begin = tt.st_date
LEFT JOIN T34 ON T34.date_begin = tt.st_date
LEFT JOIN T35 ON T35.date_begin = tt.st_date
LEFT JOIN T36 ON T36.date_begin = tt.st_date
LEFT JOIN T37 ON T37.date_begin = tt.st_date
LEFT JOIN T38 ON T38.date_begin = tt.st_date
LEFT JOIN T39 ON T39.date_begin = tt.st_date
LEFT JOIN T40 ON T40.date_begin = tt.st_date
LEFT JOIN T41 ON T41.date_begin = tt.st_date
LEFT JOIN T42 ON T42.date_begin = tt.st_date
ORDER BY st_num ;
SELECT *
FROM tmp_22_42;