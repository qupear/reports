
create temporary table if not exists all_data
select
		timestampdiff(day,min(et.date_begin), max(et.date_end)) + 1 as bold,
		e.id,
		e.pat_sex,
		e.eln_diagnos_code as edc,e.pat_birthday,
		case
			when ((e.pat_birthday + interval 15 YEAR - interval 1 DAY) < e.eln_date and (e.pat_birthday + interval 20 year) > e.eln_date) then 1
			else null
		end as a15_19,
		case
			when ((e.pat_birthday + interval 20 YEAR - interval 1 DAY) < e.eln_date and (e.pat_birthday + interval 25 year) > e.eln_date) then 1
			else null
		end as a20_24,
		case
			when ((e.pat_birthday + interval 25 YEAR - interval 1 DAY) < e.eln_date and (e.pat_birthday + interval 30 year) > e.eln_date) then 1
			else null
		end as a25_29,
		case
			when ((e.pat_birthday + interval 30 YEAR - interval 1 DAY) < e.eln_date and (e.pat_birthday + interval 35 year) > e.eln_date) then 1
			else null
		end as a30_34,
		case
			when ((e.pat_birthday + interval 35 YEAR - interval 1 DAY) < e.eln_date and (e.pat_birthday + interval 40 year) > e.eln_date) then 1
			else null
		end as a35_39,
		case
			when ((e.pat_birthday + interval 40 YEAR - interval 1 DAY) < e.eln_date and (e.pat_birthday + interval 45 year) > e.eln_date) then 1
			else null
		end as a40_44,
		case
			when ((e.pat_birthday + interval 45 YEAR - interval 1 DAY) < e.eln_date and (e.pat_birthday + interval 50 year) > e.eln_date) then 1
			else null
		end as a45_49,
		case
			when ((e.pat_birthday + interval 50 YEAR - interval 1 DAY) < e.eln_date and (e.pat_birthday + interval 55 year) > e.eln_date) then 1
			else null
		end as a50_54,
		case
			when ((e.pat_birthday + interval 55 YEAR - interval 1 DAY) < e.eln_date and (e.pat_birthday + interval 60 year) > e.eln_date) then 1
			else null
		end as a55_59,
		case
			when ((e.pat_birthday + interval 60 year - interval 1 day) < e.eln_date) then 1
			else null
		end as a60_older
	from
		eln e
	left join 
		eln_treats et on et.id_eln = e.id
	where date(fss_sync_date) between @date_start and @date_end
		and e.eln_diagnos_code in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6')
	group by e.id;


create temporary table if not exists a15_19
select
timestampdiff(day,min(et.date_begin), max(et.date_end)) + 1 as bold,
		e.id,
		e.pat_sex,
		e.eln_diagnos_code as edc,e.pat_birthday
from eln e
	left join 
		eln_treats et on et.id_eln = e.id
where date(fss_sync_date) between @date_start and @date_end
		and e.eln_diagnos_code in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6')
	and ((e.pat_birthday + interval 15 YEAR - interval 1 DAY) < e.eln_date and (e.pat_birthday + interval 20 year) > e.eln_date)
group by e.id;

create temporary table if not exists a20_24
select
timestampdiff(day,min(et.date_begin), max(et.date_end)) + 1 as bold,
		e.id,
		e.pat_sex,
		e.eln_diagnos_code as edc,e.pat_birthday
from eln e
	left join 
		eln_treats et on et.id_eln = e.id
where date(fss_sync_date) between @date_start and @date_end
		and e.eln_diagnos_code in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6')
	and ((e.pat_birthday + interval 20 YEAR - interval 1 DAY) < e.eln_date and (e.pat_birthday + interval 25 year) > e.eln_date)
group by e.id;

create temporary table if not exists a25_29
select
timestampdiff(day,min(et.date_begin), max(et.date_end)) + 1 as bold,
		e.id,
		e.pat_sex,
		e.eln_diagnos_code as edc,e.pat_birthday
from eln e
	left join 
		eln_treats et on et.id_eln = e.id
where date(fss_sync_date) between @date_start and @date_end
		and e.eln_diagnos_code in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6')
	and ((e.pat_birthday + interval 25 YEAR - interval 1 DAY) < e.eln_date and (e.pat_birthday + interval 30 year) > e.eln_date)
group by e.id;

create temporary table if not exists a30_34
select
timestampdiff(day,min(et.date_begin), max(et.date_end)) + 1 as bold,
		e.id,
		e.pat_sex,
		e.eln_diagnos_code as edc,e.pat_birthday
from eln e
	left join 
		eln_treats et on et.id_eln = e.id
where date(fss_sync_date) between @date_start and @date_end
		and e.eln_diagnos_code in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6')
	and ((e.pat_birthday + interval 30 YEAR - interval 1 DAY) < e.eln_date and (e.pat_birthday + interval 35 year) > e.eln_date)
group by e.id;

create temporary table if not exists a35_39
select
timestampdiff(day,min(et.date_begin), max(et.date_end)) + 1 as bold,
		e.id,
		e.pat_sex,
		e.eln_diagnos_code as edc,e.pat_birthday
from eln e
	left join 
		eln_treats et on et.id_eln = e.id
where date(fss_sync_date) between @date_start and @date_end
		and e.eln_diagnos_code in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6')
	and ((e.pat_birthday + interval 35 YEAR - interval 1 DAY) < e.eln_date and (e.pat_birthday + interval 40 year) > e.eln_date)
group by e.id;

create temporary table if not exists a40_44
select
timestampdiff(day,min(et.date_begin), max(et.date_end)) + 1 as bold,
		e.id,
		e.pat_sex,
		e.eln_diagnos_code as edc,e.pat_birthday
from eln e
	left join 
		eln_treats et on et.id_eln = e.id
where date(fss_sync_date) between @date_start and @date_end
		and e.eln_diagnos_code in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6')
	and ((e.pat_birthday + interval 40 YEAR - interval 1 DAY) < e.eln_date and (e.pat_birthday + interval 45 year) > e.eln_date)
group by e.id;

create temporary table if not exists a45_49
select
timestampdiff(day,min(et.date_begin), max(et.date_end)) + 1 as bold,
		e.id,
		e.pat_sex,
		e.eln_diagnos_code as edc,e.pat_birthday
from eln e
	left join 
		eln_treats et on et.id_eln = e.id
where date(fss_sync_date) between @date_start and @date_end
		and e.eln_diagnos_code in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6')
	and ((e.pat_birthday + interval 45 YEAR - interval 1 DAY) < e.eln_date and (e.pat_birthday + interval 50 year) > e.eln_date)
group by e.id;

create temporary table if not exists a50_54
select
timestampdiff(day,min(et.date_begin), max(et.date_end)) + 1 as bold,
		e.id,
		e.pat_sex,
		e.eln_diagnos_code as edc,e.pat_birthday
from eln e
	left join 
		eln_treats et on et.id_eln = e.id
where date(fss_sync_date) between @date_start and @date_end
		and e.eln_diagnos_code in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6')
	and ((e.pat_birthday + interval 50 YEAR - interval 1 DAY) < e.eln_date and (e.pat_birthday + interval 55 year) > e.eln_date)
group by e.id;

create temporary table if not exists a55_59
select
timestampdiff(day,min(et.date_begin), max(et.date_end)) + 1 as bold,
		e.id,
		e.pat_sex,
		e.eln_diagnos_code as edc,e.pat_birthday
from eln e
	left join 
		eln_treats et on et.id_eln = e.id
where date(fss_sync_date) between @date_start and @date_end
		and e.eln_diagnos_code in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6')
	and ((e.pat_birthday + interval 55 YEAR - interval 1 DAY) < e.eln_date and (e.pat_birthday + interval 60 year) > e.eln_date)
group by e.id;

create temporary table if not exists a60_older
select
timestampdiff(day,min(et.date_begin), max(et.date_end)) + 1 as bold,
		e.id,
		e.pat_sex,
		e.eln_diagnos_code as edc,e.pat_birthday
from eln e
	left join 
		eln_treats et on et.id_eln = e.id
where date(fss_sync_date) between @date_start and @date_end
		and e.eln_diagnos_code in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6')
	and ((e.pat_birthday + interval 60 year - interval 1 day) < e.eln_date)
group by e.id;



/*Эндогенный заболевания, Кол-во случаев, М*/
create temporary table if not exists EZ_case_m
select 
	count(distinct id) as id,
	count(a15_19) as a15_19,
	count(a20_24) as a20_24,
	count(a25_29) as a25_29,
	count(a30_34) as a30_34,
	count(a35_39) as a35_39,
	count(a40_44) as a40_44,
	count(a45_49) as a45_49,
	count(a50_54) as a50_54,
	count(a55_59) as a55_59,
	count(a60_older) as a60_older
from
	all_data ad
where pat_sex = 0
and edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2');

set @s1_0 = 'Болезни органов пищеварения ';
set @s1_1 = 'Число случаев ВН';
set @s1_2 = 'М';
set @s1_3 = (select a15_19	from EZ_case_m);
set @s1_4 = (select a20_24	from EZ_case_m);
set @s1_5 = (select a25_29	from EZ_case_m);
set @s1_6 = (select a30_34	from EZ_case_m);
set @s1_7 = (select a35_39	from EZ_case_m);
set @s1_8 = (select a40_44	from EZ_case_m);
set @s1_9 = (select a45_49	from EZ_case_m);
set @s1_10 = (select a50_54	from EZ_case_m);
set @s1_11 = (select a55_59	from EZ_case_m);
set @s1_12 = (select a60_older	from EZ_case_m);
set @s1_13 = (select id from EZ_case_m);

/*ЭЗ, случаи, Ж*/
create temporary table if not exists EZ_case_f
select 
	count(distinct id) as id,
	count(a15_19) as a15_19,
	count(a20_24) as a20_24,
	count(a25_29) as a25_29,
	count(a30_34) as a30_34,
	count(a35_39) as a35_39,
	count(a40_44) as a40_44,
	count(a45_49) as a45_49,
	count(a50_54) as a50_54,
	count(a55_59) as a55_59,
	count(a60_older) as a60_older
from
	all_data ad
where pat_sex = 1
and edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2');

set @s2_0 = ' ';
set @s2_1 = '';
set @s2_2 = 'Ж';
set @s2_3 = (select a15_19	from EZ_case_f);
set @s2_4 = (select a20_24	from EZ_case_f);
set @s2_5 = (select a25_29	from EZ_case_f);
set @s2_6 = (select a30_34	from EZ_case_f);
set @s2_7 = (select a35_39	from EZ_case_f);
set @s2_8 = (select a40_44	from EZ_case_f);
set @s2_9 = (select a45_49	from EZ_case_f);
set @s2_10 = (select a50_54	from EZ_case_f);
set @s2_11 = (select a55_59	from EZ_case_f);
set @s2_12 = (select a60_older	from EZ_case_f);
set @s2_13 = (select id from EZ_case_f);

/*ЭЗ, дни, М*/
create temporary table if not exists EZ_day_m
select 
	Sum(bold) as bold,
	(select Sum(bold) from a15_19 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2') and pat_sex = 0) as a15_19,
	(select Sum(bold) from a20_24 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2') and pat_sex = 0) as a20_24,
	(select Sum(bold) from a25_29 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2') and pat_sex = 0) as a25_29,
	(select Sum(bold) from a30_34 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2') and pat_sex = 0) as a30_34,
	(select Sum(bold) from a35_39 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2') and pat_sex = 0) as a35_39,
	(select Sum(bold) from a40_44 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2') and pat_sex = 0) as a40_44,
	(select Sum(bold) from a45_49 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2') and pat_sex = 0) as a45_49,
	(select Sum(bold) from a50_54 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2') and pat_sex = 0) as a50_54,
	(select Sum(bold) from a55_59 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2') and pat_sex = 0) as a55_59,
	(select Sum(bold) from a60_older where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2'))  as a60_older
from
	all_data ad
where pat_sex = 0
and edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2');

set @s3_0 = '';
set @s3_1 = 'Число Дней ВН';
set @s3_2 = 'М';
set @s3_3 = (select a15_19	from EZ_day_m);
set @s3_4 = (select a20_24	from EZ_day_m);
set @s3_5 = (select a25_29	from EZ_day_m);
set @s3_6 = (select a30_34	from EZ_day_m);
set @s3_7 = (select a35_39	from EZ_day_m);
set @s3_8 = (select a40_44	from EZ_day_m);
set @s3_9 = (select a45_49	from EZ_day_m);
set @s3_10 = (select a50_54	from EZ_day_m);
set @s3_11 = (select a55_59	from EZ_day_m);
set @s3_12 = (select a60_older	from EZ_day_m);
set @s3_13 = (select bold from EZ_day_m);

/*ЭЗ, дни, Ж*/
create temporary table if not exists EZ_day_f
select 
	Sum(bold) as bold,
	(select Sum(bold) from a15_19 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2') and pat_sex = 1) as a15_19,
	(select Sum(bold) from a20_24 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2') and pat_sex = 1) as a20_24,
	(select Sum(bold) from a25_29 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2') and pat_sex = 1) as a25_29,
	(select Sum(bold) from a30_34 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2') and pat_sex = 1) as a30_34,
	(select Sum(bold) from a35_39 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2') and pat_sex = 1) as a35_39,
	(select Sum(bold) from a40_44 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2') and pat_sex = 1) as a40_44,
	(select Sum(bold) from a45_49 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2') and pat_sex = 1) as a45_49,
	(select Sum(bold) from a50_54 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2') and pat_sex = 1) as a50_54,
	(select Sum(bold) from a55_59 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2') and pat_sex = 1) as a55_59,
	(select Sum(bold) from a60_older where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2') and pat_sex = 1)  as a60_older
from
	all_data ad
where pat_sex = 1
and edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2');

set @s4_0 = ' ';
set @s4_1 = '';
set @s4_2 = 'Ж';
set @s4_3 = (select a15_19	from EZ_day_f);
set @s4_4 = (select a20_24	from EZ_day_f);
set @s4_5 = (select a25_29	from EZ_day_f);
set @s4_6 = (select a30_34	from EZ_day_f);
set @s4_7 = (select a35_39	from EZ_day_f);
set @s4_8 = (select a40_44	from EZ_day_f);
set @s4_9 = (select a45_49	from EZ_day_f);
set @s4_10 = (select a50_54	from EZ_day_f);
set @s4_11 = (select a55_59	from EZ_day_f);
set @s4_12 = (select a60_older	from EZ_day_f);
set @s4_13 = (select bold from EZ_day_f);

/*ЭЗ, среднее дней за случаи*/
create temporary table if not exists EZ_full
select 
	Sum(bold) as bold,
	count(distinct id) as id,
	round((sum(bold)/count(id)), 2) as avrg,
	(select sum(bold)/count(id) from a15_19 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2')) as a15_19,
	(select sum(bold)/count(id) from a20_24 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2')) as a20_24,
	(select sum(bold)/count(id) from a25_29 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2')) as a25_29,
	(select sum(bold)/count(id) from a30_34 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2')) as a30_34,
	(select sum(bold)/count(id) from a35_39 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2')) as a35_39,
	(select sum(bold)/count(id) from a40_44 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2')) as a40_44,
	(select sum(bold)/count(id) from a45_49 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2')) as a45_49,
	(select sum(bold)/count(id) from a50_54 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2')) as a50_54,
	(select sum(bold)/count(id) from a55_59 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2')) as a55_59,
	(select sum(bold)/count(id) from a60_older where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2')) as a60_older
from
	all_data ad
where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2');


set @s5_0 = '';
set @s5_1 = 'Средняя продолжительность ЛВН';
set @s5_2 = '';
set @s5_3 = (select a15_19	from EZ_full);
set @s5_4 = (select a20_24	from EZ_full);
set @s5_5 = (select a25_29	from EZ_full);
set @s5_6 = (select a30_34	from EZ_full);
set @s5_7 = (select a35_39	from EZ_full);
set @s5_8 = (select a40_44	from EZ_full);
set @s5_9 = (select a45_49	from EZ_full);
set @s5_10 = (select a50_54	from EZ_full);
set @s5_11 = (select a55_59	from EZ_full);
set @s5_12 = (select a60_older	from EZ_full);
set @s5_13 = (select avrg from EZ_full);

/*Переломы-травмы, случаи, М*/
create temporary table if not exists TP_case_m
select 
	count(distinct id) as id,
	count(a15_19) as a15_19,
	count(a20_24) as a20_24,
	count(a25_29) as a25_29,
	count(a30_34) as a30_34,
	count(a35_39) as a35_39,
	count(a40_44) as a40_44,
	count(a45_49) as a45_49,
	count(a50_54) as a50_54,
	count(a55_59) as a55_59,
	count(a60_older) as a60_older
from
	all_data ad
where pat_sex = 0
and edc in ('S02.4', 'S02.6');

set @s6_0 = 'Травмы, отравления и некоторые другие последствия воздействия внешних причин';
set @s6_1 = 'Число случаев ВН';
set @s6_2 = 'М';
set @s6_3 = (select a15_19	from TP_case_m);
set @s6_4 = (select a20_24	from TP_case_m);
set @s6_5 = (select a25_29	from TP_case_m);
set @s6_6 = (select a30_34	from TP_case_m);
set @s6_7 = (select a35_39	from TP_case_m);
set @s6_8 = (select a40_44	from TP_case_m);
set @s6_9 = (select a45_49	from TP_case_m);
set @s6_10 = (select a50_54	from TP_case_m);
set @s6_11 = (select a55_59	from TP_case_m);
set @s6_12 = (select a60_older	from TP_case_m);
set @s6_13 = (select id from TP_case_m);

/*ПТ, случаи, Ж*/
create temporary table if not exists TP_case_f
select 
	count(distinct id) as id,
	count(a15_19) as a15_19,
	count(a20_24) as a20_24,
	count(a25_29) as a25_29,
	count(a30_34) as a30_34,
	count(a35_39) as a35_39,
	count(a40_44) as a40_44,
	count(a45_49) as a45_49,
	count(a50_54) as a50_54,
	count(a55_59) as a55_59,
	count(a60_older) as a60_older
from
	all_data ad
where pat_sex = 1
and edc in ('S02.4', 'S02.6');

set @s7_0 = '		';
set @s7_1 = '		';
set @s7_2 = 'Ж';
set @s7_3 = (select a15_19	from TP_case_f);
set @s7_4 = (select a20_24	from TP_case_f);
set @s7_5 = (select a25_29	from TP_case_f);
set @s7_6 = (select a30_34	from TP_case_f);
set @s7_7 = (select a35_39	from TP_case_f);
set @s7_8 = (select a40_44	from TP_case_f);
set @s7_9 = (select a45_49	from TP_case_f);
set @s7_10 = (select a50_54	from TP_case_f);
set @s7_11 = (select a55_59	from TP_case_f);
set @s7_12 = (select a60_older	from TP_case_f);
set @s7_13 = (select id from TP_case_f);

/*ПТ, дни, М*/
create temporary table if not exists TP_day_m
select 
	Sum(bold) as bold,
	(select Sum(bold) from a15_19 where edc in ('S02.4', 'S02.6') and pat_sex = 0) as a15_19,
	(select Sum(bold) from a20_24 where edc in ('S02.4', 'S02.6') and pat_sex = 0) as a20_24,
	(select Sum(bold) from a25_29 where edc in ('S02.4', 'S02.6') and pat_sex = 0) as a25_29,
	(select Sum(bold) from a30_34 where edc in ('S02.4', 'S02.6') and pat_sex = 0) as a30_34,
	(select Sum(bold) from a35_39 where edc in ('S02.4', 'S02.6') and pat_sex = 0) as a35_39,
	(select Sum(bold) from a40_44 where edc in ('S02.4', 'S02.6') and pat_sex = 0) as a40_44,
	(select Sum(bold) from a45_49 where edc in ('S02.4', 'S02.6') and pat_sex = 0) as a45_49,
	(select Sum(bold) from a50_54 where edc in ('S02.4', 'S02.6') and pat_sex = 0) as a50_54,
	(select Sum(bold) from a55_59 where edc in ('S02.4', 'S02.6') and pat_sex = 0) as a55_59,
	(select Sum(bold) from a60_older where edc in ('S02.4', 'S02.6') and pat_sex = 0)  as a60_older
from
	all_data ad
where pat_sex = 0
and edc in ('S02.4', 'S02.6');

set @s8_0 = '		';
set @s8_1 = 'Число дней ВН';
set @s8_2 = 'М';
set @s8_3 = (select a15_19	from TP_day_m);
set @s8_4 = (select a20_24	from TP_day_m);
set @s8_5 = (select a25_29	from TP_day_m);
set @s8_6 = (select a30_34	from TP_day_m);
set @s8_7 = (select a35_39	from TP_day_m);
set @s8_8 = (select a40_44	from TP_day_m);
set @s8_9 = (select a45_49	from TP_day_m);
set @s8_10 = (select a50_54	from TP_day_m);
set @s8_11 = (select a55_59	from TP_day_m);
set @s8_12 = (select a60_older	from TP_day_m);
set @s8_13 = (select bold from TP_day_m);

/*ПТ, дни, Ж*/
create temporary table if not exists TP_day_f
select 
	Sum(bold) as bold,
	(select Sum(bold) from a15_19 where edc in ('S02.4', 'S02.6') and pat_sex = 1) as a15_19,
	(select Sum(bold) from a20_24 where edc in ('S02.4', 'S02.6') and pat_sex = 1) as a20_24,
	(select Sum(bold) from a25_29 where edc in ('S02.4', 'S02.6') and pat_sex = 1) as a25_29,
	(select Sum(bold) from a30_34 where edc in ('S02.4', 'S02.6') and pat_sex = 1) as a30_34,
	(select Sum(bold) from a35_39 where edc in ('S02.4', 'S02.6') and pat_sex = 1) as a35_39,
	(select Sum(bold) from a40_44 where edc in ('S02.4', 'S02.6') and pat_sex = 1) as a40_44,
	(select Sum(bold) from a45_49 where edc in ('S02.4', 'S02.6') and pat_sex = 1) as a45_49,
	(select Sum(bold) from a50_54 where edc in ('S02.4', 'S02.6') and pat_sex = 1) as a50_54,
	(select Sum(bold) from a55_59 where edc in ('S02.4', 'S02.6') and pat_sex = 1) as a55_59,
	(select Sum(bold) from a60_older where edc in ('S02.4', 'S02.6') and pat_sex = 1)  as a60_older
from
	all_data ad
where pat_sex = 1
and edc in ('S02.4', 'S02.6');

set @s9_0 = '		';
set @s9_1 = '		';
set @s9_2 = 'Ж';
set @s9_3 = (select a15_19	from TP_day_f);
set @s9_4 = (select a20_24	from TP_day_f);
set @s9_5 = (select a25_29	from TP_day_f);
set @s9_6 = (select a30_34	from TP_day_f);
set @s9_7 = (select a35_39	from TP_day_f);
set @s9_8 = (select a40_44	from TP_day_f);
set @s9_9 = (select a45_49	from TP_day_f);
set @s9_10 = (select a50_54	from TP_day_f);
set @s9_11 = (select a55_59	from TP_day_f);
set @s9_12 = (select a60_older	from TP_day_f);
set @s9_13 = (select bold from TP_day_f);

/*ПТ, среднее*/
create temporary table if not exists TP_full
select 
	Sum(bold) as bold,
	count(distinct id) as id,
	round((sum(bold)/count(id)), 2) as avrg,
	(select sum(bold)/count(id) from a15_19 where edc in ('S02.4', 'S02.6')) as a15_19,
	(select sum(bold)/count(id) from a20_24 where edc in ('S02.4', 'S02.6')) as a20_24,
	(select sum(bold)/count(id) from a25_29 where edc in ('S02.4', 'S02.6')) as a25_29,
	(select sum(bold)/count(id) from a30_34 where edc in ('S02.4', 'S02.6')) as a30_34,
	(select sum(bold)/count(id) from a35_39 where edc in ('S02.4', 'S02.6')) as a35_39,
	(select sum(bold)/count(id) from a40_44 where edc in ('S02.4', 'S02.6')) as a40_44,
	(select sum(bold)/count(id) from a45_49 where edc in ('S02.4', 'S02.6')) as a45_49,
	(select sum(bold)/count(id) from a50_54 where edc in ('S02.4', 'S02.6')) as a50_54,
	(select sum(bold)/count(id) from a55_59 where edc in ('S02.4', 'S02.6')) as a55_59,
	(select sum(bold)/count(id) from a60_older where edc in ('S02.4', 'S02.6')) as a60_older
from
	all_data ad
where edc in ('S02.4', 'S02.6');

set @s10_0 = '		';
set @s10_1 = 'Средняя продолжительность ЛВН';
set @s10_2 = '		';
set @s10_3 = (select a15_19	from TP_full);
set @s10_4 = (select a20_24	from TP_full);
set @s10_5 = (select a25_29	from TP_full);
set @s10_6 = (select a30_34	from TP_full);
set @s10_7 = (select a35_39	from TP_full);
set @s10_8 = (select a40_44	from TP_full);
set @s10_9 = (select a45_49	from TP_full);
set @s10_10 = (select a50_54	from TP_full);
set @s10_11 = (select a55_59	from TP_full);
set @s10_12 = (select a60_older	from TP_full);
set @s10_13 = (select avrg from TP_full);

/*Всего, случаи, М*/
create temporary table if not exists OA_case_m
select 
	count(distinct id) as id,
	count(a15_19) as a15_19,
	count(a20_24) as a20_24,
	count(a25_29) as a25_29,
	count(a30_34) as a30_34,
	count(a35_39) as a35_39,
	count(a40_44) as a40_44,
	count(a45_49) as a45_49,
	count(a50_54) as a50_54,
	count(a55_59) as a55_59,
	count(a60_older) as a60_older
from
	all_data ad
where pat_sex = 0
and edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6');

set @s11_0 = 'Итого по всем причиным';
set @s11_1 = 'Число случаев ВН';
set @s11_2 = 'М';
set @s11_3 = (select a15_19	from OA_case_m);
set @s11_4 = (select a20_24	from OA_case_m);
set @s11_5 = (select a25_29	from OA_case_m);
set @s11_6 = (select a30_34	from OA_case_m);
set @s11_7 = (select a35_39	from OA_case_m);
set @s11_8 = (select a40_44	from OA_case_m);
set @s11_9 = (select a45_49	from OA_case_m);
set @s11_10 = (select a50_54	from OA_case_m);
set @s11_11 = (select a55_59	from OA_case_m);
set @s11_12 = (select a60_older	from OA_case_m);
set @s11_13 = (select id from OA_case_m);

/*Всего, случаи, Ж*/
create temporary table if not exists OA_case_f
select 
	count(distinct id) as id,
	count(a15_19) as a15_19,
	count(a20_24) as a20_24,
	count(a25_29) as a25_29,
	count(a30_34) as a30_34,
	count(a35_39) as a35_39,
	count(a40_44) as a40_44,
	count(a45_49) as a45_49,
	count(a50_54) as a50_54,
	count(a55_59) as a55_59,
	count(a60_older) as a60_older
from
	all_data ad
where pat_sex = 1
and edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6');

set @s12_0 = '	 ';
set @s12_1 = '	 ';
set @s12_2 = 'Ж';
set @s12_3 = (select a15_19	from OA_case_f);
set @s12_4 = (select a20_24	from OA_case_f);
set @s12_5 = (select a25_29	from OA_case_f);
set @s12_6 = (select a30_34	from OA_case_f);
set @s12_7 = (select a35_39	from OA_case_f);
set @s12_8 = (select a40_44	from OA_case_f);
set @s12_9 = (select a45_49	from OA_case_f);
set @s12_10 = (select a50_54	from OA_case_f);
set @s12_11 = (select a55_59	from OA_case_f);
set @s12_12 = (select a60_older	from OA_case_f);
set @s12_13 = (select id from OA_case_f);

/*Всего, дни. М*/
create temporary table if not exists OA_day_m
select 
	Sum(bold) as bold,
	(select Sum(bold) from a15_19 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6') and pat_sex = 0) as a15_19,
	(select Sum(bold) from a20_24 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6') and pat_sex = 0) as a20_24,
	(select Sum(bold) from a25_29 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6') and pat_sex = 0) as a25_29,
	(select Sum(bold) from a30_34 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6') and pat_sex = 0) as a30_34,
	(select Sum(bold) from a35_39 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6') and pat_sex = 0) as a35_39,
	(select Sum(bold) from a40_44 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6') and pat_sex = 0) as a40_44,
	(select Sum(bold) from a45_49 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6') and pat_sex = 0) as a45_49,
	(select Sum(bold) from a50_54 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6') and pat_sex = 0) as a50_54,
	(select Sum(bold) from a55_59 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6') and pat_sex = 0) as a55_59,
	(select Sum(bold) from a60_older where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6') and pat_sex = 0)  as a60_older
from
	all_data ad
where pat_sex = 0
and edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6');

set @s13_0 = '	 ';
set @s13_1 = 'Число Дней ВН';
set @s13_2 = 'М';
set @s13_3 = (select a15_19	from OA_day_m);
set @s13_4 = (select a20_24	from OA_day_m);
set @s13_5 = (select a25_29	from OA_day_m);
set @s13_6 = (select a30_34	from OA_day_m);
set @s13_7 = (select a35_39	from OA_day_m);
set @s13_8 = (select a40_44	from OA_day_m);
set @s13_9 = (select a45_49	from OA_day_m);
set @s13_10 = (select a50_54	from OA_day_m);
set @s13_11 = (select a55_59	from OA_day_m);
set @s13_12 = (select a60_older	from OA_day_m);
set @s13_13 = (select bold from OA_day_m);

/*Всего, дни, Ж*/
create temporary table if not exists OA_day_f
select 
	Sum(bold) as bold,
	(select Sum(bold) from a15_19 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6') and pat_sex = 1) as a15_19,
	(select Sum(bold) from a20_24 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6') and pat_sex = 1) as a20_24,
	(select Sum(bold) from a25_29 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6') and pat_sex = 1) as a25_29,
	(select Sum(bold) from a30_34 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6') and pat_sex = 1) as a30_34,
	(select Sum(bold) from a35_39 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6') and pat_sex = 1) as a35_39,
	(select Sum(bold) from a40_44 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6') and pat_sex = 1) as a40_44,
	(select Sum(bold) from a45_49 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6') and pat_sex = 1) as a45_49,
	(select Sum(bold) from a50_54 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6') and pat_sex = 1) as a50_54,
	(select Sum(bold) from a55_59 where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6') and pat_sex = 1) as a55_59,
	(select Sum(bold) from a60_older where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6') and pat_sex = 1)  as a60_older
from
	all_data ad
where pat_sex = 1
and edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6');

set @s14_0 = ' 	';
set @s14_1 = '	 ';
set @s14_2 = 'Ж';
set @s14_3 = (select a15_19	from OA_day_f);
set @s14_4 = (select a20_24	from OA_day_f);
set @s14_5 = (select a25_29	from OA_day_f);
set @s14_6 = (select a30_34	from OA_day_f);
set @s14_7 = (select a35_39	from OA_day_f);
set @s14_8 = (select a40_44	from OA_day_f);
set @s14_9 = (select a45_49	from OA_day_f);
set @s14_10 = (select a50_54	from OA_day_f);
set @s14_11 = (select a55_59	from OA_day_f);
set @s14_12 = (select a60_older	from OA_day_f);
set @s14_13 = (select bold from OA_day_f);

/*Всего, среднее*/
create temporary table if not exists OA_full
select 
	Sum(bold) as bold,
	count(distinct id) as id,
	round((sum(bold)/count(id)), 2) as avrg,
	(select sum(bold)/count(id) from a15_19) as a15_19,
	(select sum(bold)/count(id) from a20_24) as a20_24,
	(select sum(bold)/count(id) from a25_29) as a25_29,
	(select sum(bold)/count(id) from a30_34) as a30_34,
	(select sum(bold)/count(id) from a35_39) as a35_39,
	(select sum(bold)/count(id) from a40_44) as a40_44,
	(select sum(bold)/count(id) from a45_49) as a45_49,
	(select sum(bold)/count(id) from a50_54) as a50_54,
	(select sum(bold)/count(id) from a55_59) as a55_59,
	(select sum(bold)/count(id) from a60_older) as a60_older
from
	all_data ad
where edc in ('K01.0', 'K04.4', 'K04.5', 'K10.2', 'S02.4', 'S02.6');

set @s15_0 = ' 	';
set @s15_1 = 'Средняя продолжительность ЛВН';
set @s15_2 = '	 ';
set @s15_3 = (select a15_19	from OA_full);
set @s15_4 = (select a20_24	from OA_full);
set @s15_5 = (select a25_29	from OA_full);
set @s15_6 = (select a30_34	from OA_full);
set @s15_7 = (select a35_39	from OA_full);
set @s15_8 = (select a40_44	from OA_full);
set @s15_9 = (select a45_49	from OA_full);
set @s15_10 = (select a50_54	from OA_full);
set @s15_11 = (select a55_59	from OA_full);
set @s15_12 = (select a60_older	from OA_full);
set @s15_13 = (select avrg from OA_full);

/*ЭЗ, Случаи, М*/
select @s1_0, @s1_1, @s1_2, @s1_3, @s1_4, @s1_5, @s1_6, @s1_7, @s1_8, @s1_9, @s1_10, @s1_11, @s1_12, @s1_13
union
/*ЭЗ, случаи, Ж*/
select @s2_0, @s2_1, @s2_2, @s2_3, @s2_4, @s2_5, @s2_6, @s2_7, @s2_8, @s2_9, @s2_10, @s2_11, @s2_12, @s2_13
union
/*ЭЗ, дни, М*/
select @s3_0, @s3_1, @s3_2, @s3_3, @s3_4, @s3_5, @s3_6, @s3_7, @s3_8, @s3_9, @s3_10, @s3_11, @s3_12, @s3_13
union
/*ЭЗ, дни, Ж*/
select @s4_0, @s4_1, @s4_2, @s4_3, @s4_4, @s4_5, @s4_6, @s4_7, @s4_8, @s4_9, @s4_10, @s4_11, @s4_12, @s4_13
union
/*ЭЗ, среднее*/
select @s5_0, @s5_1, @s5_2, @s5_3, @s5_4, @s5_5, @s5_6, @s5_7, @s5_8, @s5_9, @s5_10, @s5_11, @s5_12, @s5_13
union
/*ПТ, случаи, М*/
select @s6_0, @s6_1, @s6_2, @s6_3, @s6_4, @s6_5, @s6_6, @s6_7, @s6_8, @s6_9, @s6_10, @s6_11, @s6_12, @s6_13
union
/*ПТ, случаи, Ж*/
select @s7_0, @s7_1, @s7_2, @s7_3, @s7_4, @s7_5, @s7_6, @s7_7, @s7_8, @s7_9, @s7_10, @s7_11, @s7_12, @s7_13
union
/*ПТ, дни, М*/
select @s8_0, @s8_1, @s8_2, @s8_3, @s8_4, @s8_5, @s8_6, @s8_7, @s8_8, @s8_9, @s8_10, @s8_11, @s8_12, @s8_13
union
/*ПТ, дни, Ж*/
select @s9_0, @s9_1, @s9_2, @s9_3, @s9_4, @s9_5, @s9_6, @s9_7, @s9_8, @s9_9, @s9_10, @s9_11, @s9_12, @s9_13
union
/*ПТ, среднее*/
select @s10_0, @s10_1, @s10_2, @s10_3, @s10_4, @s10_5, @s10_6, @s10_7, @s10_8, @s10_9, @s10_10, @s10_11, @s10_12, @s10_13
union
/*Всего, случаи, М*/
select @s11_0, @s11_1, @s11_2, @s11_3, @s11_4, @s11_5, @s11_6, @s11_7, @s11_8, @s11_9, @s11_10, @s11_11, @s11_12, @s11_13
union
/*Всего, случаи, Ж*/
select @s12_0, @s12_1, @s12_2, @s12_3, @s12_4, @s12_5, @s12_6, @s12_7, @s12_8, @s12_9, @s12_10, @s12_11, @s12_12, @s12_13
union
/*Всего, дни, М*/
select @s13_0, @s13_1, @s13_2, @s13_3, @s13_4, @s13_5, @s13_6, @s13_7, @s13_8, @s13_9, @s13_10, @s13_11, @s13_12, @s13_13
union
/*Всего, дни, Ж*/
select @s14_0, @s14_1, @s14_2, @s14_3, @s14_4, @s14_5, @s14_6, @s14_7, @s14_8, @s14_9, @s14_10, @s14_11, @s14_12, @s14_13
union
/*Всего, среднек*/
select @s15_0, @s15_1, @s15_2, @s15_3, @s15_4, @s15_5, @s15_6, @s15_7, @s15_8, @s15_9, @s15_10, @s15_11, @s15_12, @s15_13
