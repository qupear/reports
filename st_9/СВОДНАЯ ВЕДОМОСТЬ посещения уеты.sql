set @date_start = '2020-01-01';
set @date_end = '2020-10-31';
set @depart_id = 0;


DROP TEMPORARY TABLE IF EXISTS doctor_services;
CREATE TEMPORARY TABLE IF NOT EXISTS doctor_services
    (SELECT c.ID_DOCTOR AS dokt,
            CONCAT(SUBSTRING_INDEX(u.name, ' ', 1), ' ', CONCAT(LEFT( SUBSTRING_INDEX(SUBSTRING_INDEX(u.name, ' ', 2), ' ', -1),  1),  '.'),
                   ' ', CONCAT(LEFT(SUBSTRING_INDEX(SUBSTRING_INDEX(u.name, ' ', 3), ' ', -1),   1), '.')) AS fioD,
            dep.name AS dptNm,
            cs.date_begin, cs.id_case,
            CASE WHEN vp.profile_infis_code IS NOT NULL THEN 1 ELSE 0 END AS pic,
            CASE WHEN p.code IS NOT NULL THEN 1 ELSE 0 END AS pcode,
            CASE WHEN p.UET IS NOT NULL THEN p.uet ELSE 0 END AS puet,
            CASE WHEN pr.UET IS NOT NULL THEN pr.uet ELSE 0 END AS pruet,
            CASE WHEN pr.CODE IS NOT NULL THEN pr.CODE ELSE 0 END AS prcode,
            CASE WHEN T.uet IS NOT NULL THEN TRUNCATE(T.uet, 2) ELSE 0 END AS tuet,
            c.is_closed
     FROM cases c
              LEFT JOIN case_services cs ON c.id_case = cs.id_case
              LEFT JOIN doctor_spec ds ON c.ID_DOCTOR = ds.doctor_id
			  left join departments dep on dep.id = ds.department_id
              LEFT JOIN users u ON ds.user_id = u.id
              LEFT JOIN patient_data pd ON c.ID_PATIENT = pd.id_patient
              LEFT JOIN vmu_profile vp ON vp.id_profile = cs.id_profile AND cs.is_oms = 1
              LEFT JOIN price p ON p.id = cs.id_profile AND cs.is_oms IN (0, 3, 4)
              LEFT JOIN price_orto_soc pr ON pr.id = cs.id_profile AND cs.is_oms = 2
              LEFT JOIN vmu_diagnosis vd ON cs.id_diagnosis = vd.id_diagnosis
              LEFT JOIN (SELECT vt.uet, vt.id_profile, vt.id_zone_type,
                                vt.tariff_begin_date AS d1,
                                vt.tariff_end_date AS d2,
                                vtp.tp_begin_date AS d3,
                                vtp.tp_end_date AS d4
                         FROM vmu_tariff vt,
                              vmu_tariff_plan vtp
                         WHERE vtp.id_tariff_group = vt.id_lpu
                           AND vtp.id_lpu IN (SELECT id_lpu FROM mu_ident)) T
                        ON T.id_profile = vp.id_profile AND
                           cs.date_begin BETWEEN T.d1 AND T.d2 AND
                           cs.date_begin BETWEEN T.d3 AND T.d4 AND
                           T.id_zone_type = cs.id_net_profile AND
                           cs.is_oms IN (1)
     WHERE cs.date_begin BETWEEN @date_start AND @date_end
       AND (c.ID_DEPT = @depart_id OR @depart_id = 0));
DROP TEMPORARY TABLE IF EXISTS first_row;
CREATE TEMPORARY TABLE IF NOT EXISTS first_row
    (SELECT T.fioD, T.dokt AS dokt, T.dptNm
     FROM (SELECT ds.dokt, ds.fioD, ds.date_begin, ds.dptNm
           FROM doctor_services ds
           GROUP BY ds.dokt
           ORDER BY ds.fioD) T);
DROP TEMPORARY TABLE IF EXISTS _1oms;
CREATE TEMPORARY TABLE IF NOT EXISTS _1oms
    (SELECT dokt, fioD, COUNT(DISTINCT id_case) AS poses, ROUND(COALESCE(SUM(tuet), 0), 2) AS tue
     FROM doctor_services ds
     WHERE ds.is_closed = 1
       AND ds.pic = 1
     GROUP BY dokt
     ORDER BY ds.fioD);  
DROP TEMPORARY TABLE IF EXISTS _2pu;
CREATE TEMPORARY TABLE IF NOT EXISTS _2pu (SELECT ds.dokt, ds.fioD, COUNT(DISTINCT id_case) AS poses,
                                                  ROUND(COALESCE(SUM(ds.puet), 0), 2) AS tue
                                           FROM doctor_services ds
                                           WHERE ds.IS_CLOSED = 1
                                             AND ds.pcode = 1
                                           GROUP BY ds.dokt
                                           ORDER BY ds.fioD);
DROP TEMPORARY TABLE IF EXISTS _3bzp;
CREATE TEMPORARY TABLE IF NOT EXISTS _3bzp
    (SELECT ds.dokt, ds.fioD, COUNT(DISTINCT id_case) AS poses, ROUND(COALESCE(SUM(ds.pruet), 0), 2) AS tue
     FROM doctor_services ds
     WHERE ds.IS_CLOSED = 1
       AND ds.prcode <> 0
       AND ds.prcode IN ('2.5', '2.6', '2.7')
     GROUP BY ds.dokt
     ORDER BY ds.fioD);
DROP TEMPORARY TABLE IF EXISTS _4rez;
CREATE TEMPORARY TABLE IF NOT EXISTS _4rez (SELECT fr.dokt, fr.fioD, fr.dptNm, SUM(COALESCE(o.poses, 0)) pOMS,
                                                   SUM(COALESCE(o.tue, 0)) uetOMS, SUM(COALESCE(p.poses, 0)) pPU,
                                                   SUM(COALESCE(p.tue, 0)) uetPU, SUM(COALESCE(b.poses, 0)) pBZP,
                                                   SUM(COALESCE(b.tue, 0)) uetBZP
                                            FROM first_row fr
                                                     LEFT JOIN _1oms o ON fr.dokt = o.dokt
                                                     LEFT JOIN _2pu p ON fr.dokt = p.dokt
                                                     LEFT JOIN _3bzp b ON fr.dokt = b.dokt
                                            GROUP BY fr.dokt
                                            HAVING (pOMS + pPU + pBZP) > 0
                                            ORDER BY fr.fioD);
DROP TEMPORARY TABLE IF EXISTS _5total;
CREATE TEMPORARY TABLE IF NOT EXISTS _5total
    (SELECT fr.dokt, fr.fioD, (COALESCE(o.poses, 0) + COALESCE(p.poses, 0) + COALESCE(b.poses, 0)) AS poses,
            (COALESCE(o.tue, 0) + COALESCE(p.tue, 0) + COALESCE(b.tue, 0)) AS uets
     FROM first_row fr
              LEFT JOIN _1oms o ON fr.dokt = o.dokt
              LEFT JOIN _2pu p ON fr.dokt = p.dokt
              LEFT JOIN _3bzp b ON fr.dokt = b.dokt
     ORDER BY fr.fioD);


SELECT  r.dptNm, r.fioD, r.pOMS, r.uetOMS, r.pPU, r.uetPU, r.pBZP, r.uetBZP, t.poses, t.uets
FROM _4rez r
         JOIN _5total t ON r.dokt = t.dokt;
