set @row = 0;
SELECT @row := @row + 1, T_MAIN.time, T_MAIN.pat_name, T_MAIN.birthday, T_MAIN.unstruct_address, T_MAIN.source_lpu,
       T_MAIN.prim, T_MAIN.diagnosis_code, T_MAIN.coag_flag, 
case when count(T_MAIN.codes_oms) > 0 then '1' else '0' end as codes_oms, 
case when count(T_MAIN.codes_pu) > 0 then '1' else '0' end as codes_pu,
       COALESCE(T_MAIN.roentgen, 0), COALESCE(T_MAIN.phys, 0), COALESCE(T_MAIN.cons, 0), COALESCE(T_MAIN.stac, 0)
FROM (SELECT a.time, CONCAT(pd.surname, ' ', pd.name, ' ', pd.second_name) as pat_name, pd.birthday,
             pa.unstruct_address, ep.source_lpu as source_lpu, count(cs.id_case) as prim, vd.diagnosis_code,
             pd.coag_flag, COALESCE(T1.codes, '') as codes_oms, COALESCE(T2.codes, '') as codes_pu,
             (COALESCE(prot_roentgen.roentgen, 0) + COALESCE(COUNT(d_roentgen.id), 0) > 0) as roentgen, 0 as phys,
             prot_cons.cons as cons, prot_stac.stac as stac
      FROM (SELECT id_case FROM protocols WHERE date_protocol = @date_start AND id_doctor = @doc_id) CS_MAIN
               LEFT JOIN cases c ON c.id_case = CS_MAIN.id_case
               LEFT JOIN vmu_diagnosis vd ON vd.id_diagnosis = c.id_diagnosis
               LEFT JOIN patient_data pd ON pd.id_patient = c.id_patient
               LEFT JOIN patient_address pa ON pa.id_address = pd.id_address_reg
               LEFT JOIN (SELECT ep1.id_human,
                                 group_concat(concat('(', l.lpu_short_name, ' - ', ep1.date, ')')) as source_lpu
                          FROM epikriz ep1,
                               lpu l
                          where l.id_lpu = ep1.lpu_id
                          group by id_human) ep ON ep.id_human = pd.id_human
               LEFT JOIN appointments a ON a.case_id = c.id_case
               LEFT JOIN case_services cs ON cs.id_case = c.id_case AND cs.is_oms in (0, 3) AND
                                             cs.id_profile IN (select id from price p where p.code = '7001')
               LEFT JOIN case_services cs_sec ON cs_sec.id_case = c.id_case AND cs.is_oms in (0, 3) AND
                                                 cs_sec.id_profile IN
                                                 (select id from price p where p.code = '7016' or p.code = '7018')
               LEFT JOIN (SELECT T11.id_case, T11.id_doctor,
                                 GROUP_CONCAT(CONCAT(T11.profile_infis_code, ' (', T11.cnt, ')') SEPARATOR
                                              ', ') as codes, coalesce(SUM(T11.UET), 0) as uet
                          FROM (SELECT tcs.id_case, tcs.id_doctor, tcs.id_profile, count(tcs.id_profile) as cnt,
                                       tvp.profile_infis_code, SUM(COALESCE(tvp.UET, 0)) as uet
                                FROM case_services tcs
                                         LEFT JOIN (SELECT ttvp.profile_infis_code, tvt.uet, ttvp.id_profile,
                                                           tvt.id_zone_type
                                                    FROM vmu_profile ttvp,
                                                         vmu_tariff tvt,
                                                         vmu_tariff_plan vtp
                                                    WHERE ttvp.id_profile = tvt.id_profile
                                                      and vtp.id_tariff_group = tvt.id_lpu
                                                      AND tvt.tariff_begin_date <= @date_start
                                                      AND tvt.tariff_end_date >= @date_start
                                                      and vtp.tp_begin_date <= @date_start
                                                      and vtp.tp_end_date >= @date_start
                                                      And vtp.id_lpu in (select id_lpu from mu_ident)) tvp
                                                   ON tvp.id_profile = tcs.id_profile AND
                                                      tvp.id_zone_type = tcs.id_net_profile
                                WHERE tcs.date_begin = @date_start
                                  AND tcs.id_doctor = @doc_id
                                  and tcs.is_oms = 1
                                GROUP BY tcs.id_case, tcs.id_profile) T11
                          GROUP BY T11.id_case) T1 ON T1.id_case = c.id_case
               LEFT JOIN (SELECT T12.id_case, T12.id_doctor,
                                 GROUP_CONCAT(CONCAT(T12.code, ' (', T12.cnt, ')') SEPARATOR ', ') as codes,
                                 coalesce(SUM(T12.UET), 0) as uet
                          FROM (SELECT tcs.id_case, tcs.id_doctor, tcs.id_profile, count(tcs.id_profile) as cnt,
                                       tp.code, SUM(COALESCE(tp.UET, 0)) as uet
                                FROM case_services tcs
                                         LEFT JOIN price tp ON tp.id = tcs.id_profile
                                WHERE tcs.date_begin = @date_start
                                  AND tcs.id_doctor = @doc_id
                                  and tcs.is_oms in (0, 3)
                                GROUP BY tcs.id_case, tcs.id_profile) T12
                          GROUP BY T12.id_case) T2 ON T2.id_case = c.id_case
               LEFT JOIN (SELECT id_case, count(id_ref) as roentgen, id_diag, id_doctor
                          FROM protocols p
                          where date_protocol = @date_start
                            and id_doctor = @doc_id
                            and (id_ref = 35290)
                          group by id_case) prot_roentgen ON prot_roentgen.id_case = c.id_case
               LEFT JOIN directions d_roentgen ON d_roentgen.id_case = c.id_case and d_roentgen.id_direction = 2
               LEFT JOIN (SELECT id_case, count(id_ref) as cons, id_diag, id_doctor
                          FROM protocols p
                          where date_protocol = @date_start
                            and id_doctor = @doc_id
                            and (id_ref = 35280 or id_ref = 37230 or id_ref = 37520)
                          group by id_case) prot_cons ON prot_cons.id_case = c.id_case
               LEFT JOIN (SELECT id_case, count(id_ref) as stac, id_diag, id_doctor
                          FROM protocols p
                          where date_protocol = @date_start
                            and id_doctor = @doc_id
                            and (id_ref = 35270 or id_ref = 37220 or id_ref = 37510)
                          group by id_case) prot_stac ON prot_stac.id_case = c.id_case
      GROUP BY CS_MAIN.id_case
      order by a.time) T_MAIN;
