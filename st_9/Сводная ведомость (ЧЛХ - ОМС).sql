create temporary table if not exists doctor_services (select cs.date_begin, cs.id_case, vp.profile_infis_code, p.code,
                                                             vd.diagnosis_code, cs.tooth_name, cs.is_oms, pd.coag_flag
                                                      from case_services cs
                                                               left join vmu_profile vp on vp.id_profile = cs.id_profile and cs.is_oms in (1, 5)
                                                               left join price p on p.id = cs.id_profile and cs.is_oms in (0, 3)
                                                               left join vmu_diagnosis vd on cs.id_diagnosis = vd.id_diagnosis
                                                               left join cases c on c.id_case = cs.ID_CASE
                                                               left join patient_data pd on c.ID_PATIENT = pd.ID_PATIENT
                                                      where cs.id_doctor = @doc_id
                                                        and cs.date_begin between @date_start and @date_end);
create temporary table if not exists doctor_protocols (select p.id_case, p.date_protocol as date_begin, p.id_human
                                                       from protocols p
                                                       where p.id_doctor = @doc_id
                                                         and p.date_protocol between @date_start and @date_end
                                                       group by p.id_case, p.date_protocol);
create temporary table if not exists was_before (select distinct pd.id_human as humans
                                                       from cases c
                                                                left join patient_data pd on pd.id_patient = c.id_patient
                                                       where c.date_begin < @date_start 
                                                       );
create temporary table if not exists mes_cases_opened (select count(distinct pd.id_human) as humans, cs.date_begin
                                                       from case_services cs
                                                                left join cases c on c.id_case = cs.id_case
                                                                left join patient_data pd on pd.id_patient = c.id_patient
                                                                left join vmu_profile vp on vp.id_profile = cs.id_profile
                                                       where cs.id_doctor = @doc_id
                                                         and cs.date_begin between @date_start and @date_end
                                                         and cs.is_oms = 5
                                                         and vp.profile_infis_code in ('оВ007о','811017')
                                                       group by cs.date_begin);
create temporary table if not exists pervich (select date_begin,count(id_human) as humans
                                                      from doctor_protocols
													  where id_human not in (select humans from was_before)
                                                      group by date_begin
                                                      order by date_begin);
create temporary table if not exists mes_cases_closed (select count(T.id_human) as humans, T.date_end,
                                                              sum(datediff(T.date_end, T.date_begin) + 1) as hosp_days
                                                       from (select pd.id_human, c.date_begin, c.date_end
                                                             from case_services cs
                                                                      left join cases c on c.id_case = cs.id_case
                                                                      left join patient_data pd on pd.id_patient = c.id_patient
                                                                      left join vmu_profile vp on vp.id_profile = cs.id_profile
                                                             where cs.id_doctor = @doc_id
                                                               and c.date_end is not null
                                                               and c.date_end between @date_start and @date_end
                                                               and cs.is_oms = 5
                                                               and vp.profile_infis_code in ('оВ007о','811017')
                                                             group by pd.id_human, c.date_end) T
                                                       group by T.date_end);      set @row = 0;
create temporary table if not exists first_row (select @row := @row + 1 as st_num, T.date_begin as st_date,
                                                       T.cnt as st_count
                                                from (select date_begin, count(id_case) as cnt
                                                      from doctor_protocols
                                                      group by date_begin
                                                      order by date_begin) T);
create temporary table if not exists T3 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where profile_infis_code in ('оЖ001г', 'оЖ001д')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T4 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where profile_infis_code in ('оЖ001г')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T5 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where profile_infis_code in ('оЖ001д')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T6 (select date_begin, count(id_case) as val
                                         from doctor_services	
                                         where profile_infis_code in ('оЖ012ж', 'оЖ016в')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T7 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where profile_infis_code in ('оВ007о')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T9 (select date_begin, case COAG_FLAG when 1 then 1 when 0 then 0 end as cf
                                         from doctor_services
                                         group by date_begin
                                         order by date_begin);

 select fr.st_num, fr.st_date, fr.st_count, coalesce(p.humans, 0), T9.cf, coalesce(T1.humans, 0),
        coalesce(T2.humans, 0), coalesce(T2.hosp_days, 0), coalesce(T3.val, 0), coalesce(T4.val, 0),
        coalesce(T5.val, 0), coalesce(T6.val, 0), coalesce(T7.val, 0)
 from first_row fr
		  left join pervich p on p.date_begin = fr.st_date
          left join mes_cases_opened T1 on T1.date_begin = fr.st_date
          left join mes_cases_closed T2 on T2.date_end = fr.st_date
          left join T3 on T3.date_begin = fr.st_date
          left join T4 on T4.date_begin = fr.st_date
          left join T5 on T5.date_begin = fr.st_date
          left join T6 on T6.date_begin = fr.st_date
          left join T7 on T7.date_begin = fr.st_date
          left join T9 on T9.date_begin = fr.st_date
 order by st_num ;
