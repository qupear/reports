

create temporary table if not exists const (
select 0.5 as prim, 0.25 as sec, 0.2 as osm
);


create temporary table if not exists bzp_services (
select pos.code, count(cs.id_service) as css, count(cs.id_service) as ueti, cs.id_doctor
	from case_services cs 
	left join price_orto_soc pos on pos.id = cs.id_profile
	left join doctor_spec ds on ds.doctor_id = cs.id_doctor
	where cs.is_oms = 2 
		and pos.code in (1.4, 1.2) 
		and cs.date_begin between @date_start and @date_end
group by code
);

create temporary table if not exists primary_1 (
select cs.id_case as id
	from case_services cs
	left join price p on p.id = cs.id_profile
	left join orders o on o.id = cs.id_order
	where p.code  in (8001,8003)
		and o.orderCreateDate between @date_start and @date_end
	group by id
);


create temporary table if not exists pu_services (
select o.id ,p.code, count(cs.id_service) as css, p.uet as uet, count(cs.id_service) * p.uet as ueti, cs.id_order
	from case_services cs
	left join price p on p.id = cs.ID_PROFILE
	left join orders o on o.id = cs.ID_ORDER
	where o.orderCreateDate between @date_start and @date_end
		and cs.id_case in (select id from primary_1)
		and p.code in (8001, 8002, 8003)
	group by p.code
);

set @s1_0 = 'Рубежов А.Л.';
set @s1_1 = 'Прием первичный';
set @s1_2 = (select prim from const);
set @s1_3 = (select css from bzp_services where code = 1.4 and id_doctor in (267, 165, 347));
set @s1_4 = (select css * 0.5 from bzp_services where code = 1.4 and id_doctor in (267, 165, 347));
set @s1_5 = (select prim from const);
set @s1_6 = (select sum(css) from pu_services where code in (8001, 8003));
set @s1_7 = (select (sum(css) * 0.5) * 0.1 from pu_services where code in (8001, 8003));

set @s2_0 = '	';
set @s2_1 = 'Прием повторный';
set @s2_2 = (select sec from const);
set @s2_3 = (select coalesce(css, 0) from bzp_services where code = 1.2 and id_doctor in (267, 165, 347));
set @s2_4 = (select coalesce(css * 0.25, 0) from bzp_services where code = 1.2 and id_doctor in (267, 165, 347));
set @s2_5 = (select sec from const);
set @s2_6 = (select css from pu_services where code in (8002));
set @s2_7 = (select (css * 0.25) * 0.1 from pu_services where code in (8002));

set @s3_0 = '		';
set @s3_1 = 'Осмотры';
set @s3_2 = (select osm from const);
set @s3_3 = (coalesce(@s1_3+@s2_3, 0));
set @s3_4 = (coalesce(@s3_3*0.2,0));
set @s3_5 = (select osm from const);
set @s3_6 = (select sum(css) from pu_services);
set @s3_7 = (select (sum(css) * 0.2) * 0.1 from pu_services);

set @s4_0 = '		';
set @s4_1 = '		';
set @s4_2 = '		';
set @s4_3 = '		';
set @s4_4 = (coalesce(@s1_4, 0) + coalesce(@s2_4, 0) + coalesce(@s3_4, 0)) ;
set @s4_5 = '			';
set @s4_6 = '			';
set @s4_7 = (coalesce(@s1_7, 0) + coalesce(@s2_7, 0) + coalesce(@s3_7, 0));

set @s5_0 = '	';
set @s5_1 = '	';
set @s5_2 = '	';
set @s5_3 = '	';
set @s5_4 = '	';
set @s5_5 = '	';
set @s5_6 = '	';
set @s5_7 = '	';


set @s6_0 = 'Богданова Е.Ю.';
set @s6_1 = 'Прием первичный';
set @s6_2 = (select prim from const);
set @s6_3 = (select css from bzp_services where code = 1.4 and id_doctor in (238, 153));
set @s6_4 = (select css * 0.5 from bzp_services where code = 1.4 and id_doctor in (238, 153));
set @s6_5 = (select prim from const);
set @s6_6 = (select sum(css) from pu_services where code in (8001, 8003));
set @s6_7 = (select sum(css) * 0.5 from pu_services where code in (8001, 8003));

set @s7_0 = '			';
set @s7_1 = 'Прием повторный';
set @s7_2 = (select sec from const);
set @s7_3 = (select css from bzp_services where code = 1.2 and id_doctor in (238, 153));
set @s7_4 = (select css * 0.25 from bzp_services where code = 1.2 and id_doctor in (238, 153));
set @s7_5 = (select sec from const);
set @s7_6 = (select css from pu_services where code in (8002));
set @s7_7 = (select css * 0.25 from pu_services where code in (8002));

set @s8_0 = '			';
set @s8_1 = 'Осмотры';
set @s8_2 = (select osm from const);
set @s8_3 = (@s6_3 + @s7_3);
set @s8_4 = (@s8_3 * 0.2 );
set @s8_5 = (select osm from const);
set @s8_6 = (select sum(css) from pu_services);
set @s8_7 = (select sum(css) * 0.2 from pu_services);

set @s9_0 = '			';
set @s9_1 = '			';
set @s9_2 = '			';
set @s9_3 = '			';
set @s9_4 = (coalesce(@s6_4, 0) + coalesce(@s7_4, 0) + coalesce(@s8_4, 0)) ;
set @s9_5 = '			';
set @s9_6 = '			';
set @s9_7 = (coalesce(@s6_7, 0) + coalesce(@s7_7, 0) + coalesce(@s8_7, 0));

set @s10_0 = '		';
set @s10_1 = '		';
set @s10_2 = '		';
set @s10_3 = '		';
set @s10_4 = '		';
set @s10_5 = '		';
set @s10_6 = '		';
set @s10_7 = '		';

set @s11_0 = '		';
set @s11_1 = '		';
set @s11_2 = '		';
set @s11_3 = 'Итого';
set @s11_4 = (coalesce(@s9_4, 0) + coalesce(@s4_4, 0)) ;
set @s11_5 = '		';
set @s11_6 = 'Итого';
set @s11_7 = (coalesce(@s9_7, 0) + coalesce(@s4_7, 0));

set @s12_0 = '		';
set @s12_1 = '		';
set @s12_2 = '		';
set @s12_3 = '		';
set @s12_4 = '		';
set @s12_5 = '		';
set @s12_6 = '		';
set @s12_7 = '		';

set @s13_0 = '		';
set @s13_1 = '		';
set @s13_2 = '		';
set @s13_3 = '	Всего';
set @s13_4 = 'по БЗП и ПУ';
set @s13_5 = '		';
set @s13_6 = '		';
set @s13_7 = (coalesce(@s11_7, 0) + coalesce(@s11_4, 0));

select @s1_0, @s1_1, @s1_2, @s1_3, @s1_4, @s1_5, @s1_6, @s1_7
union
select @s2_0, @s2_1, @s2_2, @s2_3, @s2_4, @s2_5, @s2_6, @s2_7
union
select @s3_0, @s3_1, @s3_2, @s3_3, @s3_4, @s3_5, @s3_6, @s3_7
union
select @s4_0, @s4_1, @s4_2, @s4_3, @s4_4, @s4_5, @s4_6, @s4_7
union
select @s5_0, @s5_1, @s5_2, @s5_3, @s5_4, @s5_5, @s5_6, @s5_7
union
select @s6_0, @s6_1, @s6_2, @s6_3, @s6_4, @s6_5, @s6_6, @s6_7
union
select @s7_0, @s7_1, @s7_2, @s7_3, @s7_4, @s7_5, @s7_6, @s7_7
union
select @s8_0, @s8_1, @s8_2, @s8_3, @s8_4, @s8_5, @s8_6, @s8_7
union
select @s9_0, @s9_1, @s9_2, @s9_3, @s9_4, @s9_5, @s9_6, @s9_7
union
select @s10_0, @s10_1, @s10_2, @s10_3, @s10_4, @s10_5, @s10_6, @s10_7
union
select @s11_0, @s11_1, @s11_2, @s11_3, @s11_4, @s11_5, @s11_6, @s11_7
union
select @s12_0, @s12_1, @s12_2, @s12_3, @s12_4, @s12_5, @s12_6, @s12_7
union
select @s13_0, @s13_1, @s13_2, @s13_3, @s13_4, @s13_5, @s13_6, @s13_7