select d.name as dept_name, t.name as tech_name,
       concat('Наряд ', ord.Order_Number, ' (пациент №', pd.card_number, ' ', pd.surname, ' ', pd.name, ' ',
              pd.second_name, '), врач ', u.name) as order_name, p.code as serv_code, p.name as serv_name,
       count(cs.id_profile), sum(p.cost)
from orders ord
         left join case_services cs on cs.id_order = ord.id
         left join doctor_spec ds on ds.doctor_id = ord.id_doctor
         left join departments d on d.id = ds.department_id
         left join users u on u.id = ds.user_id
         left join patient_data pd on pd.id_human = ord.id_human and pd.is_active = 1 and pd.date_end = '2200-01-01'
         left join technicians t on t.id = ord.id_technician
         left join price p on p.id = cs.id_profile
where ord.orderCompletionDate between @date_start and @date_end
  and (ord.order_type = 0 or ord.order_type = 2)
  and ord.spec = 4
  and (d.id = @depart_id or @depart_id = 0)
  and (ds.doctor_id = @doc_id or @doc_id = 0)
  and (ord.id_technician = @techn_id or @techn_id = 0)
group by ord.id, cs.id_profile
order by dept_name, tech_name, ord.id;