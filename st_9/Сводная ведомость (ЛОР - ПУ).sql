create temporary table if not exists doctor_services (select cs.date_begin, cs.id_case, p.code, vd.diagnosis_code,
                                                             cs.tooth_name, p.uet, cs.is_oms, pd.COAG_FLAG
                                                      from case_services cs
                                                               left join cases c on cs.id_case = c.id_case
                                                               left join patient_data pd on c.ID_PATIENT = pd.id_patient
                                                               left join price p on p.id = cs.id_profile and cs.is_oms in (0, 3, 4)
                                                               left join vmu_diagnosis vd on cs.id_diagnosis = vd.id_diagnosis
                                                      where cs.id_doctor = @doc_id
                                                        and cs.date_begin between @date_start and @date_end
                                                        and cs.is_oms in (0, 3, 4));
set @row = 0;
create temporary table if not exists first_row (select @row := @row + 1 as st_num, T.date_begin as st_date
                                                from (select date_begin
                                                      from doctor_services
                                                      group by date_begin
                                                      order by date_begin) T);
create temporary table if not exists T1 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T2 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where code in (7015, 7017)
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T3 (select date_begin, COAG_FLAG as cf
                                         from doctor_services
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T4 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where code in (7019)
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T5 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where code in (7030, 7045)
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T6 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where code in (7031, 7046)
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T7 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where code in (7034, 7049)
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T8 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where code in (7040, 7055)
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T9 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where code in (7037, 7052)
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T10 (select date_begin, count(distinct id_case) as val
                                          from doctor_services
                                          where code in (7032, 7047)
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T11 (select date_begin, count(distinct id_case) as val
                                          from doctor_services
                                          where code in (7033, 7035, 7048, 7050)
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T12 (select date_begin, count(distinct id_case) as val
                                          from doctor_services
                                          where code in (7038, 7053)
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T13 (select date_begin, count(distinct id_case) as val
                                          from doctor_services
                                          where code in (7039, 7054)
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T14 (select date_begin, count(distinct id_case) as val
                                          from doctor_services
                                          where code in (7041, 7042)
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T15 (select date_begin, count(distinct id_case) as val
                                          from doctor_services
                                          where code in (7043, 7044)
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T16 (select date_begin, count(distinct id_case) as val
                                          from doctor_services
                                          where code in (7024)
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T17 (select date_begin, ROUND(COALESCE(SUM(UET), 0), 2) as val
                                          from doctor_services
                                          group by date_begin
                                          order by date_begin);
 select fr.st_num, fr.st_date, coalesce(T1.val, 0), coalesce(T2.val, 0), T3.cf, 0, coalesce(T4.val, 0),
         coalesce(T5.val, 0), coalesce(T6.val, 0), coalesce(T7.val, 0), coalesce(T8.val, 0), coalesce(T9.val, 0),
         coalesce(T10.val, 0), coalesce(T11.val, 0), coalesce(T13.val, 0), coalesce(T14.val, 0), coalesce(T15.val, 0),
         coalesce(T16.val, 0), coalesce(T17.val, 0)
  from first_row fr
           left join T1 on T1.date_begin = fr.st_date
           left join T2 on T2.date_begin = fr.st_date
           left join T3 on T3.date_begin = fr.st_date
           left join T4 on T4.date_begin = fr.st_date
           left join T5 on T5.date_begin = fr.st_date
           left join T6 on T6.date_begin = fr.st_date
           left join T7 on T7.date_begin = fr.st_date
           left join T8 on T8.date_begin = fr.st_date
           left join T9 on T9.date_begin = fr.st_date
           left join T10 on T10.date_begin = fr.st_date
           left join T11 on T11.date_begin = fr.st_date
           left join T13 on T13.date_begin = fr.st_date
           left join T14 on T14.date_begin = fr.st_date
           left join T15 on T15.date_begin = fr.st_date
           left join T16 on T16.date_begin = fr.st_date
           left join T17 on T17.date_begin = fr.st_date
  order by st_num ;
