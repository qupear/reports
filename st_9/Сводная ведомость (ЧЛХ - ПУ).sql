create temporary table if not exists doctor_services (SELECT cs.date_begin, cs.id_case, p.code, vd.diagnosis_code,
                                                             cs.tooth_name, p.uet, cs.is_oms, pd.coag_flag
                                                      FROM case_services cs
                                                               LEFT JOIN price p ON p.id = cs.id_profile AND cs.is_oms IN (0)
                                                               LEFT JOIN vmu_diagnosis vd ON cs.id_diagnosis = vd.id_diagnosis
                                                               left join cases c on c.id_case = cs.ID_CASE
                                                               left join patient_data pd on c.ID_PATIENT = pd.ID_PATIENT
                                                      WHERE cs.id_doctor = @doc_id
                                                        AND cs.date_begin BETWEEN @date_start AND @date_end
                                                        AND cs.is_oms IN (0));
set @row = 0;
create temporary table if not exists first_row (select @row := @row + 1 as st_num, T.date_begin as st_date
                                                from (select date_begin
                                                      from doctor_services
                                                      group by date_begin
                                                      order by date_begin) T);
create temporary table if not exists T1 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T2 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where code in ('7001')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T3 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where code in ('7005', '7006')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T4 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where code in ('7007')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T5 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where code in ('7008')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T6 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where code in ('7009')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T7 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where code in ('7011')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T8 (select date_begin, ROUND(COALESCE(SUM(UET), 0), 2) as val
                                         from doctor_services
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T9 (select date_begin, case COAG_FLAG when 1 then 1 when 0 then 0 end as cf
                                         from doctor_services
                                         group by date_begin
                                         order by date_begin);

select fr.st_num, fr.st_date, coalesce(T1.val, 0), coalesce(T2.val, 0), T9.cf, coalesce(T3.val, 0), coalesce(T4.val, 0),
       coalesce(T5.val, 0), coalesce(T6.val, 0), coalesce(T7.val, 0), coalesce(T8.val, 0)
from first_row fr
         left join T1 on T1.date_begin = fr.st_date
         left join T2 on T2.date_begin = fr.st_date
         left join T3 on T3.date_begin = fr.st_date
         left join T4 on T4.date_begin = fr.st_date
         left join T5 on T5.date_begin = fr.st_date
         left join T6 on T6.date_begin = fr.st_date
         left join T7 on T7.date_begin = fr.st_date
         left join T8 on T8.date_begin = fr.st_date
         left join T9 on T9.date_begin = fr.st_date
order by st_num ;
