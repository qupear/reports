SELECT   e.eln_number nomer,         CONCAT(e.pat_surname,' ',e.pat_name,' ',e.pat_second_name) fio   ,
       timestampdiff(year,  e.pat_birthday, current_date),         e.eln_date sozdan   ,         e.doctor_name doc   ,
       case when dis.id > 0 then 'Отменен' else es.name end as status   ,
       e.fss_sync_date redakt FROM eln e JOIN eln_states es ON e.eln_state=es.id
           left join eln_disable_reasons dis on dis.id = e.disable_reason WHERE e.eln_date BETWEEN @date_start AND @date_end;
