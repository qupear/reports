set @date_start = '2020-02-01';
set @date_end = '2020-02-29';
set @doc_id = 0;
set @depart_id = 0;


drop temporary table if exists cases_1, signatured;

create temporary table if not exists cases_1
select dep.name name_dep,u.name name_doc, p.id_case, p.id_human
	from protocols p
	left JOIN doctor_spec ds ON p.id_doctor = ds.doctor_id
	left JOIN departments dep ON ds.department_id = dep.id
	left join users u on u.id = ds.user_id
	where p.date_protocol BETWEEN @date_start AND @date_end
		AND (p.id_doctor = @doc_id OR @doc_id = 0)
		AND (ds.department_id = @depart_id OR @depart_id = 0)
group by name_doc, p.id_case;

create temporary table if not exists signatured
select p.id_case
from protocols p
inner join signatures s on s.id_case = p.id_case
left join doctor_spec ds on ds.doctor_id = p.id_doctor
where (p.id_doctor = @doc_id OR @doc_id = 0)
AND (ds.department_id = @depart_id OR @depart_id = 0)
group by p.id_doctor,p.id_case;


SELECT c1.name_dep,
	c1.name_doc,
	date(app.time), concat_ws(' ', pd.surname, pd.name, pd.second_name) as name
	FROM cases_1 c1
		LEFT JOIN patient_data pd on pd.id_human = c1.id_human
		left join appointments app on app.case_id = c1.id_case
	WHERE name_dep is not null
and c1.id_case not in (select id_case from signatured)
GROUP BY c1.name_dep, c1.name_doc, name
ORDER BY c1.name_dep, c1.name_doc;