create temporary table if not exists material_id   select id from wh_materials where code = @code or @code = 'all';   create temporary table if not exists material_codes  (index (id_material,code))  select m_c.code, m_c.quantity, m_c.pay_type, material_id.id as id_material        from materials_consumption m_c, material_id    where m_c.id_material = material_id.id;
select d.name as dept_name, u.name as doc_name,
       concat(wm.name, ', количество в упаковке: ', wm.amount_in_packing) as mat_name, sum(mc.quantity), wm.in_measure
from case_services cs
         left join doctor_spec ds on ds.doctor_id = cs.id_doctor
         left join users u on u.id = ds.user_id
         left join departments d on d.id = ds.department_id
         left join orders ord on ord.id = cs.id_order
         left join price p on p.id = cs.id_profile and cs.is_oms in (0, 3)
         left join price_orto_soc pos
                   on pos.id = cs.id_profile and (cs.is_oms = 2 or (ord.id is not null and ord.order_type = 1))
         left join vmu_profile vp on vp.id_profile = cs.id_profile and cs.is_oms = 1
         left join material_codes mc on (mc.code = p.code and cs.is_oms in (0, 3)) or (mc.code = pos.code and
                                                                                       (cs.is_oms = 2 or (ord.id is not null and ord.order_type = 1))) or
                                        (mc.code = vp.profile_infis_code and cs.is_oms = 1)
         left join wh_materials wm on wm.id = mc.id_material
where cs.date_begin between @date_start and @date_end
  and (mc.code is not null)
  and (ds.department_id = @depart_id or @depart_id is null or @depart_id = 0)
  and (ds.doctor_id = @doc_id or @doc_id is null or @doc_id = 0)
group by cs.id_doctor, wm.id
order by d.name, u.name, wm.name;
