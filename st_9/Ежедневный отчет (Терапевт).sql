 create temporary table if not exists doctor_protocols select id_case, count(id_ref) as sanir, id_diag, id_doctor 
from protocols p 
where date_protocol = @date_start and id_doctor = @doc_id 
and id_ref in (55791, 5761, 4781, 3790, 2350, 1392, 480, 56732, 74783, 107000, 109100, 114630, 117290, 119950, 122600, 125690, 127930,
130160, 131010, 133580, 136070, 138630, 141170, 143650, 164780, 168280) 
group by id_case;  
create temporary table if not exists doctor_protocols_sprav select id_case, count(id_ref) as sanir, id_diag, id_doctor 
from protocols p 
where date_protocol = @date_start and id_doctor = @doc_id 
and id_ref in (481, 1396, 2351, 3791, 4782, 5762, 18180, 55796, 56733, 107001, 109102, 114632, 117292, 119952, 122602, 125691,
127931, 130161, 131011, 133582, 136072, 138632, 141172, 143652, 164781, 168282) 
group by id_case;  
create temporary table if not exists doc_cases select id_case 
from case_services
 where date_begin = @date_start and id_doctor = @doc_id group by id_case;  
create temporary table if not exists services_oms  select T.id_case, T.id_doctor,
 		group_concat(concat(coalesce(T.profile_infis_code,''), ' (', coalesce(T.cnt,0), ')') separator ', ') as codes,
 sum(T.uet) as uet from (select cs.id_case, cs.id_doctor, count(cs.id_profile) as cnt, tvp.profile_infis_code, sum(coalesce(tvp.UET,0)) as uet 		
from case_services cs 		
left join (select ttvp.profile_infis_code, tvt.uet, ttvp.id_profile, tvt.id_zone_type 					
from vmu_profile ttvp, vmu_tariff tvt, vmu_tariff_plan vtp 					
where ttvp.id_profile = tvt.id_profile and vtp.id_tariff_group = tvt.id_lpu 							
and tvt.tariff_begin_date <= @date_start and tvt.tariff_end_date >= @date_start 							
and vtp.tp_begin_date <= @date_start and vtp.tp_end_date >= @date_start  							
and vtp.id_lpu in (select id_lpu from mu_ident) ) tvp ON tvp.id_profile = cs.id_profile and tvp.id_zone_type = cs.id_net_profile 		
where cs.date_begin = @date_start and cs.id_doctor = @doc_id and cs.is_oms = 1 		group by cs.id_case , cs.id_profile) T 
group by T.id_case;  
create temporary table if not exists services_pu  
select T.id_case, T.id_doctor,             group_concat(concat(coalesce(T.code,''), ' (', coalesce(T.cnt,0), ')') separator ', ') as codes,
 sum(T.uet) as uet from (select cs.id_case, cs.id_doctor, cs.id_profile, count(cs.id_profile) as cnt, p.code, sum(coalesce(p.UET,0)) as uet     
from case_services cs     
left join price p ON p.id = cs.id_profile     
where cs.date_begin = @date_start and cs.id_doctor = @doc_id and cs.is_oms = 0     
group by cs.id_case , cs.id_profile) T 
group by T.id_case;  
create temporary table if not exists services_bzp  
select T.id_case, T.id_doctor,  		group_concat(concat(coalesce(T.code,''), ' (', coalesce(T.cnt,0), ')') separator ', ') as codes,
 sum(coalesce(T.UET,0)) as uet from (select cs.id_case, cs.id_doctor, count(cs.id_profile) as cnt, pos.code, sum(coalesce(pos.UET,0)) as uet     
from case_services cs     
left join price_orto_soc pos on pos.id = cs.id_profile      
where cs.date_begin = @date_start and cs.id_doctor = @doc_id and cs.is_oms = 2     
group by cs.id_case , cs.id_profile) T group by T.id_case;  
create temporary table if not exists prim_visits  select id_case 
from case_services cs 
where cs.date_begin = @date_start and cs.id_doctor = @doc_id          
and cs.id_profile IN (select id_profile 
from vmu_profile where profile_infis_code in ('стт001', 'стт005', 'стт013') group by id_case);   
create temporary table if not exists case_teeth  
select id_case, group_concat(distinct tooth_name separator ', ') as teeth 
from case_services
 where date_begin = @date_start and id_doctor = @doc_id and tooth_name <> '-' 
group by id_case;          
set @row = 0;
select 
    @row:=@row + 1,
    T_MAIN.time,
    T_MAIN.pat_name,
    T_MAIN.birthday,
    T_MAIN.unstruct_address,
    T_MAIN.prim,
    T_MAIN.child,
    T_MAIN.diagnosis_code,
    T_MAIN.coag_flag,
    T_MAIN.codes_oms,
    T_MAIN.codes_pu,
    T_MAIN.codes_bzp,
    T_MAIN.teeth,
    coalesce(T_MAIN.sanir, 0),
	coalesce(T_MAIN.sprav, 0),
    T_MAIN.uet_oms,
    T_MAIN.uet_pu,
    T_MAIN.uet_bzp
from
    (select 
        a.time,
            concat(pd.surname, ' ', pd.name, ' ', pd.second_name) as pat_name,
            pd.birthday,
            pa.unstruct_address,
            (count(pv.id_case) > 0) as prim,
            pd.coag_flag,
            (count(pv.id_case) * (CURDATE() < date_add(pd.birthday, interval 15 year)) > 0) as child,
            vd.diagnosis_code,
            coalesce(T1.codes, '') as codes_oms,
            coalesce(T2.codes, '') as codes_pu,
            coalesce(T3.codes, '') as codes_bzp,
            coalesce(cs_teeth.teeth, '') as teeth,
            prot_sanir.sanir as sanir,
			prot_sprav.sanir as sprav,
            round(coalesce(T1.uet, 0), 2) as uet_oms,
            round(coalesce(T2.uet, 0), 2) as uet_pu,
            round(coalesce(T3.uet, 0), 2) as uet_bzp
    from
        doc_cases CS_MAIN
    left join cases c ON c.id_case = CS_MAIN.id_case
    left join vmu_diagnosis vd ON vd.id_diagnosis = c.id_diagnosis
    left join patient_data pd ON pd.id_patient = c.id_patient
    left join patient_address pa ON pa.id_address = pd.id_address_reg
    left join appointments a ON a.case_id = c.id_case
    left join prim_visits pv ON pv.id_case = c.id_case
    left join case_teeth cs_teeth ON cs_teeth.id_case = c.id_case
    left join services_oms T1 ON T1.id_case = c.id_case
    left join services_pu T2 ON T2.id_case = c.id_case
    left join services_bzp T3 ON T3.id_case = c.id_case
    left join doctor_protocols prot_sanir ON prot_sanir.id_case = c.id_case
	left join doctor_protocols_sprav prot_sprav on prot_sprav.id_case = c.id_case
    group by CS_MAIN.id_case
    order by a.time) T_MAIN;  