DROP TEMPORARY TABLE IF EXISTS doctor_services;
CREATE TEMPORARY TABLE IF NOT EXISTS doctor_services (SELECT cs.date_begin, cs.id_case, p.code, vd.diagnosis_code,
                                                             cs.tooth_name, p.uet, cs.is_oms, pd.birthday
                                                      FROM case_services cs
                                                               LEFT JOIN price p ON p.id = cs.id_profile AND cs.is_oms IN (0, 3, 4)
                                                               LEFT JOIN vmu_diagnosis vd ON cs.id_diagnosis = vd.id_diagnosis
                                                               LEFT JOIN cases c ON c.id_case = cs.id_case
                                                               LEFT JOIN patient_data pd ON pd.id_patient = c.id_patient
                                                               LEFT JOIN doctor_spec ds ON cs.id_doctor = ds.doctor_id
                                                      WHERE (ds.department_id = @depart_id OR @depart_id = 0)
                                                        AND (ds.spec_id = 108)
                                                        AND cs.date_begin BETWEEN @date_start AND @date_end
                                                        AND cs.is_oms IN (0, 3, 4) and pd.coag_flag = 1);
SET  @row = 0;   DROP TEMPORARY TABLE IF EXISTS first_row;
CREATE TEMPORARY TABLE IF NOT EXISTS first_row (SELECT @row := @row + 1 AS st_num, date_begin AS st_date,
                                                       ROUND(COALESCE(SUM(UET), 0), 2) AS st_uet
                                                FROM doctor_services
                                                GROUP BY date_begin
                                                ORDER BY date_begin);  DROP TEMPORARY TABLE IF EXISTS T1;
CREATE TEMPORARY TABLE IF NOT EXISTS T1 (SELECT date_begin, COUNT(DISTINCT id_case) AS val
                                         FROM doctor_services
                                         GROUP BY date_begin
                                         ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T2;
CREATE TEMPORARY TABLE IF NOT EXISTS T2 (SELECT date_begin, COUNT(DISTINCT id_case) AS val
                                         FROM doctor_services
                                         WHERE DATE_ADD(birthday, INTERVAL 15 YEAR) > date_begin
                                         GROUP BY date_begin
                                         ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T3;
CREATE TEMPORARY TABLE IF NOT EXISTS T3 (SELECT date_begin, COUNT(DISTINCT id_case) AS val
                                         FROM doctor_services
                                         WHERE DATE_ADD(birthday, INTERVAL 15 YEAR) < date_begin
                                           AND DATE_ADD(birthday, INTERVAL 18 YEAR) > date_begin
                                         GROUP BY date_begin
                                         ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T4;
CREATE TEMPORARY TABLE IF NOT EXISTS T4(select T.date_begin, COUNT(T.val) AS val
                                        FROM (SELECT date_begin, COUNT(id_case) AS val
                                              FROM doctor_services
                                              WHERE code IN ('3001', '3003', '3005')
                                              GROUP BY id_case, tooth_name
                                              ORDER BY date_begin) T
                                        GROUP BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T5;
CREATE TEMPORARY TABLE IF NOT EXISTS T5(select T.date_begin, COUNT(T.val) AS val
                                        FROM (SELECT date_begin, COUNT(id_case) AS val
                                              FROM doctor_services
                                              WHERE code IN ('3001', '3003', '3005')
                                                AND DATE_ADD(birthday, INTERVAL 15 YEAR) > date_begin
                                              GROUP BY id_case, tooth_name
                                              ORDER BY date_begin) T
                                        GROUP BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T6;
CREATE TEMPORARY TABLE IF NOT EXISTS T6(select T.date_begin, COUNT(T.val) AS val
                                        FROM (SELECT date_begin, COUNT(id_case) AS val
                                              FROM doctor_services
                                              WHERE code IN ('3001', '3003', '3005')
                                                AND DATE_ADD(birthday, INTERVAL 15 YEAR) < date_begin
                                                AND DATE_ADD(birthday, INTERVAL 18 YEAR) > date_begin
                                              GROUP BY id_case, tooth_name
                                              ORDER BY date_begin) T
                                        GROUP BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T7;
CREATE TEMPORARY TABLE IF NOT EXISTS T7(select date_begin, COUNT(id_case) AS val
                                        FROM doctor_services
                                        WHERE code IN
                                              ('3009', '3010', '3011', '3015', '3016', '3017', '3019', '3020', '3012',
                                               '3013', '3014')
                                        GROUP BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T8;
CREATE TEMPORARY TABLE IF NOT EXISTS T8(select date_begin, COUNT(id_case) AS val
                                        FROM doctor_services
                                        WHERE code IN
                                              ('3009', '3010', '3011', '3015', '3016', '3017', '3019', '3020', '3012',
                                               '3013', '3014')
                                          AND DATE_ADD(birthday, INTERVAL 18 YEAR) <= date_begin
                                        GROUP BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T9;
CREATE TEMPORARY TABLE IF NOT EXISTS T9(select date_begin, COUNT(id_case) AS val
                                        FROM doctor_services
                                        WHERE code IN
                                              ('3009', '3010', '3011', '3015', '3016', '3017', '3019', '3020', '3012',
                                               '3013', '3014')
                                          AND DATE_ADD(birthday, INTERVAL 15 YEAR) > date_begin
                                        GROUP BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T10;
CREATE TEMPORARY TABLE IF NOT EXISTS T10(select date_begin, COUNT(id_case) AS val
                                         FROM doctor_services
                                         WHERE code IN
                                               ('3009', '3010', '3011', '3015', '3016', '3017', '3019', '3020', '3012',
                                                '3013', '3014')
                                           AND DATE_ADD(birthday, INTERVAL 15 YEAR) > date_begin
                                         GROUP BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T11;
CREATE TEMPORARY TABLE IF NOT EXISTS T11(select date_begin, COUNT(id_case) AS val
                                         FROM doctor_services
                                         WHERE code IN
                                               ('3009', '3010', '3011', '3015', '3016', '3017', '3019', '3020', '3012',
                                                '3013', '3014')
                                           AND DATE_ADD(birthday, INTERVAL 15 YEAR) < date_begin
                                           AND DATE_ADD(birthday, INTERVAL 18 YEAR) > date_begin
                                         GROUP BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T12;
CREATE TEMPORARY TABLE IF NOT EXISTS T12(select date_begin, COUNT(id_case) AS val
                                         FROM doctor_services
                                         WHERE code IN ('3009', '3010', '3011')
                                         GROUP BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T13;
CREATE TEMPORARY TABLE IF NOT EXISTS T13(select date_begin, COUNT(id_case) AS val
                                         FROM doctor_services
                                         WHERE code IN ('3009', '3010', '3011')
                                           AND DATE_ADD(birthday, INTERVAL 15 YEAR) > date_begin
                                         GROUP BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T14;
CREATE TEMPORARY TABLE IF NOT EXISTS T14 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3015', '3016', '3017', '3019', '3020')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T15;
CREATE TEMPORARY TABLE IF NOT EXISTS T15 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3015', '3016', '3017', '3019', '3020')
                                            AND DATE_ADD(birthday, INTERVAL 18 YEAR) <= date_begin
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T16;
CREATE TEMPORARY TABLE IF NOT EXISTS T16 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3015', '3016', '3017', '3019', '3020')
                                            AND DATE_ADD(birthday, INTERVAL 18 YEAR) > date_begin
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T17;
CREATE TEMPORARY TABLE IF NOT EXISTS T17 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3012', '3013', '3014')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T18;
CREATE TEMPORARY TABLE IF NOT EXISTS T18 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3012', '3013', '3014')
                                            AND DATE_ADD(birthday, INTERVAL 18 YEAR) <= date_begin
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T19;
CREATE TEMPORARY TABLE IF NOT EXISTS T19 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3012', '3013', '3014')
                                            AND DATE_ADD(birthday, INTERVAL 18 YEAR) > date_begin
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T20;
CREATE TEMPORARY TABLE IF NOT EXISTS T20 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3007', '3008', '30081')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T21;
CREATE TEMPORARY TABLE IF NOT EXISTS T21 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3007', '3008', '30081')
                                            AND DATE_ADD(birthday, INTERVAL 18 YEAR) <= date_begin
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T22;
CREATE TEMPORARY TABLE IF NOT EXISTS T22 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3007', '3008', '30081')
                                            AND DATE_ADD(birthday, INTERVAL 18 YEAR) > date_begin
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T23;
CREATE TEMPORARY TABLE IF NOT EXISTS T23 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3023', '3024', '3025')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T24;
CREATE TEMPORARY TABLE IF NOT EXISTS T24 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3031', '3032', '3033', '3079')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T25;
CREATE TEMPORARY TABLE IF NOT EXISTS T25 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3034', '3035', '3036', '3037', '3038', '3039')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T26;
CREATE TEMPORARY TABLE IF NOT EXISTS T26 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3040', '3041', '3042')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T27;
CREATE TEMPORARY TABLE IF NOT EXISTS T27 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3043', '3044', '3045')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T28;
CREATE TEMPORARY TABLE IF NOT EXISTS T28 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3046', '3047', '3048')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T29;
CREATE TEMPORARY TABLE IF NOT EXISTS T29 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3049', '3050', '3051')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T30;
CREATE TEMPORARY TABLE IF NOT EXISTS T30 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3053', '3054', '3055')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T31;
CREATE TEMPORARY TABLE IF NOT EXISTS T31 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3058')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T32;
CREATE TEMPORARY TABLE IF NOT EXISTS T32 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3056')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T33;
CREATE TEMPORARY TABLE IF NOT EXISTS T33 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3057')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T34;
CREATE TEMPORARY TABLE IF NOT EXISTS T34 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3064', '3065', '3066')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T35;
CREATE TEMPORARY TABLE IF NOT EXISTS T35 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3069', '3070', '3071')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T36;
CREATE TEMPORARY TABLE IF NOT EXISTS T36 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3072', '3080', '3081', '5013', '5014', '5015', '5016', '5032')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T37;
CREATE TEMPORARY TABLE IF NOT EXISTS T37 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3073', '3074', '3075')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T38;
CREATE TEMPORARY TABLE IF NOT EXISTS T38 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3082')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T39;
CREATE TEMPORARY TABLE IF NOT EXISTS T39 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('3090')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T40;
CREATE TEMPORARY TABLE IF NOT EXISTS T40 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('5001', '5002', '5003')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T41;
CREATE TEMPORARY TABLE IF NOT EXISTS T41 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('5017')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T42;
CREATE TEMPORARY TABLE IF NOT EXISTS T42 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN
                                                ('5018', '5019', '5020', '5021', '5022', '3083', '3084', '3085', '3086',
                                                 '3087', '3089')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T43;
CREATE TEMPORARY TABLE IF NOT EXISTS T43 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN
                                                ('5034', '5035', '3060', '3061', '3063', '3089', '30631', '30601',
                                                 '3093', '3094', '3095', '3096')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS T44;
CREATE TEMPORARY TABLE IF NOT EXISTS T44 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN
                                                ('5004', '5005', '5006', '5007', '5008', '5009', '5010', '5012', '5011',
                                                 '5029', '5030', '5031', '3088')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);   DROP TEMPORARY TABLE IF EXISTS tmp_1_21;
CREATE TEMPORARY TABLE tmp_1_21(select fr.st_num, fr.st_date, COALESCE(T1.val, 0) AS v1, COALESCE(T2.val, 0) AS v2,
                                       COALESCE(T3.val, 0) AS v3, COALESCE(T4.val, 0) AS v4, COALESCE(T5.val, 0) AS v5,
                                       COALESCE(T6.val, 0) AS v6, COALESCE(T7.val, 0) AS v7, COALESCE(T8.val, 0) AS v8,
                                       COALESCE(T9.val, 0) AS v9, COALESCE(T10.val, 0) AS v10,
                                       COALESCE(T11.val, 0) AS v11, COALESCE(T12.val, 0) AS v12,
                                       COALESCE(T13.val, 0) AS v13, COALESCE(T14.val, 0) AS v14,
                                       COALESCE(T15.val, 0) AS v15, COALESCE(T16.val, 0) AS v16,
                                       COALESCE(T17.val, 0) AS v17, COALESCE(T18.val, 0) AS v18,
                                       COALESCE(T19.val, 0) AS v19, COALESCE(T20.val, 0) AS v20,
                                       COALESCE(T21.val, 0) AS v21, fr.st_uet
                                FROM first_row fr
                                         LEFT JOIN T1 ON T1.date_begin = fr.st_date
                                         LEFT JOIN T2 ON T2.date_begin = fr.st_date
                                         LEFT JOIN T3 ON T3.date_begin = fr.st_date
                                         LEFT JOIN T4 ON T4.date_begin = fr.st_date
                                         LEFT JOIN T5 ON T5.date_begin = fr.st_date
                                         LEFT JOIN T6 ON T6.date_begin = fr.st_date
                                         LEFT JOIN T7 ON T7.date_begin = fr.st_date
                                         LEFT JOIN T8 ON T8.date_begin = fr.st_date
                                         LEFT JOIN T9 ON T9.date_begin = fr.st_date
                                         LEFT JOIN T10 ON T10.date_begin = fr.st_date
                                         LEFT JOIN T11 ON T11.date_begin = fr.st_date
                                         LEFT JOIN T12 ON T12.date_begin = fr.st_date
                                         LEFT JOIN T13 ON T13.date_begin = fr.st_date
                                         LEFT JOIN T14 ON T14.date_begin = fr.st_date
                                         LEFT JOIN T15 ON T15.date_begin = fr.st_date
                                         LEFT JOIN T16 ON T16.date_begin = fr.st_date
                                         LEFT JOIN T17 ON T17.date_begin = fr.st_date
                                         LEFT JOIN T18 ON T18.date_begin = fr.st_date
                                         LEFT JOIN T19 ON T19.date_begin = fr.st_date
                                         LEFT JOIN T20 ON T20.date_begin = fr.st_date
                                         LEFT JOIN T21 ON T21.date_begin = fr.st_date);   DROP TEMPORARY TABLE IF EXISTS tmp_22_42;

CREATE TEMPORARY TABLE tmp_22_42
(SELECT tt.st_num, tt.st_date, tt.v1, tt.v2, tt.v3, tt.v4, tt.v5, tt.v6, tt.v7, tt.v8, tt.v9, tt.v10, tt.v11, tt.v12,
       tt.v13, tt.v14, tt.v15, tt.v16, tt.v17, tt.v18, tt.v19, tt.v20, tt.v21, COALESCE(T22.val, 0),
       COALESCE(T23.val, 0), COALESCE(T24.val, 0), COALESCE(T25.val, 0), COALESCE(T26.val, 0), COALESCE(T27.val, 0),
       COALESCE(T28.val, 0), COALESCE(T29.val, 0), COALESCE(T30.val, 0), COALESCE(T31.val, 0), COALESCE(T32.val, 0),
       COALESCE(T33.val, 0), COALESCE(T34.val, 0), COALESCE(T35.val, 0), COALESCE(T36.val, 0), COALESCE(T37.val, 0),
       COALESCE(T38.val, 0), COALESCE(T39.val, 0), COALESCE(T40.val, 0), COALESCE(T41.val, 0), COALESCE(T42.val, 0),
       COALESCE(T43.val, 0), COALESCE(T44.val, 0), tt.st_uet
FROM tmp_1_21 tt
         LEFT JOIN T22 ON T22.date_begin = tt.st_date
         LEFT JOIN T23 ON T23.date_begin = tt.st_date
         LEFT JOIN T24 ON T24.date_begin = tt.st_date
         LEFT JOIN T25 ON T25.date_begin = tt.st_date
         LEFT JOIN T26 ON T26.date_begin = tt.st_date
         LEFT JOIN T27 ON T27.date_begin = tt.st_date
         LEFT JOIN T28 ON T28.date_begin = tt.st_date
         LEFT JOIN T29 ON T29.date_begin = tt.st_date
         LEFT JOIN T30 ON T30.date_begin = tt.st_date
         LEFT JOIN T31 ON T31.date_begin = tt.st_date
         LEFT JOIN T32 ON T32.date_begin = tt.st_date
         LEFT JOIN T33 ON T33.date_begin = tt.st_date
         LEFT JOIN T34 ON T34.date_begin = tt.st_date
         LEFT JOIN T35 ON T35.date_begin = tt.st_date
         LEFT JOIN T36 ON T36.date_begin = tt.st_date
         LEFT JOIN T37 ON T37.date_begin = tt.st_date
         LEFT JOIN T38 ON T38.date_begin = tt.st_date
         LEFT JOIN T39 ON T39.date_begin = tt.st_date
         LEFT JOIN T40 ON T40.date_begin = tt.st_date
         LEFT JOIN T41 ON T41.date_begin = tt.st_date
         LEFT JOIN T42 ON T42.date_begin = tt.st_date
         LEFT JOIN T43 ON T43.date_begin = tt.st_date
         LEFT JOIN T44 ON T44.date_begin = tt.st_date);   SELECT *  FROM tmp_22_42  ORDER BY 1;
