set @date_start = '2019-01-27';
set @date_end = '2019-07-17';
create temporary table temp (select cs.id_case, cs.date_begin, vp.profile_infis_code, cs.tooth_name, cs.id_profile,
                                    cs.id_net_profile, sum(tvp.uet) as uet,
                                    date_add(pd.birthday, INTERVAL 15 year) > c.date_begin as AGE1,
                                    date_add(pd.birthday, INTERVAL 18 year) > c.date_begin and
                                    date_add(pd.birthday, INTERVAL 15 year) <= c.date_begin as AGE2, pd.coag_flag as cf
                             from case_services cs
                                      left join vmu_profile vp on vp.id_profile = cs.id_profile
                                      left join cases c on c.id_case = cs.id_case
                                      left join patient_data pd on pd.id_patient = c.id_patient
                                      left join (SELECT tvt.uet, ttvp.id_profile, tvt.id_zone_type
                                                 FROM vmu_profile ttvp,
                                                      vmu_tariff tvt,
                                                      vmu_tariff_plan vtp
                                                 WHERE ttvp.id_profile = tvt.id_profile
                                                   and vtp.id_tariff_group = tvt.id_lpu
                                                   AND tvt.tariff_begin_date <= @date_start
                                                   AND tvt.tariff_end_date >= @date_start
                                                   and vtp.tp_begin_date <= @date_start
                                                   and vtp.tp_end_date >= @date_start
                                                   And vtp.id_lpu in (select id_lpu from mu_ident)) tvp
                                                ON tvp.id_profile = cs.id_profile AND tvp.id_zone_type = cs.id_net_profile
                             where (cs.is_oms = 1 or cs.is_oms = 5)
                                                    and (cs.id_doctor = @doc_id or @doc_id = 0 or @doc_id is null)
                                                    and cs.date_begin between @date_start and @date_end
                             GROUP BY cs.date_begin, cs.id_case, cs.id_profile);
CREATE TEMPORARY TABLE t1 (
	                          SELECT date_begin, count(DISTINCT id_case) AS summ
		                          FROM temp
		                          WHERE profile_infis_code IN ('стт004', 'пст004', 'сст004')
		                          GROUP BY date_begin
		                          ORDER BY date_begin);

CREATE TEMPORARY TABLE t2 (
	                          SELECT date_begin, count(DISTINCT id_case) AS summ
		                          FROM temp
		                          WHERE age1 = TRUE
			                        AND profile_infis_code IN ('стт004', 'пст004', 'сст004')
		                          GROUP BY date_begin
		                          ORDER BY date_begin);

CREATE TEMPORARY TABLE t3 (
	                          SELECT date_begin, count(DISTINCT id_case) AS summ
		                          FROM temp
		                          WHERE age2 = TRUE
			                        AND profile_infis_code IN ('стт004', 'пст004', 'сст004')
		                          GROUP BY date_begin
		                          ORDER BY date_begin);

create temporary table t4 (select date_begin, count(distinct id_case) as summ
                           from temp
                           where profile_infis_code = 'стт048'
                             and (AGE2 or AGE1) = true
                           group by date_begin
                           order by date_begin);
create temporary table t5 (select date_begin, count(distinct id_case) as summ
                           from temp
                           where profile_infis_code = 'стт048'
                             and AGE1 = true
                           group by date_begin
                           order by date_begin);
/* 21.02.19 Sysoev, redefined T6  create temporary table   t6   	(select date_begin, count(id_case) as summ from temp
   where profile_infis_code = 'стт048'    	group by date_begin order by date_begin);  */
create temporary table if not exists t6
(select p.date_protocol as date_begin, sum(char_length(p.teeth_names) div 4 + 1) as summ
from protocols p
where p.id_case in (select distinct id_case from temp where profile_infis_code in ('стт048'))
  and (locate('3623(', p.child_ref_ids) > 0 or locate('19054(', p.child_ref_ids) > 0 or
       locate('79991(', p.child_ref_ids) > 0)
group by p.date_protocol);
create temporary table t7 (select date_begin, count(distinct id_case) as summ
                           from temp
                           where profile_infis_code = 'стт020'
                             and (AGE2 or AGE1) = true
                           group by date_begin
                           order by date_begin);
create temporary table t8 (select date_begin, count(distinct id_case) as summ
                           from temp
                           where profile_infis_code = 'стт020'
                             and AGE1 = true
                           group by date_begin
                           order by date_begin);
/* 21.02.19 Sysoev, redefined T9     create temporary table   t9   	(select date_begin, count(id_case) as summ from temp
   where profile_infis_code = 'стт020'    	group by date_begin order by date_begin);    */
create temporary table if not exists t9
(select p.date_protocol as date_begin, sum(char_length(p.teeth_names) div 4 + 1) as summ
from protocols p
where p.id_case in (select distinct id_case from temp where profile_infis_code in ('стт020'))
  and (locate('39560(', p.child_ref_ids) > 0 or locate('39570(', p.child_ref_ids) > 0 or
       locate('48310(', p.child_ref_ids) > 0 or locate('48320(', p.child_ref_ids) > 0 or
       locate('48330(', p.child_ref_ids) > 0 or locate('48340(', p.child_ref_ids) > 0 or
       locate('48350(', p.child_ref_ids) > 0 or locate('52510(', p.child_ref_ids) > 0 or
       locate('80021(', p.child_ref_ids) > 0)
group by p.date_protocol);
create temporary table t10 (select date_begin, count(distinct id_case) as summ
                            from temp
                            where profile_infis_code = 'стт049'
                              and (AGE2 or AGE1) = true
                            group by date_begin
                            order by date_begin);
create temporary table t11 (select date_begin, count(distinct id_case) as summ
                            from temp
                            where profile_infis_code = 'стт049'
                              and AGE1 = true
                            group by date_begin
                            order by date_begin);
/* 21.02.19 Sysoev, redefined T12    create temporary table   t12  	(select date_begin, count(id_case) as summ from temp
   where profile_infis_code = 'стт049'    	group by date_begin order by date_begin);  */
create temporary table if not exists t12
(select p.date_protocol as date_begin, sum(char_length(p.teeth_names) div 4 + 1) as summ
from protocols p
where p.id_case in (select distinct id_case from temp where profile_infis_code in ('стт049'))
  and (locate('39540(', p.child_ref_ids) > 0 or locate('48250(', p.child_ref_ids) > 0 or
       locate('48260(', p.child_ref_ids) > 0 or locate('48270(', p.child_ref_ids) > 0 or
       locate('91820(', p.child_ref_ids) > 0)
group by p.date_protocol);
create temporary table t13 (select date_begin, count(distinct id_case) as summ
                            from temp
                            where profile_infis_code = 'стт022'
                              and (AGE2 or AGE1) = true
                            group by date_begin
                            order by date_begin);
create temporary table t14 (select date_begin, count(distinct id_case) as summ
                            from temp
                            where profile_infis_code = 'стт022'
                              and AGE1 = true
                            group by date_begin
                            order by date_begin);
/* 21.02.19 Sysoev, redefined T15    create temporary table   t15  	(select date_begin, count(id_case) as summ from temp
   where profile_infis_code = 'стт022'    	group by date_begin order by date_begin);    */
create temporary table if not exists t15
(select p.date_protocol as date_begin, sum(char_length(p.teeth_names) div 4 + 1) as summ
from protocols p
where p.id_case in (select distinct id_case from temp where profile_infis_code in ('стт022'))
  and (locate('3723(', p.child_ref_ids) > 0 or locate('19202(', p.child_ref_ids) > 0 or
       locate('80032(', p.child_ref_ids) > 0)
group by p.date_protocol);
create temporary table t16 (select date_begin, count(distinct id_case) as summ
                            from temp
                            where profile_infis_code = 'стт041'
                              and (AGE2 or AGE1) = true
                            group by date_begin
                            order by date_begin);
create temporary table t17 (select date_begin, count(distinct id_case) as summ
                            from temp
                            where profile_infis_code = 'стт041'
                              and AGE1 = true
                            group by date_begin
                            order by date_begin);
/* 21.02.19 Sysoev, redefined T18 create temporary table   t18  	(select date_begin, count(id_case) as summ from temp
   where profile_infis_code = 'стт041'    	group by date_begin order by date_begin);    */
create temporary table if not exists t18
(select p.date_protocol as date_begin, sum(char_length(p.teeth_names) div 4 + 1) as summ
from protocols p
where p.id_case in (select distinct id_case from temp where profile_infis_code in ('стт041'))
  and (locate('3625(', p.child_ref_ids) > 0 or locate('4464(', p.child_ref_ids) > 0 or
       locate('5443(', p.child_ref_ids) > 0 or locate('19053(', p.child_ref_ids) > 0 or
       locate('89270(', p.child_ref_ids) > 0)
group by p.date_protocol);
create temporary table t19 (select date_begin, count(distinct id_case) as summ
                            from temp
                            where ((profile_infis_code = 'сто022' and date_begin >= '2018-01-01')
                                       or (profile_infis_code = 'стт023' and date_begin between  '2017-04-01' and '2017-12-31')
                                              or (profile_infis_code = 'з122' and date_begin < '2017-04-01'))
                              and (AGE2 or AGE1) = true
                            group by date_begin
                            order by date_begin);
create temporary table t20 (select date_begin, count(distinct id_case) as summ
                            from temp
                            where ((profile_infis_code = 'сто022' and date_begin >= '2018-01-01')
                                       or (profile_infis_code = 'стт023' and date_begin between  '2017-04-01' and '2017-12-31')
                                              or (profile_infis_code = 'з122' and date_begin < '2017-04-01'))
                              and AGE1 = true
                            group by date_begin
                            order by date_begin);
create temporary table t21 (select date_begin, count(distinct t.id_case) as summ
                            from temp t
                                     left join protocols p on p.id_case = t.id_case
                            where (locate('18730', p.child_ref_ids) > 0 or locate('80380', p.child_ref_ids) > 0)
                              and (AGE2 or AGE1) = true
                            group by date_begin
                            order by date_begin);
create temporary table t22 (select date_begin, count(distinct t.id_case) as summ
                            from temp t
                                     left join protocols p on p.id_case = t.id_case
                            where (locate('18730', p.child_ref_ids) > 0 or locate('80380', p.child_ref_ids) > 0)
                              and AGE1 = true
                            group by date_begin
                            order by date_begin);
create temporary table t23 (select date_begin, count(distinct t.id_case) as summ
                            from temp t
                                     left join protocols p on p.id_case = t.id_case
                            where (locate('17239', p.child_ref_ids) > 0 or locate('17251', p.child_ref_ids) > 0 or
                                   locate('18720', p.child_ref_ids) > 0 or locate('19010', p.child_ref_ids) > 0 or
                                   locate('44170', p.child_ref_ids) > 0 or locate('47780', p.child_ref_ids) > 0 or
                                   locate('79760', p.child_ref_ids) > 0 or locate('80370', p.child_ref_ids) > 0)
                              and (AGE2 or AGE1) = true
                            group by date_begin
                            order by date_begin);
create temporary table t24 (select date_begin, count(distinct t.id_case) as summ
                            from temp t
                                     left join protocols p on p.id_case = t.id_case
                            where (locate('17239', p.child_ref_ids) > 0 or locate('17251', p.child_ref_ids) > 0 or
                                   locate('18720', p.child_ref_ids) > 0 or locate('19010', p.child_ref_ids) > 0 or
                                   locate('44170', p.child_ref_ids) > 0 or locate('47780', p.child_ref_ids) > 0 or
                                   locate('79760', p.child_ref_ids) > 0 or locate('80370', p.child_ref_ids) > 0)
                              and AGE1 = true
                            group by date_begin
                            order by date_begin);
create temporary table t25 (select date_begin, count(distinct t.id_case) as summ
                            from temp t
                                     left join protocols p on p.id_case = t.id_case
                            where (locate('18690', p.child_ref_ids) > 0 or locate('19000', p.child_ref_ids) > 0 or
                                   locate('79750', p.child_ref_ids) > 0 or locate('80340', p.child_ref_ids) > 0)
                              and (AGE2 or AGE1) = true
                            group by date_begin
                            order by date_begin);
create temporary table t26 (select date_begin, count(distinct t.id_case) as summ
                            from temp t
                                     left join protocols p on p.id_case = t.id_case
                            where (locate('18690', p.child_ref_ids) > 0 or locate('19000', p.child_ref_ids) > 0 or
                                   locate('79750', p.child_ref_ids) > 0 or locate('80340', p.child_ref_ids) > 0)
                              and AGE1 = true
                            group by date_begin
                            order by date_begin);
create temporary table t27 (SELECT date_begin, ROUND(COALESCE(SUM(UET), 0), 2) as uet
                            FROM temp
                            group by date_begin
                            order by date_begin);
create temporary table t28 (SELECT date_begin, sum(cf) as summ
                            from temp
                            group by date_begin
                            order by date_begin);
SELECT t1.date_begin, coalesce(t1.summ, 0), coalesce(t2.summ, 0), coalesce(t3.summ, 0), coalesce(t28.summ, 0),
       coalesce(t4.summ, 0), coalesce(t5.summ, 0), coalesce(t4.summ, 0) - coalesce(t5.summ, 0), coalesce(t6.summ, 0),
       coalesce(t7.summ, 0), coalesce(t8.summ, 0), coalesce(t7.summ, 0) - coalesce(t8.summ, 0), coalesce(t9.summ, 0),
       coalesce(t10.summ, 0), coalesce(t11.summ, 0), coalesce(t10.summ, 0) - coalesce(t11.summ, 0),
       coalesce(t12.summ, 0), coalesce(t13.summ, 0), coalesce(t14.summ, 0),
       coalesce(t13.summ, 0) - coalesce(t14.summ, 0), coalesce(t15.summ, 0), coalesce(t16.summ, 0),
       coalesce(t17.summ, 0), coalesce(t16.summ, 0) - coalesce(t17.summ, 0), coalesce(t18.summ, 0),
       coalesce(t19.summ, 0), coalesce(t20.summ, 0), coalesce(t19.summ, 0) - coalesce(t20.summ, 0),
       coalesce(t21.summ, 0), coalesce(t22.summ, 0), coalesce(t21.summ, 0) - coalesce(t22.summ, 0),
       coalesce(t23.summ, 0), coalesce(t24.summ, 0), coalesce(t23.summ, 0) - coalesce(t24.summ, 0),
       coalesce(t25.summ, 0), coalesce(t26.summ, 0), coalesce(t25.summ, 0) - coalesce(t26.summ, 0), t27.uet
from t1
         left join t2 on t1.date_begin = t2.date_begin
         left join t3 on t1.date_begin = t3.date_begin
         left join t4 on t1.date_begin = t4.date_begin
         left join t5 on t1.date_begin = t5.date_begin
         left join t6 on t1.date_begin = t6.date_begin
         left join t7 on t1.date_begin = t7.date_begin
         left join t8 on t1.date_begin = t8.date_begin
         left join t9 on t1.date_begin = t9.date_begin
         left join t10 on t1.date_begin = t10.date_begin
         left join t11 on t1.date_begin = t11.date_begin
         left join t12 on t1.date_begin = t12.date_begin
         left join t13 on t1.date_begin = t13.date_begin
         left join t14 on t1.date_begin = t14.date_begin
         left join t15 on t1.date_begin = t15.date_begin
         left join t16 on t1.date_begin = t16.date_begin
         left join t17 on t1.date_begin = t17.date_begin
         left join t18 on t1.date_begin = t18.date_begin
         left join t19 on t1.date_begin = t19.date_begin
         left join t20 on t1.date_begin = t20.date_begin
         left join t21 on t1.date_begin = t21.date_begin
         left join t22 on t1.date_begin = t22.date_begin
         left join t23 on t1.date_begin = t23.date_begin
         left join t24 on t1.date_begin = t24.date_begin
         left join t25 on t1.date_begin = t25.date_begin
         left join t26 on t1.date_begin = t26.date_begin
         left join t27 on t1.date_begin = t27.date_begin
         left join t28 on t1.date_begin = t28.date_begin
group by date_begin
order by date_begin;
