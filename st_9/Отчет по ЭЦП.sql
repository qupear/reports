create temporary table if not exists cases_1
select dep.name name_dep,u.name name_doc, p.id_case
	from protocols p
	left JOIN doctor_spec ds ON p.id_doctor = ds.doctor_id
	left JOIN departments dep ON ds.department_id = dep.id
	left join users u on u.id = ds.user_id
	where p.date_protocol BETWEEN @date_start AND @date_end
		AND (p.id_doctor = @doc_id OR @doc_id = 0)
		AND (ds.department_id = @depart_id OR @depart_id = 0)
group by name_doc, p.id_case;

SELECT c1.name_dep,
	c1.name_doc,
	count(DISTINCT sig.id_case) as countcase,
	count(DISTINCT c1.id_case) - count(DISTINCT sig.id_case) as countdiff
	FROM cases_1 c1
		LEFT JOIN signatures  sig ON c1.id_case = sig.id_case
	WHERE name_dep is not null
GROUP BY c1.name_dep, c1.name_doc
ORDER BY c1.name_dep, c1.name_doc;