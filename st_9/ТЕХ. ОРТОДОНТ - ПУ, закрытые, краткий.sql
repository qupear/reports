select d.name as dept_name, t.name as doc_name, ord.Order_Number, concat(pd.surname, ' ', pd.name, ' ', pd.second_name),
       pd.card_number, u.name, sum(cs.service_cost)
from orders ord
         left join case_services cs on cs.id_order = ord.id
         left join doctor_spec ds on ds.doctor_id = ord.id_doctor
         left join departments d on d.id = ds.department_id
         left join users u on u.id = ds.user_id
         left join patient_data pd on pd.id_human = ord.id_human and pd.is_active = 1 and pd.date_end = '2200-01-01'
         left join technicians t on t.id = ord.id_technician
where ord.orderCompletionDate between @date_start and @date_end
  and (ord.order_type = 0 or ord.order_type = 2)
  and ord.spec = 5
  and (d.id = @depart_id or @depart_id = 0)
  and (ds.doctor_id = @doc_id or @doc_id = 0)
  and (ord.id_technician = @techn_id or @techn_id = 0)
group by ord.id
order by dept_name, doc_name, ord.id;