select coalesce(dms.long_name, 'УФСИН') as smo_name, u.name as doc_name,
       concat('СЛ №', c.id_case, ' от ', date_format(c.date_begin, '%d.%m.%Y'), ', ', pd.surname, ' ', pd.name, ' ',
              pd.second_name, ', ', date_format(pd.birthday, '%d.%m.%Y')) as case_data, p.code, p.name, cs.service_cost,
       count(cs.id_profile), sum(cs.service_cost) as total
from case_services cs
         left join price p on p.id = cs.id_profile
         left join cases c on c.id_case = cs.id_case
         left join patient_data pd on pd.id_patient = c.id_patient
         left join doctor_spec ds on ds.doctor_id = c.id_doctor
         left join users u on u.id = ds.user_id
         left join smo_dms dms on dms.id_smo = pd.dms_id_smo
where cs.is_oms = 3
  and c.date_begin between @date_start and @date_end
group by cs.id_case, cs.id_profile
order by smo_name, doc_name, c.date_begin, c.id_case, p.code;