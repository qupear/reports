select concat_ws(' ', pd.SURNAME, pd.NAME, pd.SECOND_NAME) as pat_name, 'Договор', pd.card_number,
       date_format(hc.contract_date, '%d.%m.%Y'), sum(pr.summ)
from prepayments pr
         left join checks ch on pr.check = ch.id
         left join cases c on pr.case_id = c.ID_case
         left join orders o on pr.order_id = o.id
         inner join patient_data pd on pd.ID_HUMAN = o.id_human or c.ID_PATIENT = pd.ID_PATIENT
         inner join human_contracts hc on hc.id_patient = c.ID_PATIENT or hc.id_human = o.Id_Human
where ch.date between @date_start and @date_end
  and ((pd.IS_ACTIVE = 1 and pd.DATE_END = '2200-01-01') or year(c.DATE_begin) = year(pd.DATE_END))
group by hc.id_patient
order by pat_name;
