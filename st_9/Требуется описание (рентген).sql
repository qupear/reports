select date(d.date_direction) as directed, d.id_case, a.patient_name, u.name, r.date as date_shot,
       concat(if(locate('330', d.child_ref_ids) > 0, 'ОМС ', ''), if(locate('370', d.child_ref_ids) > 0, 'ЦЧЛХ ', ''),
              if(locate('380', d.child_ref_ids) > 0, 'ЛОР ОМС ', ''),
              if(locate('340', d.child_ref_ids) > 0, 'ДПУ ', ''), if(locate('360', d.child_ref_ids) > 0, 'Д/О ', ''),
              if(locate('390', d.child_ref_ids) > 0, 'ЛОР ДПУ ', ''),
              if(locate('350', d.child_ref_ids) > 0, 'БЗП ', ''), if(locate('400', d.child_ref_ids) > 0, 'СП ', ''),
              if(locate('401', d.child_ref_ids) > 0, 'ВнДог ', '')) as pay_type
from directions d
         left join roentgen r on r.id_direction = d.id
         left join appointments a on a.id = d.id_appointment
         left join doctor_spec ds on ds.doctor_id = a.doctor_id
         left join users u on u.id = ds.user_id
where d.id_direction = 2
  and locate(';60;', d.child_ref_ids) > 0
  and r.date_description is null
  and d.date_direction between @date_start and @date_end
  and r.shots > 0
order by r.date;