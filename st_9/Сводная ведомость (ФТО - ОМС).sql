/*      Some general preparations... */
 create temporary table if not exists oms_patients
 (select c.id_patient
 from case_services cs
          left join cases c on c.id_case = cs.id_case
 where cs.id_doctor = @doc_id
   and cs.is_oms = 1
   and cs.date_begin between @date_start and @date_end
 group by c.id_patient);
 create temporary table if not exists chlh_patients
 (select c.id_patient
 from cases c
          left join doctor_spec ds on ds.doctor_id = c.id_doctor
 where date_add(c.DATE_BEGIN, interval 1 month) >= @date_start
   and ds.spec_id = 109
 group by c.id_patient);
 create temporary table if not exists lor_patients
 (select c.id_patient
 from cases c
          left join doctor_spec ds on ds.doctor_id = c.id_doctor
 where date_add(c.DATE_BEGIN, interval 1 month) >= @date_start
   and ds.spec_id = 40
 group by c.id_patient);
 create temporary table if not exists proc_units_oms
 select 'стф001' as c, 2 as ua, 2 as uc
 union
 select 'стф004' as c, 2 as ua, 2.5 as uc
 union
 select 'стф005' as c, 1.5 as ua, 2 as uc
 union
 select 'стф006' as c, 1 as ua, 2 as uc
 union
 select 'стф008' as c, 1 as ua, 1.5 as uc
 union
 select 'стф010' as c, 1 as ua, 1.5 as uc
 union
 select 'стф011' as c, 1.5 as ua, 2 as uc
 union
 select 'стф013' as c, 1.5 as ua, 2 as uc
 union
 select 'стф014' as c, 1.5 as ua, 2 as uc
 union
 select 'стф015' as c, 1 as ua, 1.5 as uc
 union
 select 'стф016' as c, 2 as ua, 2.5 as uc;
 create temporary table if not exists doctor_services (select cs.date_begin, cs.id_case, vp.profile_infis_code,
                                                              vd.diagnosis_code, T.uet, pu.ua, pu.uc, c.id_patient
                                                       from case_services cs
                                                                left join cases c on cs.id_case = c.id_case
                                                                left join vmu_profile vp on vp.id_profile = cs.id_profile
                                                                left join vmu_diagnosis vd on cs.id_diagnosis = vd.id_diagnosis
                                                                left join proc_units_oms pu on pu.c = vp.profile_infis_code
                                                                left join (select vt.uet, vt.id_profile,
                                                                                  vt.id_zone_type,
                                                                                  vt.tariff_begin_date as d1,
                                                                                  vt.tariff_end_date as d2,
                                                                                  vtp.tp_begin_date as d3,
                                                                                  vtp.tp_end_date as d4
                                                                           from vmu_tariff vt,
                                                                                vmu_tariff_plan vtp
                                                                           where vtp.id_tariff_group = vt.id_lpu
                                                                             And vtp.id_lpu in (select id_lpu from mu_ident)) T
                                                                          on T.id_profile = vp.id_profile and
                                                                             cs.date_begin between T.d1 and T.d2 and
                                                                             cs.date_begin between T.d3 and T.d4 and
                                                                             T.id_zone_type = cs.id_net_profile
                                                       where cs.id_doctor = @doc_id
                                                         and cs.is_oms = 1
                                                         and cs.date_begin between @date_start and @date_end);
 create temporary table if not exists doctor_protocols (select c.id_case, c.date_begin, c.id_patient,
                                                               sum(locate('31670', p.child_ref_ids) > 0) as p_no_change,
                                                               sum(locate('31660', p.child_ref_ids) > 0) as p_cured,
                                                               sum(locate('31680', p.child_ref_ids) > 0) as p_better,
                                                               sum(locate('31690', p.child_ref_ids) > 0) as p_worse,
                                                               sum(locate('31640', p.child_ref_ids) > 0) as p_cancelled
                                                        from protocols p
                                                                 left join cases c on p.id_case = c.id_case
                                                        where p.id_doctor = @doc_id
                                                          and c.id_patient in (select id_patient from oms_patients)
                                                          and c.date_begin between @date_start and @date_end
                                                        group by c.id_patient);
/*     The 1st string - unique patients      */
create temporary table if not exists unique_patients
 (select pd.id_patient, date_add(pd.birthday, interval 18 year) > @date_end as child,
        (chlh.id_patient is not null) as chlh, (lor.id_patient is not null) as lor
 from cases c
          left join patient_data pd on pd.id_patient = c.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where c.date_begin between @date_start and @date_end
   and c.id_doctor = @doc_id
   and pd.id_patient in (select id_patient from oms_patients)
 group by pd.id_patient);
set @s1_0 = '1. Принято человек';
 set @s1_1 = (select count(id_patient)
              from unique_patients
              where child = 0 and chlh = 0 and lor = 0);
 set @s1_2 = (select count(id_patient)
              from unique_patients
              where child = 1 and chlh = 0 and lor = 0);
 set @s1_3 = (select count(id_patient)
              from unique_patients
              where chlh = 1);
 set @s1_4 = (select count(id_patient)
              from unique_patients
              where lor = 1);
 set @s1_5 = (select count(id_patient)
              from unique_patients);
/*     The 2st string - visits      */
 create temporary table if not exists visits
     (select c.id_case, pd.id_patient, date_add(pd.birthday, interval 18 year) > @date_end as child,
             (chlh.id_patient is not null) as chlh, (lor.id_patient is not null) as lor
      from cases c
               left join patient_data pd on pd.id_patient = c.id_patient
               left join chlh_patients chlh on chlh.id_patient = pd.id_patient
               left join lor_patients lor on lor.id_patient = pd.id_patient
      where c.date_begin between @date_start and @date_end
        and (c.id_doctor = @doc_id or c.id_doctor = 259)
        and c.id_patient in (select id_patient from oms_patients)
      group by c.id_case);

set @s2_0 = '2. Всего посещений';
 set @s2_1 = (select count(id_case)
              from visits
              where child = 0 and chlh = 0 and lor = 0);

 set @s2_2 = (select count(id_case)
              from visits
              where child = 1 and chlh = 0 and lor = 0);
 set @s2_3 = (select count(id_case)
              from visits
              where chlh = 1);
 set @s2_4 = (select count(id_case)
              from visits
              where lor = 1);
 set @s2_5 = (select count(id_case)
              from visits);

/*     The 3rd string - visits doc      */
create temporary table if not exists visits_doc
 (select ds.id_case, pd.id_patient, date_add(pd.birthday, interval 18 year) > @date_end as child,
        (chlh.id_patient is not null) as chlh, (lor.id_patient is not null) as lor
 from doctor_services ds
          left join patient_data pd on pd.id_patient = ds.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where ds.profile_infis_code = 'стф011'
 group by ds.id_case);
set @s3_0 = '2.1 в т.ч. Врача';
 set @s3_1 = (select count(id_case)
              from visits_doc
              where child = 0 and chlh = 0 and lor = 0);
 set @s3_2 = (select count(id_case)
              from visits_doc
              where child = 1 and chlh = 0 and lor = 0);
 set @s3_3 = (select count(id_case)
              from visits_doc
              where chlh = 1);
 set @s3_4 = (select count(id_case)
              from visits_doc
              where lor = 1);
 set @s3_5 = (select count(id_case)
              from visits_doc);

/*     The 4th string - primary visits doc      */
 create temporary table if not exists sec_patients
 (select c.id_patient
 from case_services cs
          left join vmu_profile vp on vp.id_profile = cs.id_profile
          left join cases c on c.id_case = cs.id_case
 where cs.id_doctor = @doc_id
   and cs.is_oms = 1
   and year(cs.date_begin) = year(@date_start)
   and cs.date_begin < @date_start
   and vp.profile_infis_code = 'стф011'
 group by c.id_patient);
 create temporary table if not exists primary_visits_doc
(select ds.id_case, pd.id_patient, date_add(pd.birthday, interval 18 year) > @date_end as child,
        (chlh.id_patient is not null) as chlh, (lor.id_patient is not null) as lor
 from doctor_services ds
          left join patient_data pd on pd.id_patient = ds.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where ds.profile_infis_code = 'стф011'
   and pd.id_patient not in (select id_patient from sec_patients)
 group by ds.id_case);
set @s4_0 = '2.2 в т.ч. первичных';
 set @s4_1 = (select count(id_case)
              from primary_visits_doc
              where child = 0 and chlh = 0 and lor = 0);
 set @s4_2 = (select count(id_case)
              from primary_visits_doc
              where child = 1 and chlh = 0 and lor = 0);
 set @s4_3 = (select count(id_case)
              from primary_visits_doc
              where chlh = 1);
 set @s4_4 = (select count(id_case)
              from primary_visits_doc
              where lor = 1);
 set @s4_5 = (select count(id_case)
              from primary_visits_doc);

/*     The 5th string - finished the cure      */
create temporary table if not exists finished_cure
 (select pd.id_patient, date_add(pd.birthday, interval 18 year) > @date_end as child,
        (chlh.id_patient is not null) as chlh, (lor.id_patient is not null) as lor
 from doctor_protocols dp
          left join patient_data pd on pd.id_patient = dp.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where dp.p_no_change + dp.p_cured + dp.p_better + dp.p_worse > 0
 group by dp.id_patient);
set @s5_0 = '3. Закончили лечение';
 set @s5_1 = (select count(id_patient)
              from finished_cure
              where child = 0 and chlh = 0 and lor = 0);
 set @s5_2 = (select count(id_patient)
              from finished_cure
              where child = 1 and chlh = 0 and lor = 0);
 set @s5_3 = (select count(id_patient)
              from finished_cure
              where chlh = 1);
 set @s5_4 = (select count(id_patient)
              from finished_cure
              where lor = 1);
 set @s5_5 = (select count(id_patient)
              from finished_cure);

/*     The 6th string - finished the cure, no change      */
 create temporary table if not exists finished_cure_1
 (select pd.id_patient, date_add(pd.birthday, interval 18 year) > @date_end as child,
        (chlh.id_patient is not null) as chlh, (lor.id_patient is not null) as lor
 from doctor_protocols dp
          left join patient_data pd on pd.id_patient = dp.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where dp.p_no_change > 0
 group by dp.id_patient);
set @s6_0 = '3.1 без изменений';
 set @s6_1 = (select count(id_patient)
              from finished_cure_1
              where child = 0 and chlh = 0 and lor = 0);

 set @s6_2 = (select count(id_patient)
              from finished_cure_1
              where child = 1 and chlh = 0 and lor = 0);
 set @s6_3 = (select count(id_patient)
              from finished_cure_1
              where chlh = 1);
 set @s6_4 = (select count(id_patient)
              from finished_cure_1
              where lor = 1);
 set @s6_5 = (select count(id_patient)
              from finished_cure_1);

/*     The 7th string - finished the cure, cured      */
 create temporary table if not exists finished_cure_2
 (select pd.id_patient, date_add(pd.birthday, interval 18 year) > @date_end as child,
        (chlh.id_patient is not null) as chlh, (lor.id_patient is not null) as lor
 from doctor_protocols dp
          left join patient_data pd on pd.id_patient = dp.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where dp.p_cured > 0
 group by dp.id_patient);
set @s7_0 = '3.2 с выздоровлением';
 set @s7_1 = (select count(id_patient)
              from finished_cure_2
              where child = 0 and chlh = 0 and lor = 0);
 set @s7_2 = (select count(id_patient)
              from finished_cure_2
              where child = 1 and chlh = 0 and lor = 0);
 set @s7_3 = (select count(id_patient)
              from finished_cure_2
              where chlh = 1);
 set @s7_4 = (select count(id_patient)
              from finished_cure_2
              where lor = 1);
 set @s7_5 = (select count(id_patient)
              from finished_cure_2);


/*     The 8th string - finished the cure, better      */
 create temporary table if not exists finished_cure_3
 (select pd.id_patient, date_add(pd.birthday, interval 18 year) > @date_end as child,
        (chlh.id_patient is not null) as chlh, (lor.id_patient is not null) as lor
 from doctor_protocols dp
          left join patient_data pd on pd.id_patient = dp.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where dp.p_better > 0
 group by dp.id_patient);
set @s8_0 = '3.3 с улучшением';
 set @s8_1 = (select count(id_patient)
              from finished_cure_3
              where child = 0 and chlh = 0 and lor = 0);
 set @s8_2 = (select count(id_patient)
              from finished_cure_3
              where child = 1 and chlh = 0 and lor = 0);
 set @s8_3 = (select count(id_patient)
              from finished_cure_3
              where chlh = 1);
 set @s8_4 = (select count(id_patient)
              from finished_cure_3
              where lor = 1);
 set @s8_5 = (select count(id_patient)
              from finished_cure_3);

/*     The 9th string - finished the cure, worse      */
 create temporary table if not exists finished_cure_4
 (select pd.id_patient, date_add(pd.birthday, interval 18 year) > @date_end as child,
        (chlh.id_patient is not null) as chlh, (lor.id_patient is not null) as lor
 from doctor_protocols dp
          left join patient_data pd on pd.id_patient = dp.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where dp.p_worse > 0
 group by dp.id_patient);
set @s9_0 = '3.4 с ухудшением';
 set @s9_1 = (select count(id_patient)
              from finished_cure_4
              where child = 0 and chlh = 0 and lor = 0);

 set @s9_2 = (select count(id_patient)
              from finished_cure_4
              where child = 1 and chlh = 0 and lor = 0);
 set @s9_3 = (select count(id_patient)
              from finished_cure_4
              where chlh = 1);

 set @s9_4 = (select count(id_patient)
              from finished_cure_4
              where lor = 1);
set @s9_5 = (select count(id_patient) from finished_cure_4);

/*     The 10th string - finished the cure, cancelled      */
 create temporary table if not exists finished_cure_5
 (select pd.id_patient, date_add(pd.birthday, interval 18 year) > @date_end as child,
        (chlh.id_patient is not null) as chlh, (lor.id_patient is not null) as lor
 from doctor_protocols dp
          left join patient_data pd on pd.id_patient = dp.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where dp.p_cancelled > 0
 group by dp.id_patient);

set @s10_0 = '4. Прервали лечение';
 set @s10_1 = (select count(id_patient)
               from finished_cure_5
               where child = 0 and chlh = 0 and lor = 0);
 set @s10_2 = (select count(id_patient)
               from finished_cure_5
               where child = 1 and chlh = 0 and lor = 0);
 set @s10_3 = (select count(id_patient)
               from finished_cure_5
               where chlh = 1);
 set @s10_4 = (select count(id_patient)
               from finished_cure_5
               where lor = 1);
 set @s10_5 = (select count(id_patient)
               from finished_cure_5);

/*     The 11th string - all procedures      */

 create temporary table if not exists proc_all
     (select ds.id_case, date_add(pd.birthday, interval 18 year) > @date_end as child,
             (chlh.id_patient is not null) as chlh,
             (lor.id_patient is not null) as lor
      from doctor_services ds
               left join patient_data pd on pd.id_patient = ds.id_patient
               left join chlh_patients chlh on chlh.id_patient = pd.id_patient
               left join lor_patients lor on lor.id_patient = pd.id_patient 
			   where ds.profile_infis_code not in ('стф011'));
set @s11_0 = '5. Отпущено процедур всего';
 set @s11_1 = (select count(id_case)
               from proc_all
               where child = 0 and chlh = 0 and lor = 0); set @s11_2 = (select count(id_case) from proc_all where child = 1 and chlh = 0 and lor = 0);
 set @s11_3 = (select count(id_case)
               from proc_all
               where chlh = 1);
 set @s11_4 = (select count(id_case)
               from proc_all
               where lor = 1);
 set @s11_5 = (select count(id_case)
               from proc_all);

/*     The 12th string - electrotherapy      */
create temporary table if not exists proc_all_1
(select ds.id_case, date_add(pd.birthday, interval 18 year) > @date_end as child, (chlh.id_patient is not null) as chlh,
        (lor.id_patient is not null) as lor
 from doctor_services ds
          left join patient_data pd on pd.id_patient = ds.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where ds.profile_infis_code in ('стф001', 'стф004', 'стф005', 'стф0013', 'стф014'));

set @s12_0 = '5.1 Электротерапия, всего ';
 set @s12_1 = (select count(id_case)
               from proc_all_1
               where child = 0 and chlh = 0 and lor = 0); set @s12_2 = (select count(id_case) from proc_all_1 where child = 1 and chlh = 0 and lor = 0);
 set @s12_3 = (select count(id_case)
               from proc_all_1
               where chlh = 1); set @s12_4 = (select count(id_case) from proc_all_1 where lor = 1); set @s12_5 = (select count(id_case) from proc_all_1);
/*     The 13th string - electrotherapy, 005      */ create temporary table if not exists proc_all_11
 select ds.id_case, date_add(pd.birthday, interval 18 year) > @date_end as child, (chlh.id_patient is not null) as chlh,
        (lor.id_patient is not null) as lor
 from doctor_services ds
          left join patient_data pd on pd.id_patient = ds.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where ds.profile_infis_code in ('стф005');   set @s13_0 = '5.1.1 Флюктуоризация';
 set @s13_1 = (select count(id_case)
               from proc_all_11
               where child = 0 and chlh = 0 and lor = 0); set @s13_2 = (select count(id_case) from proc_all_11 where child = 1 and chlh = 0 and lor = 0);
 set @s13_3 = (select count(id_case)
               from proc_all_11
               where chlh = 1); set @s13_4 = (select count(id_case) from proc_all_11 where lor = 1);
 set @s13_5 = (select count(id_case)
               from proc_all_11);   /*     The 14th string - electrotherapy, NO CODE      */ create temporary table if not exists proc_all_12
 select ds.id_case, date_add(pd.birthday, interval 18 year) > @date_end as child, (chlh.id_patient is not null) as chlh,
        (lor.id_patient is not null) as lor
 from doctor_services ds
          left join patient_data pd on pd.id_patient = ds.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where ds.profile_infis_code in ('стф_NO_CODE');   set @s14_0 = '5.1.2 Амплипульстерапия (СМТ-терапия)';
 set @s14_1 = (select count(id_case)
               from proc_all_12
               where child = 0 and chlh = 0 and lor = 0); set @s14_2 = (select count(id_case) from proc_all_12 where child = 1 and chlh = 0 and lor = 0);
 set @s14_3 = (select count(id_case)
               from proc_all_12
               where chlh = 1); set @s14_4 = (select count(id_case) from proc_all_12 where lor = 1);
 set @s14_5 = (select count(id_case)
               from proc_all_12);   /*     The 15th string - electrotherapy, 001      */ create temporary table if not exists proc_all_13
 select ds.id_case, date_add(pd.birthday, interval 18 year) > @date_end as child, (chlh.id_patient is not null) as chlh,
        (lor.id_patient is not null) as lor
 from doctor_services ds
          left join patient_data pd on pd.id_patient = ds.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where ds.profile_infis_code in ('стф001');   set @s15_0 = '5.1.3 Электрофорез'; set @s15_1 = (select count(id_case) from proc_all_13 where child = 0 and chlh = 0 and lor = 0);
 set @s15_2 = (select count(id_case)
               from proc_all_13
               where child = 1 and chlh = 0 and lor = 0); set @s15_3 = (select count(id_case) from proc_all_13 where chlh = 1);
 set @s15_4 = (select count(id_case)
               from proc_all_13
               where lor = 1); set @s15_5 = (select count(id_case) from proc_all_13);  /*     The 16th string - electrotherapy, 013      */
 create temporary table if not exists proc_all_14
 select ds.id_case, date_add(pd.birthday, interval 18 year) > @date_end as child, (chlh.id_patient is not null) as chlh,
        (lor.id_patient is not null) as lor
 from doctor_services ds
          left join patient_data pd on pd.id_patient = ds.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where ds.profile_infis_code in ('стф013');   set @s16_0 = '5.1.4 Ионофорез'; set @s16_1 = (select count(id_case) from proc_all_14 where child = 0 and chlh = 0 and lor = 0);
 set @s16_2 = (select count(id_case)
               from proc_all_14
               where child = 1 and chlh = 0 and lor = 0); set @s16_3 = (select count(id_case) from proc_all_14 where chlh = 1);
 set @s16_4 = (select count(id_case)
               from proc_all_14
               where lor = 1); set @s16_5 = (select count(id_case) from proc_all_14);   /*     The 17th string - electrotherapy, 014      */ create temporary table if not exists proc_all_15
 select ds.id_case, date_add(pd.birthday, interval 18 year) > @date_end as child, (chlh.id_patient is not null) as chlh,
        (lor.id_patient is not null) as lor
 from doctor_services ds
          left join patient_data pd on pd.id_patient = ds.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where ds.profile_infis_code in ('стф014');   set @s17_0 = '5.1.5 Депофорез';
 set @s17_1 = (select count(id_case)
               from proc_all_15
               where child = 0 and chlh = 0 and lor = 0); set @s17_2 = (select count(id_case) from proc_all_15 where child = 1 and chlh = 0 and lor = 0);
 set @s17_3 = (select count(id_case)
               from proc_all_15
               where chlh = 1); set @s17_4 = (select count(id_case) from proc_all_15 where lor = 1);
 set @s17_5 = (select count(id_case)
               from proc_all_15);    /*     The 18th string - electrotherapy, 004      */ create temporary table if not exists proc_all_16
 select ds.id_case, date_add(pd.birthday, interval 18 year) > @date_end as child, (chlh.id_patient is not null) as chlh,
        (lor.id_patient is not null) as lor
 from doctor_services ds
          left join patient_data pd on pd.id_patient = ds.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where ds.profile_infis_code in ('стф004');   set @s18_0 = '5.1.6 Местная дарсонвализация';
 set @s18_1 = (select count(id_case)
               from proc_all_16
               where child = 0 and chlh = 0 and lor = 0); set @s18_2 = (select count(id_case) from proc_all_16 where child = 1 and chlh = 0 and lor = 0);
 set @s18_3 = (select count(id_case)
               from proc_all_16
               where chlh = 1); set @s18_4 = (select count(id_case) from proc_all_16 where lor = 1);
 set @s18_5 = (select count(id_case)
               from proc_all_16);  /*     The 19th string - electromagnetotherapy      */ create temporary table if not exists proc_all_2
 select ds.id_case, date_add(pd.birthday, interval 18 year) > @date_end as child, (chlh.id_patient is not null) as chlh,
        (lor.id_patient is not null) as lor
 from doctor_services ds
          left join patient_data pd on pd.id_patient = ds.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where ds.profile_infis_code in ('стф006', 'стф008', 'стф015');   set @s19_0 = '5.2 Электромагнитотерапия, всего ';
 set @s19_1 = (select count(id_case)
               from proc_all_2
               where child = 0 and chlh = 0 and lor = 0); set @s19_2 = (select count(id_case) from proc_all_2 where child = 1 and chlh = 0 and lor = 0);
 set @s19_3 = (select count(id_case)
               from proc_all_2
               where chlh = 1); set @s19_4 = (select count(id_case) from proc_all_2 where lor = 1);
 set @s19_5 = (select count(id_case)
               from proc_all_2);   /*     The 20th string - electromagnetotherapy, 008      */ create temporary table if not exists proc_all_21
 select ds.id_case, date_add(pd.birthday, interval 18 year) > @date_end as child, (chlh.id_patient is not null) as chlh,
        (lor.id_patient is not null) as lor
 from doctor_services ds
          left join patient_data pd on pd.id_patient = ds.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where ds.profile_infis_code in ('стф008');   set @s20_0 = '5.2.1 Воздействие токами УВЧ ';
 set @s20_1 = (select count(id_case)
               from proc_all_21
               where child = 0 and chlh = 0 and lor = 0); set @s20_2 = (select count(id_case) from proc_all_21 where child = 1 and chlh = 0 and lor = 0);
 set @s20_3 = (select count(id_case)
               from proc_all_21
               where chlh = 1); set @s20_4 = (select count(id_case) from proc_all_21 where lor = 1);
 set @s20_5 = (select count(id_case)
               from proc_all_21);   /*     The 21th string - electromagnetotherapy, 006      */ create temporary table if not exists proc_all_22
 select ds.id_case, date_add(pd.birthday, interval 18 year) > @date_end as child, (chlh.id_patient is not null) as chlh,
        (lor.id_patient is not null) as lor
 from doctor_services ds
          left join patient_data pd on pd.id_patient = ds.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where ds.profile_infis_code in ('стф006');   set @s21_0 = '5.2.2 СМВ-терапия ';
 set @s21_1 = (select count(id_case)
               from proc_all_22
               where child = 0 and chlh = 0 and lor = 0); set @s21_2 = (select count(id_case) from proc_all_22 where child = 1 and chlh = 0 and lor = 0);
 set @s21_3 = (select count(id_case)
               from proc_all_22
               where chlh = 1); set @s21_4 = (select count(id_case) from proc_all_22 where lor = 1);
 set @s21_5 = (select count(id_case)
               from proc_all_22);
/*     The 22th string - electromagnetotherapy, 015      */
 create temporary table if not exists proc_all_23
 (select ds.id_case, date_add(pd.birthday, interval 18 year) > @date_end as child, (chlh.id_patient is not null) as chlh,
        (lor.id_patient is not null) as lor
 from doctor_services ds
          left join patient_data pd on pd.id_patient = ds.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where ds.profile_infis_code in ('стф015'));

set @s22_0 = '5.2.2 УВЧ-индуктотермия ';
 set @s22_1 = (select count(id_case)
               from proc_all_23
               where child = 0 and chlh = 0 and lor = 0);
 set @s22_2 = (select count(id_case)
               from proc_all_23
               where child = 1 and chlh = 0 and lor = 0);
 set @s22_3 = (select count(id_case)
               from proc_all_23
               where chlh = 1);
 set @s22_4 = (select count(id_case)
               from proc_all_23
               where lor = 1);
 set @s22_5 = (select count(id_case)
               from proc_all_23);
/*     The 23th string - phototherapy      */
 create temporary table if not exists proc_all_3
 (select ds.id_case, date_add(pd.birthday, interval 18 year) > @date_end as child, (chlh.id_patient is not null) as chlh,
        (lor.id_patient is not null) as lor
 from doctor_services ds
          left join patient_data pd on pd.id_patient = ds.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where ds.profile_infis_code in ('стф010'));

set @s23_0 = '5.3 Фототерапия (КУФ) ';
 set @s23_1 = (select count(id_case)
               from proc_all_3
               where child = 0 and chlh = 0 and lor = 0);
 set @s23_2 = (select count(id_case)
               from proc_all_3
               where child = 1 and chlh = 0 and lor = 0);
 set @s23_3 = (select count(id_case)
               from proc_all_3
               where chlh = 1);
 set @s23_4 = (select count(id_case)
               from proc_all_3
               where lor = 1);
 set @s23_5 = (select count(id_case)
               from proc_all_3);
/*     The 24th string - mechanotherapy      */
 create temporary table if not exists proc_all_4
 (select ds.id_case, date_add(pd.birthday, interval 18 year) > @date_end as child, (chlh.id_patient is not null) as chlh,
        (lor.id_patient is not null) as lor
 from doctor_services ds
          left join patient_data pd on pd.id_patient = ds.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient
 where ds.profile_infis_code in ('стф016'));

set @s24_0 = '5.4 Механотерапия (ультрафонофорез) ';
 set @s24_1 = (select count(id_case)
               from proc_all_4
               where child = 0 and chlh = 0 and lor = 0);
 set @s24_2 = (select count(id_case)
               from proc_all_4
               where child = 1 and chlh = 0 and lor = 0);
 set @s24_3 = (select count(id_case)
               from proc_all_4
               where chlh = 1);
 set @s24_4 = (select count(id_case)
               from proc_all_4
               where lor = 1);
 set @s24_5 = (select count(id_case)
               from proc_all_4);

/*     The 25th string - P-Units      */
create temporary table if not exists proc_un
 (select ds.ua, ds.uc, ds.uet, date_add(pd.birthday, interval 18 year) > @date_end as child,
        (chlh.id_patient is not null) as chlh, (lor.id_patient is not null) as lor
 from doctor_services ds
          left join patient_data pd on pd.id_patient = ds.id_patient
          left join chlh_patients chlh on chlh.id_patient = pd.id_patient
          left join lor_patients lor on lor.id_patient = pd.id_patient) ;

set @s25_0 = '6. Процедурных единиц ';
 set @s25_1 = (select sum(coalesce(ua, 0))
               from proc_un
               where child = 0 and chlh = 0 and lor = 0);
 set @s25_2 = (select sum(coalesce(uc, 0))
               from proc_un
               where child = 1 and chlh = 0 and lor = 0);
 set @s25_3_1 = (select sum(coalesce(ua, 0))
                 from proc_un
                 where chlh = 1
                   and child = 0);
 set @s25_3_2 = (select sum(coalesce(uc, 0))
                 from proc_un
                 where chlh = 1
                   and child = 1);
 set @s25_3 = coalesce(@s25_3_1, 0) + coalesce(@s25_3_2, 0);
 set @s25_4_1 = (select sum(coalesce(ua, 0))
                 from proc_un
                 where lor = 1
                   and child = 0);
 set @s25_4_2 = (select sum(coalesce(uc, 0))
                 from proc_un
                 where lor = 1
                   and child = 1);
 set @s25_4 = coalesce(@s25_4_1, 0) + coalesce(@s25_4_2, 0);
 set @s25_5 = coalesce(@s25_1, 0) + coalesce(@s25_2, 0) + coalesce(@s25_3,0) + coalesce(@s25_4,0);

/*     The 26th string - UET      */
 set @s26_0 = '7. УЕТ ';
 set @s26_1 = (select ROUND(sum(coalesce(uet, 0)), 2)
               from proc_un
               where child = 0 and chlh = 0 and lor = 0);

 set @s26_2 = (select ROUND(sum(coalesce(uet, 0)), 2)
               from proc_un
               where child = 1 and chlh = 0 and lor = 0); set @s26_3 = (select ROUND(sum(coalesce(uet,0)),2) from proc_un where chlh = 1);
 set @s26_4 = (select ROUND(sum(coalesce(uet, 0)), 2)
               from proc_un
               where lor = 1); set @s26_5 = coalesce(@s26_1,0) + coalesce(@s26_2,0) + coalesce(@s26_3,0) + coalesce(@s26_4,0);

/*     The Final Select       */
 select @s1_0, @s1_1, @s1_2, @s1_3, @s1_4, @s1_5
 union
 select @s2_0, @s2_1, @s2_2, @s2_3, @s2_4, @s2_5
 union
 select @s3_0, @s3_1, @s3_2, @s3_3, @s3_4, @s3_5
 union
 select @s4_0, @s4_1, @s4_2, @s4_3, @s4_4, @s4_5
 union
 select @s5_0, @s5_1, @s5_2, @s5_3, @s5_4, @s5_5
 union
 select @s6_0, @s6_1, @s6_2, @s6_3, @s6_4, @s6_5
 union
 select @s7_0, @s7_1, @s7_2, @s7_3, @s7_4, @s7_5
 union
 select @s8_0, @s8_1, @s8_2, @s8_3, @s8_4, @s8_5
 union
 select @s9_0, @s9_1, @s9_2, @s9_3, @s9_4, @s9_5
 union
 select @s10_0, @s10_1, @s10_2, @s10_3, @s10_4, @s10_5
 union
 select @s11_0, @s11_1, @s11_2, @s11_3, @s11_4, @s11_5
 union
 select @s12_0, @s12_1, @s12_2, @s12_3, @s12_4, @s12_5
 union
 select @s13_0, @s13_1, @s13_2, @s13_3, @s13_4, @s13_5
 union
 select @s14_0, @s14_1, @s14_2, @s14_3, @s14_4, @s14_5
 union
 select @s15_0, @s15_1, @s15_2, @s15_3, @s15_4, @s15_5
 union
 select @s16_0, @s16_1, @s16_2, @s16_3, @s16_4, @s16_5
 union
 select @s17_0, @s17_1, @s17_2, @s17_3, @s17_4, @s17_5
 union
 select @s18_0, @s18_1, @s18_2, @s18_3, @s18_4, @s18_5
 union
 select @s19_0, @s19_1, @s19_2, @s19_3, @s19_4, @s19_5
 union
 select @s20_0, @s20_1, @s20_2, @s20_3, @s20_4, @s20_5
 union
 select @s21_0, @s21_1, @s21_2, @s21_3, @s21_4, @s21_5
 union
 select @s22_0, @s22_1, @s22_2, @s22_3, @s22_4, @s22_5
 union
 select @s23_0, @s23_1, @s23_2, @s23_3, @s23_4, @s23_5
 union
 select @s24_0, @s24_1, @s24_2, @s24_3, @s24_4, @s24_5
 union
 select @s25_0, @s25_1, @s25_2, @s25_3, @s25_4, @s25_5
 union
 select @s26_0, @s26_1, @s26_2, @s26_3, @s26_4, @s26_5;
