select dep.name as dt, u.name as doc, sp.name as spec, c.date_begin as db, count(distinct c.id_case) as open,
 	count(distinct c2.id_case) as close, count(distinct c1.id_case) as exp
 from cases c
 join case_services cs on c.id_case = cs.id_case and cs.is_oms=1
 left join doctor_spec ds on ds.doctor_id = c.ID_DOCTOR
 left join users u on u.id = ds.user_id
 left join speciality sp on sp.id = ds.spec_id
 left join departments dep on dep.id = ds.department_id
 left join cases c1 on c1.id_case = c.id_case and c1.exported =1
 left join cases c2 on c2.id_case = c.id_case and c2.date_end is not null
 where c.DATE_BEGIN between @date_start and @date_end
 group by dt, doc, spec, db
 order by dt, doc, spec, db