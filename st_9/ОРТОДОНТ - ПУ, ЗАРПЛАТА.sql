select TM.dept_name, TM.doc_name, coalesce(T1.total_sum, 0), coalesce(T12.total_uet, 0), coalesce(T2.total, 0),
       coalesce(T3.total, 0), coalesce(T1.total_sum, 0) + coalesce(T2.total, 0)
from (select d.name as dept_name, u.name as doc_name, ds.doctor_id
      from doctor_spec ds
               left join users u on u.id = ds.user_id
               left join departments d on d.id = ds.department_id
      where ds.spec_id = 103
        and u.id is not null
        and u.id <> 1
      order by dept_name, doc_name) TM
         left join (select cs.id_doctor, sum(cs.service_cost) as total_sum, sum(p.uet) as total_uet
                    from case_services cs
                             left join doctor_spec ds on ds.doctor_id = cs.id_doctor
                             left join price p on p.id = cs.id_profile
                    where cs.id_order < 0
                      and ds.spec_id = 103
                      and cs.is_oms = 0
                      and cs.date_begin between @date_start and @date_end
                    group by cs.id_doctor) T1 on T1.id_doctor = TM.doctor_id
         left join (select ROUND(COALESCE(SUM(vt.uet), 0), 2) as total_uet, cs.id_doctor
                    from (case_services cs, vmu_tariff vt, vmu_tariff_plan vtp)
                             left join doctor_spec ds on ds.doctor_id = cs.id_doctor
                    where cs.date_begin between @date_start and @date_end
                      and (cs.is_oms = 1 or cs.is_oms = 5)
                      and cs.id_order = -1
                      and vt.id_profile = cs.id_profile
                      and vt.id_zone_type = cs.id_net_profile
                      and cs.date_begin between vt.tariff_begin_date and vt.tariff_end_date
                      and vtp.id_tariff_group = vt.id_lpu
                      and cs.date_begin between vtp.tp_begin_date and vtp.tp_end_date
                      and ds.spec_id = 103
                    group by ds.doctor_id) T12 on T12.id_doctor = TM.doctor_id
         left join (select cs.id_doctor, sum(cs.service_cost) as total
                    from case_services cs
                             left join stomadb.orders ord on ord.id = cs.id_order
                             left join doctor_spec ds on ds.doctor_id = ord.id_doctor
                    where ord.order_type = 0
                      and ds.spec_id = 103
                      and ord.status = 2
                      and orderCompletionDate between @date_start and @date_end
                      and cs.id_order > 0
                    group by cs.id_doctor) T2 on T2.id_doctor = TM.doctor_id
         left join (select sum(p.summ) as total, p.doctor_id
                    from prepayments p
                             left join checks ch on ch.id = p.check
                    where ch.date between @date_start and @date_end
                    group by p.doctor_id) T3 on T3.doctor_id = TM.doctor_id;
