create temporary table if not exists pats
select distinct pd.id_human as pat_id, (DATE_ADD(pd.birthday, INTERVAL 18 YEAR) > current_date) as child
from patient_data pd
where pd.IS_ACTIVE = 1 and pd.date_end = '2200-01-01' and pd.coag_flag = 1;

select
	count(p.pat_id),
	sum(p.child)
from pats p