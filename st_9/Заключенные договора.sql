set @row = 0;
  
 select 
     @row:=@row + 1, T . *
 from
     (select 
         concat_ws(' ', pd.surname, pd.name, pd.second_name) as pat_name,
             pa.unstruct_address,
             pd.CARD_NUMBER,
             u.name
     from
         (select 
         ID_HUMAN, id_patient, min(contract_date) as date_c
     from
         human_contracts hc
     where
         contract_date between @date_start and @date_end
     group by id_human) as dc
     left join patient_data pd ON pd.ID_PATIENT = dc.id_patient
     left join patient_address pa ON pd.ID_ADDRESS_REG = pa.ID_ADDRESS
     left join cases c ON c.ID_PATIENT = pd.ID_PATIENT
     left join orders o ON o.id_human = pd.id_human
     inner join (select 
         id_doctor, min(date_begin), id_case, id_order
     from
         case_services cs
     where
         year(date_begin) between @date_start and @date_end
     group by id_case , id_doctor) as doc ON (doc.id_case = c.id_case
         and id_order = - 1)
         or (doc.id_case = c.id_case
         and doc.ID_ORDER = o.id)
     left join doctor_spec ds ON doc.id_doctor = ds.doctor_id
     left join users u ON ds.user_id = u.id
     where
         date_c between @date_start and @date_end
     group by pd.ID_HUMAN
     order by pat_name) T 
 union select 
     '', '', '', 'Итого договоров:', count(*)
 from
     (select 
         concat_ws(' ', pd.surname, pd.name, pd.second_name) as pat_name
     from
         (select 
         ID_HUMAN, id_patient, min(contract_date) as date_c
     from
         human_contracts hc
     where
         contract_date between @date_start and @date_end
     group by id_human) as dc
     left join patient_data pd ON pd.ID_PATIENT = dc.id_patient
     left join patient_address pa ON pd.ID_ADDRESS_REG = pa.ID_ADDRESS
     left join cases c ON c.ID_PATIENT = pd.ID_PATIENT
     left join orders o ON o.id_human = pd.id_human
     inner join (select 
         id_doctor, min(date_begin), id_case, id_order
     from
         case_services cs
     where
         year(date_begin) between @date_start and @date_end
     group by id_case , id_doctor) as doc ON (doc.id_case = c.id_case
         and id_order = - 1)
         or (doc.id_case = c.id_case
         and doc.ID_ORDER = o.id)
     left join doctor_spec ds ON doc.id_doctor = ds.doctor_id
     left join users u ON ds.user_id = u.id
     where
         date_c between @date_start and @date_end
                 group by pd.ID_HUMAN
     order by pat_name) T