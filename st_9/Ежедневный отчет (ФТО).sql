create temporary table if not exists proc_units_oms
select 'стф001' as c, 2 as ua, 2 as uc
union
select 'стф004' as c, 2 as ua, 2.5 as uc
union
select 'стф005' as c, 1.5 as ua, 2 as uc
union
select 'стф006' as c, 1 as ua, 2 as uc
union
select 'стф008' as c, 1 as ua, 1.5 as uc
union
select 'стф010' as c, 1 as ua, 1.5 as uc
union
select 'стф011' as c, 1.5 as ua, 2 as uc
union
select 'стф013' as c, 1.5 as ua, 2 as uc
union
select 'стф014' as c, 1.5 as ua, 2 as uc
union
select 'стф015' as c, 1 as ua, 1.5 as uc
union
select 'стф016' as c, 2 as ua, 2.5 as uc;
create temporary table if not exists proc_units_pu
select '4001' as c, 1.5 as ua, 2 as uc
union
select '4002' as c, 1.5 as ua, 2 as uc
union
select '4003' as c, 2 as ua, 2.5 as uc
union
select '4004' as c, 1 as ua, 1.5 as uc
union
select '4005' as c, 1 as ua, 1.5 as uc
union
select '4006' as c, 2 as ua, 2.5 as uc
union
select '4007' as c, 2 as ua, 2 as uc
union
select '4008' as c, 1 as ua, 1.5 as uc
union
select '4009' as c, 1 as ua, 2 as uc
union
select '4010' as c, 1.5 as ua, 2 as uc
union
select '4011' as c, 1 as ua, 2 as uc
union
select '4012' as c, 1 as ua, 1.5 as uc
union
select '7013' as c, 2 as ua, 3 as uc
union
select '4014' as c, 1.5 as ua, 2 as uc
union
select '4015' as c, 1 as ua, 1.5 as uc
union
select '4019' as c, 1 as ua, 1.5 as uc
union
select '4020' as c, 1 as ua, 1.5 as uc
union
select '4021' as c, 1 as ua, 1.5 as uc
union
select '4022' as c, 2 as ua, 2 as uc
union
select '4023' as c, 2 as ua, 2 as uc
union
select '4024' as c, 2 as ua, 2 as uc
union
select '4027' as c, 2 as ua, 2.5 as uc
union
select '4028' as c, 2 as ua, 2 as uc
union
select '7012' as c, 2 as ua, 2 as uc;
create temporary table if not exists doc_cases(select c.id_case
                                               from cases c
left join case_services cs on c.ID_CASE = cs.ID_CASE
                                               where c.date_begin = @date_start
                                                 and (c.id_doctor = @doc_id or c.id_doctor = 259) and cs.ID_SERVICE is not null
                                               group by c.id_case);
create temporary table if not exists chlh_patients(select c.id_patient
                                                   from cases c
                                                            left join doctor_spec ds on ds.doctor_id = c.id_doctor
                                                   where date_add(c.DATE_BEGIN, interval 1 month) >= @date_start
                                                     and ds.spec_id = 109
                                                   group by c.id_patient);
create temporary table if not exists services_oms (select T.id_case, T.id_doctor, group_concat(
        concat(coalesce(T.profile_infis_code, ''), ' (', coalesce(T.cnt, 0), ')') separator ', ') as codes,
                                                          sum(T.uet) as uet, sum(T.pu_a) as proc_a,
                                                          sum(T.pu_c) as proc_c
                                                   from (select cs.id_case, cs.id_doctor, count(cs.id_profile) as cnt,
                                                                tvp.profile_infis_code,
                                                                sum(coalesce(tvp.UET, 0)) as uet,
                                                                sum(coalesce(pu.ua, 0)) as pu_a,
                                                                sum(coalesce(pu.uc, 0)) as pu_c
                                                         from case_services cs
                                                                  left join (select ttvp.profile_infis_code, tvt.uet,
                                                                                    ttvp.id_profile, tvt.id_zone_type
                                                                             from vmu_profile ttvp,
                                                                                  vmu_tariff tvt,
                                                                                  vmu_tariff_plan vtp
                                                                             where ttvp.id_profile = tvt.id_profile
                                                                               and vtp.id_tariff_group = tvt.id_lpu
                                                                               and tvt.tariff_begin_date <= @date_start
                                                                               and tvt.tariff_end_date >= @date_start
                                                                               and vtp.tp_begin_date <= @date_start
                                                                               and vtp.tp_end_date >= @date_start
                                                                               and vtp.id_lpu in (select id_lpu from mu_ident)) tvp
                                                                            ON tvp.id_profile = cs.id_profile and tvp.id_zone_type = cs.id_net_profile
                                                                  left join proc_units_oms pu on pu.c = tvp.profile_infis_code
                                                                  left join cases c on c.id_case = cs.id_case
                                                         where cs.date_begin = @date_start
                                                           and cs.id_doctor = @doc_id
                                                           and cs.is_oms = 1
                                                           and c.id_patient not in (select id_patient from chlh_patients)
                                                         group by cs.id_case, cs.id_profile) T
                                                   group by T.id_case);
create temporary table if not exists services_chlh (select T.id_case, T.id_doctor, group_concat(
        concat(coalesce(T.profile_infis_code, ''), ' (', coalesce(T.cnt, 0), ')') separator ', ') as codes,
                                                           sum(T.uet) as uet, sum(T.pu_a) as proc_a,
                                                           sum(T.pu_c) as proc_c
                                                    from (select cs.id_case, cs.id_doctor, count(cs.id_profile) as cnt,
                                                                 tvp.profile_infis_code,
                                                                 sum(coalesce(tvp.UET, 0)) as uet,
                                                                 sum(coalesce(pu.ua, 0)) as pu_a,
                                                                 sum(coalesce(pu.uc, 0)) as pu_c
                                                          from case_services cs
                                                                   left join (select ttvp.profile_infis_code, tvt.uet,
                                                                                     ttvp.id_profile, tvt.id_zone_type
                                                                              from vmu_profile ttvp,
                                                                                   vmu_tariff tvt,
                                                                                   vmu_tariff_plan vtp
                                                                              where ttvp.id_profile = tvt.id_profile
                                                                                and vtp.id_tariff_group = tvt.id_lpu
                                                                                and tvt.tariff_begin_date <= @date_start
                                                                                and tvt.tariff_end_date >= @date_start
                                                                                and vtp.tp_begin_date <= @date_start
                                                                                and vtp.tp_end_date >= @date_start
                                                                                and vtp.id_lpu in (select id_lpu from mu_ident)) tvp
                                                                             ON tvp.id_profile = cs.id_profile and tvp.id_zone_type = cs.id_net_profile
                                                                   left join proc_units_oms pu on pu.c = tvp.profile_infis_code
                                                                   left join cases c on c.id_case = cs.id_case
                                                          where cs.date_begin = @date_start
                                                            and cs.id_doctor = @doc_id
                                                            and cs.is_oms = 1
                                                            and c.id_patient in (select id_patient from chlh_patients)
                                                          group by cs.id_case, cs.id_profile) T
                                                    group by T.id_case);
create temporary table if not exists services_pu (select T.id_case, T.id_doctor, group_concat(
        concat(coalesce(T.code, ''), ' (', coalesce(T.cnt, 0), ')') separator ', ') as codes, sum(T.uet) as uet,
                                                         sum(T.pu_a) as proc_a, sum(T.pu_c) as proc_c
                                                  from (select cs.id_case, cs.id_doctor, cs.id_profile,
                                                               count(cs.id_profile) as cnt, p.code,
                                                               sum(coalesce(p.UET, 0)) as uet,
                                                               sum(coalesce(pu.ua, 0)) as pu_a,
                                                               sum(coalesce(pu.uc, 0)) as pu_c
                                                        from case_services cs
                                                                 left join price p ON p.id = cs.id_profile
                                                                 left join proc_units_pu pu on pu.c = p.code
                                                        where cs.date_begin = @date_start
                                                          and cs.id_doctor = @doc_id
                                                          and cs.is_oms = 0
                                                        group by cs.id_case, cs.id_profile) T
                                                  group by T.id_case);
create temporary table if not exists sec_patients
 (select c.id_patient, c.id_case
 from case_services cs
          left join vmu_profile vp on vp.id_profile = cs.id_profile
          left join cases c on c.id_case = cs.id_case
			   left join doctor_spec ds on ds.doctor_id = c.ID_DOCTOR
 where (ds.spec_id = 83 or ds.spec_id = 212)
   and cs.is_oms = 1
   and year(cs.date_begin) = year(@date_start)
   and cs.date_begin < @date_start
   and vp.profile_infis_code = 'стф011'
 group by c.id_patient);
create temporary table if not exists prim_visits (select id_case
                                                  from case_services cs
                                                  where cs.date_begin = @date_start
                                                    and cs.id_doctor = @doc_id
                                                    and (cs.id_profile IN (select id_profile
                                                                           from vmu_profile
                                                                           where profile_infis_code in ('стф011')) or
                                                         cs.id_profile IN (select id from price where code in ('4001')))
													and id_case not in (select id_case from sec_patients)
                                                  group by id_case);
set @row = 0;
select @row := @row + 1, T_MAIN.time, T_MAIN.pat_name, T_MAIN.birthday, T_MAIN.unstruct_address, T_MAIN.prim,
       T_MAIN.diagnosis_code, T_MAIN.codes_oms, T_MAIN.codes_chlh, T_MAIN.codes_pu, T_MAIN.proc_oms, T_MAIN.proc_chlh,
       T_MAIN.proc_pu, T_MAIN.uet_oms, T_MAIN.uet_pu
from (select a.time, concat(pd.surname, ' ', pd.name, ' ', pd.second_name) as pat_name, pd.birthday,
             pa.unstruct_address, (count(pv.id_case) > 0) as prim, vd.diagnosis_code,
             coalesce(T1.codes, '') as codes_oms, coalesce(T2.codes, '') as codes_pu,
             coalesce(T3.codes, '') as codes_chlh,
             coalesce(T1.proc_a, 0) * (date_add(pd.birthday, interval 18 year) <= c.date_begin) +
             coalesce(T1.proc_c, 0) * (date_add(pd.birthday, interval 18 year) > c.date_begin) as proc_oms,
             coalesce(T2.proc_a, 0) * (date_add(pd.birthday, interval 18 year) <= c.date_begin) +
             coalesce(T2.proc_c, 0) * (date_add(pd.birthday, interval 18 year) > c.date_begin) as proc_pu,
             coalesce(T3.proc_a, 0) * (date_add(pd.birthday, interval 18 year) <= c.date_begin) +
             coalesce(T3.proc_c, 0) * (date_add(pd.birthday, interval 18 year) > c.date_begin) as proc_chlh,
             round(coalesce(T1.uet, 0), 2) + round(coalesce(T3.uet, 0), 2) as uet_oms,
             round(coalesce(T2.uet, 0), 2) as uet_pu
      from doc_cases CS_MAIN
               left join cases c on c.id_case = CS_MAIN.id_case
               left join vmu_diagnosis vd on vd.id_diagnosis = c.id_diagnosis
               left join patient_data pd on pd.id_patient = c.id_patient
               left join patient_address pa on pa.id_address = pd.id_address_reg
               left join appointments a on a.case_id = c.id_case
               left join prim_visits pv on pv.id_case = c.id_case
               left join services_oms T1 ON T1.id_case = c.id_case
               left join services_pu T2 ON T2.id_case = c.id_case
               left join services_chlh T3 ON T3.id_case = c.id_case
      group by CS_MAIN.id_case
      order by a.time) T_MAIN;
