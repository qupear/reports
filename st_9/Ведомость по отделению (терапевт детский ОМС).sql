
CREATE TEMPORARY TABLE IF NOT EXISTS doctor_services (SELECT c.id_case, cs.date_begin, vp.profile_infis_code, vd.diagnosis_code, cs.tooth_name, T.uet, cs.is_oms, (DATE_ADD(pd.birthday, INTERVAL 1 YEAR) > c.date_begin) AS age_0, (DATE_ADD(pd.birthday, INTERVAL 15 YEAR) > c.date_begin) AS age_1, (DATE_ADD(pd.birthday, INTERVAL 15 YEAR) < c.date_begin
AND DATE_ADD(pd.birthday, INTERVAL 18 YEAR) > c.date_begin) AS age_2, (DATE_ADD(pd.birthday, INTERVAL 18 YEAR) > c.date_begin) AS child, cs.tooth_name IN ('51', '52', '53', '54', '55', '61', '62', '63', '64', '65', '71', '72', '73', '74', '75', '81', '82', '83', '84', '85') AS milk_tooth, pd.coag_flag as cf
FROM case_services cs
LEFT JOIN cases c ON
c.id_case = cs.id_case
LEFT JOIN patient_data pd ON
pd.id_patient = c.id_patient
LEFT JOIN vmu_profile vp ON
vp.id_profile = cs.id_profile
LEFT JOIN doctor_spec ds ON
cs.id_doctor = ds.doctor_id
LEFT JOIN (SELECT vt.uet, vt.id_profile, vt.id_zone_type, vt.tariff_begin_date AS d1, vt.tariff_end_date AS d2, vtp.tp_begin_date AS d3, vtp.tp_end_date AS d4
FROM vmu_tariff vt, vmu_tariff_plan vtp
WHERE vtp.id_tariff_group = vt.id_lpu
AND vtp.id_lpu IN (SELECT id_lpu
FROM mu_ident)) T ON
T.id_profile = vp.id_profile
AND cs.date_begin BETWEEN T.d1 AND T.d2
AND cs.date_begin BETWEEN T.d3 AND T.d4
AND T.id_zone_type = cs.id_net_profile
LEFT JOIN vmu_diagnosis vd ON
cs.id_diagnosis = vd.id_diagnosis
WHERE (ds.department_id = @depart_id
OR @depart_id = 0)
AND ds.spec_id = 105
AND cs.date_begin BETWEEN @date_start AND @date_end
AND cs.is_oms IN (1, 5));

/*create temporary table if not exists doctor_protocols (select ds.id_case, ds.date_begin, ds.age_1, ds.age_2,
                                                              locate('18730', p.child_ref_ids) > 0 as p18730,
                                                              locate('80380', p.child_ref_ids) > 0 as p80380,
                                                              locate('17239', p.child_ref_ids) > 0 as p17239,
                                                              locate('17251', p.child_ref_ids) > 0 as p17251,
                                                              locate('18720', p.child_ref_ids) > 0 as p18720,
                                                              locate('19010', p.child_ref_ids) > 0 as p19010,
                                                              locate('44170', p.child_ref_ids) > 0 as p44170,
                                                              locate('47780', p.child_ref_ids) > 0 as p47780,
                                                              locate('79760', p.child_ref_ids) > 0 as p79760,
                                                              locate('80370', p.child_ref_ids) > 0 as p80370,
                                                              locate('17591', p.child_ref_ids) > 0 as p17591,
                                                              locate('17691', p.child_ref_ids) > 0 as p17691,
                                                              locate('55791', p.child_ref_ids) > 0 as p55791,
                                                              locate(';5761', p.child_ref_ids) > 0 as p5761,
                                                              locate(';4781', p.child_ref_ids) > 0 as p4781,
                                                              locate(';3790', p.child_ref_ids) > 0 as p3790,
                                                              locate(';2350', p.child_ref_ids) > 0 as p2350,
                                                              locate(';1392', p.child_ref_ids) > 0 as p1392,
                                                              locate(';480', p.child_ref_ids) > 0 as p480,
                                                              locate('18690', p.child_ref_ids) > 0 as p18690,
                                                              locate('19000', p.child_ref_ids) > 0 as p19000,
                                                              locate('44160', p.child_ref_ids) > 0 as p44160,
                                                              locate('79750', p.child_ref_ids) > 0 as p79750,
                                                              locate('80340', p.child_ref_ids) > 0 as p80340,
                                                              locate('4790', p.child_ref_ids) > 0 as p4790,
                                                              locate('5770', p.child_ref_ids) > 0 as p5770
                                                       from (select id_case, date_begin, age_1, age_2
                                                             from doctor_services
                                                             group by id_case) ds
                                                                left join protocols p on p.id_case = ds.id_case
                                                       where p.id_doctor = @doc_id);*/
/*2019-10-15 shamshurina - в предыдущей версии doctor_protocols ошибки из-за изменений в дереве протоколов, у первых веток лучше использовать id_ref*/
CREATE TEMPORARY TABLE IF NOT EXISTS doctor_protocols (SELECT ds.id_case, ds.date_begin, ds.age_1, ds.age_2, LOCATE('18730', p.id_ref) = 1 AS p18730, 
LOCATE('80380', p.id_ref) = 1 AS p80380, LOCATE('17239', p.id_ref) = 1 AS p17239, LOCATE('17251', p.id_ref) = 1 AS p17251, 
LOCATE('18720', p.id_ref) = 1 AS p18720, LOCATE('19010', p.id_ref) = 1 AS p19010, LOCATE('44170', p.id_ref) = 1 AS p44170, 
LOCATE('47780', p.id_ref) = 1 AS p47780, LOCATE('79760', p.id_ref) = 1 AS p79760, LOCATE('80370', p.id_ref) = 1 AS p80370, 
LOCATE('17591', p.id_ref) = 1 AS p17591, LOCATE('17691', p.id_ref) = 1 AS p17691, LOCATE('55791', p.id_ref) = 1 AS p55791, 
LOCATE('5761', p.id_ref) = 1 AS p5761, LOCATE('4781', p.id_ref) = 1 AS p4781, LOCATE('3790', p.id_ref) = 1 AS p3790, 
LOCATE('2350', p.id_ref) = 1 AS p2350, LOCATE('1392', p.id_ref) = 1 AS p1392, LOCATE('480', p.id_ref) = 1 AS p480, 
LOCATE('18690', p.id_ref) = 1 AS p18690, LOCATE('19000', p.id_ref) = 1 AS p19000, LOCATE('44160', p.id_ref) = 1 AS p44160, 
LOCATE('79750', p.id_ref) = 1 AS p79750, LOCATE('80340', p.id_ref) = 1 AS p80340, LOCATE('4790', p.id_ref) = 1 AS p4790, 
LOCATE('5770', p.id_ref) = 1 AS p5770, locate('3791', p.id_ref) = 1 as p3791, locate('4782', p.id_ref) = 1 as p4782, locate('5762', p.id_ref) = 1 as p5762
FROM (SELECT id_case, date_begin, age_1, age_2
FROM doctor_services
GROUP BY id_case) ds
LEFT JOIN protocols p ON
p.id_case = ds.id_case );

SET
@row = 0;

CREATE TEMPORARY TABLE IF NOT EXISTS dates (SELECT @row := @row + 1 AS st_num, T.date_begin
FROM (SELECT date_begin
FROM doctor_services
GROUP BY date_begin
ORDER BY date_begin) T);

CREATE TEMPORARY TABLE IF NOT EXISTS T1 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T2 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE age_1
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T3 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE age_2
GROUP BY id_case) T
GROUP BY date_begin);

/*с проф целью*/
/*всего*/
CREATE TEMPORARY TABLE IF NOT EXISTS T57 ( SELECT date_begin, COUNT(id_case) AS summ
FROM ( SELECT date_begin, id_case
FROM doctor_services
WHERE diagnosis_code IN ('Z01.2', 'K02.3')
GROUP BY id_case) T
GROUP BY date_begin);

/*от 0 до 12 мес*/
CREATE TEMPORARY TABLE IF NOT EXISTS T58 ( SELECT date_begin, COUNT(id_case) AS summ
FROM ( SELECT date_begin, id_case
FROM doctor_services
WHERE diagnosis_code IN ('Z01.2')
AND age_0
GROUP BY id_case) T
GROUP BY date_begin);

/*от 1 до 14*/
CREATE TEMPORARY TABLE IF NOT EXISTS T59 ( SELECT date_begin, COUNT(id_case) AS summ
FROM ( SELECT date_begin, id_case
FROM doctor_services
WHERE diagnosis_code IN ('Z01.2', 'K02.3')
AND age_1
GROUP BY id_case) T
GROUP BY date_begin);

/*от 15 до 17*/
CREATE TEMPORARY TABLE IF NOT EXISTS T60 ( SELECT date_begin, COUNT(id_case) AS summ
FROM ( SELECT date_begin, id_case
FROM doctor_services
WHERE diagnosis_code IN ('Z01.2', 'K02.3')
AND age_2
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T4 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт001')
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T5 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт001')
AND age_1
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T6 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт001')
AND age_2
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T7 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт025', 'стт028', 'стт031')
AND diagnosis_code IN ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5', 'k02.6', 'k02.7', 'k02.8', 'k02.9', 'k03.2', 'k04.0', 'k04.1', 'k04.2', 'k04.3', 'k04.4', 'k04.5')
GROUP BY id_case, tooth_name) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T8 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт025', 'стт028', 'стт031')
AND diagnosis_code IN ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5', 'k02.6', 'k02.7', 'k02.8', 'k02.9', 'k03.2', 'k04.0', 'k04.1', 'k04.2', 'k04.3', 'k04.4', 'k04.5')
AND age_1
GROUP BY id_case, tooth_name) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T9 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт025', 'стт028', 'стт031')
AND diagnosis_code IN ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5', 'k02.6', 'k02.7', 'k02.8', 'k02.9', 'k03.2', 'k04.0', 'k04.1', 'k04.2', 'k04.3', 'k04.4', 'k04.5')
AND age_2
GROUP BY id_case, tooth_name) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T10 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт025', 'стт028', 'стт031', 'нстт025', 'нстт028', 'нстт031')
AND diagnosis_code IN ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5', 'k02.6', 'k02.7', 'k02.8', 'k02.9', 'k03.2')
AND NOT milk_tooth
GROUP BY id_case, tooth_name) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T11 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт025', 'стт028', 'стт031', 'нстт025', 'нстт028', 'нстт031')
AND diagnosis_code IN ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5', 'k02.6', 'k02.7', 'k02.8', 'k02.9', 'k03.2')
AND NOT milk_tooth
AND age_1
GROUP BY id_case, tooth_name) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T12 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт025', 'стт028', 'стт031', 'нстт025', 'нстт028', 'нстт031')
AND diagnosis_code IN ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5', 'k02.6', 'k02.7', 'k02.8', 'k02.9', 'k03.2')
AND NOT milk_tooth
AND age_2
GROUP BY id_case, tooth_name) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T13 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт025', 'стт028', 'стт031', 'нстт025', 'нстт028', 'нстт031')
AND diagnosis_code IN ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5', 'k02.6', 'k02.7', 'k02.8', 'k02.9', 'k03.2')
AND milk_tooth
GROUP BY id_case, tooth_name) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T14 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт025', 'стт028', 'стт031', 'нстт025', 'нстт028', 'нстт031')
AND diagnosis_code IN ('k04.0', 'k04.1', 'k04.2', 'k04.3', 'k04.4', 'k04.5')
AND NOT milk_tooth
GROUP BY id_case, tooth_name) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T15 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт025', 'стт028', 'стт031', 'нстт025', 'нстт028', 'нстт031')
AND diagnosis_code IN ('k04.0', 'k04.1', 'k04.2', 'k04.3', 'k04.4', 'k04.5')
AND NOT milk_tooth
AND age_1
GROUP BY id_case, tooth_name) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T16 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт025', 'стт028', 'стт031', 'нстт025', 'нстт028', 'нстт031')
AND diagnosis_code IN ('k04.0', 'k04.1', 'k04.2', 'k04.3', 'k04.4', 'k04.5')
AND NOT milk_tooth
AND age_2
GROUP BY id_case, tooth_name) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T17 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт025', 'стт028', 'стт031', 'нстт025', 'нстт028', 'нстт031')
AND diagnosis_code IN ('k04.0', 'k04.1', 'k04.2', 'k04.3', 'k04.4', 'k04.5')
AND milk_tooth
GROUP BY id_case, tooth_name) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T18 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт026', 'стт027', 'стт029', 'стт030', 'стт032', 'стт033', 'стт057', 'стт058', 'нстт034')) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T19 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт026', 'стт027', 'стт029', 'стт030', 'стт032', 'стт033', 'стт057', 'стт058', 'нстт034')
AND milk_tooth) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T20 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт041')
AND child
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T21 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт041')
AND age_1
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T22 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт041')
AND age_2
GROUP BY id_case) T
GROUP BY date_begin);

/* 21.02.19 Sysoev, redefined T23 create temporary table if not exists T23  select date_begin, count(id_case) as summ from
   (select date_begin, id_case from doctor_services where profile_infis_code in ('стт041') and child group by id_case, tooth_name) T group by date_begin;  */
CREATE TEMPORARY TABLE IF NOT EXISTS T23 (SELECT p.date_protocol AS date_begin, SUM(CHAR_LENGTH(p.teeth_names) DIV 4 + 1) AS summ
FROM protocols p
WHERE p.id_case IN (SELECT DISTINCT id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт041'))
AND (LOCATE('3625(', p.child_ref_ids) > 0
OR LOCATE('4464(', p.child_ref_ids) > 0
OR LOCATE('5443(', p.child_ref_ids) > 0
OR LOCATE('19053(', p.child_ref_ids) > 0
OR LOCATE('89270(', p.child_ref_ids) > 0)
GROUP BY p.date_protocol);

CREATE TEMPORARY TABLE IF NOT EXISTS T24 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт048')
AND child
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T25 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт048')
AND age_1
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T26 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт048')
AND age_2
GROUP BY id_case) T
GROUP BY date_begin);

/* 21.02.19 Sysoev, redefined T27  create temporary table if not exists T27  select date_begin, count(id_case) as summ from
   (select date_begin, id_case from doctor_services where profile_infis_code in ('стт048') and child group by id_case, tooth_name) T group by date_begin;  */
CREATE TEMPORARY TABLE IF NOT EXISTS T27 (SELECT p.date_protocol AS date_begin, SUM(CHAR_LENGTH(p.teeth_names) DIV 4 + 1) AS summ
FROM protocols p
WHERE p.id_case IN (SELECT DISTINCT id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт048'))
AND (LOCATE('3623(', p.child_ref_ids) > 0
OR LOCATE('19054(', p.child_ref_ids) > 0
OR LOCATE('79991(', p.child_ref_ids) > 0)
GROUP BY p.date_protocol);

CREATE TEMPORARY TABLE IF NOT EXISTS T28 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт020', 'нстт020')
AND child
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T29 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт020', 'нстт020')
AND age_1
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T30 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт020', 'нстт020')
AND age_2
GROUP BY id_case) T
GROUP BY date_begin);

/* 21.02.19 Sysoev, redefined T31     create temporary table if not exists T31  select date_begin, count(id_case) as summ from
   (select date_begin, id_case from doctor_services where profile_infis_code in ('стт020') and child group by id_case, tooth_name) T group by date_begin;  */
CREATE TEMPORARY TABLE IF NOT EXISTS T31 (SELECT p.date_protocol AS date_begin, SUM(CHAR_LENGTH(p.teeth_names) DIV 4 + 1) AS summ
FROM protocols p
WHERE p.id_case IN (SELECT DISTINCT id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт020', 'нстт020'))
AND (LOCATE('39560(', p.child_ref_ids) > 0
OR LOCATE('39570(', p.child_ref_ids) > 0
OR LOCATE('48310(', p.child_ref_ids) > 0
OR LOCATE('48320(', p.child_ref_ids) > 0
OR LOCATE('48330(', p.child_ref_ids) > 0
OR LOCATE('48340(', p.child_ref_ids) > 0
OR LOCATE('48350(', p.child_ref_ids) > 0
OR LOCATE('52510(', p.child_ref_ids) > 0
OR LOCATE('80021(', p.child_ref_ids) > 0)
GROUP BY p.date_protocol);

CREATE TEMPORARY TABLE IF NOT EXISTS T32 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт049')
AND child
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T33 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт049')
AND age_1
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T34 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт049')
AND age_2
GROUP BY id_case) T
GROUP BY date_begin);

/* 21.02.19 Sysoev, redefined T35    create temporary table if not exists T35  select date_begin, count(id_case) as summ from
   (select date_begin, id_case from doctor_services where profile_infis_code in ('стт049') and child group by id_case, tooth_name) T group by date_begin;  */
CREATE TEMPORARY TABLE IF NOT EXISTS T35 (SELECT p.date_protocol AS date_begin, SUM(CHAR_LENGTH(p.teeth_names) DIV 4 + 1) AS summ
FROM protocols p
WHERE p.id_case IN (SELECT DISTINCT id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт049'))
AND (LOCATE('39540(', p.child_ref_ids) > 0
OR LOCATE('48250(', p.child_ref_ids) > 0
OR LOCATE('48260(', p.child_ref_ids) > 0
OR LOCATE('48270(', p.child_ref_ids) > 0
OR LOCATE('91820(', p.child_ref_ids) > 0)
GROUP BY p.date_protocol);

CREATE TEMPORARY TABLE IF NOT EXISTS T36 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт021')
AND age_1
GROUP BY id_case, tooth_name) T
GROUP BY date_begin);

/* 21.02.19 Sysoev, redefined T37    create temporary table if not exists T37  select date_begin, count(id_case) as summ from
   (select date_begin, id_case from doctor_services where profile_infis_code in ('стт021') and child group by id_case, tooth_name) T group by date_begin;  */
CREATE TEMPORARY TABLE IF NOT EXISTS T37 (SELECT p.date_protocol AS date_begin, SUM(CHAR_LENGTH(p.teeth_names) DIV 4 + 1) AS summ
FROM protocols p
WHERE p.id_case IN (SELECT DISTINCT id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт021'))
AND (LOCATE('57710(', p.child_ref_ids) > 0
OR LOCATE('57730(', p.child_ref_ids) > 0
OR LOCATE('57740(', p.child_ref_ids) > 0)
GROUP BY p.date_protocol);

CREATE TEMPORARY TABLE IF NOT EXISTS T38 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт022', 'нстт022')
AND child
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T39 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт022', 'нстт022')
AND age_1
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T40 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт022', 'нстт022')
AND age_2
GROUP BY id_case) T
GROUP BY date_begin);

/* 21.02.19 Sysoev, redefined T41    create temporary table if not exists T41  select date_begin, count(id_case) as summ from
   (select date_begin, id_case from doctor_services where profile_infis_code in ('стт022') and child group by id_case, tooth_name) T group by date_begin;  */
CREATE TEMPORARY TABLE IF NOT EXISTS T41 (SELECT p.date_protocol AS date_begin, SUM(CHAR_LENGTH(p.teeth_names) DIV 4 + 1) AS summ
FROM protocols p
WHERE p.id_case IN (SELECT DISTINCT id_case
FROM doctor_services
WHERE profile_infis_code IN ('стт022', 'нстт022'))
AND (LOCATE('3723(', p.child_ref_ids) > 0
OR LOCATE('19202(', p.child_ref_ids) > 0
OR LOCATE('80032(', p.child_ref_ids) > 0)
GROUP BY p.date_protocol);

CREATE TEMPORARY TABLE IF NOT EXISTS T42 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('сто022')
AND child
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T43 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('сто022')
AND age_1
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T44 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_services
WHERE profile_infis_code IN ('сто022')
AND age_2
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T45 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_protocols
WHERE (p18730
OR p80380)
GROUP BY id_case) T
GROUP BY date_begin) ;

CREATE TEMPORARY TABLE IF NOT EXISTS T46 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_protocols
WHERE (p18730
OR p80380)
AND age_1
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T47 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_protocols
WHERE (p18730
OR p80380)
AND age_2
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T48 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_protocols
WHERE (p17239
OR p17251
OR p18720
OR p19010
OR p44170
OR p47780
OR p79760
OR p80370)
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T49 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_protocols
WHERE (p17239
OR p17251
OR p18720
OR p19010
OR p44170
OR p47780
OR p79760
OR p80370)
AND age_1
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T50 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_protocols
WHERE (p17239
OR p17251
OR p18720
OR p19010
OR p44170
OR p47780
OR p79760
OR p80370)
AND age_2
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T51 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_protocols
WHERE (p18690
OR p19000
OR p44160
OR p79750
OR p80340)
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T52 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_protocols
WHERE (p18690
OR p19000
OR p44160
OR p79750
OR p80340)
AND age_1
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T53 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_protocols
WHERE (p18690
OR p19000
OR p44160
OR p79750
OR p80340)
AND age_2
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T54 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_protocols
WHERE (p4790
OR p5761
OR p4781
OR p3790
OR p5770)
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T55 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_protocols
WHERE (p4790
OR p5761
OR p4781
OR p3790
OR p5770)
AND age_1
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T56 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_protocols
WHERE (p4790
OR p5761
OR p4781
OR p3790
OR p5770)
AND age_2
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T54_5 (SELECT date_begin, COUNT(id_case) AS summ
FROM (SELECT date_begin, id_case
FROM doctor_protocols
where (p5762 or p4782 or p3791)
GROUP BY id_case) T
GROUP BY date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS T_UET (SELECT date_begin, ROUND(COALESCE(SUM(UET), 0), 2) AS summ
FROM doctor_services
GROUP BY date_begin);

create temporary table t_cf (SELECT date_begin, count(distinct id_case) as summ
                             from doctor_services
							 where cf = 1
                             group by date_begin
                             order by date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS tmp_1 (SELECT dates.st_num, dates.date_begin, COALESCE(T1.summ, 0) AS s1, COALESCE(T2.summ, 0) AS s2, COALESCE(T3.summ, 0) AS s3, COALESCE(t57.summ, 0) AS s57, COALESCE(t58.summ, 0) AS s58, COALESCE(t59.summ, 0) AS s59, COALESCE(t60.summ, 0) AS s60, COALESCE(T4.summ, 0) AS s4, COALESCE(T5.summ, 0) AS s5, COALESCE(T6.summ, 0) AS s6, Coalesce(t_cf.summ, 0) as cf, COALESCE(T7.summ, 0) AS s7, COALESCE(T8.summ, 0) AS s8, COALESCE(T9.summ, 0) AS s9, COALESCE(T10.summ, 0) AS s10, COALESCE(T11.summ, 0) AS s11, COALESCE(T12.summ, 0) AS s12, COALESCE(T13.summ, 0) AS s13, COALESCE(T14.summ, 0) AS s14, COALESCE(T15.summ, 0) AS s15, COALESCE(T16.summ, 0) AS s16, COALESCE(T17.summ, 0) AS s17, COALESCE(T18.summ, 0) AS s18, COALESCE(T19.summ, 0) AS s19, COALESCE(T20.summ, 0) AS s20
FROM dates
LEFT JOIN T1 ON
T1.date_begin = dates.date_begin
LEFT JOIN T2 ON
T2.date_begin = dates.date_begin
LEFT JOIN T3 ON
T3.date_begin = dates.date_begin
LEFT JOIN T4 ON
T4.date_begin = dates.date_begin
LEFT JOIN T5 ON
T5.date_begin = dates.date_begin
LEFT JOIN T6 ON
T6.date_begin = dates.date_begin
LEFT JOIN T7 ON
T7.date_begin = dates.date_begin
LEFT JOIN T8 ON
T8.date_begin = dates.date_begin
LEFT JOIN T9 ON
T9.date_begin = dates.date_begin
LEFT JOIN T10 ON
T10.date_begin = dates.date_begin
LEFT JOIN T11 ON
T11.date_begin = dates.date_begin
LEFT JOIN T12 ON
T12.date_begin = dates.date_begin
LEFT JOIN T13 ON
T13.date_begin = dates.date_begin
LEFT JOIN T14 ON
T14.date_begin = dates.date_begin
LEFT JOIN T15 ON
T15.date_begin = dates.date_begin
LEFT JOIN T16 ON
T16.date_begin = dates.date_begin
LEFT JOIN T17 ON
T17.date_begin = dates.date_begin
LEFT JOIN T18 ON
T18.date_begin = dates.date_begin
LEFT JOIN T19 ON
T19.date_begin = dates.date_begin
LEFT JOIN T20 ON
T20.date_begin = dates.date_begin
LEFT JOIN t57 ON
t57.date_begin = dates.date_begin
LEFT JOIN t58 ON
t58.date_begin = dates.date_begin
LEFT JOIN t59 ON
t59.date_begin = dates.date_begin
LEFT JOIN t60 ON
t60.date_begin = dates.date_begin
LEFT JOIN t_cf on
t_cf.date_begin = dates.date_begin);

CREATE TEMPORARY TABLE IF NOT EXISTS tmp_2 (SELECT tmp_1.*, COALESCE(T21.summ, 0) AS s21, COALESCE(T22.summ, 0) AS s22, COALESCE(T23.summ, 0) AS s23, COALESCE(T24.summ, 0) AS s24, COALESCE(T25.summ, 0) AS s25, COALESCE(T26.summ, 0), COALESCE(T27.summ, 0), COALESCE(T28.summ, 0), COALESCE(T29.summ, 0), COALESCE(T30.summ, 0), COALESCE(T31.summ, 0), COALESCE(T32.summ, 0), COALESCE(T33.summ, 0), COALESCE(T34.summ, 0), COALESCE(T35.summ, 0), COALESCE(T36.summ, 0), COALESCE(T37.summ, 0), COALESCE(T38.summ, 0), COALESCE(T39.summ, 0), COALESCE(T40.summ, 0)
FROM tmp_1
LEFT JOIN T21 ON
T21.date_begin = tmp_1.date_begin
LEFT JOIN T22 ON
T22.date_begin = tmp_1.date_begin
LEFT JOIN T23 ON
T23.date_begin = tmp_1.date_begin
LEFT JOIN T24 ON
T24.date_begin = tmp_1.date_begin
LEFT JOIN T25 ON
T25.date_begin = tmp_1.date_begin
LEFT JOIN T26 ON
T26.date_begin = tmp_1.date_begin
LEFT JOIN T27 ON
T27.date_begin = tmp_1.date_begin
LEFT JOIN T28 ON
T28.date_begin = tmp_1.date_begin
LEFT JOIN T29 ON
T29.date_begin = tmp_1.date_begin
LEFT JOIN T30 ON
T30.date_begin = tmp_1.date_begin
LEFT JOIN T31 ON
T31.date_begin = tmp_1.date_begin
LEFT JOIN T32 ON
T32.date_begin = tmp_1.date_begin
LEFT JOIN T33 ON
T33.date_begin = tmp_1.date_begin
LEFT JOIN T34 ON
T34.date_begin = tmp_1.date_begin
LEFT JOIN T35 ON
T35.date_begin = tmp_1.date_begin
LEFT JOIN T36 ON
T36.date_begin = tmp_1.date_begin
LEFT JOIN T37 ON
T37.date_begin = tmp_1.date_begin
LEFT JOIN T38 ON
T38.date_begin = tmp_1.date_begin
LEFT JOIN T39 ON
T39.date_begin = tmp_1.date_begin
LEFT JOIN T40 ON
T40.date_begin = tmp_1.date_begin) ;

SELECT tmp_2.*, COALESCE(T41.summ, 0), COALESCE(T42.summ, 0), COALESCE(T43.summ, 0), COALESCE(T44.summ, 0), COALESCE(T45.summ, 0), COALESCE(T46.summ, 0), COALESCE(T47.summ, 0), COALESCE(T48.summ, 0), COALESCE(T49.summ, 0), COALESCE(T50.summ, 0), COALESCE(T51.summ, 0), COALESCE(T52.summ, 0), COALESCE(T53.summ, 0), COALESCE(T54.summ, 0), COALESCE(T54_5.summ, 0), COALESCE(T55.summ, 0), COALESCE(T56.summ, 0), COALESCE(T_UET.summ, 0)
FROM tmp_2
LEFT JOIN T41 ON
T41.date_begin = tmp_2.date_begin
LEFT JOIN T42 ON
T42.date_begin = tmp_2.date_begin
LEFT JOIN T43 ON
T43.date_begin = tmp_2.date_begin
LEFT JOIN T44 ON
T44.date_begin = tmp_2.date_begin
LEFT JOIN T45 ON
T45.date_begin = tmp_2.date_begin
LEFT JOIN T46 ON
T46.date_begin = tmp_2.date_begin
LEFT JOIN T47 ON
T47.date_begin = tmp_2.date_begin
LEFT JOIN T48 ON
T48.date_begin = tmp_2.date_begin
LEFT JOIN T49 ON
T49.date_begin = tmp_2.date_begin
LEFT JOIN T50 ON
T50.date_begin = tmp_2.date_begin
LEFT JOIN T51 ON
T51.date_begin = tmp_2.date_begin
LEFT JOIN T52 ON
T52.date_begin = tmp_2.date_begin
LEFT JOIN T53 ON
T53.date_begin = tmp_2.date_begin
LEFT JOIN T54 ON
T54.date_begin = tmp_2.date_begin
LEFT JOIN T54_5 ON
T54_5.date_begin = tmp_2.date_begin
LEFT JOIN T55 ON
T55.date_begin = tmp_2.date_begin
LEFT JOIN T56 ON
T56.date_begin = tmp_2.date_begin
LEFT JOIN T_UET ON
T_UET.date_begin = tmp_2.date_begin
ORDER BY tmp_2.date_begin;