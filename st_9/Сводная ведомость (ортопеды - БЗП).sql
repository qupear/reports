create temporary table if not exists bzp_107 (select ord.orderCompletionDate as date_begin, pos.code, pos.uet
                                              from case_services cs
                                                       left join orders ord on ord.id = cs.id_order
                                                       left join price_orto_soc pos on pos.id = cs.id_profile
                                              where cs.id_doctor = @doc_id
                                                and cs.id_order > 0
                                                and ord.orderCompletionDate between @date_start and @date_end
                                                and ord.order_type = 1
                                                and ord.status = 2
                                              order by ord.orderCompletionDate);
create temporary table if not exists bzp_107_dates (select distinct date_begin
                                                    from bzp_107
                                                    order by date_begin);
create temporary table if not exists bzp_11(select bd.date_begin, coalesce(count(code), 0) as cnt
                                            from bzp_107_dates bd
                                                     left join bzp_107 b on b.date_begin = bd.date_begin and code = '1.1'
                                            group by bd.date_begin);
create temporary table if not exists bzp_12(select bd.date_begin, coalesce(count(code), 0) as cnt
                                            from bzp_107_dates bd
                                                     left join bzp_107 b on b.date_begin = bd.date_begin and code = '1.2'
                                            group by bd.date_begin);
create temporary table if not exists bzp_138(select bd.date_begin, coalesce(count(code), 0) as cnt
                                             from bzp_107_dates bd
                                                      left join bzp_107 b on b.date_begin = bd.date_begin and code = '13.8'
                                             group by bd.date_begin);
create temporary table if not exists bzp_132(select bd.date_begin, coalesce(count(code), 0) as cnt
                                             from bzp_107_dates bd
                                                      left join bzp_107 b on b.date_begin = bd.date_begin and code = '13.2'
                                             group by bd.date_begin);
create temporary table if not exists bzp_group_1 (select bd.date_begin, coalesce(count(code), 0) as cnt
                                                  from bzp_107_dates bd
                                                           left join bzp_107 b on b.date_begin = bd.date_begin and
                                                                                  code in ('3.4', '3.5', '3.7', '5.3', '5.4')
                                                  group by bd.date_begin);
create temporary table if not exists bzp_36 (select bd.date_begin, coalesce(count(code), 0) as cnt
                                             from bzp_107_dates bd
                                                      left join bzp_107 b on b.date_begin = bd.date_begin and code = '3.6'
                                             group by bd.date_begin);
create temporary table if not exists bzp_group_2 (select bd.date_begin, coalesce(count(code), 0) as cnt
                                                  from bzp_107_dates bd
                                                           left join bzp_107 b on b.date_begin = bd.date_begin and
                                                                                  code in ('3.1', '3.2', '3.17', '5.1', '5.2')
                                                  group by bd.date_begin);
create temporary table if not exists bzp_122 (select bd.date_begin, coalesce(count(code), 0) as cnt
                                              from bzp_107_dates bd
                                                       left join bzp_107 b on b.date_begin = bd.date_begin and code = '12.2'
                                              group by bd.date_begin);
create temporary table if not exists bzp_413 (select bd.date_begin, coalesce(count(code), 0) as cnt
                                              from bzp_107_dates bd
                                                       left join bzp_107 b on b.date_begin = bd.date_begin and code = '4.13'
                                              group by bd.date_begin);
create temporary table if not exists bzp_153 (select bd.date_begin, coalesce(count(code), 0) as cnt
                                              from bzp_107_dates bd
                                                       left join bzp_107 b on b.date_begin = bd.date_begin and code = '15.3'
                                              group by bd.date_begin);
create temporary table if not exists bzp_157 (select bd.date_begin, coalesce(count(code), 0) as cnt
                                              from bzp_107_dates bd
                                                       left join bzp_107 b on b.date_begin = bd.date_begin and code = '15.7'
                                              group by bd.date_begin);
create temporary table if not exists bzp_156 (select bd.date_begin, coalesce(count(code), 0) as cnt
                                              from bzp_107_dates bd
                                                       left join bzp_107 b on b.date_begin = bd.date_begin and code = '15.6'
                                              group by bd.date_begin);
create temporary table if not exists bzp_155 (select bd.date_begin, coalesce(count(code), 0) as cnt
                                              from bzp_107_dates bd
                                                       left join bzp_107 b on b.date_begin = bd.date_begin and code = '15.5'
                                              group by bd.date_begin);
create temporary table if not exists bzp_uet (select bd.date_begin, coalesce(sum(uet), 0) as cnt
                                              from bzp_107_dates bd
                                                       left join bzp_107 b on b.date_begin = bd.date_begin
                                              group by bd.date_begin);

SELECT dt.date_begin, b11.cnt, b12.cnt, b138.cnt, b132.cnt, bg1.cnt, b36.cnt, bg2.cnt, b122.cnt, b413.cnt, b153.cnt,
       b157.cnt, b156.cnt, b155.cnt, bu.cnt
from bzp_107_dates dt
         left join bzp_11 b11 on b11.date_begin = dt.date_begin
         left join bzp_12 b12 on b12.date_begin = dt.date_begin
         left join bzp_138 b138 on b138.date_begin = dt.date_begin
         left join bzp_132 b132 on b132.date_begin = dt.date_begin
         left join bzp_group_1 bg1 on bg1.date_begin = dt.date_begin
         left join bzp_36 b36 on b36.date_begin = dt.date_begin
         left join bzp_group_2 bg2 on bg2.date_begin = dt.date_begin
         left join bzp_122 b122 on b122.date_begin = dt.date_begin
         left join bzp_413 b413 on b413.date_begin = dt.date_begin
         left join bzp_153 b153 on b153.date_begin = dt.date_begin
         left join bzp_157 b157 on b157.date_begin = dt.date_begin
         left join bzp_156 b156 on b156.date_begin = dt.date_begin
         left join bzp_155 b155 on b155.date_begin = dt.date_begin
         left join bzp_uet bu on bu.date_begin = dt.date_begin
order by dt.date_begin;
