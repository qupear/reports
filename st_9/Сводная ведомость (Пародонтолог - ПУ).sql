create temporary table if not exists doctor_services (select cs.date_begin, cs.id_case, p.code, vd.diagnosis_code,
                                                             cs.tooth_name, p.uet, cs.is_oms, pd.COAG_FLAG
                                                      from case_services cs
                                                               left join cases c on cs.id_case = c.id_case
                                                               left join patient_data pd on c.ID_PATIENT = pd.id_patient
                                                               left join price p on p.id = cs.id_profile and cs.is_oms in (0, 3, 4)
                                                               left join vmu_diagnosis vd on cs.id_diagnosis = vd.id_diagnosis
                                                               left join vmu_profile vp on vp.id_profile = cs.id_profile and cs.is_oms = 1
                                                               left join (select vt.uet, vt.id_profile, vt.id_zone_type,
                                                                                 vt.tariff_begin_date as d1,
                                                                                 vt.tariff_end_date as d2,
                                                                                 vtp.tp_begin_date as d3,
                                                                                 vtp.tp_end_date as d4
                                                                          from vmu_tariff vt,
                                                                               vmu_tariff_plan vtp
                                                                          where vtp.id_tariff_group = vt.id_lpu
                                                                            And vtp.id_lpu in (select id_lpu from mu_ident)) T
                                                                         on T.id_profile = vp.id_profile and
                                                                            cs.date_begin between T.d1 and T.d2 and
                                                                            cs.date_begin between T.d3 and T.d4 and
                                                                            T.id_zone_type = cs.id_net_profile and
                                                                            cs.is_oms = 1
                                                      where cs.id_doctor = @doc_id
                                                        and cs.date_begin between @date_start and @date_end
                                                        and cs.is_oms in (0, 3, 4));   set @row = 0;
create temporary table if not exists first_row (select @row := @row + 1 as st_num, T.date_begin as st_date
                                                from (select date_begin
                                                      from doctor_services
                                                      group by date_begin
                                                      order by date_begin) T);
create temporary table if not exists T1 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T2 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where code in ('2001', '2003', '2070', '2072')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T3 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where diagnosis_code in ('k05.0', 'k05.1')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T4 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where diagnosis_code in ('k05.2', 'k05.3')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T5 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where diagnosis_code in ('k05.4')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T6 (select date_begin, count(distinct id_case) as val
                                         from doctor_services
                                         where diagnosis_code in
                                               ('k12.0', 'k12.1', 'k12.2', 'k13', 'k13.0', 'k13.1', 'k13.2', 'k13.3',
                                                'k13.4', 'k13.5', 'k13.6', 'k13.7', 'k14', 'k14.0', 'k14.1', 'k14.2',
                                                'k14.3', 'k14.4', 'k14.5', 'k14.6', 'k14.7', 'k14.8', 'k14.9')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T7 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where code in ('2006', '2010', '2057')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T8 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where code in ('2008', '2012', '2016')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T9 (select date_begin, count(id_case) as val
                                         from doctor_services
                                         where code in ('2018')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T10 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('2028', '2029', '2031')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T11 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('2007')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T12 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('2052', '2053', '2054', '2067')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T13 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('2051')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T14 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('2065', '2069')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T15 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('2055', '2056', '2059')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T16 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('2032', '2033', '2034', '2037')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T17 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('2036')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T18 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('2041')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T19 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('2044', '2045', '20451')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T20 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('2060')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T21 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('2046', '2047', '2048', '2049', '2050')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T22 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('2052', '2053', '2054', '2067')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T23 (select date_begin, count(id_case) as val
                                          from doctor_services
                                          where code in ('2058', '2062', '2063')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T24 (select date_begin, ROUND(COALESCE(SUM(UET), 0), 2) as val
                                          from doctor_services
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T25 (select date_begin, COAG_FLAG as cf
                                          from doctor_services
                                          group by date_begin
                                          order by date_begin);
select fr.st_num, fr.st_date, coalesce(T1.val, 0), coalesce(T2.val, 0), T25.cf, coalesce(T3.val, 0),
       coalesce(T4.val, 0), coalesce(T5.val, 0), coalesce(T6.val, 0), coalesce(T7.val, 0), coalesce(T8.val, 0),
       coalesce(T9.val, 0), coalesce(T10.val, 0), coalesce(T11.val, 0), coalesce(T12.val, 0), coalesce(T13.val, 0),
       coalesce(T14.val, 0), coalesce(T15.val, 0), coalesce(T16.val, 0), coalesce(T17.val, 0), coalesce(T8.val, 0),
       coalesce(T9.val, 0), coalesce(T20.val, 0), coalesce(T21.val, 0), coalesce(T22.val, 0), coalesce(T23.val, 0), 0,
       0, 0, coalesce(T24.val, 0)
from first_row fr
         left join T1 on T1.date_begin = fr.st_date
         left join T2 on T2.date_begin = fr.st_date
         left join T3 on T3.date_begin = fr.st_date
         left join T4 on T4.date_begin = fr.st_date
         left join T5 on T5.date_begin = fr.st_date
         left join T6 on T6.date_begin = fr.st_date
         left join T7 on T7.date_begin = fr.st_date
         left join T8 on T8.date_begin = fr.st_date
         left join T9 on T9.date_begin = fr.st_date
         left join T10 on T10.date_begin = fr.st_date
         left join T11 on T11.date_begin = fr.st_date
         left join T12 on T12.date_begin = fr.st_date
         left join T13 on T13.date_begin = fr.st_date
         left join T14 on T14.date_begin = fr.st_date
         left join T15 on T15.date_begin = fr.st_date
         left join T16 on T16.date_begin = fr.st_date
         left join T17 on T17.date_begin = fr.st_date
         left join T18 on T18.date_begin = fr.st_date
         left join T19 on T19.date_begin = fr.st_date
         left join T20 on T20.date_begin = fr.st_date
         left join T21 on T21.date_begin = fr.st_date
         left join T22 on T22.date_begin = fr.st_date
         left join T23 on T23.date_begin = fr.st_date
         left join T24 on T24.date_begin = fr.st_date
         left join T25 on T25.date_begin = fr.st_date
order by st_num ;





























