set @row = 0;
SELECT @row := @row + 1, t.*
	FROM
		(
			SELECT concat(pd.surname, ' ', pd.name, ' ', pd.second_name, ' ', pd.birthday, ' ',
			              pa.unstruct_address) 'pat row', '' c1, '' c2, '' c3, '' c4, '' c5, '' c6, '' c7, '' c8, '' c9,
			       '' c10, '' c11, '' c12, '' c13, '' c14, '' c15, group_concat(vp.profile_infis_code), u.name, '' c16
				FROM
					appointments                  ap
						JOIN      patient_data    pd ON ap.id_patient = pd.id_patient
						JOIN      patient_address pa ON pd.id_address_reg = pa.id_address
						JOIN      doctor_spec     ds ON ap.doctor_id = ds.doctor_id
						JOIN      users           u ON ds.user_id = u.id
						LEFT JOIN case_services   cs ON ap.case_id = cs.id_case AND cs.is_oms = 1
						LEFT JOIN vmu_profile     vp ON cs.id_profile = vp.id_profile AND cs.is_oms = 1
				WHERE ap.doctor_id = @doc_id AND ap.case_id > 0 AND date(ap.time) = @date_start
				GROUP BY pd.surname, pd.name, pd.second_name, pd.birthday, pa.unstruct_address
				ORDER BY ap.id) t;
