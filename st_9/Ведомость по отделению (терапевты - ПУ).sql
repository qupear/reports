DROP TEMPORARY TABLE IF EXISTS doctor_services;
CREATE TEMPORARY TABLE IF NOT EXISTS doctor_services (SELECT cs.date_begin, cs.id_case, p.code, vd.diagnosis_code,
                                                             cs.tooth_name, p.uet, cs.is_oms, pd.coag_flag
                                                      FROM case_services cs
                                                               LEFT JOIN price p ON p.id = cs.id_profile AND cs.is_oms IN (0, 3, 4)
                                                               LEFT JOIN vmu_diagnosis vd ON cs.id_diagnosis = vd.id_diagnosis
                                                               LEFT JOIN doctor_spec ds ON cs.id_doctor = ds.doctor_id
															   Left join cases c on c.id_case = cs.id_case
															   LEFT JOIN patient_data pd on pd.id_patient = c.id_patient

                                                      WHERE (ds.department_id = @depart_id OR @depart_id = 0)
                                                        AND (ds.spec_id = 106)
                                                        AND cs.date_begin BETWEEN @date_start AND @date_end
                                                        AND cs.is_oms IN (0, 3, 4));  DROP TEMPORARY TABLE IF EXISTS doctor_protocols;
CREATE TEMPORARY TABLE IF NOT EXISTS doctor_protocols (SELECT ds.id_case, ds.date_begin,
                                                              LOCATE('17239', p.child_ref_ids) > 0 AS p17239,
                                                              LOCATE('17251', p.child_ref_ids) > 0 AS p17251,
                                                              LOCATE('18720', p.child_ref_ids) > 0 AS p18720,
                                                              LOCATE('19010', p.child_ref_ids) > 0 AS p19010,
                                                              LOCATE('44170', p.child_ref_ids) > 0 AS p44170,
                                                              LOCATE('47780', p.child_ref_ids) > 0 AS p47780,
                                                              LOCATE('79760', p.child_ref_ids) > 0 AS p79760,
                                                              LOCATE('80370', p.child_ref_ids) > 0 AS p80370,
                                                              LOCATE('18730', p.child_ref_ids) > 0 AS p18730,
                                                              LOCATE('80380', p.child_ref_ids) > 0 AS p80380,
                                                              LOCATE('55791', p.child_ref_ids) > 0 AS p55791,
                                                              LOCATE(';5761', p.child_ref_ids) > 0 AS p5761,
                                                              LOCATE(';4781', p.child_ref_ids) > 0 AS p4781,
                                                              LOCATE(';3790', p.child_ref_ids) > 0 AS p3790,
                                                              LOCATE(';2350', p.child_ref_ids) > 0 AS p2350,
                                                              LOCATE(';1392', p.child_ref_ids) > 0 AS p1392,
                                                              LOCATE(';480', p.child_ref_ids) > 0 AS p480
                                                       FROM doctor_services ds
                                                                LEFT JOIN protocols p ON p.id_case = ds.id_case);  SET @row = 0;  DROP TEMPORARY TABLE IF EXISTS first_row;
CREATE TEMPORARY TABLE IF NOT EXISTS first_row (SELECT @row := @row + 1 AS st_num, T.date_begin AS st_date
                                                FROM (SELECT date_begin
                                                      FROM doctor_services
                                                      GROUP BY date_begin
                                                      ORDER BY date_begin) T);  DROP TEMPORARY TABLE IF EXISTS T1;
CREATE TEMPORARY TABLE IF NOT EXISTS T1 (SELECT date_begin, COUNT(DISTINCT id_case) AS val
                                         FROM doctor_services
                                         GROUP BY date_begin
                                         ORDER BY date_begin);  DROP TEMPORARY TABLE IF EXISTS T2;
CREATE TEMPORARY TABLE IF NOT EXISTS T2 (SELECT date_begin, COUNT(DISTINCT id_case) AS val
                                         FROM doctor_services
                                         WHERE code IN ('1001', '1003', '1004')
                                         GROUP BY date_begin
                                         ORDER BY date_begin);  DROP TEMPORARY TABLE IF EXISTS T3;
CREATE TEMPORARY TABLE IF NOT EXISTS T3 (select date_begin, count(distinct id_case) as val
                                         from doctor_protocols
                                         where p17239
                                            or p17251
                                            or p18720
                                            or p19010
                                            or p44170
                                            or p47780
                                            or p79760
                                            or p80370
                                         group by date_begin
                                         order by date_begin);  DROP TEMPORARY TABLE IF EXISTS T4;
CREATE TEMPORARY TABLE IF NOT EXISTS T4 (SELECT date_begin, COUNT(DISTINCT id_case) AS val
                                         FROM doctor_protocols
                                         WHERE p18730
                                            OR p80380
                                         GROUP BY date_begin
                                         ORDER BY date_begin);  DROP TEMPORARY TABLE IF EXISTS T5;
CREATE TEMPORARY TABLE IF NOT EXISTS T5
(SELECT T.date_begin, COUNT(T.val) AS val
FROM (SELECT date_begin, COUNT(id_case) AS val
      FROM doctor_services
      WHERE code IN ('1014', '1016', '1017', '1027', '1028', '1108', '1109', '1110', '1111')
      GROUP BY id_case, tooth_name
      ORDER BY date_begin) T
GROUP BY date_begin);  DROP TEMPORARY TABLE IF EXISTS T6;
CREATE TEMPORARY TABLE IF NOT EXISTS T6
(SELECT T.date_begin, COUNT(T.val) AS val
FROM (SELECT date_begin, COUNT(id_case) AS val
      FROM doctor_services
      WHERE code IN ('1014', '1016', '1017', '1027', '1028', '1108', '1109', '1110', '1111')
        AND diagnosis_code IN
            ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5', 'k02.6', 'k02.7', 'k02.8', 'k02.9', 'k03.2')
      GROUP BY id_case, tooth_name
      ORDER BY date_begin) T
GROUP BY date_begin);  DROP TEMPORARY TABLE IF EXISTS T7;
CREATE TEMPORARY TABLE IF NOT EXISTS T7
(SELECT T.date_begin, COUNT(T.val) AS val
FROM (SELECT date_begin, COUNT(id_case) AS val
      FROM doctor_services
      WHERE code IN ('1014', '1016', '1017', '1027', '1028', '1108', '1109', '1110', '1111')
        AND diagnosis_code IN ('k04.0', 'k04.1', 'k04.2', 'k04.3')
      GROUP BY id_case, tooth_name
      ORDER BY date_begin) T
GROUP BY date_begin);  DROP TEMPORARY TABLE IF EXISTS T8;
CREATE TEMPORARY TABLE IF NOT EXISTS T8 (SELECT T.date_begin, COUNT(T.val) AS val
                                         FROM (SELECT date_begin, COUNT(id_case) AS val
                                               FROM doctor_services
                                               WHERE code IN
                                                     ('1014', '1016', '1017', '1027', '1028', '1108', '1109', '1110',
                                                      '1111')
                                                 AND diagnosis_code IN ('k04.4', 'k04.5')
                                               GROUP BY id_case, tooth_name
                                               ORDER BY date_begin) T
                                         GROUP BY date_begin);  DROP TEMPORARY TABLE IF EXISTS T9;
CREATE TEMPORARY TABLE IF NOT EXISTS T9 (SELECT T.date_begin, COUNT(T.val) AS val
                                         FROM (SELECT date_begin, COUNT(id_case) AS val
                                               FROM doctor_services
                                               WHERE code IN ('1060')
                                               GROUP BY id_case, tooth_name
                                               ORDER BY date_begin) T
                                         GROUP BY date_begin);  DROP TEMPORARY TABLE IF EXISTS T10;
CREATE TEMPORARY TABLE IF NOT EXISTS T10 (SELECT T.date_begin, COUNT(T.val) AS val
                                          FROM (SELECT date_begin, COUNT(id_case) AS val
                                                FROM doctor_services
                                                WHERE code IN
                                                      ('1027', '1028', '1029', '1031', '1033', '1034', '1039', '1040',
                                                       '1041', '1042', '1043', '1044', '1045', '1046', '1047', '1048',
                                                       '1049', '1050', '1052', '1054', '1058', '1059', '1062', '1073',
                                                       '1074', '1092', '1093', '1094', '1095', '1096', '1097', '1098',
                                                       '1099', '1100', '1101', '1102', '1103', '1104', '1105', '1106',
                                                       '1107', '1108', '1109', '1110', '1111', '1118', '1119')
                                                  AND diagnosis_code IN ('k04.0', 'k04.1', 'k04.2', 'k04.3', 'k04.4', 'k04.5')
                                                GROUP BY id_case, tooth_name
                                                ORDER BY date_begin) T
                                          GROUP BY date_begin);  DROP TEMPORARY TABLE IF EXISTS T11;
CREATE TEMPORARY TABLE IF NOT EXISTS T11 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN
                                                ('1027', '1028', '1029', '1031', '1033', '1034', '1039', '1040', '1041',
                                                 '1042', '1043', '1044', '1045', '1046', '1047', '1048', '1049', '1050',
                                                 '1052', '1054', '1058', '1059', '1062', '1073', '1074', '1092', '1093',
                                                 '1094', '1095', '1096', '1097', '1098', '1099', '1100', '1101', '1102',
                                                 '1103', '1104', '1105', '1106', '1107', '1108', '1109', '1110', '1111',
                                                 '1118', '1119')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);  DROP TEMPORARY TABLE IF EXISTS T13;
CREATE TEMPORARY TABLE IF NOT EXISTS T13 (SELECT date_begin, COUNT(DISTINCT id_case) AS val
                                          FROM doctor_protocols
                                          WHERE p55791
                                             OR p5761
                                             OR p4781
                                             OR p3790
                                             OR p2350
                                             OR p1392
                                             OR p480
                                          GROUP BY date_begin
                                          ORDER BY date_begin);  DROP TEMPORARY TABLE IF EXISTS T14;
CREATE TEMPORARY TABLE IF NOT EXISTS T14 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('2006', '2007')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);  DROP TEMPORARY TABLE IF EXISTS T15;
CREATE TEMPORARY TABLE IF NOT EXISTS T15 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('2014')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);  DROP TEMPORARY TABLE IF EXISTS T16;
CREATE TEMPORARY TABLE IF NOT EXISTS T16 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('2026', '2028', '2029')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);  DROP TEMPORARY TABLE IF EXISTS T17;
CREATE TEMPORARY TABLE IF NOT EXISTS T17 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('NO_CODE')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);  DROP TEMPORARY TABLE IF EXISTS T18;
CREATE TEMPORARY TABLE IF NOT EXISTS T18 (SELECT date_begin, COUNT(id_case) AS val
                                          FROM doctor_services
                                          WHERE code IN ('1009', '1010', '10101')
                                          GROUP BY date_begin
                                          ORDER BY date_begin);  DROP TEMPORARY TABLE IF EXISTS T19;
CREATE TEMPORARY TABLE IF NOT EXISTS T19 (SELECT date_begin, ROUND(COALESCE(SUM(UET), 0), 2) AS val
                                          FROM doctor_services
                                          GROUP BY date_begin
                                          ORDER BY date_begin);

create temporary table if not exists t_cf (SELECT date_begin, count(distinct id_case) as val
                             from doctor_services
							 where coag_flag = 1
                             group by date_begin
                             order by date_begin);







SELECT fr.st_num, fr.st_date, COALESCE(T1.val, 0), COALESCE(T2.val, 0), COALESCE(t_cf.val, 0), COALESCE(T3.val, 0), COALESCE(T4.val, 0),
       COALESCE(T5.val, 0), COALESCE(T6.val, 0), COALESCE(T7.val, 0), COALESCE(T8.val, 0), COALESCE(T9.val, 0),
       COALESCE(T10.val, 0), COALESCE(T11.val, 0), COALESCE(T13.val, 0), COALESCE(T14.val, 0), COALESCE(T15.val, 0),
       COALESCE(T16.val, 0), COALESCE(T17.val, 0), COALESCE(T18.val, 0), COALESCE(T19.val, 0)
FROM first_row fr
         LEFT JOIN T1 ON T1.date_begin = fr.st_date
         LEFT JOIN T2 ON T2.date_begin = fr.st_date
		 left join t_cf on t_cf.date_begin = fr.st_date
         LEFT JOIN T3 ON T3.date_begin = fr.st_date
         LEFT JOIN T4 ON T4.date_begin = fr.st_date
         LEFT JOIN T5 ON T5.date_begin = fr.st_date
         LEFT JOIN T6 ON T6.date_begin = fr.st_date
         LEFT JOIN T7 ON T7.date_begin = fr.st_date
         LEFT JOIN T8 ON T8.date_begin = fr.st_date
         LEFT JOIN T9 ON T9.date_begin = fr.st_date
         LEFT JOIN T10 ON T10.date_begin = fr.st_date
         LEFT JOIN T11 ON T11.date_begin = fr.st_date
         LEFT JOIN T13 ON T13.date_begin = fr.st_date
         LEFT JOIN T14 ON T14.date_begin = fr.st_date
         LEFT JOIN T15 ON T15.date_begin = fr.st_date
         LEFT JOIN T16 ON T16.date_begin = fr.st_date
         LEFT JOIN T17 ON T17.date_begin = fr.st_date
         LEFT JOIN T18 ON T18.date_begin = fr.st_date
         LEFT JOIN T19 ON T19.date_begin = fr.st_date
ORDER BY st_num;
