CREATE TEMPORARY TABLE if NOT EXISTS doctor_services (
SELECT cs.date_begin, cs.id_case, vp.profile_infis_code, p.code, vd.diagnosis_code, cs.tooth_name, T.uet, cs.is_oms, c.is_closed
FROM case_services cs
LEFT JOIN cases c ON c.id_case = cs.id_case
LEFT JOIN vmu_profile vp ON vp.id_profile = cs.id_profile AND cs.is_oms = 1
LEFT JOIN price p ON p.id = cs.id_profile AND cs.is_oms in (0, 3, 4)
LEFT JOIN vmu_diagnosis vd ON cs.id_diagnosis = vd.id_diagnosis
LEFT JOIN doctor_spec ds ON cs.id_doctor = ds.doctor_id
left join appointments a on a.case_id = cs.id_case and a.time between @date_start AND @date_end + interval 1 day
LEFT JOIN (
	SELECT vt.uet, vt.id_profile, vt.id_zone_type, vt.tariff_begin_date AS d1,
	vt.tariff_end_date AS d2, vtp.tp_begin_date AS d3, vtp.tp_end_date AS d4
	FROM vmu_tariff vt, vmu_tariff_plan vtp
	WHERE vtp.id_tariff_group = vt.id_lpu AND vtp.id_lpu in (
		SELECT id_lpu
		FROM mu_ident)) T ON T.id_profile = vp.id_profile
			AND cs.date_begin BETWEEN T.d1 AND T.d2 AND cs.date_begin BETWEEN
			T.d3 AND T.d4 AND T.id_zone_type = cs.id_net_profile AND cs.is_oms = 1
WHERE (ds.department_id = @depart_id OR @depart_id = 0) AND ds.spec_id = 106 and a.user_id = 1
AND cs.date_begin BETWEEN @date_start AND @date_end);

CREATE TEMPORARY TABLE if NOT EXISTS doctor_protocols (
SELECT ds.id_case, ds.date_begin, LOCATE('17239', p.id_ref) = 1 AS p17239,
LOCATE('17251', p.id_ref) = 1 AS p17251,
LOCATE('18720', p.id_ref) = 1 AS p18720,
LOCATE('19010', p.id_ref) = 1 AS p19010,
LOCATE('44170', p.id_ref) = 1 AS p44170,
LOCATE('47780', p.id_ref) = 1 AS p47780,
LOCATE('79760', p.id_ref) = 1 AS p79760, 
LOCATE('80370', p.id_ref) = 1 AS p80370,
LOCATE('18730', p.id_ref) = 1 AS p18730,
LOCATE('80380', p.id_ref) = 1 AS p80380,
LOCATE('55791', p.id_ref) = 1 AS p55791,
LOCATE('5761', p.id_ref) = 1 AS p5761,
LOCATE('4781', p.id_ref) = 1 AS p4781,
LOCATE('3790', p.id_ref) = 1 AS p3790,
LOCATE('2350', p.id_ref) = 1 AS p2350,
LOCATE('1392', p.id_ref) = 1 AS p1392,
LOCATE('480', p.id_ref) = 1 AS p480 
FROM doctor_services ds
LEFT JOIN protocols p ON p.id_case = ds.id_case
WHERE ds.is_oms = 1); 
SET @ROW = 0;
CREATE TEMPORARY TABLE if NOT EXISTS first_row (
SELECT @ROW := @ROW + 1 AS st_num, T.date_begin AS st_date
FROM (
SELECT date_begin
FROM doctor_services
GROUP BY date_begin
ORDER BY date_begin) T); 
CREATE TEMPORARY TABLE if NOT EXISTS T1 (
SELECT date_begin, COUNT(DISTINCT id_case) AS val
FROM doctor_services
WHERE is_closed = 1
GROUP BY date_begin
ORDER BY date_begin);
CREATE TEMPORARY TABLE if NOT EXISTS T2 (
SELECT date_begin, COUNT(DISTINCT id_case) AS val
FROM doctor_services
WHERE profile_infis_code in ('стт005')
GROUP BY date_begin
ORDER BY date_begin);
CREATE TEMPORARY TABLE if NOT EXISTS T3 (
SELECT date_begin, COUNT(DISTINCT id_case) AS val
FROM doctor_protocols
WHERE p17239 OR p17251 OR p18720 OR p19010 OR p44170 OR p47780 OR p79760 OR p80370
GROUP BY date_begin
ORDER BY date_begin);
CREATE TEMPORARY TABLE if NOT EXISTS T4 (
SELECT date_begin, COUNT(DISTINCT id_case) AS val
FROM doctor_protocols
WHERE p18730 OR p80380
GROUP BY date_begin
ORDER BY date_begin);
CREATE TEMPORARY TABLE if NOT EXISTS T5 (
SELECT T.date_begin, COUNT(T.val) AS val
FROM (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code in ('стт025', 'стт028', 'стт031')
GROUP BY id_case, tooth_name
ORDER BY date_begin) T
GROUP BY date_begin);
CREATE TEMPORARY TABLE if NOT EXISTS T6 (
SELECT T.date_begin, COUNT(T.val) AS val
FROM (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code in ('стт025', 'стт028', 'стт031')
AND diagnosis_code in ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5', 'k02.6', 'k02.7', 'k02.8', 'k02.9', 'k03.2')
GROUP BY id_case, tooth_name
ORDER BY date_begin) T
GROUP BY date_begin);
CREATE TEMPORARY TABLE if NOT EXISTS T7 (
SELECT T.date_begin, COUNT(T.val) AS val
FROM (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code in ('стт025', 'стт028', 'стт031') AND diagnosis_code in ('k04.0', 'k04.1', 'k04.2', 'k04.3')
GROUP BY id_case, tooth_name
ORDER BY date_begin) T
GROUP BY date_begin);
CREATE TEMPORARY TABLE if NOT EXISTS T8 (
SELECT T.date_begin, COUNT(T.val) AS val
FROM (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code in ('стт025', 'стт028', 'стт031') AND diagnosis_code in ('k04.4', 'k04.5')
GROUP BY id_case, tooth_name
ORDER BY date_begin) T
GROUP BY date_begin);
CREATE TEMPORARY TABLE if NOT EXISTS T9 (
SELECT T.date_begin, COUNT(T.val) AS val
FROM (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code in ('стт053')
GROUP BY id_case, tooth_name
ORDER BY date_begin) T
GROUP BY date_begin);
CREATE TEMPORARY TABLE if NOT EXISTS T10 (
SELECT T.date_begin, COUNT(T.val) AS val
FROM (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code in ('стт026', 'стт027', 'стт029', 'стт030', 'стт032', 'стт033', 'стт057', 'стт058')
AND diagnosis_code in ('k04.0', 'k04.1', 'k04.2', 'k04.3', 'k04.4', 'k04.5')
GROUP BY id_case, tooth_name
ORDER BY date_begin) T
GROUP BY date_begin);
CREATE TEMPORARY TABLE if NOT EXISTS T11 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code in ('стт026', 'стт027', 'стт029', 'стт030', 'стт032', 'стт033', 'стт034', 'стт057', 'стт058')
GROUP BY date_begin
ORDER BY date_begin);
CREATE TEMPORARY TABLE if NOT EXISTS T12 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE code in ('1027', '1028', '1029', '1031', '1033', '1034', '1039', '1040',
 '1041', '1042', '1043', '1044', '1045', '1046', '1047', '1048', '1049', '1050',
 '1052', '1054', '1058', '1059', '1062', '1073', '1074', '1092', '1093', '1094',
 '1095', '1096', '1097', '1098', '1099', '1100', '1101', '1102', '1103', '1104',
'1105', '1106', '1107', '1108', '1109', '1110', '1111', '1118', '1119')
GROUP BY date_begin
ORDER BY date_begin);
CREATE TEMPORARY TABLE if NOT EXISTS T13 (
SELECT date_begin, COUNT(DISTINCT id_case) AS val
FROM doctor_protocols
WHERE p55791 OR p5761 OR p4781 OR p3790 OR p2350 OR p1392 OR p480
GROUP BY date_begin
ORDER BY date_begin);
CREATE TEMPORARY TABLE if NOT EXISTS T14 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code in ('стт041')
GROUP BY date_begin
ORDER BY date_begin);
CREATE TEMPORARY TABLE if NOT EXISTS T15 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code in ('сто022')
GROUP BY date_begin
ORDER BY date_begin);
CREATE TEMPORARY TABLE if NOT EXISTS T16 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code in ('стт020')
GROUP BY date_begin
ORDER BY date_begin);
CREATE TEMPORARY TABLE if NOT EXISTS T17 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code in ('стт022')
GROUP BY date_begin
ORDER BY date_begin);
CREATE TEMPORARY TABLE if NOT EXISTS T18 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code in ('сто001', 'сто002', 'сто003')
GROUP BY date_begin
ORDER BY date_begin);
CREATE TEMPORARY TABLE if NOT EXISTS T19 (
SELECT date_begin, ROUND(COALESCE(SUM(UET), 0), 2) AS val
FROM doctor_services
GROUP BY date_begin
ORDER BY date_begin);
CREATE TEMPORARY TABLE if NOT EXISTS T21 (
SELECT date_begin, COUNT(id_case) AS val
FROM doctor_services
WHERE profile_infis_code in ('стт008')
GROUP BY date_begin
ORDER BY date_begin);
SELECT fr.st_num, fr.st_date, COALESCE(T1.val, 0), COALESCE(T2.val, 0), 0, COALESCE(T3.val, 0), COALESCE(T4.val, 0), COALESCE(T5.val, 0), COALESCE(T6.val, 0), COALESCE(T7.val, 0), COALESCE(T8.val, 0), COALESCE(T9.val, 0), COALESCE(T10.val, 0), COALESCE(T11.val, 0) + COALESCE(T12.val, 0), COALESCE(T11.val, 0), COALESCE(T12.val, 0), COALESCE(T13.val, 0), COALESCE(T21.val, 0), COALESCE(T14.val, 0), COALESCE(T15.val, 0), COALESCE(T16.val, 0), COALESCE(T17.val, 0), COALESCE(T18.val, 0), COALESCE(T19.val, 0)
FROM first_row fr
LEFT JOIN T1 ON T1.date_begin = fr.st_date
LEFT JOIN T2 ON T2.date_begin = fr.st_date
LEFT JOIN T3 ON T3.date_begin = fr.st_date
LEFT JOIN T4 ON T4.date_begin = fr.st_date
LEFT JOIN T5 ON T5.date_begin = fr.st_date
LEFT JOIN T6 ON T6.date_begin = fr.st_date
LEFT JOIN T7 ON T7.date_begin = fr.st_date
LEFT JOIN T8 ON T8.date_begin = fr.st_date
LEFT JOIN T9 ON T9.date_begin = fr.st_date
LEFT JOIN T10 ON T10.date_begin = fr.st_date
LEFT JOIN T11 ON T11.date_begin = fr.st_date
LEFT JOIN T12 ON T12.date_begin = fr.st_date
LEFT JOIN T13 ON T13.date_begin = fr.st_date
LEFT JOIN T14 ON T14.date_begin = fr.st_date
LEFT JOIN T15 ON T15.date_begin = fr.st_date
LEFT JOIN T16 ON T16.date_begin = fr.st_date
LEFT JOIN T17 ON T17.date_begin = fr.st_date
LEFT JOIN T18 ON T18.date_begin = fr.st_date
LEFT JOIN T19 ON T19.date_begin = fr.st_date
LEFT JOIN T21 ON T21.date_begin = fr.st_date
ORDER BY st_num;