set @row = 0;
 create temporary table if not exists t1(
 select tech.id, tech.name, pre.name as descr, COUNT(pre.name) as name_count, pre.koef as koef
         from case_services as cs
 left join price_orto_soc as Po on po.id = cs.id_profile
 left join orders o on o.id = cs.ID_ORDER
 left join price as P  on p.id = cs.id_profile
 left join precursors pre on pre.price_value = p.code and o.order_type <> 1 
 left join technicians tech on tech.id = o.Id_technician
 where o.orderCompletionDate between @date_start and @date_end 
   
 and p.code in (8095, 8096, 8098, 8099, 8100, 8123, 8133, 8141, 8194, 8195, 8196, 82081, 82082, 8213)
 GROUP BY name, descr, koef
 having COUNT(pre.name) > 0);
 create temporary table if not exists t2(
 select tech.id, tech.name, pre.name as descr, COUNT(pre.name) as name_count, pre.koef as koef
         from case_services as cs
 left join price_orto_soc as Po on po.id = cs.id_profile
 left join orders o on o.id = cs.ID_ORDER
 left join price as P  on p.id = cs.id_profile
 left join precursors pre on pre.price_value = p.code and o.order_type <> 1 
 left join technicians tech on tech.id = o.Id_technician
 where o.orderCompletionDate between @date_start and @date_end
   
 and p.code in (80911, 8152, 8157, 8197, 8214, 8215, 8216)
 GROUP BY name, descr, koef
 having COUNT(pre.name) > 0);
 
 select @row:=@row+1, T.* from (select t.name,  coalesce(t1.name_count,0), 2 as a,
        coalesce(t1.name_count,0) * 2,
         coalesce(t2.name_count,0), 2 as b,
        coalesce(t2.name_count,0) * 2
        from technicians t
 left join t1 on t.id = t1.id
 left join t2 on t.ID = t2.id
 where t.is_active = 1 and (t1.name_count > 0 or t2.name_count > 0)
 
 order by t.name) T