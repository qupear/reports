
set @date_start = '2018-01-01';
set @date_end = '2018-12-31';

create temporary table if not exists doctor_services (select cs.date_begin, cs.id_case, vp.profile_infis_code, p.code,
                                                             vd.diagnosis_code, cs.tooth_name, T.uet, cs.is_oms,
                                                             pd.COAG_FLAG, pd.BIRTHDAY
                                                      from case_services cs
                                                               left join cases c on cs.id_case = c.id_case
                                                               left join patient_data pd on c.ID_PATIENT = pd.id_patient
                                                               left join vmu_profile vp on vp.id_profile = cs.id_profile and cs.is_oms = 1
                                                               left join price p on p.id = cs.id_profile and cs.is_oms in (0, 3)
                                                               left join vmu_diagnosis vd on cs.id_diagnosis = vd.id_diagnosis
                                                               left join (select vt.uet, vt.id_profile, vt.id_zone_type,
                                                                                 vt.tariff_begin_date as d1,
                                                                                 vt.tariff_end_date as d2,
                                                                                 vtp.tp_begin_date as d3,
                                                                                 vtp.tp_end_date as d4
                                                                          from vmu_tariff vt,
                                                                               vmu_tariff_plan vtp
                                                                          where vtp.id_tariff_group = vt.id_lpu
                                                                            And vtp.id_lpu in (select id_lpu from mu_ident)) T
                                                                         on T.id_profile = vp.id_profile and
                                                                            cs.date_begin between T.d1 and T.d2 and
                                                                            cs.date_begin between T.d3 and T.d4 and
                                                                            T.id_zone_type = cs.id_net_profile and
                                                                            cs.is_oms = 1
                                                      where cs.id_doctor = @doc_id
                                                        and cs.date_begin between @date_start and @date_end);
create temporary table if not exists child (select *
                                            from doctor_services
                                            where DATE_ADD(birthday, INTERVAL 18 year) > date_begin);
create temporary table if not exists child_before_15 (select *
                                                      from doctor_services
                                                      where DATE_ADD(birthday, INTERVAL 15 year) > date_begin);
create temporary table if not exists child_between_15_18 (select *
                                                          from doctor_services
                                                          where DATE_ADD(birthday, INTERVAL 15 year) < date_begin
                                                            and DATE_ADD(birthday, INTERVAL 18 year) > date_begin);  set @row = 0;
create temporary table if not exists first_row (select @row := @row + 1 as st_num, T.date_begin as st_date
                                                from (select date_begin
                                                      from doctor_services
                                                      group by date_begin
                                                      order by date_begin) T);
create temporary table if not exists T1 (select count(distinct id_case) as val, date_begin
                                         from doctor_services
                                         where profile_infis_code in ('стт014')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T2 (select count(distinct id_case) as val, date_begin
                                         from child_before_15
                                         where profile_infis_code in ('стт014')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T3 (select count(distinct id_case) as val, date_begin
                                         from child_between_15_18
                                         where profile_infis_code in ('стт014')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T4 (select date_begin, COAG_FLAG as cf
                                         from doctor_services
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T5 (select count(distinct id_case) as val, date_begin
                                         from child
                                         where profile_infis_code in ('стт048')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T6 (select count(distinct id_case) as val, date_begin
                                         from child_before_15
                                         where profile_infis_code in ('стт048')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T7 (select count(distinct id_case) as val, date_begin
                                         from child_between_15_18
                                         where profile_infis_code in ('стт048')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T8 (select count(tooth_name) as val, date_begin
                                         from child
                                         where profile_infis_code in ('стт048')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T9 (select count(distinct id_case) as val, date_begin
                                         from child
                                         where profile_infis_code in ('стт020')
                                         group by date_begin
                                         order by date_begin);
create temporary table if not exists T10 (select count(distinct id_case) as val, date_begin
                                          from child_before_15
                                          where profile_infis_code in ('стт020')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists part_1 (select fr.st_num as s1, fr.st_date as s2, coalesce(T1.val, 0) as s3,
                                                    coalesce(T2.val, 0) as s4, coalesce(T3.val, 0) as s6, T4.cf as s5,
                                                    coalesce(T5.val, 0) as s7, coalesce(T6.val, 0) as s8,
                                                    coalesce(T7.val, 0) as s9, coalesce(T8.val, 0) as s10,
                                                    coalesce(T9.val, 0) as s11, coalesce(T10.val, 0) as s12
                                             from first_row fr
                                                      left join T1 on T1.date_begin = fr.st_date
                                                      left join T2 on T2.date_begin = fr.st_date
                                                      left join T3 on T3.date_begin = fr.st_date
                                                      left join T4 on T4.date_begin = fr.st_date
                                                      left join T5 on T5.date_begin = fr.st_date
                                                      left join T6 on T6.date_begin = fr.st_date
                                                      left join T7 on T7.date_begin = fr.st_date
                                                      left join T8 on T8.date_begin = fr.st_date
                                                      left join T9 on T9.date_begin = fr.st_date
                                                      left join T10 on T10.date_begin = fr.st_date
                                             order by s1);
create temporary table if not exists T11 (select count(distinct id_case) as val, date_begin
                                          from child_between_15_18
                                          where profile_infis_code in ('стт020')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T12 (select count(tooth_name) as val, date_begin
                                          from child
                                          where profile_infis_code in ('стт020')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T13 (select count(distinct id_case) as val, date_begin
                                          from child
                                          where profile_infis_code in ('стт049')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T14 (select count(distinct id_case) as val, date_begin
                                          from child_before_15
                                          where profile_infis_code in ('стт049')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T15 (select count(distinct id_case) as val, date_begin
                                          from child_between_15_18
                                          where profile_infis_code in ('стт049')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T16 (select count(tooth_name) as val, date_begin
                                          from child
                                          where profile_infis_code in ('стт049')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T17 (select count(distinct id_case) as val, date_begin
                                          from child
                                          where profile_infis_code in ('стт022')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T18 (select count(distinct id_case) as val, date_begin
                                          from child_before_15
                                          where profile_infis_code in ('стт022')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T19 (select count(distinct id_case) as val, date_begin
                                          from child_between_15_18
                                          where profile_infis_code in ('стт022')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T20 (select count(tooth_name) as val, date_begin
                                          from child
                                          where profile_infis_code in ('стт022')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T21 (select count(distinct id_case) as val, date_begin
                                          from child
                                          where profile_infis_code in ('стт041')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T22 (select count(distinct id_case) as val, date_begin
                                          from child_before_15
                                          where profile_infis_code in ('стт041')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T23 (select count(distinct id_case) as val, date_begin
                                          from child_between_15_18
                                          where profile_infis_code in ('стт041')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T24 (select count(tooth_name) as val, date_begin
                                          from child
                                          where profile_infis_code in ('стт041')
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T25 (select count(distinct id_case) as val, date_begin
                                          from child
                                          where ((profile_infis_code = 'сто022' and date_begin >= '2018-01-01')
                                       or (profile_infis_code = 'стт023' and date_begin between  '2017-04-01' and '2017-12-31')
                                              or (profile_infis_code = 'з122' and date_begin < '2017-04-01'))
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T26 (select count(distinct id_case) as val, date_begin
                                          from child_before_15
                                          where ((profile_infis_code = 'сто022' and date_begin >= '2018-01-01')
                                       or (profile_infis_code = 'стт023' and date_begin between  '2017-04-01' and '2017-12-31')
                                              or (profile_infis_code = 'з122' and date_begin < '2017-04-01'))
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T27 (select count(distinct id_case) as val, date_begin
                                          from child_between_15_18
                                          where ((profile_infis_code = 'сто022' and date_begin >= '2018-01-01')
                                       or (profile_infis_code = 'стт023' and date_begin between  '2017-04-01' and '2017-12-31')
                                              or (profile_infis_code = 'з122' and date_begin < '2017-04-01'))
                                          group by date_begin
                                          order by date_begin);
create temporary table if not exists T28 (select date_begin, ROUND(COALESCE(SUM(UET), 0), 2) as val
                                          from doctor_services
                                          group by date_begin
                                          order by date_begin);


select p1.*, coalesce(T11.val, 0), coalesce(T12.val, 0),
       coalesce(T13.val, 0), coalesce(T14.val, 0), coalesce(T15.val, 0), coalesce(T16.val, 0), coalesce(T17.val, 0),
       coalesce(T18.val, 0), coalesce(T19.val, 0), coalesce(T20.val, 0), coalesce(T21.val, 0), coalesce(T22.val, 0),
       coalesce(T23.val, 0), coalesce(T24.val, 0), coalesce(T25.val, 0), coalesce(T26.val, 0), coalesce(T27.val, 0),
       coalesce(T28.val, 0)
from part_1 p1
         left join T11 on T11.date_begin = p1.s2
         left join T12 on T12.date_begin = p1.s2
         left join T13 on T13.date_begin = p1.s2
         left join T14 on T14.date_begin = p1.s2
         left join T15 on T15.date_begin = p1.s2
         left join T16 on T16.date_begin = p1.s2
         left join T17 on T17.date_begin = p1.s2
         left join T18 on T18.date_begin = p1.s2
         left join T19 on T19.date_begin = p1.s2
         left join T20 on T20.date_begin = p1.s2
         left join T21 on T21.date_begin = p1.s2
         left join T22 on T22.date_begin = p1.s2
         left join T23 on T23.date_begin = p1.s2
         left join T24 on T24.date_begin = p1.s2
         left join T25 on T25.date_begin = p1.s2
         left join T26 on T26.date_begin = p1.s2
         left join T27 on T27.date_begin = p1.s2
         left join T28 on T28.date_begin = p1.s2
order by s1;
