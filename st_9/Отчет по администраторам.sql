
 
 create temporary table if not exists pat_before 
 (select distinct id_human from 
 (select distinct pd.id_human from appointments a
 left join patient_data pd on pd.id_patient = a.id_patient 
 where a.id_patient > 0 and date(a.time) < @date_start 
 UNION
 select distinct pd.id_human from cases c 
 left join patient_data pd on pd.id_patient = c.id_patient 
 where c.date_begin < @date_start) T); 
 
 create temporary table if not exists pat_now  
 (select distinct pd.id_human from appointments a
 left join patient_data pd on pd.id_patient = a.id_patient 
 where a.id_patient > 0 and date(a.time) between @date_start and @date_end); 
 
 create temporary table if not exists pat_primary  
 (select pn.id_human from pat_now pn where pn.id_human not in (select id_human from pat_before));
 
 create temporary table if not exists app_primary 
 (select a.time, pd.id_human, a.id_registrator from appointments a 
 left join patient_data pd on pd.id_patient = a.id_patient 
 where date(a.time) between @date_start and @date_end and a.id_patient > 0 and a.id_registrator > 0
 	and pd.id_human in (select id_human from pat_primary)
 order by a.time);
 
 create temporary table if not exists pat_reg 
 (select ap.id_human, ap.id_registrator from app_primary ap  
 group by ap.id_human order by ap.time); 
 create temporary table if not exists pat_reg_1 (select * from pat_reg); 
 
 select * from 
 (select u.name as reg_name, pd.card_number, concat(pd.surname, ' ', pd.name, ' ', pd.second_name) as pat_name, ch.date as pay_date, 
 	ud.name as doc_name, p.summ  
 from prepayments p 
 left join checks ch on ch.id = p.check 
 left join cases c on c.id_case = p.case_id 
 left join patient_data pd on pd.id_patient = c.id_patient 
 left join pat_reg pr on pr.id_human = pd.id_human 
 left join users u on u.id = pr.id_registrator 
 left join doctor_spec ds on p.doctor_id = ds.doctor_id 
 left join users ud on ud.id = ds.user_id 
 where ch.date between @date_start and @date_end and p.case_id > 0 
 	and pr.id_registrator is not null
 
 UNION 
 
 select u.name as reg_name, pd.card_number, concat(pd.surname, ' ', pd.name, ' ', pd.second_name) as pat_name, ch.date as pay_date, 
 	ud.name as doc_name, p.summ  
 from prepayments p 
 left join checks ch on ch.id = p.check 
 left join orders ord on ord.id = p.order_id 
 left join patient_data pd on pd.id_human = ord.id_human and pd.is_active = 1 and (ch.date between pd.date_begin and pd.date_end)  
 left join pat_reg_1 pr on pr.id_human = pd.id_human 
 left join users u on u.id = pr.id_registrator 
 left join doctor_spec ds on p.doctor_id = ds.doctor_id 
 left join users ud on ud.id = ds.user_id 
 where ch.date between @date_start and @date_end and p.order_id > 0 
 	and pr.id_registrator is not null) T 
 order by reg_name, pat_name, pay_date ;
 