set @date_start = '2019-03-01';
set @date_end = '2019-03-31';
set @doc_id = 167;
CREATE TEMPORARY TABLE IF NOT EXISTS doctor_services (
	                                                     SELECT c.id_case, cs.date_begin, p.code, vd.diagnosis_code,
	                                                            cs.tooth_name, p.uet, cs.is_oms, pd.coag_flag AS cf,
	                                                            (DATE_ADD(pd.birthday, INTERVAL 1 YEAR) > c.date_begin) AS age_0,
	                                                            (DATE_ADD(pd.birthday, INTERVAL 15 YEAR) > c.date_begin) AS age_1,
	                                                            (DATE_ADD(pd.birthday, INTERVAL 15 YEAR) < c.date_begin AND
	                                                             DATE_ADD(pd.birthday, INTERVAL 18 YEAR) > c.date_begin) AS age_2,
	                                                            (DATE_ADD(pd.birthday, INTERVAL 18 YEAR) > c.date_begin) AS child,
	                                                            cs.tooth_name IN
	                                                            ('51', '52', '53', '54', '55', '61', '62', '63', '64',
	                                                             '65', '71', '72', '73', '74', '75', '81', '82', '83',
	                                                             '84', '85') AS milk_tooth
		                                                     FROM
			                                                     case_services               cs
				                                                     LEFT JOIN cases         c ON c.id_case = cs.id_case
				                                                     LEFT JOIN patient_data  pd ON pd.id_patient = c.id_patient
				                                                     LEFT JOIN price         p ON p.id = cs.id_profile
				                                                     LEFT JOIN vmu_diagnosis vd ON cs.id_diagnosis = vd.id_diagnosis
		                                                     WHERE cs.id_doctor = @doc_id
			                                                   AND cs.date_begin BETWEEN @date_start AND @date_end
			                                                   AND cs.is_oms IN (0, 3));

/*create temporary table if not exists doctor_protocols (select ds.id_case, ds.date_begin, ds.age_1, ds.age_2,
                                                              locate('18730', p.child_ref_ids) > 0 as p18730,
                                                              locate('80380', p.child_ref_ids) > 0 as p80380,
                                                              locate('17239', p.child_ref_ids) > 0 as p17239,
                                                              locate('17251', p.child_ref_ids) > 0 as p17251,
                                                              locate('18720', p.child_ref_ids) > 0 as p18720,
                                                              locate('19010', p.child_ref_ids) > 0 as p19010,
                                                              locate('44170', p.child_ref_ids) > 0 as p44170,
                                                              locate('47780', p.child_ref_ids) > 0 as p47780,
                                                              locate('79760', p.child_ref_ids) > 0 as p79760,
                                                              locate('80370', p.child_ref_ids) > 0 as p80370,
                                                              locate('17591', p.child_ref_ids) > 0 as p17591,
                                                              locate('17691', p.child_ref_ids) > 0 as p17691,
                                                              locate('55791', p.child_ref_ids) > 0 as p55791,
                                                              locate(';5761', p.child_ref_ids) > 0 as p5761,
                                                              locate(';4781', p.child_ref_ids) > 0 as p4781,
                                                              locate(';3790', p.child_ref_ids) > 0 as p3790,
                                                              locate(';2350', p.child_ref_ids) > 0 as p2350,
                                                              locate(';1392', p.child_ref_ids) > 0 as p1392,
                                                              locate(';480', p.child_ref_ids) > 0 as p480,
                                                              locate('18690', p.child_ref_ids) > 0 as p18690,
                                                              locate('19000', p.child_ref_ids) > 0 as p19000,
                                                              locate('44160', p.child_ref_ids) > 0 as p44160,
                                                              locate('79750', p.child_ref_ids) > 0 as p79750,
                                                              locate('80340', p.child_ref_ids) > 0 as p80340,
                                                              locate('4790', p.child_ref_ids) > 0 as p4790,
                                                              locate('5770', p.child_ref_ids) > 0 as p5770
                                                       from (select id_case, date_begin, age_1, age_2
                                                             from doctor_services
                                                             group by id_case) ds
                                                                left join protocols p on p.id_case = ds.id_case
                                                       where p.id_doctor = @doc_id);  */
/*2019-10-15 shamshurina - в предыдущей версии doctor_protocols ошибки из-за изменений в дереве протоколов, у первых веток лучше использовать id_ref*/
create temporary table if not exists doctor_protocols (select ds.id_case, ds.date_begin, ds.age_0, ds.age_1, ds.age_2,
                                                              locate('18730', p.id_ref) = 1 as p18730,
                                                              locate('80380', p.id_ref) = 1 as p80380,
                                                              locate('17239', p.id_ref) = 1 as p17239,
                                                              locate('17251', p.id_ref) = 1 as p17251,
                                                              locate('18720', p.id_ref) = 1 as p18720,
                                                              locate('19010', p.id_ref) = 1 as p19010,
                                                              locate('44170', p.id_ref) = 1 as p44170,
                                                              locate('47780', p.id_ref) = 1 as p47780,
                                                              locate('79760', p.id_ref) = 1 as p79760,
                                                              locate('80370', p.id_ref) = 1 as p80370,
                                                              locate('17591', p.id_ref) = 1 as p17591,
                                                              locate('17691', p.id_ref) = 1 as p17691,
                                                              locate('55791', p.id_ref) = 1 as p55791,
                                                              locate('5761', p.id_ref) = 1 as p5761,
                                                              locate('4781', p.id_ref) = 1 as p4781,
                                                              locate('3790', p.id_ref) = 1 as p3790,
                                                              locate('2350', p.id_ref) = 1 as p2350,
                                                              locate('1392', p.id_ref) = 1 as p1392,
                                                              locate('480', p.id_ref) = 1 as p480,
                                                              locate('18690', p.id_ref) = 1 as p18690,
                                                              locate('19000', p.id_ref) = 1 as p19000,
                                                              locate('44160', p.id_ref) = 1 as p44160,
                                                              locate('79750', p.id_ref) = 1 as p79750,
                                                              locate('80340', p.id_ref) = 1 as p80340,
                                                              locate('4790', p.id_ref) = 1 as p4790,
                                                              locate('5770', p.id_ref) = 1 as p5770
                                                       from (select id_case, date_begin, age_0, age_1, age_2
                                                             from doctor_services
                                                             group by id_case) ds
                                                                left join protocols p on p.id_case = ds.id_case
                                                       where p.id_doctor = @doc_id);
set @row = 0;
create temporary table if not exists dates
(select @row := @row + 1 as st_num, T.date_begin
from (select date_begin from doctor_services group by date_begin order by date_begin) T);
create temporary table if not exists T1 (select date_begin, count(id_case) as summ
                                         from (select date_begin, id_case from doctor_services group by id_case) T
                                         group by date_begin);
create temporary table if not exists T2 (select date_begin, count(id_case) as summ
                                         from (select date_begin, id_case
                                               from doctor_services
                                               where age_1
                                               group by id_case) T
                                         group by date_begin);
create temporary table if not exists T3 (select date_begin, count(id_case) as summ
                                         from (select date_begin, id_case
                                               from doctor_services
                                               where age_2
                                               group by id_case) T
                                         group by date_begin);
/*с проф целью*/
/*всего*/
create temporary table if not exists T57 (select date_begin, count(id_case) as summ
                                         from (select date_begin, id_case from doctor_services
                                         WHERE diagnosis_code in ('Z01.2', 'K02.3')
                                         group by id_case) T
                                         group by date_begin);
/*от 0 до 12 мес*/
create temporary table if not exists T58 (select date_begin, count(id_case) as summ
                                         from (select date_begin, id_case from doctor_services
                                         WHERE diagnosis_code in ('Z01.2') AND age_0
                                         group by id_case) T
                                         group by date_begin);
/*от 1 до 14*/
create temporary table if not exists T59 (select date_begin, count(id_case) as summ
                                         from (select date_begin, id_case from doctor_services
                                         WHERE diagnosis_code in ('Z01.2', 'K02.3') AND age_1
                                         group by id_case) T
                                         group by date_begin);
/*от 15 до 17*/
create temporary table if not exists T60 (select date_begin, count(id_case) as summ
                                         from (select date_begin, id_case from doctor_services
                                         WHERE diagnosis_code in ('Z01.2', 'K02.3') AND age_2
                                         group by id_case) T
                                         group by date_begin);
create temporary table if not exists T4 (select date_begin, count(id_case) as summ
                                         from (select date_begin, id_case
                                               from doctor_services
                                               where code in ('1002')
                                               group by id_case) T
                                         group by date_begin);
create temporary table if not exists T5 (select date_begin, count(id_case) as summ
                                         from (select date_begin, id_case
                                               from doctor_services
                                               where code in ('1002') AND (age_1 OR age_0)
                                               group by id_case) T
                                         group by date_begin);
create temporary table if not exists T6 (select date_begin, count(id_case) as summ
                                         from (select date_begin, id_case
                                               from doctor_services
                                               where code in ('1002') and age_2
                                               group by id_case) T
                                         group by date_begin);
create temporary table if not exists T7 (select date_begin, count(id_case) as summ
                                         from (select date_begin, id_case
                                               from doctor_services
                                               where code in
                                                     ('1014', '1015', '1016', '1017', '1018', '1019', '1020', '1027',
                                                      '1028')
                                                 and diagnosis_code in
                                                     ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5', 'k02.6',
                                                      'k02.7', 'k02.8', 'k02.9', 'k03.2', 'k04.0', 'k04.1', 'k04.2',
                                                      'k04.3', 'k04.4', 'k04.5')
                                               group by id_case, tooth_name) T
                                         group by date_begin);
create temporary table if not exists T8 (select date_begin, count(id_case) as summ
                                         from (select date_begin, id_case
                                               from doctor_services
                                               where code in
                                                     ('1014', '1015', '1016', '1017', '1018', '1019', '1020', '1027',
                                                      '1028')
                                                 and diagnosis_code in
                                                     ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5', 'k02.6',
                                                      'k02.7', 'k02.8', 'k02.9', 'k03.2', 'k04.0', 'k04.1', 'k04.2',
                                                      'k04.3', 'k04.4', 'k04.5')
                                                 AND (age_1 OR age_0)
                                               group by id_case, tooth_name) T
                                         group by date_begin);
create temporary table if not exists T9 (select date_begin, count(id_case) as summ
                                         from (select date_begin, id_case
                                               from doctor_services
                                               where code in
                                                     ('1014', '1015', '1016', '1017', '1018', '1019', '1020', '1027',
                                                      '1028')
                                                 and diagnosis_code in
                                                     ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5', 'k02.6',
                                                      'k02.7', 'k02.8', 'k02.9', 'k03.2', 'k04.0', 'k04.1', 'k04.2',
                                                      'k04.3', 'k04.4', 'k04.5')
                                                 and age_2
                                               group by id_case, tooth_name) T
                                         group by date_begin);
create temporary table if not exists T10 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('1014', '1016', '1017', '1019', '1027', '1028')
                                                  and diagnosis_code in
                                                      ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5', 'k02.6',
                                                       'k02.7', 'k02.8', 'k02.9', 'k03.2')
                                                  and not milk_tooth
                                                group by id_case, tooth_name) T
                                          group by date_begin);
create temporary table if not exists T11 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('1014', '1016', '1017', '1019', '1027', '1028')
                                                  and diagnosis_code in
                                                      ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5', 'k02.6',
                                                       'k02.7', 'k02.8', 'k02.9', 'k03.2')
                                                  and not milk_tooth
                                                  AND (age_1 OR age_0)
                                                group by id_case, tooth_name) T
                                          group by date_begin);
create temporary table if not exists T12 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('1014', '1016', '1017', '1019', '1027', '1028')
                                                  and diagnosis_code in
                                                      ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5', 'k02.6',
                                                       'k02.7', 'k02.8', 'k02.9', 'k03.2')
                                                  and not milk_tooth
                                                  and age_2
                                                group by id_case, tooth_name) T
                                          group by date_begin);
create temporary table if not exists T13 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('1014', '1016', '1017', '1019', '1027', '1028')
                                                  and diagnosis_code in
                                                      ('k02.0', 'k02.1', 'k02.2', 'k02.3', 'k02.4', 'k02.5', 'k02.6',
                                                       'k02.7', 'k02.8', 'k02.9', 'k03.2')
                                                  and milk_tooth
                                                group by id_case, tooth_name) T
                                          group by date_begin);
create temporary table if not exists T14 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in
                                                      ('1014', '1015', '1016', '1017', '1018', '1019', '1020', '1027',
                                                       '1028')
                                                  and diagnosis_code in ('k04.0', 'k04.1', 'k04.2', 'k04.3', 'k04.4', 'k04.5')
                                                  and not milk_tooth
                                                group by id_case, tooth_name) T
                                          group by date_begin);
create temporary table if not exists T15 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in
                                                      ('1014', '1015', '1016', '1017', '1018', '1019', '1020', '1027',
                                                       '1028')
                                                  and diagnosis_code in ('k04.0', 'k04.1', 'k04.2', 'k04.3', 'k04.4', 'k04.5')
                                                  and not milk_tooth
                                                  AND (age_1 OR age_0)
                                                group by id_case, tooth_name) T
                                          group by date_begin);
create temporary table if not exists T16 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in
                                                      ('1014', '1015', '1016', '1017', '1018', '1019', '1020', '1027',
                                                       '1028')
                                                  and diagnosis_code in ('k04.0', 'k04.1', 'k04.2', 'k04.3', 'k04.4', 'k04.5')
                                                  and not milk_tooth
                                                  and age_2
                                                group by id_case, tooth_name) T
                                          group by date_begin);
create temporary table if not exists T17 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in
                                                      ('1014', '1015', '1016', '1017', '1018', '1019', '1020', '1027',
                                                       '1028')
                                                  and diagnosis_code in ('k04.0', 'k04.1', 'k04.2', 'k04.3', 'k04.4', 'k04.5')
                                                  and milk_tooth
                                                group by id_case, tooth_name) T
                                          group by date_begin);
create temporary table if not exists T18 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in
                                                      ('1027', '1028', '1029', '1031', '1033', '1034', '1039', '1040',
                                                       '1041', '1042', '1043', '1044', '1045', '1046', '1047', '1048',
                                                       '1049', '1050', '1052', '1054', '1058', '1059', '1062', '1073',
                                                       '1074', '1092', '1093', '1094', '1095', '1096', '1097', '1098',
                                                       '1099', '1100', '1101', '1102', '1103', '1104', '1105', '1106',
                                                       '1107', '1108', '1109', '1110', '1111', '1118', '1119')) T
                                          group by date_begin);
create temporary table if not exists T19 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('1030', '1032', '1035', '1036') and milk_tooth) T
                                          group by date_begin);
create temporary table if not exists T20 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('2006', '2007') and child
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T21 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('2006', '2007') AND (age_1 OR age_0)
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T22 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('2006', '2007') and age_2
                                                group by id_case) T
                                          group by date_begin);
/* 21.02.19 Sysoev, redefined T23 create temporary table if not exists T23 (select date_begin, count(id_case) as summ from
   (select date_begin, id_case from doctor_services where code in ('2006', '2007') and child group by id_case, tooth_name) T group by date_begin;  */
create temporary table if not exists T23 (select p.date_protocol as date_begin,
                                                 sum(char_length(p.teeth_names) div 4 + 1) as summ
                                          from protocols p
                                          where p.id_case in (select distinct id_case
                                                              from doctor_services
                                                              where code in ('2006', '2007'))
                                            and (locate('3625(', p.child_ref_ids) > 0 or
                                                 locate('4464(', p.child_ref_ids) > 0 or
                                                 locate('5443(', p.child_ref_ids) > 0 or
                                                 locate('19053(', p.child_ref_ids) > 0 or
                                                 locate('89270(', p.child_ref_ids) > 0)
                                          group by p.date_protocol);
create temporary table if not exists T24 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('2010') and child
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T25 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('2010') AND (age_1 OR age_0)
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T26 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('2010') and age_2
                                                group by id_case) T
                                          group by date_begin);
/* 21.02.19 Sysoev, redefined T27  create temporary table if not exists T27  select date_begin, count(id_case) as summ from
   (select date_begin, id_case from doctor_services where code in ('2010') and child group by id_case, tooth_name) T group by date_begin;  */
create temporary table if not exists T27 (select p.date_protocol as date_begin,
                                                 sum(char_length(p.teeth_names) div 4 + 1) as summ
                                          from protocols p
                                          where p.id_case in
                                                (select distinct id_case from doctor_services where code in ('2010'))
                                            and (locate('3623(', p.child_ref_ids) > 0 or
                                                 locate('19054(', p.child_ref_ids) > 0 or
                                                 locate('79991(', p.child_ref_ids) > 0)
                                          group by p.date_protocol);
create temporary table if not exists T28 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('2026', '2028', '2029') and child
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T29 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('2026', '2028', '2029') AND (age_1 OR age_0)
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T30 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('2026', '2028', '2029') and age_2
                                                group by id_case) T
                                          group by date_begin);
/* 21.02.19 Sysoev, redefined T31     create temporary table if not exists T31  select date_begin, count(id_case) as summ from
   (select date_begin, id_case from doctor_services where code in ('2026', '2028', '2029') and child group by id_case, tooth_name) T group by date_begin;  */
create temporary table if not exists T31 (select p.date_protocol as date_begin,
                                                 sum(char_length(p.teeth_names) div 4 + 1) as summ
                                          from protocols p
                                          where p.id_case in (select distinct id_case
                                                              from doctor_services
                                                              where code in ('2026', '2028', '2029'))
                                            and (locate('39560(', p.child_ref_ids) > 0 or
                                                 locate('39570(', p.child_ref_ids) > 0 or
                                                 locate('48310(', p.child_ref_ids) > 0 or
                                                 locate('48320(', p.child_ref_ids) > 0 or
                                                 locate('48330(', p.child_ref_ids) > 0 or
                                                 locate('48340(', p.child_ref_ids) > 0 or
                                                 locate('48350(', p.child_ref_ids) > 0 or
                                                 locate('52510(', p.child_ref_ids) > 0 or
                                                 locate('80021(', p.child_ref_ids) > 0)
                                          group by p.date_protocol);
create temporary table if not exists T32 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('1053') and child
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T33 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('1053') AND (age_1 OR age_0)
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T34 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('1053') and age_2
                                                group by id_case) T
                                          group by date_begin);
/* 21.02.19 Sysoev, redefined T35    create temporary table if not exists T35  select date_begin, count(id_case) as summ from
   (select date_begin, id_case from doctor_services where code in ('1053') and child group by id_case, tooth_name) T group by date_begin;  */
create temporary table if not exists T35 (select p.date_protocol as date_begin,
                                                 sum(char_length(p.teeth_names) div 4 + 1) as summ
                                          from protocols p
                                          where p.id_case in
                                                (select distinct id_case from doctor_services where code in ('1053'))
                                            and (locate('39540(', p.child_ref_ids) > 0 or
                                                 locate('48250(', p.child_ref_ids) > 0 or
                                                 locate('48260(', p.child_ref_ids) > 0 or
                                                 locate('48270(', p.child_ref_ids) > 0 or
                                                 locate('91820(', p.child_ref_ids) > 0)
                                          group by p.date_protocol);
create temporary table if not exists T38 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('1013') and child
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T39 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('1013') AND (age_1 OR age_0)
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T40 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('1013') and age_2
                                                group by id_case) T
                                          group by date_begin);
/* 21.02.19 Sysoev, redefined T41    create temporary table if not exists T41  select date_begin, count(id_case) as summ from
   (select date_begin, id_case from doctor_services where code in ('1013') and child group by id_case, tooth_name) T group by date_begin;  */
create temporary table if not exists T41 (select p.date_protocol as date_begin,
                                                 sum(char_length(p.teeth_names) div 4 + 1) as summ
                                          from protocols p
                                          where p.id_case in
                                                (select distinct id_case from doctor_services where code in ('1013'))
                                            and (locate('3723(', p.child_ref_ids) > 0 or
                                                 locate('19202(', p.child_ref_ids) > 0 or
                                                 locate('80032(', p.child_ref_ids) > 0)
                                          group by p.date_protocol);
create temporary table if not exists T42 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('2014', '2015', '2021') and child
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T43 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('2014', '2015', '2021') AND (age_1 OR age_0)
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T44 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_services
                                                where code in ('2014', '2015', '2021') and age_2
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T45 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_protocols
                                                where (p18730 or p80380)
                                                group by id_case) T
                                          group by date_begin) ;
create temporary table if not exists T46 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_protocols
                                                where (p18730 or p80380) AND (age_1 OR age_0)
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T47 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_protocols
                                                where (p18730 or p80380) and age_2
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T48 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_protocols
                                                where (p17239 or p17251 or p18720 or p19010 or p44170 or p47780 or p79760 or p80370)
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T49 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_protocols
                                                where (p17239 or p17251 or p18720 or p19010 or p44170 or p47780 or p79760 or p80370)
                                                  AND (age_1 OR age_0)
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T50 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_protocols
                                                where (p17239 or p17251 or p18720 or p19010 or p44170 or p47780 or p79760 or p80370)
                                                  and age_2
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T51 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_protocols
                                                where (p18690 or p19000 or p44160 or p79750 or p80340)
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T52 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_protocols
                                                where (p18690 or p19000 or p44160 or p79750 or p80340)
                                                  AND (age_1 OR age_0)
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T53 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_protocols
                                                where (p18690 or p19000 or p44160 or p79750 or p80340)
                                                  and age_2
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T54 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_protocols
                                                where (p4790 or p5761 or p4781 or p3790 or p5770)
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T55 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_protocols
                                                where (p4790 or p5761 or p4781 or p3790 or p5770) AND (age_1 OR age_0)
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T56 (select date_begin, count(id_case) as summ
                                          from (select date_begin, id_case
                                                from doctor_protocols
                                                where (p4790 or p5761 or p4781 or p3790 or p5770) and age_2
                                                group by id_case) T
                                          group by date_begin);
create temporary table if not exists T_UET (select date_begin, ROUND(COALESCE(SUM(UET), 0), 2) as summ
                                            from doctor_services
                                            group by date_begin);
create temporary table t_cf (SELECT date_begin, sum(cf) as summ
                             from doctor_services
                             group by date_begin
                             order by date_begin);
CREATE TEMPORARY TABLE IF NOT EXISTS tmp_1 (
	                                           SELECT dates.date_begin, COALESCE(t1.summ, 0) AS s1,
	                                                  COALESCE(t2.summ, 0) AS s2, COALESCE(t3.summ, 0) AS s3,
	                                                  COALESCE(t_cf.summ, 0) AS cf,
	                                                  COALESCE(t57.summ, 0) AS s57, COALESCE(t58.summ, 0) AS s58,
	                                                  COALESCE(t59.summ, 0) AS s59,
	                                                  COALESCE(t60.summ, 0) AS s60,
	                                                  COALESCE(t4.summ, 0) AS s4,
	                                                  COALESCE(t5.summ, 0) AS s5, COALESCE(t6.summ, 0) AS s6,
	                                                  COALESCE(t7.summ, 0) AS s7, COALESCE(t8.summ, 0) AS s8,
	                                                  COALESCE(t9.summ, 0) AS s9, COALESCE(t10.summ, 0) AS s10,
	                                                  COALESCE(t11.summ, 0) AS s11, COALESCE(t12.summ, 0) AS s12,
	                                                  COALESCE(t13.summ, 0) AS s13, COALESCE(t14.summ, 0) AS s14,
	                                                  COALESCE(t15.summ, 0) AS s15, COALESCE(t16.summ, 0) AS s16,
	                                                  COALESCE(t17.summ, 0) AS s17, COALESCE(t18.summ, 0) AS s18,
	                                                  COALESCE(t19.summ, 0) AS s19, COALESCE(t20.summ, 0) AS s20
		                                           FROM
			                                           dates
				                                           LEFT JOIN t1 ON t1.date_begin = dates.date_begin
				                                           LEFT JOIN t2 ON t2.date_begin = dates.date_begin
				                                           LEFT JOIN t3 ON t3.date_begin = dates.date_begin
				                                           LEFT JOIN t4 ON t4.date_begin = dates.date_begin
				                                           LEFT JOIN t5 ON t5.date_begin = dates.date_begin
				                                           LEFT JOIN t6 ON t6.date_begin = dates.date_begin
				                                           LEFT JOIN t7 ON t7.date_begin = dates.date_begin
				                                           LEFT JOIN t8 ON t8.date_begin = dates.date_begin
				                                           LEFT JOIN t9 ON t9.date_begin = dates.date_begin
				                                           LEFT JOIN t10 ON t10.date_begin = dates.date_begin
				                                           LEFT JOIN t11 ON t11.date_begin = dates.date_begin
				                                           LEFT JOIN t12 ON t12.date_begin = dates.date_begin
				                                           LEFT JOIN t13 ON t13.date_begin = dates.date_begin
				                                           LEFT JOIN t14 ON t14.date_begin = dates.date_begin
				                                           LEFT JOIN t15 ON t15.date_begin = dates.date_begin
				                                           LEFT JOIN t16 ON t16.date_begin = dates.date_begin
				                                           LEFT JOIN t17 ON t17.date_begin = dates.date_begin
				                                           LEFT JOIN t18 ON t18.date_begin = dates.date_begin
				                                           LEFT JOIN t19 ON t19.date_begin = dates.date_begin
				                                           LEFT JOIN t20 ON t20.date_begin = dates.date_begin
				                                           LEFT JOIN t_cf ON t_cf.date_begin = dates.date_begin
				                                           LEFT JOIN t57 ON t57.date_begin = dates.date_begin
				                                           LEFT JOIN t58 ON t58.date_begin = dates.date_begin
				                                           LEFT JOIN t59 ON t59.date_begin = dates.date_begin
				                                           LEFT JOIN t60 ON t60.date_begin = dates.date_begin);


create temporary table if not exists tmp_2 (select tmp_1.*, COALESCE(T21.summ, 0) as s21, COALESCE(T22.summ, 0) as s22,
                                                   COALESCE(T23.summ, 0) as s23, COALESCE(T24.summ, 0) as s24,
                                                   COALESCE(T25.summ, 0) as s25, COALESCE(T26.summ, 0),
                                                   COALESCE(T27.summ, 0), COALESCE(T28.summ, 0), COALESCE(T29.summ, 0),
                                                   COALESCE(T30.summ, 0), COALESCE(T31.summ, 0), COALESCE(T32.summ, 0),
                                                   COALESCE(T33.summ, 0), COALESCE(T34.summ, 0), COALESCE(T35.summ, 0),
                                                   COALESCE(T38.summ, 0), COALESCE(T39.summ, 0), COALESCE(T40.summ, 0)
                                            from tmp_1
                                                     left join T21 on T21.date_begin = tmp_1.date_begin
                                                     left join T22 on T22.date_begin = tmp_1.date_begin
                                                     left join T23 on T23.date_begin = tmp_1.date_begin
                                                     left join T24 on T24.date_begin = tmp_1.date_begin
                                                     left join T25 on T25.date_begin = tmp_1.date_begin
                                                     left join T26 on T26.date_begin = tmp_1.date_begin
                                                     left join T27 on T27.date_begin = tmp_1.date_begin
                                                     left join T28 on T28.date_begin = tmp_1.date_begin
                                                     left join T29 on T29.date_begin = tmp_1.date_begin
                                                     left join T30 on T30.date_begin = tmp_1.date_begin
                                                     left join T31 on T31.date_begin = tmp_1.date_begin
                                                     left join T32 on T32.date_begin = tmp_1.date_begin
                                                     left join T33 on T33.date_begin = tmp_1.date_begin
                                                     left join T34 on T34.date_begin = tmp_1.date_begin
                                                     left join T35 on T35.date_begin = tmp_1.date_begin
                                                     left join T38 on T38.date_begin = tmp_1.date_begin
                                                     left join T39 on T39.date_begin = tmp_1.date_begin
                                                     left join T40 on T40.date_begin = tmp_1.date_begin)    ;

select tmp_2.*, COALESCE(T41.summ, 0), COALESCE(T42.summ, 0), COALESCE(T43.summ, 0), COALESCE(T44.summ, 0),
        COALESCE(T45.summ, 0), COALESCE(T46.summ, 0), COALESCE(T47.summ, 0), COALESCE(T48.summ, 0),
        COALESCE(T49.summ, 0), COALESCE(T50.summ, 0), COALESCE(T51.summ, 0), COALESCE(T52.summ, 0),
        COALESCE(T53.summ, 0), COALESCE(T54.summ, 0), COALESCE(T55.summ, 0), COALESCE(T56.summ, 0),
        COALESCE(T_UET.summ, 0), dts.st_num
 from tmp_2
          left join T41 on T41.date_begin = tmp_2.date_begin
          left join T42 on T42.date_begin = tmp_2.date_begin
          left join T43 on T43.date_begin = tmp_2.date_begin
          left join T44 on T44.date_begin = tmp_2.date_begin
          left join T45 on T45.date_begin = tmp_2.date_begin
          left join T46 on T46.date_begin = tmp_2.date_begin
          left join T47 on T47.date_begin = tmp_2.date_begin
          left join T48 on T48.date_begin = tmp_2.date_begin
          left join T49 on T49.date_begin = tmp_2.date_begin
          left join T50 on T50.date_begin = tmp_2.date_begin
          left join T51 on T51.date_begin = tmp_2.date_begin
          left join T52 on T52.date_begin = tmp_2.date_begin
          left join T53 on T53.date_begin = tmp_2.date_begin
          left join T54 on T54.date_begin = tmp_2.date_begin
          left join T55 on T55.date_begin = tmp_2.date_begin
          left join T56 on T56.date_begin = tmp_2.date_begin
          left join dates dts on tmp_2.date_begin = dts.date_begin
          left join T_UET on T_UET.date_begin = tmp_2.date_begin
 order by dts.date_begin;
