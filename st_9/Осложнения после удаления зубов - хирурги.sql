set @date_start = '2019-01-29';
set @date_end = '2019-04-29';

drop temporary table if exists doctor_services, t1, t1_1, t2, t2_1, t3, t3_1, t4, t4_1, t5, t5_1, t6, t6_1, t7, t7_1;

CREATE TEMPORARY TABLE if NOT EXISTS doctor_services 
SELECT cs.date_begin, cs.id_case, vd.diagnosis_code, pd.id_human, pd.BIRTHDAY
FROM case_services cs
LEFT JOIN vmu_diagnosis vd ON cs.id_diagnosis = vd.id_diagnosis
LEFT JOIN cases c ON c.id_case = cs.id_case
LEFT JOIN patient_data pd ON pd.id_patient = c.id_patient
LEFT JOIN doctor_spec ds ON cs.id_doctor = ds.doctor_id
WHERE ds.spec_id = 108 AND cs.date_begin BETWEEN @date_start AND @date_end;

create temporary table if not exists t1
select ds.date_begin, count(distinct ds.id_case) as sum
from doctor_services ds
where diagnosis_code in ('K10.3')
group by date_begin;

create temporary table if not exists t1_1
select ds.date_begin, count(distinct ds.id_case) as sum
from doctor_services ds
where diagnosis_code in ('K10.3') and date_add(ds.birthday, interval 18 year) > @date_end
group by date_begin;

create temporary table if not exists t2
select ds.date_begin, count(distinct ds.id_case) as sum
from doctor_services ds
where diagnosis_code in ('G50.9')
group by date_begin;

create temporary table if not exists t2_1
select ds.date_begin, count(distinct ds.id_case) as sum
from doctor_services ds
where diagnosis_code in ('G50.9') and date_add(ds.birthday, interval 18 year) > @date_end
group by date_begin;

create temporary table if not exists t3
select ds.date_begin, count(distinct ds.id_case) as sum
from doctor_services ds
where diagnosis_code in ('G51.8')
group by date_begin;

create temporary table if not exists t3_1
select ds.date_begin, count(distinct ds.id_case) as sum
from doctor_services ds
where diagnosis_code in ('G51.8') and date_add(ds.birthday, interval 18 year) > @date_end
group by date_begin;

create temporary table if not exists t4
select ds.date_begin, count(distinct ds.id_case) as sum
from doctor_services ds
where diagnosis_code in ('T81.2')
group by date_begin;

create temporary table if not exists t4_1
select ds.date_begin, count(distinct ds.id_case) as sum
from doctor_services ds
where diagnosis_code in ('T81.2') and date_add(ds.birthday, interval 18 year) > @date_end
group by date_begin;

create temporary table if not exists t5
select ds.date_begin, count(distinct ds.id_case) as sum
from doctor_services ds
where diagnosis_code in ('T81.0')
group by date_begin;

create temporary table if not exists t5_1
select ds.date_begin, count(distinct ds.id_case) as sum
from doctor_services ds
where diagnosis_code in ('T81.0') and date_add(ds.birthday, interval 18 year) > @date_end
group by date_begin;

create temporary table if not exists t6
select ds.date_begin, count(distinct ds.id_case) as sum
from doctor_services ds
where diagnosis_code in ('S02.8')
group by date_begin;

create temporary table if not exists t6_1
select ds.date_begin, count(distinct ds.id_case) as sum
from doctor_services ds
where diagnosis_code in ('S02.8') and date_add(ds.birthday, interval 18 year) > @date_end
group by date_begin;

create temporary table if not exists t7
select ds.date_begin, count(distinct ds.id_case) as sum
from doctor_services ds
where diagnosis_code in ('S02.6')
group by date_begin;

create temporary table if not exists t7_1
select ds.date_begin, count(distinct ds.id_case) as sum
from doctor_services ds
where diagnosis_code in ('S02.6') and date_add(ds.birthday, interval 18 year) > @date_end
group by date_begin;

select ds.date_begin,
	coalesce(t1.sum, 0) 'K10.3', coalesce(t1_1.sum, 0) 'K10.3 ch',
	coalesce(t2.sum, 0) 'G50.9', coalesce(t2_1.sum, 0) 'G50.9 ch',
	coalesce(t3.sum, 0) 'G51.8', coalesce(t3_1.sum, 0) 'G51.8 ch',
	coalesce(t4.sum, 0) 'T81.2', coalesce(t4_1.sum, 0) 'T81.2 ch',
	coalesce(t5.sum, 0) 'T81.0', coalesce(t5_1.sum, 0) 'T81.0 ch',
	coalesce(t6.sum, 0) 'S02.8', coalesce(t6_1.sum, 0) 'S02.8 ch',
	coalesce(t7.sum, 0) 'S02.6', coalesce(t7_1.sum, 0) 'S02.6 ch'
from doctor_services ds
left join t1 on t1.date_begin = ds.date_begin
left join t1_1 on t1_1.date_begin = ds.date_begin
left join t2 on t2.date_begin = ds.date_begin
left join t2_1 on t2_1.date_begin = ds.date_begin
left join t3 on t3.date_begin = ds.date_begin
left join t3_1 on t3_1.date_begin = ds.date_begin
left join t4 on t4.date_begin = ds.date_begin
left join t4_1 on t4_1.date_begin = ds.date_begin
left join t5 on t5.date_begin = ds.date_begin
left join t5_1 on t5_1.date_begin = ds.date_begin
left join t6 on t6.date_begin = ds.date_begin
left join t6_1 on t6_1.date_begin = ds.date_begin
left join t7 on t7.date_begin = ds.date_begin
left join t7_1 on t7_1.date_begin = ds.date_begin
where ds.DIAGNOSIS_CODE in ('K10.3', 'G50.9', 'G51.8', 'T81.2', 'T81.0', 'S02.8', 'S02.6')
group by ds.DATE_BEGIN
order by ds.DATE_BEGIN;