set @date_start = '2020-12-05';
set @date_end = '2020-12-08';

set @row = 0; 
select @row:= @row+1, T.* from ( 
select coalesce(dms.long_name, "УФСИН") as smo_name, u.name as doc_name, 	
concat("ЗН №", o.Order_Number, " от ", 
date_format(o.orderCreateDate, "%d.%m.%Y"), ", ", pd.surname, " ", pd.name, " ", pd.second_name, ", ", 	date_format(pd.birthday, "%d.%m.%Y")) as case_data, 	
p.code, p.name, cs.service_cost, count(cs.id_profile), sum(cs.service_cost) as total 
from case_services cs 
left join price p on p.id = cs.id_profile 
left join orders o on o.id = cs.ID_ORDER 
left join patient_data pd on pd.id_human = o.Id_Human and pd.date_end > o.orderCreateDate and pd.date_begin <= o.orderCreateDate
left join doctor_spec ds on ds.doctor_id = cs.id_doctor 
left join users u on u.id = ds.user_id 
left join smo_dms dms on dms.id_smo = pd.dms_id_smo 
where o.order_type = 2 and o.orderCreateDate between @date_start and @date_end 
Group by o.id, p.code 
ORDER BY o.ordercreatedate, o.id ) T