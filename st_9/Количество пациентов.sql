set @date_start = '2020-04-01';
set @date_end = '2020-04-01';
drop temporary table if exists children;

create temporary table if not exists children
select c.date_begin, count(distinct pd.id_human) as child
from cases c
join case_services cs on cs.id_case = c.id_case
left join patient_data pd on pd.ID_PATIENT = c.ID_PATIENT
where date_add(pd.birthday, interval 18 year) > @date_end
	and c.DATE_BEGIN between @date_start and @date_end;

select 'Количество пациентов за период', 
	count(distinct pd.ID_HUMAN), child
from cases c
join case_services cs on cs.id_case = c.id_case
left join patient_data pd on c.ID_PATIENT = pd.ID_PATIENT
left join children ch on ch.DATE_BEGIN = c.DATE_BEGIN
where c.DATE_BEGIN between @date_start and @date_end;