-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

--
-- Set default database
--
USE stomadb;

DELIMITER $$

--
-- Create function `fio`
--
CREATE DEFINER = 'test_user'@'%'
FUNCTION fio (name text, surname text, patronymic text)
RETURNS text CHARSET cp1251
BEGIN
  RETURN CONCAT(
  `surname`,
  ' ',
  SUBSTRING(`name`, 1, 1),
  '. ',
  SUBSTRING(`patronymic`, 1, 1),
  '.');
END
$$

DELIMITER ;