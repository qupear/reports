DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `count_codes`(date_start date, date_end date, depart_id int, codes varchar(255), pay_type int, detail bool, closed bool)
BEGIN
  IF pay_type = 0 THEN
    IF detail THEN
      SELECT
        p.code,
        u.name AS doc_name,
        COUNT(p.code) AS code_count
      FROM case_services cs
        LEFT JOIN doctor_spec ds
          ON ds.doctor_id = cs.id_doctor
        LEFT JOIN users u
          ON u.id = ds.user_id
        LEFT JOIN departments d
          ON d.id = ds.department_id
        LEFT JOIN price p
          ON p.id = cs.id_profile
        LEFT JOIN orders o
          ON o.id = cs.id_order
      WHERE ((cs.date_begin BETWEEN date_start AND date_end
      AND cs.is_oms = 0
      AND cs.id_order < 0)
      OR (cs.is_oms = 0
      AND o.order_type <> 1
      AND ((o.orderCompletionDate BETWEEN date_start AND date_end
      AND o.status = 2) * closed
      OR (o.orderCreateDate BETWEEN date_start AND date_end
      AND o.status < 2) * (NOT closed))))
      AND (d.id = depart_id
      OR depart_id = 0)
      AND ((FIND_IN_SET(p.code, codes)
      OR CHAR_LENGTH(codes) < 2)
      OR codes = 'all')
      GROUP BY ds.doctor_id,
               cs.id_profile
      ORDER BY p.code, doc_name;
    ELSE
      SELECT
        p.code,
        COUNT(p.code) AS code_count
      FROM case_services cs
        LEFT JOIN doctor_spec ds
          ON ds.doctor_id = cs.id_doctor
        LEFT JOIN departments d
          ON d.id = ds.department_id
        LEFT JOIN price p
          ON p.id = cs.id_profile
        LEFT JOIN orders o
          ON o.id = cs.id_order
      WHERE ((cs.date_begin BETWEEN date_start AND date_end
      AND cs.is_oms = 0
      AND cs.id_order < 0)
      OR (cs.is_oms = 0
      AND o.order_type <> 1
      AND ((o.orderCompletionDate BETWEEN date_start AND @date_end
      AND o.status = 2) * closed
      OR (o.orderCreateDate BETWEEN date_start AND date_end
      AND o.status < 2) * (NOT closed))))
      AND (d.id = depart_id
      OR depart_id = 0)
      AND ((FIND_IN_SET(p.code, codes)
      OR CHAR_LENGTH(codes) < 2)
      OR codes = 'all')
      GROUP BY cs.id_profile
      ORDER BY p.code;
    END IF;
  ELSEIF pay_type = 1 THEN
    IF detail THEN
      SELECT
        vp.profile_infis_code AS code,
        u.name,
        COUNT(cs.id_profile) AS number
      FROM case_services cs
        LEFT JOIN doctor_spec ds
          ON ds.doctor_id = cs.id_doctor
        LEFT JOIN users u
          ON u.id = ds.user_id
        LEFT JOIN departments d
          ON d.id = ds.department_id
        LEFT JOIN vmu_profile vp
          ON vp.id_profile = cs.id_profile
      WHERE cs.date_begin BETWEEN date_start AND date_end
      AND cs.is_oms = 1
      AND (d.id = depart_id
      OR depart_id = 0)
      AND ((FIND_IN_SET(vp.profile_infis_code, codes)
      OR CHAR_LENGTH(codes) < 2)
      OR codes = 'all')
      GROUP BY ds.doctor_id,
               cs.id_profile
      ORDER BY code, u.name;
    ELSE
      SELECT
        vp.profile_infis_code AS code,
        COUNT(cs.id_profile) AS number
      FROM case_services cs
        LEFT JOIN doctor_spec ds
          ON ds.doctor_id = cs.id_doctor
        LEFT JOIN departments d
          ON d.id = ds.department_id
        LEFT JOIN vmu_profile vp
          ON vp.id_profile = cs.id_profile
      WHERE cs.date_begin BETWEEN date_start AND date_end
      AND cs.is_oms = 1
      AND (d.id = depart_id
      OR depart_id = 0)
      AND ((FIND_IN_SET(vp.profile_infis_code, codes)
      OR CHAR_LENGTH(codes) < 2)
      OR codes = 'all')
      GROUP BY cs.id_profile
      ORDER BY code;
    END IF;
  ELSE
    IF detail THEN
      SELECT
        p.code,
        u.name AS doc_name,
        COUNT(p.code) AS code_count
      FROM case_services cs
        LEFT JOIN doctor_spec ds
          ON ds.doctor_id = cs.id_doctor
        LEFT JOIN users u
          ON u.id = ds.user_id
        LEFT JOIN departments d
          ON d.id = ds.department_id
        LEFT JOIN price_orto_soc p
          ON p.id = cs.id_profile
        LEFT JOIN orders ord
          ON ord.id = cs.id_order
      WHERE ((cs.date_begin BETWEEN date_start AND date_end
      AND cs.is_oms = 2)
      OR (cs.id_order > 0
      AND ord.order_type = 1
      AND ((ord.orderCompletionDate BETWEEN date_start AND date_end
      AND ord.status = 2) * closed
      OR (ord.orderCreateDate BETWEEN date_start AND date_end
      AND ord.status < 2) * (NOT closed))))
      AND (d.id = depart_id
      OR depart_id = 0)
      AND ((FIND_IN_SET(p.code, codes)
      OR CHAR_LENGTH(codes) < 2)
      OR codes = 'all')
      GROUP BY ds.doctor_id,
               cs.id_profile
      ORDER BY p.code, doc_name;
    ELSE
      SELECT
        p.code,
        COUNT(p.code) AS code_count
      FROM case_services cs
        LEFT JOIN doctor_spec ds
          ON ds.doctor_id = cs.id_doctor
        LEFT JOIN departments d
          ON d.id = ds.department_id
        LEFT JOIN price_orto_soc p
          ON p.id = cs.id_profile
        LEFT JOIN orders ord
          ON ord.id = cs.id_order
      WHERE ((cs.date_begin BETWEEN date_start AND date_end
      AND cs.is_oms = 2)
      OR (cs.id_order > 0
      AND ord.order_type = 1
      AND ((ord.orderCompletionDate BETWEEN date_start AND date_end
      AND ord.status = 2) * closed
      OR (ord.orderCreateDate BETWEEN date_start AND date_end
      AND ord.status < 2) * (NOT closed))))
      AND (d.id = depart_id
      OR depart_id = 0)
      AND ((FIND_IN_SET(p.code, codes)
      OR CHAR_LENGTH(codes) < 2)
      OR codes = 'all')
      GROUP BY cs.id_profile
      ORDER BY p.code;
    END IF;
  END IF;
END$$
DELIMITER ;
