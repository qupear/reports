DELIMITER $$
CREATE DEFINER=`test_user`@`%` PROCEDURE `procedureUpdCalcJobs`( )
BEGIN
	DECLARE
		rawDone INT DEFAULT FALSE;
	DECLARE
		rawName VARCHAR ( 1024 );
	DECLARE
		rawId VARCHAR ( 12 );
	DECLARE
		cursRaw CURSOR FOR SELECT
		pc.CODE 
	FROM
		price pc
		 WHERE
		1 = 1 
		AND pc.DATE_END = '2200-01-01' 
	ORDER BY
		pc.CODE;
	DECLARE
		CONTINUE HANDLER FOR NOT FOUND 
		SET rawDone = TRUE;
	DROP TEMPORARY TABLE
	IF
		EXISTS tblFinal;
	CREATE TEMPORARY TABLE
	IF
		NOT EXISTS tblFinal ( rCd VARCHAR ( 12 ), rNm VARCHAR ( 1024 ) );
	DROP TEMPORARY TABLE
	IF
		EXISTS rawCodesNames;
	CREATE TEMPORARY TABLE
	IF
		NOT EXISTS rawCodesNames AS (
		SELECT
			pc.CODE AS cd,
			pc.NAME AS nm 
		FROM
			price pc
			 WHERE
			1 = 1 
			AND pc.DATE_END = '2200-01-01' 
		ORDER BY
			pc.CODE 
		);
	OPEN cursRaw;
	read_loop :
	LOOP
			FETCH cursRaw INTO rawId;
		IF
			rawDone THEN
				LEAVE read_loop;
			
		END IF;
		SELECT
			rawCodesNames.nm 
		FROM
			rawCodesNames 
		WHERE
			rawCodesNames.cd = rawId INTO rawName;
		IF
			( LEFT ( rawId, 1 ) = '1' ) THEN
				INSERT INTO calculation_types_jobs ( section, id_services, name_services )
			VALUES
				( 1, CAST( rawId AS UNSIGNED ), rawName );
			
		END IF;
		
		IF
			( LEFT ( rawId, 1 ) = '2' ) THEN
				INSERT INTO calculation_types_jobs ( section, id_services, name_services )
			VALUES
				( 2, CAST( rawId AS UNSIGNED ), rawName );
			
		END IF;
		
		IF
			( LEFT ( rawId, 1 ) = '3' ) THEN
				INSERT INTO calculation_types_jobs ( section, id_services, name_services )
			VALUES
				( 3, CAST( rawId AS UNSIGNED ), rawName );
			
		END IF;
		
				IF
			( LEFT ( rawId, 1 ) = '5' ) THEN
				INSERT INTO calculation_types_jobs ( section, id_services, name_services )
			VALUES
				( 4, CAST( rawId AS UNSIGNED ), rawName );
			
		END IF;
		
		IF
			( LEFT ( rawId, 1 ) = '9' ) THEN
			IF
			( LENGTH( rawId ) = 3 ) THEN
				INSERT INTO calculation_types_jobs ( section, id_services, name_services )
			VALUES
				( 5, CAST( rawId AS UNSIGNED ), rawName );
		END IF;	
		END IF;
		
						IF
			( LEFT ( rawId, 1 ) = '7' ) THEN
				INSERT INTO calculation_types_jobs ( section, id_services, name_services )
			VALUES
				( 6, CAST( rawId AS UNSIGNED ), rawName );
			
		END IF;
		
								IF
			( LEFT ( rawId, 1 ) = '4' ) THEN
				INSERT INTO calculation_types_jobs ( section, id_services, name_services )
			VALUES
				( 11, CAST( rawId AS UNSIGNED ), rawName );
			
		END IF;
		
		IF
			( LEFT ( rawId, 1 ) = '8' ) THEN
				INSERT INTO calculation_types_jobs ( section, id_services, name_services )
			VALUES
				( 5, CAST( rawId AS UNSIGNED ), rawName );
			
		END IF;
		
				IF
			( LEFT ( rawId, 2 ) in ('90','99') ) THEN
				INSERT INTO calculation_types_jobs ( section, id_services, name_services )
			VALUES
				( 8, CAST( rawId AS UNSIGNED ), rawName );
			
		END IF;
				
		INSERT INTO tblFinal ( rCd, rNm )
		VALUES
			( rawId, rawName );
		
	END LOOP;
	CLOSE cursRaw;
	SELECT
		* 
	FROM
		tblFinal;

END$$
DELIMITER ;
