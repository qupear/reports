DELIMITER $$
CREATE DEFINER=`test_user`@`%` PROCEDURE `zn_tech_st8`(IN date_start date, IN date_end_ date, IN closed int, IN pu int, IN techn_id int)
begin
    set @rn = 0;
select  @rn := @rn + 1, T.tech_name, T.order_number, T.orderCreateDate, T.orderCompletionDate, 
    case when T.service_type = 100 then '�������' 
		when T.service_type = 90 then '��������������'     
		when T.service_type = 75 then '�������������' 
		when T.service_type = 50 then '��. � ���. �������' 
		else '������' end as group_name, 
    T.total, T.doc_name 
from
(select t.name as tech_name, ord.order_number, ord.orderCreateDate, ord.orderCompletionDate,
        MAX(case when p.group_ids like '%1%' then 100 /*'�������'*/ 
             when p.group_ids like '%4%' then 90 /*'��������������'*/         
             when p.group_ids like '%2%' then 75 /*'�������������'*/ 
             when p.group_ids like '%3%' then 50 /*'��. � ���. �������'*/ 
             when pos.ID_GROUP like '%111%' then 100 /*'�������'*/ 
             when pos.ID_GROUP like '%38%' or pos.ID_GROUP like '%39%' or pos.ID_GROUP like '%40%' or pos.ID_GROUP like '%41%' then 90 /*'��������������'*/              
             when pos.ID_GROUP like '%112%' then 50 /*'��. � ���. �������'*/ 
             else 0 /*'������'*/ end) as service_type,
        sum(cs.service_cost) as total,
        u.name as doc_name
from case_services cs
left join orders ord on ord.id = cs.id_order
left join doctor_spec ds on ds.doctor_id = ord.id_doctor
left join users u on u.id = ds.user_id
left join technicians t on t.id = ord.id_technician
left join price p on p.id = cs.id_profile and ord.order_type <> 1 
left join price_orto_soc pos on pos.id = cs.id_profile and ord.order_type = 1 
left join patient_data pd on pd.id_human = ord.id_human and pd.is_active = 1 and pd.date_end = '2200-01-01'
left join patient_address pa on pa.id_address = pd.id_address_reg and pd.id_address_reg <> 1
where ((ord.orderCompletionDate between date_start and date_end_ and ord.status = 2) * closed
    or (ord.orderCreateDate between date_start and date_end_ and ord.status < 2 ) * (not closed))
  and (ord.id_technician = techn_id or techn_id = 0) and t.id <> 1
  and ((p.in_tech = 1 and pu in (0,1)) or (pos.in_tech = 1 and pu = 2))
  and ((ord.order_type = 0 and pu = 0) or (ord.order_type = 1 and pu = 2) or (ord.order_type = 2 and pu = 1))
 group by ord.id 
order by tech_name) T;
end$$
DELIMITER ;
