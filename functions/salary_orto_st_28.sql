DELIMITER $$
CREATE DEFINER=`test_user`@`%` PROCEDURE `salary_orto_st_28`(IN date_start DATE, IN date_end DATE, IN depart_id INT, IN pu INT, IN detail INT)
begin
/*detail: 0-����, 1-������, 2-����������*/
set @rn = 0;

if detail = 0 then
select  @rn := @rn + 1, T.*
from(
SELECT dep.name, u.name as doc_name, sum(cs.service_cost) as total_zn, round(SUM(cs.service_cost * cs.discount),2) as total_sum,  
 concat('������� �����:', ' ', ds.doc_percent, '%') as doc_percent, ds.doc_percent_with_tech ,
 case ord.Id_technician when 1 
 then round((SUM(cs.service_cost * cs.discount) *  ds.doc_percent) / 100, 2)
else
   round((SUM(cs.service_cost * cs.discount) *  ds.doc_percent_with_tech) / 100, 2)
 end
 as salary
 from  case_services AS cs
 left join orders ord on cs.ID_ORDER = ord.id AND ord.status = 2
 left join doctor_spec ds on ds.doctor_id = ord.id_doctor
 left join departments dep on ds.department_id = dep.id
 left join users u on u.id = ds.user_id 
left join price pr on pr.id= cs.id_profile and ord.order_type <> 1 
left join price_orto_soc pos on pos.id = cs.ID_PROFILE and ord.order_type = 1 
WHERE cs.id_order >= 0 
AND ((ord.order_type = 0 and pu = 0) or (ord.order_type = 1 and pu = 2) or (ord.order_type = 2 and pu = 1))
and ord.orderCompletionDate between date_start AND date_end 
and (ds.department_id = depart_id or depart_id = 0) and LEFT(u.name,1)<>'.'
Group by ds.department_id, doc_percent, ds.doctor_id 
Order by ds.department_id, doc_percent, doc_name) T;

elseif detail = 1 then
select  @rn := @rn + 1, T.*
from(
SELECT TECH.name as tech_name, SUM(cs.service_cost) as total_zn, round(SUM(cs.service_cost * cs.discount),2) as total_sum, 
tech.tech_percent,  round((SUM(cs.service_cost * cs.discount) *  tech.tech_percent) / 100, 2) as salary
FROM case_services AS cs
left join orders ord on cs.id_order = ord.id AND ord.status = 2
left join doctor_spec ds on ds.doctor_id = ord.id_doctor
left join price pr on pr.id= cs.id_profile and ord.order_type <> 1 
left join price_orto_soc pos on pos.id = cs.ID_PROFILE and ord.order_type = 1 
left join technicians tech on tech.id = ord.Id_technician
WHERE cs.id_order >= 0 
AND ((ord.order_type = 0 and pu = 0 and pr.in_tech = 1) or (ord.order_type = 1 and pu = 2 and pos.IN_TECH=1) or (ord.order_type = 2 and pu = 1 and pr.in_tech = 1))
and ord.orderCompletionDate between date_start AND date_end 
and (ds.department_id = depart_id or depart_id = 0) 
GROUP BY Id_technician 
ORDER BY tech_name) T;

else
select  @rn := @rn + 1, T.*
from(
SELECT pers.name as pers_name, SUM(cs.service_cost) as total_zn, round(SUM(cs.service_cost * cs.discount),2) as total_sum, 
pers.procent As pol_percent,  round((SUM(cs.service_cost * cs.discount) *  pers.procent) / 100, 2) as salary
FROM case_services AS cs
left join orders ord on cs.id_order = ord.id AND ord.status = 2
left join doctor_spec ds on ds.doctor_id = ord.id_doctor
left join price pr on pr.id= cs.id_profile and ord.order_type <> 1 
left join price_orto_soc pos on pos.id = cs.ID_PROFILE and ord.order_type = 1
left join personal pers on pers.id = ord.id_polir
WHERE cs.id_order >= 0 
AND ((ord.order_type = 0 and pu = 0 and pr.in_tech = 1) or (ord.order_type = 1 and pu = 2 and pos.IN_TECH=1) or (ord.order_type = 2 and pu = 1 and pr.in_tech = 1))
and ord.orderCompletionDate between date_start And date_end 
and (ds.department_id = depart_id or depart_id = 0) 
GROUP BY pers.ID 
ORDER BY pers_name) T;
end if;
end$$
DELIMITER ;
