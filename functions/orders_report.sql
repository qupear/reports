DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `orders_report`(date_start DATE, date_end DATE, doc_id INT, depart_id INT, techn_id INT, closed BOOL, pu INT, spec INT, tech BOOL, detail INT)
BEGIN
	if detail = 0 then
		select d.name as dept_name, 
			case tech when True then t.name else u.name end as doc_name,  
			case tech when True Then 
				concat("����� ", ord.Order_Number, " (������� �", pd.card_number, " ", pd.surname, " ", pd.name, " ", pd.second_name, "), ���� ", u.name ) 
			else
				concat("����� ", ord.Order_Number, " (������� �", pd.card_number, " ", pd.surname, " ", pd.name, " ", pd.second_name, "), ������ ", t.name ) 
			end as order_name,  
            case pu when 0 then p.code when 1 then p.code else pos.code end as serv_code, 
            case pu when 0 then p.name when 1 then p.name else pos.name end as serv_name, 
            count(cs.id_profile) as cnt, sum(cs.service_cost * cs.discount) +    
			sum(cs.SERVICE_COST * (1 - cs.discount)*(cs.discount=0.3 or cs.discount=0)) as sum     
		from orders ord 
		left join case_services cs on cs.id_order = ord.id 
		left join doctor_spec ds on ds.doctor_id = ord.id_doctor 
		left join departments d on d.id = ds.department_id 
		left join users u on u.id = ds.user_id 
		left join patient_data pd on pd.id_human = ord.id_human  and pd.IS_ACTIVE = 1 and pd.DATE_END = '2200-01-01' 
		left join technicians t on t.id = ord.id_technician 
		left join price p on p.id = cs.id_profile and ord.order_type in (0,2) 
		left join price_orto_soc pos on pos.id = cs.id_profile and ord.order_type = 1  
		where ((ord.orderCompletionDate between date_start and date_end and ord.status = 2) * closed 
			or (ord.orderCreateDate between date_start and date_end and ord.status < 2) * (not closed))
			and (d.id = depart_id or depart_id = 0)  
			and (ds.doctor_id = doc_id or doc_id = 0)  
			and (ord.id_technician = techn_id or techn_id = 0) 
                    and ((tech <> 1) or ((p.in_tech = 1 and pu in (0,1)) or (pos.in_tech = 1 and pu = 2)))  
			and ((ord.order_type = 0 and pu = 0) or (ord.order_type = 2 and pu = 1) or (ord.order_type = 1 and pu = 2)) 
			and ds.spec_id = spec 
			group by ord.id, cs.id_profile 
			having cnt > 0
			order by dept_name, doc_name, ord.id ;
	elseif detail = 1 then 
		select d.name as dept_name, 
			case tech when True then t.name else u.name end as doc_name,  
			case tech when True Then 
				concat("����� ", ord.Order_Number, " (������� �", pd.card_number, " ", pd.surname, " ", pd.name, " ", pd.second_name, "), ���� ", u.name ) 
			else
				concat("����� ", ord.Order_Number, " (������� �", pd.card_number, " ", pd.surname, " ", pd.name, " ", pd.second_name, "), ������ ", t.name ) 
			end as order_name,  
			sum(cs.service_cost * cs.discount) +    
				sum(cs.SERVICE_COST * (1 - cs.discount)*(cs.discount=0.3 or cs.discount=0)) as sum     
		from orders ord 
		left join case_services cs on cs.id_order = ord.id 
		left join doctor_spec ds on ds.doctor_id = ord.id_doctor 
		left join departments d on d.id = ds.department_id 
		left join users u on u.id = ds.user_id 
		left join patient_data pd on pd.id_human = ord.id_human  and pd.IS_ACTIVE = 1 and pd.DATE_END = '2200-01-01' 
		left join technicians t on t.id = ord.id_technician  
        left join price p on p.id = cs.id_profile and ord.order_type in (0,2) 
		left join price_orto_soc pos on pos.id = cs.id_profile and ord.order_type = 1 
		where ((ord.orderCompletionDate between date_start and date_end and ord.status = 2) * closed 
			or (ord.orderCreateDate between date_start and date_end and ord.status < 2) * (not closed))
			and (d.id = depart_id or depart_id = 0)  
			and (ds.doctor_id = doc_id or doc_id = 0)  
			and (ord.id_technician = techn_id or techn_id = 0)  
                and ((tech <> 1) or ((p.in_tech = 1 and pu in (0,1)) or (pos.in_tech = 1 and pu = 2)))    
			and ((ord.order_type = 0 and pu = 0) or (ord.order_type = 2 and pu = 1) or (ord.order_type = 1 and pu = 2))  
			and ds.spec_id = spec 
			group by ord.id  
			having sum > 0
			order by dept_name, doc_name, ord.id ;
	else 
		select d.name as dept_name, 
			case tech when True then t.name else u.name end as doc_name,  
			sum(cs.service_cost * cs.discount) +    
				sum(cs.SERVICE_COST * (1 - cs.discount)*(cs.discount=0.3 or cs.discount=0)) as sum, 
                            case pu when 0 then sum(p.uet) when 1 then 0 else sum(pos.uet) end       
		from orders ord 
		left join case_services cs on cs.id_order = ord.id 
		left join doctor_spec ds on ds.doctor_id = ord.id_doctor 
		left join departments d on d.id = ds.department_id 
		left join users u on u.id = ds.user_id 
		left join patient_data pd on pd.id_human = ord.id_human  and pd.IS_ACTIVE = 1 and pd.DATE_END = '2200-01-01' 
		left join technicians t on t.id = ord.id_technician 
		left join price p on p.id = cs.id_profile and ord.order_type in (0,2) 
		left join price_orto_soc pos on pos.id = cs.id_profile and ord.order_type = 1  
		where ((ord.orderCompletionDate between date_start and date_end and ord.status = 2) * closed 
			or (ord.orderCreateDate between date_start and date_end and ord.status < 2) * (not closed))
			and (d.id = depart_id or depart_id = 0)  
			and (ds.doctor_id = doc_id or doc_id = 0)  
			and (ord.id_technician = techn_id or techn_id = 0) 
                and ((tech <> 1) or ((p.in_tech = 1 and pu in (0,1)) or (pos.in_tech = 1 and pu = 2)))  
			and ((ord.order_type = 0 and pu = 0) or (ord.order_type = 2 and pu = 1) or (ord.order_type = 1 and pu = 2)) 
			and ds.spec_id = spec 
			group by ds.doctor_id   
			order by dept_name, doc_name, ord.id ;
	end if;
   END$$
DELIMITER ;
