DELIMITER $$
CREATE DEFINER=`test_user`@`%` PROCEDURE `list_zn_st8`(IN date_start DATE, IN date_end DATE, IN doc_id INT, IN closed int, IN pu INT)
begin
 set @rn = 0;
select  @rn := @rn + 1, T.card_number, T.order_number, T.orderCreateDate, T.orderCompletionDate, T.pat, T.addr, T.doc_name, T.tec_name, 
	case when T.service_type = 100 then '�������' 
		when T.service_type = 90 then '��������������' 
		when T.service_type = 75 then '�������������' 
		when T.service_type = 50 then '��. � ���. �������' 
		else '������' end as group_name,
	T.summ, T.avance, T.debt 
from
(select pd.CARD_NUMBER, o.order_number , o.orderCreateDate,  o.orderCompletionDate,
       concat(pd.surname,' ', pd.name,' ', pd.second_name) as pat, concat(pa.UNSTRUCT_ADDRESS, '   ', coalesce(pd.REMARK,  ' ')) as addr,
       concat(SUBSTRING_INDEX(u.name, ' ', 1), ' ',
left(SUBSTRING_INDEX(SUBSTRING_INDEX(u.name, ' ', 2), ' ', -1), 1), '. ',
left(SUBSTRING_INDEX(SUBSTRING_INDEX(u.name, ' ', -1), ' ', -1), 1), '.') as doc_name,
concat(SUBSTRING_INDEX(tec.name, ' ', 1), ' ',
left(SUBSTRING_INDEX(SUBSTRING_INDEX(tec.name, ' ', 2), ' ', -1), 1), '. ',
left(SUBSTRING_INDEX(SUBSTRING_INDEX(tec.name, ' ', -1), ' ', -1), 1), '.') as tec_name,
  MAX(case when p.group_ids like '%1%' then 100 /*'�������'*/ 
                        when p.group_ids like '%4%' then 90 /*'��������������'*/ 
                        when p.group_ids like '%2%' then 75 /*'�������������'*/ 
                        when p.group_ids like '%3%' then 50 /*'��. � ���. �������'*/ 
                        when pos.ID_GROUP like '%111%' then 100 /*'�������'*/ 
                        when pos.ID_GROUP like '%38%' or pos.ID_GROUP like '%39%' or pos.ID_GROUP like '%40%' or pos.ID_GROUP like '%41%' then 90 /*'��������������'*/ 
                        when pos.ID_GROUP like '%112%' then 50 /*'��. � ���. �������'*/
                        else 0 /*'������'*/ end) 
                    as service_type ,  coalesce(sum(cs.SERVICE_COST ), 0) as summ, coalesce(prep.summ, 0) * (pu=0) as avance, (coalesce(sum(cs.SERVICE_COST)) - coalesce(prep.summ, 0)) * (pu=0) as debt
                    
FROM orders o
    left join case_services cs on cs.id_order = o.id
    left join patient_data pd on o.id_human= pd.id_human and pd.is_active = 1 and pd.date_end = '2200-01-01'
    left join patient_address pa on pd.ID_ADDRESS_REG = pa.ID_ADDRESS
    left join discounts dis on dis.id = o.id_discount
    left join price_orto_soc pos on cs.id_profile = pos.id and o.order_type = 1 
    left join price p on cs.id_profile = p.id and o.order_type <> 1 
    left join doctor_spec ds on o.id_doctor = ds.doctor_id
    left join users u on ds.user_id = u.id
    left join technicians tec on o.Id_technician = tec.id
    left join (select order_id, sum(summ) as summ from prepayments where order_id > 0 group by order_id) prep on cs.ID_ORDER = prep.order_id
WHERE  ((o.orderCompletionDate between date_start and date_end and o.status = 2) * closed
      or (o.orderCreateDate between date_start and date_end  and o.status < 2) * (not closed)) and
  ((o.order_type = 0 and pu = 0) or (o.order_type = 2 and pu = 1) or (o.order_type = 1 and pu = 2)) 
  and (ds.doctor_id = doc_id or doc_id = 0) and cs.id_order > 0 
group by o.id 
order by doc_name, pat ) T;
end$$
DELIMITER ;
