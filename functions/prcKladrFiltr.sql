DELIMITER $$
CREATE DEFINER=`test_user`@`%` PROCEDURE `prcKladrFiltr`(IN `IN_STR_NAME` varchar(255))
BEGIN
  DECLARE currentName varchar(255);
  DECLARE currentC varchar(255);
  DECLARE currentG varchar(255);
  DECLARE strC varchar(255);
  DECLARE idG int;
  DECLARE strR varchar(255);
  DECLARE idR int;
  DECLARE strP varchar(255);
  DECLARE strG varchar(255);
  DECLARE idC int;
  DECLARE poop int;

  DECLARE strG_C varchar(255);
  DECLARE strG_R varchar(255);
  DECLARE strG_G varchar(255);

  DECLARE strR_C varchar(255);
  DECLARE strR_R varchar(255);

  DECLARE resultC varchar(255) DEFAULT ' ';
  DECLARE resultR varchar(255) DEFAULT ' ';
  DECLARE resultG varchar(255) DEFAULT ' ';
  DECLARE resultP varchar(255) DEFAULT ' ';
  DECLARE resultType varchar(10) DEFAULT ' ';
  DECLARE rawDone int DEFAULT FALSE;
  DECLARE rawId int;
  DECLARE rawNum int;
  DECLARE cursRaw CURSOR FOR
  SELECT
    id, idStr
  FROM rawSearch s;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET rawDone = TRUE;
  DROP TEMPORARY TABLE IF EXISTS tblResults;
  CREATE TEMPORARY TABLE IF NOT EXISTS tblResults (
    resId int,
    resC varchar(255),
    resR varchar(255),
    resG varchar(255),
    resP varchar(255),
    resType varchar(10)
  );
  DROP TEMPORARY TABLE IF EXISTS rawSearch;
  CREATE TEMPORARY TABLE IF NOT EXISTS rawSearch (
  id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    idStr  varchar(255)
  );
  INSERT into rawSearch (idStr) SELECT
      vk.ID_KLADR
    FROM vmu_kladr vk
    WHERE vk.KLADR_NAME = IN_STR_NAME;
  OPEN cursRaw;
read_loop:
  LOOP
    FETCH cursRaw INTO rawNum,rawId;
    IF rawDone THEN
      LEAVE read_loop;
    END IF;
		
    set resultType=(SELECT vk.KLADR_TYPE FROM vmu_kladr vk WHERE vk.ID_KLADR = rawId);

    
    SET strP = (SELECT
        vk.KLADR_CODE_P
      FROM vmu_kladr vk
      WHERE vk.ID_KLADR = rawId);
    SET strG = (SELECT
        vk.KLADR_CODE_G
      FROM vmu_kladr vk
      WHERE vk.ID_KLADR = rawId);
    SET strR = (SELECT
        vk.KLADR_CODE_R
      FROM vmu_kladr vk
      WHERE vk.ID_KLADR = rawId);
    SET strC = (SELECT
        vk.KLADR_CODE_C
      FROM vmu_kladr vk
      WHERE vk.ID_KLADR = rawId);
    SET idG = (SELECT
        ID_KLADR
      FROM vmu_kladr
      WHERE ID_KLADR <= rawId
      AND KLADR_CODE_G = strG
      AND KLADR_CODE_R = strR
      AND KLADR_CODE_C = strC
      AND KLADR_CODE_P = '000'
      ORDER BY ID_KLADR DESC LIMIT 1);
    SET resultG = (SELECT
        KLADR_NAME
      FROM vmu_kladr
      WHERE ID_KLADR <= rawId
      AND KLADR_CODE_G = strG
      AND KLADR_CODE_R = strR
      AND KLADR_CODE_C = strC
      AND KLADR_CODE_P = '000'
      ORDER BY ID_KLADR DESC LIMIT 1);
    
    SET resultR = (SELECT
        KLADR_NAME
      FROM vmu_kladr
      WHERE ID_KLADR <= idG
      AND KLADR_CODE_R = strR
      AND KLADR_CODE_C = strC
      AND KLADR_CODE_G = '000'
      AND KLADR_CODE_P = '000'
      ORDER BY ID_KLADR DESC LIMIT 1);
    IF resultG=resultR THEN
      set resultG='���';
    END IF;
    SET resultC = (SELECT
        KLADR_NAME
      FROM vmu_kladr
      WHERE ID_KLADR <= idG
      AND KLADR_CODE_C = strC
      AND KLADR_CODE_G = '000'
      AND KLADR_CODE_P = '000'
      AND KLADR_CODE_R = '000'
      ORDER BY ID_KLADR DESC LIMIT 1);
    SET currentName = (SELECT
        vk.KLADR_NAME
      FROM vmu_kladr vk
      WHERE vk.ID_KLADR = rawId);
    INSERT INTO tblResults (resId, resC, resR, resG, resP,resType)
            VALUES (rawNum, resultC, resultR, resultG, currentName,resultType);
  END LOOP;
  CLOSE cursRaw;
  SELECT
    *
  FROM tblResults r;
END$$
DELIMITER ;
