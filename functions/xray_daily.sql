DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `xray_daily`(date_start DATE, doc_id INT, doc BOOL)
BEGIN

create temporary table if not exists doc_xrays 
select ro.id as ro_id, ro.dose, concat(pd.surname, " ", pd.name, " ", pd.second_name) as pat_name, pd.birthday, pa.unstruct_address,  
    ro.shots, 
    		coalesce(concat(SUBSTRING_INDEX(u.name, ' ', 1), ' ', 
				 left(SUBSTRING_INDEX(SUBSTRING_INDEX(u.name, ' ', 2), ' ', -1), 1), '. ',            
		         left(SUBSTRING_INDEX(SUBSTRING_INDEX(u.name, ' ', -1), ' ', -1), 1), '.'), "���� �� ������� ���") as send_doc,  
	di.date_value, di.teeth_value, di.child_ref_ids, di.id_arbitrary_text, di.value, art.text as arbitrary_text, ro.date as shot_date   
from roentgen ro 
left join directions di on di.id = ro.id_direction 
left join doctor_spec ds on ds.doctor_id = di.id_send_doctor 
left join users u on u.id = ds.user_id 
left join cases c on c.id_case = ro.case_id 
left join patient_data pd on pd.id_patient = c.id_patient 
left join patient_address pa on pa.id_address = pd.id_address_reg 
left join arbitrary_texts art on art.id = di.id_arbitrary_text 
where ((ro.date = date_start and ro.Assist_Doc_ID = doc_id) or doc) and ((ro.date_description = date_start and ro.Doc_id = doc_id) or not doc)  
    AND di.id_direction in (2,32) and ro.dose > 0; 

create temporary table if not exists xray_proto_1 
select T.ro_id, concat(T.s1, T.s2, T.s3) as val from       
(select dx.ro_id,  
	case when (locate(';50;', dx.child_ref_ids) > 0) then "������ " else "" end as s1,   
	case when (locate(';60;', dx.child_ref_ids) > 0 or locate('810', dx.child_ref_ids) > 0) then "� ���������" else "" end as s2,  
	case when (locate(';70;', dx.child_ref_ids) > 0 or locate('820', dx.child_ref_ids) > 0) then "��� ��������" else "" end as s3 
from doc_xrays dx) T; 

create temporary table if not exists xray_proto_2 
select T.ro_id, concat(T.s1, T.s2, T.s3, T.s4, T.s5, T.s6, T.s7, T.s8, T.s9, T.s10, T.s11, T.s12, T.s13, T.s14, T.s15, T.s16, T.s17, T.s18, T.s19, T.s20, T.s21, T.s22, T.s23, T.s24, T.s25, T.s26, T.s27) as val from       
(select dx.ro_id,  
	case when (locate(';90;', dx.child_ref_ids) > 0) then concat("������������� ������ (��������) ", dx.teeth_value, " ����(��). ") else "" collate utf8_general_ci end as s1,   
	case when (locate('100', dx.child_ref_ids) > 0) then concat("������������� ������ (��������) ", dx.teeth_value, " ����(��). ") else "" collate utf8_general_ci end as s2,   
	case when (locate('110', dx.child_ref_ids) > 0) then "������ ������� ������ ���������� ������. " else "" collate utf8_general_ci end as s3,   
	case when (locate('120', dx.child_ref_ids) > 0) then "������ ������� ����� ���������� ������. " else "" collate utf8_general_ci end as s4,   
	case when (locate('130', dx.child_ref_ids) > 0) then "������ ���� ������ ��������. " else "" collate utf8_general_ci end as s5,   
	case when (locate('140', dx.child_ref_ids) > 0) then "��� ������� ��� ������ ��������. " else "" collate utf8_general_ci end as s6,   
	case when (locate('150', dx.child_ref_ids) > 0) then "���� (��������). " else "" collate utf8_general_ci end as s7,   
	case when (locate('160', dx.child_ref_ids) > 0) then "���� (��������). " else "" collate utf8_general_ci end as s8,   
	case when (locate('161', dx.child_ref_ids) > 0) then "���� ��������������� ������ (��������). " else "" collate utf8_general_ci end as s9,   
	case when (locate('162', dx.child_ref_ids) > 0) then "���� ��������������� ������ (��������). " else "" collate utf8_general_ci end as s10,   
	case when (locate('190', dx.child_ref_ids) > 0) then "���� ���� (��������/�������� ���) (��������). " else "" collate utf8_general_ci end as s11,   
	case when (locate('200', dx.child_ref_ids) > 0) then "���� ���� (��������/�������� ���) (��������). " else "" collate utf8_general_ci end as s12,   
	case when (locate('205', dx.child_ref_ids) > 0) then "���� (�� 3D). " else "" collate utf8_general_ci end as s13,   
	case when (locate('207', dx.child_ref_ids) > 0) then "���� (�� 3D) ���� ����������� ����� ���� 13�15��. " else "" collate utf8_general_ci end as s14,   
	case when (locate('210', dx.child_ref_ids) > 0) then "���� (�� 3D) ������� � ������ �������. " else "" collate utf8_general_ci end as s15,   
	case when (locate('220', dx.child_ref_ids) > 0) then "���� (�� 3D) ������� ������� � ��������������� ������. " else "" collate utf8_general_ci end as s16,   
	case when (locate('224', dx.child_ref_ids) > 0) then "���� (�� 3D) ���������������� ������ ������. " else "" collate utf8_general_ci end as s17,   
	case when (locate('227', dx.child_ref_ids) > 0) then "���� (�� 3D) ���������������� ������ �����. " else "" collate utf8_general_ci end as s18,   
	case when (locate('230', dx.child_ref_ids) > 0) then "���� (�� 3D) �������-�������������� ������ ������. " else "" collate utf8_general_ci end as s19,   
	case when (locate('240', dx.child_ref_ids) > 0) then "���� (�� 3D) �������-�������������� ������ �����. " else "" collate utf8_general_ci end as s20,   
	case when (locate('241', dx.child_ref_ids) > 0) then "���� (�� 3D) 2� �������-�������������� �������� 8�15��. " else "" collate utf8_general_ci end as s21,   
	case when (locate('250', dx.child_ref_ids) > 0) then "��� ������. " else "" collate utf8_general_ci end as s22,   
	case when (locate('260', dx.child_ref_ids) > 0) then "��� �������. " else "" collate utf8_general_ci end as s23,   
	case when (locate('570', dx.child_ref_ids) > 0) then "������������ ���� ������� ����� ����� (��������)." else "" collate utf8_general_ci end as s27,
	case when (locate('840', dx.child_ref_ids) > 0) then concat("���� (�� 3D) KaVo ", coalesce(dx.value, ""), ". ") else "" collate utf8_general_ci end as s24,   
	case when (locate('850', dx.child_ref_ids) > 0) then concat("���� (�� 3D) Sirona ", coalesce(dx.value, ""), ". ") else "" collate utf8_general_ci end as s25,   
	case when (locate('930', dx.child_ref_ids) > 0) then "����. " else "" collate utf8_general_ci end as s26   
from doc_xrays dx) T; 

create temporary table if not exists xray_proto_3 
select T.ro_id, concat(T.s1, T.s2, T.s3, T.s4, T.s5, T.s6, T.s7, T.s8, T.s9, T.s10) as val from       
(select dx.ro_id,  
	case when (locate('330', dx.child_ref_ids) > 0) then "���" else "" end as s1,   
	case when (locate('340', dx.child_ref_ids) > 0) then "���" else "" end as s2,  
	case when (locate('350', dx.child_ref_ids) > 0) then "���" else "" end as s3,  
	case when (locate('360', dx.child_ref_ids) > 0) then "�/�" else "" end as s4,  
	case when (locate('370', dx.child_ref_ids) > 0) then "����" else "" end as s5,  
	case when (locate('380', dx.child_ref_ids) > 0) then "��� ���" else "" end as s6,  
	case when (locate('390', dx.child_ref_ids) > 0) then "��� ���" else "" end as s7,  
	case when (locate('400', dx.child_ref_ids) > 0) then "��" else "" end as s8,  
	case when (locate('401', dx.child_ref_ids) > 0) then "�����" else "" end as s9,  
	case when (locate('920', dx.child_ref_ids) > 0) then "����" else "" end as s10  
from doc_xrays dx) T; 

set @num = 0;
select @num := @num + 1, T.* from 
(select dx.pat_name, dx.birthday, dx.unstruct_address, xr1.val v1, xr2.val v2, dx.shots, dx.shot_date, dx.send_doc, dx.dose, xr3.val v3, dx.arbitrary_text  
from doc_xrays dx 
left join xray_proto_1 xr1 on dx.ro_id = xr1.ro_id 
left join xray_proto_2 xr2 on dx.ro_id = xr2.ro_id 
left join xray_proto_3 xr3 on dx.ro_id = xr3.ro_id 
order by dx.ro_id ) T; 

END$$
DELIMITER ;
