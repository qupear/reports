DELIMITER $$
CREATE DEFINER=`test_user`@`%` PROCEDURE `export`(
IN ID_CASE_MIS INT, 
IN ID_SERVICE_MIS INT)
BEGIN
SELECT RowNumber FROM (SELECT id_service, @i:=@i+1 AS `RowNumber`    
FROM case_services, (SELECT @i:=0) AS `RowNumberTable` where id_case = (ID_CASE_MIS)and is_oms=1) x    
WHERE id_service = (ID_SERVICE_MIS);
END$$
DELIMITER ;
