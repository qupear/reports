CREATE DEFINER=`test_user`@`%` FUNCTION `stomadb`.`short_name`(long_name varchar(255)) RETURNS varchar(255) CHARSET cp1251
BEGIN
 DECLARE short_name varchar(255);
  select concat(substring_index(long_name, ' ', 1), ' ',
       left(substring_index(substring_index(long_name, ' ', -2), ' ', 1), 1),
'. ',
        left(substring_index(long_name, ' ', -1),1), '.') into short_name;
  RETURN short_name;
END